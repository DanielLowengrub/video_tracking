# README #

The purpose of this project is to compute the real world positions of soccer players
that appear in a sequence of video frames.

Here is a [video](https://www.dropbox.com/s/7trtyl8a6kwc774/absolute_positions.mp4?dl=0) of the system in action.

The main work is done in the preprocessing stage.
During this stage, we successively overlay auxillary data on each frame. These data include image segmentation,
camera coefficients and player positions.

After preprocessing, the players are tracked through the video with a particle filter.

Here is an overview of the source directories:
* auxillary: This contains general purpose code such as iterator operations, mask operations, common numpy operations and camera matrix methods.

* dynamics: The particle filter used to track the players and ball.

* image_processing: Modules to extract information from an image. This includes blob detections, line and ellipse detection and foreground detection.

* nn: Tensorflow code for segmenting an image and finding the camera.

* postprocessing: cleanup after the players have been tracked.

* preprocessing: This is where the majority of the code is launched from. Below is a description of the preprocessing stage.

### Preprocessing Overview ###

INPUT: A list of images image_0.png,...,image_N.png

OUTPUT:
The outcome of the preprocessing step is to produce the following subdirectories of preprocessing/.
For each directory in preprocessing/ there is a corresponding directory in visualization/ which displays the information in a picture.

Here are descriptions what data is generated at each preprocessing step:

1) annotation: a list of files annotation_i.npy. annotation_i.npy is a numpy array A with shape (n,m) and type np.int32.
               A[x,y] =
	       0 if image[x,y] is in the background
	       1 if image[x,y] is grass
	       2 if image[x,y] is a sideline
	       3 if image[x,y] is a player

2) camera: a list of files camera_i.npy. camera_i.npy is a numpy array of shape (4,3) and type np.float32. It represents the camera matrix that was used for image_i.

3) player_positions: a list of files player_contours_i.npy. player_contours_i.npy is a numpy array with shape (number_of_players, 2) and type.
                     It represents a list of positions of players that appear in image_i in absolute coordinates.
		     Note that these are not the positions of all of the players, but only the players that are standing far enough away from the others to be able
		     to identify them in a single frame.
		     Also, no tracking is being done here, these are simply lists of positions of players in each frame individually.

We also generate the following auxillary files which are used to produce the files above
1) field_mask: a list of numpy files field_mask_i.npy. field_mask_i.npy stores a (n,m) numpy array A of type np.float32.
               A[x,y] = 1 <=> image_i[x,y] is on the soccer field. This means that the pixel is either grass, a sideline, or a player

2) highlighted_shapes: a list of directories highlighted_shapes_i. Each directory stores a list of HighlightedShape object, which represent the highlighted
                       shapes (lines and ellipses) in image_i

3) frame_to_frame_homography: a list of files homography_i.npy. homography_i.npy is a numpy array with shape (3,3) and type np.float32.
                              it stores a planar homography from image_i to image_{i+1}

4) soccer_field_embedding:  a list of directories soccer_field_embedding_i/. Each directory contains 2 files:
                            homography.npy - the homography embedding the soccer field into the image
			    shape_matches.pkl - a list of keys of the SoccerFieldGeometry shape dictionary. the j-th
			                        key records which shape on the soccer field maps to the j-th highlighted shape
						in this frame.
                            This only stores the embeddings of frames for which we can compute the absolute homography with a
			    fairly high degree of accuracy.

5) optimized_homography: a list of files homography_i.npy. These correspond to homography matrices from the soccer field to
                         the image. They are optimized versions of the homography matrices in the previous step.
			 
6) full_absolute_homography: a list of files homography_i.npy. homography_i.npy is a 3x3 numpy array which represents the
                             homography matrix from the soccer field
                             to image_i. This only stores the absolute homography matrices of all of the images


### Preprocessing Pipeline ###
The current pipeline is done in this order:

1) generate_field_masks:
   INPUT:  images/
   OUTPUT: populate the field_mask directory
   VISUALIZATION: for each frame we store an image where the field is highlighted in blue.
   NOTES:
   
2) generate_highlighted_shapes: 
   INPUT:  images/, field_mask/
   OUTPUT: populate the highlighted_shapes directory.
   VISUALIZATION: on each image we draw the shapes in green, and the highlighted parts in blue.
   NOTES:  since the shapes are built directly from the contours, we only need the field_mask and not the full annotation directory

3) generate_annotations:
   INPUT:  images/, field_mask/, highlighted_shapes/
   OUTPUT: populate the annotation/ directory
   VISUALIZATION: on each image, black out the background, highlight the grass in green, the sidelines in white, and players in blue.
   NOTES:  We currently annotate the image by declaring the highlighted regions of the highlighted shapes to be sidelines, the remaining contours to be player,
           the rest of the field_mask to be grass, and everything else to be background

4) frame_to_frame_homography:
   INPUT:  images/, annotations/
   OUTPUT: populate the frame_to_frame_homography directory
   VISUALIZATION: On each frame, draw the contours in this frame in blue, and ontop of that draw the transformations of the contours of the previous frame in green, but with a thinner line.
   NOTES:  We need the annotations because we do not want to use player or background pixels to compute homography.
   
5) generate_soccer_field_embedding:
   INPUT: images/, highlighted_shapes/
   OUTPUT: populate the soccer_field_embedding directory
   VISUALIZATION: For each image for which we computed the embedding, draw the transformation of a grid on the absolute field,
   		  the transformation of the sidelines on the absolute field and label the embedded shapes.
   NOTES: * We try to match the shapes that are stored in highlighted_shapes/ to the sidelines on the field.
            We only store the absolute homography if we are confident of the quality of the match. I.e, that the matched
	    shapes agree within a specified threshold.
        
	  * HACK: At the moment we are not correcting for radial distortion which means that we can not trust the top and
	    bottom sidelines. To deal with this, we do not use these shapes with ellipse based homography calculation.

6) generate_optimized_homography:
   INPUT: images/, highlighted_shapes/, soccer_field_embedding/
   OUTPUT: populate the optimized_homography/ directory
   VISUALIZATION: For each image for which we computed the embedding, draw the transformation of a grid on the absolute field
   		  and the transformation of the sidelines on the absolute field
   NOTE: At this stage we also throw out homographies that do not align the corresponding shapes within a specified
         margin of error. Since these are supposed to be optimized, the threshold here is smaller than in the previous step.
	 
7) generate_absolute_homography:
   INPUT: frame_to_frame_homography/, optimized_homography/
   OUTPUT: absolute_homography/
   VISUALIZATION: For each image, draw the transformation of a grid on the absolute field, plus the transformation of the
                  sidelines on the absolute field
   NOTES: we use the frame to frame homographies to stitch together the partial absolute homographies

8) generate_camera_samples:
   INPUT: absolute_homography/, annotation/
   OUPUT: camera_sample/. camera_sample/ has base directories which are the same as absolute_homography/.
                    I.e, it contains folders of the
                    form camera_sample_i_j where i<j. Each directory contains files camera_k1.npy,...,camera_kM.npy
		    Note that there is not a camera_matrix for every frame, because some frames do not contain enough players
		    to compute a camera

9) interpolate_camera_samples:
   INPUT: camera/
   OUTPUT: camera/. camera/ has base directories which are the same as absolute_homography/. I.e, it contains folders of the
                    form camera_i_j where i<j. Each directory contains files
		    camera_0.npy,...,camera_{j-i}.npy
		    If camera_k.npy already exists, we don't touch it. Otherwise, we compute the missing camera matrices
		    by interpolating between the existing ones.
		    
   VISUALIZATION: We do the visualization after computing player locations
   
10) generate_players:
    INPUT: annotation/, camera/
    OUTPUT: players/. players/ has base directories which are the same as absolute_homography/. I.e, it contains folders of the
                    form players_i_j where i<j. Each directory contains directories:
		    players_0/,...,players_{j-i}/
		    The folder players_i contains two files: player_positions.npy and player_contours.pkl
		    
    VISUALIZATION: On each image, project ellipsoids that are based at the feet of the players in absolute coordinates.