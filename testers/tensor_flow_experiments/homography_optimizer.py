"""
Here we optimize a homography based on a collection of source and target shapes
"""
import numpy as np
import tensorflow as tf
from ...source.auxillary import planar_geometry
from ...source.auxillary.soccer_field_geometry import SoccerFieldGeometry
from ..auxillary import primitive_drawing
from ...source.auxillary.fit_points_to_shape.line import Line
from ...source.auxillary.fit_points_to_shape.ellipse import Ellipse
from ...source.auxillary import tf_operations
import cv2
import os

IMAGE_FILE = 'test_data/video5/image_0.png'
SHAPE_DIR = 'output/tester/shape_matching_graph'

SOURCE_SHAPE_COLOR = (0,255,0)
TARGET_SHAPE_COLOR = (255,0,0)
POINT_COLOR = (0,0,255)
POINT_RADIUS = 3

def multiply_matrices(M, matrices):
    '''
    M - a tensor with shape (n,m)
    matrices - a tensor with shape (k, m, p)

    return a tensor with shape (k, n, p)

    For each i, output[i,:,:] = M * matrices[i,:,:]
    '''
    #we first build a tensor with shape (m, k*p) which is obtained by stacking up the given
    #matrices side by side
    N = tf.concat(axis=1, values=tf.unstack(matrices))

    #We now multiply by M, and get the matrices M*matrices[i] lines up side by side
    #this is an array with shape (n, k*p)
    MN = tf.matmul(M,N)

    #finally, we have to repack the output into an array with shape (k,n,p)
    #this is an array with shape (3,) that stores the values (k,n,p)
    kpn = tf.concat(axis=0, values=[tf.shape(matrices)[:1], tf.shape(matrices)[2:], tf.shape(M)[:1]])

    #to repack MN, we first transpose it to get a (k*p, n) array
    output = tf.transpose(MN)
    #we then reshape it to have shape (k,p,n).
    output = tf.reshape(MN, kpn)
    #finally, we transpose each of the matrices output[i,:,:] so that output has shape (k, n, p)
    output = tf.transpose(output, perm = [0,2,1])

    return output

def apply_inverse_homography_to_lines(H, lines):
    '''
    H - a (3,3) tensor of type np.float32 representing a homography matrix
    lines - a tensor with shape (number of lines, 3) and type np.float32

    Each row of lines represents the parameters (a,b,c) of the line a*x + b*y + c = 0

    Return a tensor with shape (number of lines, 3) and type np.float32.

    If L represents a line in the lines array, the corresponding line of the output is L' such that
    L' = (H^T)L. I.e, L' is the image of L under the homography H^{-1}.
    
    Recall that if L the the original line and M is a homography, the p\inL <=> L^Tp = 0. So:
    L'^Tq=0 <=> L^T*M^{-1}*q <=> (M^{-T}L)^Tq = 0. So L' = ((M^{-1})^T)L
    '''
    
    new_lines =  tf.transpose(tf.matmul(tf.transpose(H), tf.transpose(lines)))

    return new_lines

def apply_inverse_homography_to_ellipse(H, E):
    '''
    H - a (3,3) tensor of type np.float32 representing a homography matrix
    E - a tensor with shape (3, 3) and type np.float32

    Return a tensor with shape (3, 3) and type np.float32.

    The output E' satisfies:
    E' = (H^T)*E*H
    I.e, E' is the image of E under the homography H^{-1}.
    '''

    return tf.matmul(tf.transpose(H), tf.matmul(E, H))

def ellipse_cost(H, source_ellipse, target_ellipse):
    '''
    H - a (3,3) tensor of type np.float32 representing a homography matrix
    source_ellipse, target_ellipse - a tensor with shape (3, 3) and type np.float32

    return a float32

    Let G=H^{-1}. If E is the source ellipse and E' is the target we should have:
    E = G(E') = (H^T)*E'*H

    To measure difference between G(E') and E, we compute:
    (tr(G(E') * (E^{-1})) - 3)^2
    '''    
    source_ellipse_pred = apply_inverse_homography_to_ellipse(H,target_ellipse)
    source_ellipse_pred_norm = tf.sqrt(tf.reduce_sum(tf.pow(source_ellipse_pred, 2)))
    source_ellipse_pred = source_ellipse_pred / source_ellipse_pred_norm
    
    trace_1 = tf.trace(tf.matmul(source_ellipse_pred, tf.matrix_inverse(source_ellipse)))
    trace_2 = tf.trace(tf.matmul(tf.matrix_inverse(source_ellipse_pred), source_ellipse))
    
    return tf.pow(trace_1 - 3, 2) + tf.pow(trace_2 - 3, 2)
    #return tf.reduce_sum(tf.pow(source_ellipse - source_ellipse_pred, 2))

def lines_cost(H, source_lines, target_lines):
    '''
    H - a (3,3) tensor of type np.float32 representing a homography matrix
    lines - a tensor with shape (number of lines, 3) and type np.float32

    Each row of lines represents the parameters (a,b,c) of the line a*x + b*y + c = 0

    Return a float32.

    Let G=H^{-1}. 
    For each line, we compute the sum of squares of the difference 
    between G(target_line) and source_line. We return the sum over all lines.
    '''
    source_lines_pred = apply_inverse_homography_to_lines(H, target_lines)

    #normalize the prediction
    source_lines_pred_norms = tf.sqrt(tf.reduce_sum(tf.pow(source_lines_pred, 2), 1))
    norms_shape = tf.concat(axis=0, values=[tf.shape(source_lines)[:1], [1]])
    source_lines_pred_norms = tf.reshape(source_lines_pred_norms, norms_shape)
    source_lines_pred = source_lines_pred / source_lines_pred_norms
    
    #return tf.reduce_sum(tf.pow(tf.abs(source_lines) - tf.abs(source_lines_pred), 2))
    return tf.reduce_sum(tf.pow(source_lines - source_lines_pred, 2))

def homography_cost(H_flat, source_lines, target_lines, source_ellipse, target_ellipse):
    '''
    H_flat - a tensor with shape (8,) and type np.float32 representing a homography matrix
    source_lines, target_lines - a tensor with shape (number of lines, 3) and type np.float32
    source_ellipse, target_ellipse - a tensor with shape (3, 3) and type np.float32

    return a float representing the difference between the source shapes and the preimages of the target
    shapes under H.
    '''
    #first reshape H_flat into a 3x3 matrix with 1 in position (2,2)
    one = np.ones((1,), np.float32)
    H = tf.reshape(tf.concat(axis=0, values=[H_flat, one]), [3,3])
    #last_row = np.array([0,0,1], np.float32)
    #H = tf.reshape(tf.concat(0, [H_flat, last_row]), [3,3])

    return lines_cost(H, source_lines, target_lines) + ellipse_cost(H, source_ellipse, target_ellipse)

def compute_normalization_matrix(image_shape):
    '''
    image_shape - a numpy array with length 3 and type np.int32
    
    Return a numpy array with shape (3,3) and type np.float32.
    N is a homgoraphy matrix mapping the original shapes to the normalized shapes.
    '''
    image_dimensions = np.array([image_shape[1], image_shape[0]], np.float32)
    
    #first get a matrix that translates the image center to the origin
    image_center = image_dimensions / 2.0
    T = planar_geometry.translation_matrix(-image_center)
    
    #then scale by the inverse of the image shape
    scale = 1.0 / image_dimensions
    S = planar_geometry.scaling_matrix(scale)
    
    N = np.dot(S,T)
    
    return N

def optimize_homography(initial_homography, source_image_shape, target_image_shape,
                        source_lines, target_lines, source_ellipse, target_ellipse):
    '''
    H - a (3,3) tensor of type np.float32 representing a homography matrix
    source_lines, target_lines - a tensor with shape (number of lines, 3) and type np.float32
    source_ellipse, target_ellipse - a tensor with shape (3, 3) and type np.float32

    Return a numpy array with shape (3,3) and type np.float32.
    
    We return the homography matrix that minimizes the distance between H*source_shapes and target_shapes
    '''
    sess = tf.Session()
    print 'before normalizing:'
    transformed_target = sess.run(apply_inverse_homography_to_ellipse(initial_homography, target_ellipse))
    transformed_target = transformed_target / np.linalg.norm(transformed_target)
    source_ellipse = source_ellipse / np.linalg.norm(source_ellipse)
    target_ellipse = target_ellipse / np.linalg.norm(target_ellipse)
    print 'transformed target ellipse (parametric rep):'
    print transformed_target
    print 'transformed target ellipse (geometric rep):'
    print str(Ellipse.from_parametric_representation(transformed_target))
    print 'source ellipse (parametric rep):'
    print source_ellipse
    print 'source ellipse (geo rep):'
    print str(Ellipse.from_parametric_representation(source_ellipse))
    print 'ellipse cost:'
    print sess.run(ellipse_cost(initial_homography, source_ellipse, target_ellipse))
    print 'transformed ellipse parameters:'

    sess.close()
    
    #compute the matrix that takes the source ellipse to the unit circle
    #E = Ellipse.from_parametric_representation(source_ellipse)
    #N_source = planar_geometry.compute_homography_to_unit_circle(E)
    N_source = compute_normalization_matrix(source_image_shape)
    
    #same for the target ellipse
    #E = Ellipse.from_parametric_representation(target_ellipse)
    #N_target = planar_geometry.compute_homography_to_unit_circle(E)
    N_target = compute_normalization_matrix(source_image_shape)
    
    #map the source lines and target lines by these matrices
    Ls = [Line.from_parametric_representation(sl) for sl in source_lines]
    source_lines = np.vstack([planar_geometry.apply_homography_to_line(N_source, l).get_parametric_representation()
                              for l in Ls])
    source_lines = np.float32(source_lines / np.linalg.norm(source_lines, axis=1).reshape((-1,1)))
    Ls = [Line.from_parametric_representation(tl) for tl in target_lines]
    target_lines = np.vstack([planar_geometry.apply_homography_to_line(N_target, l).get_parametric_representation()
                              for l in Ls])
    target_lines = np.float32(target_lines / np.linalg.norm(target_lines, axis=1).reshape((-1,1)))


    source_ellipse = Ellipse.from_parametric_representation(source_ellipse)
    source_ellipse = planar_geometry.apply_homography_to_ellipse(N_source, source_ellipse).get_parametric_representation()
    source_ellipse = np.float32(source_ellipse / np.linalg.norm(source_ellipse))
    target_ellipse = Ellipse.from_parametric_representation(target_ellipse)
    target_ellipse = planar_geometry.apply_homography_to_ellipse(N_target, target_ellipse).get_parametric_representation()
    target_ellipse = np.float32(target_ellipse / np.linalg.norm(target_ellipse))
    
    ##the new ellipses are unit circles
    # source_ellipse = Ellipse.from_geometric_parameters(np.array([0,0]), np.array([1,1]), 0).get_parametric_representation()
    # source_ellipse = np.float32(source_ellipse)
    # target_ellipse = Ellipse.from_geometric_parameters(np.array([0,0]), np.array([1,1]), 0).get_parametric_representation()
    # target_ellipse = np.float32(target_ellipse)
    
    #replace H with the homography between the normalized shapes
    #after finding the homography H, the actual homography will be: (N_target)^(-1) * H * N_source
    initial_homography = np.float32(np.dot(N_target, np.dot(initial_homography, np.linalg.inv(N_source))))
    initial_homography /= initial_homography[2,2]

    #finally, we possibly multiply each source line by -1, depending on whether this makes the source line closer to the
    #inverse transformation of the corresponding target line
    source_line_coefficients = []
    for source_line, target_line in zip(source_lines, target_lines):
        transformed_target = np.dot(initial_homography.transpose(), target_line)
        sum_squares_1 = np.sum((source_line - transformed_target)**2)
        sum_squares_neg_1 = np.sum((-source_line - transformed_target)**2)
        if sum_squares_1 < sum_squares_neg_1:
            source_line_coefficients.append(1)
        else:
            source_line_coefficients.append(-1)

    source_line_coefficients = np.array(source_line_coefficients, np.float32)
    source_lines *= source_line_coefficients.reshape((-1,1))
    
    print 'after normalizing the shapes we have:'
    print 'initial homography:'
    print initial_homography
    print 'source lines:'
    print source_lines
    print 'source ellipse:'
    print source_ellipse
    print 'target_lines:'
    print target_lines
    print 'target ellipse:'
    print target_ellipse

    #construct the variable that we will be optimizing for
    #H_flat = tf.Variable(initial_homography.flatten()[:-3], name='H_flat')
    H_flat = tf.Variable(initial_homography.flatten()[:-1], name='H_flat')

    #construct place holders for the source and target shapes
    SL = tf.placeholder(np.float32)
    TL = tf.placeholder(np.float32) 
    SE = tf.placeholder(np.float32, shape=[3,3])
    TE = tf.placeholder(np.float32, shape=[3,3])
   
    #construct the cost function using the source and target points
    cost = homography_cost(H_flat, SL, TL, SE, TE)

    one = np.ones((1,), np.float32)
    H = tf.reshape(tf.concat(axis=0, values=[H_flat, one]), [3,3])
    transformed_target_lines = tf_operations.normalized_vectors(apply_inverse_homography_to_lines(H, TL))
    transformed_target_ellipse = apply_inverse_homography_to_ellipse(H, TE)
    
    lc = lines_cost(H, SL, TL)
    ec = ellipse_cost(H, SE, TE)
    
    #minimize the homography cost
    learning_rate = 0.005
    optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost)

    n_epochs = 10000
    sess = tf.Session()
    
    #initialize the variables
    sess.run(tf.global_variables_initializer())

    prev_training_cost = 0.0
    print 'initial cost: ', sess.run(cost, feed_dict={SL: source_lines, TL: target_lines, SE: source_ellipse, TE: target_ellipse})
    for epoch_i in range(n_epochs):
        #run one step of the gradient descent for each of the source / target points
        #for p,q in zip(source_points, target_points):
        #    sess.run(optimizer, feed_dict={P: p.reshape((1,2)), Q: q.reshape((1,2))})
        sess.run(optimizer, feed_dict={SL: source_lines, TL: target_lines, SE: source_ellipse, TE: target_ellipse})

        training_cost = sess.run(cost, feed_dict={SL: source_lines, TL: target_lines, SE: source_ellipse, TE: target_ellipse})

        if (epoch_i % 100) == 0:
            #print the cost after this step
            print 'training cost at epoch %d: %f' % (epoch_i, training_cost)
            current_H_flat = sess.run(H_flat)
            print 'current H:'
            #last_row = np.array([0,0,1], np.float32)
            #print np.hstack([current_H_flat, last_row]).reshape((3,3))
            print np.append(current_H_flat, 1).reshape((3,3))
            transformed_lines = sess.run(transformed_target_lines, feed_dict={TL: target_lines})
            print 'the transformed lines: \n', transformed_lines
            print 'the source lines:      \n', source_lines
            transformed_ellipse = sess.run(transformed_target_ellipse, feed_dict={TE: target_ellipse})
            transformed_ellipse /= np.linalg.norm(transformed_ellipse)
            print 'the transformed ellipses: \n', transformed_ellipse
            print 'the source ellipse: \n', source_ellipse
            print 'the lines cost: ', sess.run(lc, feed_dict={SL: source_lines, TL: target_lines})
            print 'the ellipse cost: ', sess.run(ec, feed_dict={SE: source_ellipse, TE: target_ellipse})
            print '\n\n'
        # Allow the training to quit if we've reached a minimum
        if np.abs(prev_training_cost - training_cost) < 10**(-12):
            break
            
        prev_training_cost = training_cost

    #get the final value of H_flat
    best_H_flat = sess.run(H_flat)

    sess.close()
    
    best_H = np.append(best_H_flat, 1).reshape((3,3))
    #last_row = np.array([0,0,1], np.float32)
    #return np.hstack([best_H_flat, last_row]).reshape((3,3))

    #compute the unnormalized H
    best_H = np.dot(np.linalg.inv(N_target), np.dot(best_H, N_source))
    best_H /= best_H[2,2]
    
    return best_H

def test_apply_inverse_homography_to_shapes():
    sess = tf.InteractiveSession()
    
    H = np.ones((3,3), np.float32)
    H[0,2] = 3

    source_lines = np.array([[0, 1, -100],
                             [1, 0, -100]], np.float32)
    source_ellipse = np.eye(3, dtype=np.float32)
    source_ellipse[2,2] = -1

    print 'H:'
    print H
    
    print 'applying inverse homography to the lines:'
    print source_lines

    print 'we got:'
    print apply_inverse_homography_to_lines(H, source_lines).eval()

    print 'we should have gotten:'
    print np.dot(H.transpose(), source_lines.transpose()).transpose()

    print 'applying inverse homography to the ellipse:'
    print source_ellipse

    print 'we got:'
    print apply_inverse_homography_to_ellipse(H, source_ellipse).eval()

    print 'we should have gotten:'
    print np.dot(H.transpose(), np.dot(source_ellipse, H))

def load_absolute_soccer_field():
    '''
    Return a tuple (absolute shapes, absolute grid points)
    '''
    ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)
    SCALE = 6
    FIELD_WIDTH = absolute_image.shape[1]-1
    FIELD_LENGTH = absolute_image.shape[0]-1
    GRID_WIDTH = 20
    GRID_LENGTH = 14
    soccer_field = SoccerFieldGeometry(SCALE, FIELD_WIDTH, FIELD_LENGTH, GRID_WIDTH, GRID_LENGTH)

    highlighted_shape_dict = soccer_field.get_highlighted_shape_dict(format_keys=True)

    absolute_shapes = [hs.get_shape() for hs in highlighted_shape_dict.itervalues()]
    absolute_grid_points = soccer_field.get_grid_points()
    
    return absolute_image, absolute_shapes, absolute_grid_points

def padded_image(image):
    padding = 100
    padded_shape = (image.shape[0]+padding, image.shape[1]+padding, 3)
    padded_image = np.zeros(padded_shape, np.uint8)
    padded_image[:padded_shape[0]-padding, :padded_shape[1]-padding] = image
    return padded_image

def display_homography(image, H, source_shapes, target_shapes, source_grid_points):
    '''
    image - a numpy array with shape (n,m,3) and type np.uint8
    H - a numpy array with shape (3,3) and type np.float32
    source_shapes - a list of PlanarShape objects
    target_shapes - a list of PlanarShape objects
    source_grid_points - a list of points

    Use the homography to transfrom the source shapes to the target image and draw
    both the transformed shapes and the target shapes.
    '''
    image = padded_image(image.copy())
    for shape in target_shapes:
        primitive_drawing.draw_shape(image, shape, TARGET_SHAPE_COLOR)

    translated_shapes = (planar_geometry.apply_homography_to_shape(H,s) for s in source_shapes)
    for shape in translated_shapes:
        primitive_drawing.draw_shape(image, shape, SOURCE_SHAPE_COLOR)

    transformed_grid_points = cv2.perspectiveTransform(source_grid_points.reshape((-1,1,2)), H).reshape((-1,2))
    for point in transformed_grid_points:
        cv2.circle(image, tuple(np.int32(point).tolist()), POINT_RADIUS, POINT_COLOR, thickness=-1)
        
    cv2.imshow('target and transformed shapes', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    return

def generate_test_homography_and_shapes():
    '''
    return a tuple (homography, source_lines, target_lines, source_ellipse, target_ellipse)
    '''
    H = np.eye(3, dtype=np.float32)
    H[0,2] = 3

    source_lines = np.array([[0, 1, -100],
                             [1, 0, -100]], np.float32)
    center = np.array([200,200], np.float32)
    axes = np.array([100,70], np.float32)
    angle = 0.0
    source_ellipse = Ellipse.from_geometric_parameters(center, axes, angle)
    source_ellipse = np.float32(source_ellipse.get_parametric_representation())

    source_lines = source_lines / np.linalg.norm(source_lines, axis=1).reshape((-1,1))
    source_ellipse = source_ellipse / np.linalg.norm(source_ellipse)
    
    H_inv = np.linalg.inv(H)
    target_lines = np.dot(H_inv.transpose(), source_lines.transpose()).transpose()
    target_ellipse = np.dot(H_inv.transpose(), np.dot(source_ellipse, H_inv))

    target_lines = target_lines / np.linalg.norm(target_lines, axis=1).reshape((-1,1))
    target_ellipse = target_ellipse / np.linalg.norm(target_ellipse)
    
    initial_H = H.copy()
    initial_H[0,2] += 5.0
    initial_H[1,0] += 0.1
    initial_H[2,0] += 0.0001

    return initial_H, source_lines, target_lines, source_ellipse, target_ellipse

def load_homography_and_shapes():
    '''
    return a tuple (homography, source_lines, target_lines, source_ellipse, target_ellipse)
    '''

    homography_file = os.path.join(SHAPE_DIR, 'homography.npy')
    source_lines_file = os.path.join(SHAPE_DIR, 'source_lines.npy')
    target_lines_file = os.path.join(SHAPE_DIR, 'target_lines.npy')
    source_ellipse_file = os.path.join(SHAPE_DIR, 'source_ellipse.npy')
    target_ellipse_file = os.path.join(SHAPE_DIR, 'target_ellipse.npy')

    H = np.float32(np.load(homography_file))
    source_lines = np.load(source_lines_file)
    target_lines = np.load(target_lines_file)
    source_ellipse = np.load(source_ellipse_file)
    target_ellipse = np.load(target_ellipse_file)

    source_lines = source_lines / np.linalg.norm(source_lines, axis=1).reshape((-1,1))
    target_lines = target_lines / np.linalg.norm(target_lines, axis=1).reshape((-1,1))
    return H, source_lines, target_lines, source_ellipse, target_ellipse

def test_optimize_homography():
    image = cv2.imread(IMAGE_FILE, 1)
    source_image, all_source_shapes, source_grid_points = load_absolute_soccer_field()
    
    initial_H, source_lines, target_lines, source_ellipse, target_ellipse = load_homography_and_shapes()
    
    print 'initial H:'
    print initial_H

    print 'source lines:'
    print source_lines
    print 'source ellipse:'
    print source_ellipse
    print 'target_lines:'
    print target_lines
    print 'target ellipse:'
    print target_ellipse

    #turn the shapes into PlanarShape objects so that we can draw them
    target_lines_ob = [Line.from_parametric_representation(tl) for tl in target_lines]
    target_ellipse_ob = Ellipse.from_parametric_representation(target_ellipse)
    target_shapes_ob = target_lines_ob + [target_ellipse_ob]
    
    display_homography(image, initial_H, all_source_shapes, target_shapes_ob, source_grid_points)

    source_image_shape = np.array(source_image.shape, np.int32)
    target_image_shape = np.array(image.shape, np.int32)
    optimal_H = optimize_homography(initial_H, source_image_shape, target_image_shape,
                                    source_lines, target_lines, source_ellipse, target_ellipse)

    print 'the optimal H is:'
    print optimal_H

    print 'actual target shapes:'
    print target_lines
    print target_ellipse
    
    print 'initial transformed shapes:'
    H_inv = np.linalg.inv(initial_H)
    initial_lines = np.dot(H_inv.transpose(), source_lines.transpose()).transpose()
    initial_ellipse =  np.dot(H_inv.transpose(), np.dot(source_ellipse, H_inv))
    initial_lines = initial_lines / np.linalg.norm(initial_lines, axis=1).reshape((-1,1))
    print initial_lines
    print initial_ellipse
    
    print 'computed transformed shapes:'
    H_inv = np.linalg.inv(optimal_H)
    optimal_lines = np.dot(H_inv.transpose(), source_lines.transpose()).transpose()
    optimal_ellipse =  np.dot(H_inv.transpose(), np.dot(source_ellipse, H_inv))
    optimal_lines = optimal_lines / np.linalg.norm(optimal_lines, axis=1).reshape((-1,1))
    print optimal_lines
    print optimal_ellipse

    display_homography(image, optimal_H, all_source_shapes, target_shapes_ob, source_grid_points)
    
if __name__ == '__main__':
    #test_apply_inverse_homography_to_shapes()
    test_optimize_homography()
    
