import numpy as np
import cv2

from ....source.data_structures.frame_sequence.simple_frame_sequence_builder import SimpleFrameSequenceBuilder
#######
# This is a tester for the file code/source/data_structures/frame_sequence/simple_frame_sequence_builder
#
# We load a list of soccer images and create a frame sequence
######


frame_indices = range(50,270)
TEST_IMAGE_NAMES = ['test_data/video3/out%d.png' % (frame_index) for frame_index in frame_indices]
BASE_NAME = 'frame_sequences_%d_%d' % (frame_indices[0], frame_indices[-1])

OUTPUT_DIR = 'output/tester/frame_sequences/frame_sequence_test_8'

if __name__ == '__main__':

    frame_sequence_builder = SimpleFrameSequenceBuilder()

    print 'building frame sequences for images:'
    print TEST_IMAGE_NAMES

    frame_sequences = frame_sequence_builder.build_frame_sequences(TEST_IMAGE_NAMES, BASE_NAME)

    print 'we got %d frame sequences' % len(frame_sequences)
    print 'saving the frame sequences...'

    for fs in frame_sequences:
        print 'saving frame sequence %s' % fs.get_name()
        fs.save_frame_sequence(OUTPUT_DIR)
