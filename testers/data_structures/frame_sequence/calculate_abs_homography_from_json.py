import json
import numpy as np
import matplotlib.pyplot as plt
import cv2

class CalculateAbsHomographyFromJSON(object):
    '''
    This class takes care of translating image coordinates to absolute coordinates
    using json data.

    First of all, it loads a json file containing markings for vertical and horizontal lines.
    It uses these markings to obtain a homography matrix to absolute coordinates which it returns.
    
    The absolute coordinates refer to coordinates in the image soccer-field.png.
    In terms of the squares on the field in the video, there are 20 squares from
    left to right and 14 squares from top to bottom.
    
    The json file contains pairs H1X,H1Y, H2X,H2Y, ... 
    Each pair marks 2 points on the corresponding line. The first horizontal line
    is the top sideline and the first vertical line is the rightmost sideline.
    (It should be the left most but the right one is easier to see in the video...)
    
    After loading the lines, we intersect them to get points which we use to calculate the homography matrix.
    '''

    NUM_HORIZONTAL_LINES = 20
    NUM_VERTICAL_LINES = 20

    def __init__(self, json_file_name, relative_img, absolute_soccer_img):
        f = open(json_file_name)
        self._json_data = json.loads(f.read())

        #This is the image that we sill be mapping to
        self._absolute_soccer_img = absolute_soccer_img
        self._relative_img = relative_img
        
        self._absolute_height = absolute_soccer_img.shape[0]
        self._absolute_width = absolute_soccer_img.shape[1]

        #this is the distance between to lines in the absolute image
        self._absolute_width_step_size = self._absolute_width / 20.0
        self._absolute_height_step_size = self._absolute_height / 14.0
        
        #print 'absolute shape: ', absolute_soccer_img.shape
        #print 'absolute width: %d' % self._absolute_width
        #print 'absolute step size: %d' % self._absolute_step_size
        #this is a list of pairs of points.
        #each pair corresponds to a horizontal line
        self._horizontal_lines = []

        #the same for vertical lines
        self._vertical_lines = []

        #the points on the frame coordinates
        self._relative_points = None

        #the points in absolute coordinates
        self._absolute_points = None
        
        self._homography_matrix = None
        self._mask = None
        
        #since the json data may have been scaled, calculate the amount
        #we need to rescale it
        json_height = self._json_data['height']
        json_width = self._json_data['width']

        self._json_to_rel_img_height = float(self._relative_img.shape[0]) / json_height
        self._json_to_rel_img_width = float(self._relative_img.shape[1]) / json_width
        
    def get_abs_grass_coords(self, h_index, v_index):
        '''
        h_index starts from 0 on the top sideline.
        v_index starts from 0 on the right sideline
        '''
        
        x = self._absolute_width - self._absolute_width_step_size * (v_index)
        y = self._absolute_height_step_size * (h_index)

        #print '(h_index = %d, v_index = %d)' % (h_index, v_index)
        #print '(x = %d, y = %d)' % (x,y)
        return np.array([x,y])

    def get_lines_intersection(self, P0, P1, Q0, Q1):
        '''
        P1 and P2 are 2D numpy arrays. Same for Q1, Q2.
        '''
        u = P1 - P0
        v = Q1 - Q0
        w = P0 - Q0

        s = (v[1]*w[0] - v[0]*w[1]) / (v[0]*u[1] - v[1]*u[0])
        
        return P0 + s*u

    def np_array_from_dict(self, d):
        '''
        The input is a dictionary with an 'x' key and a 'y' key. This function
        turns this into a numpy array.
        '''
        if d is False:
            return np.array([np.nan, np.nan])

        return np.array([float(d['x']), float(d['y'])])   

    def load_horizontal_and_vertical_lines(self):
        '''
        Read the horizontal and vertical lines from the json file
        '''
        for i in range(1,self.NUM_HORIZONTAL_LINES):
            #get the keys for the points on the i-th horizontal line
            s0 = 'h%dx' % i
            s1 = 'h%dy' % i

            #append the pair of points to the list of horizontal lines
            point0 = self.np_array_from_dict(self._json_data[s0])
            point1 = self.np_array_from_dict(self._json_data[s1])

            #scale the points to the relative image coords
            point0 *= (self._json_to_rel_img_height, self._json_to_rel_img_width)
            point1 *= (self._json_to_rel_img_height, self._json_to_rel_img_width)

            self._horizontal_lines.append((point0, point1))

        for i in range(1, self.NUM_VERTICAL_LINES):
            #get the keys for the points on the i-th vertical line
            s0 = 'v%dx' % i
            s1 = 'v%dy' % i

            #append the pair of points to the list of vertical lines
            point0 = self.np_array_from_dict(self._json_data[s0])
            point1 = self.np_array_from_dict(self._json_data[s1])

            #scale the points to the relative image coords
            point0 *= (self._json_to_rel_img_height, self._json_to_rel_img_width)
            point1 *= (self._json_to_rel_img_height, self._json_to_rel_img_width)

            self._vertical_lines.append((point0, point1))

        return
    
    def get_absolute_and_relative_points(self):
        '''
        Calculate the intersection points of the vertical and horizontal lines.
        We then return a list of the intersection points and a list of the corresponding absolute points.

        This should only be called after loading the lines from the json file.
        '''

        self._relative_points = []
        self._absolute_points = []

        for h_index, h in enumerate(self._horizontal_lines):
            for v_index,v in enumerate(self._vertical_lines):

                #if one of the points is nan, continue
                if np.isnan((h[0]+h[1]+v[0]+v[1]).sum()):
                    continue
            
                self._relative_points.append(self.get_lines_intersection(h[0], h[1], v[0], v[1]))
                self._absolute_points.append(self.get_abs_grass_coords(h_index, v_index))

        self._relative_points = np.array(self._relative_points)
        self._absolute_points = np.array(self._absolute_points)
        return

    def calculate_homography(self):
        #First load the points from the json file
        self.load_horizontal_and_vertical_lines()

        #now get the absolute and relative points
        self.get_absolute_and_relative_points()
        
        M, mask = cv2.findHomography(self._relative_points, self._absolute_points, cv2.RANSAC, 5.0)

        #print 'the mask is: '
        #print mask

        self._mask = mask
        self._homography_matrix = M
        
        return

    def get_homography_matrix(self):
        return self._homography_matrix

    def get_masked_absolute_points(self):
        return [self._absolute_points[i] for i in range(len(self._absolute_points)) if self._mask[i] == 1]

    def get_masked_relative_points(self):
        return [self._relative_points[i] for i in range(len(self._relative_points)) if self._mask[i] == 1]

def visualize_homography_from_json(hom_calc, relative_image, absolute_image):

    #draw the vertical and horizontal lines on the relative image
    print 'drawing lines'
    relative_image_copy = relative_image.copy()
    for points in hom_calc._vertical_lines:
        if np.isnan(points[0].sum()):
            continue
        
        cv2.line(relative_image_copy, (int(points[0][0]),int(points[0][1])), (int(points[1][0]),int(points[1][1])), (255,0,0), 1)

    for points in hom_calc._horizontal_lines:
        if np.isnan(points[0].sum()):
            continue
        
        cv2.line(relative_image_copy, (int(points[0][0]),int(points[0][1])), (int(points[1][0]),int(points[1][1])), (0,0,255), 1)

    cv2.imshow('draw lines', relative_image_copy)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    print 'drawing the source and destination points'
    draw_source_and_destination_points(relative_image, np.array(hom_calc.get_masked_relative_points()).reshape(-1,1,2),
                                       absolute_image, np.array(hom_calc.get_masked_absolute_points()).reshape(-1,1,2))

    print 'drawing the contours on the absolute image'
    #first get the grass using the field extractor
    field_extractor = StupidFieldExtractor()
    grass_mask = field_extractor.get_field_mask(relative_image)
    
    #get all contours in the image
    contour_generator = EdgesContourGenerator()
    contours = contour_generator.generate_contours(relative_image, grass_mask)

    #transform the contours to absolute coordinates
    homography_to_abs = hom_calc.get_homography_matrix()
    transformed_cv_contours = transform_cv_contours([contour.get_cv_contour() for contour in contours], homography_to_abs)

    #and draw them
    absolute_image_copy = absolute_image.copy()
    cv2.drawContours(absolute_image_copy, transformed_cv_contours, -1, (255,0,0), 2)
    cv2.imshow('absolute contours', absolute_image_copy)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

if __name__ == '__main__':
    from ....source.image_processing.contour_generator.edges_contour_generator import EdgesContourGenerator
    from ....source.image_processing.field_extractor.stupid_field_extractor import StupidFieldExtractor
    from ...image_processing.homography_calculator.homography_visualization import draw_source_and_destination_points
    from .frame_sequence_visualization import transform_cv_contours
    
    json_file_name = 'test_data/json_files/img_240'
    frame_file_name = 'test_data/video3/out240.png'
    absolute_file_name = 'test_data/soccer-field.png'

    absolute_image = cv2.imread(absolute_file_name, 1)
    relative_image = cv2.imread(frame_file_name, 1)

    print 'calculating homography to abs'
    hom_calc = CalculateAbsHomographyFromJSON(json_file_name, relative_image, absolute_image)
    hom_calc.calculate_homography()

    visualize_homography_from_json(hom_calc, relative_image, absolute_image)
