import numpy as np
import cv2
import os

from ....source.data_structures.frame_sequence.frame_sequence import FrameSequence
from ....source.image_processing.image_annotator.soccer_field_annotator import SoccerFieldAnnotator
from ....source.image_processing.contour_generator.edges_contour_generator import EdgesContourGenerator

###################################################
# Here are methods for visualizing a frame sequence
###################################################

SIDELINES_COLOR = np.array([255,255,255])
PLAYERS_COLOR = np.array([255,0,0])

CONTOUR_COLOR = (0,0,255)

ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'

def color_by_annotation(image, annotation):
    players_mask = SoccerFieldAnnotator.get_players_mask(annotation)
    sidelines_mask = SoccerFieldAnnotator.get_sidelines_mask(annotation)

    image[players_mask == True] = PLAYERS_COLOR
    image[sidelines_mask == True] = SIDELINES_COLOR

    return

def transform_cv_contours(cv_contours, homography_matrix):
    transformed_cv_contours = []
       
    for cv_contour in cv_contours:
        cv_contour = np.float32(cv_contour)
        new_cv_contour = cv2.perspectiveTransform(cv_contour, homography_matrix)
        transformed_cv_contours.append(np.int32(new_cv_contour))
        
    return transformed_cv_contours

def _save_frame_sequence_images(frame_sequence, parent_dir, display_annotation=False, display_player_contours=False):
    '''
    Save the images in the frame_sequences' frames in the directory:
    parent_directory/frame_sequence_name
    '''

    output_dir = os.path.join(parent_dir, frame_sequence.get_name())
    
    for frame in frame_sequence.get_frames():
        image_path = os.path.join(output_dir, ('img_%3d.png' % frame.get_index()).replace(' ','0'))
        output_image = frame.get_image().copy()

        #if we are supposed to display annotation, color the sidelines white and the players blue
        if display_annotation:
            annotation = frame.get_annotation()
            color_by_annotation(output_image, annotation)

        #if we are supposed to display the player contours, draw the contours in red
        if display_player_contours:
            player_contours = frame.get_player_contours()
            cv2.drawContours(output_image, [player_contour.get_cv_contour() for player_contour in player_contours], -1, CONTOUR_COLOR, 2)
            
        print 'writing image to: ', image_path
        
        cv2.imwrite(image_path, output_image)


    return

def _save_player_contours_on_last_frame(frame_sequence, parent_dir):
    '''
    For each frame in the frame sequence:
    *    Take the player contours from the frame and use the homography to transfrom them to the last frame in the sequence.
    *    Draw the transformed contours on the last image, and save the result in parent_directory/frame_sequence_name
    '''

    output_dir = os.path.join(parent_dir, frame_sequence.get_name())
    last_image = frame_sequence.get_frames()[-1].get_image()

    #compute the homographies to the last frame
    frame_sequence.calculate_homography_to_last()
    
    for frame in frame_sequence.get_frames():
        image_path = os.path.join(output_dir, ('img_%3d.png' % frame.get_index()).replace(' ','0'))
        output_image = last_image.copy()
        player_contours = frame.get_player_contours()

        #transform the contours to the last frame
        homography_to_last = frame.get_homography_to_last()
        transformed_cv_contours = transform_cv_contours([player_contour.get_cv_contour() for player_contour in player_contours], homography_to_last)

        #and draw them
        cv2.drawContours(output_image, transformed_cv_contours, -1, CONTOUR_COLOR, 2)
        print 'writing image to: ', image_path
        
        cv2.imwrite(image_path, output_image)

    return


def _save_all_contours_on_last_frame(frame_sequence, parent_dir):
    '''
    For each frame in the frame sequence:
    *    compute the contours in the frame and use the homography to transfrom them to the last frame in the sequence.
    *    Draw the transformed contours on the last image, and save the result in parent_directory/frame_sequence_name
    '''

    output_dir = os.path.join(parent_dir, frame_sequence.get_name())
    last_image = frame_sequence.get_frames()[-1].get_image()

    #compute the homographies to the last frame
    frame_sequence.calculate_homography_to_last()
    
    for frame in frame_sequence.get_frames():
        output_image_path = os.path.join(output_dir, ('img_%3d.png' % frame.get_index()).replace(' ','0'))
        output_image = last_image.copy()

        #get all contours in the image
        image = frame.get_image()
        annotation = frame.get_annotation()
        contour_generator = EdgesContourGenerator()
        grass_mask = SoccerFieldAnnotator.get_soccer_field_mask(annotation)
        contours = contour_generator.generate_contours(image, grass_mask)

        #transform the contours to the last frame
        homography_to_last = frame.get_homography_to_last()
        transformed_cv_contours = transform_cv_contours([contour.get_cv_contour() for contour in contours], homography_to_last)

        #and draw them
        cv2.drawContours(output_image, transformed_cv_contours, -1, CONTOUR_COLOR, 2)
        print 'writing image to: ', output_image_path
        
        cv2.imwrite(output_image_path, output_image)

def _save_all_contours_on_absolute_image(frame_sequence, parent_dir):
    '''
    For each frame in the frame sequence:
    *    compute the contours in the frame and use the homography to transfrom them to absolute coordinates.
    *    Draw the transformed contours on the absolute image, and save the result in parent_directory/frame_sequence_name
    '''

    output_dir = os.path.join(parent_dir, frame_sequence.get_name())
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)

    for frame in frame_sequence.get_frames():
        output_image_path = os.path.join(output_dir, ('img_%3d.png' % frame.get_index()).replace(' ','0'))
        output_image = absolute_image.copy()

        #get all contours in the image
        image = frame.get_image()
        annotation = frame.get_annotation()
        contour_generator = EdgesContourGenerator()
        grass_mask = SoccerFieldAnnotator.get_soccer_field_mask(annotation)
        contours = contour_generator.generate_contours(image, grass_mask)

        #transform the contours to absolute coordinates
        homography_to_abs = frame.get_homography_to_abs()
        transformed_cv_contours = transform_cv_contours([contour.get_cv_contour() for contour in contours], homography_to_abs)

        #and draw them
        cv2.drawContours(output_image, transformed_cv_contours, -1, CONTOUR_COLOR, 2)
        print 'writing image to: ', output_image_path
        
        cv2.imwrite(output_image_path, output_image)


def save_frame_sequence_images(frame_sequence_directory):
    OUTPUT_DIR = 'output/tester/frame_sequence_images'

    #load a frame sequence and save it as a sequence of images
    frame_sequence = FrameSequence.load_frame_sequence(frame_sequence_directory)

    #print 'saving the images from the frame sequence'
    _save_frame_sequence_images(frame_sequence, OUTPUT_DIR, display_annotation=True, display_player_contours=True)

def save_player_contours_on_last_frame(frame_sequence_directory):
    OUTPUT_DIR = 'output/tester/frame_sequence_contours_on_last'

    #load a frame sequence
    frame_sequence = FrameSequence.load_frame_sequence(frame_sequence_directory)

    print 'drawing the player contours on the last image'
    _save_player_contours_on_last_frame(frame_sequence, OUTPUT_DIR)

def save_all_contours_on_last_frame(frame_sequence_directory):
    OUTPUT_DIR = 'output/tester/frame_sequence_all_contours_on_last'

    #load a frame sequence
    frame_sequence = FrameSequence.load_frame_sequence(frame_sequence_directory)

    print 'drawing the player contours on the last image'
    _save_all_contours_on_last_frame(frame_sequence, OUTPUT_DIR)

def save_all_contours_on_absolute_image(frame_sequence_directory):
    OUTPUT_DIR = 'output/tester/frame_sequence_all_contours_on_abs'

    #load a frame sequence
    frame_sequence = FrameSequence.load_frame_sequence(frame_sequence_directory, load_homography_to_abs=True)

    print 'drawing the player contours on the absolute image'
    _save_all_contours_on_absolute_image(frame_sequence, OUTPUT_DIR)

if __name__ == '__main__':
    #FRAME_SEQUENCE_DIR = 'output/tester/frame_sequences/frame_sequence_test_3/frame_sequences_50_79-0_29'
    #FRAME_SEQUENCE_DIR = 'output/tester/frame_sequences/frame_sequence_test_5/frame_sequences_50_109-0_59'
    #FRAME_SEQUENCE_DIR = 'output/tester/frame_sequences/frame_sequence_test_4/frame_sequences_148_150-0_2'
    #FRAME_SEQUENCE_DIR = 'output/tester/frame_sequences/frame_sequence_test_6/frame_sequences_50_148-0_98'
    FRAME_SEQUENCE_DIR = 'output/tester/frame_sequences/frame_sequence_test_8/frame_sequences_50_269-0_219'
    
    save_frame_sequence_images(FRAME_SEQUENCE_DIR)
    #save_player_contours_on_last_frame(FRAME_SEQUENCE_DIR)
    #save_all_contours_on_last_frame(FRAME_SEQUENCE_DIR)
    #save_all_contours_on_absolute_image(FRAME_SEQUENCE_DIR)
