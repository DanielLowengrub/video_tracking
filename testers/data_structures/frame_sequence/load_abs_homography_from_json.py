import numpy as np
import cv2
from ....source.data_structures.frame_sequence.frame_sequence import FrameSequence
from .calculate_abs_homography_from_json import CalculateAbsHomographyFromJSON
from ....source.auxillary.matrix_interpolation import interpolate_between_invertible_matrices

#######################
# This is a script that we use to load a homography to absolute coordinates matrix from a json file.
#
# It can be used to set absolute homography matrices for a frame sequence.
#######################

ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'

PRECISION = 5 #the precision for the matrix interpolation

def load_homography_to_abs(frame, lines_json):
    '''
    frame - a Frame object
    lines_json - a json file with line markings

    Get a frame and a json file with line markings.
    Return a homography matrix to absolute coordinates
    '''
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)
    relative_image = frame.get_image()

    #calculate the homography from the last frame to the absolute frame
    hom_calc = CalculateAbsHomographyFromJSON(lines_json, relative_image, absolute_image)
    hom_calc.calculate_homography()
    return hom_calc.get_homography_matrix()

def _compute_homography_to_abs(frames, homography_from_first, homography_from_last, precision):
    '''
    frame_sequence - a FrameSequence object
    homography_from_first - the homography matrix from the first frame in the sequence to absolute coordinates
    homography_from_last - similar
    precision - an integer used for interpolating between matrix values. The higher the number the more precise the interpolation. 6 is a high number.

    We set the homography to absolute coordinates for each of the frames in the frame sequence. We do this by computing the homography for each
    frame in two ways, once using the homography_from_first and once using the homography from last. We then extrapolate between the two.
    '''

    #we first extrapolate from homography_from_first
    extrapolations_from_first = [homography_from_first]

    for i, frame in enumerate(frames[:-1]):
        #this is the homography matrix from frame i+1 to frame i
        homography_from_next = np.linalg.inv(frame.get_homography_to_next())

        #the homography from frame i+1 to absolute coordinates
        new_extrapolation = np.dot(extrapolations_from_first[i], homography_from_next) 
        extrapolations_from_first.append(new_extrapolation)


    #we now extrapolate from homography_from_last
    extrapolations_from_last = [homography_from_last]

    for i, frame in enumerate(frames[-2::-1]):
        homography_to_next = frame.get_homography_to_next()

        #the homography from the frame to absolute coordinates
        #note that extrapolations_from_last[i] holds the homography_to_abs matrix of the frame that comes after the current frame
        new_extrapolation = np.dot(extrapolations_from_last[i], homography_to_next) 
        extrapolations_from_last.append(new_extrapolation)

    extrapolations_from_last.reverse()

    #we now interpolate between the two approximations
    num_frames = len(frames)

    for i, frame in enumerate(frames):
        #homography_to_abs = interpolate_between_invertible_matrices(extrapolations_from_first[i], extrapolations_from_last[i], float(i) / num_frames, precision)
        alpha = 1 - (float(i) / num_frames)
        homography_to_abs = (alpha * extrapolations_from_first[i]) + ((1-alpha) * extrapolations_from_last[i])
        #homography_to_abs = extrapolations_from_first[i]
        frame.set_homography_to_abs(homography_to_abs)

    return

def compute_homography_to_abs(frame_sequence, json_files):
    '''
    frame_sequence - a FrameSequence object
    json_files - a list of tuples (frame index, json file)

    We compute the homography to abs matrices of all of the frames in the frame sequence.
    '''
    homography_matrices = [(frame_index, load_homography_to_abs(frame_sequence.get_frames()[frame_index], json_file)) for frame_index, json_file in json_files]
    #homography_from_first = load_homography_to_abs(frame_sequence.get_frames()[0], first_json)
    #homography_from_last = load_homography_to_abs(frame_sequence.get_frames()[-1], last_json)
    for i in range(len(homography_matrices) - 1):
        first_frame_index, homography_from_first = homography_matrices[i]
        last_frame_index, homography_from_last = homography_matrices[i+1]
        
        frames = frame_sequence.get_frames()[first_frame_index: last_frame_index+1]
        _compute_homography_to_abs(frames, homography_from_first, homography_from_last, PRECISION)


if __name__ == '__main__':
    #PARENT_DIR = 'output/tester/frame_sequences/frame_sequence_test_3'
    #FRAME_SEQUENCE_DIR = PARENT_DIR + '/frame_sequences_50_79-0_29'
    #FIRST_JSON = 'test_data/json_files/img_50'
    #LAST_JSON = 'test_data/json_files/img_79'

    #PARENT_DIR = 'output/tester/frame_sequences/frame_sequence_test_4'
    #FRAME_SEQUENCE_DIR = PARENT_DIR + '/frame_sequences_148_150-0_2'
    #json_148 = 'test_data/json_files/img_148'
    #json_150 = 'test_data/json_files/img_150'
    #JSON_FILES = [(0, json_148), (2,json_150)]

    #PARENT_DIR = 'output/tester/frame_sequences/frame_sequence_test_5'
    #FRAME_SEQUENCE_DIR = PARENT_DIR + '/frame_sequences_50_109-0_59'
    #json_50 = 'test_data/json_files/img_50'
    #json_79 = 'test_data/json_files/img_79'
    #json_109 = 'test_data/json_files/img_109'
    #JSON_FILES = [(0, json_50), (79-50,json_79) ,(109-50, json_109)]

    #PARENT_DIR = 'output/tester/frame_sequences/frame_sequence_test_6'
    #FRAME_SEQUENCE_DIR = PARENT_DIR + '/frame_sequences_50_148-0_98'
    #json_50 = 'test_data/json_files/img_50'
    #json_79 = 'test_data/json_files/img_79'
    #json_109 = 'test_data/json_files/img_109'
    #json_148 = 'test_data/json_files/img_148'
    #JSON_FILES = [(0, json_50), (79-50,json_79) ,(109-50, json_109), (148-50, json_148)]

    PARENT_DIR = 'output/tester/frame_sequences/frame_sequence_test_8'
    FRAME_SEQUENCE_DIR = PARENT_DIR + '/frame_sequences_50_269-0_219'
    json_50 = 'test_data/json_files/img_50'
    json_79 = 'test_data/json_files/img_79'
    json_109 = 'test_data/json_files/img_109'
    json_148 = 'test_data/json_files/img_148'
    json_199 = 'test_data/json_files/img_199'
    json_240 = 'test_data/json_files/img_240'
    json_269 = 'test_data/json_files/img_269'
    JSON_FILES = [(0, json_50), (79-50,json_79) ,(109-50, json_109), (148-50, json_148), (199-50, json_199), (240-50, json_240), (269-50, json_269)]
    
    print 'loading the frame sequence: ', FRAME_SEQUENCE_DIR
    frame_sequence = FrameSequence.load_frame_sequence(FRAME_SEQUENCE_DIR)

    print 'computing the homography to absolute coordinates...'
    compute_homography_to_abs(frame_sequence, JSON_FILES)

    print 'saving the homography matrices...'
    frame_sequence.save_homography_to_abs(PARENT_DIR)
