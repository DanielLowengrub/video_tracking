import numpy as np
import cv2
import os
from ....source.data_structures.frame_sequence.frame_sequence import FrameSequence
from ....source.auxillary.camera_matrix import CameraMatrix

#PARENT_DIR = 'output/tester/frame_sequences/frame_sequence_test_3'
#FRAME_SEQUENCE_NAME = 'frame_sequences_50_79-0_29'
#FRAME_SEQUENCE_DIR = os.path.join(PARENT_DIR, FRAME_SEQUENCE_NAME)
#PLAYER_DATA_DIR = os.path.join('output/tester/player_tracker/player_positions', FILE_SEQUENCE_NAME)

# PARENT_DIR = 'output/tester/frame_sequences/frame_sequence_test_7'
# FRAME_SEQUENCE_NAME = 'frame_sequences_50_199-0_149'
# FRAME_SEQUENCE_DIR = os.path.join(PARENT_DIR, FRAME_SEQUENCE_NAME)
# PLAYER_DATA_NAME = 'frame_sequence_50_199-0_149'
# PLAYER_DATA_DIR = os.path.join('output/tester/player_tracker/player_positions', PLAYER_DATA_NAME)

PARENT_DIR = 'output/tester/frame_sequences/frame_sequence_test_8'
FRAME_SEQUENCE_NAME = 'frame_sequences_50_269-0_219'
FRAME_SEQUENCE_DIR = os.path.join(PARENT_DIR, FRAME_SEQUENCE_NAME)
PLAYER_DATA_NAME = 'frame_sequence_50_269-0_219'
PLAYER_DATA_DIR = os.path.join('output/tester/player_tracker/player_positions', PLAYER_DATA_NAME)

PLAYER_POSITIONS_FILE = os.path.join(PLAYER_DATA_DIR, 'player_positions.npy')
HEAD_IMAGE_POSITIONS_FILE = os.path.join(PLAYER_DATA_DIR, 'head_image_positions.npy')

PLAYER_HEIGHT = 13 #the players height in the coordinate system of the field relative to the size of the field. (i.e, in pixel units with respect to the image of the field)
PLAYER_WIDTH = 4

MIN_HEADS = 2
#################
# We load a frame sequence and a list of player positions.
# We use these to compute the camera matrices at each frame, and then save them to the frame sequence
################


def load_player_absolute_positions():
    '''
    Return a list containing the positions in absolute coordinates at the given frame of the player with ids PLAYER_IDS
    '''
    absolute_positions = np.load(PLAYER_POSITIONS_FILE)
    
    return absolute_positions

def load_head_image_positions():
    '''
    Return a list containing the head positions in image coordinates at the given frame of the player with ids PLAYER_IDS
    '''
    head_image_positions = np.load(HEAD_IMAGE_POSITIONS_FILE)
    return head_image_positions

def get_head_world_positions(player_absolute_positions):
    '''
    player_absolute_positions - a numpy array of shape (num players, num frames, 2)

    We return a numpy array of shape (num players, num frames, 3) by changing each 2d position (x,y) to the 3d position
    (x,y,PLAYER_HEIGHT)
    '''
    heights = np.ones(player_absolute_positions.shape[:2], np.float32) * PLAYER_HEIGHT
    heights = heights.reshape((heights.shape[0], heights.shape[1], 1))
    return np.concatenate([player_absolute_positions, heights], axis=2)

def compute_camera_matrices(frame_sequence, head_world_positions, head_image_positions):
    '''
    For each frame in the frame sequence, use the head positions to compute the camera matrix and store it in the frame.
    '''

    for frame_index, frame in enumerate(frame_sequence.get_frames()):
        homography_to_abs = frame.get_homography_to_abs()
        homography_from_abs = np.linalg.inv(homography_to_abs)
        
        #pick out the points in this frame
        head_world_positions_in_frame = head_world_positions[:,frame_index,:]
        head_image_positions_in_frame = head_image_positions[:,frame_index,:]

        #remove the ones without head positions
        head_not_nan_indices =          np.logical_not(np.isnan(head_image_positions_in_frame.sum(axis=1)))

        #if there were not enough head positions, raise an exception
        if head_not_nan_indices.sum() < MIN_HEADS:
            if frame_index > 0:
                raise RuntimeError('Not enough camera matrices in frame %d' % frame_index)
            else:
                camera_matrix = CameraMatrix(np.ones((3,4)) * np.nan)
            
        else:
            head_image_positions_not_nan =  head_image_positions_in_frame[head_not_nan_indices]
            head_world_positions_not_nan =  head_world_positions_in_frame[head_not_nan_indices]

            #compute the camera matrix
            camera_matrix = CameraMatrix.from_planar_homography(homography_from_abs, head_world_positions_not_nan, head_image_positions_not_nan)

        #put the camera matrix in the frame
        frame.set_camera_matrix(camera_matrix)

def save_camera_matrices():
    frame_sequence = FrameSequence.load_frame_sequence(FRAME_SEQUENCE_DIR, load_homography_to_abs=True)
    player_absolute_positions = load_player_absolute_positions()
    head_image_positions = load_head_image_positions()
    head_world_positions = get_head_world_positions(player_absolute_positions)

    #compute the camera matrices in each frame
    compute_camera_matrices(frame_sequence, head_world_positions, head_image_positions)

    #save the camera matrices
    frame_sequence.save_camera_matrices(PARENT_DIR)


if __name__ == '__main__':
    save_camera_matrices()
