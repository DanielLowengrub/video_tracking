import numpy as np
import cv2
from ...source.data_structures.contour import Contour

def test_get_points_in_contour():
    image = np.zeros((300,600,3), np.uint8)
    cv_contour = np.array([[[196, 192]],
                           [[196, 198]],
                           [[199, 198]],
                           [[199, 192]]])
    contour = Contour(cv_contour, image)

    mask = contour.get_mask()
    print 'there are %d points in the contour.' % (mask.sum())
    
    points_in_contour = contour.get_points_in_contour()

    print 'cv contour:'
    print cv_contour
    
    print 'points in contour:'
    print points_in_contour

if __name__ == '__main__':
    test_get_points_in_contour()
