import numpy as np
import cv2
from ...source.field_extractor.stupid_field_extractor import StupidFieldExtractor
#######
# This is a tester for the file code/field_extractor/stupid_field_extractor.py
#
# We load an image and display the part of the image that the field extractor declares as a field.
######

#interesting test images
#215
#141
#2
#96

TEST_IMAGE_NAME = 'test_data/video3/out96.png'

if __name__=='__main__':
    
    print 'starting stupid field extractor tester'
    print 'loading image: ', TEST_IMAGE_NAME
    img = cv2.imread(TEST_IMAGE_NAME, 1)

    print 'creating field extractor'
    field_extractor = StupidFieldExtractor(img)

    print 'generating the field mask'
    field_mask = field_extractor.get_field_mask()
    mask_shape = field_mask.shape
    
    masked_image = np.uint8(img * field_mask.reshape((mask_shape[0], mask_shape[1], 1)))

    cv2.imshow('the original image', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    cv2.imshow('the mask', field_mask)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    cv2.imshow('the masked image', masked_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


