from ...source.preprocessing_full.preprocessing_data_controller import PreprocessingDataController
from ...source.auxillary.iterator_len import IteratorLen

OUTPUT_PARENT_DIR = '/Users/daniel/Documents/soccer/output/preprocessing_full/data/images-0_00-1_00/stage_1/annotation'
INPUT_PARENT_DIR =  '/Users/daniel/Documents/soccer/output/preprocessing_full/data/images-0_00-1_00/annotation'

REFINED_INTERVAL_ITER = IteratorLen(1, iter([(10,20)]))

def test():
    pd_controller = PreprocessingDataController(2)
    pd_controller.refine_intervals(OUTPUT_PARENT_DIR, INPUT_PARENT_DIR, REFINED_INTERVAL_ITER)

if __name__ == '__main__':
    test()
