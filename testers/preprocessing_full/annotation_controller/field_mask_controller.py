import os
import numpy as np
import cv2
from ....source.preprocessing_full.annotation_controller.stupid_field_mask_controller import StupidFieldMaskController
from ....source.preprocessing_full.preprocessing_data_type import PreprocessingDataType
from ....source.preprocessing_full.preprocessing_data_tuple import PreprocessingDataTuple
from ....source.auxillary import mask_operations

frame = 50
IMAGE_FILENAME = os.path.join('/Users/daniel/Documents/soccer/images', 'video_2', 'images-6_35-6_38',
                              'image_%02d.png' % frame)

HUD_MASK = np.zeros((352,624), np.bool)
HUE_MIN = 36
HUE_MAX = 54

def test_field_mask():
    print 'loading image from: %s' % IMAGE_FILENAME
    image = cv2.imread(IMAGE_FILENAME, 1)
    pd_tuple = PreprocessingDataTuple({PreprocessingDataType.IMAGE: image})

    print 'computing the field mask of image:'
    cv2.imshow('input image', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    
    fm_controller = StupidFieldMaskController(0, HUD_MASK, HUE_MIN, HUE_MAX)
    field_mask = fm_controller._build_data_from_pd_tuple(pd_tuple)

    mask_operations.draw_mask(image, field_mask.get_grass_mask(), (255,0,0), 0.7)
    cv2.imshow('grass shaded blue', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    return

if __name__ == '__main__':
    test_field_mask()


