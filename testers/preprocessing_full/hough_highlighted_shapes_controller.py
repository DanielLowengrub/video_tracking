import os
import cv2
import numpy as np
from ...source.image_processing.image_annotator.soccer_annotation import SoccerAnnotation
from ...source.preprocessing_full.highlighted_shapes_controller.hough_highlighted_shapes_controller import HoughHighlightedShapesController
from ...source.auxillary import mask_operations
from ..auxillary import primitive_drawing

#frame = 162 + 48
#frame = 162 + 285
#frame = 300
#frame  = 450
#frame = 650
frame = 597 + 29
#frame = 880
#frame = 843 + 1
#frame = 843 + 76
#frame = 843 + 113
#frame = 843 + 133
#frame = 843 + 64
#frame = 990
#frame = 1141 + 63
#frame = 1141 + 68
#frame = 1141 + 165
#frame = 1141 + 153
# IMAGE_FILENAME = os.path.join('/Users/daniel/Documents/soccer/output/preprocessing_full/data/images-0_00-1_00',
#                               'stage_0/image/interval_0_1498', 'frame_%d' % frame, 'image.png')
# ANNOTATION_FILENAME = os.path.join('/Users/daniel/Documents/soccer/output/preprocessing_full/data/images-0_00-1_00',
#                               'stage_0/annotation/interval_0_1498', 'frame_%d' % frame, 'annotation.npy')

interval = (12356, 14353)
frame = 1017
IMAGE_FILENAME = os.path.join('/Users/daniel/Dropbox/soccer/daniel_refactored/test_data/10_min_investigation',
                              'interval_%d_%d' % interval, 'frame_%d' % frame, 'image.png')
ANNOTATION_FILENAME = os.path.join('/Users/daniel/Dropbox/soccer/daniel_refactored/test_data/10_min_investigation',
                                   'interval_%d_%d' % interval, 'frame_%d' % frame, 'annotation.npy')

SHAPE_COLOR = (0,255,0)
HIGHLIGHTED_COLOR = (255,0,0)
HIGHLIGHTED_ALPHA = 0.6

NUM_THETA_BINS  = 800
NUM_RADIUS_BINS = 300
MASK_SHAPE = (352, 624)
PEAK_THRESHOLD = 60
PEAK_THRESHOLD_RATIO = 0.75
CLUSTERING_RADIUS = 10

MAX_THETA_STD = 1.0*np.pi/180.0
MAX_RADIUS_STD = 3.0
MIN_LONG_LINE_LENGTH = 200

MIN_POINTS_IN_ELLIPSE = 100
RANSAC_MIN_POINTS_TO_FIT = 20
RANSAC_NUM_ITERATIONS = 50
RANSAC_DISTANCE_TO_SHAPE_THRESHOLD = 3
RANSAC_MIN_PERCENTAGE_IN_SHAPE = 0.9

MAX_AVERAGE_ELLIPSE_RESIDUE = 10
THICKNESS = 3

MIN_INTERVAL_ON_LINE_LENGTH = 50
MIN_INTERVAL_ON_ELLIPSE_LENGTH = 5

MIN_TOTAL_LINE_LENGTH = 40
MIN_ELLIPSE_AXIS = 15
MIN_TOTAL_ELLIPSE_LENGTH = 270

def load_image_annotation():
    image = cv2.imread(IMAGE_FILENAME, 1)
    annotation = SoccerAnnotation(np.load(ANNOTATION_FILENAME))

    return image, annotation

def find_shapes():
    image, annotation = load_image_annotation()

    print 'finding shapes in the following image:'
    cv2.imshow('image', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    hs_controller = HoughHighlightedShapesController(0,
                                                     NUM_THETA_BINS, NUM_RADIUS_BINS, MASK_SHAPE, PEAK_THRESHOLD,
                                                     PEAK_THRESHOLD_RATIO, CLUSTERING_RADIUS,
                                                     MAX_THETA_STD, MAX_RADIUS_STD, MIN_LONG_LINE_LENGTH,
                                                     MIN_POINTS_IN_ELLIPSE,
                                                     RANSAC_MIN_POINTS_TO_FIT, RANSAC_NUM_ITERATIONS,
                                                     RANSAC_DISTANCE_TO_SHAPE_THRESHOLD,
                                                     RANSAC_MIN_PERCENTAGE_IN_SHAPE, MAX_AVERAGE_ELLIPSE_RESIDUE,
                                                     THICKNESS,
                                                     MIN_INTERVAL_ON_LINE_LENGTH, MIN_INTERVAL_ON_ELLIPSE_LENGTH,
                                                     MIN_TOTAL_LINE_LENGTH, MIN_ELLIPSE_AXIS, MIN_TOTAL_ELLIPSE_LENGTH)
        
    
    highlighted_shapes = hs_controller._find_highlighted_shapes(image, annotation)

    #first draw the shapes on the image
    for hs in highlighted_shapes:
        primitive_drawing.draw_shape(image, hs.get_shape(), SHAPE_COLOR)
        
    #now draw the highlighted regions
    for hs in highlighted_shapes:
        mask = hs.generate_mask(image)
        mask_operations.draw_mask(image, mask, np.array(HIGHLIGHTED_COLOR), HIGHLIGHTED_ALPHA)

    cv2.imshow('the shapes', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
        

    return

if __name__ == '__main__':
    find_shapes()
