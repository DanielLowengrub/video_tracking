import numpy as np
import cv2
import os
import matplotlib as mpl
#mpl.use('Agg')
import matplotlib.pyplot as plt
from ....source.preprocessing_full.annotation_training_data_controller.simple_training_data_controller import SimpleTrainingDataController
from ....source.preprocessing_full.annotation_training_data_controller.annotation_training_data_controller import AnnotationTrainingValues
from ....source.preprocessing_full.image_controller import ImageController
from ....source.preprocessing_full.annotation_controller.annotation_controller import AnnotationController
from ....source.preprocessing_full.highlighted_shapes_controller.highlighted_shapes_controller import HighlightedShapesController
from ....source.preprocessing_full.labelled_shapes_controller.labelled_shapes_controller import LabelledShapesController

from ....source.preprocessing_full.absolute_homography_controller.absolute_homography_controller import AbsoluteHomographyController
from ....source.preprocessing_full.players_controller.players_controller import PlayersController
from ....source.auxillary.soccer_field_geometry import SoccerFieldGeometry
from ...auxillary import label_shapes
from ...auxillary import primitive_drawing, highlighted_shape_drawing
from ....source.auxillary.fit_points_to_shape.line import Line

#PARENT_DIR = '/Users/daniel/Documents/soccer/output/preprocessing_pass_0/data'
#PARENT_DIR = '/home/daniel/soccer/output/preprocessing_pass_0/data'

#INPUT_DATA_NAME = 'images-9_20-9_31'
#INPUT_DATA_NAME = 'images-8_15-9_31'
#INPUT_DATA_NAME = 'images-0_00-10_00'

#INTERVAL = (25,84)
#FRAME = 20
#INTERVAL = (1143, 1212)
#FRAME = 18

# IMAGE_DIR = os.path.join(PARENT_DIR, INPUT_DATA_NAME, 'stage_3', 'image',
#                                       'interval_%d_%d' % INTERVAL, 'frame_%d' % FRAME)
# HIGHLIGHTED_SHAPES_DIR = os.path.join(PARENT_DIR, INPUT_DATA_NAME, 'stage_3', 'highlighted_shapes',
#                                       'interval_%d_%d' % INTERVAL, 'frame_%d' % FRAME)
# LABELLED_SHAPES_DIR = os.path.join(PARENT_DIR, INPUT_DATA_NAME, 'stage_3', 'labelled_shapes',
#                                       'interval_%d_%d' % INTERVAL, 'frame_%d' % FRAME)
# ABSOLUTE_HOMOGRAPHY_DIR = os.path.join(PARENT_DIR, INPUT_DATA_NAME, 'stage_3', 'absolute_homography',
#                                       'interval_%d_%d' % INTERVAL, 'frame_%d' % FRAME)
# ANNOTATION_DIR = os.path.join(PARENT_DIR, INPUT_DATA_NAME, 'stage_3', 'annotation',
#                                       'interval_%d_%d' % INTERVAL, 'frame_%d' % FRAME)
# PLAYERS_DIR = os.path.join(PARENT_DIR, INPUT_DATA_NAME, 'stage_3', 'players',
#                                       'interval_%d_%d' % INTERVAL, 'frame_%d' % FRAME)

PARENT_DIR = '/Users/daniel/Dropbox/soccer/daniel_refactored/test_data/failure_modes/annotation_training_data'
VIDEO_NAME = 'Germany-USA-06_10_2015-FrxUfp0B3pA'
FRAME_IMAGES_NAME = 'images-10_00-20_00'
#FRAME_IMAGES_NAME = 'images-9_00-10_00'

# INTERVAL = (1168,1479)
# FRAME = 0
# INTERVAL = (12324, 12459)
# FRAME = 50
INTERVAL = (13450, 13490)
FRAME = 0

# INTERVAL = (1024, 1083)
# FRAME = 40

FM_DIR = os.path.join(PARENT_DIR, VIDEO_NAME, FRAME_IMAGES_NAME, 'interval_%d_%d' % INTERVAL, 'frame_%d' % FRAME)

IMAGE_DIR               = os.path.join(FM_DIR, 'image')
HIGHLIGHTED_SHAPES_DIR  = os.path.join(FM_DIR, 'highlighted_shapes')
LABELLED_SHAPES_DIR     = os.path.join(FM_DIR, 'labelled_shapes')
ABSOLUTE_HOMOGRAPHY_DIR = os.path.join(FM_DIR, 'absolute_homography')
ANNOTATION_DIR          = os.path.join(FM_DIR, 'annotation')
PLAYERS_DIR             = os.path.join(FM_DIR, 'players')

MATCHED_LINE_THICKNESS = 2
MATCHED_ELLIPSE_THICKNESS = 3
UNMATCHED_THICKNESS = 25
PREDICTED_SHAPE_DILATION = 10

#the absolute image that we compute the homography to
ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
SCALE = 6
ABSOLUTE_IMAGE = cv2.imread(ABSOLUTE_FILE_NAME, 1)

def load_image():
    image = cv2.imread(os.path.join(IMAGE_DIR, 'image.png'), 1)
    return image

def load_annotation():
    annotation = AnnotationController.load_data_from_frame_dir(ANNOTATION_DIR)
    return annotation

def load_highlighted_shapes():
    highlighted_shapes = HighlightedShapesController.load_data_from_frame_dir(HIGHLIGHTED_SHAPES_DIR)
    return highlighted_shapes

def load_labelled_shapes():
    labelled_shapes = LabelledShapesController.load_data_from_frame_dir(LABELLED_SHAPES_DIR)
    return labelled_shapes

def load_absolute_homography():
    H = AbsoluteHomographyController.load_data_from_frame_dir(ABSOLUTE_HOMOGRAPHY_DIR)
    return H

def load_players():
    players = PlayersController.load_data_from_frame_dir(PLAYERS_DIR)
    return players

def build_soccer_field():
    field_width  = ABSOLUTE_IMAGE.shape[1] - 1
    field_length = ABSOLUTE_IMAGE.shape[0] - 1
    return SoccerFieldGeometry(SCALE, field_width, field_length)

def display_training_data(image, training_data):    
    fig = plt.figure()
    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)

    cmap = mpl.colors.ListedColormap(['k', 'g', 'w', 'b', '0.5'])
    norm = mpl.colors.BoundaryNorm(range(6), cmap.N)
    
    ax1.imshow(image, interpolation='nearest')
    ax1.set_ylim([image.shape[0], 0])
    ax2.imshow(training_data, cmap=cmap, norm=norm, interpolation='nearest')
    ax2.set_ylim([training_data.shape[0], 0])

    #plt.savefig('/home/daniel/Documents/Pictures/annotation_training_data.png', bbox_inches='tight')
    print 'plotting...'
    plt.show()
    return

def test():
    image = load_image()
    annotation = load_annotation()
    highlighted_shapes = load_highlighted_shapes()
    labelled_shapes = load_labelled_shapes()
    H = load_absolute_homography()
    players = load_players()
    
    soccer_field = build_soccer_field()

    abs_image = cv2.imread('/Users/daniel/Dropbox/soccer/daniel_refactored/test_data/sideline_mask.png', 1)
    line = Line.from_parametric_representation(H[2])
    primitive_drawing.draw_line(abs_image, line, (0,255,0))
    cv2.imshow('line at infinity', abs_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    training_data_controller = SimpleTrainingDataController(0, soccer_field, MATCHED_LINE_THICKNESS,
                                                            MATCHED_ELLIPSE_THICKNESS, UNMATCHED_THICKNESS,
                                                            PREDICTED_SHAPE_DILATION)

    training_data = training_data_controller._build_training_data(H, annotation, highlighted_shapes,
                                                                  labelled_shapes, players)

    if training_data is None:
        print 'could not build training data.'

    else:
        print 'number of sideline pixels: ', np.sum(training_data == AnnotationTrainingValues.SIDELINE)
        print 'number of player pixels: ',   np.sum(training_data == AnnotationTrainingValues.PLAYER)
        display_training_data(image, training_data)
        
    return

if __name__ == '__main__':
    test()
    
