import numpy as np
import cv2
import os
from ....source.preprocessing_full.labelled_shapes_controller.simple_labelled_shapes_controller import SimpleLabelledShapesController
from ....source.preprocessing_full.image_controller import ImageController
from ....source.preprocessing_full.highlighted_shapes_controller.highlighted_shapes_controller import HighlightedShapesController
from ....source.preprocessing_full.absolute_homography_controller.absolute_homography_controller import AbsoluteHomographyController
from ....source.auxillary.soccer_field_geometry import SoccerFieldGeometry
from ...auxillary import label_shapes

#PARENT_DIR = '/Users/daniel/Documents/soccer/output/preprocessing_pass_0/data'
PARENT_DIR = '/home/daniel/soccer/output/preprocessing_pass_0/data'

#INPUT_DATA_NAME = 'images-9_20-9_31'
INPUT_DATA_NAME = 'images-8_15-9_31'

INTERVAL = (820,860)
FRAME = 23

IMAGE_DIR = os.path.join(PARENT_DIR, INPUT_DATA_NAME, 'stage_3', 'image',
                                      'interval_%d_%d' % INTERVAL, 'frame_%d' % FRAME)
HIGHLIGHTED_SHAPES_DIR = os.path.join(PARENT_DIR, INPUT_DATA_NAME, 'stage_3', 'highlighted_shapes',
                                      'interval_%d_%d' % INTERVAL, 'frame_%d' % FRAME)
ABSOLUTE_HOMOGRAPHY_DIR = os.path.join(PARENT_DIR, INPUT_DATA_NAME, 'stage_3', 'absolute_homography',
                                      'interval_%d_%d' % INTERVAL, 'frame_%d' % FRAME)

#the absolute image that we compute the homography to
ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
SCALE = 6
ABSOLUTE_IMAGE = cv2.imread(ABSOLUTE_FILE_NAME, 1)

LINE_THRESH = 0.1
ELLIPSE_THRESH = 0.1

def load_image():
    image = ImageController.load_data_from_frame_dir(IMAGE_DIR)
    return image

def load_highlighted_shapes():
    highlighted_shapes = HighlightedShapesController.load_data_from_frame_dir(HIGHLIGHTED_SHAPES_DIR)
    return highlighted_shapes

def load_absolute_homography():
    H = AbsoluteHomographyController.load_data_from_frame_dir(ABSOLUTE_HOMOGRAPHY_DIR)
    return H

def build_soccer_field():
    field_width  = ABSOLUTE_IMAGE.shape[1] - 1
    field_length = ABSOLUTE_IMAGE.shape[0] - 1
    return SoccerFieldGeometry(SCALE, field_width, field_length)


def test():
    image = load_image()
    highlighted_shapes = load_highlighted_shapes()
    absolute_homography = load_absolute_homography()
    soccer_field = build_soccer_field()

    labelled_shapes_controller = SimpleLabelledShapesController(0, soccer_field, image.shape[:2],
                                                                LINE_THRESH, ELLIPSE_THRESH)

    labelled_shapes = labelled_shapes_controller._build_labelled_shapes(highlighted_shapes, absolute_homography)

    print 'labelled shapes:'
    print labelled_shapes
    
    # shape_indices = dict((str(i),hs.get_shape_with_position()) for i,hs in enumerate(highlighted_shapes))
    # label_shapes.label_shapes(image, shape_indices)
    # cv2.imshow('shape indices', image)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    
if __name__ == '__main__':
    test()
    
