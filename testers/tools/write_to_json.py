import json
import numpy as np
import os
from ...source.dynamics.particle_filter_fast.player_tracker_controller import PlayerTrackerController
from ...source.dynamics.particle_filter_fast.ball_tracker_controller import BallTrackerController
from ...source.preprocessing.camera_samples_interpolator.camera_samples_interpolator import CameraSamplesInterpolator
from ...source.auxillary import polygon_operations
#############################
# In this file we have code that saves a list of player positions to a json file
#############################

OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
INPUT_DATA_NAME = 'images-8_15-9_31'

POSTPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'postprocessing', 'data', INPUT_DATA_NAME)

JSON_FILE = os.path.join(OUTPUT_PARENT_DIRECTORY, 'json', 'data', INPUT_DATA_NAME + 'with_viz_poly' + '.json')
TRACKED_PLAYERS_DIRECTORY = os.path.join(POSTPROCESSING_DIRECTORY, 'consolidated_team_labels')
TRACKED_BALLS_DIRECTORY = os.path.join(POSTPROCESSING_DIRECTORY, 'no_ball_trajectory_overlaps')
CAMERA_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing', 'data', INPUT_DATA_NAME, 'camera')
PLAYER_FORMAT = ["id", "team", "x", "y"]
VISIBLE_POLYGON_FORMAT = ["x", "y"]
FPS = 25

IMAGE_SHAPE = (352, 624)
ABSOLUTE_SHAPE = (480, 744)

def format_player_positions(tracked_players):
    '''
    tracked_players - a TrackedPlayers object

    return a list of lists of lists: (each row corresponds to a frame)
    [ [[player_id, team, x, y], [player_id, team, x, y], ... ],
      [[player_id, team, x, y], [player_id, team, x, y], ... ],
       ... ,
      [[player_id, team, x, y], [player_id, team, x, y], ... ] ]
    '''

    output = []

    num_frames = tracked_players.number_of_frames()
    
    for frame_index in range(num_frames):
        print 'formatting frame %d for json' % frame_index
        
        frame_entries = []
        labelled_position_dict = tracked_players.get_labelled_positions_in_frame(frame_index)
        
        for player_id, (team_label, position) in labelled_position_dict.iteritems():
            if np.isnan(position).any():
                continue

            position = np.int32(position)
            team_label = np.int32(team_label)
            entry = [int(player_id), int(team_label), int(position[0]), int(position[1])]
            frame_entries.append(entry)

        #print frame_entries
        output.append(frame_entries)

    return output

def format_ball_positions(tracked_balls, num_frames):
    '''
    tracked_balls - a TrackedPlayers object

    return a list of lists: (each row corresponds to a frame)
    [[ball in frame 0], [ball in frame 1],...
    If there is no ball in a specific frame, the list is empty
    '''
    print 'formatting ball positions...'
    output = []

    for frame_index in range(num_frames):
        print '   formatting frame %d for json' % frame_index
        
        frame_entries = []
        abs_positions, img_positions = tracked_balls.get_ball_positions_in_frame(frame_index)

        if len(abs_positions) == 1:
            p = abs_positions[0]
            output.append([int(p[0]), int(p[1])])

        else:
            output.append([])
            
    return output

def build_visible_polygons(cameras):
    '''
    cameras - an iterator of Camera objects
    return - a list of the form [polygon in frame 0, polygon in frame 1, ...]
    each polygon is a list of points: [[x0,y0], [x1,y2], ... ]
    '''
    output = []

    for camera in cameras:
        H = camera.get_homography_matrix()
        visible_poly_points = polygon_operations.compute_bounded_visibility_polygon_points(H,
                                                                                           IMAGE_SHAPE,
                                                                                           ABSOLUTE_SHAPE)
        output.append(visible_poly_points.astype(np.int32).tolist())

    return output

    
def build_positions_dict(tracked_players, tracked_balls, cameras):
    '''
    tracked_players - a TrackedPlayers object
    tracked_balls - a TrackedBalls object

    return a dictionary with keys 'format', 'fps', 'sequence'
    the value of 'format' is FORMAT. The value of 'fps' is FPS. The values of 'sequence' is the output of format_positions.
    '''
    num_frames = tracked_players.number_of_frames()
    
    formatted_player_positions = format_player_positions(tracked_players)
    formatted_ball_positions   = format_ball_positions(tracked_balls, num_frames)
    visible_polygons = build_visible_polygons(cameras)
    
    positions_dict = {'player_format' : PLAYER_FORMAT,
                      'fps' : FPS,
                      'visible_polygon_format' : VISIBLE_POLYGON_FORMAT,
                      'player_sequence' : formatted_player_positions,
                      'ball_sequence' : formatted_ball_positions,
                      'visible_polygons': visible_polygons}

    return positions_dict

def save_positions_to_json(tracked_players_directory, tracked_balls_directory, camera_directory, json_filename):
    '''
    tracked_players_directory - a directory that can be loaded by the PlayerTrackerController
    tracked_balls_directory - a directory that can be loaded with the BallTrackerController
    json_filename - path to the json file
    
    We save the positions in the json file.
    '''
    indexed_tracked_players = PlayerTrackerController.load_tracked_players(tracked_players_directory)
    indexed_tracked_balls = BallTrackerController.load_tracked_balls(tracked_balls_directory)
    indexed_cameras = CameraSamplesInterpolator.load_cameras(camera_directory)

    tracked_players = next(indexed_tracked_players)[1]
    tracked_balls = next(indexed_tracked_balls)[1]
    cameras = next(indexed_cameras)[1]
    
    positions_dict = build_positions_dict(tracked_players, tracked_balls, cameras)

    f = open(json_filename, 'wb')
    json.dump(positions_dict, f)
    f.close()

    return

if __name__ == '__main__':
    save_positions_to_json(TRACKED_PLAYERS_DIRECTORY, TRACKED_BALLS_DIRECTORY, CAMERA_DIRECTORY, JSON_FILE)
    
