import numpy as np
import cv2
import itertools

from ...source.image_processing.contour_generator.edges_contour_generator import EdgesContourGenerator
from ...source.image_processing.field_extractor.stupid_field_extractor import StupidFieldExtractor

IMAGE_NUMBER = 223
IMAGE_ABS_NAME = 'test_data/video3/out%d.png' % IMAGE_NUMBER
OUTPUT_ABS_NAME = 'output/tester/points_on_image/edge_points_on_image_%d' % IMAGE_NUMBER

def extract_contours(image):
    field_extractor = StupidFieldExtractor()
    field_mask = field_extractor.get_field_mask(image)

    contourGenerator = EdgesContourGenerator()
    contours = contourGenerator.generate_contours(image, mask=field_mask)
    
    contours += itertools.chain(*(contour.get_child_contours() for contour in contours))

    return contours

def write_edge_points_to_file():
    image = cv2.imread(IMAGE_ABS_NAME, 1)
    contours = extract_contours(image)
    points = np.vstack(contour.get_cv_contour().reshape((-1,2)) for contour in contours)

    image[points[:,1], points[:,0]] = np.array([255,0,0])
    cv2.imshow('image with edge points', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    np.save(OUTPUT_ABS_NAME, points)
    

if __name__ == '__main__':
    write_edge_points_to_file()
