from ...source.auxillary.contour_shape_overlap.highlighted_shape_generator import HighlightedShapeGenerator
from ...source.auxillary.contour_shape_overlap.highlighted_shape import HighlightedShape
from ...source.auxillary.mask_operations import mix_images_with_mask
from ...source.data_structures.contour import Contour
from ...source.image_processing.hough_transform.line_detector import LineDetector
from ...source.image_processing.contour_generator.edges_contour_generator import EdgesContourGenerator
from ...source.image_processing.field_extractor.stupid_field_extractor import StupidFieldExtractor
from ...source.auxillary.contour_shape_overlap.line_with_position import LineWithPosition
from ...source.auxillary.contour_shape_overlap.ellipse_with_position import EllipseWithPosition
from ...source.image_processing.hough_transform.single_ellipse_detector import SingleEllipseDetector
from ..auxillary.contour_shape_overlap.highlighted_shape_generator import draw_line, draw_interval_mask
import cv2
import numpy as np
import itertools
import os

#########################
#
# This file is used to generate HighlightedShape objects from a list of images and save them to a file.
# It also saves a visualization of the results.
#
#########################

frame_indices = range(1,20)
TEST_IMAGE_NAMES = ['test_data/video3/out%d.png' % (frame_index) for frame_index in frame_indices]

#OUTPUT_DIRECTORY = 'output/tester/highlighted_shapes/saved_data/frames_%d_%d' % (frame_indices[0], frame_indices[-1])
#VISUALIZATION_DIRECTORY = 'output/tester/highlighted_shapes/visualization/frames_%d_%d' % (frame_indices[0], frame_indices[-1])

OUTPUT_DIRECTORY = 'output/tester/highlighted_shapes/saved_data/frames_%d_%d' % (frame_indices[0], frame_indices[-1])
VISUALIZATION_DIRECTORY = 'output/tester/highlighted_shapes/visualization/frames_%d_%d' % (frame_indices[0], frame_indices[-1])

HIGHLIGHTED_SHAPE_DIRECTORY = {HighlightedShape.LINE:'highlighted_line', HighlightedShape.ELLIPSE:'highlighted_ellipse'}

# THICKNESS = 3
# MIN_TANGENT_ARC_LENGTH = 20
# MAX_TANGENT_ALGEBRAIC_RESIDUE = 1.5
# THETA_BIN_WIDTH = 2
# RADIUS_BIN_WIDTH = 5
# MAX_THETA_DEVIATION = 0.7
# MAX_RADIUS_DEVIATION = 5
# MIN_ARC_LENGTH = 300
# MAX_AVERAGE_ALGEBRAIC_RESIDUE = 10

MIN_TANGENT_TO_LINE_ARC_LENGTH = 20
MIN_TANGENT_TO_ELLIPSE_ARC_LENGTH = 50
MAX_TANGENT_TO_LINE_GEOMETRIC_RESIDUE = 1.5
MAX_TANGENT_TO_ELLIPSE_GEOMETRIC_RESIDUE = 10
MIN_TANGENT_TO_ELLIPSE_AREA = 500

THETA_BIN_WIDTH = 2
RADIUS_BIN_WIDTH = 5
MAX_THETA_DEVIATION = 0.7
MAX_RADIUS_DEVIATION = 5
MIN_ARC_LENGTH = 300
MAX_AVERAGE_ALGEBRAIC_RESIDUE = 10

RANSAC_MIN_POINTS_TO_FIT = 20
RANSAC_NUM_ITERATIONS = 50
RANSAC_DISTANCE_TO_SHAPE_THRESHOLD = 3
RANSAC_MIN_PERCENTAGE_IN_SHAPE = 0.9

MAX_AVERAGE_ELLIPSE_RESIDUE = 10
THICKNESS = 3
    
def save_highlighted_shapes(highlighted_shapes, base_directory):
    '''
    Save the highlighted shapes in the given directory
    '''
    for i, highlighted_shape in enumerate(highlighted_shapes):
        highlighted_shape_directory = HIGHLIGHTED_SHAPE_DIRECTORY[highlighted_shape.get_shape_type()]
        highlighted_shape_directory = os.path.join(base_directory, '%s_%d' % (highlighted_shape_directory, i))
        os.mkdir(highlighted_shape_directory)
        print 'saving a highlighted shape to: ', highlighted_shape_directory
        highlighted_shape.save_to_directory(highlighted_shape_directory)

    return

def draw_highlighted_shape(image, highlighted_shape):
    if highlighted_shape.is_line():
        draw_line(image, highlighted_shape.get_shape())
    if highlighted_shape.is_ellipse():
        center, axes, angle = highlighted_shape.get_shape().get_geometric_parameters(in_degrees=True)
        cv2.ellipse(image, tuple(np.int32(center).tolist()), tuple(np.int32(axes).tolist()), angle, 0, 360, (0,255,0), 1, cv2.CV_AA)

    draw_interval_mask(image, highlighted_shape)
            
    return

def save_highlighted_shapes_visualization(image, highlighted_shapes, output_image_path):
    '''
    create a visualization of the highlighted lines and save it to the given path
    '''

    output_image = image.copy()

    #draw each of the highlighted lines on the image
    for highlighted_shape in highlighted_shapes:
        draw_highlighted_shape(output_image, highlighted_shape)

    print 'writing visualization to: ', output_image_path
    cv2.imwrite(output_image_path, output_image)
    return

def extract_highlighted_shapes_from_image(image):
    '''
    Return a list of highlighted shapes
    '''

    print 'getting the field mask...'
    field_extractor = StupidFieldExtractor()
    field_mask = field_extractor.get_field_mask(image)
    
    print 'generating contours...'
    contourGenerator = EdgesContourGenerator()
    base_contours = contourGenerator.generate_contours(image, mask=field_mask)
    base_contours_with_children = [([base_contour] + base_contour.get_child_contours()) for base_contour in base_contours]
    all_contours = list(itertools.chain(*(base_contour_with_children for base_contour_with_children in base_contours_with_children)))

    #contours = contourGenerator.generate_contours(image, mask=field_mask)
    #contours += itertools.chain(*(contour.get_child_contours() for contour in contours))

    line_detector = LineDetector(MIN_TANGENT_TO_LINE_ARC_LENGTH, MAX_TANGENT_TO_LINE_GEOMETRIC_RESIDUE, THETA_BIN_WIDTH, RADIUS_BIN_WIDTH, \
                                 MAX_THETA_DEVIATION, MAX_RADIUS_DEVIATION, MIN_ARC_LENGTH, MAX_AVERAGE_ALGEBRAIC_RESIDUE)

    single_ellipse_detector = SingleEllipseDetector(MIN_TANGENT_TO_LINE_ARC_LENGTH, MAX_TANGENT_TO_LINE_GEOMETRIC_RESIDUE,
                                                    MIN_TANGENT_TO_ELLIPSE_ARC_LENGTH, MAX_TANGENT_TO_ELLIPSE_GEOMETRIC_RESIDUE, MIN_TANGENT_TO_ELLIPSE_AREA,
                                                    RANSAC_MIN_POINTS_TO_FIT, RANSAC_NUM_ITERATIONS, RANSAC_DISTANCE_TO_SHAPE_THRESHOLD, RANSAC_MIN_PERCENTAGE_IN_SHAPE,
                                                    MAX_AVERAGE_ELLIPSE_RESIDUE)

    # image_with_contours = image.copy()
    # for contour in contours:
    #     cv_contour = contour.get_cv_contour()
    #     cv2.drawContours(image_with_contours, [cv_contour], -1, (255, 255, 255), 1)

    # cv2.imshow('image with contours', image_with_contours)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    print 'looking for lines in the contour...'
    lines = line_detector.get_lines(image, all_contours)
    lines_with_position = [LineWithPosition(line) for line in lines]

    print 'highlighting the lines...'
    
    highlighted_shape_generator = HighlightedShapeGenerator()
    highlighted_lines_and_contour_masks = [highlighted_shape_generator.generate_highlighted_shape(l, THICKNESS, all_contours) for l in lines_with_position]

    highlighted_lines = [x[0] for x in highlighted_lines_and_contour_masks]

    print 'removing the lines from the image...'
    new_base_contour_components = HighlightedShape.remove_shapes_from_contours(highlighted_lines, base_contours)

    print 'finding ellipses...'
    
    highlighted_ellipses = []
    
    for base_contour_component in new_base_contour_components:
        all_contours_in_component = list(itertools.chain(*([base_contour] + base_contour.get_child_contours() for base_contour in base_contour_component)))
        
        #fitted_ellipse, total_geometric_residue, inlier_mask = ransac_ellipse_fitter.fit_points(points_on_ellipse_in_component)
        fitted_ellipse = single_ellipse_detector.find_ellipse(all_contours_in_component)
        
        if fitted_ellipse is not None:
            fitted_ellipse.compute_geometric_parameters()
            ellipse_with_position = EllipseWithPosition(fitted_ellipse)
            highlighted_ellipse, contour_masks = highlighted_shape_generator.generate_highlighted_shape(ellipse_with_position, THICKNESS, all_contours_in_component)
            highlighted_ellipses.append(highlighted_ellipse)
            
    
    return highlighted_lines + highlighted_ellipses

def generate_highlighted_shapes():

    for i, path_to_image in enumerate(TEST_IMAGE_NAMES):
        print 'loading image from: ', path_to_image
        image = cv2.imread(path_to_image, 1)

        #extract the highlighed shapes from the image
        highlighted_shapes = extract_highlighted_shapes_from_image(image)

        #save the highlighted shapes
        highlighted_shapes_directory = os.path.join(OUTPUT_DIRECTORY, ('image_%d' % i))
        os.mkdir(highlighted_shapes_directory)
        save_highlighted_shapes(highlighted_shapes, highlighted_shapes_directory)

        #save a visualization of the highlighted shapes
        output_image_path = os.path.join(VISUALIZATION_DIRECTORY, ('image_%3d.png' % i).replace(' ','0'))
        save_highlighted_shapes_visualization(image, highlighted_shapes, output_image_path)

    return

if __name__ == '__main__':
    generate_highlighted_shapes()
