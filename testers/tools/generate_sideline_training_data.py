import os
import re
import itertools, collections
import operator
import numpy as np
import cv2

from ...source.auxillary.fit_points_to_shape import planar_shape
from ...source.auxillary.contour_shape_overlap.highlighted_shape import HighlightedShape
from ...source.data_structures.frame_sequence.frame_sequence import FrameSequence
from ...source.auxillary.mask_operations import mix_images_with_mask, get_union, apply_mask_to_image
from ...source.image_processing.contour_generator.edges_contour_generator import EdgesContourGenerator
from ...source.image_processing.field_extractor.stupid_field_extractor import StupidFieldExtractor
from ...source.image_processing.image_annotator.soccer_field_annotator import SoccerFieldAnnotator
#######
# This file is used to generate training data for sideline detection.
# Specifically:
#
# 1) We generate positive examples by using highlighted shapes on the image
# 2) We generate negative samples by using points on player contours that are not on the highighted shapes.
#######

HIGHLIGHTED_SHAPES_DIRECTORY = 'output/tester/highlighted_shapes/saved_data/frames_50_269'
FRAME_SEQUENCE_DIRECTORY = 'output/tester/frame_sequences/frame_sequence_test_8/frame_sequences_50_269-0_219'

HIGHLIGHTED_SHAPE_BASENAME_DICT = {'highlighted_line': planar_shape.LINE, 'highlighted_ellipse': planar_shape.ELLIPSE}

highlighted_shape_regex = re.compile('([a-z_]+)_([0-9]+)')
image_directory_regex = re.compile('image_([0-9]+)')

MIN_INTERVAL_LENGTH_DICT = {planar_shape.LINE: 50, planar_shape.ELLIPSE: 20}
SHAPE_PADDING_DICT = {planar_shape.LINE: 0, planar_shape.ELLIPSE: 0}

IMAGE_BOUNDARY_PADDING = 50

IMAGE_PATCH_SIZE = 16

NUM_GRASS_POINTS_PER_IMAGE = 200

PATCH_BASENAME = 'patch'
POSITIVE_SET_DIRECTORY = '/Users/daniel/Documents/soccer/training_data/sideline_detection/positive_set'
NEGATIVE_SET_DIRECTORY = '/Users/daniel/Documents/soccer/training_data/sideline_detection/negative_set'
GRASS_DIRECTORY = '/Users/daniel/Documents/soccer/training_data/sideline_detection/grass'

def get_shape_type(shape_directory):
    '''
    Return HighlightedShape.LINE or HighlightedShape.ELLIPSE depending on the directory name
    '''

    match = highlighted_shape_regex.match(shape_directory)

    if match is None:
        return None

    shape_string, shape_index = match.groups()

    if shape_string in HIGHLIGHTED_SHAPE_BASENAME_DICT:
        return HIGHLIGHTED_SHAPE_BASENAME_DICT[shape_string]

    else:
        return None

    return
        
def load_highlighted_shapes_on_image(base_directory):
    '''
    base_directory - a directory containing subdirectories corresponding to highlighted shapes on an image
    Return a list of HighlightedShape objects
    '''
    print '   * loading highlighted shapes from the image: ', base_directory
    highlighted_shapes = []
    
    for shape_directory in os.listdir(base_directory):
        print '     - loading highlighted shape from: ', shape_directory
        shape_directory_abs_path = os.path.join(base_directory, shape_directory)
        shape_type = get_shape_type(shape_directory)

        if shape_type is None:
            continue

        #print 'shape type: ', shape_type
        
        highlighted_shape = HighlightedShape.load_from_directory(shape_directory_abs_path, shape_type)
        highlighted_shapes.append(highlighted_shape)

    return highlighted_shapes

def load_highlighted_shapes_on_images(base_directory):
    '''
    base_directory - a directory containing a subdirectories of the form image_%d.
    Return a list [ [HighlightedShape objects in image_0], [HS objects in image_1],...]
    The list is sorted according to the indices of the image directories.
    '''
    print 'loading highlighted shapes from: ', base_directory

    #this dictionary will store key/value pairs (image index, dictionary of highighted shapes)
    highlighted_shapes_on_images_dict = dict()

    for image_directory in os.listdir(base_directory):
        image_directory_abs_path = os.path.join(base_directory, image_directory)
        print 'image directory: ', image_directory
        m = image_directory_regex.match(image_directory)
        if m is None:
            continue
        
        image_index = int(m.groups()[0])
        print 'image index: ', image_index
        
        highlighted_shapes_on_images_dict[image_index] = load_highlighted_shapes_on_image(image_directory_abs_path)

    #now sort the highlighted shapes object by the image index
    sorted_highlighted_shapes = sorted(highlighted_shapes_on_images_dict.items(), key=operator.itemgetter(0))
    return map(operator.itemgetter(1), sorted_highlighted_shapes)

def extract_contours(image):
    field_extractor = StupidFieldExtractor()
    field_mask = field_extractor.get_field_mask(image)

    contourGenerator = EdgesContourGenerator()
    contours = contourGenerator.generate_contours(image, mask=field_mask)
    
    contours += itertools.chain(*(contour.get_child_contours() for contour in contours))

    return contours

def display_highlighted_shape_mask(image, highlighted_shape_mask):
    '''
    display the mask on the image
    '''
    highlighted_image = image.copy()
    blue_image = np.ones(image.shape, np.uint8) * 255
    blue_image[:,:,(1,2)] = 0
    colored_image = mix_images_with_mask(blue_image, image, highlighted_shape_mask)
    alpha = 0.6

    highlighted_image = np.uint8(alpha*image + (1-alpha)*colored_image)

    #cv2.imshow('highlighted shape mask', highlighted_shape_mask)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()

    #cv2.imshow('image highlighted shape', highlighted_image)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()

    return
    
def get_sideline_points(image, highlighted_shapes_on_image):
    '''
    highlighted_shapes_on_image - a list of HighlightedShape objects
    Return a numpy array with shape (number of points, 2) which represents a list of points that are on a sideline.
    '''
    print 'finding sideline points'
    sideline_points = []
    
    #first get the contours on the image and all of the points on these contours
    contours = extract_contours(image)
    points = np.vstack([contour.get_cv_contour().reshape((-1,2)) for contour in contours])

    #build a mask containing the all of the padded sidelines. The padding is used to throw out parts of the sidelines near the ends.
    #we also disgard sideline intervals whose length is too small
    highlighted_shapes_mask = np.zeros(image.shape[:2], np.float32)

    print '   building highlighted shape mask from %d shapes...' % len(highlighted_shapes_on_image)
    for highlighted_shape in highlighted_shapes_on_image:
        shape_type = highlighted_shape.get_shape().get_shape_type()
        min_interval_length = MIN_INTERVAL_LENGTH_DICT[shape_type]
        padding = SHAPE_PADDING_DICT[shape_type]

        highlighted_shape_mask = highlighted_shape.generate_mask(image, min_interval_length, padding)
        highlighted_shapes_mask = get_union(highlighted_shapes_mask, highlighted_shape_mask)


    display_highlighted_shape_mask(image, highlighted_shapes_mask)
    
    #now record the points that are in the highlighted shapes mask
    point_in_highlighted_shapes = highlighted_shapes_mask[points[:,1], points[:,0]] > 0.0
    return points[point_in_highlighted_shapes]

def get_player_points(image, player_contours, highlighted_shapes_on_image):
    '''
    highlighted_shapes_on_image - a list of HighlightedShape objects
    player_contours - a list of Contour objects
    '''
    points = np.vstack([contour.get_cv_contour().reshape((-1,2)) for contour in player_contours])

    #build a mask containing all of the sidelines.
    highlighted_shapes_mask = np.zeros(image.shape[:2], np.float32)
    
    for highlighted_shape in highlighted_shapes_on_image:
        highlighted_shape_mask = highlighted_shape.generate_mask(image)
        highlighted_shapes_mask = get_union(highlighted_shapes_mask, highlighted_shape_mask)

    #now record the points that are not in the highlighted shapes mask
    point_in_highlighted_shapes = highlighted_shapes_mask[points[:,1], points[:,0]] == 0.0
    point_in_image = (IMAGE_BOUNDARY_PADDING < points[:,0]) * (points[:,0] < (image.shape[1] - IMAGE_BOUNDARY_PADDING))
    point_mask = point_in_highlighted_shapes * point_in_image
                      
    return points[point_mask]

def get_grass_points(image, grass_mask):
    grass_mask = np.float32(grass_mask)

    kernel_erode = np.ones((25,25), np.uint8)
    grass_mask = cv2.erode(grass_mask, kernel_erode) 
    grass_image = apply_mask_to_image(image, grass_mask)
    
    # cv2.imshow('image highlighted shape', grass_image)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    ycoords, xcoords = np.where(grass_mask)
    grass_points = np.vstack([xcoords, ycoords]).transpose()
    grass_point_indices = np.random.choice(len(grass_points), NUM_GRASS_POINTS_PER_IMAGE, replace=False)
    return grass_points[grass_point_indices]
    
def load_images_and_player_contours(frame_sequence_directory):
    '''
    Return a list of images, a list of grass masks, and a list of lists of Contour objects.
    '''
    frame_sequence = FrameSequence.load_frame_sequence(frame_sequence_directory)
    images = [frame.get_image() for frame in frame_sequence.get_frames()]
    grass_masks = [SoccerFieldAnnotator.get_grass_mask(frame.get_annotation()) for frame in frame_sequence.get_frames()]
    player_contours = [frame.get_player_contours() for frame in frame_sequence.get_frames()]
    
    return images, grass_masks, player_contours

def display_points(image, player_points, sideline_points, grass_points):
    '''
    We display the player points in blue and the sideline points in red.
    '''

    image_with_points = image.copy()
    player_color = np.array([255,0,0])
    sideline_color = np.array([0,0,255])
    grass_color = np.array([0,255,0])
    
    image_with_points[player_points[:,1], player_points[:,0]] = player_color
    image_with_points[sideline_points[:,1], sideline_points[:,0]] = sideline_color
    image_with_points[grass_points[:,1], grass_points[:,0]] = grass_color

    cv2.imshow('image with points', image_with_points)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def get_image_patch(image, point):
    half_width = IMAGE_PATCH_SIZE/2

    #if the patch will not be contained in the shape
    if not ( (half_width  <= point[0] < image.shape[1] - half_width) and (half_width  <= point[1] < image.shape[0] - half_width)):
        return None
    
    patch = image[point[1]-half_width : point[1]+half_width, point[0]-half_width : point[0]+half_width]

    assert (np.array(patch.shape[:2]) - np.array([IMAGE_PATCH_SIZE, IMAGE_PATCH_SIZE]) == 0).all()

    return patch.copy()

def generate_image_patches(patch_directory, image, image_index, points):
    
    for i,point in enumerate(points):
        patch = get_image_patch(image, point)
        if patch is None:
            continue
        
        patch_name = 'image_%d_position_[%d_%d].png' % (image_index, point[0], point[1])
        patch_path = os.path.join(patch_directory, patch_name)
        cv2.imwrite(patch_path, patch)
                                  
if __name__ == '__main__':
    images, grass_masks, player_contours_on_images = load_images_and_player_contours(FRAME_SEQUENCE_DIRECTORY)
    highlighted_shapes_on_images = load_highlighted_shapes_on_images(HIGHLIGHTED_SHAPES_DIRECTORY)
    image_indices = range(len(images))
    
    for image_index, image, grass_mask, player_contours_on_image, highlighted_shapes_on_image in zip(image_indices, images, grass_masks, player_contours_on_images, highlighted_shapes_on_images):
        print 'generating patches on image: ', image_index

        #generate player and grass points every other frame
        if image_index % 2 == 0:
            player_points = get_player_points(image, player_contours_on_image, highlighted_shapes_on_image)
            grass_points = get_grass_points(image, grass_mask)
            
            generate_image_patches(NEGATIVE_SET_DIRECTORY, image, image_index, player_points)
            generate_image_patches(GRASS_DIRECTORY, image, image_index, grass_points)
            
        #only generate sidelines and grass points every 4 frames
        if image_index % 4 == 0:
            sideline_points = get_sideline_points(image, highlighted_shapes_on_image)
            #display_points(image, player_points, sideline_points, grass_points)
            
            generate_image_patches(POSITIVE_SET_DIRECTORY, image, image_index, sideline_points)

        
