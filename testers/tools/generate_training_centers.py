import cv2
import numpy as np
import itertools
import os
import re
from ...source.preprocessing import image_loader
from ...source.preprocessing.annotation_generator.annotation_generator import AnnotationGenerator
from ...source.preprocessing.absolute_homography_generator.absolute_homography_generator import AbsoluteHomographyGenerator
from ...source.preprocessing.players_generator.players_generator import PlayersGenerator
from ...source.auxillary.soccer_field_geometry import SoccerFieldGeometry
from ..auxillary import primitive_drawing
from ...source.auxillary import mask_operations
from ...source.auxillary import iterator_operations
from ...source.auxillary import planar_geometry
from ...source.preprocessing import file_sequence_loader

'''
This file is used to generate training data for image annotators.
We output four files:
grass.npy, sidelines.npy, players.npy, background.npy

each file stores a numpy array with shape(~50,000,2), which are positions of pixels with the corresponding annotation.

We generate the annotations as follows:
0) for each frame, load a SoccerAnnotation, an absolute homography matrix, and player contours
1) The grass pixels are generated by projecting a grid from the absolute field onto the image and using all of the pixels that
   are on the grass region of the soccer annotation
2) The sideline pixels are sampled from the sideline region of the soccer annotation
3) The player pixels are sampled from the player contours.
4) The background pixels are generated by intersecting a grid with the background pixels of the soccer annotation
'''

# PREPROCESSING_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts/preprocessing/data'
# IMAGE_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/test_data'
# TRAINING_POINTS_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/training_data/annotation'
# INPUT_DATA_NAME = 'images_50_269'

PREPROCESSING_PARENT_DIRECTORY = '/home/daniel/Dropbox/soccer/daniel_refactored/output/scripts/preprocessing/data'
IMAGE_PARENT_DIRECTORY = '/home/daniel/Dropbox/soccer/daniel_refactored/test_data'
TRAINING_POINTS_PARENT_DIRECTORY = '/home/daniel/soccer/training_data/annotation'
INPUT_DATA_NAME = 'images_50_269'

# PREPROCESSING_PARENT_DIRECTORY = '/home/daniel/soccer/output'
# IMAGE_PARENT_DIRECTORY = '/home/daniel/soccer/images'
# TRAINING_POINTS_PARENT_DIRECTORY = '/home/daniel/soccer/training'
# INPUT_DATA_NAME = 'images-8_15-9_31'

PREPROCESSING_DIRECTORY = os.path.join(PREPROCESSING_PARENT_DIRECTORY, INPUT_DATA_NAME)
#IMAGE_DIRECTORY = os.path.join(IMAGE_PARENT_DIRECTORY, INPUT_DATA_NAME)
IMAGE_DIRECTORY = os.path.join(IMAGE_PARENT_DIRECTORY, 'video4')
TRAINING_POINTS_DIRECTORY = os.path.join(TRAINING_POINTS_PARENT_DIRECTORY, 'data', INPUT_DATA_NAME)
VISUALIZATION_DIRECTORY = os.path.join(TRAINING_POINTS_PARENT_DIRECTORY, 'visualization', INPUT_DATA_NAME)

ANNOTATION_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'annotation')
ABSOLUTE_HOMOGRAPHY_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'absolute_homography')
PLAYERS_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'players')


FRAME_DIRECTORY = 'frame_%d'
FRAME_REGEX = re.compile('frame_([0-9]+)')

GRASS_BASENAME = 'grass.npy'
TIGHT_GRASS_BASENAME = 'tight_grass.npy'
SIDELINE_BASENAME = 'sideline.npy'
PLAYERS_BASENAME = 'players.npy'
BACKGROUND_BASENAME = 'background.npy'

VISUALIZATION_BASENAME = 'image_%d.png'

ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'

NUM_SIDELINE_POINTS_PER_IMAGE = 200

BACKGROUND_GRID_WIDTH = 70 #the number of points on each row of the background grid
BACKGROUND_GRID_LENGTH = 30 #the numer of points on each columns of the background grid

TIGHT_CENTERS_MIN_DISTANCE = 3
TIGHT_CENTERS_MAX_DISTANCE = 6

def save_frame(frame_index, grass_centers, tight_grass_centers, sideline_centers, players_centers, background_centers):
    '''
    grass/sideline/players/background_centers - a numpy array with shape (num pts, 2) and type np.float32

    save the points in TRAINING_POINTS_DIRECTORY/frame_{frame_index}/
    '''
    frame_directory = os.path.join(TRAINING_POINTS_DIRECTORY, FRAME_DIRECTORY % frame_index)
    os.mkdir(frame_directory)

    grass_file_name = os.path.join(frame_directory, GRASS_BASENAME)
    tight_grass_file_name = os.path.join(frame_directory, TIGHT_GRASS_BASENAME)
    sideline_file_name = os.path.join(frame_directory, SIDELINE_BASENAME)
    players_file_name = os.path.join(frame_directory, PLAYERS_BASENAME)
    background_file_name = os.path.join(frame_directory, BACKGROUND_BASENAME)

    np.save(grass_file_name, grass_centers)
    np.save(tight_grass_file_name, tight_grass_centers)
    np.save(sideline_file_name, sideline_centers)
    np.save(players_file_name, players_centers)
    np.save(background_file_name, background_centers)

    return

def build_soccer_field_geometry(absolute_image):
    SCALE = 6
    FIELD_WIDTH = absolute_image.shape[1]-1
    FIELD_LENGTH = absolute_image.shape[0]-1
    GRID_WIDTH = 20*4
    GRID_LENGTH = 14*2
    soccer_field = SoccerFieldGeometry(SCALE, FIELD_WIDTH, FIELD_LENGTH, GRID_WIDTH, GRID_LENGTH)

    absolute_grid_points = build_absolute_grid_points((FIELD_LENGTH,FIELD_WIDTH), (GRID_LENGTH,GRID_WIDTH), (0, 4))
    
    return soccer_field, absolute_grid_points

def build_background_grid_points(num_points_in_row, num_points_in_column, image_shape):
    '''
    num_points_in_row/column - an integer
    image_shape - a length 2 tuple of integers

    return a numpy array of shape (num points, 2). It represents the points on a grid on the image with
    the specified number of points in each row and column.
    '''

    x_ticks = np.linspace(0, image_shape[1]-1, num_points_in_row)
    y_ticks = np.linspace(0, image_shape[0]-1, num_points_in_column)
    
    grid_points = np.dstack(np.meshgrid(x_ticks, y_ticks)).reshape((-1,2))
    
    return grid_points

def build_absolute_grid_points(field_shape, num_points, num_padding_points):
    '''
    field_shape - a tuple (field height, field width)
    num_points - a tuple (num points in col, num points in row)
    num_padding_points - a tuple (num padding points in col, num padding points in row)

    Return a list of grid points.
    '''
    field_shape = np.array(field_shape, np.float64)
    num_points = np.array(num_points, np.float64)
    num_padding_points = np.array(num_padding_points, np.float64)
    
    grid_tile_size = field_shape / (num_points - 1)
    padding_size = num_padding_points * grid_tile_size

    #Note that the store the top left and bottom right as (y,x). I.e, first the y axis then the x axis
    grid_top_left = -padding_size
    grid_bottom_right = field_shape + padding_size

    print 'grid top left: ', grid_top_left
    print 'grid bottom right: ', grid_bottom_right

    x_ticks = np.linspace(grid_top_left[1], grid_bottom_right[1], num_points[1] + 2*num_padding_points[1])
    y_ticks = np.linspace(grid_top_left[0], grid_bottom_right[0], num_points[0] + 2*num_padding_points[0])

    print 'x ticks: ', x_ticks
    print 'y ticks: ', y_ticks
    
    grid_points = np.dstack(np.meshgrid(x_ticks, y_ticks)).reshape((-1,2))
    
    return grid_points
    
def generate_grass_centers(soccer_field, absolute_grid_points, H, annotation):
    '''
    soccer_field - a SoccerFieldGeometry object
    H - a numpy array with shape (3,3) and type np.float32. It represents a homography from the field to the image.
    annotation - a SoccerAnnotation object
    
    return a numpy array with shape (num grass centers, 2)
    '''

    #project the soccer field grid points onto the image
    source_grid_points = absolute_grid_points

    #remove the source points that are on sidelines, but keep the ones that are not even on the field
    thickness=5
    sideline_mask = soccer_field.get_sideline_mask(thickness)
    soccer_field_rectangle = soccer_field.get_field_rectangle()
    in_soccer_field = soccer_field_rectangle.contains_points(source_grid_points)
    source_grid_points_not_sidelines = mask_operations.filter_points(source_grid_points[in_soccer_field], 1.0 - sideline_mask)
    source_grid_points = np.vstack([source_grid_points_not_sidelines, source_grid_points[np.logical_not(in_soccer_field)]])
    
    #after removing the points that are on sidelines, project the remaining points onto the image
    target_grid_points = planar_geometry.apply_homography_to_points(H, source_grid_points)

    #collect the target grid points that are in the grass region of the annotation
    #first filter out the points that are not even on the image
    image_shape = annotation.get_image_shape()
    image_region = np.array([[0,0],[image_shape[1],image_shape[0]]])
    in_image = planar_geometry.points_in_region(target_grid_points, image_region)
    
    target_grid_points = target_grid_points[in_image]

    #then filter out the points that are not on the grass annotation
    grass_mask = annotation.get_grass_mask()
    grass_centers = mask_operations.filter_points(target_grid_points, grass_mask)

    return grass_centers

def generate_tight_grass_centers(annotation):
    '''
    annotation - a SoccerFieldAnnotation
    
    return a numpy array with shape (num points, 2) representing grass points that are close to players and sidelines.
    '''

    grass_mask = annotation.get_grass_mask() > 0
    players_sidelines_mask = np.uint8(np.logical_or(annotation.get_players_mask()>0, annotation.get_sidelines_mask()>0))

    #dilate the players and sideline mask so that we do not include pixels that are too close
    kernel = np.ones((TIGHT_CENTERS_MIN_DISTANCE, TIGHT_CENTERS_MIN_DISTANCE))
    padded_players_sidelines_mask = cv2.dilate(players_sidelines_mask, kernel)

    #dilate it more to get the points that are close to the players and sidelines
    kernel = np.ones((TIGHT_CENTERS_MAX_DISTANCE, TIGHT_CENTERS_MAX_DISTANCE))
    tight_grass_mask = cv2.dilate(players_sidelines_mask, kernel)

    #the remove the padded player and sideline points from the tight grass
    tight_grass_mask = np.logical_and(tight_grass_mask>0, np.logical_not(padded_players_sidelines_mask>0))
    #and make sure that it is contained in the grass
    tight_grass_mask = np.logical_and(tight_grass_mask, grass_mask)
    
    #return the points that are in the tight grass mask
    tight_grass_centers = mask_operations.find_points_in_mask(tight_grass_mask)

    return tight_grass_centers

def generate_sideline_centers(annotation):
    '''
    annotation - a SoccerFieldAnnotation

    return a numpy array with shape (num points, 2) representing the points that are on a sideline.
    '''

    sideline_mask = annotation.get_sidelines_mask()
    sideline_centers = mask_operations.find_points_in_mask(sideline_mask)
    
    return sideline_centers

def generate_player_centers(player_contours):
    '''
    player_contours - a list of Contour objects

    return a numpy array with shape (num points, 2) and type np.float32. It represents the points that are on players.
    '''

    if len(player_contours) == 0:
        return np.array([]).reshape((-1,2))

    player_mask = player_contours[0].get_outer_mask()

    for contour in player_contours[1:]:
        player_mask = mask_operations.get_union(player_mask, contour.get_outer_mask())

    player_centers = mask_operations.find_points_in_mask(player_mask)

    return player_centers

def generate_background_centers(background_grid_points, annotation):
    '''
    annotation - a SoccerAnnotation object.
    background_grid_points - a numpy array with shape (num points, 2)

    return a numpy array with shape (num points, 2) which represents points in the grid
    that are in the background of the image.
    '''
    
    background_mask = annotation.get_background_mask()
    background_centers = mask_operations.filter_points(background_grid_points, background_mask)

    return background_centers

def generate_centers_in_frame(soccer_field, absolute_grid_points, background_grid_points, H, annotation, player_contours):
    '''
    soccer_field - a SoccerFieldGeometry object
    background_grid_points - a numpy array with shape (num points, 2)
    H - a numpy array with shape (3,3) and type np.float32. It represents a homography from the field to the image.
    annotation - a SoccerAnnotation object
    player_contours - a list of Contour objects. They represent contours surrounding players
    '''

    grass_centers = generate_grass_centers(soccer_field, absolute_grid_points, H, annotation)
    tight_grass_centers = generate_tight_grass_centers(annotation)
    sideline_centers = generate_sideline_centers(annotation)
    player_centers = generate_player_centers(player_contours)
    background_centers = generate_background_centers(background_grid_points, annotation)

    return grass_centers, tight_grass_centers, sideline_centers, player_centers, background_centers

def generate_centers_in_interval(interval, soccer_field, absolute_grid_points, background_grid_points,
                                 Hs, images, annotations, player_contours):
    '''
    intervals a tuple (i,j) of integers. They represent the indices of the interval that we are processing.
    soccer_field - a SoccerFieldGeometry object
    background_grid_points - a numpy array with shape (num points, 2)
    Hs - an iterator of numpy arrays with shape (3,3) and type np.float32. 
         They represent a homography from the field to the image.
    images - an iterator of numpy arrays with shape (n,m,3) and type np.uint8
    annotations - an iterator of SoccerAnnotation objects
    player_contours - an iterator of lists of Contour objects. They represent contours surrounding players for each frame

    For each frame in the interval, we generate the image centers and save them
    '''

    for i, (H, image, annotation, player_contours_in_frame) in enumerate(itertools.izip(Hs, images, annotations,
                                                                                        player_contours)):
        absolute_index = interval[0] + i
        print 'generating centers from image: %d...' % absolute_index

        for contour in player_contours_in_frame:
            contour.set_image(image)

        training_points = generate_centers_in_frame(soccer_field, absolute_grid_points, background_grid_points,
                                                    H, annotation, player_contours_in_frame)
        
        grass_pts, tight_grass_pts, sideline_pts, players_pts, background_pts = training_points

        
        # primitive_drawing.draw_points(image, grass_pts, (0,255,0), 1)
        # primitive_drawing.draw_points(image, tight_grass_pts, (0,255,0), 1)
        # primitive_drawing.draw_points(image, sideline_pts, (255,255,255), 1)
        # primitive_drawing.draw_points(image, players_pts, (255,0,0), 1)
        # primitive_drawing.draw_points(image, background_pts, (0,0,255), 1)

        # cv2.imshow('annotated points', image)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        save_frame(absolute_index, grass_pts, tight_grass_pts, sideline_pts, players_pts, background_pts)
        
    return

def generate_training_centers(absolute_image_name, image_directory, absolute_homography_directory, annotation_directory,
                              players_directory):

    #load the input data
    absolute_image = cv2.imread(absolute_image_name, 1)
    soccer_field, absolute_grid_points = build_soccer_field_geometry(absolute_image)
    
    #this is an iterator: image_0,image_1,...
    images = image_loader.load_images(image_directory)

    #read the first image and then put it back
    first_image = next(images)
    images = itertools.chain([first_image], images)

    #build grid of points that we use to search for background points
    background_grid_points = build_background_grid_points(BACKGROUND_GRID_WIDTH, BACKGROUND_GRID_LENGTH,
                                                          first_image.shape[:2])         
    #this is an iterator: annotation_0,annotation_1,...
    annotations = AnnotationGenerator.load_annotations(annotation_directory)
    
    #this is an iterator of tuples of the form: ((ik,jk), (H0,...,H_{jk-ik}))
    indexed_homographies = AbsoluteHomographyGenerator.load_homographies(absolute_homography_directory)

    #this is an iterator of tuples of the form ((ik,jk), ((contours_0,positions_0),...,(contours_{jk-ik},positions_{jk-ik})))
    players = PlayersGenerator.load_players(players_directory)

    #create an indepentant iterator of intervals
    indexed_homographies, iter_copy_1, iter_copy_2 = itertools.tee(indexed_homographies, 3)
    intervals_1 = (interval for interval,Hs in iter_copy_1)
    intervals_2 = (interval for interval,Hs in iter_copy_2)

    #group the images and annotations by interval
    images = iterator_operations.extract_intervals(images, intervals_1)
    annotations = iterator_operations.extract_intervals(annotations, intervals_2)
    

    #build an iterator containing only the player contours, and grouped by interval
    player_contours = ((contours_in_frame for contours_in_frame,positions_in_frame in contours_positions)
                       for interval,contours_positions in players)

    
    #process each of the intervals
    for ((i,j), Hs), images, annotations, player_contours in itertools.izip(indexed_homographies, images, annotations,
                                                                            player_contours):
        generate_centers_in_interval((i,j), soccer_field, absolute_grid_points, background_grid_points,
                                     Hs, images, annotations, player_contours)


def generate_visualizations(training_data_directory, image_directory, visualization_directory):
    '''
    training_data_directory - a directory with subdirectories frame_i1,frame_i1,...
    image_directory - a directory with files image_0.png,image_1.png,...
    visualization_directory - a directory

    save visualizations of the training data in the visualization directory in files image_i1.png,image_i2.png,...
    '''
    images = image_loader.load_images(image_directory)
    
    #load the frame directories
    indexed_frame_directories = file_sequence_loader.get_sorted_file_names(training_data_directory, FRAME_REGEX,
                                                                            with_index=True)

    #get an iterator of the indices alone
    indexed_frame_directories, iter_copy = itertools.tee(indexed_frame_directories, 2)
    frame_indices = (frame_index for frame_index,frame_directory in iter_copy)
    
    #replace the images with the subsequence of images at the given indices
    images = iterator_operations.extract_subsequence(images, frame_indices)
    
    for (frame_index, frame_directory), image in itertools.izip(indexed_frame_directories, images):

        print 'generating visualization for frame %d' % frame_index
        
        grass_file_name = os.path.join(frame_directory, GRASS_BASENAME)
        tight_grass_file_name = os.path.join(frame_directory, TIGHT_GRASS_BASENAME)
        sideline_file_name = os.path.join(frame_directory, SIDELINE_BASENAME)
        players_file_name = os.path.join(frame_directory, PLAYERS_BASENAME)
        background_file_name = os.path.join(frame_directory, BACKGROUND_BASENAME)

        grass_centers = np.load(grass_file_name)
        tight_grass_centers = np.load(tight_grass_file_name)
        sideline_centers = np.load(sideline_file_name)
        players_centers = np.load(players_file_name)
        background_centers = np.load(background_file_name)

        primitive_drawing.draw_points(image, grass_centers, (0,255,0), 1)
        primitive_drawing.draw_points(image, tight_grass_centers, (0,255,0), 1)
        primitive_drawing.draw_points(image, sideline_centers, (255,255,255), 1)
        primitive_drawing.draw_points(image, players_centers, (255,0,0), 1)
        primitive_drawing.draw_points(image, background_centers, (0,0,255), 1)

        #save the image in the visualization directory
        visualization_file_name = os.path.join(visualization_directory, VISUALIZATION_BASENAME % frame_index)
        cv2.imwrite(visualization_file_name, image)
        
if __name__ == '__main__':
    generate_training_centers(ABSOLUTE_FILE_NAME, IMAGE_DIRECTORY, ABSOLUTE_HOMOGRAPHY_DIRECTORY, ANNOTATION_DIRECTORY,
                             PLAYERS_DIRECTORY)

    generate_visualizations(TRAINING_POINTS_DIRECTORY, IMAGE_DIRECTORY, VISUALIZATION_DIRECTORY)
