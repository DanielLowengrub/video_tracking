import os
import re
import sys

INPUT_REGEX = re.compile('image_([0-9]+)\.png')
INPUT_BASENAME = 'image_%d.png'
FIRST_INPUT_INDEX = 1

def format_image_indices(image_directory):
    '''
    image_directory - a directory with files of the form image_n.png where n is an integer.
       we assume that n goes from 1 to num_images
    rename the files so that they have the format: image_%d0M.png where M is the maximum
    number of digits in the numbers n.
    '''

    filenames = [os.path.join(image_directory, f)
                 for f in os.listdir(image_directory) if INPUT_REGEX.match(f) is not None]
    
    print '\n'.join(filenames[:10])
    max_digits = max(len(INPUT_REGEX.match(os.path.basename(f)).group(1))
                     for f in filenames if os.path.isfile(f))

    print 'max digits: %d' % max_digits
    
    formatted_basename = ''.join(['image_%0', str(max_digits), 'd.png'])
    
    for i in range(FIRST_INPUT_INDEX,len(filenames)+FIRST_INPUT_INDEX):
        #rename image_i.png to image_0..0{i-1}.png
        input_filename     = os.path.join(image_directory, INPUT_BASENAME  % i)
        formatted_filename = os.path.join(image_directory, formatted_basename % (i-FIRST_INPUT_INDEX))
        print 'renaming %s to %s' % (input_filename, formatted_filename)
        os.rename(input_filename, formatted_filename)

    return

if __name__ == '__main__':
    image_directory = sys.argv[1]
    format_image_indices(image_directory)
