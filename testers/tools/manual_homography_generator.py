import numpy as np

def compute_camera_matrix(R, P, f):
    '''
    R - the 3x3 rotation matrix from the world coordinates to the camera coordinates
    P - the position of the camera in the world
    f - in camera coordinates, the objects are projected onto the z=f plane. AKA the focus.
    '''
    h = input_image.shape[0]
    w = input_image.shape[1]

    # calibration matrix
    C = np.array([[f, 0, 0],
                  [0, f, 0],
                  [0, 0, 1]])

    # transformation matrix from projective world coordinates to camera coordinates
    R_RP = np.hstack([R, np.dot(R,-P.reshape((-1,1)))])

    #the camera matrix from world coordinates to image coordinates
    camera_matrix = np.dot(C, R_RP)

    return camera_matrix

def compute_homography(R, P, f):
    '''
    R - the 3x3 rotation matrix from the world coordinates to the camera coordinates
    P - the position of the camera in the world
    f - in camera coordinates, the objects are projected onto the z=f plane. AKA the focus.
    '''
    camera_matrix = compute_camera_matrix

    #for the homography matrix, remove the third column
    homography_matrix = camera_matrix[:,(0,1,3)]

    return homography_matrix
