import numpy as np
import cv2
import matplotlib.pyplot as plt

from ...source.auxillary.mask_operations import apply_mask_to_image, mix_images_with_mask, convert_to_image, convert_to_cv_mask

########################
# This script is used to debug the template matching part of the player tracker.
#
# It loads an image, and obstruction mask, a texture map and a foreground mask.
# It then allows the user to place the texture map at various position on the image and view the quality of the match.
########################

'''
IMAGE = cv2.imread('test_data/video3/out67.png', 1)
OBSTRUCTION_MASK = np.float32(cv2.imread('output/tester/player_tracker/general_data_dump/player_6/obstruction_mask_017.png', 0)) / 255.0

TEXTURE_MAP = cv2.imread('output/tester/player_tracker/general_data_dump/player_6/texture_map_016.png', 1)
FOREGROUND_MASK = np.float32(cv2.imread('output/tester/player_tracker/general_data_dump/player_6/fg_mask_016.png', 0)) / 255.0

OUTPUT_IMAGE_NAME_PREFIX = 'output/tester/template_matching_viewer/general_stats_player_6_frame_67-'

POSITION_ON_IMAGE = np.array([ 278, 194])
#POSITION_ON_IMAGE = np.array([ 262, 190])
#POSITION_ON_IMAGE = np.array([ 268, 182])
#POSITION_ON_IMAGE = np.array([ 259, 188])
#POSITION_ON_IMAGE = np.array([ 265, 164])
#POSITION_ON_IMAGE = np.array([ 265, 198])
'''

'''
IMAGE = cv2.imread('test_data/video3/out56.png', 1)
OBSTRUCTION_MASK = np.float32(cv2.imread('output/tester/player_tracker/general_data_dump/player_6/obstruction_mask_006.png', 0)) / 255.0

TEXTURE_MAP = cv2.imread('output/tester/player_tracker/general_data_dump/player_6/texture_map_005.png', 1)
FOREGROUND_MASK = np.float32(cv2.imread('output/tester/player_tracker/general_data_dump/player_6/fg_mask_005.png', 0)) / 255.0

OUTPUT_IMAGE_NAME_PREFIX = 'output/tester/template_matching_viewer/general_stats_player_6_frame_56-'

#POSITION_ON_IMAGE = np.array([ 267, 185])
POSITION_ON_IMAGE = np.array([ 264, 183])
'''

'''
IMAGE = cv2.imread('test_data/video3/out70.png', 1)
OBSTRUCTION_MASK = np.float32(cv2.imread('output/tester/player_tracker/general_data_dump/player_6/obstruction_mask_020.png', 0)) / 255.0

TEXTURE_MAP = cv2.imread('output/tester/player_tracker/general_data_dump/player_6/texture_map_019.png', 1)
FOREGROUND_MASK = np.float32(cv2.imread('output/tester/player_tracker/general_data_dump/player_6/fg_mask_019.png', 0)) / 255.0

OUTPUT_IMAGE_NAME_PREFIX = 'output/tester/template_matching_viewer/general_stats_player_6_frame_70-'

#POSITION_ON_IMAGE = np.array([ 283, 198])
POSITION_ON_IMAGE = np.array([ 268, 200])
'''

'''
IMAGE = cv2.imread('test_data/video3/out62.png', 1)
OBSTRUCTION_MASK = np.float32(cv2.imread('output/tester/player_tracker/general_data_dump/player_6/obstruction_mask_012.png', 0)) / 255.0

TEXTURE_MAP = cv2.imread('output/tester/player_tracker/general_data_dump/player_6/texture_map_011.png', 1)
FOREGROUND_MASK = np.float32(cv2.imread('output/tester/player_tracker/general_data_dump/player_6/fg_mask_011.png', 0)) / 255.0

OUTPUT_IMAGE_NAME_PREFIX = 'output/tester/template_matching_viewer/general_stats_player_6_frame_62-'

#POSITION_ON_IMAGE = np.array([ 272, 193])
POSITION_ON_IMAGE = np.array([ 276, 203])
'''

'''
IMAGE = cv2.imread('test_data/video3/out94.png', 1)
OBSTRUCTION_MASK = np.float32(cv2.imread('output/tester/player_tracker/combined_output_2/general_data_dump/player_5/obstruction_mask_044.png', 0)) / 255.0

TEXTURE_MAP = cv2.imread('output/tester/player_tracker/combined_output_2/general_data_dump/player_5/texture_map_043.png', 1)
FOREGROUND_MASK = np.float32(cv2.imread('output/tester/player_tracker/combined_output_2/general_data_dump/player_5/fg_mask_043.png', 0)) / 255.0
HISTOGRAM = np.load('output/tester/player_tracker/combined_output_2/general_data_dump/player_5/histogram_001.npy')

OUTPUT_IMAGE_NAME_PREFIX = 'output/tester/template_matching_viewer/general_stats_player_5_frame_94-'

POSITION_ON_IMAGE = np.array([ 257, 211])
#POSITION_ON_IMAGE = np.array([ 258, 202])
'''

'''
IMAGE = cv2.imread('test_data/video3/out67.png', 1)
OBSTRUCTION_MASK = np.float32(cv2.imread('output/tester/player_tracker/combined_output_4/general_data_dump/player_8/obstruction_mask_017.png', 0)) / 255.0

TEXTURE_MAP = cv2.imread('output/tester/player_tracker/combined_output_4/general_data_dump/player_8/texture_map_016.png', 1)
FOREGROUND_MASK = np.float32(cv2.imread('output/tester/player_tracker/combined_output_4/general_data_dump/player_8/fg_mask_016.png', 0)) / 255.0
HISTOGRAM = np.load('output/tester/player_tracker/combined_output_4/general_data_dump/player_8/histogram_016.npy')

OUTPUT_IMAGE_NAME_PREFIX = 'output/tester/template_matching_viewer/general_stats_player_8_frame_17-'

#POSITION_ON_IMAGE = np.array([ 240, 209])
POSITION_ON_IMAGE = np.array([ 248, 194])
'''

IMAGE = cv2.imread('test_data/video3/out166.png', 1)
OBSTRUCTION_MASK = np.float32(cv2.imread('output/tester/player_tracker/combined_output_5/general_data_dump/player_8/obstruction_mask_116.png', 0)) / 255.0

TEXTURE_MAP = cv2.imread('output/tester/player_tracker/combined_output_5/general_data_dump/player_8/texture_map_115.png', 1)
FOREGROUND_MASK = np.float32(cv2.imread('output/tester/player_tracker/combined_output_5/general_data_dump/player_8/fg_mask_115.png', 0)) / 255.0
HISTOGRAM = np.load('output/tester/player_tracker/combined_output_5/general_data_dump/player_8/histogram_016.npy')

OUTPUT_IMAGE_NAME_PREFIX = 'output/tester/template_matching_viewer/general_stats_player_8_frame_116-'

POSITION_ON_IMAGE = np.array([ 199, 175])
#POSITION_ON_IMAGE = np.array([ 254, 211])

MAX_NORM = np.linalg.norm(np.array([255,255,255]))
MIN_MASK_VALUE = 0.2
OBSTRUCTION_PENALTY = 0.9

def compute_chi_squared(hist_A, hist_B):

    new_hist_A = hist_A[ (hist_A + hist_B) > 0]
    new_hist_B = hist_B[ (hist_A + hist_B) > 0]

    return 0.5 * ( (new_hist_A - new_hist_B)**2 / (new_hist_A + new_hist_B) ).sum()

def display_histograms_side_by_side(hist_A, hist_B, title_A, title_B):
    '''
    hist_A, hist_B - numpy arrays with shape (num bins, num bins) of type np.float32. These correspond to 2D histograms.
    title_A, title_B - the names of the histograms
    We display the histograms side by side.
    '''
    fig = plt.figure()

    ax = fig.add_subplot(121)
    p = ax.imshow(hist_A, interpolation="nearest")
    ax.set_title(title_A)
    plt.colorbar(p)

    ax = fig.add_subplot(122)
    p = ax.imshow(hist_B, interpolation="nearest")
    ax.set_title(title_B)
    plt.colorbar(p)

    plt.show()
    
def compute_histogram(image, mask=None):
    '''
    image - a numpy array with shape (n,m,3) and type uint8
    mask - a numpy array with shape (n,m) and type np.float32

    We compute the 2D histogram of the masked part of the image using the HS channels.
    '''

    if mask is None:
        mask = np.ones(image_A.shape[:2])
        
    mask = mask.copy()
    mask[mask<MIN_MASK_VALUE] = 0.0
    mask[mask >= MIN_MASK_VALUE] = 1.0
    cv_mask = convert_to_cv_mask(mask)

    #convert the images to HSV space
    image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

    #compute the histogram using the HS channels
    hue_range = [0, 180]
    saturation_range = [0,256]
    nbins = [10, 10]
    
    hist = cv2.calcHist([image], [0,1], cv_mask, nbins, hue_range + saturation_range)

    hist /= np.linalg.norm(hist)
    
    return hist

def compute_histogram_distance(hist_A, hist_B):
    '''
    compute the distance between the two histograms
    '''

    print 'the distance between the histograms is: ', np.linalg.norm(hist_A - hist_B)
    #print 'the chi squared distance between the images is: ', cv2.compareHist(hist_A, hist_B, cv2.cv.CV_COMP_CHISQR)
    print 'we computed a chi squared of: ', compute_chi_squared(hist_A, hist_B)
    #print 'the hellinger distance between the images is: ', cv2.compareHist(hist_A, hist_B, cv2.cv.CV_COMP_BHATTACHARYYA)

    return np.linalg.norm(hist_A - hist_B)

def get_cropped_image_and_obstruction_mask(image, obstruction_mask, position_on_image, texture_map):
    '''
    Cut out the portion of the image and obstruction mask determined by the position on the image and the texture map.
    The position on the image corresponds to the middle of the bottom part of the texture map
    '''

    height = texture_map.shape[0]
    width = texture_map.shape[1]

    top_left_corner = position_on_image + np.array([ -width / 2, -height])
    bottom_right_corner = position_on_image + np.array([width / 2, 0])

    cropped_image = image[ top_left_corner[1]:bottom_right_corner[1], top_left_corner[0]:bottom_right_corner[0], : ]
    cropped_obstruction_mask = obstruction_mask[ top_left_corner[1]:bottom_right_corner[1], top_left_corner[0]:bottom_right_corner[0]]

    return cropped_image, cropped_obstruction_mask


#NOTE: Try the correlation of hue / saturation. Also do the display using each channel of HSV, BGR seperately
def compute_correlation(image_A, image_B, mask_A=None, mask_B=None):
    '''
    both images are numpy arrays of shape (n,m,3) and type np.uint8.
    We return a numpy array of length 3, with values given by the correlation of the images in each of the channels
    '''
    
    image_A = np.float32(image_A)
    image_B = np.float32(image_B)

    if mask_A is None:
        mask_A = np.ones(image_A.shape[:2], np.float64)

    if mask_B is None:
        mask_B = np.ones(image_B.shape[:2], np.float64)

    values_A = image_A[mask_A > MIN_MASK_VALUE]
    values_B = image_B[mask_B > MIN_MASK_VALUE]

    print 'shape of values A: ', values_A.shape
    print 'shape of values B: ', values_B.shape

    print 'std of image A: ', values_A.std(axis=0)
    print 'std of image B: ', values_B.std(axis=0)
    
    return (((values_A - values_A.mean(axis=0)) * (values_B - values_B.mean(axis=0)))).mean(axis=0) / (values_A.std(axis=0) * values_B.std(axis=0))

def display_basic_stats(cropped_image, cropped_obstruction_mask, texture_map, foreground_mask):
    '''
    Print basic statistics regarding the texture map relative to the cropped image.
    '''

    print 'cropped image shape: ', cropped_image.shape
    print 'cropped_obstruction_mask shape: ', cropped_obstruction_mask.shape
    print 'texture map shape: ', texture_map.shape
    print 'foreground mask.shape: ', foreground_mask.shape
    print 'area of foreground mask: ', foreground_mask.sum()
    print 'area of cropped obstruction: ', cropped_obstruction_mask.sum()
    print 'area of unobstructed foreground: ', (foreground_mask * (1 - cropped_obstruction_mask)).sum()
    
    vertical_divider = np.zeros( (texture_map.shape[0], 10, 3) , np.uint8)
    vertical_divider[:,:,0] = 255
    horizontal_divider = np.zeros( (15, 2*texture_map.shape[1] + vertical_divider.shape[1], 3), np.uint8 )
    horizontal_divider[:,:,0] = 255
    
    unobstructed_foreground_mask = foreground_mask * (1 - cropped_obstruction_mask)
    unobstructed_foreground_mask[unobstructed_foreground_mask<MIN_MASK_VALUE] = 0.0
    unobstructed_foreground_mask[unobstructed_foreground_mask>=MIN_MASK_VALUE] = 1.0
    image_on_unobstructed_foreground = apply_mask_to_image(cropped_image, unobstructed_foreground_mask)
    texture_map_on_unobstructed_foreground = apply_mask_to_image(texture_map, unobstructed_foreground_mask)
    norms_of_difference = np.linalg.norm(np.int32(texture_map) - np.int32(cropped_image), axis=2)
    difference = np.uint8(np.abs(np.int32(texture_map) - np.int32(cropped_image)))

    image_and_texture_map = np.hstack([cropped_image, vertical_divider, texture_map])
    difference_image = np.hstack([difference, vertical_divider, difference])
    obstruction_and_foreground_masks = np.hstack([convert_to_image(cropped_obstruction_mask), vertical_divider, convert_to_image(foreground_mask)])
    unobstructed_foreground_masks = np.hstack([convert_to_image(unobstructed_foreground_mask), vertical_divider, convert_to_image(unobstructed_foreground_mask)])
    image_and_texture_map_on_unobstructed_foreground = np.hstack([image_on_unobstructed_foreground, vertical_divider, texture_map_on_unobstructed_foreground])
    norms_of_difference_image = norms_of_difference / MAX_NORM
    norms_of_difference_image = np.hstack([convert_to_image(norms_of_difference_image), vertical_divider, convert_to_image(norms_of_difference_image)])

    stats_image = np.vstack([image_and_texture_map, horizontal_divider,
                             difference_image, horizontal_divider,
                             obstruction_and_foreground_masks, horizontal_divider,
                             unobstructed_foreground_masks, horizontal_divider,
                             image_and_texture_map_on_unobstructed_foreground, horizontal_divider,
                             norms_of_difference_image])
    
    cv2.imshow('stats: img | texmap', stats_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    
    unobstructed_foreground_area = unobstructed_foreground_mask.sum()
    evaluation_on_unobstructed_foreground = (norms_of_difference * unobstructed_foreground_mask).sum() / unobstructed_foreground_area

    print 'area of foreground: ', foreground_mask.sum()
    print 'area of obstructed region: ', cropped_obstruction_mask.sum()
    print 'sum of norms on unobstructed foreground: ', (norms_of_difference * unobstructed_foreground_mask).sum()
    print 'area of unobstructed foreground: ', unobstructed_foreground_area
    print 'average norm of differences on unobstructed foreground: ', evaluation_on_unobstructed_foreground

    cropped_image_hsv = cv2.cvtColor(cropped_image, cv2.COLOR_BGR2HSV)
    texture_map_hsv = cv2.cvtColor(texture_map, cv2.COLOR_BGR2HSV)
    cropped_image_color_and_hsv = np.concatenate((cropped_image, cropped_image_hsv), axis=2)
    texture_map_color_and_hsv = np.concatenate((texture_map, texture_map_hsv), axis=2)
    print 'the correlation between the images (b, g, r, h, s, v): ', compute_correlation(cropped_image_color_and_hsv,
                                                                                         texture_map_color_and_hsv,
                                                                                         unobstructed_foreground_mask, unobstructed_foreground_mask)
    return stats_image

def basic_viewer(image, obstruction_mask, texture_map, foreground_mask, position_on_image, histogram=None):
    marked_image = image.copy()
    cv2.circle(marked_image, tuple(np.int32(position_on_image)), 5, (255, 255, 255), -1)
    cv2.imshow('marked image', marked_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    print obstruction_mask.shape
    print obstruction_mask.dtype
    
    cv2.imshow('obstruction mask', obstruction_mask)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    cv2.imshow('texture_map', texture_map)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    cv2.imshow('fg mask', foreground_mask)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    print 'evaluating position: ', position_on_image

    cropped_image, cropped_obstruction_mask = get_cropped_image_and_obstruction_mask(image, obstruction_mask, position_on_image, texture_map)
    stats_image = display_basic_stats(cropped_image, cropped_obstruction_mask, texture_map, foreground_mask)

    #compare the histograms
    unobstructed_foreground_mask = foreground_mask * (1 - cropped_obstruction_mask)

    #if we were not given a histogram, compute it from the texture map
    if histogram is None:
        histogram = compute_histogram(texture_map, unobstructed_foreground_mask)
        
    cropped_image_histogram = compute_histogram(cropped_image, unobstructed_foreground_mask)
    histogram_distance = compute_histogram_distance(histogram, cropped_image_histogram)

    fg_mask = foreground_mask.copy()
    obs_mask = cropped_obstruction_mask.copy()
    fg_mask[fg_mask < MIN_MASK_VALUE] = 0.0
    fg_mask[fg_mask >= MIN_MASK_VALUE] = 1.0
    obs_mask[obs_mask < MIN_MASK_VALUE] = 0.0
    obs_mask[obs_mask >= MIN_MASK_VALUE] = 1.0
    
    unobstructed_foreground_area = (fg_mask * (1 - obs_mask)).sum()
    foreground_area = fg_mask.sum()
    alpha = unobstructed_foreground_area / foreground_area

    print 'foreground area: ', foreground_area
    print 'unobstructed_foreground_area: ', unobstructed_foreground_area
    print 'weighing the histogram distance with alpha = ', alpha
    weighted_histogram_distance = (alpha * histogram_distance) + ( (1-alpha) * OBSTRUCTION_PENALTY )
    print 'weighted histogram distance: ', weighted_histogram_distance

    #display the two histograms
    display_histograms_side_by_side(histogram, cropped_image_histogram, 'model histogram', 'image histogram')
    
    #we also save the stats image
    pos = np.int32(position_on_image)
    image_name = '%s[%d,%d].png' % (OUTPUT_IMAGE_NAME_PREFIX, pos[0], pos[1])
    cv2.imwrite(image_name, stats_image)

    
if __name__ == '__main__':
    basic_viewer(IMAGE, OBSTRUCTION_MASK, TEXTURE_MAP, FOREGROUND_MASK, POSITION_ON_IMAGE, HISTOGRAM)
