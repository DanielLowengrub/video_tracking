import os
from ...source.data_structures.frame_sequence.frame_sequence import FrameSequence
from ...source.auxillary.fit_points_to_shape.ellipse import Ellipse
from ...source.auxillary.camera_matrix import CameraMatrix
import numpy as np
import cv2

######################
# This file has two main goals.
#
# The first one is to provide a convenient way to view 3D homography.
# We do this by projecting lines and ellipses from 3D to the image.
#
# The second goal is to be a testing ground for computing the 3D homography given the 2D one, and a list of head positions.
# To do this, we load a frame sequence, together with player positions, and head positions.
######################

#PARENT_DIR = 'output/tester/frame_sequences/frame_sequence_test_3'
#FRAME_SEQUENCE_NAME = 'frame_sequence_50_79-0_29'
#FRAME_SEQUENCE_DIR = os.path.join(PARENT_DIR, 'frame_sequences_50_79-0_29')
#PLAYER_DATA_DIR = os.path.join('output/tester/player_tracker/player_positions', FRAME_SEQUENCE_NAME)
#OUTPUT_DIR = os.path.join('output/tester/frame_sequence_ellipsoid_projections', FRAME_SEQUENCE_NAME)  

# PARENT_DIR = 'output/tester/frame_sequences/frame_sequence_test_7'
# FRAME_SEQUENCE_NAME = 'frame_sequence_50_199-0_149'
# FRAME_SEQUENCE_DIR = os.path.join(PARENT_DIR, 'frame_sequences_50_199-0_149')
# PLAYER_DATA_DIR = os.path.join('output/tester/player_tracker/player_positions', FRAME_SEQUENCE_NAME)
# OUTPUT_DIR = os.path.join('output/tester/frame_sequence_ellipsoid_projections', FRAME_SEQUENCE_NAME)  

PARENT_DIR = 'output/tester/frame_sequences/frame_sequence_test_8'
FRAME_SEQUENCE_NAME = 'frame_sequence_50_269-0_219'
FRAME_SEQUENCE_DIR = os.path.join(PARENT_DIR, 'frame_sequences_50_269-0_219')
PLAYER_DATA_DIR = os.path.join('output/tester/player_tracker/player_positions', FRAME_SEQUENCE_NAME)
OUTPUT_DIR = os.path.join('output/tester/frame_sequence_ellipsoid_projections', FRAME_SEQUENCE_NAME)

PLAYER_POSITIONS_FILE = os.path.join(PLAYER_DATA_DIR, 'player_positions.npy')
HEAD_IMAGE_POSITIONS_FILE = os.path.join(PLAYER_DATA_DIR, 'head_image_positions.npy')

FRAME_INDEX = 20

PLAYER_HEIGHT = 13 #the players height in the coordinate system of the field relative to the size of the field. (i.e, in pixel units with respect to the image of the field)
PLAYER_WIDTH = 4

def load_image_and_homography_from_abs(frame_index):
    '''
    Load a frame sequence and return the homography from absolute coordinates to image coordinates at the given frame.
    '''
    
    frame_sequence = FrameSequence.load_frame_sequence(FRAME_SEQUENCE_DIR, load_homography_to_abs=True)
    frame = frame_sequence.get_frames()[frame_index]
    transformation_to_abs = frame.get_homography_to_abs()
    image = frame.get_image()
    
    return image, np.linalg.inv(transformation_to_abs)

def load_player_absolute_positions(frame_index=None):
    '''
    Return a list containing the positions in absolute coordinates at the given frame of the player with ids PLAYER_IDS
    '''
    absolute_positions = np.load(PLAYER_POSITIONS_FILE)

    if frame_index is None:
        return absolute_positions

    else:
        return absolute_positions[:,frame_index,:]

def load_head_image_positions(frame_index=None):
    '''
    Return a list containing the head positions in image coordinates at the given frame of the player with ids PLAYER_IDS
    '''
    head_image_positions = np.load(HEAD_IMAGE_POSITIONS_FILE)

    if frame_index is None:
        return head_image_positions

    else:
        return head_image_positions[:,frame_index,:]

#def get_head_world_positions(player_absolute_positions):
#    heights = np.ones(len(player_absolute_positions), np.float32) * PLAYER_HEIGHT
#    return np.hstack([player_absolute_positions, heights.reshape(-1,1)])

def get_head_world_positions(player_absolute_positions):
    '''
    player_absolute_positions - a numpy array of shape (num players, num frames, 2)

    We return a numpy array of shape (num players, num frames, 3) by changing each 2d position (x,y) to the 3d position
    (x,y,PLAYER_HEIGHT)
    '''
    heights = np.ones(player_absolute_positions.shape[:2], np.float32) * PLAYER_HEIGHT
    heights = heights.reshape((heights.shape[0], heights.shape[1], 1))
    return np.concatenate([player_absolute_positions, heights], axis=2)

def get_ellipsoid_world_positions(player_absolute_positions):
    '''
    player_absolute_positions - a numpy array of shape (num players, num frames, 2)

    We return a numpy array of shape (num players, num frames, 3) by changing each 2d position (x,y) to the 3d position
    (x,y,PLAYER_HEIGHT)
    '''
    heights = np.ones(player_absolute_positions.shape[:2], np.float32) * (PLAYER_HEIGHT / 2.0)
    heights = heights.reshape((heights.shape[0], heights.shape[1], 1))
    return np.concatenate([player_absolute_positions, heights], axis=2)


def test_ellipsoid_projection(image, player_absolute_positions, camera_matrix):
    ellipsoid_heights = np.ones(len(player_absolute_positions), np.float32) * (PLAYER_HEIGHT / 2.0)
    ellipsoid_positions = np.hstack([player_absolute_positions, ellipsoid_heights.reshape(-1,1)])
    axes = np.array([PLAYER_WIDTH / 2.0, PLAYER_WIDTH / 2.0, PLAYER_HEIGHT / 2.0], np.float32)
    player_ellipses = [camera_matrix.project_ellipsoid_to_image(position, axes) for position in ellipsoid_positions]
    
    for ellipse in player_ellipses:
        ellipse.compute_geometric_parameters()

        color = (255, 0, 0)
        center, axes, angle = ellipse.get_geometric_parameters(in_degrees=True)
        cv2.ellipse(image, tuple(np.int32(center).tolist()), tuple(np.int32(axes).tolist()), angle, 0, 360, color, 1, cv2.CV_AA)
        
    cv2.imshow('the ellipse', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def compute_camera_matrix_in_frame():
    '''
    Load a frame_sequence and player positions and use this to compute the camera matrix for a specific frame.
    '''
    image, homography_from_abs = load_image_and_homography_from_abs(FRAME_INDEX)
    player_absolute_positions = load_player_absolute_positions(FRAME_INDEX)
    head_image_positions = load_head_image_positions(FRAME_INDEX)

    head_not_nan_indices = np.logical_not(np.isnan(head_image_positions.sum(axis=1)))
    player_absolute_positions_not_nan = player_absolute_positions[head_not_nan_indices]
    head_image_positions_not_nan = head_image_positions[head_not_nan_indices]
    head_world_positions_not_nan = get_head_world_positions(player_absolute_positions_not_nan.reshape((-1,1,2))).reshape((-1,3))

    print 'computing camera matrix...'
    camera_matrix = CameraMatrix.from_planar_homography(homography_from_abs, head_world_positions_not_nan, head_image_positions_not_nan)

    print 'projecting player ellipsoids...'
    player_not_nan_indices = np.logical_not(np.isnan(player_absolute_positions.sum(axis=1)))
    test_ellipsoid_projection(image, player_absolute_positions[player_not_nan_indices].reshape(-1,2), camera_matrix)

def draw_ellipsoids_on_frames():
    '''
    Load a frame sequence which is assumed to already have camera matrices.
    Load player positions, use the camera matrices to project the corresponding ellipsoids, 
    and draw the ellipses we get on the frame images.
    '''

    frame_sequence = FrameSequence.load_frame_sequence(FRAME_SEQUENCE_DIR, load_homography_to_abs=True, load_camera_matrix=True)
    player_absolute_positions = load_player_absolute_positions()
    ellipsoid_world_positions = get_ellipsoid_world_positions(player_absolute_positions)
    ellipsoid_axes = np.array([PLAYER_WIDTH/2.0, PLAYER_WIDTH/2.0, PLAYER_HEIGHT/2.0], np.float32)
    ellipse_color = (255, 0, 0)
            
    for frame_index, frame in enumerate(frame_sequence.get_frames()):
        output_image = frame.get_image().copy()
        output_image_path = os.path.join(OUTPUT_DIR, ('img_%3d.png' % frame.get_index()).replace(' ','0'))
        camera_matrix = frame.get_camera_matrix()

        if np.isnan(camera_matrix.get_numpy_matrix().sum()):
            cv2.imwrite(output_image_path, output_image)
            continue
        
        ellipsoid_world_positions_in_frame = ellipsoid_world_positions[:,frame_index,:]
        not_nan_mask = np.logical_not(np.isnan(ellipsoid_world_positions_in_frame.sum(axis=1)))
        ellipsoid_world_positions_not_nan = ellipsoid_world_positions_in_frame[not_nan_mask]

        #now project the ellipsoids onto the image
        for ellipsoid_position in ellipsoid_world_positions_not_nan:
            ellipse = camera_matrix.project_ellipsoid_to_image(ellipsoid_position, ellipsoid_axes)
            ellipse.compute_geometric_parameters()
            center, axes, angle = ellipse.get_geometric_parameters(in_degrees=True)
            print 'center: ', center
            print 'axes: ', axes
            print 'angle: ', angle
            
            cv2.ellipse(output_image, tuple(np.int32(center).tolist()), tuple(np.int32(axes).tolist()), angle, 0, 360, ellipse_color, 1, cv2.CV_AA)

        #now save the image
        print 'writing to: ', output_image_path
        cv2.imwrite(output_image_path, output_image)

    return

if __name__ == '__main__':
    #compute_camera_matrix_in_frame()
    draw_ellipsoids_on_frames()
