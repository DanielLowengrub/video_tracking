import os
import numpy as np
import cv2
from ...source.auxillary.camera_matrix import CameraMatrix
from ...source.data_structures.frame_sequence.frame_sequence import FrameSequence

##############################################################
# Here we test some of the methods in the camera matrix class.
##############################################################

PARENT_DIR = 'output/tester/frame_sequences/frame_sequence_test_7'
FRAME_SEQUENCE_NAME = 'frame_sequence_50_199-0_149'
FRAME_SEQUENCE_DIR = os.path.join(PARENT_DIR, 'frame_sequences_50_199-0_149')
PLAYER_DATA_DIR = os.path.join('output/tester/player_tracker/player_positions', FRAME_SEQUENCE_NAME)

PLAYER_POSITIONS_FILE = os.path.join(PLAYER_DATA_DIR, 'player_positions.npy')

FRAME_INDEX = 1
PLAYER_INDEX = 6
PLAYER_VARIANCE = 5

def load_player_absolute_positions(frame_index=None):
    '''
    Return a list containing the positions in absolute coordinates at the given frame of the player with ids PLAYER_IDS
    '''
    absolute_positions = np.load(PLAYER_POSITIONS_FILE)

    if frame_index is None:
        return absolute_positions

    else:
        return absolute_positions[:,frame_index,:]

def basic_project_ellipse_test():
    '''
    We use a camera that is located at the origin and pointing straight up, and rotated around the z axis by 90 degrees.
    We project an ellipse that is located on the z=2 plane.
    '''

    camera_matrix = np.zeros((3,4), np.float32)
    camera_matrix[1,0] = camera_matrix[0,1] = camera_matrix[2,2] = 1
    camera_matrix = CameraMatrix(camera_matrix)
    
    world_position = np.array([0,0,2], np.float32)
    xy_axes = np.array([1,2], np.float32)

    image_ellipse = camera_matrix.project_ellipse_to_image(world_position, xy_axes)
    image_ellipse.compute_geometric_parameters()
    center, axes, angle = image_ellipse.get_geometric_parameters(in_degrees=True)

    print 'the projected ellipse has parameters:'
    print 'center: ', center
    print 'axes: ', axes
    print 'angle: ', angle

def frame_sequence_project_ellipse_test():
    '''
    We load a frame from a frame sequence and project an ellipse centered on a player.
    '''

    frame_sequence = FrameSequence.load_frame_sequence(FRAME_SEQUENCE_DIR, load_homography_to_abs=True, load_camera_matrix=True)
    player_absolute_position = load_player_absolute_positions(frame_index=FRAME_INDEX)[PLAYER_INDEX]
    frame = frame_sequence.get_frames()[FRAME_INDEX]
    camera_matrix = frame.get_camera_matrix()
    image = frame.get_image()
    
    #We now project one ellipse centered around the player's feet, and another centered around the head
    bottom_ellipse_world_position = np.append(player_absolute_position, 0)
    bottom_xy_axes = np.array([PLAYER_VARIANCE, PLAYER_VARIANCE], np.float32)

    top_ellipse_world_position = np.append(player_absolute_position, 13)
    top_xy_axes = np.array([PLAYER_VARIANCE, PLAYER_VARIANCE], np.float32)
    
    bottom_image_ellipse = camera_matrix.project_ellipse_to_image(bottom_ellipse_world_position, bottom_xy_axes)
    top_image_ellipse = camera_matrix.project_ellipse_to_image(top_ellipse_world_position, top_xy_axes)

    bottom_image_ellipse.compute_geometric_parameters()
    top_image_ellipse.compute_geometric_parameters()

    center, axes, angle = bottom_image_ellipse.get_geometric_parameters(in_degrees=True)
    cv2.ellipse(image, tuple(np.int32(center).tolist()), tuple(np.int32(axes).tolist()), angle, 0, 360, (255,0,0), 1, cv2.CV_AA)
    center, axes, angle = top_image_ellipse.get_geometric_parameters(in_degrees=True)
    cv2.ellipse(image, tuple(np.int32(center).tolist()), tuple(np.int32(axes).tolist()), angle, 0, 360, (255,0,0), 1, cv2.CV_AA)
    
    cv2.imshow('projected ellipse', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def get_camera_position_test():
    frame_sequence = FrameSequence.load_frame_sequence(FRAME_SEQUENCE_DIR, load_homography_to_abs=True, load_camera_matrix=True)
    frame = frame_sequence.get_frames()[FRAME_INDEX]
    camera_matrix = frame.get_camera_matrix()

    print 'the camera matrix is:'
    print camera_matrix._camera_matrix

    camera_matrix.compute_camera_parameters()

    print 'the internal parameters are:'
    print camera_matrix._C

    print 'the rotation matrix is:'
    print camera_matrix._R

    print 'the translation vector is:'
    print camera_matrix._T

    print 'the camera position is:'
    print camera_matrix.get_camera_position()
    
    C = np.matrix(camera_matrix._C)
    R = np.matrix(camera_matrix._R)
    T = np.matrix(camera_matrix._T)
    RT = np.matrix(np.hstack([R, T.reshape((3,1))]))

    print 'R^-1 * T = '
    print R.I * T.T
    print '[R | T]:'
    print RT
    print 'C * [R | T] = '
    print C * RT

    point1 = np.array([450, 400, 0, 1])
    point2 = np.array([450, 100, 0, 1])
    proj1 = np.dot(camera_matrix._camera_matrix, point1)
    proj2 = np.dot(camera_matrix._camera_matrix, point2)
    
    print 'point1: ', point1
    print 'point2: ', point2

    print 'projection of point1:'
    print proj1[:2] / proj1[2]

    print 'projection of point2:'
    print proj2[:2] / proj2[2]

    print 'we now project without the internal parameters'
    proj1 = np.dot(np.array(RT), point1)
    proj2 = np.dot(np.array(RT), point2)

    print 'projection of point1:'
    print proj1, ' -> ', proj1[:2] / proj1[2]

    print 'projection of point2:'
    print proj2, ' -> ', proj2[:2] / proj2[2]

def test_sort_indices_by_distance():
    frame_sequence = FrameSequence.load_frame_sequence(FRAME_SEQUENCE_DIR, load_homography_to_abs=True, load_camera_matrix=True)
    player_absolute_positions = load_player_absolute_positions(frame_index=FRAME_INDEX)
    #remove the nan entries
    player_absolute_positions = player_absolute_positions[~np.isnan(player_absolute_positions).any(axis=1)]
    frame = frame_sequence.get_frames()[FRAME_INDEX]
    camera_matrix = frame.get_camera_matrix()
    camera_matrix.compute_camera_parameters()
    player_world_positions = np.hstack([player_absolute_positions, np.zeros((player_absolute_positions.shape[0],1))])

    print 'player positions'
    print player_world_positions

    #shuffle the positions first
    np.random.shuffle(player_world_positions)

    print 'shuffled positions:'
    print player_world_positions
    
    print 'sorting players by distance to camera...'
    sorted_indices = camera_matrix.sort_indices_by_distance_to_camera(player_world_positions)

    print 'the sorted indices'
    print sorted_indices

def test_from_camera_position_and_direction():
    f = 1
    camera_position = np.array([0,0,1])
    camera_direction = np.array([0,0,1])
    C = CameraMatrix.from_camera_position_and_direction(camera_position, camera_direction, f)

    print 'position: ', camera_position
    print 'direction: ', camera_direction
    print 'camera: '
    print C.get_numpy_matrix()

def test_compute_preimage_on_plane():
    camera_position = np.array([0,10,1], np.float64)
    camera_direction = np.array([0,-1,0], np.float64)
    f=1
    
    camera = CameraMatrix.from_camera_position_and_direction(camera_position, camera_direction, f)

    world_plane = np.array([1,-1,0,0], np.float64)
    world_point = np.array([1,1,0], np.float64)

    image_point = camera.project_point_to_image(world_point)

    print 'image point: ', image_point
    print 'world point: ', world_point

    print 'computing preimage of the image point on the plane: ', world_plane
    preimage = camera.compute_preimage_on_plane(image_point, world_plane)

    print 'the preimage is: ', preimage
    
if __name__ == '__main__':
    #basic_project_ellipse_test()
    #frame_sequence_project_ellipse_test()
    #get_camera_position_test()
    #test_sort_indices_by_distance()
    #test_from_camera_position_and_direction()
    test_compute_preimage_on_plane()
