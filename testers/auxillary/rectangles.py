import numpy as np
from ...source.auxillary.rectangles import Rectangles

def test_get_slices():
    x = np.arange(5)
    y = np.arange(5)
    X,Y = np.meshgrid(x,y)
    array = np.dstack([X,Y])
    
    shape = (2,2)
    top_left_corners = np.array([[0,0], [0,3], [2,1]])
    rectangles = Rectangles(shape, top_left_corners)

    slices = rectangles.get_slices(array)

    print 'the initial array: '
    print array
    
    for s,t in zip(slices, top_left_corners):
        print 'the slice coming from top left corner %s is: ' % str(t)
        print s

def test_contained_in_rectangles():
    print 'testing when the first object has one rectangle and the other has N...'
    source_shape = (3,3)
    target_shape = (5,5)

    source_tl_corners = np.array([[2,2]])
    target_tl_corners = np.array([[1,1], [2,2], [3,2]])

    source_rectangles = Rectangles(source_shape, source_tl_corners)
    target_rectangles = Rectangles(target_shape, target_tl_corners)
    
    containment = source_rectangles.contained_in(target_rectangles)
    
    sr = source_rectangles._rectangles[0]
    for tr, c in zip(target_rectangles._rectangles, containment):
        print 'source rectangle: '
        print sr.reshape((2,2))
        print 'target rectangle: '
        print tr.reshape((2,2))
        print 'source contained in target: ', c

    print 'testing when the first object has N rectangles and the other has 1...'
    source_shape = (3,3)
    target_shape = (5,5)

    source_tl_corners = np.array([[1,1], [2,2], [3,2]])
    target_tl_corners = np.array([[2,2]])

    source_rectangles = Rectangles(source_shape, source_tl_corners)
    target_rectangles = Rectangles(target_shape, target_tl_corners)

    containment = source_rectangles.contained_in(target_rectangles)
    print containment
    
    tr = target_rectangles._rectangles[0]
    for sr, c in zip(source_rectangles._rectangles, containment):
        print 'source rectangle: '
        print sr.reshape((2,2))
        print 'target rectangle: '
        print tr.reshape((2,2))
        print 'source contained in target: ', c

    print 'testing when both objects have N rectangles...'
    source_shape = (3,3)
    target_shape = (5,5)

    source_tl_corners = np.array([[2,2], [4,2], [2,2]])
    target_tl_corners = np.array([[1,1], [2,2], [3,2]])

    source_rectangles = Rectangles(source_shape, source_tl_corners)
    target_rectangles = Rectangles(target_shape, target_tl_corners)
    
    containment = source_rectangles.contained_in(target_rectangles)
    
    for sr,tr, c in zip(source_rectangles._rectangles, target_rectangles._rectangles, containment):
        print 'source rectangle: '
        print sr.reshape((2,2))
        print 'target rectangle: '
        print tr.reshape((2,2))
        print 'source contained in target: ', c

    

def test_get_intersection_areas():
    
    print 'testing when the first object has one rectangle and the other has N...'
    source_shape = (3,3)
    target_shape = (5,5)

    source_tl_corners = np.array([[2,2]])
    target_tl_corners = np.array([[1,1], [2,2], [3,2], [7,2]])

    source_rectangles = Rectangles(source_shape, source_tl_corners)
    target_rectangles = Rectangles(target_shape, target_tl_corners)
    
    areas = source_rectangles.get_intersection_areas(target_rectangles)
    
    sr = source_rectangles._rectangles[0]
    for tr, a in zip(target_rectangles._rectangles, areas):
        print 'source rectangle: '
        print sr.reshape((2,2))
        print 'target rectangle: '
        print tr.reshape((2,2))
        print 'intersection area: ', a

    print 'testing when the first object has N rectangles and the other has 1...'
    source_shape = (3,3)
    target_shape = (5,5)

    source_tl_corners = np.array([[1,1], [2,2], [3,2]])
    target_tl_corners = np.array([[2,2]])

    source_rectangles = Rectangles(source_shape, source_tl_corners)
    target_rectangles = Rectangles(target_shape, target_tl_corners)

    areas = source_rectangles.get_intersection_areas(target_rectangles)
    
    tr = target_rectangles._rectangles[0]
    for sr, a in zip(source_rectangles._rectangles, areas):
        print 'source rectangle: '
        print sr.reshape((2,2))
        print 'target rectangle: '
        print tr.reshape((2,2))
        print 'intersection area: ', a

    print 'testing when both objects have N rectangles...'
    source_shape = (3,3)
    target_shape = (5,5)

    source_tl_corners = np.array([[2,2], [4,2], [2,2]])
    target_tl_corners = np.array([[1,1], [2,2], [3,2]])

    source_rectangles = Rectangles(source_shape, source_tl_corners)
    target_rectangles = Rectangles(target_shape, target_tl_corners)
    
    areas = source_rectangles.get_intersection_areas(target_rectangles)
    
    for sr,tr,a in zip(source_rectangles._rectangles, target_rectangles._rectangles, areas):
        print 'source rectangle: '
        print sr.reshape((2,2))
        print 'target rectangle: '
        print tr.reshape((2,2))
        print 'intersection area: ', a

    
def test_transfer_slices():
    print 'testing when the first object has N rectangles and the other has 1...'
    target_shape = (3,3)
    source_shape = (5,5)
    
    target_tl_corners = np.array([[1,1], [2,2], [3,2]])
    source_tl_corners = np.array([[2,2]])

    target_rectangles = Rectangles(target_shape, target_tl_corners)
    source_rectangles = Rectangles(source_shape, source_tl_corners)

    target_array = np.zeros((len(target_tl_corners),) + target_shape, np.int32)
    source_array = np.ones((len(source_tl_corners),) + source_shape, np.int32)

    target_rectangles.transfer_slices(target_array, source_rectangles, source_array)
    
    sr = source_rectangles._rectangles[0]
    for tr, ta in zip(target_rectangles._rectangles, target_array):
        print 'source rectangle: '
        print sr.reshape((2,2))
        print 'target rectangle: '
        print tr.reshape((2,2))
        print 'new target array:'
        print ta

    print 'testing when both objects have N rectangles...'
    source_shape = (3,3)
    target_shape = (5,5)

    source_tl_corners = np.array([[2,2], [4,2], [2,2]])
    target_tl_corners = np.array([[1,1], [2,2], [3,2]])

    source_rectangles = Rectangles(source_shape, source_tl_corners)
    target_rectangles = Rectangles(target_shape, target_tl_corners)

    source_array = np.ones((len(source_tl_corners),) + source_shape, np.int32)
    target_array = np.zeros((len(target_tl_corners),) + target_shape, np.int32)

    target_rectangles.transfer_slices(target_array, source_rectangles, source_array)
    
    for sr,tr,ta in zip(source_rectangles._rectangles, target_rectangles._rectangles, target_array):
        print 'source rectangle: '
        print sr.reshape((2,2))
        print 'target rectangle: '
        print tr.reshape((2,2))
        print 'new target array:'
        print ta


if __name__ == '__main__':
    #test_get_slices()
    #test_contained_in_rectangles()
    #test_get_intersection_areas()
    test_transfer_slices()
