import numpy as np
import cv2
import operator
import os
from ....source.image_processing.contour_generator.edges_contour_generator import EdgesContourGenerator
from ....source.image_processing.field_extractor.stupid_field_extractor import StupidFieldExtractor
from ....source.image_processing.image_annotator.soccer_annotation import SoccerAnnotation
from ....source.auxillary.shape_matching.directed_shape_matching_graph_builder import DirectedShapeMatchingGraphBuilder
from ....source.auxillary.contour_shape_overlap.highlighted_shape import HighlightedShape
from ....source.auxillary.fit_points_to_shape.line import Line
from ....source.auxillary.fit_points_to_shape.ellipse import Ellipse
from ....source.auxillary.contour_shape_overlap.line_with_position import LineWithPosition
from ....source.auxillary.contour_shape_overlap.ellipse_with_position import EllipseWithPosition
from ....source.auxillary.contour_shape_overlap.interval_on_ellipse import IntervalOnEllipse
from ...auxillary import primitive_drawing, label_shapes
from ....source.auxillary import mask_operations
from ..contour_shape_overlap.highlighted_shape_generator import draw_interval_mask
from ..soccer_field_geometry import draw_soccer_field_geometry, build_soccer_field_geometry
from ...tools.generate_sideline_training_data import load_highlighted_shapes_on_image
from ....source.image_processing.homography_calculator.correspondence_homography_calculator import CorrespondenceHomographyCalculator
from ....source.image_processing.homography_calculator.soccer_homography_calculator import SoccerHomographyCalculator
from ...image_processing.homography_calculator.correspondence_homography_visualization import CorrespondenceHomographyVisualization
from ....source.auxillary.soccer_field_geometry import SoccerFieldGeometry
from ....source.auxillary import planar_geometry
from ....source.data_structures.contour import Contour

def basic_test():
    THICKNESS = 3
    HIGHLIGHTED_LINE_THRESHOLD = 2
    HIGHLIGHTED_ELLIPSE_THRESHOLD = 2
    PARALLEL_LINES_THRESHOLD = np.pi / 10
    shape_matching_graph_builder = ShapeMatchingGraphBuilder(HIGHLIGHTED_LINE_THRESHOLD, HIGHLIGHTED_ELLIPSE_THRESHOLD, PARALLEL_LINES_THRESHOLD)
    
    image = np.zeros((500,500,3), np.uint8)
    mask = np.zeros(image.shape[:2], np.float32)
    
    #first set up a list of highlighted lines
    lines_with_position = []
    highlighting_boundaries = []
    highlighted_line_names = []

    highlighted_line_names.append('bottom horizontal line')
    lines_with_position.append(LineWithPosition(Line.from_point_and_direction(np.array([200,240]), np.array([1,0]))))
    highlighting_boundaries.append((np.array([100,200]), np.array([300,200])))

    highlighted_line_names.append('top horizontal line')
    lines_with_position.append(LineWithPosition(Line.from_point_and_direction(np.array([200,200]), np.array([1,0]))))
    highlighting_boundaries.append((np.array([200,200]), np.array([300,200])))

    highlighted_line_names.append('middle horizontal line')
    lines_with_position.append(LineWithPosition(Line.from_point_and_direction(np.array([200,220]), np.array([1,0]))))
    highlighting_boundaries.append((np.array([200,220]), np.array([300,220])))

    highlighted_line_names.append('vertical line')
    lines_with_position.append(LineWithPosition(Line.from_point_and_direction(np.array([200,0]), np.array([0,1]))))
    highlighting_boundaries.append((np.array([200,200]), np.array([200,240])))

    highlighted_line_dict = dict()
    for highlighted_line_name, line_with_position, highlighting_boundary in zip(highlighted_line_names, lines_with_position, highlighting_boundaries):
        intervals_on_line = line_with_position._find_intervals_in_point_sequence(np.vstack(highlighting_boundary))
        highlighted_line_dict[highlighted_line_name] = HighlightedShape(line_with_position, THICKNESS, intervals_on_line)

    print 'highlighted line dict:'
    print '\n'.join('%s: %s' % (key, str(hs)) for key,hs in highlighted_line_dict.iteritems())
    
    shape_matching_graph = shape_matching_graph_builder.build_shape_matching_graph(image, mask, highlighted_line_dict)

    sub_highlighted_line_dict = dict(('sub_%s' % k, v) for k,v in highlighted_line_dict.iteritems() if k in ('top horizontal line', 'bottom horizontal line'))
    sub_shape_matching_graph = shape_matching_graph_builder.build_shape_matching_graph(image, mask, sub_highlighted_line_dict)
    
    image[mask > 0] = np.ones(3)*255
    for highlighted_line in highlighted_line_dict.itervalues():
        primitive_drawing.draw_line(image, highlighted_line.get_shape(), (0,255,0))
        draw_interval_mask(image, highlighted_line)
        
    cv2.imshow('image with mask and shapes', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    print 'shape matching graph:'
    print str(shape_matching_graph)

    print 'sub shape matching graph:'
    print str(sub_shape_matching_graph)

    print 'sub graph embeddings:'
    print '\n'.join(map(str,shape_matching_graph.compute_graph_embeddings(sub_shape_matching_graph)))
    
def ellipse_test():
    THICKNESS = 3
    HIGHLIGHTED_LINE_THRESHOLD = 2
    HIGHLIGHTED_ELLIPSE_THRESHOLD = 2
    PARALLEL_LINES_THRESHOLD = np.pi / 10
    shape_matching_graph_builder = ShapeMatchingGraphBuilder(HIGHLIGHTED_LINE_THRESHOLD, HIGHLIGHTED_ELLIPSE_THRESHOLD, PARALLEL_LINES_THRESHOLD)
    
    image = np.zeros((500,500,3), np.uint8)
    mask = np.zeros(image.shape[:2], np.float32)
    mask[100-10:100+10, 200-10:200+10] = 1
    
    line_with_position = LineWithPosition(Line.from_point_and_direction(np.array([100,100]), np.array([1,0])))
    interval_on_line = line_with_position.get_interval_on_shape(np.array([0, 100]), np.array([500, 100]))
    highlighted_line = HighlightedShape(line_with_position, THICKNESS, [interval_on_line])

    center = np.array([150,100])
    axes = np.array([50,50])
    angle = 0
    ellipse_with_position = EllipseWithPosition(Ellipse.from_geometric_parameters(center, axes, angle))
    interval_on_ellipse = IntervalOnEllipse(ellipse_with_position, 0, 270)
    highlighted_ellipse = HighlightedShape(ellipse_with_position, THICKNESS, [interval_on_ellipse])

    highlighted_shape_dict = {'line':highlighted_line, 'ellipse':highlighted_ellipse}

    print 'highlighted shape dict:'
    print '\n'.join('%s: %s' % (key, str(hs)) for key,hs in highlighted_shape_dict.iteritems())


    image[mask > 0] = np.ones(3)*255
    primitive_drawing.draw_line(image, line_with_position.get_shape(), (0,255,0))
    primitive_drawing.draw_ellipse(image, ellipse_with_position.get_shape(), (0,255,0))
    draw_interval_mask(image, highlighted_line)
    draw_interval_mask(image, highlighted_ellipse)
    
    cv2.imshow('image with mask and shapes', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    shape_matching_graph = shape_matching_graph_builder.build_shape_matching_graph(image, mask, highlighted_shape_dict)

def load_absolute_soccer_field():
    '''
    Return a tuple (image, mask, highlighted shape dict)
    '''
    ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)
    mask = np.zeros(absolute_image.shape[:2], np.float32)
    
    HIGHLIGHTED_LINE_THRESHOLD = 10
    HIGHLIGHTED_ELLIPSE_THRESHOLD = 10
    PARALLEL_LINES_THRESHOLD = np.pi / 10
    shape_matching_graph_builder = DirectedShapeMatchingGraphBuilder(HIGHLIGHTED_LINE_THRESHOLD, HIGHLIGHTED_ELLIPSE_THRESHOLD,
                                                                     PARALLEL_LINES_THRESHOLD)
    
    soccer_field = build_soccer_field_geometry(absolute_image)
    highlighted_shape_dict = soccer_field.get_highlighted_shape_dict(format_keys=True)
    shape_matching_graph = shape_matching_graph_builder.build_shape_matching_graph(absolute_image, mask, highlighted_shape_dict)
    return soccer_field
    #return absolute_image, mask, highlighted_shape_dict, shape_matching_graph
    #return absolute_image, soccer_field, shape_matching_graph

def soccer_absolute_test():
    absolute_image, soccer_field, shape_matching_graph = load_absolute_soccer_field()

    draw_soccer_field_geometry(absolute_image, soccer_field)

    cv2.imshow('soccer field geometry', absolute_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    print str(shape_matching_graph)
    print 'parallel components:'
    print shape_matching_graph.find_parallel_line_components()
    return

def padded_image(image):
    padding = 100
    padded_shape = (image.shape[0]+padding, image.shape[1]+padding, 3)
    padded_image = np.zeros(padded_shape, np.uint8)
    padded_image[:padded_shape[0]-padding, :padded_shape[1]-padding] = image
    return padded_image

def get_contour_mask(image):
    field_extractor = StupidFieldExtractor()
    field_mask = field_extractor.get_field_mask(image)
    contour_generator = EdgesContourGenerator()
    base_contours = contour_generator.generate_contours(image, mask=field_mask)

    contour_mask = base_contours[0].get_outer_mask()
    for base_contour in base_contours[1:]:
        contour_mask = mask_operations.get_union(contour_mask, base_contour.get_outer_mask())

    return contour_mask

def display_noisy_highlighted_shapes(image, noise_mask, highlighted_shape_with_noise_dict):
    noisy_image = image.copy()
    noisy_image[noise_mask > 0] = np.ones(3)*255
    image = image.copy()
    image[noise_mask > 0] = np.ones(3)*255
    
    for highlighted_shape_with_noise in highlighted_shape_with_noise_dict.itervalues():
        noisy_highlighted_shape = highlighted_shape_with_noise.noisy
        highlighted_shape = highlighted_shape_with_noise.original
        if highlighted_shape.is_line():
            primitive_drawing.draw_line(image, highlighted_shape.get_shape(), (0,255,0))
            primitive_drawing.draw_line(noisy_image, highlighted_shape.get_shape(), (0,255,0))
        elif highlighted_shape.is_ellipse():
            primitive_drawing.draw_ellipse(image, highlighted_shape.get_shape(), (0,255,0))
            primitive_drawing.draw_ellipse(noisy_image, highlighted_shape.get_shape(), (0,255,0))
        
        draw_interval_mask(image, highlighted_shape)
        draw_interval_mask(noisy_image, noisy_highlighted_shape)

    cv2.imshow('image with mask and shapes', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    cv2.imshow('image with mask and noisy shapes', noisy_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    return

def draw_absolute_field_on_image(image, H, soccer_field, color=(0,0,255), alpha=0.5):
    absolute_field_mask = np.ones((soccer_field.field_length(), soccer_field.field_width()), np.bool)
    transformed_field_mask = planar_geometry.apply_homography_to_mask(H, absolute_field_mask, image.shape[:2])

    image_with_mask = image.copy()
    image_with_mask[transformed_field_mask] = np.array(color)
    image[...] = alpha*image_with_mask + (1-alpha)*image

    return

def load_soccer_image(image_name, annotation_name, highlighted_shapes_directory, remove_ellipses=False):
    
    highlighted_shapes = load_highlighted_shapes_on_image(highlighted_shapes_directory)
    highlighted_shape_dict = dict(enumerate(highlighted_shapes))

    for hs in highlighted_shape_dict.itervalues():
        if hs.is_ellipse():
            print 'we loaded an ellipse with highilghted arc length: ', hs.highlighted_arc_length()
            
    if remove_ellipses:
        print 'removing ellipses...'
        highlighted_shape_dict = dict((k,v) for k,v in highlighted_shape_dict.iteritems() if v.is_line())
        
    image = cv2.imread(image_name, 1)
    annotation = SoccerAnnotation(np.load(annotation_name))

    #shape_matching_graph_builder = ShapeMatchingGraphBuilder(HIGHLIGHTED_LINE_THRESHOLD, HIGHLIGHTED_ELLIPSE_THRESHOLD, PARALLEL_LINES_THRESHOLD)
    #shape_matching_graph = shape_matching_graph_builder.build_shape_matching_graph(image, mask, highlighted_shape_dict)
    
    return image, annotation, highlighted_shape_dict

def compute_homography_jacobian(H):
    D = H[2,2]
    return (1 / D**2) * np.array([[H[0,0] - H[2,0]*H[0,2], H[0,1] - H[2,1]*H[0,2]],
                                  [H[1,0] - H[2,0]*H[1,2], H[1,1] - H[2,1]*H[1,2]]])

def save_shape_homography_and_correspondences(H, shape_correspondences):
    
    #save the list of shape correspondences.
    source_lines = []
    target_lines = []
    source_ellipse = None
    target_ellipse = None

    for source_shape, target_shape in shape_correspondences:
        if source_shape.is_line():
            source_lines.append(source_shape)
            target_lines.append(target_shape)

        elif source_shape.is_ellipse():
            source_ellipse = source_shape
            target_ellipse = target_shape

    source_lines = np.vstack([sl.get_parametric_representation() for sl in source_lines])
    target_lines = np.vstack([tl.get_parametric_representation() for tl in target_lines])
    source_ellipse = source_ellipse.get_parametric_representation()
    target_ellipse = target_ellipse.get_parametric_representation()
    
    SHAPE_DIR = 'output/tester/shape_matching_graph'
    homography_file = os.path.join(SHAPE_DIR, 'homography')
    source_lines_file = os.path.join(SHAPE_DIR, 'source_lines')
    target_lines_file = os.path.join(SHAPE_DIR, 'target_lines')
    source_ellipse_file = os.path.join(SHAPE_DIR, 'source_ellipse')
    target_ellipse_file = os.path.join(SHAPE_DIR, 'target_ellipse')

    np.save(homography_file, H)
    np.save(source_lines_file, source_lines)
    np.save(target_lines_file, target_lines)
    np.save(source_ellipse_file, source_ellipse)
    np.save(target_ellipse_file, target_ellipse)

    return

def soccer_image_test():
    MIN_LINES_IN_CORRESPONDENCE = 5
    HIGHLIGHTED_LINE_THRESHOLD = 10
    HIGHLIGHTED_ELLIPSE_THRESHOLD = 2
    PARALLEL_LINES_THRESHOLD = 1 - np.cos(np.pi / 10)

    IWASAWA_THRESHOLD = 0.2
    CORRESPONDENCE_THRESHOLD = 0.1
    
    #We load shapes from a highlighted shape directory, and create a correspondence with the shapes in a SoccerFieldGeometry object
    FRAME_INDEX = 1682
    HIGHLIGHTED_SHAPES_DIRECTORY = '/Users/daniel/Documents/soccer/output/preprocessing/data/images-8_15-9_31/highlighted_shapes/highlighted_shapes_%d' % FRAME_INDEX
    IMAGE_NAME = '/Users/daniel/Documents/soccer/images/images-8_15-9_31/image_%d.png' % FRAME_INDEX
    ANNOTATION_NAME = '/Users/daniel/Documents/soccer/output/preprocessing/data/images-8_15-9_31/annotation/annotation_%d.npy' % FRAME_INDEX
    # image, mask, highlighted_shape_dict, shape_matching_graph = load_soccer_image(IMAGE_NAME, HIGHLIGHTED_SHAPES_DIRECTORY, load_ellipses=False)
    soccer_field = load_absolute_soccer_field()
    image, annotation, highlighted_shape_dict = load_soccer_image(IMAGE_NAME, ANNOTATION_NAME, HIGHLIGHTED_SHAPES_DIRECTORY,
                                                                  remove_ellipses=False)
    ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
    SCALE = 6
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)

    noise_mask = np.logical_or(annotation.get_sidelines_mask()>0, annotation.get_players_mask()>0)
    image[noise_mask > 0] = np.ones(3)*255
    for highlighted_shape in highlighted_shape_dict.itervalues():
        if highlighted_shape.is_line():
            primitive_drawing.draw_line(image, highlighted_shape.get_shape(), (0,255,0))
        elif highlighted_shape.is_ellipse():
            primitive_drawing.draw_ellipse(image, highlighted_shape.get_shape(), (0,255,0))
        
        draw_interval_mask(image, highlighted_shape)

    
    #print 'shape matching graph:'
    #print str(shape_matching_graph)

    #compute the homography based on the highlighted shapes
    soccer_homography_calculator = SoccerHomographyCalculator(absolute_image, SCALE,
                                                              MIN_LINES_IN_CORRESPONDENCE,
                                                              HIGHLIGHTED_LINE_THRESHOLD, HIGHLIGHTED_ELLIPSE_THRESHOLD,
                                                              PARALLEL_LINES_THRESHOLD,
                                                              CORRESPONDENCE_THRESHOLD, IWASAWA_THRESHOLD)

    H, embedding = soccer_homography_calculator.compute_homography(image, annotation, highlighted_shape_dict)

    #map the absolute soccer field to the image and draw it
    if H is not None:
        draw_absolute_field_on_image(image, H, soccer_field)

    labelled_image = padded_image(image.copy())
    labelled_shapes_with_position = dict((str(k), highlighted_shape.get_shape_with_position())
                                         for k,highlighted_shape in highlighted_shape_dict.iteritems())
    print 'labelled shapes with position:'
    print labelled_shapes_with_position
    label_shapes.label_shapes(labelled_image, labelled_shapes_with_position)

    cv2.imshow('image with mask and shapes', labelled_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    if H is None:
        print 'we did not find a homography.'
        return
    
    print 'we found the homography:'
    print H
    
    #display the homography and the embedding
    labelled_image = padded_image(image.copy())
    
    # soccer_field = soccer_homography_calculator._soccer_field
    # absolute_highlighted_shape_dict = soccer_homography_calculator._absolute_highlighted_shape_dict
    # shape_correspondences = [(absolute_highlighted_shape_dict[source_key].get_shape(), highlighted_shape_dict[target_key].get_shape())
    #                          for source_key, target_key in embedding.iteritems()]

    # chv = CorrespondenceHomographyVisualization()
    # labelled_image = chv.get_visualization(image, H, shape_correspondences, soccer_field.get_grid_points())

    # labelled_shapes_with_position = dict((SoccerFieldGeometry.format_key(source_key), highlighted_shape_dict[target_key].get_shape_with_position()) for source_key, target_key in embedding.iteritems())
    # label_shapes.label_shapes(labelled_image, labelled_shapes_with_position)

    all_absolute_shapes_dict = soccer_field.get_highlighted_shape_dict(format_keys=True)
    transformed_absolute_shapes_dict = dict((k, planar_geometry.apply_homography_to_shape(H, hs.get_shape()))
                                            for k,hs in all_absolute_shapes_dict.iteritems())
                                            #if ('penalty' in k or 'sideline' in k) and ('right' in k or 'left' in k))

    for shape in transformed_absolute_shapes_dict.itervalues():
        primitive_drawing.draw_shape(labelled_image, shape, (0,255,0))

    for k, shape in transformed_absolute_shapes_dict.items():
        if shape.is_line():
            transformed_absolute_shapes_dict[k] = LineWithPosition(shape)
        elif shape.is_ellipse():
            transformed_absolute_shapes_dict[k] = EllipseWithPosition(shape)
            
    label_shapes.label_shapes(labelled_image, transformed_absolute_shapes_dict)

    v = np.dot(H, np.array([0,1,0]))
    v /= v[2]
    L_inf = np.array([0,0,1], np.float32)
    image_of_L_inf = Line.from_parametric_representation(np.dot(np.linalg.inv(H).transpose(), L_inf))
    image_center = np.array([image.shape[1]-1,image.shape[0]-1], np.float32) / 2.0
    inf_to_center = image_of_L_inf.get_algebraic_distance_from_point(image_center)
    
    primitive_drawing.draw_shape(labelled_image, image_of_L_inf, (0,0,255))
    
    print 'H*[0,1,0] = ', v
    print 'distance of line at infinity = ', inf_to_center
    print 'image center norm: ', np.linalg.norm(image_center)
    
    cv2.imshow('image with mask and shapes', labelled_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    #save_shape_homography_and_correspondences(H, shape_correspondences)
    
if __name__ == '__main__':
    #basic_test()
    #ellipse_test()
    #soccer_absolute_test()
    soccer_image_test()
