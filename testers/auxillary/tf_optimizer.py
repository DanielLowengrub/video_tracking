import tensorflow as tf
import numpy as np
from ...source.auxillary.tf_optimizer import TFOptimizer

def basic_cost(x):
    return tf.pow(x, 2)

def basic_test():
    '''
    We minimize the function f(x) = x^2
    '''

    LEARNING_RATE = 0.01
    MAX_EPOCHS = 1000
    LOCAL_MINIMUM_THRESHOLD = 0.00001

    optimizer = TFOptimizer(LEARNING_RATE, MAX_EPOCHS, LOCAL_MINIMUM_THRESHOLD)

    initial_value = np.array([1.0], np.float32)
    print 'the initial value is: ', initial_value
    
    best_value = optimizer.minimize(initial_value, basic_cost, print_progress_interval=5)

    print 'the best value is: ', best_value

if __name__ == '__main__':
    basic_test()
