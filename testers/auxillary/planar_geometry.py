import numpy as np
import cv2
from ...source.auxillary.fit_points_to_shape.line import Line
from ...source.auxillary.fit_points_to_shape.ellipse import Ellipse
from ...source.auxillary import planar_geometry
import primitive_drawing
from ...source.auxillary.camera_matrix import CameraMatrix

def homography_to_unit_circle_test():
    center = np.array([0,0])
    axes = np.array([3,10])
    angle = np.pi/3

    ellipse = Ellipse.from_geometric_parameters(center, axes, angle)
    H = planar_geometry.compute_homography_to_unit_circle(ellipse)

    print 'started with the ellipse:'
    print 'center: ', center
    print 'axes: ', axes

    print 'the homography to the unit circle:'
    print H

    transformed_ellipse = planar_geometry.apply_homography_to_ellipse(H, ellipse)
    transformed_ellipse.compute_geometric_parameters()
    center, axes, angle = transformed_ellipse.get_geometric_parameters()

    print 'the new ellipse has parameters:'
    print 'center: ', center
    print 'axes: ', axes

def homography_to_y_axis():
    line = Line.from_point_and_direction(np.array([10,2]), np.array([1,1]))
    H = planar_geometry.compute_homography_to_y_axis(line)
    transformed_line = planar_geometry.apply_homography_to_line(H, line)

    print 'started with the line: ', str(line)
    print 'got the line: ', str(transformed_line)

def line_ellipse_intersection():
    center = np.array([100,150])
    axes = np.array([150,70])
    angle = -np.pi/3
    ellipse = Ellipse.from_geometric_parameters(center, axes, angle)
    line = Line.from_point_and_direction(np.array([200,80]), np.array([1,1]))

    points = planar_geometry.line_ellipse_intersection(line, ellipse)

    image = np.zeros((500,500,3), np.uint8)
    primitive_drawing.draw_ellipse(image, ellipse, (0,255,0))
    primitive_drawing.draw_line(image, line, (0,255,0))

    for point in points:
        primitive_drawing.draw_point(image, point, (255,0,0))

    cv2.imshow('line ellipse intersection points', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def test_line_region_intersection():
    image = np.zeros((100,100,3), np.uint8)
    image_center = np.array([100,100], np.float64) / 2.0

    line_direction = np.array([1,1], np.float64)
    line_direction /= np.linalg.norm(line_direction)

    line = Line.from_point_and_direction(image_center, line_direction)

    primitive_drawing.draw_line(image, line, (0,255,0), 5)

    cv2.imshow('the line', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    return

def projective_line_line_intersection():
    image = np.zeros((500,500,3), np.uint8)
    line_1 = Line.from_point_and_direction(np.array([100,100]), np.array([1,0]))
    line_2 = Line.from_point_and_direction(np.array([100,200]), np.array([1,0]))

    point = planar_geometry.projective_line_line_intersection(line_1, line_2)

    if point is not None:
        print 'the intersection point is: ', point

        if point[2] > 0:
            point = point[:2]/point[2]
            primitive_drawing.draw_line(image, line_1, (0,255,0))
            primitive_drawing.draw_line(image, line_2, (0,255,0))
            primitive_drawing.draw_point(image, point, (255,0,0))

            cv2.imshow('line line intersection points', image)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

def generate_test_homography():
    #position of the camera
    P = np.array([-5, 20,15])

    #the camera Z axis. I.e, where the camera is pointing.
    CZ = np.array([1, -5, -3])
    CZ = CZ / np.linalg.norm(CZ)

    print 'the camera is pointing in direction: ', CZ

    #now find the camera rotation matrix. We already have the Z axis
    #The new y axis will be the projection of the old z axis onto the plane defined by n
    z_axis = np.array([0,0,1])
    CY = z_axis - np.dot(z_axis, CZ)*CZ
    CY = CY / np.linalg.norm(CY)

    print 'the camera Y axis: ', CY
    #the camera X axis is orthogonal to CY and CZ
    CX = np.cross(CY, CZ)

    print 'the camera X axis: ', CX
    
    R = np.vstack([CX, CY, CZ]).transpose()

    print 'R:'
    print R
    
    f = 2
    
    camera_matrix = CameraMatrix.from_camera_position(P, R, f)

    return camera_matrix.get_homography_matrix()

def compute_homography_on_line():
    image = np.zeros((500,500,3), np.uint8)
    line_center = np.array([100,100], np.float32)
    line_direction = np.array([1,0])
    line = Line.from_point_and_direction(line_center, line_direction)
    points_on_line = np.vstack([line_center + a*line_direction for a in (0, 10, 50, 100)])

    H = generate_test_homography()

    print 'the homography:'
    print H
    
    print 'The original points: '
    print points_on_line

    transformed_points = cv2.perspectiveTransform(points_on_line.reshape((-1,1,2)), H).reshape((-1,2))

    print 'The transformed points:'
    print transformed_points

    projective_point_correspondences = [(np.append(p1,1), np.append(p2,1)) for p1,p2 in zip(points_on_line, transformed_points)]

    print 'the point correspondences:'
    print projective_point_correspondences
    
    H = planar_geometry.compute_homography_on_line(line, projective_point_correspondences)

    print 'we found the homography:'
    print H

    transformed_points = cv2.perspectiveTransform(points_on_line.reshape((-1,1,2)), H).reshape((-1,2))

    print 'and used it to transform the points to get:'
    print transformed_points

def test_lines_ellipses_intersection():
    line_0 = Line.from_point_and_direction(np.array([100,100]), np.array([1,0]))
    line_1 = Line.from_point_and_direction(np.array([100,100]), np.array([0,1]))
    line_2 = Line.from_point_and_direction(np.array([100,100]), np.array([1,1]))
    lines = [line_0 for i in range(1)] + [line_1 for i in range(1)] + [line_2 for i in range(1)]

    print 'lines:'
    print '\n'.join(map(str, lines))
    
    center = np.array([100,100])
    axes = np.array([80,80])
    angle = 0
    ellipse_0 = Ellipse.from_geometric_parameters(center, axes, angle)
    ellipses = [ellipse_0 for i in range(3)]

    print 'ellipses:'
    print '\n'.join(map(str, ellipses))
    
    L = np.vstack([l.get_parametric_representation() for l in lines])
    C = np.stack([e.get_parametric_representation() for e in ellipses])

    print 'L:'
    print L
    print 'C:'
    print C
    
    intersection_points = planar_geometry.lines_ellipses_intersection(L,C)

    print 'intersection points:'
    print intersection_points

def test_ellipse_regions():
    image = np.zeros((500,500,3), np.uint8)
    center = np.array([100,100])
    axes = np.array([80,20])
    angle = np.pi/4.0
    ellipse_0 = Ellipse.from_geometric_parameters(center, axes, angle)

    center = np.array([300,200])
    axes = np.array([80,70])
    angle = 0.0
    ellipse_1 = Ellipse.from_geometric_parameters(center, axes, angle)
    
    ellipses = [ellipse_0, ellipse_1]

    print 'ellipses:'
    print '\n'.join(map(str, ellipses))

    for e in ellipses:
        primitive_drawing.draw_ellipse(image, e, (0,255,0))
        
    cv2.imshow('ellipses', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    C = np.stack([e.get_parametric_representation() for e in ellipses])

    print 'C:'
    print C

    regions = planar_geometry.get_ellipse_regions(C)

    print 'regions: '
    print regions

    #draw the regions
    for region in regions:
        for l in planar_geometry.get_region_boundary_lines(region):
            primitive_drawing.draw_line(image, l, (255,0,0))

    cv2.imshow('ellipses with boundary lines', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def test_evaluate_ellipses_on_points():
    image = np.zeros((500,500,3), np.uint8)
    center = np.array([200,200])
    axes = np.array([80,80])
    angle = 0.0
    ellipse = Ellipse.from_geometric_parameters(center, axes, angle)

    points_x = np.arange(100,301,10)
    points_y = np.ones(len(points_x)) * 200
    points = np.vstack([points_x, points_y]).transpose()
    
    ellipses = [ellipse for i in range(len(points))]
    C = np.stack([e.get_parametric_representation() for e in ellipses])

    print 'points:'
    print points
    print 'C:'
    print C

    for e in ellipses:
        primitive_drawing.draw_ellipse(image, e, (0,255,0))

    for p in points:
        primitive_drawing.draw_point(image, p, (255,0,0))
        
    cv2.imshow('ellipses and points', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    values = planar_geometry.evaluate_ellipses_on_points(C, points)

    print 'values: ', values


def test_evaluate_ellipses_on_grid():
    image = np.zeros((500,500,3), np.uint8)

    ellipses = []
    
    center = np.array([100,100])
    axes = np.array([80,40])
    angle = np.pi/4.0
    ellipses.append(Ellipse.from_geometric_parameters(center, axes, angle))

    center = np.array([300,250])
    axes = np.array([80,30])
    angle = 0.0
    ellipses.append(Ellipse.from_geometric_parameters(center, axes, angle))

    C = np.stack([e.get_parametric_representation() for e in ellipses])
    
    x = np.arange(0,400)
    y = np.arange(0,400)
    XX,YY = np.meshgrid(x,y)

    values = planar_geometry.evaluate_ellipses_on_grid(C, XX, YY)    
    print 'shape of values: '
    print values.shape

    #values = values[1]

    #print 'shape of values: '
    #print values.shape

    #we remove the negative values and scale the remaining values from 0 to 1
    values *= -1
    values[values < 0] = 0
    values = values[0] + values[1]
    max_val = values.max()
    values /= max_val

    print 'values:'
    print values
    print 'shape of values: '
    print values.shape

    cv2.imshow('values', values)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def test_ellipse_center():
    # center = np.array([20,1])
    # axes = np.array([10,5])
    # angle = np.pi/5

    # ellipse = Ellipse.from_geometric_parameters(center, axes, angle)
    # ellipse.compute_geometric_parameters()
    # C = ellipse.get_parametric_representation()
    C = np.array([[-1,0,0],[0,0,1],[0,1,0]], np.float32)

    print 'C:'
    print C
    
    K = planar_geometry.compute_ellipse_center(C)

    # print 'the center of the ellipse is: ', center
    print 'we calculated the center to be:       ', K

def test_normalized_ellipse():
    center = np.array([20,1])
    axes = np.array([10,5])
    angle = np.pi/5

    ellipse = Ellipse.from_geometric_parameters(center, axes, angle)
    ellipse.compute_geometric_parameters()
    C = ellipse.get_parametric_representation()

    K,M = planar_geometry.compute_normalized_ellipse(C)

    print 'the normalized ellipse should be:'
    print 'M:'
    print ellipse._M
    print 'K: ', center

    print 'we got:'
    print 'M:'
    print M
    print 'K: ', K

def test_ellipse_ellipse_intersection():
    image = np.zeros((500,500,3), np.uint8)
    
    center_0 = np.array([200,200])
    axes_0 = np.array([150,100])
    angle_0 = 0
    ellipse_0 = Ellipse.from_geometric_parameters(center_0, axes_0, angle_0)
    
    center_1 = np.array([300,200])
    axes_1 = np.array([150,100])
    angle_1 = np.pi/1.5
    ellipse_1 = Ellipse.from_geometric_parameters(center_1, axes_1, angle_1)

    primitive_drawing.draw_ellipse(image, ellipse_0, (255,0,0))
    primitive_drawing.draw_ellipse(image, ellipse_1, (255,0,0))
    
    intersection_points = planar_geometry.ellipse_ellipse_intersection(ellipse_0, ellipse_1)

    primitive_drawing.draw_points(image, intersection_points, (0,255,0))
    print 'intersection points: ', intersection_points

    cv2.imshow('intersection points', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def test_compute_bitangent_lines():
    image = np.zeros((500,500,3), np.uint8)
    
    center_0 = np.array([100,200])
    axes_0 = np.array([50,50])
    angle_0 = 0
    ellipse_0 = Ellipse.from_geometric_parameters(center_0, axes_0, angle_0)
    
    center_1 = np.array([300,200])
    axes_1 = np.array([50,50])
    angle_1 = 0
    ellipse_1 = Ellipse.from_geometric_parameters(center_1, axes_1, angle_1)

    primitive_drawing.draw_ellipse(image, ellipse_0, (255,0,0))
    primitive_drawing.draw_ellipse(image, ellipse_1, (255,0,0))
    
    bitangent_lines = planar_geometry.compute_bitangent_lines(ellipse_0, ellipse_1)

    for l in bitangent_lines:
        primitive_drawing.draw_line(image, l, (0,255,0))

    cv2.imshow('bitangent lines', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def test_compute_homography_to_unit_line():
    line = np.random.normal(size=(3,))
    H = planar_geometry.compute_homography_to_unit_line(line)

    print 'line: ', line
    print 'H:'
    print H

    print 'H^-T * line:'
    print np.matmul(np.linalg.inv(H).transpose(), line)

def test_projective_line_conic_intersection():
    line = np.array([0,1,0])
    conic = np.array([[1,  0 ,  0 ],
                      [0, -1 ,  10],
                      [0,  10, -99]])

    points = planar_geometry.projective_line_conic_intersection(line, conic)

    print 'line: ', line
    print 'conic:'
    print conic
    print 'intersection points: '
    print points

    print 'evaluating line on points: ', [np.matmul(line, p) for p in points]
    print 'evaluating conic on points: ', [np.matmul(p, np.matmul(conic,p)) for p in points]

def test_tangent_ellipse_intersection():
    image = np.zeros((400,400,3), np.uint8)

    center = np.array([200,200])
    axes = np.array([100,80])
    angle = 0

    ellipse = Ellipse.from_geometric_parameters(center, axes, angle)
    H = planar_geometry.compute_homography_to_unit_circle(ellipse)
    H_inv = np.linalg.inv(H)

    point = np.array([1,-1])
    point = point / np.linalg.norm(point)
    direction = planar_geometry.perp(point)
    
    line = Line.from_point_and_direction(point, direction)
    line = planar_geometry.apply_homography_to_line(H_inv, line)

    primitive_drawing.draw_ellipse(image, ellipse, (255,0,0))
    primitive_drawing.draw_line(image, line, (255,0,0))

    cv2.imshow('tangent line', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    tangent_point = planar_geometry.tangent_ellipse_intersection(line, ellipse)

    print 'tangent point: ', tangent_point

    primitive_drawing.draw_point(image, tangent_point, (0,255,0))

    cv2.imshow('tangent point', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def test_apply_homography_to_mask():
    H = planar_geometry.translation_matrix(np.array([1,1]))
    source_mask = np.ones((4,4), np.bool)
    source_mask[1,1] = False
    target_mask_shape = (4,4)

    target_mask = planar_geometry.apply_homography_to_mask(H, source_mask, target_mask_shape)

    print 'H:'
    print H
    print 'source_mask:'
    print source_mask
    print 'target_mask:'
    print target_mask

def test_interpolate_on_circle():
    start = np.array([-0.9489611,  -0.28774353])
    end = np.array([-0.97831968, -0.18188366])
    weights = np.linspace(1, 0, 4)

    print 'start: ', start
    print 'end: ', end

    interpolated_points = planar_geometry.interpolate_on_circle(start, end, weights)

    print 'interpolated points:'
    print interpolated_points

    print 'point norms:'
    print np.linalg.norm(interpolated_points, axis=1)

def test_apply_homography_to_ray():
    H = np.random.normal(size=(8,))
    H = np.append(H,1).reshape((3,3))
    
    source_ray = np.array([[0,0], [1,0]])
    target_ray = planar_geometry.apply_homography_to_ray(H, source_ray)
    target_ray[1] = target_ray[1] / np.linalg.norm(target_ray[1])
    
    target_points = planar_geometry.apply_homography_to_points(H, source_ray)
    target_origin = target_points[0]
    target_direction = target_points[1] - target_points[0]
    target_direction /= np.linalg.norm(target_direction)
    
    print 'H:'
    print H
    print 'source ray:        origin=%s, direction=%s' % tuple(source_ray.tolist())
    print 'target ray:        origin=%s, direction=%s' % tuple(target_ray.tolist())
    print 'actual target ray: origin=%s, direction=%s' % (target_origin, target_direction)

def test_ray_region_intersection():
    region = np.array([[1,1],[11,11]])
    ray =    np.array([[0,5],[1,0]])

    intersection_points = planar_geometry.ray_region_intersection(ray, region)

    print 'region: '
    print region

    print 'ray:'
    print ray

    print 'intersection points:'
    print intersection_points

    return

def test_line_segment_region_intersection():
    region = np.array([[0,0],[10,10]], np.float32)
    line_segment = np.array([[5,10],[5,15]], np.float32)

    intersection = planar_geometry.line_segment_region_intersection(line_segment, region)

    print 'region:'
    print region

    print 'line segment:'
    print line_segment

    print 'intersection:'
    print intersection
    
if __name__ == '__main__':
    #homography_to_unit_circle_test()
    #homography_to_y_axis()
    #line_ellipse_intersection()
    #test_tangent_ellipse_intersection()
    #projective_line_line_intersection()
    #compute_homography_on_line()
    #test_lines_ellipses_intersection()
    #test_ellipse_regions()
    #test_evaluate_ellipses_on_points()
    #test_evaluate_ellipses_on_grid()
    #test_ellipse_center()
    #test_normalized_ellipse()
    #test_ellipse_ellipse_intersection()
    #test_compute_bitangent_lines()
    #test_compute_homography_to_unit_line()
    #test_projective_line_conic_intersection()
    #test_tangent_ellipse_intersection()
    #test_line_region_intersection()
    #test_apply_homography_to_mask()
    #test_interpolate_on_circle()
    #test_apply_homography_to_ray()
    #test_ray_region_intersection()
    test_line_segment_region_intersection()
