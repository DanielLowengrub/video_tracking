import numpy as np
import multiprocessing as mp
from ...source.auxillary.pool import DataProcessor, Pool
import sys

class MatmulProcessor(object):
    def __init__(self, size, num_mats):
        self._size = size
        self._num_mats = num_mats

    def initialize(self):
        print '[%s] initializing writer...' % mp.current_process().name
        sys.stdout.flush()
        return

    def close(self):
        print '[%s] closing writer...' % mp.current_process().name
        sys.stdout.flush()
        return

    def process_data(self, data, exit_event):
        print '[%s] starting job %d' % (mp.current_process().name, data)
        sys.stdout.flush()
        
        for i in range(self._num_mats):
            if exit_event.is_set():
                break
            M1 = np.random.rand(self._size, self._size)
            M2 = np.random.rand(self._size, self._size)
            M  = np.matmul(M1,M2)
            
        return

def test():
    data_processor = MatmulProcessor(100, 10000)
    p = Pool(8, data_processor, range(10))
    p.run()

if __name__ == '__main__':
    test()
