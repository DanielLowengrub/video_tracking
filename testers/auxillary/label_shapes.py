import numpy as np
import collections, itertools, functools
import cv2
from ...source.auxillary import planar_geometry

##############################
# This file provides a method for labelling a collection of shapes with text
##############################

FONT_TYPE = cv2.FONT_HERSHEY_COMPLEX_SMALL
FONT_SCALE = 0.8
FONT_COLOR = (255, 0, 0)
LINE_THICKNESS = 1
LINE_TYPE = cv2.CV_AA

def point_in_region_filter(region):
    return lambda x: (x is not None) and planar_geometry.point_in_region(x, region)

def interval_in_region(region, interval_on_shape):
    shape_with_position = interval_on_shape.get_shape_with_position()
    middle_point = shape_with_position.compute_absolute_position(interval_on_shape.compute_middle_position())
    return planar_geometry.point_in_region(middle_point, region)

def get_image_region(image):
    return np.array([[0,0],[image.shape[1],image.shape[0]]])

def compute_intersection_with_boundary_positions(image, shape_with_position_dict):
    '''
    shape_with_position_dict - a dictionary whose keys are strings and whose values are ShapeWithPosition objects

    Return a dictionary whose keys are the keys in the shape dict. 
    For each key, we store a np array of the positions where the shape intersects the boundary of the image
    '''
    #print 'computing intersections with boundaries...'
    
    #we first create a dictionary of intersection positions between the shapes and themselves, and with the boundaries of the image
    intersection_position_dict = collections.defaultdict(functools.partial(np.empty,0))

    image_region = get_image_region(image)
    boundary_lines = planar_geometry.get_region_boundary_lines(image_region)
    #get the intersections of each shape with the region boundaries
    for key, shape_with_position in shape_with_position_dict.iteritems():
        shape = shape_with_position.get_shape()
        #print '   computing the intersection of %s with the boundary' % str(shape)
        
        if shape.is_line():
            intersection_points = filter(point_in_region_filter(image_region),
                                         (planar_geometry.affine_line_line_intersection(shape, line) for line in boundary_lines))
            
        elif shape.is_ellipse():
            intersection_points = filter(lambda x: len(x)==2,
                                         (planar_geometry.line_ellipse_intersection(line, shape) for line in boundary_lines))

            if len(intersection_points) > 0:
                intersection_points = np.vstack(intersection_points)
                intersection_points = filter(point_in_region_filter(image_region), intersection_points)
            
        #print '   intersection points: ', intersection_points

        if len(intersection_points) > 0:
            intersection_positions = shape_with_position.compute_positions(np.vstack(intersection_points))
            intersection_position_dict[key] = intersection_positions

    return intersection_position_dict

def compute_intersection_with_shape_positions(image, shape_with_position_dict):
    '''
    shape_with_position_dict - a dictionary whose keys are strings and whose values are ShapeWithPosition objects

    Return a dictionary whose keys are the keys in the shape dict. 
    For each key, we store a np array of the positions where the shape intersects other shapes inside the image
    '''

    intersection_position_dict = collections.defaultdict(functools.partial(np.empty,0))
    image_region = get_image_region(image)
    
    for (key_a, shape_with_position_a), (key_b, shape_with_position_b) in itertools.combinations(shape_with_position_dict.iteritems(), 2):
        shape_a = shape_with_position_a.get_shape()
        shape_b = shape_with_position_b.get_shape()

        if shape_a.is_line() and shape_b.is_line():
            intersection_points = [planar_geometry.affine_line_line_intersection(shape_a, shape_b)]

        elif shape_a.is_line() and shape_b.is_ellipse():
            intersection_points = planar_geometry.line_ellipse_intersection(shape_a, shape_b)

        elif shape_a.is_ellipse() and shape_b.is_line():
            intersection_points = planar_geometry.line_ellipse_intersection(shape_b, shape_a)

        else:
            intersection_points = []

        intersection_points = filter(point_in_region_filter(image_region), intersection_points)

        if len(intersection_points) > 0:
            intersection_positions_a = shape_with_position_a.compute_positions(np.vstack(intersection_points))
            intersection_positions_b = shape_with_position_b.compute_positions(np.vstack(intersection_points))
            intersection_position_dict[key_a] = np.hstack([intersection_position_dict[key_a], intersection_positions_a])
            intersection_position_dict[key_b] = np.hstack([intersection_position_dict[key_b], intersection_positions_b])

    return intersection_position_dict
        
def compute_label_positions(image, shape_with_position_dict):
    '''
    shape_with_position_dict - a dictionary whose keys are strings and whose values are ShapeWithPosition objects

    Return a dictionary whose keys are the keys in the shape dict. For each key, we store the position of the label of the corresponding shape.
    '''
    #print 'shape with position dict:'
    #print shape_with_position_dict
    #print 'computing label positions...'
    image_region = get_image_region(image)
    
    #for each shape, store a sorted list of the positions where it intersects either the boundary of the image or other shapes
    intersection_with_boundary_positions = compute_intersection_with_boundary_positions(image, shape_with_position_dict)
    intersection_with_shape_positions = compute_intersection_with_shape_positions(image, shape_with_position_dict)
    position_dict = dict((k, np.hstack([intersection_with_boundary_positions[k], intersection_with_shape_positions[k]])) for k in shape_with_position_dict.iterkeys())

    #print 'boundary positions: ', ', '.join('%s: %s' % (k, str(position)) for k,position in intersection_with_boundary_positions.iteritems())
    #print 'shape intersection positions: ', ', '.join('%s: %s' % (k, str(position)) for k,position in intersection_with_shape_positions.iteritems())
    #print 'all positions: ', ', '.join('%s: %s' % (k, str(position)) for k,position in position_dict.iteritems())
    
    #for each shape, create a list of intervals connecting the positions
    interval_dict = dict((k, shape_with_position.get_intervals_between_positions(position_dict[k])) for k,shape_with_position in shape_with_position_dict.iteritems())

    #print 'intervals: ', ', '.join('%s: %s' % (k, map(str,intervals)) for k,intervals in interval_dict.iteritems())
    
    #remove the intervals that are not contained in the image
    interval_dict.update((k, filter(functools.partial(interval_in_region, image_region), intervals)) for k,intervals in interval_dict.iteritems())

    #remove the shapes that do not have any intervals
    interval_dict = dict((k,intervals) for k,intervals in interval_dict.iteritems() if len(intervals)>0)
    
    #print 'updated intervals: ', ', '.join('%s: %s' % (k, map(str,intervals)) for k,intervals in interval_dict.iteritems())
    
    #for each shape, find the longest interval
    interval_dict.update((k, max(intervals, key=lambda x: x.length())) for k,intervals in interval_dict.iteritems())

    #print 'longest intervals: ', ', '.join('%s: %s' % (k, str(interval)) for k,interval in interval_dict.iteritems())
    
    #the position of the label is the center of the the longest interval
    label_position_dict = dict((k, np.int32(interval.get_shape_with_position().compute_absolute_position(interval.compute_middle_position())))
                               for k,interval in interval_dict.iteritems())

    return label_position_dict

def draw_label(image, position, label_string):
    '''
    image - a numpy array of shape (n,m,3) and type np.uint8
    position - a length 2 numpy array of type np.int32
    label_string - a string
    
    Draw the label string at the given position
    '''

    cv2.putText(image, label_string, tuple(position.tolist()), FONT_TYPE, FONT_SCALE, FONT_COLOR, LINE_THICKNESS, LINE_TYPE)
    
def label_shapes(image, shape_with_position_dict):
    '''
    shape_with_position_dict - a dictionary whose keys are strings and whose values are ShapeWithPosition objects

    For each pair (text label, shape) we draw the text on the image near the shape.
    '''
    #print 'drawing labels...'
    
    #get the label positions
    label_position_dict = compute_label_positions(image, shape_with_position_dict)

    #print 'label position dict:'
    #print ', '.join('%s: %s' % (k, str(position)) for k,position in label_position_dict.iteritems())
    
    #draw each label at the specified position
    for label_string, position in label_position_dict.iteritems():
        draw_label(image, position, label_string)

def test():
    from ...source.auxillary.fit_points_to_shape.line import Line
    from ...source.auxillary.contour_shape_overlap.line_with_position import LineWithPosition
    from ..auxillary import primitive_drawing
    
    image = np.zeros((400,400,3), np.uint8)

    lines_with_position = []
    lines_with_position.append(LineWithPosition(Line.from_point_and_direction(np.array([0,200]), np.array([1,0]))))
    lines_with_position.append(LineWithPosition(Line.from_point_and_direction(np.array([200,0]), np.array([0,1]))))
    lines_with_position.append(LineWithPosition(Line.from_point_and_direction(np.array([300,0]), np.array([0,1]))))

    shape_with_position_dict = dict(('line_%d' % i, line_with_position) for i,line_with_position in enumerate(lines_with_position))

    for line_with_position in lines_with_position:
        primitive_drawing.draw_line(image, line_with_position.get_shape(), (0,255,0))
        
    label_shapes(image, shape_with_position_dict)

    cv2.imshow('image with shape labels', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

if __name__ == '__main__':
    test()
