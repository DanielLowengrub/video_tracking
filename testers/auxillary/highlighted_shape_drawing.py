import numpy as np
from ..auxillary import primitive_drawing
from ...source.auxillary import mask_operations

def draw_highlighted_shape(image, highlighted_shape, shape_color=(0,255,0),
                           highlighted_color=(255,0,0), highlighted_alpha=0.6):
    '''
    image - a numpy array with shape (n,m,3) and type np.uint8
    highlighted_shape - a HighlightedShape object
    shape_color - a tuple of ints (B,G,R)
    highlighted_color - a tuple of ints (B,G,R)
    highlighted_alpha - a float between 0 and 1

    draw the highlighted shape in the image
    '''
    primitive_drawing.draw_shape(image, highlighted_shape.get_shape(), shape_color)

    mask = highlighted_shape.generate_mask(image)
    mask_operations.draw_mask(image, mask, np.array(highlighted_color), highlighted_alpha)

    return
