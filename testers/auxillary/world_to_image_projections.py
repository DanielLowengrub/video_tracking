import os
import numpy as np
import cv2
from ...source.auxillary.world_to_image_projections import project_player_with_variance, project_players_to_image, build_player_masks, _extract_lines, project_infinite_cylinder_to_image, project_cylinder_to_image
from ...source.auxillary.mask_operations import mix_images_with_mask
from ...source.data_structures.frame_sequence.frame_sequence import FrameSequence
from ...source.auxillary.camera_matrix import CameraMatrix
from ...source.auxillary.fit_points_to_shape.ellipse import Ellipse
from ...source.auxillary.fit_points_to_shape.line import Line
from ..auxillary import primitive_drawing
from ...source.auxillary import planar_geometry

##############################################################
# Here we test some of the methods in the camera matrix class.
##############################################################

PARENT_DIR = 'output/tester/frame_sequences/frame_sequence_test_3'
FRAME_SEQUENCE_NAME = 'frame_sequence_50_79-0_29'
FRAME_SEQUENCE_DIR = os.path.join(PARENT_DIR, 'frame_sequences_50_79-0_29')
PLAYER_DATA_DIR = os.path.join('output/tester/player_tracker/player_positions', FRAME_SEQUENCE_NAME)

PLAYER_POSITIONS_FILE = os.path.join(PLAYER_DATA_DIR, 'player_positions.npy')

FRAME_INDEX = 1
PLAYER_INDEX = 6
PLAYER_VARIANCE = np.array([10,10])

def load_player_absolute_positions(frame_index=None):
    '''
    Return a list containing the positions in absolute coordinates at the given frame of the player with ids PLAYER_IDS
    '''
    absolute_positions = np.load(PLAYER_POSITIONS_FILE)

    if frame_index is None:
        return absolute_positions

    else:
        return absolute_positions[:,frame_index,:]

def project_player_with_variance_test():
    '''
    We load a frame from a frame sequence and project an ellipse centered on a player.
    '''
    frame = 50
    #load an image and camera
    IMAGE_FILE = 'test_data/video4/image_%d.png' % frame
    CAMERA_FILE = 'output/scripts/preprocessing/data/images_50_269/camera/camera_0_219/camera_%d.npy' % frame
    PLAYER_POSITIONS_FILE = 'output/scripts/preprocessing/data/images_50_269/players/players_0_219/players_%d/positions.npy' % frame

    image = cv2.imread(IMAGE_FILE, 1)
    camera_matrix = CameraMatrix(np.load(CAMERA_FILE))
    positions = np.load(PLAYER_POSITIONS_FILE)

    absolute_position = positions[2]
    player_mask, player_rectangle = project_player_with_variance(image, camera_matrix, absolute_position, PLAYER_VARIANCE)

    print 'player rectangle: ', player_rectangle
    print 'with shape: ', player_rectangle.get_shape()
    print 'shape of player mask: ', player_mask.shape
    
    cv2.imshow('player mask', np.float32(player_mask))
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    #mix the mask and the image
    image_with_blue = image.copy()
    player_region = player_rectangle.get_slice(image_with_blue)

    print 'shape of player region: ', player_region.shape
    
    player_region[player_mask] = np.uint8([255,0,0])
    
    alpha = 0.2
    image = np.uint8((alpha * image_with_blue) + ((1-alpha) * image))
    
    cv2.imshow('projected player variance', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def test_project_players_to_image():
    #load an image and camera
    IMAGE_FILE = 'test_data/video4/image_50.png'
    CAMERA_FILE = 'output/scripts/preprocessing/data/images_50_269/camera/camera_0_219/camera_50.npy'
    PLAYER_POSITIONS_FILE = 'output/scripts/preprocessing/data/images_50_269/players/players_0_219/players_50/positions.npy'

    image = cv2.imread(IMAGE_FILE, 1)
    camera_matrix = CameraMatrix(np.load(CAMERA_FILE))
    positions = np.load(PLAYER_POSITIONS_FILE)

    print 'positions: '
    print positions
    
    E = project_players_to_image(camera_matrix, positions)

    ellipses = [Ellipse.from_parametric_representation(e) for e in E]

    print 'ellipses:'
    print '\n'.join(map(str, ellipses))
    
    for ellipse in ellipses:
        primitive_drawing.draw_ellipse(image, ellipse, (0,255,0))

    cv2.imshow('projected ellipses', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def test_build_player_masks():
    #load an image and camera
    IMAGE_FILE = 'test_data/video4/image_50.png'
    CAMERA_FILE = 'output/scripts/preprocessing/data/images_50_269/camera/camera_0_219/camera_50.npy'
    PLAYER_POSITIONS_FILE = 'output/scripts/preprocessing/data/images_50_269/players/players_0_219/players_50/positions.npy'
    alpha = 0.6
    
    image = cv2.imread(IMAGE_FILE, 1)
    camera_matrix = CameraMatrix(np.load(CAMERA_FILE))
    positions = np.load(PLAYER_POSITIONS_FILE)

    cv2.imshow('image', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    player_rectangles, player_masks = build_player_masks(camera_matrix, positions)

    for rectangle, mask in zip(player_rectangles, player_masks):
        image_slice = rectangle.get_slice(image)
        blue_image = image_slice.copy()
        blue_image[mask] = np.array([255,0,0])
        image_slice[...] = alpha*blue_image + (1-alpha)*image_slice

    cv2.imshow('player masks', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def test_extract_lines():
    l1 = np.random.normal(size=(3,))
    l2 = np.random.normal(size=(3,))

    C = np.matmul(l1.reshape((3,1)), l2.reshape((1,3)))

    print 'C1:'
    print C
    
    C = C + C.transpose()

    print 'l1: ', l1
    print 'l2: ', l2

    L1 = Line.from_parametric_representation(l1)
    L2 = Line.from_parametric_representation(l2)
    p = planar_geometry.affine_line_line_intersection(L1,L2)
    print 'intersection point: ', p
    
    print 'C: '
    print C

    m1, m2 = _extract_lines(C)

    print 'l1: ', l1 / np.linalg.norm(l1)
    print 'l2: ', l2 / np.linalg.norm(l2)
    print 'm1: ', m1 / np.linalg.norm(m1)
    print 'm2: ', m2 / np.linalg.norm(m2)

def test_project_infinite_cylinder_to_image():
    camera_position = np.array([50,100,10])
    camera_direction = np.array([-1,0,-1])
    f = 1
    camera_matrix = CameraMatrix.from_camera_position_and_direction(camera_position, camera_direction, f)

    cylinder_position = np.array([0,100])
    cylinder_axes = np.array([1,1])
    
    lines = project_infinite_cylinder_to_image(camera_matrix, cylinder_position, cylinder_axes)

    print 'lines: '
    print lines

def test_project_infinite_cylinder_to_soccer_image():
    frame = 3
    #load an image and camera
    IMAGE_FILE = 'test_data/video7/image_%d.png' % frame
    CAMERA_FILE = 'output/scripts/preprocessing/data/video7/camera/camera_0_274/camera_%d.npy' % frame
    PLAYER_POSITIONS_FILE = 'output/scripts/preprocessing/data/video7/players/players_0_274/players_%d/positions.npy' % frame

    image = cv2.imread(IMAGE_FILE, 1)
    camera_matrix = CameraMatrix(np.load(CAMERA_FILE))
    positions = np.load(PLAYER_POSITIONS_FILE)
    #cylinder_position = positions[1]
    cylinder_position = np.array([475.50692102, 273.00310178])
    cylinder_axes = np.array([5,5])

    cylinder_lines = project_infinite_cylinder_to_image(camera_matrix, cylinder_position, cylinder_axes)

    for line in cylinder_lines:
        primitive_drawing.draw_line(image, line, (255,0,0))

    cv2.imshow('cylinder', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def test_project_cylinder_to_soccer_image():
    frame = 122
    
    #load an image and camera
    IMAGE_FILE = 'test_data/video7/image_%d.png' % frame
    CAMERA_FILE = 'output/scripts/preprocessing/data/video7/camera/camera_0_274/camera_%d.npy' % frame
    PLAYER_POSITIONS_FILE = 'output/scripts/preprocessing/data/video7/players/players_0_274/players_%d/positions.npy' % frame

    image = cv2.imread(IMAGE_FILE, 1)
    camera_matrix = CameraMatrix(np.load(CAMERA_FILE))
    positions = np.load(PLAYER_POSITIONS_FILE)
    #cylinder_position = positions[0]
    cylinder_position = np.array([543.11562644, 252.77713521])

    cylinder_axes = np.array([2,2])
    cylinder_height = 15
    rectangle, mask = project_cylinder_to_image(image.copy(), camera_matrix, cylinder_position, cylinder_axes, cylinder_height)

    alpha = 0.4
    image_slice = rectangle.get_slice(image)
    blue_image = image_slice.copy()
    blue_image[mask] = np.array([255,0,0])
    image_slice[...] = (alpha*blue_image) + ((1-alpha)*image_slice)

    cv2.imshow('cylinder', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

if __name__ == '__main__':
    np.set_printoptions(precision=6, linewidth=200)
    #project_player_with_variance_test()
    #test_project_players_to_image()
    #test_build_player_masks()
    #test_extract_lines()
    #test_project_infinite_cylinder_to_image()
    #test_project_infinite_cylinder_to_soccer_image()
    test_project_cylinder_to_soccer_image()
