import numpy as np
from ...source.auxillary import np_operations

def test_group_by_row():
    a = np.zeros((50,2), np.int32)
    a[:10,0] = 1
    a[40:,0] = 1
    a[15:25,1] = 1

    print 'grouping the following array by row:'
    print a

    print 'start grouping...'
    group_representatives, group_ids = np_operations.group_by_row(a)
    print 'finished'

    print 'group ids:'
    print group_ids

    print 'groups:'
    for i, rep in enumerate(group_representatives):
        print 'group with rep %s: ' % rep
        print a[group_ids==i]

def test_find_unique_rows():
    a = np.zeros((10,2), np.float64)
    a[:3,0] = 1
    a[5:7,0] = 1.005
    print 'a:'
    print a

    unique_rows = np_operations.find_unique_rows(a, decimals=2)

    print 'unique rows:'
    print unique_rows
    
def test_adjugate():
    A = np.random.normal(size=(3,3))

    print 'A:'
    print A
    
    adjA = np_operations.adjugate(A)

    print 'A*adjA = '
    print np.matmul(A, adjA)
    print 'detA = ', np.linalg.det(A)

def test_poly_adjugate():
    A = np.ones((3,3))
    B = np.ones((3,3)) * 2
    B[0,1] = 3
    poly_adj = np_operations.poly_adjugate(A,B)

    print 'A:'
    print A
    print 'B:'
    print B
    print 'adj(A + xB):'
    print poly_adj

def test_find_points_in_mask():
    mask = np.zeros((3,3), np.bool)
    mask[0,0] = True
    mask[2,0] = True
    mask[1,2] = True

    print 'the mask is:'
    print mask

    points_in_mask = np_operations.find_points_in_mask(mask)

    print 'points in the mask:'
    print points_in_mask

    return

def test_substitute_values():
    a = np.array([0,0,1,0,2,2,1,1,1,0])
    sub_dict = {0:3, 1:0, 2:1}
    sub_a = np_operations.substitute_values(a, sub_dict)

    print 'a:'
    print a
    print 'sub dict:'
    print sub_dict
    print 'sub_a:'
    print sub_a

def test_find_slice():
    array = np.array([5,6,7,9,10,15,16,17,18])
    interval = (9,16)
    print 'array: ', array
    print 'interval: ', interval
    print 'slice: ', array[np_operations.find_slice(interval, array)]

def test_group_points_by_distance():
    max_dist = 5
    points = np.array([[10,1],
                       [11,0],
                       [2,20],
                       [2,21],
                       [-5,-5]], np.float32)
    print 'points:\n', points
    num_groups, point_labels = np_operations.group_points_by_distance(points, max_dist)

    print 'num groups: ', num_groups
    print 'point labels: ', point_labels
    
if __name__ == '__main__':
    #test_group_by_row()
    #test_adjugate()
    #test_poly_adjugate()
    #test_find_unique_rows()
    #test_find_points_in_mask()
    #test_substitute_values()
    #test_find_slice()
    test_group_points_by_distance()
