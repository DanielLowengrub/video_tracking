import cv2
import numpy as np
from ...source.auxillary import planar_geometry

def draw_line(image, line, color, thickness=2):
    #intersect the line with the rectangle surrounding the image
    image_region = planar_geometry.get_image_region(image)
    intersection_points = planar_geometry.line_region_intersection(line, image_region)

    #if there were intersection points, draw the line between them
    if (intersection_points is not None) and len(intersection_points) == 2:
        intersection_points = np.int32(intersection_points)
        cv2.line(image,
                 tuple(intersection_points[0].tolist()),
                 tuple(intersection_points[1].tolist()),
                 color, thickness)
    
    return

def draw_rectangle(image, rectangle, color, thickness=2):
    tl = tuple(rectangle.get_top_left().tolist())
    br = tuple(rectangle.get_bottom_right().tolist())

    cv2.rectangle(image, tl, br, color, thickness)
    return

def draw_ellipse(image, ellipse, color, thickness=2):
    ellipse.compute_geometric_parameters()
    center, axes, angle = ellipse.get_geometric_parameters(in_degrees=True)
    cv2.ellipse(image, tuple(np.int32(center).tolist()), tuple(np.int32(axes).tolist()), angle, 0, 360, color, thickness, cv2.CV_AA)
    return

def draw_shape(image, shape, color, thickness=2):
    if shape.is_line():
        draw_line(image, shape, color, thickness)

    elif shape.is_ellipse():
        draw_ellipse(image, shape, color, thickness)

    else:
        raise ValueError('We only can draw lines and ellipses')

    return

def draw_point(image, point, color, size=3, thickness=-1):
    cv2.circle(image, tuple(np.int32(point).tolist()), size, color, thickness)
    return

def draw_points(image, points, color, size=3):
    for p in points:
        draw_point(image, p, color, size)

    return
