import numpy as np
from ...source.auxillary.plane import Plane

def test_compute_line_intersection():
    #the plane is th x-y plane
    plane_origin = np.array([0,0,0], np.float32)
    plane_x_axis = np.array([1,0,1], np.float32)
    plane_y_axis = np.array([0,1,0], np.float32)
    plane = Plane(plane_origin, plane_x_axis, plane_y_axis)

    #the line is the z axis
    line = np.array([[0,0,1,-1],
                     [0,1,0,0]], np.float32)

    print 'plane: ', plane
    print 'line: '
    print line
    
    print 'intersecting the plane and line...'
    plane_point = plane.compute_line_intersection(line)
    print 'the intersection point in plane coords is: ', plane_point
    print 'the intersection point in world coords is: ', plane.plane_position_to_world_coordinates(plane_point)

def test_compute_lines_intersection():
    #the plane is th x-y plane
    plane_origin = np.array([0,0,0], np.float32)
    plane_x_axis = np.array([1,0,0], np.float32)
    plane_y_axis = np.array([0,1,0], np.float32)
    plane = Plane(plane_origin, plane_x_axis, plane_y_axis)

    #the line is the z axis
    line = np.array([[1,0,0,-2],
                     [0,1,0,3]], np.float32)

    lines = np.stack([line, line])
    print 'plane: ', plane
    print 'lines: '
    print lines

    plane_points = plane.compute_lines_intersection(lines)
    print 'the intersection points in plane coords are: '
    print plane_points
    
    print 'the intersection point in world coords are: '
    print plane.plane_positions_to_world_coordinates(plane_points)

if __name__ == '__main__':
    #test_compute_line_intersection()
    test_compute_lines_intersection()
