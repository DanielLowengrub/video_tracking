import numpy as np
import cv2
import os
from ...source.auxillary.soccer_field_geometry import SoccerFieldGeometry
from ..auxillary import label_shapes, primitive_drawing
from ...source.auxillary import planar_geometry
from ...source.auxillary.contour_shape_overlap.line_with_position import LineWithPosition
from ...source.auxillary.contour_shape_overlap.ellipse_with_position import EllipseWithPosition

"""
This is used to inspect absolute homography matrices.
"""

FRAME_INDEX = 132
INTERVAL = (0, 269)
IMAGE_DIRECTORY = 'test_data/images_0_269'
PREPROCESSING_NAME = 'images_0_269'

ABSOLUTE_HOMOGRAPHY_DIRECTORY = os.path.join('output/scripts/preprocessing/data', PREPROCESSING_NAME, 'absolute_homography')

IMAGE_FILE = os.path.join(IMAGE_DIRECTORY, 'image_%d.png' % FRAME_INDEX)
HOMOGRAPHY_FILE = os.path.join(ABSOLUTE_HOMOGRAPHY_DIRECTORY, 'homography_%d_%d' % INTERVAL, 'homography_%d.npy' % FRAME_INDEX)

def load_absolute_soccer_field():
    '''
    Return a tuple (absolute shapes, absolute grid points)
    '''
    ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)
    SCALE = 6
    FIELD_WIDTH = absolute_image.shape[1]-1
    FIELD_LENGTH = absolute_image.shape[0]-1
    GRID_WIDTH = 20
    GRID_LENGTH = 14
    soccer_field = SoccerFieldGeometry(SCALE, FIELD_WIDTH, FIELD_LENGTH, GRID_WIDTH, GRID_LENGTH)

    absolute_shape_dict = soccer_field.get_highlighted_shape_dict(format_keys=True)
    absolute_grid_points = soccer_field.get_grid_points()
    
    return absolute_shape_dict, absolute_grid_points

def draw_absolute_shapes(image, H):
    absolute_shape_dict, absolute_grid_points = load_absolute_soccer_field()

    labelled_transformed_shapes = dict()
    
    for k, highlighted_shape in absolute_shape_dict.iteritems():
        shape = highlighted_shape.get_shape()
        transformed_shape = planar_geometry.apply_homography_to_shape(H, shape)

        primitive_drawing.draw_shape(image, transformed_shape, (0,255,0))

        print 'drawing transformation of: ', k
        print 'the original shape is: ', shape
        print 'the transformed shape is: ', transformed_shape

        cv2.imshow('image with transformed sidelines', image)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

        if transformed_shape.is_line():
            swp = LineWithPosition(transformed_shape)
        elif transformed_shape.is_ellipse():
            swp = EllipseWithPosition(transformed_shape)

        labelled_transformed_shapes[k] = swp
    
    label_shapes.label_shapes(image, labelled_transformed_shapes)
    cv2.imshow('image with transformed sidelines', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    return

if __name__ == '__main__':
    image = cv2.imread(IMAGE_FILE, 1)
    H = np.load(HOMOGRAPHY_FILE)
    
    draw_absolute_shapes(image, H)
