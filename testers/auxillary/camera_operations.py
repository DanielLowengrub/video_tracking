import numpy as np
import cv2
from ...source.auxillary import camera_operations
from ...source.auxillary.soccer_field_geometry import SoccerFieldGeometry

def test_build_camera():
    IMAGE_FILENAME = 'test_data/test_pattern.png'
    CAMERA_POSITION  = np.array([64, 200, 60])

    THETA_X = (90+10) * np.pi/180
    THETA_Z = 0 * np.pi/180
    FOCUS = 200
    ASPECT = 1
    OUTPUT_IMAGE_SHAPE = (256, 256)
    CAMERA_CENTER = np.array(OUTPUT_IMAGE_SHAPE) / 2

    image = cv2.imread(IMAGE_FILENAME, 1)
    camera_parameters = camera_operations.CameraParameters(THETA_X, THETA_Z, FOCUS, ASPECT, CAMERA_CENTER, CAMERA_POSITION)
    camera = camera_operations.build_camera(camera_parameters)
    output_image = camera_operations.apply_camera(image, camera, OUTPUT_IMAGE_SHAPE)

    cv2.imshow('output', output_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    
    return

def test_compute_camera_parameters():
    CAMERA_POSITION  = np.array([64, 200, 60])
    THETA_X = (90-10) * np.pi/180
    THETA_Z = 10 * np.pi/180
    FOCUS = 200
    ASPECT = 1
    OUTPUT_IMAGE_SHAPE = (256, 256)
    CAMERA_CENTER = np.array(OUTPUT_IMAGE_SHAPE) / 2

    camera_parameters = camera_operations.CameraParameters(THETA_X, THETA_Z, FOCUS, ASPECT, CAMERA_CENTER, CAMERA_POSITION)
    camera = camera_operations.build_camera(camera_parameters)
    computed_parameters = camera_operations.compute_camera_parameters(camera)

    print 'original parameters:'
    print camera_parameters
    print 'computed parameters:'
    print computed_parameters

def test_camera_on_sidelines():
    SOCCER_FIELD = SoccerFieldGeometry(6, 743, 479, 20, 14)
    SIDELINE_MASK = SOCCER_FIELD.get_sideline_mask(10)
    
    OUTPUT_IMAGE_SHAPE = (352, 624)

    CAMERA_POSITION = np.array([372, 700, 400], np.float32)
    THETA_X = (90 + 20) * np.pi/180
    THETA_Z = 0 * np.pi/180
    FOCUS = 100
    ASPECT = 1.0
    CAMERA_CENTER = np.array(OUTPUT_IMAGE_SHAPE[::-1], np.float32) / 2.0

    camera_parameters = camera_operations.CameraParameters(THETA_X, THETA_Z, FOCUS, ASPECT,
                                                           CAMERA_CENTER, CAMERA_POSITION)
    camera = camera_operations.build_camera(camera_parameters)
    output_image = camera_operations.apply_camera(SIDELINE_MASK, camera, OUTPUT_IMAGE_SHAPE)

    cv2.imshow('output image', output_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    return


if __name__ == '__main__':
    #test_build_camera()
    #test_compute_camera_parameters()
    test_camera_on_sidelines()
