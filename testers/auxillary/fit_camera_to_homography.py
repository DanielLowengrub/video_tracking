from ...source.auxillary import fit_camera_to_homography
from ...source.auxillary.camera_matrix import CameraMatrix
import pickle
import numpy as np
import os
import cv2

CAMERA_FILE = os.path.join('/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts',
                           'preprocessing', 'data', 'video7', 'camera_sample',
                           'camera_sample_0_274', 'camera_%d.npy' % 238)

TARGET_FRAME = 259
TRACKED_PLAYERS_FILE = os.path.join('/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts',
                                    'player_tracker', 'data', 'video7', 
                                    'tracked_players_0_274.pckl')

HOMOGRAPHY_FILE = os.path.join('/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts',
                               'preprocessing', 'data', 'video7', 'absolute_homography',
                               'homography_0_274', 'homography_%d.npy' % TARGET_FRAME)

IMAGE_FILE = os.path.join('/Users/daniel/Dropbox/soccer/daniel_refactored', 'test_data', 'video7',
                          'image_%d.png' % TARGET_FRAME)

PLAYER_MASK_VARIANCE = np.array([2,2])
PLAYER_MASK_COLOR = np.array([255,0,0])
PLAYER_MASK_ALPHA = 0.4
PLAYER_HEIGHT = 12

def test():
    camera = CameraMatrix(np.load(camera_file))
    C_initial = camera.get_internal_parameters()
    R_initial = camera.get_world_to_camera_rotation_matrix()
    
    T = camera.get_camera_position() + np.array([10,-20,5])
    H = camera.get_homography_matrix()

    print 'the initial camera matrix:'
    print camera.get_numpy_matrix()

    print 'C_initial:'
    print C_initial
    print 'R_initial:'
    print R_initial
    print 'T: ', T
    print 'H:'
    print H
    
    C_opt, R_opt = fit_camera_to_homography.fit_camera_to_homography(C_initial, R_initial, T, H)

    camera_opt = CameraMatrix.from_position_rotation_internal_parameters(T, R_opt, C_opt)

    print 'C_opt:'
    print C_opt
    print 'R_opt:'
    print R_opt
    print 'optimized camera:'
    print camera_opt.get_numpy_matrix()

    print 'optimized H:'
    print camera_opt.get_homography_matrix()

def test_interpolation():
    camera = CameraMatrix(np.load(CAMERA_FILE))
    f = open(TRACKED_PLAYERS_FILE, 'rb')
    tracked_players = pickle.load(f)
    f.close()
    H_target = np.load(HOMOGRAPHY_FILE)
    image = cv2.imread(IMAGE_FILE, 1)
    
    C_initial = camera.get_internal_parameters()
    R_initial = camera.get_world_to_camera_rotation_matrix()
    T = camera.get_camera_position() + np.array([10,-20,5])

    print 'the initial camera matrix:'
    print camera.get_numpy_matrix()

    print 'camera position: ', T
    
    C_opt, R_opt = fit_camera_to_homography.fit_camera_to_homography(C_initial, R_initial, T, H_target)
    target_camera = CameraMatrix.from_position_rotation_internal_parameters(T, R_opt, C_opt)

    print 'target homography:'
    print H_target
    print 'the homography we found:'
    print target_camera.get_homography_matrix()
    alpha = target_camera.get_alpha()
    print 'alpha: ', alpha
    
    target_camera = CameraMatrix.from_homography_and_alpha(H_target, alpha)

    print 'target camera:'
    print target_camera.get_numpy_matrix()

    target_player_mask = tracked_players.get_player_mask_in_frame(TARGET_FRAME, image, target_camera,
                                                                  PLAYER_MASK_VARIANCE, PLAYER_HEIGHT)

    colored_mask = image.copy()
    colored_mask[target_player_mask] = PLAYER_MASK_COLOR
    image = np.uint8((PLAYER_MASK_ALPHA * colored_mask) + ((1 - PLAYER_MASK_ALPHA)*image))
    
    cv2.imshow('player mask', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    
if __name__ == '__main__':
    test_interpolation()
