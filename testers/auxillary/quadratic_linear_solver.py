import numpy as np
from numpy.polynomial.polynomial import polyval, polymul

from ...source.auxillary import quadratic_linear_solver

def basic_test():
    #Set up a system of equations
    #Q = np.array([[1, 1, 0],
    #              [1, 1, 1],
    #              [1, 0, 1]], np.float32)

    Q = np.array([[1, 1, 1],
                  [1, 0, 1]], np.float32)

    r = 4
    s = np.array([1])
    
    #compute the values of alpha[j,i] using Q, s and r
    Qr = np.array([polyval(r, q) for q in Q])

    #since Q[i](r) + alpha[i]*s_ = 0, alpha[i] = -Q[i](r) / s
    alpha = -Qr / s

    print 'testing the solver with:'
    print 'Q:'
    print Q
    print 'alpha: '
    print alpha

    print 'we want to get:'
    print 'r = ', r
    print 's = ', s

    r_and_s = quadratic_linear_solver.solve_least_squares([(Q, alpha)])

    print r_and_s
    
    print 'we got:'
    print 'r = ', r_and_s[:,0]
    print 's = ', r_and_s[:,1:]

def two_systems():
    #Set up two systems of equations
    Q1 = np.array([[1, 2, 0],
                   [1, 1, 1],
                   [1, 0, 1]], np.float32) 
                  
    Q2 = np.array([[1, 2, 1],
                   [2, 0, 1]], np.float32)

    r = 0.5
    s1 = 1.01
    s2 = -2.5
    
    #compute the values of alpha[j,i] using Q, s and r
    Q1r = np.array([polyval(r, q) for q in Q1])
    Q2r = np.array([polyval(r, q) for q in Q2])
    
    #since Q[i](r) + alpha[i]*s_ = 0, alpha[i] = -Q[i](r) / s
    alpha1 = -Q1r / s1
    alpha2 = -Q2r / s2

    #we add some noise
    #Q1 += 0.01*np.random.normal(size=Q1.shape)
    #Q2 += 0.01*np.random.normal(size=Q2.shape)

    systems_of_equations = [(Q1, alpha1), (Q2, alpha2)]
    
    print 'testing the solver with:'
    for i,(Q,alpha) in enumerate(systems_of_equations):
        print 'system %d' % i
        print '   Q:'
        print Q
        print '   alpha: ', alpha
    
    print 'we want to get:'
    print 'r = ', r
    print 's = ', np.array([s1,s2])

    r_and_s = quadratic_linear_solver.solve_least_squares(systems_of_equations)

    print r_and_s
    
    print 'we got:'
    print 'r = ', r_and_s[:,0]
    print 's = ', r_and_s[:,1:]

if __name__ == '__main__':
    #basic_test()
    two_systems()
