import numpy as np
import cv2
from ...source.auxillary import polygon_operations, mask_operations, camera_operations
from ...source.auxillary.fit_points_to_shape.line import Line
from ...source.auxillary.soccer_field_geometry import SoccerFieldGeometry
from ..auxillary import primitive_drawing

def test_visible_polygon_on_sidelines():
    SOCCER_FIELD = SoccerFieldGeometry(6, 743, 479, 20, 14)
    SIDELINE_MASK = SOCCER_FIELD.get_sideline_mask(10)
    
    OUTPUT_IMAGE_SHAPE = (352, 624)

    CAMERA_POSITION = np.array([372, 700, 400], np.float32)
    THETA_X = (90 + 50) * np.pi/180
    THETA_Z = 5 * np.pi/180
    FOCUS = 600
    ASPECT = 1.0
    CAMERA_CENTER = np.array(OUTPUT_IMAGE_SHAPE[::-1], np.float32) / 2.0

    camera_parameters = camera_operations.CameraParameters(THETA_X, THETA_Z, FOCUS, ASPECT,
                                                           CAMERA_CENTER, CAMERA_POSITION)
    camera = camera_operations.build_camera(camera_parameters)
    output_image = camera_operations.apply_camera(SIDELINE_MASK, camera, OUTPUT_IMAGE_SHAPE)

    cv2.imshow('output image', output_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    H = camera.get_homography_matrix()
    visible_poly_points = polygon_operations.compute_bounded_visibility_polygon_points(H,
                                                                                       OUTPUT_IMAGE_SHAPE,
                                                                                       SIDELINE_MASK.shape)
    
    sideline_mask = SOCCER_FIELD.get_sideline_mask(2)
    sideline_image = np.zeros(sideline_mask.shape + (3,), np.uint8)
    sideline_image[sideline_mask>0] = np.array([255, 255, 255])

    cv2.fillConvexPoly(sideline_image, visible_poly_points.reshape((-1,1,2)).astype(np.int32), (255,0,0))

    cv2.imshow('visible polygon', sideline_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    return

if __name__ == '__main__':
    test_visible_polygon_on_sidelines()
