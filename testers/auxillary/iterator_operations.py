from ...source.auxillary import iterator_operations
import itertools

def test_find_sequence():
    iterator = (i for i in xrange(10))
    pred = lambda x: 2 <= x <= 5

    sequence, iterator = iterator_operations.find_sequence(pred, iterator)

    print 'we found:'
    print sequence
    print 'the updated iterator has elements:'
    print list(iterator)

def test_extract_intervals():
    iterator = range(10)
    intervals = [(0,1),(2,4), (5,8)]

    print 'iterator values:'
    print iterator
    print 'intervals: '
    print intervals

    print 'extracting intervals...'
    
    for interval in iterator_operations.extract_intervals(iter(iterator), iter(intervals)):
        print 'interval: ', list(interval)

def test_extract_subsequence():
    iterator = (i for i in range(10))
    subsequence_indices = [2,5,7]

    sub_iter = iterator_operations.extract_subsequence(iterator, subsequence_indices)

    print 'subsequence: ', list(sub_iter)

def test_merge_indexed_and_not_indexed():
    indexed_iterator_0 = [((i,i+4), xrange(i,i+5)) for i in range(0,20,5)]
    indexed_iterator_1 = [((i,i+4), (2*k for k in xrange(i,i+5))) for i in range(0,20,5)]
    iterator_0 = [i for i in range(20)]
    iterator_1 = [3*i for i in range(20)]

    indexed_iterators = [iter(indexed_iterator_0), iter(indexed_iterator_1)]
    iterators = [iter(iterator_0), iter(iterator_1)]
    merged_iterator = iterator_operations.merge_indexed_and_non_indexed(indexed_iterators, iterators)

    print 'indexed iterators:'
    print indexed_iterators
    print 'iterators:'
    print iterators

    print 'merged iterator:'
    for interval, iterators_in_interval in merged_iterator:
        print 'interval: ', interval
        for iterator_in_interval in iterators_in_interval:
            print '   ', list(iterator_in_interval)

def test_extract_local_maxima():
    indices = range(10)
    values = range(5) + range(5,0,-1)
    items = zip(indices, values)

    key = lambda x: x
    bandwidth = 2
    
    print 'items: ', items
    
    local_maxima = iterator_operations.extract_local_maxima(iter(items), key, bandwidth)

    print 'local maxima:'
    print list(local_maxima)

def test_izip_dictionary():
    iterator_dict = {'a':iter([1,2,3,4]), 'b':iter([5,6,7,8])}

    dict_iterator = iterator_operations.izip_dictionary(iterator_dict)

    print 'dict iterator:'
    print '\n'.join(map(str, dict_iterator))
    
if __name__ == '__main__':
    #test_find_sequence()
    #test_extract_intervals()
    #test_extract_subsequence()
    #test_merge_indexed_and_not_indexed()
    #test_extract_local_maxima()
    test_izip_dictionary()
