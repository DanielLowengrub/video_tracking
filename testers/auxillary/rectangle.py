import numpy as np
from ...source.auxillary.rectangle import Rectangle


def test_get_slices():
    top_left = np.array([10,10])
    bottom_right = np.array([15,15])
    rectangle = Rectangle(top_left, bottom_right)
    
    arrays = np.stack([np.zeros((20,20)), np.ones((20,20))])

    print 'rectangle: ', rectangle
    print 'arrays: '
    print arrays

    slices = rectangle.get_slices(arrays)
    print 'slices:'
    print slices

def test_get_relative_slices():
    rectangle = Rectangle(np.array([0,0]), np.array([20,20]))
    arrays = np.stack([np.zeros((20,20)), np.ones((20,20))])
    
    top_left = np.array([10,10])
    bottom_right = np.array([15,15])
    child_rectangle = Rectangle(top_left, bottom_right)
    


    print 'rectangle: ', rectangle
    print 'child rectangle: ', child_rectangle
    print 'arrays: '
    print arrays

    slices = rectangle.get_relative_slices(arrays, child_rectangle)
    print 'slices:'
    print slices

def test_insert_arrays():
    arrays = np.stack([np.zeros((10,10)), np.zeros((10,10))])
    child_arrays = np.stack([np.ones((5,5)), np.ones((5,5))])
    
    top_left = np.array([2,2])
    bottom_right = np.array([7,7])
    rectangle = Rectangle(top_left, bottom_right)
    


    print 'rectangle: ', rectangle
    print 'arrays: '
    print arrays
    print 'child arrays:'
    print child_arrays

    rectangle.insert_arrays(arrays, child_arrays)
    
    print 'arrays:'
    print arrays

def test_insert_relative_arrays():
    arrays = np.stack([np.zeros((10,10)), np.zeros((10,10))])
    child_arrays = np.stack([np.ones((5,5)), np.ones((5,5))])

    rectangle = Rectangle(np.array([0,0]), np.array([10,10]))
    top_left = np.array([2,2])
    bottom_right = np.array([7,7])
    child_rectangle = Rectangle(top_left, bottom_right)
    


    print 'rectangle: ', rectangle
    print 'child rectangle: ', child_rectangle
    print 'arrays: '
    print arrays
    print 'child arrays:'
    print child_arrays

    rectangle.insert_relative_arrays(arrays, child_arrays, child_rectangle)
    
    print 'arrays:'
    print arrays

    
if __name__ == '__main__':
    #test_get_slices()
    #test_get_relative_slices()
    #test_insert_arrays()
    test_insert_relative_arrays()
