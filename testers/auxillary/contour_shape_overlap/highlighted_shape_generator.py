from ....source.auxillary.fit_points_to_shape.line import Line
from ....source.auxillary.fit_points_to_shape.ellipse import Ellipse
from ....source.auxillary.contour_shape_overlap.line_with_position import LineWithPosition
from ....source.auxillary.contour_shape_overlap.ellipse_with_position import EllipseWithPosition
from ....source.auxillary.contour_shape_overlap.highlighted_shape_generator import HighlightedShapeGenerator
from ....source.auxillary.mask_operations import mix_images_with_mask
from ....source.data_structures.contour import Contour
from ....source.image_processing.hough_transform.line_detector import LineDetector
from ..fit_points_to_shape.fit_points_to_ellipse import generate_points as generate_points_on_ellipse

import cv2
import numpy as np
import itertools

#########################
#
# This is a tester for code/source/auxillary/fit_points_to_shape/fit_points_to_line.py
#
#########################

def generate_points_on_line(line_with_position, start, end, num_points):
    '''
    We generate some points on a line and add noise
    '''

    positions = np.linspace(start, end, num_points)
    points = np.vstack([line_with_position.compute_absolute_position(p) for p in positions])

    #now add some gaussian noise
    #points += np.random.randn(*points.shape)
    
    return points

def draw_line(image, line, color=(0,255,0)):

    if np.abs(line._b) > 10**(-2):
        x = np.array([0, image.shape[1]])
        y = (-line._a / line._b)*x - (line._c / line._b)

    else:
        y = np.array([0, image.shape[0]])
        x = -(line._b / line._a)*y - (line._c / line._a)
        
    points = np.int32(np.vstack([x,y]).transpose().reshape((-1, 2)))

    cv2.line(image, tuple(points[0].tolist()), tuple(points[1].tolist()), color, thickness=2)
    return

def draw_interval_mask(image, highlighted_shape):
    interval_mask = highlighted_shape.generate_mask(image)
    blue_image = np.ones(image.shape, np.uint8) * 255
    blue_image[:,:,(1,2)] = 0
    colored_image = mix_images_with_mask(blue_image, image, interval_mask)
    alpha = 0.6

    image[:,:,:] = np.uint8(alpha*image + (1-alpha)*colored_image)
        
    return
    
def test_horizontal_line():
    print 'testing the highlighted_shape_generator with a horizontal line...'

    print 'building line y = -0.5x + 400'
    line = Line(-0.5, -1.0, 400.0)
    line_with_position = LineWithPosition(line)
    height = 5
    num_points = 5

    a,b,c = line.get_parametric_representation()
    vertical_offset = np.array([a,b])

    
    points_right_1 = generate_points_on_line(line_with_position, 100, 300, num_points) + height*vertical_offset
    points_left_1 = generate_points_on_line(line_with_position, 300, 100, num_points) + 4*height*vertical_offset

    points_right_2 = generate_points_on_line(line_with_position, 100, 300, num_points) - height*vertical_offset
    points_left_2 = generate_points_on_line(line_with_position, 300, 100, num_points) - 4*height*vertical_offset

    image = np.zeros((500, 500, 3), np.uint8)
    cv_contour_1 = np.vstack([points_right_1, points_left_1]).reshape((-1,1,2))
    cv_contour_2 = np.vstack([points_right_2, points_left_2]).reshape((-1,1,2))
    cv_contours = [cv_contour_1, cv_contour_2]
    contours = [Contour(cv_contour_1, image), Contour(cv_contour_2, image)]

    print 'cv_contours :'
    for cv_contour in cv_contours:
        print cv_contour
        
    print 'finding intervals in the following points:'
    radius = 4
    color = (0, 0, 255)
    thickness = -1

    for cv_contour in cv_contours:
        for point in cv_contour.reshape((-1,2)):
            cv2.circle(image, tuple(np.int32(point).tolist()), radius, color, thickness=thickness)

    draw_line(image, line)
    
    cv2.imshow('points and line', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    #now find the intrevals
    highlighted_shape_generator = HighlightedShapeGenerator()
    highlighted_line, contour_masks = highlighted_shape_generator.generate_highlighted_shape(line_with_position, 2*height, contours)
    
    #draw the the interval on the image in blue
    draw_interval_mask(image, highlighted_line)
    
    print 'the contour masks:'
    for cm in contour_masks:
        print cm
    
    cv2.imshow('the interval on the line', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def test_broken_horizontal_line():
    print 'testing the highlighted_shape_generator with a horizontal line...'

    print 'building line y = -0.5x + 400'
    line = Line(-0.5, -1.0, 400.0)
    line_with_position = LineWithPosition(line)
    height = 5
    num_points = 10

    a,b,c = line.get_parametric_representation()
    vertical_offset = np.array([a,b])
    
    points_right = generate_points_on_line(line_with_position, 100, 200, num_points) + height*vertical_offset
    points_left = generate_points_on_line(line_with_position, 200, 100, num_points) - height*vertical_offset

    mid = num_points / 2
    points_right[mid-1:mid+1] += 4*height*vertical_offset
    points_left[mid-1:mid+1]  -= 4*height*vertical_offset

    points_right[-2] += 4*height*vertical_offset
    points_left[2]  -= 4*height*vertical_offset

    image = np.zeros((500, 500, 3), np.uint8)
    cv_contour = np.vstack([points_right, points_left]).reshape((-1,1,2))
    contour = Contour(cv_contour, image)

    print 'cv_contour:'
    print cv_contour
        
    print 'finding intervals in the following points:'
    radius = 4
    color = (0, 0, 255)
    thickness = -1

    for point in cv_contour.reshape((-1,2)):
        cv2.circle(image, tuple(np.int32(point).tolist()), radius, color, thickness=thickness)

    draw_line(image, line)
    
    cv2.imshow('points and line', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    #now find the intervals
    highlighted_shape_generator = HighlightedShapeGenerator()
    highlighted_line, contour_masks = highlighted_shape_generator.generate_highlighted_shape(line_with_position, 2*height, [contour])

    #draw the the interval on the image in blue
    draw_interval_mask(image, highlighted_line)
    
    # #draw the the interval on the image in blue
    # interval_mask = highlighted_line.generate_mask(image)

    # cv2.imshow('the interval mask', interval_mask)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    
    # blue_image = np.ones(image.shape, np.uint8) * 255
    # blue_image[:,:,(1,2)] = 0
    # colored_image = mix_images_with_mask(blue_image, image, interval_mask)
    # alpha = 0.95
    # image = alpha*image + (1-alpha)*colored_image


    print 'the contour masks:'
    for cm in contour_masks:
        print cm
        
    cv2.imshow('the interval on the line', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def test_on_ellipse():
    print 'testing the highlighted_shape_generator with an ellipse...'

    image = np.zeros((500, 500, 3), np.uint8)
    center = np.array([200, 150])
    axes = np.array([174, 45])
    angle = 0
    ellipse_thickness = 2
    
    ellipse = Ellipse.from_geometric_parameters(center, axes, angle)
    ellipse_with_position = EllipseWithPosition(ellipse)

    start_angle_1 = -45 * (np.pi / 180)
    end_angle_1 = 45 * (np.pi / 180)
    num_points_1 = 20

    start_angle_2 = 90 * (np.pi / 180)
    end_angle_2 = 180 * (np.pi / 180)
    num_points_2 = 20

    noise_std = 0
    points_on_ellipse_1 = generate_points_on_ellipse(center, axes, angle, start_angle_1, end_angle_1, num_points_1, noise_std)
    points_on_ellipse_2 = generate_points_on_ellipse(center, axes, angle, start_angle_2, end_angle_2, num_points_2, noise_std)

    contours = [Contour(points_on_ellipse_1.reshape((-1,1,2)), image), Contour(points_on_ellipse_2.reshape((-1,1,2)), image)]
    #contours = [Contour(points_on_ellipse_1.reshape((-1,1,2)), image)]

    print 'finding intervals in the following points:'
    radius = 4
    color = (0, 0, 255)
    thickness = -1

    for contour in contours:
        for point in contour.get_cv_contour().reshape((-1,2)):
            cv2.circle(image, tuple(np.int32(point).tolist()), radius, color, thickness=thickness)

    cv2.ellipse(image, tuple(np.int32(center).tolist()), tuple(np.int32(axes).tolist()), angle, 0, 360, (0,255,0), 1, cv2.CV_AA)
    
    cv2.imshow('points and ellipse', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    #now find the intervals
    highlighted_shape_generator = HighlightedShapeGenerator()
    highlighted_ellipse, contour_masks = highlighted_shape_generator.generate_highlighted_shape(ellipse_with_position, 2*ellipse_thickness, contours)
    
    #draw the the interval on the image in blue
    draw_interval_mask(image, highlighted_ellipse)
    
    print 'the contour masks:'
    for cm in contour_masks:
        print cm
    
    cv2.imshow('the interval on the ellipse', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def test_on_soccer_image():
    from ....source.image_processing.contour_generator.edges_contour_generator import EdgesContourGenerator
    from ....source.image_processing.field_extractor.stupid_field_extractor import StupidFieldExtractor

    THICKNESS = 3
    MIN_TANGENT_ARC_LENGTH = 20
    MAX_TANGENT_ALGEBRAIC_RESIDUE = 1.5
    THETA_BIN_WIDTH = 2
    RADIUS_BIN_WIDTH = 5
    MAX_THETA_DEVIATION = 0.7
    MAX_RADIUS_DEVIATION = 5
    MIN_ARC_LENGTH = 300
    MAX_AVERAGE_ALGEBRAIC_RESIDUE = 10

    #interesting test images
    #215
    #141
    #2
    #96

    TEST_IMAGE_NAME = 'test_data/video3/out40.png'

    print 'loading image: ', TEST_IMAGE_NAME
    image = cv2.imread(TEST_IMAGE_NAME, 1)

    print 'getting the field mask...'
    field_extractor = StupidFieldExtractor()
    field_mask = field_extractor.get_field_mask(image)
    
    print 'generating contours...'
    contourGenerator = EdgesContourGenerator()
    contours = contourGenerator.generate_contours(image, mask=field_mask)
    contours += itertools.chain(*(contour.get_child_contours() for contour in contours))

    line_detector = LineDetector(MIN_TANGENT_ARC_LENGTH, MAX_TANGENT_ALGEBRAIC_RESIDUE, THETA_BIN_WIDTH, RADIUS_BIN_WIDTH, \
                                 MAX_THETA_DEVIATION, MAX_RADIUS_DEVIATION, MIN_ARC_LENGTH, MAX_AVERAGE_ALGEBRAIC_RESIDUE)

    for contour in contours:
        cv_contour = contour.get_cv_contour()
        cv2.drawContours(image, [cv_contour], -1, (255, 255, 255), 1)

    cv2.imshow('image with contour and points', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    print 'looking for lines in the contour...'
    lines = line_detector.get_lines(image, contours)
    lines_with_position = [LineWithPosition(line) for line in lines]
    
    highlighted_shape_generator = HighlightedShapeGenerator()
    highlighted_lines_and_contour_masks = [highlighted_shape_generator.generate_highlighted_shape(l, THICKNESS, contours) for l in lines_with_position]

    for line in lines:
        draw_line(image, line)

    for highlighted_line, contour_mask in highlighted_lines_and_contour_masks:
        draw_interval_mask(image, highlighted_line)


    cv2.imshow('the interval on the line', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

if __name__ == '__main__':
    #test_horizontal_line()
    #test_broken_horizontal_line()
    #test_on_soccer_image()
    test_on_ellipse()
