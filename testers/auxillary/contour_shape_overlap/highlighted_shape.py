import cv2
import numpy as np
from ....source.auxillary.contour_shape_overlap.highlighted_shape import HighlightedShape
from highlighted_shape_generator import draw_line, draw_interval_mask
from ....source.image_processing.contour_generator.edges_contour_generator import EdgesContourGenerator
from ....source.image_processing.field_extractor.stupid_field_extractor import StupidFieldExtractor
from ....source.image_processing.hough_transform.line_detector import LineDetector
from ....source.image_processing.hough_transform.single_ellipse_detector import SingleEllipseDetector
from ....source.image_processing.point_on_contour_classifier.ellipse_classifier import EllipseClassifier
from ....source.auxillary.contour_shape_overlap.line_with_position import LineWithPosition
from ....source.auxillary.contour_shape_overlap.ellipse_with_position import EllipseWithPosition
from ....source.auxillary.contour_shape_overlap.interval_on_ellipse import IntervalOnEllipse
from ....source.auxillary.contour_shape_overlap.highlighted_shape_generator import HighlightedShapeGenerator
from ....source.auxillary.fit_points_to_shape.fit_points_to_ellipse_stable import FitPointsToEllipseStable
from ....source.auxillary.fit_points_to_shape.ransac_fit_points import RansacFitPoints
from ....source.auxillary.fit_points_to_shape.line import Line
from ....source.auxillary.fit_points_to_shape.ellipse import Ellipse
from ...image_processing.hough_transform.ellipse_detector import display_ellipses
from ...auxillary import primitive_drawing, highlighted_shape_drawing
from ....source.auxillary import planar_geometry
import itertools


def test_load_line_from_directory():
    DIRECTORY_NAME = 'output/tester/highlighted_shapes/saved_data/frames_50_55/image_0/highlighted_line_1'
    IMAGE_NAME = 'test_data/video3/out50.png'

    image = cv2.imread(IMAGE_NAME, 1)

    print 'loading highlighted shape from: ', DIRECTORY_NAME
    highlighted_shape = HighlightedShape.load_from_directory(DIRECTORY_NAME, HighlightedShape.LINE)
    line = highlighted_shape.get_shape()

    draw_line(image, line)
    draw_interval_mask(image, highlighted_shape)

    cv2.imshow('image with highlighted shape', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def test_load_ellipse_from_directory():
    DIRECTORY_NAME = 'output/tester/highlighted_shapes/saved_data/frames_50_55/image_0/highlighted_ellipse_2'
    IMAGE_NAME = 'test_data/video3/out50.png'

    image = cv2.imread(IMAGE_NAME, 1)

    print 'loading highlighted shape from: ', DIRECTORY_NAME
    highlighted_shape = HighlightedShape.load_from_directory(DIRECTORY_NAME, HighlightedShape.ELLIPSE)
    ellipse = highlighted_shape.get_shape()

    center, axes, angle = ellipse.get_geometric_parameters(in_degrees=True)
    cv2.ellipse(image, tuple(np.int32(center).tolist()), tuple(np.int32(axes).tolist()), angle, 0, 360, (0,255,0), 1, cv2.CV_AA)
    draw_interval_mask(image, highlighted_shape)

    cv2.imshow('image with highlighted shape', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def test_add_mask_to_line():
    image = np.zeros((500,500,3), np.uint8)
    mask = np.zeros(image.shape[:2], np.float32)
    center = np.array([100,300])
    width = 50
    THICKNESS = 3

    mask[center[1]-width:center[1]+width, center[0]-width:center[0]+width] = 1.0
    
    line_with_position = LineWithPosition(Line.from_point_and_direction(np.array([100,100]), np.array([1,0])))
    interval_on_line = line_with_position.get_interval_on_shape(np.array([0, 0]), np.array([100, 0]))
    #highlighted_line = HighlightedShape(line_with_position, THICKNESS, [interval_on_line])
    highlighted_line = HighlightedShape(line_with_position, THICKNESS, [])
    print 'adding mask to the highlighted line: ', highlighted_line
    new_highlighted_line = highlighted_line.add_mask(mask)

    print 'the original highlighted line: ', highlighted_line
    print 'the new highlighted shape: ', new_highlighted_line

def test_add_mask_to_ellipse():
    image = np.zeros((500,500,3), np.uint8)
    mask = np.zeros(image.shape[:2], np.float32)
    center_1 = np.array([75,150])
    center_2 = np.array([250,150])
    width = 50
    THICKNESS = 3

    #mask[center_1[1]-width:center_1[1]+width, center_1[0]-width:center_1[0]+width] = 1.0
    mask[center_2[1]-width:center_2[1]+width, center_2[0]-width:center_2[0]+width] = 1.0
    image[mask > 0] = np.ones(3)*255

    cv2.imshow('image with mask', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    ellipse_with_position = EllipseWithPosition(Ellipse.from_geometric_parameters(np.array([150,150]), np.array([100,100]), 0))
    intervals_on_ellipse = []
    highlighted_ellipse = HighlightedShape(ellipse_with_position, THICKNESS, intervals_on_ellipse)

    primitive_drawing.draw_ellipse(image, highlighted_ellipse.get_shape(), (0,255,0))

    cv2.imshow('adding the mask to the following ellipse', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    highlighted_ellipse = highlighted_ellipse.add_mask(mask)

    draw_interval_mask(image, highlighted_ellipse)

    cv2.imshow('new highlighted shape', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def test_remove_from_contours():
    MIN_TANGENT_TO_LINE_ARC_LENGTH = 20
    MIN_TANGENT_TO_ELLIPSE_ARC_LENGTH = 50
    MAX_TANGENT_TO_LINE_GEOMETRIC_RESIDUE = 1.5
    MAX_TANGENT_TO_ELLIPSE_GEOMETRIC_RESIDUE = 10
    MIN_TANGENT_TO_ELLIPSE_AREA = 500

    THETA_BIN_WIDTH = 2
    RADIUS_BIN_WIDTH = 5
    MAX_THETA_DEVIATION = 0.7
    MAX_RADIUS_DEVIATION = 5
    MIN_ARC_LENGTH = 300
    MAX_AVERAGE_ALGEBRAIC_RESIDUE = 10

    RANSAC_MIN_POINTS_TO_FIT = 20
    RANSAC_NUM_ITERATIONS = 50
    RANSAC_DISTANCE_TO_SHAPE_THRESHOLD = 3
    RANSAC_MIN_PERCENTAGE_IN_SHAPE = 0.85

    MAX_AVERAGE_ELLIPSE_RESIDUE = 10
    THICKNESS = 3
    
    TEST_IMAGE_NAME = '/Users/daniel/Documents/soccer/images/images-8_15-9_31/image_1110.png'

    print 'loading image: ', TEST_IMAGE_NAME
    image = cv2.imread(TEST_IMAGE_NAME, 1)

    print 'getting the field mask...'
    field_extractor = StupidFieldExtractor()
    field_mask = field_extractor.get_field_mask(image)
    
    print 'generating contours...'
    contourGenerator = EdgesContourGenerator()
    base_contours = contourGenerator.generate_contours(image, mask=field_mask)
    base_contours_with_children = [([base_contour] + base_contour.get_child_contours()) for base_contour in base_contours]
    all_contours = list(itertools.chain(*(base_contour_with_children for base_contour_with_children in base_contours_with_children)))

    
    line_detector = LineDetector(MIN_TANGENT_TO_LINE_ARC_LENGTH, MAX_TANGENT_TO_LINE_GEOMETRIC_RESIDUE, THETA_BIN_WIDTH, RADIUS_BIN_WIDTH,
                                 MAX_THETA_DEVIATION, MAX_RADIUS_DEVIATION, MIN_ARC_LENGTH, MAX_AVERAGE_ALGEBRAIC_RESIDUE)

    single_ellipse_detector = SingleEllipseDetector(MIN_TANGENT_TO_LINE_ARC_LENGTH, MAX_TANGENT_TO_LINE_GEOMETRIC_RESIDUE,
                                                    MIN_TANGENT_TO_ELLIPSE_ARC_LENGTH,
                                                    MAX_TANGENT_TO_ELLIPSE_GEOMETRIC_RESIDUE, MIN_TANGENT_TO_ELLIPSE_AREA,
                                                    RANSAC_MIN_POINTS_TO_FIT, RANSAC_NUM_ITERATIONS,
                                                    RANSAC_DISTANCE_TO_SHAPE_THRESHOLD, RANSAC_MIN_PERCENTAGE_IN_SHAPE,
                                                    MAX_AVERAGE_ELLIPSE_RESIDUE)
                                                        
    image_with_contours = image.copy()
    for contour in all_contours:
        cv_contour = contour.get_cv_contour()
        cv2.drawContours(image_with_contours, [cv_contour], -1, (255, 255, 255), 1)

    # cv2.imshow('image with contours', image_with_contours)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    print 'looking for lines in the contour...'
    lines = line_detector.get_lines(image, all_contours)
    lines_with_position = [LineWithPosition(line) for line in lines]
    
    highlighted_shape_generator = HighlightedShapeGenerator()
    highlighted_lines_and_contour_masks = [highlighted_shape_generator.generate_highlighted_shape(l, THICKNESS, all_contours) for l in lines_with_position]

    for line in lines:
        draw_line(image_with_contours, line)

    for highlighted_line, contour_masks in highlighted_lines_and_contour_masks:
        draw_interval_mask(image_with_contours, highlighted_line)

    cv2.imshow('the intervals on the lines', image_with_contours)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    print 'removing the lines from the contours...'
    highlighted_lines = [highlighted_line for highlighted_line, contour_mask in highlighted_lines_and_contour_masks]
    print 'highlighted lines: ', highlighted_lines
    new_base_contour_components = HighlightedShape.remove_shapes_from_contours(highlighted_lines, base_contours)

    highlighted_ellipses = []
    
    for base_contour_component in new_base_contour_components:
        image_with_new_contours = image.copy()
        all_contours_in_component = list(itertools.chain(*([base_contour] + base_contour.get_child_contours() for base_contour in base_contour_component)))

        all_cv_contours_in_component = [contour.get_cv_contour() for contour in all_contours_in_component]
        
        #for each of the contours, get a mask that records which points are on an ellipse
        #all_point_on_ellipse_masks_in_component = [point_on_ellipse_classifier.classify_points(contour) for contour in all_contours_in_component]
        
        #points_on_ellipse_in_component = np.vstack([contour.get_cv_contour().reshape((-1,2))[point_on_ellipse_mask] for contour, point_on_ellipse_mask in
        #                                            zip(all_contours_in_component, all_point_on_ellipse_masks_in_component)])
        
        cv2.drawContours(image_with_new_contours, all_cv_contours_in_component, -1, (255, 255, 255), 3)
        #draw the point that we classified as on an ellipse in blue
        #image_with_new_contours[points_on_ellipse_in_component[:,1], points_on_ellipse_in_component[:,0]] = np.array([255,0,0])
            
        # cv2.imshow('finding ellipse in these contours', image_with_new_contours)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()
        
        #fitted_ellipse, total_geometric_residue, inlier_mask = ransac_ellipse_fitter.fit_points(points_on_ellipse_in_component)
        fitted_ellipse = single_ellipse_detector.find_ellipse(all_contours_in_component)
        
        if fitted_ellipse is not None:
            #inliers = points_on_ellipse_in_component[inlier_mask]
            #image_with_new_contours[inliers[:,1], inliers[:,0]] = np.array([255,0,0])
            fitted_ellipse.compute_geometric_parameters()
            ellipse_with_position = EllipseWithPosition(fitted_ellipse)
            highlighted_ellipse, contour_masks = highlighted_shape_generator.generate_highlighted_shape(ellipse_with_position,
                                                                                                        THICKNESS,
                                                                                                        all_contours_in_component)
            print 'interval lengths: ', [ios.length() for ios in highlighted_ellipse._intervals_on_shape]
            highlighted_ellipses.append(highlighted_ellipse)
            
            center, axes, angle = fitted_ellipse.get_geometric_parameters(in_degrees=True)
            print 'axes: ', axes
            display_ellipses(image_with_contours, [fitted_ellipse])

    for highlighted_ellipse in highlighted_ellipses:
        draw_interval_mask(image_with_contours, highlighted_ellipse)

    cv2.imshow('highlighted shapes', image_with_contours)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def test_apply_homography():
    image = np.zeros((500,500,3), np.uint8)
    THICKNESS = 3

    line_endpoints = np.vstack([np.array([150, 100]), np.array([250, 100]),
                                np.array([350, 100]), np.array([400, 100])]).astype(np.float32)
    line_with_position = LineWithPosition(Line.from_point_and_direction(np.array([100,100]), np.array([1,0])))
    intervals_on_line = [line_with_position.get_interval_on_shape(*line_endpoints[:2]),
                         line_with_position.get_interval_on_shape(*line_endpoints[2:])]
    highlighted_line = HighlightedShape(line_with_position, THICKNESS, intervals_on_line)

    center = np.array([300,300], np.float64)
    axes = np.array([100,70], np.float64)
    ellipse_endpoints = center.reshape((1,2)) + np.vstack([np.array([100,0]), np.array([0,-70])])
    ellipse_with_position = EllipseWithPosition(Ellipse.from_geometric_parameters(center, axes, 0))
    #intervals_on_ellipse = [ellipse_with_position.get_interval_on_shape(*ellipse_endpoints)]
    intervals_on_ellipse = [IntervalOnEllipse(ellipse_with_position, 0, 270)]
    highlighted_ellipse = HighlightedShape(ellipse_with_position, THICKNESS, intervals_on_ellipse)
    
    #H = planar_geometry.translation_matrix(np.array([0,50]))
    H = np.eye(3)
    H[:2,:2] = planar_geometry.rotation_matrix(np.pi/10)

    endpoints = np.vstack([line_endpoints, ellipse_endpoints])
    target_endpoints = planar_geometry.apply_homography_to_points(H, endpoints)

    image_region = planar_geometry.get_image_region(image)
    print 'applying homography to line...'
    target_highlighted_line = highlighted_line.apply_homography(H, image_region, 1)
    print 'applying homography to ellipse...'
    target_highlighted_ellipse = highlighted_ellipse.apply_homography(H, image_region, 1)
    
    image_copy = image.copy()
    highlighted_shape_drawing.draw_highlighted_shape(image_copy, highlighted_line)
    highlighted_shape_drawing.draw_highlighted_shape(image_copy, highlighted_ellipse)
    highlighted_shape_drawing.draw_highlighted_shape(image_copy, target_highlighted_line, shape_color=(0,0,255))
    highlighted_shape_drawing.draw_highlighted_shape(image_copy, target_highlighted_ellipse,
                                                     shape_color=(0,0,255))
    
    primitive_drawing.draw_points(image_copy, endpoints, (255,255,255))
    primitive_drawing.draw_points(image_copy, target_endpoints, (255,255,255))
        
    cv2.imshow('highlighted shapes', image_copy)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    return

def test_snap_to_shape():
    image = np.zeros((500,500,3), np.uint8)
    THICKNESS = 3

    line_endpoints = np.vstack([np.array([150, 100]), np.array([250, 100]),
                                np.array([350, 100]), np.array([400, 100])]).astype(np.float32)
    line_with_position =     LineWithPosition(Line.from_point_and_direction(np.array([100,100]),
                                                                            np.array([1,0])))
    new_line_with_position = LineWithPosition(Line.from_point_and_direction(np.array([100,120]),
                                                                            np.array([1,0])))
    intervals_on_line = [line_with_position.get_interval_on_shape(*line_endpoints[:2]),
                         line_with_position.get_interval_on_shape(*line_endpoints[2:])]
    highlighted_line = HighlightedShape(line_with_position, THICKNESS, intervals_on_line)
    new_highlighted_line = highlighted_line.snap_to_shape(new_line_with_position)

    center = np.array([300,300], np.float64)
    axes = np.array([100,70], np.float64)
    ellipse_with_position = EllipseWithPosition(Ellipse.from_geometric_parameters(center, axes, 0))
    new_ellipse_with_position = EllipseWithPosition(Ellipse.from_geometric_parameters(center + np.array([10,0]),
                                                                                      axes, 0))
    intervals_on_ellipse = [IntervalOnEllipse(ellipse_with_position, 0, 270)]
    highlighted_ellipse = HighlightedShape(ellipse_with_position, THICKNESS, intervals_on_ellipse)
    new_highlighted_ellipse = highlighted_ellipse.snap_to_shape(new_ellipse_with_position)
    
    image_copy = image.copy()
    highlighted_shape_drawing.draw_highlighted_shape(image_copy, highlighted_line)
    highlighted_shape_drawing.draw_highlighted_shape(image_copy, highlighted_ellipse)
    highlighted_shape_drawing.draw_highlighted_shape(image_copy, new_highlighted_line, shape_color=(0,0,255))
    highlighted_shape_drawing.draw_highlighted_shape(image_copy, new_highlighted_ellipse, shape_color=(0,0,255))
    
    cv2.imshow('highlighted shapes', image_copy)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    return

def test_intersect_intervals_lines():
    image = np.zeros((500,500,3), np.uint8)
    image[:,100,:] = 255
    image[:,200,:] = 255
    image[:,300,:] = 255
    
    THICKNESS = 3

    line_endpoints_1 = np.vstack([np.array([150, 100]), np.array([250, 100]),
                                  np.array([350, 100]), np.array([400, 100])]).astype(np.float32)
    
    line_endpoints_2 = np.vstack([np.array([200, 100]), np.array([300, 100]),
                                  np.array([375, 100]), np.array([450, 100])]).astype(np.float32)
    
    line_with_position =     LineWithPosition(Line.from_point_and_direction(np.array([100,100]),
                                                                            np.array([1,0])))

    intervals_on_line_1 = [line_with_position.get_interval_on_shape(*line_endpoints_1[:2]),
                           line_with_position.get_interval_on_shape(*line_endpoints_1[2:])]
    intervals_on_line_2 = [line_with_position.get_interval_on_shape(*line_endpoints_2[:2]),
                           line_with_position.get_interval_on_shape(*line_endpoints_2[2:])]

    highlighted_line_1 = HighlightedShape(line_with_position, THICKNESS, intervals_on_line_1)
    highlighted_line_2 = HighlightedShape(line_with_position, THICKNESS, intervals_on_line_2)
    highlighted_line = highlighted_line_1.intersect_intervals(highlighted_line_2)
    
    image_1 = image.copy()
    highlighted_shape_drawing.draw_highlighted_shape(image_1, highlighted_line_1)
    image_2 = image.copy()
    highlighted_shape_drawing.draw_highlighted_shape(image_2, highlighted_line_2)
    highlighted_shape_drawing.draw_highlighted_shape(image, highlighted_line)

    vd = np.ones((image.shape[0],10,3), np.uint8) * 255
    output_image = np.concatenate([image_1, vd, image_2, vd, image], axis=1)
    cv2.imshow('interval overlaps', output_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def test_intersect_intervals_ellipse():
    image = np.zeros((500,500,3), np.uint8)
    image[:,100,:] = 255
    image[:,200,:] = 255
    image[:,300,:] = 255
    image[:,400,:] = 255

    center = np.array([300,300], np.float64)
    axes = np.array([100,70], np.float64)
    ellipse_with_position = EllipseWithPosition(Ellipse.from_geometric_parameters(center, axes, 0))
    intervals_on_ellipse_1 = [IntervalOnEllipse(ellipse_with_position, 0, 270)]
    intervals_on_ellipse_2 = [IntervalOnEllipse(ellipse_with_position, 180, 310)]
    highlighted_ellipse_1 = HighlightedShape(ellipse_with_position, 3, intervals_on_ellipse_1)
    highlighted_ellipse_2 = HighlightedShape(ellipse_with_position, 3, intervals_on_ellipse_2)
    highlighted_ellipse = highlighted_ellipse_1.intersect_intervals(highlighted_ellipse_2)

    image_1 = image.copy()
    highlighted_shape_drawing.draw_highlighted_shape(image_1, highlighted_ellipse_1)
    image_2 = image.copy()
    highlighted_shape_drawing.draw_highlighted_shape(image_2, highlighted_ellipse_2)
    highlighted_shape_drawing.draw_highlighted_shape(image, highlighted_ellipse)

    vd = np.ones((image.shape[0],10,3), np.uint8) * 255
    output_image = np.concatenate([image_1, vd, image_2, vd, image], axis=1)
    cv2.imshow('interval overlaps', output_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def test_interval_closing_line():
    image = np.zeros((500,500,3), np.uint8)
    THICKNESS = 3

    line_endpoints = np.vstack([np.array([150, 100]), np.array([250, 100]),
                                  np.array([260, 100]), np.array([400, 100])]).astype(np.float32)
    
    line_with_position = LineWithPosition(Line.from_point_and_direction(np.array([100,100]),
                                                                        np.array([1,0])))
    intervals_on_line = [line_with_position.get_interval_on_shape(*line_endpoints[:2]),
                         line_with_position.get_interval_on_shape(*line_endpoints[2:])]

    highlighted_line = HighlightedShape(line_with_position, THICKNESS, intervals_on_line)
    image_copy = image.copy()
    highlighted_shape_drawing.draw_highlighted_shape(image_copy, highlighted_line)

    cv2.imshow('original line', image_copy)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    closed_hl = highlighted_line.interval_closing(5)
    highlighted_shape_drawing.draw_highlighted_shape(image, closed_hl)
    cv2.imshow('closed line', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    return

def test_interval_closing_ellipse():
    image = np.zeros((500,500,3), np.uint8)

    center = np.array([300,300], np.float64)
    axes = np.array([100,70], np.float64)
    ellipse_with_position = EllipseWithPosition(Ellipse.from_geometric_parameters(center, axes, 0))
    intervals_on_ellipse = [IntervalOnEllipse(ellipse_with_position, 300, 50),
                            IntervalOnEllipse(ellipse_with_position, 60, 200)]
    highlighted_ellipse = HighlightedShape(ellipse_with_position, 3, intervals_on_ellipse)
    
    image_copy = image.copy()
    highlighted_shape_drawing.draw_highlighted_shape(image_copy, highlighted_ellipse)
    cv2.imshow('original ellipse', image_copy)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    closed_he = highlighted_ellipse.interval_closing(5)
    highlighted_shape_drawing.draw_highlighted_shape(image, closed_he)
    cv2.imshow('closed ellipse', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    
if __name__ == '__main__':
    #test_load_line_from_directory()
    #test_load_ellipse_from_directory()
    #test_remove_from_contours()
    #test_add_mask_to_ellipse()
    test_apply_homography()
    #test_snap_to_shape()
    #test_intersect_intervals_lines()
    #test_intersect_intervals_ellipse()
    #test_interval_closing_line()
    #test_interval_closing_ellipse()
