import numpy as np
import itertools
from ....source.auxillary.contour_shape_overlap.interval_on_loop import IntervalOnLoop

def basic_test():
    N = 10
    intervals = [IntervalOnLoop(0,3,N), IntervalOnLoop(0,9,N), IntervalOnLoop(3,8,N), IntervalOnLoop(4,6,N),
                 IntervalOnLoop(7,5,N), IntervalOnLoop(8,2,N), IntervalOnLoop(5,0,N)]

    for I1,I2 in itertools.combinations(intervals, 2):
        print 'the intersection of %s and %s is: %s' % (str(I1), str(I2), ', '.join(str(I) for I in I1.get_intersection(I2)))

def test_slice():
    N = 10
    intervals = [IntervalOnLoop(0,5,N), IntervalOnLoop(8,3,N)]
    array = np.arange(N)

    print 'computing slices of: ', array
    
    for interval in intervals:
        print 'slice with interval=%s is ' % interval, interval.get_slice(array)

def test_merge():
    N=10
    intervals = [IntervalOnLoop(0,3,N), IntervalOnLoop(0,9,N), IntervalOnLoop(3,8,N), IntervalOnLoop(4,6,N),
                 IntervalOnLoop(7,5,N), IntervalOnLoop(8,2,N), IntervalOnLoop(5,0,N)]

    for I1,I2 in itertools.combinations(intervals, 2):
        #print 'I1: %s, I2: %s' % (str(I1), str(I2))
        if not I1.can_merge(I2):
            print 'we can not merge the intervals %s and %s.' % (str(I1), str(I2))
        else:
            print 'the union of %s and %s is: %s' % (str(I1), str(I2), str(I1.merge(I2)))

def test_dilation():
    N=10
    dilation_radius = 2
    intervals = [IntervalOnLoop(0,3,N), IntervalOnLoop(0,9,N), IntervalOnLoop(3,8,N), IntervalOnLoop(4,6,N),
                 IntervalOnLoop(7,5,N), IntervalOnLoop(8,2,N), IntervalOnLoop(5,0,N)]

    for interval in intervals:
        print 'dilation of %s: %s' % (interval, interval.interval_dilation(dilation_radius))

    return

if __name__ == '__main__':
    #basic_test()
    #test_slice()
    #test_merge()
    test_dilation()
