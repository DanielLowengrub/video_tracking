import numpy as np
import os
import cv2
from scipy import ndimage
from ...source.auxillary import skeletonization, mask_operations
from ...source.image_processing.contour_generator.edges_contour_generator import EdgesContourGenerator
from ...source.image_processing.image_annotator.soccer_annotation import SoccerAnnotation
import matplotlib.pyplot as plt

def test():
    mask = np.zeros((300,500), np.bool)
    mask[200:220, 100:400] = True
    
    cv2.imshow('mask', np.float32(mask))
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    skeleton = skeletonization.compute_skeleton(mask, 3, thickness_map=False)

    mask_color = mask_operations.convert_to_image(mask)
    mask_color[skeleton] = np.array([255,0,0])
    
    cv2.imshow('skeleton (in blue)', mask_color)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def test_thickness_map():
    mask = np.zeros((300,500), np.bool)
    mask[200:220, 100:200] = True
    mask[205:215, 200:400] = True

    cv2.imshow('mask', np.float32(mask))
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    skeleton = skeletonization.compute_skeleton(mask, 3, thickness_map=True)
    skeleton[mask] += 1
    
    p = plt.imshow(skeleton, interpolation="nearest")
    plt.colorbar(p)
    plt.show()

def load_image_and_mask():
    # input_name = 'images-6_35-6_38'
    # interval = (0,74)
    # frame = 0
    # input_name = 'images-6_51-6_55'
    # interval = (0,99)
    # frame = 50
    # input_name = 'images-7_45-7_50'
    # interval = (0, 124)
    # frame = 100
    # input_name = 'images-9_17-9_22'
    # interval = (0,124)
    # frame = 80
    # input_name = 'images-11_54-11_59'
    # interval = (0,124)
    # frame = 15
    # input_name = 'images-12_28-12_38'
    # interval = (0,249)
    # frame = 0
    # input_name = 'images-12_45-12_50'
    # interval = (0,124)
    # frame = 0
    
    # IMAGE_FILENAME = os.path.join('/Users/daniel/Documents/soccer/output/preprocessing_pass_0/data',
    #                               'video_2', input_name, 'stage_1', 'image',
    #                               'interval_%d_%d' % interval, 'frame_%d' % frame, 'image.png')
    # ANNOTATION_FILENAME = os.path.join('/Users/daniel/Documents/soccer/output/preprocessing_pass_0/data',
    #                                    'video_2', input_name, 'stage_1', 'field_mask',
    #                                    'interval_%d_%d' % interval, 'frame_%d' % frame, 'annotation.npy')

    interval = (0, 1874)
    frame = 1150
    IMAGE_FILENAME = os.path.join('/Users/daniel/Documents/soccer/output/preprocessing_pass_0/data',
                                  'images-8_15-9_31', 'stage_1', 'image',
                                  'interval_%d_%d' % interval, 'frame_%d' % frame, 'image.png')
    
    ANNOTATION_FILENAME = os.path.join('/Users/daniel/Documents/soccer/output/preprocessing_pass_0/data',
                                       'images-8_15-9_31', 'stage_1', 'field_mask',
                                       'interval_%d_%d' % interval, 'frame_%d' % frame, 'annotation.npy')

    CANNY_MIN = 65
    CANNY_MAX = 100

    image = cv2.imread(IMAGE_FILENAME, 1)
    annotation = SoccerAnnotation(np.load(ANNOTATION_FILENAME))

    contour_generator = EdgesContourGenerator(CANNY_MIN, CANNY_MAX)
    contours, closed_edges = contour_generator.generate_contours(image, annotation.get_soccer_field_mask())

    return image, closed_edges

def test_thickness_map_soccer():
    image, mask = load_image_and_mask()

    print 'mask type: ', mask.dtype
    cv2.imshow('image', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    cv2.imshow('mask', np.float32(mask))
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    skeleton = skeletonization.compute_skeleton(mask, 3, thickness_map=True)
    dilated_skel = ndimage.morphology.binary_dilation(skeleton > 0, iterations=3)
    
    fig = plt.figure()
    ax1 = fig.add_subplot(211)
    ax1.imshow(skeleton, interpolation="nearest")
    ax1.set_ylim(skeleton.shape[0], 0)
    ax2 = fig.add_subplot(212)
    ax2.imshow(dilated_skel, interpolation="nearest")
    ax2.set_ylim(skeleton.shape[0], 0)
    plt.show()
    
    good_width = np.logical_and(skeleton >= 1, skeleton <=3)
    bad_width = (skeleton >= 4)

    cv2.imshow('good', np.float32(good_width))
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    cv2.imshow('bad', np.float32(bad_width))
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    width_image = np.zeros(image.shape[:2], np.int32)
    width_image[good_width] = 1
    width_image[bad_width] = 2
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.imshow(width_image, interpolation="nearest")
    ax.set_ylim(width_image.shape[0], 0)
    plt.show()

if __name__ == '__main__':
    test_thickness_map_soccer()
