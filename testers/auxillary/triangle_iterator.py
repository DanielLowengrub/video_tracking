from ...source.auxillary.triangle_iterator import Vertex, Edge, Graph

def basic_test():
    vertices = [Vertex() for i in range(4)]
    edge_vertex_ids = [(0,1), (1,2), (2,3), (2,0)]
    edges = [Edge(vertices[id_a], vertices[id_b]) for id_a,id_b in edge_vertex_ids]

    graph = Graph(edges)

    for triangle in graph.get_triangle_iterator():
        print 'triangle with edges: %s, %s, %s' % triangle

if __name__ == '__main__':
    basic_test()
