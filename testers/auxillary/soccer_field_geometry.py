import numpy as np
import cv2
from .primitive_drawing import draw_line, draw_ellipse, draw_point
from .contour_shape_overlap.highlighted_shape_generator import draw_interval_mask 
from ...source.auxillary.soccer_field_geometry import SoccerFieldGeometry

def draw_soccer_field_geometry(image, soccer_field):
    '''
    draw all of the shapes in the soccer field in the image.
    '''
    shape_color = (0,255,0)
    for highlighted_shape in soccer_field.get_highlighted_shape_dict().itervalues():
        shape = highlighted_shape.get_shape()
        if shape.is_line():
            draw_line(image, shape, shape_color)
        elif shape.is_ellipse():
            draw_ellipse(image, shape, shape_color)

        draw_interval_mask(image, highlighted_shape)
        

    return

def display_sideline_mask(soccer_field):
    thickness=3
    sideline_mask = soccer_field.get_sideline_mask(thickness)

    cv2.imshow('sideline mask', sideline_mask)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    return

def display_grid_off_sidelines(absolute_image, soccer_field):
    grid_points = soccer_field.get_grid_points()
    thickness=5
    sideline_mask = soccer_field.get_sideline_mask(thickness)

    grid_point_indices = np.int32(grid_points)
    on_sidelines = sideline_mask[grid_point_indices[:,1],grid_point_indices[:,0]]

    grid_points = grid_points[on_sidelines == 0]

    for p in grid_points:
        draw_point(absolute_image, p, (255,0,0))

    cv2.imshow('sidelines mask', sideline_mask)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    cv2.imshow('grid points not on sidelines', absolute_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def build_soccer_field_geometry(absolute_image):
    SCALE = 6
    FIELD_WIDTH = absolute_image.shape[1]-1
    FIELD_LENGTH = absolute_image.shape[0]-1
    GRID_WIDTH = 20
    GRID_LENGTH = 14
    soccer_field = SoccerFieldGeometry(SCALE, FIELD_WIDTH, FIELD_LENGTH, GRID_WIDTH, GRID_LENGTH)

    return soccer_field

if __name__ == '__main__':
    ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)

    #We draw all of the shapes on an image to check that everything is in the right position
    soccer_field = build_soccer_field_geometry(absolute_image)
    #display_sideline_mask(soccer_field)
    display_grid_off_sidelines(absolute_image, soccer_field)
    
    # draw_soccer_field_geometry(absolute_image, soccer_field)

    # cv2.imshow('soccer field geometry', absolute_image)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()


                                       
