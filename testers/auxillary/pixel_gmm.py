import numpy as np
import cv2
from ...source.auxillary.pixel_gmm import PixelGMM

#######
# This is a tester for the file code/source/auxillary/pixel_gmm.py
#
# We generate an image with two squares where each square has a different distribution of hue and saturation.
# We then apply the PixelGMM to find the two regions.
######

LABEL_COLORS = [np.array([255, 0, 0]), np.array([0, 255, 0]), np.array([0, 0, 255])]

def draw_gaussain(img, mean, cov, color):
    x, y = np.int32(mean)
    w, u, vt = cv2.SVDecomp(cov)
    ang = np.arctan2(u[1, 0], u[0, 0])*(180/np.pi)
    s1, s2 = np.sqrt(w)*3.0
    print 'axis lengths: ', (s1, s2)
    print 'position: (%f, %f)' % (x, y)
    print 'color: ', color
    cv2.ellipse(img, (x, y), (s1, s2), ang, 0, 360, color, 1, cv2.CV_AA)

def draw_em_and_points(img, em, points, title):

    means = em.getMat('means')
    covs = em.getMatVector('covs')
    distrs = zip(means, covs)

    output_img = np.zeros(img.shape, np.uint8)
    for x, y in np.int32(points):
        cv2.circle(output_img, (x, y), 1, (255, 255, 255), -1)

    for i, (m, cov) in enumerate(distrs):
        draw_gaussain(output_img, m, cov, tuple(LABEL_COLORS[i]))

    cv2.imshow(title, output_img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def get_image(means, stddevs, square_size):
    '''
    Return an image with one square per (mean, stddev).
    We color each square using the mean and stddev
    '''

    squares = []

    for i, mean in enumerate(means):
        stddev = stddevs[i]

        square = np.zeros((square_size, square_size, 2), np.uint8)
        cv2.randn(square, mean, stddev)

        #now add a value of 100 to each of the (hue, saturation) pairs
        square_list = square.reshape((-1, 2))
        square_list = np.hstack([square_list, 100 * np.ones((square_list.shape[0], 1), np.uint8)])
        square = square_list.reshape((square_size, square_size, 3))

        #convert from HSV to BGR
        square = cv2.cvtColor(square, cv2.COLOR_HSV2BGR)
        squares.append(square)

    #stack the squares horizontally and return the result
    return np.hstack(squares)

def test():

    print 'testing pixel gmm...'

    means = [np.array([10, 60]), np.array([70, 120])]
    stddev1 = np.array([[10, 2],
                        [3, 10]])
    stddev2 = np.array([[2, 2],
                        [3, 2]])
    stddevs = [stddev1, stddev2]
    
    print 'using the following pairs of mean and stddev'
    
    for i, mean in enumerate(means):
        print '%d:' % i
        print 'mean: '
        print mean
        print 'stddev:'
        print stddevs[i]

    image = get_image(means, stddevs, 50)

    print 'displaying input image'
    cv2.imshow('input image', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    print 'running gmm on input image'

    pixelGMM = PixelGMM(2)
    pixelGMM.train(image)

    print 'finished.'

    print 'displaying label map'
    labelled_image = np.zeros(image.shape, np.uint8)
    label_map = pixelGMM.get_label_map()
    
    labelled_image[label_map == 0] = LABEL_COLORS[0]
    labelled_image[label_map == 1] = LABEL_COLORS[1]

    cv2.imshow('labelled image', labelled_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    means = pixelGMM._em.getMat('means')
    covs = pixelGMM._em.getMatVector('covs')

    print 'the means are:'
    print means
    print 'the covariances are:'
    print covs

    print 'displaying the GMM'
    points = pixelGMM._get_samples()
    img = np.zeros((180, 255, 3), np.uint8)
    draw_em_and_points(img, pixelGMM._em, points, 'output GMM')

    print 'getting the mask of all pixels whose label has cov of less than 10'
    dense_mask = pixelGMM.get_mask_of_pixels_in_dense_cluster(10)

    cv2.imshow('homogeneous part', dense_mask)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

if __name__ == '__main__':

    test()
