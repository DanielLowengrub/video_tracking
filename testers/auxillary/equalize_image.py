import numpy as np
import cv2
from ...source.auxillary.equalize_image import equalize_image
from ...source.image_processing.image_annotator.gmm_soccer_field_annotator import GMMSoccerFieldAnnotator
#######
# This is a tester for the file code/auxillary/equalize_image
#
# We load an image of a soccer field and equalize the part containing grass.
######

#interesting test images
#215
#141
#2
#96

TEST_IMAGE_NAME = 'test_data/video3/out2.png'

if __name__=='__main__':
    
    print 'starting image equalization test'
    print 'loading image: ', TEST_IMAGE_NAME
    image = cv2.imread(TEST_IMAGE_NAME, 1)

    print 'generating annotations'
    image_annotator = GMMSoccerFieldAnnotator()
    annotation = image_annotator.annotate_image(image)
    grass_mask = GMMSoccerFieldAnnotator.get_grass_mask(annotation)
    
    image_gs = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    equalized_image = image_gs.copy()
    equalize_image(equalized_image, grass_mask)

    print 'the shape of the equalized image is: '
    print equalized_image.shape

    print 'and the type is: ', equalized_image.dtype
    cv2.imshow('the gray scale image', image_gs)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    cv2.imshow('the equalized image', equalized_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()



