import numpy as np
from ...source.auxillary.matrix_interpolation import interpolate_between_invertible_matrices


if __name__ == '__main__':

    A = np.array([[1, 0, 1],
                  [0, 1, 0],
                  [0, 0, 1]])

    B = np.array([[1, 1, 0],
                  [0, 1.01, 0],
                  [0, 0, 1]])

    precision = 4

    alpha = 0.5

    print 'A:'
    print A

    print'B:'
    print B

    print 'interpolating between A and B with precision %f and alpha equal to %f...' % (precision, alpha)

    C = interpolate_between_invertible_matrices(A, B, alpha, precision)
    print 'the result is:'
    print C
