from ....source.auxillary.fit_points_to_shape.ellipse import Ellipse
import cv2
import numpy as np
from ....source.auxillary import planar_geometry

#########################
#
# This is a tester for code/source/auxillary/fit_points_to_shape/ellipse.py
#
# We mainly test the functions that turn an algebraic representation of an ellipse into a geometric one.
#########################

def get_ellipse():

    #we construct an ellipse corresponding to the equation:
    # (x-3)^2 + 4*(y-2)^2 = 5^2
    # x^2 - 6x + 9 + 4y^2 - 16y + 16 - 25 = 0

    return Ellipse(1, 0, 1, 0, 0, -25)
    
def test():

    ellipse = get_ellipse()

    #we now compute the geometric representation and draw it
    print 'computing geometric parameters'
    ellipse.compute_geometric_parameters()

    image = np.zeros((100, 100, 3))
    color = (255, 0, 0)
    center, axes, angle = ellipse.get_geometric_parameters(in_degrees=True)

    print 'drawing the ellipse'
    print 'center: ', tuple(np.int32(center).tolist())
    print 'axes: ', tuple(axes.tolist())
    print 'angle: ', angle
    
    cv2.ellipse(image, tuple(np.int32(center + np.array([50,50])).tolist()), tuple(np.int32(axes).tolist()), angle, 0, 360, color, 1, cv2.CV_AA)
    cv2.imshow('the ellipse', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def test_from_geometric_parameters():
    center = np.array([10, 10])
    axes = np.array([40, 30]) #np.array([30, 10])
    rotation = np.pi / 3.0

    print 'we constructed an ellipse with geometric parameters:'
    print 'center: ', center
    print 'axes: ', axes
    print 'rotation: ', rotation

    ellipse = Ellipse.from_geometric_parameters(center, axes, rotation)

    print 'and got the following ellipse: '
    print ellipse.get_conic_string()
    print 'to double check, we now recalculate the geometric parameters:'

    ellipse.compute_geometric_parameters()
    center, axes, angle = ellipse.get_geometric_parameters()

    print 'center: ', center
    print 'axes: ', axes
    print 'angle (radians): ', angle
    print 'angle + pi: ', angle + np.pi

def test_bounding_box():
    image = np.zeros((100,100,3))
    ellipse_color = (255,0,0)
    box_color = np.array([255,255,255])

    center = np.array([50, 50])
    axes = np.array([40, 10]) #np.array([30, 10])
    angle_radians = np.pi / 2.0
    angle_degrees = (180 * angle_radians) / np.pi
    
    ellipse = Ellipse.from_geometric_parameters(center, axes, angle_radians)
    width, height = ellipse.get_aa_bounding_box()

    print 'width: ', width
    print 'height: ', height
    
    image[center[1]-height:center[1]+height+1 , center[0]-width : center[0]+width+1] = box_color
    cv2.ellipse(image, tuple(np.int32(center).tolist()), tuple(np.int32(axes).tolist()), angle_degrees, 0, 360, ellipse_color, 1, cv2.CV_AA)
    cv2.imshow('the ellipse', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def test_scaling():
    center = np.array([0,0], np.float32)
    axes = np.array([60,60], np.float32)
    angle = 0.0
    ellipse = Ellipse.from_geometric_parameters(center, axes, angle)

    scale = np.array([0.00269179,  0.00417537], np.float32)
    S = planar_geometry.scaling_matrix(scale)

    print 'original ellipse: ', str(ellipse)
    print 'scaled ellipse: ', str(planar_geometry.apply_homography_to_ellipse(S, ellipse))
                                  
if __name__ == '__main__':

    #test()
    #test_from_geometric_parameters()
    #test_bounding_box()
    test_scaling()
