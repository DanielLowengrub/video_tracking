from ....source.auxillary.fit_points_to_shape.fit_points_to_ellipse import FitPointsToEllipse
from ....source.auxillary.fit_points_to_shape.fit_points_to_ellipse_stable import FitPointsToEllipseStable
from ....source.auxillary.fit_points_to_shape.ransac_fit_points import RansacFitPoints
from ....source.auxillary.fit_points_to_shape.ellipse import Ellipse
import cv2
import numpy as np

#########################
#
# This is a tester for code/source/auxillary/fit_points_to_shape/fit_points_to_ellipse.py
#
#########################

def generate_points(center, axes, rotation_angle, start_angle, end_angle, num_points=20, noise_std=1.0):
    '''
    We generate some points on an ellipse and add noise
    '''
    
    print 'generating points on the ellipse with:'
    print 'center: ', center
    print 'axes: ', axes
    print 'angle: ', rotation_angle

    theta = np.linspace(start_angle, end_angle, num_points)
    print 'using the angles: '
    print theta * (180 / np.pi)
    
    r1 = axes[0]
    r2 = axes[1]
    
    x = r1 * np.cos(theta)
    y = r2 * np.sin(theta)

    rotation_matrix = np.array([[np.cos(rotation_angle), -np.sin(rotation_angle)],
                                [np.sin(rotation_angle),  np.cos(rotation_angle)]])

    #multiply all the points by the rotation matrix
    points = np.dot(rotation_matrix, np.float64(np.vstack([x,y])))

    #add the translation
    points = points.transpose() + center
    
    #reshape the points
    points = points.reshape((-1,1,2))

    #now add some gaussian noise
    points += noise_std * np.random.randn(*points.shape)
    
    return points

def test():
    print 'testing the fit ellipse algorithm...'
    fit_points = FitPointsToEllipseStable()
    scale = 1
    center = np.array([scale*200, scale*150])
    axes = scale * np.array([174, 100])
    angle = 0
    start_angle_1 = 0.0
    end_angle_1 = 1.5*np.pi
    num_points_1 = 5
    noise_std = 0.0
    points_1 = generate_points(center, axes, angle, start_angle_1, end_angle_1, num_points_1, noise_std)
    # start_angle_2 = 0.0*np.pi
    # end_angle_2 = 0.4 * np.pi
    # num_points_2 = 30
    # points_2 = generate_points(center, axes, angle, start_angle_2, end_angle_2, num_points_2, noise_std)
    # points = np.vstack([points_1, points_2])
    # points = np.int32(points)
    points = np.int32(points_1)
    print 'fitting an ellipse to the following points:'

    ideal_ellipse = Ellipse.from_geometric_parameters(center, axes, angle)
    image = np.zeros((scale*300, scale*500, 3), np.uint8)
    radius = 2
    color = (255, 0, 0)
    thickness = -1

    for point in points:
        translated_point = point[0]
        cv2.circle(image, tuple(np.int32(translated_point).tolist()), radius, color, thickness=thickness)

    cv2.imshow('points', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    #now fit an ellipse to the points
    ellipse, algebraic_residue = fit_points.fit_points(points)

    print 'we got the ellipse: '
    print ellipse.get_conic_string()

    print 'the residue is: ', fit_points.get_algebraic_residue()

    print 'indeed, the ellipse fits the points with residue: ', ellipse.get_algebraic_residue(points)
    print 'the geometric residue is: ', ellipse.get_geometric_residue(points)
    
    ellipse.compute_geometric_parameters()
    center_fitted, axes_fitted, angle_fitted = ellipse.get_geometric_parameters(in_degrees=True)

    print '-'*20
    print 'Summary'
    print '-'*20

    angle = 180 * angle / np.pi
    print 'original center: ', center
    print 'original axes: ', axes
    print 'original angle (degrees): ', angle

    print ''
    
    print 'fitted center: ', center_fitted
    print 'fitted axes: ', axes_fitted
    print 'fitted angle (degrees): ', angle_fitted

    print ''
    
    print 'the residue is          : ', fit_points.get_algebraic_residue()
    print 'the geometric residue is: ', ellipse.get_geometric_residue(points)
    print 'the geometric residue of the actual ellipse: ', ideal_ellipse.get_geometric_residue(points)
    
    color = (0, 255, 0)
    color_fitted = (255, 255, 255)
    
    cv2.ellipse(image, tuple(np.int32(center_fitted).tolist()), tuple(np.int32(axes_fitted).tolist()), angle_fitted, 0, 360, color_fitted, 1, cv2.CV_AA)
    cv2.ellipse(image, tuple(np.int32(center).tolist()), tuple(np.int32(axes).tolist()), angle, 0, 360, color, 1, cv2.CV_AA)
    
    cv2.imshow('the ellipse (green) and the fitted ellipse (white)', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def test_normalization():
    center = np.array([0, 0])
    axes = np.array([10, 5])
    rotation = 0
    ellipse = Ellipse.from_geometric_parameters(center, axes, rotation)
    fit_points = FitPointsToEllipse()
    
    #first generate some points
    points = np.array([[-1,0],[1,5],[2,1],[-3,-1], [-2,1]], np.float64)

    translation = np.array([5,0])
    scale = np.array([2,3])

    points = points*scale + translation

    print 'the mean of the points is: ', points.mean(axis=0)
    print 'the range of the x axis is: (%f, %f)' % (points[:,0].min(), points[:,0].max())
    print 'the range of the y axis is: (%f, %f)' % (points[:,1].min(), points[:,1].max())

    points = points.reshape((-1,1,2))
    
    #now we normalize the points
    normalization_matrix = fit_points._get_normalization_matrix(points)
    normalized_points = fit_points._normalize_points(normalization_matrix, points)

    normalized_points = normalized_points.reshape((-1,2))
    print 'the mean of the normalized points is: ', normalized_points.mean(axis=0)
    print 'the range of the x axis is: (%f, %f)' % (normalized_points[:,0].min(), normalized_points[:,0].max())
    print 'the range of the y axis is: (%f, %f)' % (normalized_points[:,1].min(), normalized_points[:,1].max())
    normalized_points = normalized_points.reshape((-1,1,2))

    #now, apply the normalization matrix to the parameters of our ellipse
    print ''
    print 'we now apply the normalization matrix to an ellipse with the following parameters:'
    print 'ellipse 1 has geometric parameters:'
    print 'center: ', center
    print 'axes: ', axes
    print 'rotation: ', rotation

    ellipse_parameters = ellipse.get_parameters()
    denormalized_ellipse_parameters = fit_points._denormalize_shape_parameters(normalization_matrix, ellipse_parameters)
    denormalized_ellipse = Ellipse(*denormalized_ellipse_parameters.tolist())

    denormalized_ellipse.compute_geometric_parameters()
    dn_center, dn_axes, dn_angle = denormalized_ellipse.get_geometric_parameters()

    print 'denormalized center: ', dn_center
    print 'denormalized axes: ', dn_axes
    print 'denormalized angle (radians): ', dn_angle

    print 'we now check if the algebraic distance of the original points from the denormalized ellipse'
    print 'is equal to the algebraic distance of the normalized points from the original ellipse'

    points_to_denormalized = [denormalized_ellipse.get_algebraic_distance_from_point(point[0]) for point in points]
    normalized_points_to_ellipse = [ellipse.get_algebraic_distance_from_point(point[0]) for point in normalized_points]

    print 'distances from points to denormalized ellipse:'
    print points_to_denormalized

    print 'distances from normalized points to ellipse:'
    print normalized_points_to_ellipse

def test_ransac():
    print 'testing the fit ellipse algorithm with RANSAC...'
    fit_points = FitPointsToEllipseStable()

    MIN_POINTS_TO_FIT = 20
    NUM_ITERATIONS = 10
    DISTANCE_TO_SHAPE_THRESHOLD = 2
    MIN_PERCENTAGE_IN_SHAPE = 0.95

    ransac = RansacFitPoints(fit_points, MIN_POINTS_TO_FIT, NUM_ITERATIONS, DISTANCE_TO_SHAPE_THRESHOLD, MIN_PERCENTAGE_IN_SHAPE)
    
    center = np.array([200, 150])
    axes = np.array([100, 40])
    angle = 0
    start_angle = 0
    end_angle = 2*np.pi
    num_points = 70
    noise_std = 0.1
    points = generate_points(center, axes, angle, start_angle, end_angle, num_points, noise_std).reshape((-1,2))

    #add some outliers
    outliers = np.array([[10,10], [300,200]])
    points = np.vstack([points, outliers])
    points = np.int32(points)
    print 'fitting an ellipse to the following points:'

    image = np.zeros((300, 500, 3), np.uint8)
    radius = 2
    color = (255, 0, 0)
    thickness = -1

    for point in points:
        cv2.circle(image, tuple(np.int32(point).tolist()), radius, color, thickness=thickness)

    cv2.imshow('points', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    #now fit an ellipse to the points
    ellipse, geometric_residue, inlier_mask = ransac.fit_points(points)

    print 'we got the ellipse: '
    print ellipse.get_conic_string()

    print 'the residue is: ', geometric_residue

    ellipse.compute_geometric_parameters()
    center_fitted, axes_fitted, angle_fitted = ellipse.get_geometric_parameters(in_degrees=True)

    print '-'*20
    print 'Summary'
    print '-'*20

    angle = 180 * angle / np.pi
    print 'original center: ', center
    print 'original axes: ', axes
    print 'original angle (degrees): ', angle

    print ''
    
    print 'fitted center: ', center_fitted
    print 'fitted axes: ', axes_fitted
    print 'fitted angle (degrees): ', angle_fitted

    print ''
    
    color = (0, 255, 0)
    color_fitted = (255, 255, 255)
    
    cv2.ellipse(image, tuple(np.int32(center_fitted).tolist()), tuple(np.int32(axes_fitted).tolist()), angle_fitted, 0, 360, color_fitted, 1, cv2.CV_AA)
    cv2.ellipse(image, tuple(np.int32(center).tolist()), tuple(np.int32(axes).tolist()), angle, 0, 360, color, 1, cv2.CV_AA)

    #draw the inliers in blue and outliers in red
    radius = 2
    thickness = -1
    for i, point in enumerate(points):
        color = (255,0,0) if inlier_mask[i] else (0,0,255)
        cv2.circle(image, tuple(np.int32(point).tolist()), radius, color, thickness=thickness)
        
    cv2.imshow('the ellipse (green) and the fitted ellipse (white)', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def draw_reference_ellipse():
    TEST_IMAGE_NAME = 'test_data/video3/out180.png'

    print 'loading image: ', TEST_IMAGE_NAME
    image = cv2.imread(TEST_IMAGE_NAME, 1)

    center = np.array([529, 185])
    axes = np.array([174, 45])
    angle = 0
    start_angle = 0
    end_angle = 360
    color = (0,255,0)
    cv2.ellipse(image, tuple(np.int32(center).tolist()), tuple(np.int32(axes).tolist()), angle, start_angle, end_angle, color, 1, cv2.CV_AA)

    cv2.imshow('the reference ellipse', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

if __name__ == '__main__':
    np.set_printoptions(precision=4, linewidth=150)
    test()
    #test_normalization()
    #test_ransac()
    #draw_reference_ellipse()
