from ....source.auxillary.fit_points_to_shape.fit_points_to_line import FitPointsToLine
import cv2
import numpy as np

#########################
#
# This is a tester for code/source/auxillary/fit_points_to_shape/fit_points_to_line.py
#
#########################

def generate_points():
    '''
    We generate some points on a line and add noise
    '''

    print 'generating points near the line y = x + 1'

    x = np.linspace(0, 200, 100)
    y = 5*x + 1

    points = np.vstack([x,y]).transpose().reshape((-1, 1, 2))

    #now add some gaussian noise
    points += np.random.randn(*points.shape)
    
    return points

def generate_horizontal_line():
    x_coords = np.linspace(50, 450, 15)
    y_coords = 250 * np.ones(len(x_coords))

    coords = np.vstack([x_coords, y_coords]).transpose().reshape((-1,1,2))
    coords += 0.01 * np.random.randn(*coords.shape)

    return coords

def generate_horizontal_line_with_outliers():
    x_coords = np.linspace(50, 450, 15)
    y_coords = 250 * np.ones(len(x_coords))
    coords = np.vstack([x_coords, y_coords]).transpose().reshape((-1,1,2))
    
    x_outliers = np.linspace(450, 440, 10)[1:]
    y_outliers = np.linspace(250, 260, len(x_outliers))
    outlier_coords = np.vstack([x_outliers, y_outliers]).transpose().reshape((-1,1,2))

    coords += 0.1 * np.random.randn(*coords.shape)

    return np.vstack([coords, outlier_coords])

def generate_vertical_line():
    y_coords = np.linspace(50, 400, 15)
    x_coords = 250 * np.ones(len(y_coords))

    coords = np.vstack([x_coords, y_coords]).transpose().reshape((-1,1,2))
    coords += 1.0 * np.random.randn(*coords.shape)

    return coords

def draw_line(image, line):

    if np.abs(line._b) > 10**(-2):
        x = np.array([0, image.shape[1]])
        y = (-line._a / line._b)*x - (line._c / line._b)

    else:
        y = np.array([0, image.shape[0]])
        x = -(line._b / line._a)*y - (line._c / line._a)
        
    points = np.int32(np.vstack([x,y]).transpose().reshape((-1, 2)))

    cv2.line(image, tuple(points[0].tolist()), tuple(points[1].tolist()), (0, 255, 0), thickness=2)
    return

def test_horizontal_line():
    print 'testing the fit lines algorithm with a horizontal line...'

    points = generate_horizontal_line_with_outliers()
    #points = generate_horizontal_line()

    print 'fitting a line to the following points:'

    image = np.zeros((500, 500, 3), np.uint8)
    radius = 4
    color = (255, 0, 0)
    thickness = -1

    for point in points:
        cv2.circle(image, tuple(np.int32(point[0]).tolist()), radius, color, thickness=thickness)

    cv2.imshow('points', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    #now fit a line to the points
    fit_points = FitPointsToLine()
    line = fit_points.fit_points(points, weight_by_interval_length=True)

    print 'we got the line: (%f, %f, %f)' % (line._a, line._b, line._c)
    print 'with total residue: ', fit_points.get_algebraic_residue()
    print 'with average residue: ', fit_points.get_algebraic_residue() / len(points)

    print 'the directions are: '
    print [line.get_direction(points[i][0], points[i+1][0]) for i in range(len(points)-1)]
    
    print 'drawing the line on the image'
    draw_line(image, line)

    cv2.imshow('points with line', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def test_vertical_line():
    print 'testing the fit lines algorithm with a vertical line...'

    points = generate_vertical_line()

    print 'fitting a line to the following points:'

    image = np.zeros((500, 500, 3), np.uint8)
    radius = 4
    color = (255, 0, 0)
    thickness = -1

    for point in points:
        cv2.circle(image, tuple(np.int32(point[0]).tolist()), radius, color, thickness=thickness)

    cv2.imshow('points', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    #now fit a line to the points
    fit_points = FitPointsToLine()
    line = fit_points.fit_points(points)

    print 'we got the line: (%f, %f, %f)' % (line._a, line._b, line._c)
    print 'with average residue: ', fit_points.get_algebraic_residue() / len(points)
    
    print 'drawing the line on the image'
    draw_line(image, line)

    cv2.imshow('points with line', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def test():
    print 'testing the fit lines algorithm...'

    points = generate_points()

    print 'fitting a line to the following points:'

    image = np.zeros((300, 300, 3), np.uint8)
    radius = 4
    color = (255, 0, 0)
    thickness = -1

    for point in points:
        cv2.circle(image, tuple(np.int32(point[0]).tolist()), radius, color, thickness=thickness)

    cv2.imshow('points', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    #now fit a line to the points
    fit_points = FitPointsToLine()
    line = fit_points.fit_points(points)

    print 'we got the line: (%f, %f, %f)' % (line._a, line._b, line._c)
    print 'with average residue: ', fit_points.get_algebraic_residue() / len(points)
    
    print 'drawing the line on the image'
    draw_line(image, line)

    cv2.imshow('points with line', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

if __name__ == '__main__':
    test()
    #test_vertical_line()
