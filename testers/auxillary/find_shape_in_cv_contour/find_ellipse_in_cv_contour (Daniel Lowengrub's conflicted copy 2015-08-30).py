from ....source.auxillary.fit_points_to_shape.fit_points_to_ellipse_stable import FitPointsToEllipseStable
from ....source.auxillary.find_shape_in_cv_contour.find_ellipse_in_cv_contour import FindEllipseInCvContour
from ..fit_points_to_shape.fit_points_to_ellipse import generate_points
import cv2
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse as EllipsePlotter

#################
# This is a tester for code/source/auxillary/find_shape_in_cv_contour/find_line_in_cv_contour
#
# We generate some contours and look for lines.
################

POINT_COLORS = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (50, 0, 0), (0, 50, 0)]
FONT_TYPE = cv2.FONT_HERSHEY_COMPLEX_SMALL
FONT_SCALE = 1.0
FONT_COLOR = (0, 255, 0)
LINE_THICKNESS = 2
LINE_TYPE = cv2.CV_AA

def plot_ellipse_and_points(ellipse, points):
    '''
    plot the points and ellipse with pyplot so we can see them better
    '''
    ellipse.compute_geometric_parameters()
    center, axes, angle = ellipse.get_geometric_parameters(in_degrees=True)
    points = points.reshape((-1,2))
    
    fig = plt.figure()
    ax = fig.add_subplot(111, aspect='equal')
    ell = EllipsePlotter(xy = center, width = 2*axes[0], height= 2*axes[1], angle=angle)

    ax.plot(points[:,0], points[:,1], 'ro')
    ax.add_artist(ell)

    ell.set_clip_box(ax.bbox)
    ell.set_alpha(0.5)
    ell.set_facecolor((1,0,0))

    ax.set_xlim(points[:,0].min()-10, points[:,0].max()+10)
    ax.set_ylim(points[:,1].min()-10, points[:,1].max()+10)

    plt.show()
    
def display_matching_summary(image, cv_contours, matches_in_contours):

    cv2.imshow('found ellipses in these contours', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    fig = plt.figure()
    ax = fig.add_subplot(111, aspect='equal')
    ellipse_plotters = []
    
    match_data = [] #here we store tuples (distance from start to end, number_of_points, total_residue). One tuple per match.
    #first draw all the matches on the image, together with numbers
    for contour_index, cv_contour in enumerate(cv_contours):
        points = cv_contour.reshape((-1,2))

        #plot the contour directions in pyplot
        ax.quiver(points[:-1,0], -points[:-1,1], points[1:,0]-points[:-1,0], -(points[1:,1]-points[:-1,1]), scale_units='xy', angles='xy', scale=1)

        for match in matches_in_contours[contour_index]:
            ellipse = match[0]
            point_mask = match[1]
            points_in_match = points[point_mask]
            match_index = len(match_data)

            color = POINT_COLORS[match_index % len(POINT_COLORS)]

            #draw the ellipse on the image
            ellipse.compute_geometric_parameters()
            center, axes, angle = ellipse.get_geometric_parameters(in_degrees=True)
            cv2.ellipse(image, tuple(np.int32(center).tolist()), tuple(np.int32(axes).tolist()), angle, 0, 360, color, 1, cv2.CV_AA)

            #add a plotter so we can plot it with pyplot
            ax.plot(points_in_match[:,0], -points_in_match[:,1], '.', markerfacecolor=np.array(color)/255.0)

            ellipse_plotter = EllipsePlotter(xy = (center[0], -center[1]), width = 2*axes[0], height= 2*axes[1], angle=-angle)
            ax.add_artist(ellipse_plotter)
            ellipse_plotter.set_clip_box(ax.bbox)
            ellipse_plotter.set_alpha(0.4)
            ellipse_plotter.set_facecolor(np.array(color) / 255.0)

            #draw the points in one of the possible colors
            radius = 1
            thickness = -1

            for point in points_in_match:
                cv2.circle(image, tuple(np.int32(point).tolist()), radius, color, thickness=thickness)
    
            #draw the match index so we can find it later
            text_position = tuple(np.int32(points_in_match[0]) + np.array([-5, -5]))
            cv2.putText(image, str(match_index), text_position, FONT_TYPE, FONT_SCALE, FONT_COLOR, LINE_THICKNESS, LINE_TYPE)

            #now store the various match statistics
            fit_points = FitPointsToEllipseStable()
            fit_points.fit_points(points_in_match.reshape((-1,1,2)))

            algebraic_residue = fit_points.get_algebraic_residue() #/ len(points_in_match)
            geometric_residue = ellipse.get_geometric_residue(points_in_match.reshape((-1,1,2)))
            number_of_points = len(points_in_match)
            directions = [ellipse.get_direction(points_in_match[i], points_in_match[i+1]) for i in range(len(points_in_match)-1)]
            
            match_data.append((algebraic_residue, geometric_residue, number_of_points,
                               axes, ellipse.get_conic_string(), directions,
                               ellipse, points_in_match))

    print '\n\n'
    print '##########################'
    print 'Summary of matches'
    print '##########################'
    print '\n'
    
    #now print the match data
    for match_index, data in enumerate(match_data):
        print 'match number %d:' % match_index
        print '    match color:           ', POINT_COLORS[match_index % len(POINT_COLORS)]
        print '    algebraic residue:     ', data[0]
        print '    geometric residue:     ', data[1]
        print '    number of points:      ', data[2]
        print '    axes:                  ', data[3]
        print '    parameters:            ', data[4]
        print '    directions:            ', data[5]
        print '    set of directions:     ', set(data[5])
        print '\n'

    cv2.imshow('the ellipses we found', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    ax.set_xlim(0, image.shape[1])
    ax.set_ylim(-image.shape[0],0)

    plt.show()
    
    

def generate_basic_contour():
    '''
    We generate a single ellipse

    '''
    number_of_outer_points = 100
    number_of_inner_points = 100
    noise = 0.1
    
    center = np.array([200, 200])
    rotation_angle = np.pi/4.0

    outer_axes = np.array([200, 150])
    outer_start_angle = 0
    outer_end_angle = 2*np.pi

    inner_axes = np.array([180, 110])
    inner_start_angle = 2*np.pi
    inner_end_angle = 0
    
    outer_ellipse = generate_points(center, outer_axes, rotation_angle, outer_start_angle, outer_end_angle, num_points=number_of_outer_points, noise_std=noise)
    inner_ellipse = generate_points(center, inner_axes, rotation_angle, inner_start_angle, inner_end_angle, num_points=number_of_inner_points, noise_std=noise)

    line_x_coords = np.linspace(380, 50, 100)
    line_y_coords = np.ones(len(line_x_coords)) * 200
    line_coords = np.vstack([line_x_coords, line_y_coords]).transpose().reshape((-1,1,2))

    contour = np.vstack([outer_ellipse, inner_ellipse, line_coords])
    #contour = np.vstack([outer_ellipse, inner_ellipse])
    contour += 0.01*np.random.randn(*contour.shape)

    point_mask = np.ones(len(contour), np.bool)
    
    return np.int32(contour), point_mask

def test_case_1():
    '''
    Test what happens if there are not enough contiguous points, even though they span a long arc.
    '''

    noise = 0.1
    
    center = np.array([250, 200])
    rotation_angle = 0
    axes = np.array([100, 80])
    start_angle = 0
    end_angle = 2*np.pi
    ellipse = generate_points(center, axes, rotation_angle, start_angle, end_angle, num_points=30, noise_std=noise)
    ellipse = ellipse[:-1].reshape((-1,2))
    
    top_right_line_x = np.linspace(400, ellipse[0][0], 10)
    top_right_line_y = ellipse[0][1] * np.ones(len(top_right_line_x))
    top_right_line = np.vstack([top_right_line_x, top_right_line_y]).transpose()

    top_left_line_x = np.linspace(ellipse[15][0], 100, 10)
    top_left_line_y = ellipse[15][1] * np.ones(len(top_left_line_x))
    top_left_line = np.vstack([top_left_line_x, top_left_line_y]).transpose()

    bottom_left_line_x = np.linspace(100, ellipse[16][0], 10)
    bottom_left_line_y = ellipse[16][1] * np.ones(len(bottom_left_line_x))
    bottom_left_line = np.vstack([bottom_left_line_x, bottom_left_line_y]).transpose()

    bottom_right_line_x = np.linspace(ellipse[-1][0], 400, 10)
    bottom_right_line_y = ellipse[-1][1] * np.ones(len(bottom_right_line_x))
    bottom_right_line = np.vstack([bottom_right_line_x, bottom_right_line_y]).transpose()

    contour = np.vstack([top_right_line, ellipse[0:16], top_left_line, bottom_left_line, ellipse[16:], bottom_right_line]).reshape((-1,1,2))
    point_mask = np.ones(len(contour), np.bool)
    
    return np.int32(contour), point_mask


def basic_test():
    image = np.zeros((500, 500, 3))
    #cv_contour, point_mask = generate_basic_contour()
    cv_contour, point_mask = test_case_1()

    image_with_contour = image.copy()
    radius = 2
    color = (255,0,0)
    masked_color = (50,50,50)
    thickness = -1

    print cv_contour.shape
    
    cv2.drawContours(image_with_contour, [cv_contour], -1, (255,255,255), 1)
    for i,point in enumerate(cv_contour):
        if point_mask[i] == 1:
            #image_with_contour[point[0][1], point[0][0]] = np.array(color)
            cv2.circle(image_with_contour, tuple(np.int32(point[0]).tolist()), radius, color, thickness=thickness)
        else:
            #image_with_contour[point[0][1], point[0][0]] = np.array(masked_color)
            cv2.circle(image_with_contour, tuple(np.int32(point[0]).tolist()), radius, masked_color, thickness=thickness)
            
    print 'we are starting with the following contour: '
    cv2.imshow('image with contours and points', image_with_contour)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    ellipse_finder = FindEllipseInCvContour()

    print 'looking for ellipses in the contour...'
    matches = ellipse_finder.find_shape(cv_contour, point_mask)

    display_matching_summary(image_with_contour, [cv_contour], [matches])
        
    return


def test_on_soccer_image():
    from ....source.image_processing.contour_generator.edges_contour_generator import EdgesContourGenerator
    from ....source.image_processing.field_extractor.stupid_field_extractor import StupidFieldExtractor

    ellipse_finder = FindEllipseInCvContour()
    
    #interesting test images
    #215
    #141
    #2
    #96
    #25
    
    TEST_IMAGE_NAME = 'test_data/video3/out96.png'

    print 'loading image: ', TEST_IMAGE_NAME
    image = cv2.imread(TEST_IMAGE_NAME, 1)

    print 'getting the field mask...'
    field_extractor = StupidFieldExtractor()
    field_mask = field_extractor.get_field_mask(image)
    
    print 'generating contours...'
    contourGenerator = EdgesContourGenerator()
    contours = contourGenerator.generate_contours(image, mask=field_mask)
    cv_contours = [contour.get_cv_contour() for contour in contours]
    matches_in_contours = []
    
    print 'finding lines in contours...'
    #cv_contours = cv_contours[14:15]
    
    for cv_contour in cv_contours:
        #print 'finding lines in this contour'
        #image_copy = image.copy()

        #first draw the contours on the image in white
        cv2.drawContours(image, [cv_contour], -1, (255, 255, 255), 1)

        #then draw all the points in blue
        points = cv_contour.reshape((-1,2))
        color = (255,0,0)
    
        for point in points:
            image[point[1],point[0]] = np.array([255,0,0])
        
        #cv2.imshow('image with contour and points', image_copy)
        #cv2.waitKey(0)
        #cv2.destroyAllWindows()

        print 'looking for lines in the contour...'
        matches = ellipse_finder.find_shape(cv_contour)
        matches_in_contours.append(matches)
        #print 'drawing the lines we found in green'

        #for match in matches:
        #    line = match[0]
        #    draw_line(image_copy, line)

        #cv2.imshow('the lines we found are in green', image_copy)
        #cv2.waitKey(0)
        #cv2.destroyAllWindows()

    display_matching_summary(image, cv_contours, matches_in_contours)
        
if __name__ == '__main__':

    basic_test()
    #test_on_soccer_image()
