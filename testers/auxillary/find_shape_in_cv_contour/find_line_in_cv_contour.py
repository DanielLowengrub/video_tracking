from ....source.auxillary.fit_points_to_shape.fit_points_to_line import FitPointsToLine
from ....source.auxillary.find_shape_in_cv_contour.find_line_in_cv_contour import FindLineInCvContour
import cv2
import numpy as np

#################
# This is a tester for code/source/auxillary/find_shape_in_cv_contour/find_line_in_cv_contour
#
# We generate some contours and look for lines.
################

POINT_COLORS = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (5, 0, 0), (255, 255, 255)]
FONT_TYPE = cv2.FONT_HERSHEY_COMPLEX_SMALL
FONT_SCALE = 1.0
FONT_COLOR = (0, 255, 0)
LINE_THICKNESS = 2
LINE_TYPE = cv2.CV_AA

def draw_line(image, line, color=(0,255,0)):

    if np.abs(line._b) > 10**(-2):
        x = np.array([0, image.shape[1]])
        y = (-line._a / line._b)*x - (line._c / line._b)

    else:
        y = np.array([0, image.shape[0]])
        x = -(line._b / line._a)*y - (line._c / line._a)
        
    points = np.int32(np.vstack([x,y]).transpose().reshape((-1, 2)))

    cv2.line(image, tuple(points[0].tolist()), tuple(points[1].tolist()), color, thickness=1)
    return

def display_matching_summary(image, cv_contours, matches_in_contours):

    match_data = [] #here we store tuples (distance from start to end, number_of_points, total_residue). One tuple per match.
    #first draw all the matches on the image, together with numbers
    for contour_index, cv_contour in enumerate(cv_contours):
        points = cv_contour.reshape((-1,2))
        
        for match in matches_in_contours[contour_index]:
            line = match[0]
            point_mask = match[1]
            points_in_match = points[point_mask]
            match_index = len(match_data)

            color = POINT_COLORS[match_index % len(POINT_COLORS)]

            #draw the line on the image
            draw_line(image, line, color)

            #draw the points in one of the possible colors
            radius = 3
            thickness = -1

            for point in points_in_match:
                cv2.circle(image, tuple(np.int32(point).tolist()), radius, color, thickness=thickness)
    
            #draw the match index so we can find it later
            text_position = tuple(np.int32(points_in_match[0]) + np.array([-5, -5]))
            cv2.putText(image, str(match_index), text_position, FONT_TYPE, FONT_SCALE, FONT_COLOR, LINE_THICKNESS, LINE_TYPE)

            #now store the various match statistics
            fit_points = FitPointsToLine()
            fit_points.fit_points(points_in_match.reshape((-1,1,2)), weight_by_interval_length=False)

            residue = fit_points.get_algebraic_residue() #/ len(points_in_match)
            start_to_end_distance = np.linalg.norm(points_in_match[-1] - points_in_match[0])
            number_of_points = len(points_in_match)

            match_data.append((start_to_end_distance, residue, number_of_points))

    print '\n\n'
    print '##########################'
    print 'Summary of matches'
    print '##########################'
    print '\n'
    
    #now print the match data
    for match_index, data in enumerate(match_data):
        print 'match number %d:' % match_index
        print '    match color:                             ', POINT_COLORS[match_index % len(POINT_COLORS)]
        print '    distance from first point to last point: ', data[0]
        print '    total residue:                           ', data[1]
        print '    number of points:                        ', data[2]
        print '\n'

    cv2.imshow('the lines we found are in green', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    return

def generate_basic_contour():
    '''
    We generate a single line:
    (50,100)----->------------>--(450,100)
    (50,100+h)----------<--------(450,100+h)
    '''
    h=10
    points_in_top_row = 3
    points_in_bottom_row = 20
    
    x_coords_top = np.linspace(50, 450, points_in_top_row)
    coords_top = np.vstack([x_coords_top, 100*np.ones(len(x_coords_top))]).transpose()

    x_coords_bottom = np.linspace(50, 450, points_in_bottom_row)
    coords_bottom = np.vstack([x_coords_bottom, (100 + h)*np.ones(len(x_coords_bottom))]).transpose()
    coords_bottom = coords_bottom[::-1]
    
    contour = np.vstack([coords_top, coords_bottom]).reshape((-1,1,2))

    #add noise
    contour += 0.1*np.random.randn(*contour.shape)

    point_mask = np.ones(len(contour), np.bool)
    #point_mask[len(coords_top):] = 1
    
    return np.int32(contour), point_mask

def generate_T_contour():
    '''
    We generate a T shaped contour: (h = thickness of the T)
    
    (50,100)----->-------------->---------------->--(450,100)
    (50,100+h)-<-(250-h/2,100+h)  (250+h/2,100+h)-<-(450,100+h)
                              /|\ |
                               :  :
                               :  :
                               | \|/
                   (250-h/2,450)--(250+h/2,450)
    '''
    h = 10
    
    x_coords_top_horizontal = np.linspace(50, 450, 15)
    y_coords_top_horizontal = 100 * np.ones(len(x_coords_top_horizontal))
    coords_top_horizontal = np.vstack([x_coords_top_horizontal, y_coords_top_horizontal]).transpose()
    
    x_coords_bottom_right_horizontal = np.linspace(450, 250+h/2, 4)
    y_coords_bottom_right_horizontal = (100+h) * np.ones(len(x_coords_bottom_right_horizontal))
    coords_bottom_right_horizontal = np.vstack([x_coords_bottom_right_horizontal, y_coords_bottom_right_horizontal]).transpose()

    y_coords_right_vertical = np.linspace(100+h, 450, 30)[1:]
    x_coords_right_vertical = (250 + h/2) * np.ones(len(y_coords_right_vertical))
    coords_right_vertical = np.vstack([x_coords_right_vertical, y_coords_right_vertical]).transpose()

    y_coords_left_vertical = np.linspace(450, 100+h, 15)[:-1]
    x_coords_left_vertical = (250 - h/2) * np.ones(len(y_coords_left_vertical))
    coords_left_vertical = np.vstack([x_coords_left_vertical, y_coords_left_vertical]).transpose()

    x_coords_bottom_left_horizontal = np.linspace(250-h/2, 50, 4)
    y_coords_bottom_left_horizontal = (100+h) * np.ones(len(x_coords_bottom_left_horizontal))
    coords_bottom_left_horizontal = np.vstack([x_coords_bottom_left_horizontal, y_coords_bottom_left_horizontal]).transpose()

    
    contour = np.vstack([coords_top_horizontal, coords_bottom_right_horizontal, coords_right_vertical, coords_left_vertical, coords_bottom_left_horizontal]).reshape((-1,1,2))

    #add noise
    contour += 0.01*np.random.randn(*contour.shape)
    
    point_mask = np.ones(len(contour), np.bool)
    #point_mask[len(coords_top):] = 1
    
    return np.int32(contour), point_mask


def basic_test():
    image = np.zeros((500, 500, 3))
    cv_contour, point_mask = generate_T_contour()

    image_with_contour = image.copy()
    radius = 3
    color = (255,0,0)
    masked_color = (50,50,50)
    thickness = -1

    print cv_contour.shape
    
    cv2.drawContours(image_with_contour, [cv_contour], -1, (255,0,0), 1)
    for i,point in enumerate(cv_contour):
        if point_mask[i] == 1:
            cv2.circle(image_with_contour, tuple(np.int32(point[0]).tolist()), radius, color, thickness=thickness)
        else:
            cv2.circle(image_with_contour, tuple(np.int32(point[0]).tolist()), radius, masked_color, thickness=thickness)
            
    print 'we are starting with the following contour: '
    cv2.imshow('image with contours and points', image_with_contour)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    line_finder = FindLineInCvContour()

    print 'looking for lines in the contour...'
    matches = line_finder.find_shape(cv_contour, point_mask)

    display_matching_summary(image_with_contour, [cv_contour], [matches])

    return
    
def test_on_soccer_image():
    from ....source.image_processing.contour_generator.edges_contour_generator import EdgesContourGenerator
    from ....source.image_processing.field_extractor.stupid_field_extractor import StupidFieldExtractor

    line_finder = FindLineInCvContour()
    
    #interesting test images
    #215
    #141
    #2
    #96

    TEST_IMAGE_NAME = 'test_data/video3/out215.png'

    print 'loading image: ', TEST_IMAGE_NAME
    image = cv2.imread(TEST_IMAGE_NAME, 1)

    print 'getting the field mask...'
    field_extractor = StupidFieldExtractor()
    field_mask = field_extractor.get_field_mask(image)
    
    print 'generating contours...'
    contourGenerator = EdgesContourGenerator()
    contours = contourGenerator.generate_contours(image, mask=field_mask)
    cv_contours = [contour.get_cv_contour() for contour in contours]
    matches_in_contours = []
    
    print 'finding lines in contours...'
    #cv_contours = cv_contours[14:15]
    
    for cv_contour in cv_contours:
        #print 'finding lines in this contour'
        #image_copy = image.copy()

        #first draw the contours on the image in white
        cv2.drawContours(image, [cv_contour], -1, (255, 255, 255), 1)

        #then draw all the points in blue
        points = cv_contour.reshape((-1,2))
        color = (255,0,0)
    
        for point in points:
            image[point[1],point[0]] = np.array([255,0,0])
        
        #cv2.imshow('image with contour and points', image_copy)
        #cv2.waitKey(0)
        #cv2.destroyAllWindows()

        print 'looking for lines in the contour...'
        matches = line_finder.find_shape(cv_contour)
        matches_in_contours.append(matches)
        #print 'drawing the lines we found in green'

        #for match in matches:
        #    line = match[0]
        #    draw_line(image_copy, line)

        #cv2.imshow('the lines we found are in green', image_copy)
        #cv2.waitKey(0)
        #cv2.destroyAllWindows()

    display_matching_summary(image, cv_contours, matches_in_contours)
        
if __name__ == '__main__':

    #basic_test()
    test_on_soccer_image()
