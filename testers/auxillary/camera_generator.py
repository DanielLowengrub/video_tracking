import numpy as np
from ...source.auxillary.camera_matrix import CameraMatrix
from ...source.auxillary import planar_geometry
import cv2

def build_camera(camera_position, camera_direction, focus, camera_position_image_coords):
    '''
    camera_position - a numpy array with length 3. It is the camera position in world coords.
    camera_direction - a numpy array with length 3 (and norm 1). It is the camera direction in world coords
    focus - a float
    camera_position_image_coords - a numpy array with length 2. It is the camera position in image coords

    We use the following coordinate systems:
    WORLD:               CAMERA:     IMAGE:

      Z                 y          ------- u
      |                 | z       |    y
      |____ X           |/        |    |
      /                  ---x     | (u0,v0)-- x
     /                            v 
    Y
    
    The world coords are (X,Y,Z), the camera coords are (x,y,z) and the image coords are (u,v)
    Note that XxY = -Z. I.e, we are using a coordinate system with orientation -1.
    '''
    Rinv_z = camera_direction
    Rinv_y = np.array([0,0,1])
    Rinv_x = np.cross(Rinv_y, Rinv_z)
    Rinv_y = -np.cross(Rinv_x, Rinv_z)

    Rinv_x /= np.linalg.norm(Rinv_x)
    Rinv_y /= np.linalg.norm(Rinv_y)
    Rinv_z /= np.linalg.norm(Rinv_z)
    Rinv = np.vstack([Rinv_x,Rinv_y,Rinv_z]).transpose()
    
    R = np.linalg.inv(Rinv)
    
    T  = -camera_position
    RT = np.matmul(R, T)
    R_RT = np.hstack([R, RT.reshape((3,1))])
    
    f = focus
    u0,v0 = camera_position_image_coords

    if len(f) == 1:
        C = np.array([[f,   0,  u0],
                      [0,  -f,  v0],
                      [0,   0,  1]])

    else:
        C = np.array([[f[0],   0,  u0],
                      [0,  -f[1],  v0],
                      [0,   0,  1]])


    P = np.matmul(C, R_RT)

    return CameraMatrix(P)

def apply_camera(image, camera, output_image_shape, pad_with_zero=False):
    '''
    image - a numpy array representing an image
    camera - a CameraMatrix object
    output_image_shape - a tuple (height, width)
    pad_with_zero - if True, the image is padded with zeros before being transformed.
       this can be done to fix a bug in cv2.warpPerspective

    we consider the image as being in world coordinates by putting it in the plane z=0.
    we then use the camera to map it to an image with the given shape.
    '''
    H = camera.get_homography_matrix()

    if pad_with_zero:
        #pad all axes with zero
        image = np.pad(image, 1, mode='constant', constant_values=0)

        #also shift the homography by (-1,-1) (i.e, (0,0) -> (-1,-1))
        M = planar_geometry.translation_matrix(np.array([-1,-1], np.float32))
        H = np.matmul(H, M)
        
    output_image = cv2.warpPerspective(image, H, output_image_shape)
    return output_image

def build_camera_direction(theta_x, theta_z):
    '''
    theta_x - rotation around the x axis
    theta_z - rotation around the z axis

    If both angles are zero, the returned vector is [0, -1, 0]
    theta_z rotates this vector clockwise around the z axis
    theta_x rotates this vector clockwise around the x axis
    return - a numpy array with length 3
    '''
    return np.array([np.cos(theta_x)*np.sin(theta_z),
                     -np.cos(theta_x)*np.cos(theta_z),
                     -np.sin(theta_x)])

def _get_camera_coordinate_change(C):
    #we want to transform the camera parameters so that
    #the diagonal of C is (focus, -focus, 1), where focus is some positive number.
    M = np.eye(3)
    if C[0,0] < 0:
        M[0,0] = -1

    if C[1,1] > 0:
        M[1,1] = -1

    if C[2,2] < 0:
        M[2,2] = -1

    return M

def _compute_angles_from_camera_direction(camera_direction):
    theta_x = np.arctan2(-camera_direction[2], -camera_direction[1])
    theta_z = np.arctan2(camera_direction[0],  -camera_direction[1])
    return theta_x, theta_z

def compute_camera_parameters(camera):
    '''
    return - a tuple (position, theta_x, theta_z, focus, camera_center_in_image_coords)
    position - a numpy array with shape (3,)
    theta_x, theta_z - floats
    focus - a numpy array with shape (2,)
    camera_center_in_image_coords - a numpy array with shape (2,)
    '''
    position = camera.get_camera_position()
    C = camera.get_internal_parameters()
    R = camera.get_world_to_camera_rotation_matrix()
    
    #transform the camera coordinates so that y is pointing up
    M = _get_camera_coordinate_change(C)
    C = np.matmul(C, M)
    R = np.matmul(M, R)
    
    #scale C so that C[2,2]=1
    C /= C[2,2]
    
    #the camera direction is where the z axis is pointing
    Rinv = np.linalg.inv(R)
    camera_direction = Rinv[:,2]
    
    #get the angles from the camera direction
    theta_x, theta_z = _compute_angles_from_camera_direction(camera_direction)

    camera_center = C[:2, 2]
    focus = np.array([C[0,0], -C[1,1]])
    
    return position, theta_x, theta_z, focus, camera_center
