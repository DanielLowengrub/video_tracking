import numpy as np
from ...source.auxillary.interval import IntervalList

def test_interval_list_from_boolean_array():
    array = np.array([0,0,1,1,1,0,1,1,0,0,1], np.bool)
    print 'generating intervals from the array:'
    print array

    intervals = IntervalList.from_boolean_array(array).get_intervals()

    print 'we got:'
    print [str(interval) for interval in intervals]

if __name__ == '__main__':
    test_interval_list_from_boolean_array()
                    
