import numpy as np
import tensorflow as tf
from ...source.auxillary import tf_operations

def test_apply_cameras_to_points():
    camera = np.hstack([np.eye(3), np.zeros((3,1))]).astype(np.float32)
    point  = np.array([1,1,2], np.float32)
    image_point = np.matmul(camera, np.append(point,1))
    image_point = image_point[:2] / image_point[2]
    
    print 'camera:'
    print camera
    print 'point: ', point
    print 'image point: ', image_point

    cameras = np.stack([camera for i in range(5)])
    points  = np.stack([point for i in range(5)])

    image_points = tf_operations.apply_cameras_to_points(cameras, points)

    sess = tf.Session()
    image_points = sess.run(image_points)
    sess.close()

    print 'image points: '
    print image_points


if __name__ == '__main__':
    test_apply_cameras_to_points()
