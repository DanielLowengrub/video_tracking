import os
import pickle
import cv2
import numpy as np
import itertools
from ....source.dynamics.particle_filter_fast.player_tracker import PlayerTracker
from ....source.dynamics.particle_filter_fast.frame_data import PlayerCandidate, FrameData
from ....source.preprocessing import image_loader
from ....source.preprocessing.annotation_generator.annotation_generator import AnnotationGenerator
from ....source.preprocessing.absolute_homography_generator.absolute_homography_generator import AbsoluteHomographyGenerator
from ....source.preprocessing.camera_samples_interpolator.camera_samples_interpolator import CameraSamplesInterpolator
from ....source.preprocessing.players_generator.players_generator import PlayersGenerator
from ....source.preprocessing.team_labels_generator.team_labels_generator import TeamLabelsGenerator
from ....source.auxillary import iterator_operations
from ....source.auxillary.soccer_field_geometry import SoccerFieldGeometry
from .player_tracker_visualizer import PlayerTrackerVisualizer

##########
# In this script we load preprocessing data and feed it to the PlayerTracker,
# We then use player_tracker_visualization to output a visualization of the algorithm at work.
#########

# IMAGE_DIRECTORY = 'test_data/video4'
# PREPROCESSING_NAME = 'images_50_269'

# IMAGE_DIRECTORY = 'test_data/video5'
# PREPROCESSING_NAME = 'images_50_55'

IMAGE_DIRECTORY = 'test_data/images_0_269'
PREPROCESSING_NAME = 'images_0_269'

DATA_DIRECTORY = 'output/scripts/preprocessing/data'

ANNOTATION_DIRECTORY = os.path.join(DATA_DIRECTORY, PREPROCESSING_NAME, 'annotation')
PLAYERS_DIRECTORY = os.path.join(DATA_DIRECTORY, PREPROCESSING_NAME, 'players')
CAMERA_DIRECTORY = os.path.join(DATA_DIRECTORY, PREPROCESSING_NAME, 'camera')
TEAM_LABELS_DIRECTORY = os.path.join(DATA_DIRECTORY, PREPROCESSING_NAME, 'team_labels')

OUTPUT_DIRECTORY = os.path.join('output/tester/particle_filter_fast/preprocessing_data_loader', PREPROCESSING_NAME, 'data')
VISUALIZATION_DIRECTORY = os.path.join('output/tester/particle_filter_fast/preprocessing_data_loader', PREPROCESSING_NAME,
                                       'visualization')
DEBUGGING_DIRECTORY = os.path.join('output/tester/particle_filter_fast/preprocessing_data_loader', PREPROCESSING_NAME,
                                   'debugging')

def load_absolute_soccer_field():
    '''
    Return a tuple (soccer field, absolute image)
    '''
    ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)
    SCALE = 6
    FIELD_WIDTH = absolute_image.shape[1]-1
    FIELD_LENGTH = absolute_image.shape[0]-1
    GRID_WIDTH = 20
    GRID_LENGTH = 14
    soccer_field = SoccerFieldGeometry(SCALE, FIELD_WIDTH, FIELD_LENGTH, GRID_WIDTH, GRID_LENGTH)

    return soccer_field, absolute_image

def save_player_data(player_tracker):
    data_dict = dict()
    data_dict['player_positions'] = player_tracker.get_player_positions()
    data_dict['player_position_variances'] = player_tracker.get_player_position_variances()
    data_dict['player_velocity'] = player_tracker.get_player_velocities()
    data_dict['player_labels'] = player_tracker.get_team_labels()
    
    #data_dict['player_position_entropies'] = player_tracker.get_player_position_entropies()
    #data_dict['player_obstructed_percentages'] = player_tracker.get_player_obstructed_percentages()
    #data_dict['player_unobstructed_foreground_percentages'] = player_tracker.get_player_unobstructed_foreground_percentages()

    f = open(os.path.join(OUTPUT_DIRECTORY, 'player_data.pckl'), 'w')
    pickle.dump(data_dict, f)
    f.close()
        
    return

def load_frame_data_iterator(image_directory, annotation_directory, camera_directory, players_directory,
                             team_labels_directory):
    '''
    Return an iterator of tuples (interval, list of FrameData objects in the interval)
    '''

    print 'loading the preprocessing data...'

    #these are iterators of the form (frame_0, frame_1,...)
    images = image_loader.load_images(image_directory)
    annotations = AnnotationGenerator.load_annotations(annotation_directory)

    #get the fg mask from the annotation, and convert it to a boolean array
    foreground_masks = ((a.get_players_mask()>0) for a in annotations)
    #foreground_masks = (a.get_players_mask() for a in annotations)
    
    #these are iterators of tuples ((interval_0, iterator of frames in interval), (interval_1, ...), ...)
    indexed_cameras = CameraSamplesInterpolator.load_cameras(camera_directory)
    indexed_player_data_objects = PlayersGenerator.load_player_data_objects(players_directory)
    indexed_team_labels = TeamLabelsGenerator.load_team_labels(team_labels_directory)
    
    #we now convert the image and fg mask iterators to the form (interval, list of frames in interval) as well
    indexed_cameras, iter_copy_0, iter_copy_1, iter_copy_2 = itertools.tee(indexed_cameras, 4)
    intervals   = (interval for interval,Hs in iter_copy_0)
    intervals_1 = (interval for interval,Hs in iter_copy_1)
    intervals_2 = (interval for interval,Hs in iter_copy_2)

    #all of the following iterators have the form (I0, I1, ...) where Ik is an iterator over the frames in the k-th interval
    images = iterator_operations.extract_intervals(images, intervals_1)
    foreground_masks = iterator_operations.extract_intervals(foreground_masks, intervals_2)
    cameras = (Cs_in_interval for interval,Cs_in_interval in indexed_cameras)
    player_data_objects = (pdos_in_interval for interval,pdos_in_interval in indexed_player_data_objects)
    team_labels = (tls_in_interval for interval,tls_in_interval in indexed_team_labels)
                   
    #pdos_in_interval is an iterator of the form (pdos in frame 0, pdos in frame 1,...)
    #pdos_in_frame is a list of PlayerData objects
    player_candidates = (([PlayerCandidate(pdo.contour, pdo.position, tl) for pdo,tl in zip(pdos_in_frame, tls_in_frame)]
                          for pdos_in_frame, tls_in_frame in itertools.izip(pdos_in_interval, tls_in_interval))
                         for pdos_in_interval, tls_in_interval in itertools.izip(player_data_objects, team_labels))

    #build an iterator of tuples (interval, iterator of FrameData objects in this interval)
    frame_data_iterator = ((interval, (FrameData(img, fg, camera, pc)
                                       for img,fg,camera,pc in itertools.izip(I_img, I_fg, I_camera, I_pc)))
                           for interval,I_img,I_fg,I_camera,I_pc in itertools.izip(intervals, images, foreground_masks,
                                                                                   cameras, player_candidates))

    return frame_data_iterator

def test_player_tracker(image_directory, annotation_directory, camera_directory, players_directory, team_labels_directory):
                        
    #this is an iterator of tuples (interval, list of FrameData objects in this interval)
    frame_data_iterator = load_frame_data_iterator(image_directory, annotation_directory, camera_directory,
                                                   players_directory, team_labels_directory)

    #this is a SoccerFieldGeometry object
    soccer_field, absolute_image = load_absolute_soccer_field()
    
    #use only the first interval
    interval, frame_data_in_interval = next(frame_data_iterator)
    
    print 'initializing player tracker with frames in the interval: ', interval

    #build the player tracker visualizer
    player_tracker_visualizer = PlayerTrackerVisualizer(DEBUGGING_DIRECTORY, absolute_image)

    #and the player tracker
    player_tracker = PlayerTracker(frame_data_in_interval, soccer_field, player_tracker_visualizer)

    
    #track players over time
    print 'tracking players in interval (%d,%d)...' % interval
    player_tracker.track_players()

    #save the player data that we recorded
    save_player_data(player_tracker)
    
    #generate the visualization
    #generate_player_tracker_visualization(VISUALIZATION_DIRECTORY, player_tracker, absolute_image)

if __name__ == '__main__':
    test_player_tracker(IMAGE_DIRECTORY, ANNOTATION_DIRECTORY, CAMERA_DIRECTORY, PLAYERS_DIRECTORY, TEAM_LABELS_DIRECTORY)
                        
