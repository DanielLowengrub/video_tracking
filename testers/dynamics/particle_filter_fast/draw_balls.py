import os
import numpy as np
import cv2
import itertools
import pickle

from ....source.preprocessing import image_loader
from ....source.preprocessing.camera_samples_interpolator.camera_samples_interpolator import CameraSamplesInterpolator
from ....source.dynamics.particle_filter_fast.ball_tracker_controller import BallTrackerController
from ....source.auxillary.soccer_field_geometry import SoccerFieldGeometry
from ....source.auxillary import planar_geometry, iterator_operations, np_operations
from ...auxillary import primitive_drawing

"""
This script is used to draw the players that have been tracked by a player tracker.
It draws them both on the absolute image and on the camera image.
"""

# OUTPUT_PARENT_DIRECTORY =        '/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts'
# IMAGE_PARENT_DIRECTORY =         '/Users/daniel/Dropbox/soccer/daniel_refactored/test_data'
# INPUT_DATA_NAME = 'video7'

OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
IMAGE_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/images'
INPUT_DATA_NAME = 'images-8_15-9_31'

# OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'
# IMAGE_PARENT_DIRECTORY = '/home/daniel/soccer/images'
# INPUT_DATA_NAME = 'images-8_15-9_31'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing', 'data', INPUT_DATA_NAME)
POSTPROCESSING_VISUALIZATION_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'postprocessing', 'visualization',
                                                      INPUT_DATA_NAME)
POSTPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'postprocessing', 'data', INPUT_DATA_NAME)

# TRACKED_BALLS_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'ball_tracker', 'data', INPUT_DATA_NAME)
# BALLS_VISUALIZATION_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'ball_tracker', 'visualization', INPUT_DATA_NAME)
# TRACKED_BALLS_DIRECTORY = os.path.join(POSTPROCESSING_DIRECTORY, 'no_penalty_spots')
# BALLS_VISUALIZATION_DIRECTORY = os.path.join(POSTPROCESSING_VISUALIZATION_DIRECTORY, 'no_penalty_spots')
# TRACKED_BALLS_DIRECTORY = os.path.join(POSTPROCESSING_DIRECTORY, 'cropped_ball_trajectories')
# BALLS_VISUALIZATION_DIRECTORY = os.path.join(POSTPROCESSING_VISUALIZATION_DIRECTORY, 'cropped_ball_trajectories')
# TRACKED_BALLS_DIRECTORY = os.path.join(POSTPROCESSING_DIRECTORY, 'no_ball_trajectory_overlaps')
# BALLS_VISUALIZATION_DIRECTORY = os.path.join(POSTPROCESSING_VISUALIZATION_DIRECTORY, 'no_ball_trajectory_overlaps')
TRACKED_BALLS_DIRECTORY = os.path.join(POSTPROCESSING_DIRECTORY, 'extracted_airborne_trajectories', 'tracked_balls')
BALLS_VISUALIZATION_DIRECTORY = os.path.join(POSTPROCESSING_VISUALIZATION_DIRECTORY,
                                             'extracted_airborne_trajectories', 'tracked_balls')

IMAGE_DIRECTORY = os.path.join(IMAGE_PARENT_DIRECTORY, INPUT_DATA_NAME)
CAMERA_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'camera')

INTERVAL_DIRECTORY = 'images_%d_%d'
OUTPUT_IMAGE_BASENAME = 'image_%04d.png'

START = 0
MIDDLE = 1
END = 2

BALL_SIZE = {START:5, MIDDLE:2, END:8}
BALL_COLOR = {START:(0,255,0), MIDDLE:(255,0,0), END:(0,0,255)}
BALL_THICKNESS = {START:2, MIDDLE:-1, END:2}

def load_absolute_soccer_field():
    '''
    Return a tuple (soccer field, absolute image)
    '''
    ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)
    SCALE = 6
    FIELD_WIDTH = absolute_image.shape[1]-1
    FIELD_LENGTH = absolute_image.shape[0]-1
    GRID_WIDTH = 20
    GRID_LENGTH = 14
    soccer_field = SoccerFieldGeometry(SCALE, FIELD_WIDTH, FIELD_LENGTH, GRID_WIDTH, GRID_LENGTH)

    return absolute_image

def draw_balls_in_frame(frame_index, absolute_image, image, camera, tracked_balls):
    '''
    image - an image
    absolute_image - an image
    tracked_balls - a TrackedBalls object
    '''
    absolute_image = absolute_image.copy()

    #iterate over balls that we have tracked in this frame
    #start_positions is a tuple (abs positions, image positions) same for middle_positions and end_positions
    start_positions, middle_positions, end_positions = tracked_balls.get_ball_positions_in_frame(frame_index, True)
    # print 'start: ', start_positions
    # print 'middle: ', middle_positions
    # print 'zip middle: ', zip(*middle_positions)
    # print 'end: ', end_positions
    #merge the three tupes of positions into a single list, and record the types of each position
    position_tuples = ([(START, ap, ip) for ap,ip in zip(*start_positions)] +
                       [(MIDDLE, ap, ip) for ap,ip in zip(*middle_positions)] +
                       [(END, ap, ip) for ap,ip in zip(*end_positions)])
    
    for position_type, absolute_position, image_position in position_tuples:
        color = BALL_COLOR[position_type]
        thickness = BALL_THICKNESS[position_type]
        size = BALL_SIZE[position_type]
        #draw the positions on the image and absolute image
        primitive_drawing.draw_point(absolute_image, absolute_position[:2], color, size, thickness)
        primitive_drawing.draw_point(image, image_position, color, size, thickness)

    #stack the image ontop of the absolute image
    output = np_operations.stack_images([image, absolute_image])

    return output


def draw_balls_in_interval(absolute_image, images, cameras, tracked_balls, interval_directory):
    '''
    absolute_image - an image
    images - an iterator of images
    cameras - an iterator of cameras
    tracked_balls - a TrackedBalls object
    interval_directory - a directory name

    draw the ball in each frame and save the output to interval_directory/image_i
    '''

    for frame_index, (image, camera) in enumerate(itertools.izip(images, cameras)):
        balls_in_frame_image = draw_balls_in_frame(frame_index, absolute_image, image, camera, tracked_balls)
        
        frame_file = os.path.join(interval_directory, OUTPUT_IMAGE_BASENAME % frame_index)
        print '   writing frame %d to %s' % (frame_index, frame_file)
        cv2.imwrite(frame_file, balls_in_frame_image)

    return

def draw_balls(image_directory, camera_directory, tracked_balls_directory, output_directory):
    '''
    image_directory - a directory with the images of each frame.
    camera_directory - a directory that contains the cameras in each frame.
    tracked_balls_directory - a directory that can be loaded with the BallTrackerController
    output_directory - the directory where we save the player images
    '''

    images = image_loader.load_images(image_directory)
    absolute_image = load_absolute_soccer_field()

    #an iterator of elements of the form (interval, iterator over cameras of the frames in the interval)
    indexed_cameras = CameraSamplesInterpolator.load_cameras(camera_directory)
    indexed_tracked_balls = BallTrackerController.load_tracked_balls(tracked_balls_directory)
    
    #build an independent iterator for the intervals
    intervals, cameras = iterator_operations.iunzip(indexed_cameras)
    intervals, intervals_copy = itertools.tee(intervals, 2)

    #regroup the images by interval
    images = iterator_operations.extract_intervals(images, intervals_copy)

    #stip the intervals off of the tracked balls
    tracked_balls = (tb_in_interval for interval,tb_in_interval in indexed_tracked_balls)
    
    data_in_intervals = itertools.izip(images, cameras, tracked_balls)
    indexed_data_in_intervals = itertools.izip(intervals, data_in_intervals)
    
    for (ik,jk), (images_in_I, cameras_in_I, tracked_balls_in_I)  in indexed_data_in_intervals:
        interval_directory = os.path.join(output_directory, INTERVAL_DIRECTORY % (ik,jk))
        os.mkdir(interval_directory)
        print 'saving interval (%d,%d) in: %s' % (ik,jk,interval_directory)

        draw_balls_in_interval(absolute_image, images_in_I, cameras_in_I, tracked_balls_in_I, interval_directory)

    return

if __name__ == '__main__':
    draw_balls(IMAGE_DIRECTORY, CAMERA_DIRECTORY, TRACKED_BALLS_DIRECTORY, BALLS_VISUALIZATION_DIRECTORY)
