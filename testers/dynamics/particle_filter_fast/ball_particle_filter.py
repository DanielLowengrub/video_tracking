import numpy as np
import cv2
import time
import os
import pickle
import matplotlib.pyplot as plt
from ....source.auxillary.soccer_field_geometry import SoccerFieldGeometry
from ....source.dynamics.particle_filter_fast.local_ball_data import LocalBallData
from ....source.dynamics.particle_filter_fast.ball_particle_filter import BallParticleFilter
from ....source.image_processing.edge_ball_evaluator import EdgeBallEvaluator
from ....source.image_processing.image_annotator.soccer_annotation import SoccerAnnotation
from ....source.auxillary.camera_matrix import CameraMatrix
from ....source.dynamics.particle_filter_fast.frame_data import FrameData
from ....source.dynamics.particle_filter_fast.ball_tracker_controller import BallTrackerController
from ...auxillary import primitive_drawing

INTERVAL = (0,274)
PREPROCESSING_NAME = 'video7'

IMAGE_BASENAME = 'test_data/video7/image_%d.png'
ANNOTATION_BASENAME = os.path.join('output/scripts/preprocessing/data',PREPROCESSING_NAME,'annotation/annotation_%d.npy')
CAMERA_DIR = os.path.join('output/scripts/preprocessing/data',PREPROCESSING_NAME,'camera/camera_%d_%d' % INTERVAL)
CAMERA_BASENAME = os.path.join(CAMERA_DIR, 'camera_%d.npy')

BALLS_FILE_DIR = os.path.join('output/scripts/preprocessing/data', PREPROCESSING_NAME,'balls/balls_%d_%d' % INTERVAL)
BALL_TRAJECTORIES_DIR = os.path.join('output/scripts/preprocessing/data', PREPROCESSING_NAME,
                                     'ball_trajectories/ball_trajectories_%d_%d' % INTERVAL)
BALL_TRAJECTORY_FILE = os.path.join(BALL_TRAJECTORIES_DIR, 'ball_trajectories_%d.pckl')
TRACKED_PLAYERS_FILE = os.path.join('output/scripts/player_tracker/data', PREPROCESSING_NAME,
                                    'tracked_players_%d_%d.pckl' % INTERVAL)

POSITION_STDEV = np.array([3,3], np.float64)
PLAYER_HEIGHT = 12.0

#This is for video7:
FPS = 25

# FRAME_INDICES = range(9,43)
# INITIAL_BALL_TRAJECTORY_INDEX = 0
# INITIAL_HEIGHT = 4.3/6.0
# ON_GROUND = True
# REVERSED = False

FRAME_INDICES = range(62,70)
INITIAL_BALL_TRAJECTORY_INDEX = 0
INITIAL_HEIGHT = 4.3/6.0
ON_GROUND = True
REVERSED = False

# FRAME_INDICES = range(248,236,-1)
# INITIAL_BALL_TRAJECTORY_INDEX = 0
# INITIAL_HEIGHT = 6.0
# ON_GROUND = False
# REVERSED = True

# FRAME_INDICES = range(127,117,-1)
# INITIAL_BALL_TRAJECTORY_INDEX = 0
# INITIAL_HEIGHT = 12.0 # 5.0
# ON_GROUND = False         #false
# REVERSED = True

# FRAME_INDICES = range(247,260)
# INITIAL_BALL_TRAJECTORY_INDEX = 0
# INITIAL_HEIGHT = 8.0
# ON_GROUND = False
# REVERSED = False

# FRAME_INDICES = range(7,40)
# INITIAL_BALL_TRAJECTORY_INDEX = 0
# INITIAL_HEIGHT = 4.3/6
# ON_GROUND = True

# FRAME_INDICES = range(248,237,-1)
# INITIAL_BALL_TRAJECTORY_INDEX = 
# INITIAL_HEIGHT = 6.0
# ON_GROUND = False
# REVERSED = True

# FRAME_INDICES = range(184,204)
# INITIAL_BALL_TRAJECTORY_INDEX = 0
# INITIAL_HEIGHT = 4.3/6
# ON_GROUND = True
# REVERSED = False

#This is for images_0_269
# #FRAME_INDICES = [i for i in range(97,136) if (i%6) != 3]

# BALLS_FILE = os.path.join(BALLS_FILE_DIR, 'balls_%d.pckl')
# BALL_CANDIDATE_INDICES = [1,0]
# INITIAL_BALL_HEIGHT = 5.0


# FRAME_INDICES = [237,238,239,240]
# BALLS_FILE = os.path.join(BALLS_FILE_DIR, 'balls_%d.pckl')
# BALL_CANDIDATE_INDICES = [2,1]
# INITIAL_BALL_HEIGHT = 2.0

# FRAME_INDICES = list(reversed([i for i in range(226,239) if (i % 6) != 3]))
# BALLS_FILE = os.path.join(BALLS_FILE_DIR, 'balls_%d.pckl')
# BALL_CANDIDATE_INDICES = [1,2]
# INITIAL_BALL_HEIGHT = 8.0

# FRAME_INDICES = [i for i in range(2,20) if (i % 6) != 3]
# BALLS_FILE = os.path.join(BALLS_FILE_DIR, 'balls_%d.pckl')
# BALL_CANDIDATE_INDICES = [0,0]
# INITIAL_BALL_HEIGHT = 15.0

# FRAME_INDICES = [i for i in range(31,45) if (i % 6) != 3]
# BALLS_FILE = os.path.join(BALLS_FILE_DIR, 'balls_%d.pckl')
# BALL_CANDIDATE_INDICES = [0,0]
# INITIAL_BALL_HEIGHT = 1.0

# FRAME_INDICES = [i for i in range(46,80) if (i % 6) != 3]
# BALLS_FILE = os.path.join(BALLS_FILE_DIR, 'balls_%d.pckl')
# BALL_CANDIDATE_INDICES = [0,0]
# INITIAL_BALL_HEIGHT = 1.0

# FRAME_INDICES = [i for i in range(82,100) if (i % 6) != 3]
# BALLS_FILE = os.path.join(BALLS_FILE_DIR, 'balls_%d.pckl')
# BALL_CANDIDATE_INDICES = [0,0]
# INITIAL_BALL_HEIGHT = 1.0

# FRAME_INDICES = [i for i in range(95,120) if (i % 6) != 3]
# BALLS_FILE = os.path.join(BALLS_FILE_DIR, 'balls_%d.pckl')
# BALL_CANDIDATE_INDICES = [0,0]
# INITIAL_BALL_HEIGHT = 1.0

#FPS = 30
###################
def load_absolute_soccer_field():
    '''
    Return a tuple (absolute shapes, absolute grid points)
    '''
    ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)
    SCALE = 6
    FIELD_WIDTH = absolute_image.shape[1]-1
    FIELD_LENGTH = absolute_image.shape[0]-1
    GRID_WIDTH = 20
    GRID_LENGTH = 14
    soccer_field = SoccerFieldGeometry(SCALE, FIELD_WIDTH, FIELD_LENGTH, GRID_WIDTH, GRID_LENGTH)

    return absolute_image, soccer_field

def load_frame_datas_and_sideline_masks():
    images = [cv2.imread(IMAGE_BASENAME % i, 1) for i in FRAME_INDICES]
    soccer_annotations = [SoccerAnnotation(np.load(ANNOTATION_BASENAME % i)) for i in FRAME_INDICES]
    foreground_masks = [sa.get_players_mask() > 0 for sa in soccer_annotations]
    sideline_masks = [sa.get_sidelines_mask() > 0 for sa in soccer_annotations]
    camera_matrices = [CameraMatrix(np.load(CAMERA_BASENAME % i)) for i in FRAME_INDICES]

    frame_datas = [FrameData(img, fm, cam, [], []) for img,fm,cam in zip(images, foreground_masks, camera_matrices)]

    return frame_datas, sideline_masks

def load_tracked_players():
    f = open(TRACKED_PLAYERS_FILE, 'rb')
    tracked_players = pickle.load(f)
    f.close()
    return tracked_players
    
def load_position(frame_index, ball_candidate_index, height):
    f = open(BALLS_FILE % frame_index, 'rb')
    ball_candidates = pickle.load(f)
    ball_candidate = ball_candidates[ball_candidate_index]
    f.close()
    
    print 'ball candidate: '
    print ball_candidate
    
    low_position = ball_candidate.low_position
    low_to_high =  ball_candidate.high_position - low_position
    low_to_high /= np.linalg.norm(low_to_high)
    alpha = height / low_to_high[2]

    position = low_position + alpha*low_to_high
    
    return position

def load_initial_state():
    initial_position = load_position(FRAME_INDICES[0], BALL_CANDIDATE_INDICES[0], INITIAL_BALL_HEIGHT)
    next_position    = load_position(FRAME_INDICES[1], BALL_CANDIDATE_INDICES[1], INITIAL_BALL_HEIGHT)

    print 'initial position:'
    print initial_position
    print 'next position:'
    print next_position

    initial_velocity = (next_position - initial_position) * FPS

    print 'initial velocity: ', initial_velocity
    
    return initial_position, initial_velocity

def load_initial_state_from_trajectory_candidate():
    print 'loading file: ', BALL_TRAJECTORY_FILE % FRAME_INDICES[0]
    ball_trajectories = np.load(BALL_TRAJECTORY_FILE % FRAME_INDICES[0])
    print 'got: ', ball_trajectories
    
    ball_trajectory_candidate = ball_trajectories[INITIAL_BALL_TRAJECTORY_INDEX]

    initial_position, initial_velocity = ball_trajectory_candidate.compute_state(INITIAL_HEIGHT, FPS)

    if REVERSED:
        initial_velocity *= -1
        
    return initial_position, initial_velocity

def display_ball_filter(frame_index, frame_data, ball_filter, local_ball_data):
    #image_copy = frame_data.get_image().copy()
    image_copy = cv2.cvtColor(frame_data.get_edge_mask().astype(np.float32), cv2.COLOR_GRAY2BGR)
    world_positions = ball_filter._position_slice()
    image_positions = frame_data.get_camera_matrix().project_points_to_image(world_positions).astype(np.int32)
    #image_copy[image_positions[:,1],image_positions[:,0]] = np.array([255,0,0], np.uint8)

    foreground_mask = frame_data.get_foreground_mask()
    
    expected_position = ball_filter.get_expected_position()
    expected_image_position = frame_data.get_camera_matrix().project_points_to_image(expected_position.reshape((-1,3)))[0]
    expected_image_position = np.int32(expected_image_position)
    
    expected_evaluation = ball_filter.evaluate_expected_position()
    in_foreground = foreground_mask[expected_image_position[1], expected_image_position[0]]
    
    print '----------------------------'
    print 'frame %d:' % frame_index
    print 'expected evaluation: ', expected_evaluation
    print 'in foreground:       ', in_foreground
    print 'position mean: ', expected_position
    print 'position variance: ', ball_filter.get_position_variance()
    print 'velocity mean: ', ball_filter.get_expected_velocity()
    print 'velocity norm: ', np.linalg.norm(ball_filter.get_expected_velocity())
    print 'velocity variance: ', ball_filter.get_velocity_variance()

    print 'inside=1 and outside=2:'
    inside_mask, outside_mask = local_ball_data._radius_sq_masks
    m = np.int32(inside_mask)
    m[outside_mask] = 2
    print m

    camera_matrix = frame_data.get_camera_matrix()
    expected_image_position = camera_matrix.project_points_to_image(expected_position.reshape((1,3)))[0]
    
    cv2.circle(image_copy, tuple(expected_image_position.astype(np.int32).tolist()), 5, (0,0,255), 2)
    cv2.imshow('image with particle positions', image_copy)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    return

def get_player_sideline_mask(frame_index, frame_data, tracked_players, sideline_masks):
    image = frame_data.get_image()
    camera_matrix = frame_data.get_camera_matrix()
    player_mask = tracked_players.get_player_mask_in_frame(FRAME_INDICES[frame_index], image, camera_matrix,
                                                           POSITION_STDEV, PLAYER_HEIGHT)
    sideline_mask = sideline_masks[frame_index]

    return np.logical_or(player_mask, sideline_mask)

def test_particle_filter(velocity_stdev, num_particles):
    absolute_image, soccer_field = load_absolute_soccer_field()
    frame_datas, sideline_masks = load_frame_datas_and_sideline_masks()
    tracked_players = load_tracked_players()

    #initial_position, initial_velocity = load_initial_state()
    initial_position, initial_velocity = load_initial_state_from_trajectory_candidate()
    ball_evaluator = EdgeBallEvaluator()

    print 'building ball filter with:'
    print 'initial position: ', initial_position
    print 'initial velocity: ', initial_velocity

    current_frame = 0
    current_frame_data = frame_datas[0]
    player_sideline_mask = get_player_sideline_mask(current_frame, current_frame_data, tracked_players, sideline_masks)
    edge_mask = current_frame_data.get_edge_mask()
    current_frame_data._edge_mask = np.logical_and(edge_mask, np.logical_not(player_sideline_mask))
                  
    if ON_GROUND:
        field_position = initial_position[:2]
        field_velocity = initial_velocity[:2]
        ball_filter = BallParticleFilter.from_initial_state_on_ground(field_position, field_velocity, velocity_stdev,
                                                                      num_particles, ball_evaluator,
                                                                      soccer_field, current_frame_data)

    else:
        ball_filter = BallParticleFilter.from_initial_state(initial_position, initial_velocity, velocity_stdev,
                                                            num_particles, ball_evaluator,
                                                            soccer_field, current_frame_data)

    print 'displaying initial particle filter...'
    display_ball_filter(current_frame, current_frame_data, ball_filter, ball_filter._local_ball_data)

    particle_evaluations = []
    expected_evaluations = []
    variances = []
    positions = []
    
    while current_frame+1 < len(FRAME_INDICES):
        current_frame += 1
        print 'updating to frame %d (frame %d in images)...' % (current_frame, FRAME_INDICES[current_frame])
        current_frame_data.update_from_frame_data(frame_datas[current_frame])
        player_sideline_mask = get_player_sideline_mask(current_frame, current_frame_data, tracked_players, sideline_masks)
        edge_mask = current_frame_data.get_edge_mask()
        current_frame_data._edge_mask = np.logical_and(edge_mask, np.logical_not(player_sideline_mask))
    
        ball_filter.update()
        local_ball_data = ball_filter._local_ball_data
        positions.append(ball_filter.get_expected_position())
        variances.append(np.linalg.norm(ball_filter.get_position_variance()))
        #if FRAME_INDICES[current_frame] not in (229,):
        particle_evaluations.append(-ball_filter._local_ball_data.compute_log_probabilities())

        if FRAME_INDICES[current_frame] not in (241,):
            ball_filter.compute_particle_probabilities()
            ball_filter.compute_particle_weights()
            ball_filter.resample()

        else:
            print 'not updating probability for frame %d.' % current_frame

        expected_evaluations.append(ball_filter.evaluate_expected_position())

            
        print '   finished updating. displaying current filter state.'
        display_ball_filter(current_frame, current_frame_data, ball_filter, local_ball_data)

    positions = np.vstack(positions)
    variances = np.vstack(variances)
    
    primitive_drawing.draw_points(absolute_image, positions[:,:2], (255,0,0), size=3)
    cv2.imshow('expected positions', absolute_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    
    #particle_evaluations = np.vstack(particle_evaluations)
    #best_evaluations = particle_evaluations.min(axis=1)
    #print 'average particle evaluation: ', particle_evaluations.mean()
    #print 'average best evaluation in frame:     ', best_evaluations.mean()

    print 'expected evaluations:'
    print expected_evaluations
    plt.bar(range(len(expected_evaluations)), expected_evaluations)
    plt.show()

    plt.bar(range(len(variances)), variances)
    plt.show()

    return

if __name__ == '__main__':
    velocity_stdev = 10
    num_particles = 2000

    test_particle_filter(velocity_stdev, num_particles)
