import os
import pickle
import cv2
import numpy as np
from ....source.dynamics.particle_filter_fast.ball_tracker import BallTracker
from ....source.dynamics.particle_filter_fast.frame_data import FrameData
from ....source.auxillary.soccer_field_geometry import SoccerFieldGeometry

"""
This is used to track balls in a sequence of FrameData objects.
For each iterator if frame data objects that we load from the preprocessing data, track the balls in that frame sequence and
store them as a dictionary of BallTrajectory objects. If a trajectory was initialized with the ball trajectory candidate btc,
the key for the trajectory in the dictionary is the id of btc.
"""
IMAGE_DIRECTORY = 'test_data/video7'
PREPROCESSING_NAME = 'video7'

DATA_DIRECTORY = 'output/scripts/preprocessing/data'

ANNOTATION_DIRECTORY = os.path.join(DATA_DIRECTORY, PREPROCESSING_NAME, 'annotation')
PLAYERS_DIRECTORY = os.path.join(DATA_DIRECTORY, PREPROCESSING_NAME, 'players')
CAMERA_DIRECTORY = os.path.join(DATA_DIRECTORY, PREPROCESSING_NAME, 'camera')
TEAM_LABELS_DIRECTORY = os.path.join(DATA_DIRECTORY, PREPROCESSING_NAME, 'team_labels')
BALLS_DIRECTORY = os.path.join(DATA_DIRECTORY, PREPROCESSING_NAME, 'ball_trajectories')

OUTPUT_FILE = os.path.join('output/tester/particle_filter_fast/ball_tracker', PREPROCESSING_NAME,
                           'data/ball_trajectories.pckl')


FPS = 25
NUM_PARTICLES = 1500
INITIAL_VELOCITY_STDEV = 10
INITIAL_HEIGHTS_YARDS = (0.1, 1, 2)
MAX_BAD_FRAMES = 3
MAX_POSITION_VARIANCE = 20
MAX_POSITION_VARIANCE_GROUND = 5
GROUND_THRESHOLD = 1
EVALUATION_THRESHOLD = -0.5

def load_absolute_soccer_field():
    '''
    Return a tuple (soccer field, absolute image)
    '''
    ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)
    SCALE = 6
    FIELD_WIDTH = absolute_image.shape[1]-1
    FIELD_LENGTH = absolute_image.shape[0]-1
    GRID_WIDTH = 20
    GRID_LENGTH = 14
    soccer_field = SoccerFieldGeometry(SCALE, FIELD_WIDTH, FIELD_LENGTH, GRID_WIDTH, GRID_LENGTH)

    return soccer_field, absolute_image

def run_ball_tracker(image_directory, annotation_directory, camera_directory, players_directory, team_labels_directory,
                     balls_directory, output_file):
    '''
    image_directory - a directory with files image_0.png,...,image_N.png
    annotation_directory - a directory with files annotation_0.png,...,annotation_N.png
    camera_directory - a directory with subdirs cameras_ik_jk/. each subdir has files: camera_0.npy,...,camera_{jk-ik}.npy
    players_directory - a directory with subdirs players_ik_jk/. each subdir has directories: 
                       players_0/,...,players_{jk-ik}/
    team_labels_directory - a directory with subdirs team_labels_ik_jk/. each subdir has files 
                            team_labels_0.npy,...,team_labels_{jk-ik}.npy
    balls_directory - a directory with subdirs ball_trajectories_ik_jk/. 
                      each subdir has files: ball_trajectories_0.pckl,...,ball_trajectories_{jk-ik}.pckl

    Track the balls through the frames and save the resulting trajectories in output_file.
    '''
    #this is a SoccerFieldGeometry object
    soccer_field, absolute_image = load_absolute_soccer_field()

    #this is an iterator of tuples of the form (interval, iterator of FrameData objects in interval)
    indexed_frame_datas = FrameData.load_frame_datas(image_directory, annotation_directory, camera_directory,
                                                     players_directory, team_labels_directory, balls_directory)

    #for now, only use the first interval
    interval, frame_datas = next(indexed_frame_datas)

    #build the ball tracker
    print 'tracking the balls in interval: ', interval
    ball_tracker = BallTracker(soccer_field, frame_datas, FPS, NUM_PARTICLES, INITIAL_VELOCITY_STDEV, INITIAL_HEIGHTS_YARDS,
                               MAX_BAD_FRAMES, MAX_POSITION_VARIANCE, MAX_POSITION_VARIANCE_GROUND,
                               GROUND_THRESHOLD, EVALUATION_THRESHOLD)

    #track the balls
    ball_tracker.track_balls()

    #save the ball trajectories
    ball_trajectories = ball_tracker.get_ball_trajectory_dict()

    print 'saving the ball trajectories in file: %s' % output_file
    f = open(output_file, 'wb')
    pickle.dump(ball_trajectories, f)
    f.close()

    return


if __name__ == '__main__':
    run_ball_tracker(IMAGE_DIRECTORY, ANNOTATION_DIRECTORY, CAMERA_DIRECTORY, PLAYERS_DIRECTORY, TEAM_LABELS_DIRECTORY,
                     BALLS_DIRECTORY, OUTPUT_FILE)
    
