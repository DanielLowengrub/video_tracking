import numpy as np
import cv2
import os
import time
import cProfile
from ....source.auxillary.soccer_field_geometry import SoccerFieldGeometry
from ....source.dynamics.particle_filter_fast.local_player_data import LocalPlayerData
from ....source.image_processing.image_annotator.soccer_annotation import SoccerAnnotation
from ....source.auxillary.camera_matrix import CameraMatrix
from ....source.dynamics.particle_filter_fast.frame_data import FrameData
from ....source.dynamics.particle_filter_fast.padded_frame_data import PaddedFrameData
from ....source.dynamics.particle_filter_fast.local_data_evaluator import LocalDataEvaluator
from ....source.dynamics.particle_filter_fast.player_particle_filter import PlayerParticleFilter
from ...auxillary import primitive_drawing
from ....source.auxillary import planar_geometry

FRAME = 117
INTERVAL = (0,274)
PREPROCESSING_NAME = 'video7'

IMAGE_FILE = 'test_data/video7/image_117.png'
ANNOTATION_FILE = os.path.join('output/scripts/preprocessing/data', PREPROCESSING_NAME,
                               'annotation/annotation_%d.npy' % FRAME)
CAMERA_FILE = os.path.join('output/scripts/preprocessing/data', PREPROCESSING_NAME,
                           'camera/camera_%d_%d' % INTERVAL, 'camera_%d.npy' % FRAME)
PLAYER_POSITIONS_FILE = os.path.join('output/scripts/preprocessing/data', PREPROCESSING_NAME,
                                     'players/players_%d_%d' % INTERVAL, 'players_%d/positions.npy' % FRAME)

def load_absolute_soccer_field():
    '''
    Return a tuple (absolute shapes, absolute grid points)
    '''
    ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)
    SCALE = 6
    FIELD_WIDTH = absolute_image.shape[1]-1
    FIELD_LENGTH = absolute_image.shape[0]-1
    GRID_WIDTH = 20
    GRID_LENGTH = 14
    soccer_field = SoccerFieldGeometry(SCALE, FIELD_WIDTH, FIELD_LENGTH, GRID_WIDTH, GRID_LENGTH)

    return soccer_field

def single_player_test(num_players, num_particles, num_frames):
    '''
    Build a particle filter and run it for a couple of frames.
    '''

    image = cv2.imread(IMAGE_FILE, 1)
    soccer_annotation = SoccerAnnotation(np.load(ANNOTATION_FILE))
    foreground_mask = soccer_annotation.get_players_mask() > 0
    camera_matrix = CameraMatrix(np.load(CAMERA_FILE))
    positions = np.load(PLAYER_POSITIONS_FILE)

    frame_data = FrameData(image, foreground_mask, camera_matrix, [], [])
    padding = 50
    frame_data = PaddedFrameData.from_frame_data(frame_data, padding)
    
    soccer_field = load_absolute_soccer_field()
    field_rectangle = soccer_field.get_field_rectangle()

    image = frame_data.get_image()
    foreground_mask = frame_data.get_foreground_mask()
    camera_matrix = frame_data.get_camera_matrix()
    
    cv2.imshow('image', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    cv2.imshow('fg mask', np.float32(foreground_mask))
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    local_data_evaluator = LocalDataEvaluator()
    N = len(positions)
    particle_filters = [PlayerParticleFilter.from_initial_position(positions[i % N], num_particles, local_data_evaluator,
                                                                   field_rectangle, frame_data)
                        for i in range(num_players)]

    
    particle_filters = [PlayerParticleFilter.from_initial_position(positions[4], num_particles, local_data_evaluator,
                                                                   field_rectangle, frame_data)]
    
    print 'running the particle filter...'
    start = time.time()
    
    for i in range(1,num_frames):
        print '\n\n---------------------------\nupdating to frame %d.' % i

        for particle_filter in particle_filters:
            particle_filter.update()

            #print 'player positions:'
            #print particle_filter._position_slice()
            # print 'player velocities:'
            # print particle_filter._velocity_slice()
            # print 'player accelerations:'
            # print particle_filter._acceleration_slice()
            
            # print 'particle type: ', particle_filter._particles.dtype
            # print 'average position: ', particle_filter.get_expected_position()
            # print 'min coords: ', particle_filter._position_slice().min(axis=0)
            # print 'max coords: ', particle_filter._position_slice().max(axis=0)
            # print 'min velocity coords: ', particle_filter._velocity_slice().min(axis=0)
            # print 'max velocity coords: ', particle_filter._velocity_slice().max(axis=0)
            
            print 'local data summary:'
            lpd = particle_filter._local_player_data
            print '   number of players:    ', lpd.number_of_player_positions()
            print '   number of particles:  ', lpd.number_of_particles()
            print '   number of rectangles: ', lpd._rectangles.get_number_of_rectangles()
            
            #print 'computing probabilities and weights...'
            particle_filter.compute_particle_probabilities()
            particle_filter.compute_particle_weights()
            
            # print 'player weights:'
            # print particle_filter._particle_weights
            #print 'expected position: ', particle_filter.get_expected_position()
            
            particle_filter.resample()
            
            print 'after resampling:'
            print 'position variance: ', particle_filter.get_position_variance()
            # print 'player positions:'
            # print particle_filter._position_slice()
            # print 'player velocities:'
            # print particle_filter._velocity_slice()
            # print 'player accelerations:'
            # print particle_filter._acceleration_slice()

            image_copy = image.copy()
            H = camera_matrix.get_homography_matrix()
            points_on_image = planar_geometry.apply_homography_to_points(H, particle_filter._position_slice())
            for p in points_on_image:
                primitive_drawing.draw_point(image, p, (255,0,0))
                
            cv2.imshow('image with points', image_copy)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

    print 'finished in: ', time.time() - start
    
    H = camera_matrix.get_homography_matrix()

    for particle_filter in particle_filters:
        points_on_image = planar_geometry.apply_homography_to_points(H, particle_filter._position_slice())
        for p in points_on_image:
            primitive_drawing.draw_point(image, p, (255,0,0))

    cv2.imshow('image with points', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

        
    return

    
    
if __name__ == '__main__':
    num_players = 1
    num_particles = 300
    num_frames = 50
    
    single_player_test(num_players, num_particles, num_frames)
    #cProfile.run('single_player_test()')
