import numpy as np
import cv2
import time
import os
from ....source.auxillary.soccer_field_geometry import SoccerFieldGeometry
from ....source.dynamics.particle_filter_fast.local_ball_data import LocalBallData
from ....source.image_processing.ball_evaluator import BallEvaluator
from ....source.image_processing.image_annotator.soccer_annotation import SoccerAnnotation
from ....source.auxillary.camera_matrix import CameraMatrix
from ....source.dynamics.particle_filter_fast.frame_data import FrameData

INTERVAL = (0,269)
FRAME_INDEX = 98
PREPROCESSING_NAME = 'images_0_269'

IMAGE_FILE = 'test_data/images_0_269/image_%d.png' % FRAME_INDEX
ANNOTATION_FILE = os.path.join('output/scripts/preprocessing/data', PREPROCESSING_NAME,
                               'annotation/annotation_%d.npy' % FRAME_INDEX)
CAMERA_FILE = os.path.join('output/scripts/preprocessing/data', PREPROCESSING_NAME,
                           'camera/camera_%d_%d/camera_%d.npy' % (INTERVAL + (FRAME_INDEX,)))

BALLS_FILE = os.path.join('output/scripts/preprocessing/data', PREPROCESSING_NAME,
                           'balls/balls_%d_%d/balls_%d.npy' % (INTERVAL + (FRAME_INDEX,)))


def load_absolute_soccer_field():
    '''
    Return a tuple (absolute shapes, absolute grid points)
    '''
    ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)
    SCALE = 6
    FIELD_WIDTH = absolute_image.shape[1]-1
    FIELD_LENGTH = absolute_image.shape[0]-1
    GRID_WIDTH = 20
    GRID_LENGTH = 14
    soccer_field = SoccerFieldGeometry(SCALE, FIELD_WIDTH, FIELD_LENGTH, GRID_WIDTH, GRID_LENGTH)

    return soccer_field

def load_ball_and_frame_data():
    image = cv2.imread(IMAGE_FILE, 1)
    soccer_annotation = SoccerAnnotation(np.load(ANNOTATION_FILE))
    foreground_mask = soccer_annotation.get_players_mask() > 0
    camera_matrix = CameraMatrix(np.load(CAMERA_FILE))
    ball_positions = np.load(BALLS_FILE)
    
    player_candidates = []
    ball_candidates = []
    
    frame_data = FrameData(image, foreground_mask, camera_matrix, player_candidates, ball_candidates)

    return ball_positions, frame_data

def test_build_local_ball_data(num_particles):
    soccer_field = load_absolute_soccer_field()
    ball_positions_in_frame, frame_data = load_ball_and_frame_data()

    ball_position = ball_positions_in_frame[0]
    ball_positions = ball_position + 0.2*np.random.normal(size=(num_particles,3))

    print 'building local ball data with positions:'
    print ball_positions

    image_positions = frame_data.get_camera_matrix().project_points_to_image(ball_positions).astype(np.int32)
    image_copy = frame_data.get_image().copy()
    image_copy[image_positions[:,1], image_positions[:,0]] = np.array([255,0,0], np.uint8)
    cv2.imshow('image with ball positions', image_copy)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    ball_evaluator = BallEvaluator()
    local_ball_data = LocalBallData.from_frame_and_positions(soccer_field, frame_data, ball_evaluator, ball_positions)

    print 'local ball data summary:'

    for i,rsm in enumerate(local_ball_data._radius_sq_masks):
        print '%d-th radius sq map: ' % i
        print rsm

    print 'there are %d rectangles' % len(local_ball_data._ball_rectangles._rectangles)
    print 'rectangles: '
    for r in local_ball_data._ball_rectangles.get_rectangle_objects():
        print r

    print 'particle to rectangle map:'
    print local_ball_data._particle_to_rectangle_map

    log_probabilities = local_ball_data.compute_log_probabilities()

    print 'log probabilities:'
    print log_probabilities
    
if __name__ == '__main__':
    np.set_printoptions(precision=2, linewidth=200)
    num_balls = 300
    test_build_local_ball_data(num_balls)
