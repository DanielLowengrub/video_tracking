import numpy as np
import cv2
from ....source.auxillary.camera_matrix import CameraMatrix
from ....source.dynamics.particle_filter_fast.frame_data import FrameData
from ....source.dynamics.particle_filter_fast.padded_frame_data import PaddedFrameData
from ...auxillary import primitive_drawing

def basic_test():
    image = np.ones((500,500,3), np.uint8) * np.array([0,255,0], np.uint8)
    foreground_mask = np.zeros((500,500), np.bool)
    foreground_mask[100:300, 100:300] = True
    C = np.hstack([np.eye(3, dtype=np.float32), np.array([0,0,1], np.float32)[:,np.newaxis]])
    camera_matrix = CameraMatrix(C)

    frame_data = FrameData(image, foreground_mask, camera_matrix)

    cv2.imshow('the original image: ', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    cv2.imshow('the original fg mask: ', np.float32(foreground_mask))
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    padding = 50
    padded_frame_data = PaddedFrameData.from_frame_data(frame_data, padding)

    padded_image = padded_frame_data.get_image()
    unpadded_rectangle = padded_frame_data.get_unpadded_rectangle()

    print 'the unpadded rectangle is: ', unpadded_rectangle
    primitive_drawing.draw_rectangle(padded_image, unpadded_rectangle, (255,0,0))

    new_C = padded_frame_data.get_camera_matrix()
    center_point = new_C.project_points_to_image(np.array([250,250,0], np.float32)[:,np.newaxis])[0]

    primitive_drawing.draw_point(padded_image, center_point, (0,0,255), size=5)
    
    cv2.imshow('the padded image: ', padded_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    cv2.imshow('the padded fg mask: ', np.float32(padded_frame_data.get_foreground_mask()))
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    cv2.imshow('the padded bg mask: ', np.float32(padded_frame_data.get_background_mask()))
    cv2.waitKey(0)
    cv2.destroyAllWindows()

if __name__ == '__main__':
    basic_test()
