import warnings
import os
import matplotlib.pyplot as plt
import numpy as np
import cv2
from ....source.auxillary import planar_geometry

class PlayerTrackerVisualizer(object):
    '''
    This class is in charge of generating visualizations of the PlayerTracker as it is running.
    '''

    SUMMARY_DIR = 'summary'
    PLAYER_NUMBERS_DIR = 'player_ids'
    IAAP_DIR = 'image_and_absolute_positions'
    PP_DIR = 'player_particles'
    PM_DIR = 'player_masks'
    IMAGE_BASENAME = 'image_%04d.png'

    MF_DIR = 'mean_field'
    MF_FRAME_DIR = 'frame_%d'
    MF_ITER_DIR = 'iteration_%d'
    MF_PLAYER_DIR = 'player_%d'
    MF_PARTICLE_BASENAME = 'particle_%d_neighbors_%s.png'
    
    #Parameters for the "player ids" visualization.
    #This draws the player positions together with labels showing the player ids.
    PLAYER_IDS_LEFT_MARGIN = 50 #the width of the margin that we put to the left of the image. This is the margin where
                                    #the player ids are written.
    PLAYER_IDS_ARROW_COLOR = (255,0,0) #The arrow color in opencv BGR notation
    PLAYER_IDS_POSITION_COLOR = (0,0,255) #the color of the dot we put in the image position of each player
    PLAYER_IDS_LABEL_BASENAME = 'id: %d'

    #Parameters for the "image and absolute positions" visualization
    IAAP_POSITION_COLOR = (255,0,0)

    #Parameters for the "player particles" visualization
    PP_PARTICLE_RADIUS = 30
    PP_PARTICLE_ALPHA =  0.2
    PP_POSITION_COLOR = (255,0,0)

    #Parameters for the "player masks" visualization
    PM_MASK_COLOR = (255,0,0)
    PM_MASK_ALPHA = 0.6

    #Parameters for the mean field visualization
    MF_NUM_PARTICLE_SAMPLES = 20
    MF_NEIGHBOR_COLOR = np.array([255,255,255], np.uint8)
    MF_PLAYER_COLOR = np.array([255,0,0], np.uint8)
    MF_RECTANGLE_COLOR = np.array([0,255,0], np.uint8)
    MF_OBSTRUCTION_COLOR = np.array([0,0,255], np.uint8)
    
    def __init__(self, output_directory, absolute_image):
        '''
        output_directory - a directory name
        absolute_image - a numpy array with shape (n,m,3) and type np.uint8
        '''
        self._output_directory = output_directory
        self._absolute_image = absolute_image
        
    @classmethod
    def _get_axis_on_image(cls, image, margin=(0,0,0,0)):
        '''
        image - numpy array of shape (n,m,3) and type np.uint8
        margin - a tuple of 4 integers: (left, right, bottom, top) specifying how big the margins should be around the image.
        Return a tuple fig,ax where fig is a matplotlib Figure and ax is an Axes object that has the image in the background.
        '''
        # print 'getting axis for image with shape: ', image.shape
        # print 'margin: ', margin
        
        fig = plt.figure(figsize=(13.0,8.0))
        ax = fig.add_subplot(111)

        x_axis_length = image.shape[1] + margin[0] + margin[1]
        y_axis_length = image.shape[0] + margin[2] + margin[3]

        #print 'x axis length: ', x_axis_length
        #print 'y axis length: ', y_axis_length

        # print 'setting x lim to: ', [-margin[0], image.shape[1]+margin[1]]
        ax.set_xlim([-margin[0], image.shape[1]+margin[1]])
        ax.set_ylim([image.shape[0]+margin[2], -margin[3]])

        #image_extent is a tuple (left, right, bottom, top) specifying in axis coordinates
        #where the bottom left and top right corners of the image should be on the axis
        #image_extent = None
        #if margin is not None:
        image_extent = (0, image.shape[1], image.shape[0], 0)

        #print 'image extent: ', image_extent
        ax.imshow(image, extent=image_extent)
        
        return fig, ax

    @classmethod
    def _get_axes_on_images(cls, image_0, image_1):
        '''
        image_0/1 - a length 2 tuple of numpy array of shape (n,m,3) and type np.uint8

        Return a tuple fig,ax_1,ax_2 where fig is a matplotlib Figure and 
        ax_i is an Axes object that has image_i in the background.
        '''
        fig = plt.figure(figsize=(13.0,16.0))
        ax_0 = fig.add_subplot(211)
        ax_1 = fig.add_subplot(212)

        axes = (ax_0, ax_1)
        images = (image_0, image_1)
        
        for ax,image in zip(axes,images):
            ax.set_xlim([0, image.shape[1]])
            ax.set_ylim([image.shape[0], 0])
            ax.imshow(image)
        
        return fig, axes[0], axes[1]

    @classmethod
    def _cv_to_mpl(cls, cv_color):
        '''
        cv_color - a tuple of integers from 0 to 255 specifying a color in opencvs BGR format
        
        return a tuple of floats from 0 to 1 specifying the color in matplotlibs RGB format
        '''

        mpl_color = (cv_color[2]/255.0, cv_color[1]/255.0, cv_color[0]/255.0)
        return mpl_color

    @classmethod
    def _position_to_axes_fraction(cls, position, image_shape):
        '''
        position - a numpy array with length 2 which specifies a point on an image.
        image_shape - a length two tuple (height, width)

        return a length 2 tuple of floats (x fraction, y fraction). They are numbers between zero and one.
        x fraction is equal to position[0] / image_shape[1]
        y fraction is equal to (image_shape[0] - position[1]) / image_shape[0]

        This function is used to format position input for the matplotlib annotate method while using the textcoords
        "axes fraction".
        '''
        position = np.float32(position)
        
        x_fraction = position[0] / image_shape[1]
        y_fraction = (image_shape[0] - position[1]) / image_shape[0]

        return (x_fraction, y_fraction)
    
    def generate_player_ids(self, frame_index, frame_data, players):
        '''
        frame_index - an integer
        frame_data - a PaddedFrameData object
        players - a list of TrackedPlayer objects

        Save an image image_{frame_index}.png in the folder output_dir/summary/player_ids/

        The image has red dots marked at the player positions. On the left of the image there is a margin
        with labels "player 0", player 1",... with blue arrow connecting the player id to the corresponding point.
        '''
        print 'generating player ids...'
        
        output_file = os.path.join(self._output_directory, self.SUMMARY_DIR, self.PLAYER_NUMBERS_DIR,
                                   self.IMAGE_BASENAME % frame_index)

        #setup an axis containing the image, with a margin on the left for drawing the player ids
        image = frame_data.get_image()
        margins = (self.PLAYER_IDS_LEFT_MARGIN, 0, 0, 0)
        fig, ax = self._get_axis_on_image(image, margins)

        #draw points in each of the player positions
        if len(players) > 0:
            player_absolute_positions = np.vstack(player.get_expected_position() for player in players)
        else:
            player_absolute_positions = np.empty((0,2))
        
        #print 'player abs positions:'
        #print player_absolute_positions
        
        #use the homography matrix to get the points in image coords
        H = frame_data.get_camera_matrix().get_homography_matrix()
        player_image_positions = planar_geometry.apply_homography_to_points(H, player_absolute_positions)

        #now compute the label positions for the id numbers.
        #To draw the id numbers, we start at the top part of the middle of the margin and work our way down.
        #distance_from_edges indicates how far we want the text to be from the top and bottom of the image
        id_x_position = self.PLAYER_IDS_LEFT_MARGIN / 2.0 - 10
        distance_from_edges = 10
        id_y_positions = np.linspace(image.shape[0]-distance_from_edges, distance_from_edges, len(players))

        if len(players) > 0:
            id_positions = np.vstack(np.array([id_x_position, y]) for y in id_y_positions)
        else:
            id_positions = np.empty((0,2))
            
        #print 'id positions:'
        #print id_positions
        
        #finally we draw everything on the axis
        #first draw the player positions
        color = self._cv_to_mpl(self.PLAYER_IDS_POSITION_COLOR)
        ax.plot(player_image_positions[:,0], player_image_positions[:,1], 'o', color=color, markersize=10)

        #now the annotations
        image_shape = (image.shape[0], image.shape[1]+self.PLAYER_IDS_LEFT_MARGIN)
        color = self._cv_to_mpl(self.PLAYER_IDS_ARROW_COLOR)
        
        for player, player_image_position, id_position in zip(players, player_image_positions, id_positions):
            data_position = tuple(player_image_position.tolist())
            label_text = self.PLAYER_IDS_LABEL_BASENAME % player.get_player_id()
            label_position = self._position_to_axes_fraction(id_position, image_shape)
            ax.annotate(label_text, xy=data_position,  xycoords='data',
                        xytext=label_position, textcoords='axes fraction',
                        arrowprops=dict(color=color,arrowstyle='->'))

        #save the image
        print 'saving figure to: ', output_file
        fig.savefig(output_file, bbox_inches='tight', dpi=100)
        plt.close(fig)
        #plt.close()
        
        return
    
    def generate_image_and_absolute_positions(self, frame_index, frame_data, players):
        '''
        frame_index - an integer
        frame_data - a PaddedFrameData object
        players - a list of TrackedPlayer objects

        Save an image image_{frame_index}.png in the folder output_dir/summary/image_and_absolute_positions/

        The output image has the original image frame on top, and a picure of the absolute field on the bottom.
        Each image has red dots marked at the player positions.
        '''
    
        output_file = os.path.join(self._output_directory, self.SUMMARY_DIR, self.IAAP_DIR,
                                   self.IMAGE_BASENAME % frame_index)

        #setup an axis containing the image, with a margin on the left for drawing the player ids
        image = frame_data.get_image()
        fig, ax_image, ax_absolute = self._get_axes_on_images(image, self._absolute_image)

        #draw points in each of the player positions
        if len(players) > 0:
            player_absolute_positions = np.vstack(player.get_expected_position() for player in players)

        else:
            player_absolute_positions = np.empty((0,2))
            
        #use the homography matrix to get the points in image coords
        H = frame_data.get_camera_matrix().get_homography_matrix()
        player_image_positions = planar_geometry.apply_homography_to_points(H, player_absolute_positions)
        
        color = self._cv_to_mpl(self.IAAP_POSITION_COLOR)
        ax_image.plot(player_image_positions[:,0], player_image_positions[:,1], 'o', color=color, markersize=10)
        ax_absolute.plot(player_absolute_positions[:,0], player_absolute_positions[:,1], 'o', color=color, markersize=10)

        #save the image
        print 'saving figure to: ', output_file
        fig.savefig(output_file, bbox_inches='tight', dpi=100)
        plt.close(fig)
        #plt.close()

        return

    def generate_player_particles(self, frame_index, frame_data, players):
        '''
        frame_index - an integer
        frame_data - a PaddedFrameData object
        players - a list of TrackedPlayer objects

        Save an image image_{frame_index}.png in the folder output_dir/summary/player_particles/

        The image has red dots marked at the player positions, and blue dots at the particle positions.
        '''
        print 'generating player particles...'
        
        output_file = os.path.join(self._output_directory, self.SUMMARY_DIR, self.PP_DIR,
                                   self.IMAGE_BASENAME % frame_index)

        #setup an axis containing the image, with a margin on the left for drawing the player ids
        image = frame_data.get_image()
        fig, ax = self._get_axis_on_image(image)

        #get the player positions on the image
        if len(players) > 0:
            player_absolute_positions = np.vstack(player.get_expected_position() for player in players)
        else:
            player_absolute_positions = np.empty((0,2))
        
        #use the homography matrix to get the points in image coords
        H = frame_data.get_camera_matrix().get_homography_matrix()
        
        #now apply the new homography to the absolute points
        player_image_positions = planar_geometry.apply_homography_to_points(H, player_absolute_positions)

        #draw the player positions
        color = self._cv_to_mpl(self.PP_POSITION_COLOR)
        #ax.plot(player_image_positions[:,0], player_image_positions[:,1], 'o', color=color, markersize=10)

        #now the particles
        for player in players:
            particle_absolute_positions = player.get_player_particle_filter()._position_slice()
            particle_image_positions = planar_geometry.apply_homography_to_points(H, particle_absolute_positions)

            ax.scatter(particle_image_positions[:,0], particle_image_positions[:,1], color='r',
                       s=self.PP_PARTICLE_RADIUS, alpha=self.PP_PARTICLE_ALPHA, zorder=1)
                
        #save the image
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            print 'saving figure to: ', output_file
            fig.savefig(output_file, bbox_inches='tight', dpi=100)
            plt.close(fig)

        #plt.close()
            
        return

    def generate_player_masks(self, frame_index, frame_data, players):
        '''
        frame_index - an integer
        frame_data - a PaddedFrameData object
        players - a list of TrackedPlayer objects

        Save an image image_{frame_index}.png in the folder output_dir/summary/player_masks/

        The image has each of the players color mask blended in.
        '''

        print 'generating player masks...'
        
        output_file = os.path.join(self._output_directory, self.SUMMARY_DIR, self.PM_DIR,
                                   self.IMAGE_BASENAME % frame_index)

        image = frame_data.get_image().copy()
        mask_color = np.array(self.PM_MASK_COLOR, np.uint8)
        alpha = self.PM_MASK_ALPHA
        
        for player in players:
            epd = player.get_expected_player_data()
            player_mask = epd.get_player_mask()
            player_rectangle = epd.get_player_rectangle()

            image_slice = player_rectangle.get_slice(image)
            colored_image_slice = image_slice.copy()
            colored_image_slice[player_mask] = mask_color

            image_slice[...] = alpha*colored_image_slice + (1-alpha)*image_slice


        #save the image
        cv2.imwrite(output_file, image)
        return

    def _generate_mean_field_particle(self, player_rectangle, player_mask, obstruction_mask,
                                      neighbor_player_rectangles, neighbor_player_masks):
        '''
        player_rectangle - a Rectangle object
        player_mask - a numpy array with the same shape as the player rectangle and type np.bool
        obstruction_mask - a numpy array with the same shape as the player rectangle and type np.bool
        neighbor_player_rectangles - a list of Rectangle objects
        neighbor_player_masks - a list of np arrays of type np.bool. The i-th array has the same shape as the i-th player
        rectangle.

        return an image whose shape is the shape of the smalles rectangle containing the player and neighbor rectangles.
        The player mask is drawn in blue, and the obs mask in red. The neighbor player masks are drawn in white.
        '''

        #first get the union of the player rectangle and all of the neighbor rectangles
        union_rectangle = player_rectangle
        for rectangle in neighbor_player_rectangles:
            union_rectangle = union_rectangle.union(rectangle)

        #create an empty image with this shape
        image = np.zeros(union_rectangle.get_shape()+(3,), np.uint8)

        #first draw the neighbor player masks in white
        for n_rectangle, n_mask in zip(neighbor_player_rectangles, neighbor_player_masks):
            image_slice = union_rectangle.get_relative_slice(image, n_rectangle)
            image_slice[n_mask] = self.MF_NEIGHBOR_COLOR

        #then draw the player rectangle in green and player mask in blue
        image_slice = union_rectangle.get_relative_slice(image, player_rectangle)
        image_slice[...] = self.MF_RECTANGLE_COLOR
        image_slice[player_mask] = self.MF_PLAYER_COLOR

        #and the obs mask in red
        image_slice[obstruction_mask] = self.MF_OBSTRUCTION_COLOR

        return image
    
    def generate_mean_field(self, frame_index, mean_field_iteration, player_id, player_particle_filter,
                            neighbor_ids, neighbor_particle_filters):
        '''
        frame_index - an integer
        player_id - an integer
        particle_filter - a ParticleFilter
        neighbor_ids - a list of integers
        neighboring_particle_filters - a list of ParticleFilter objects

        Save an images particle_j_neighbors_[i1,...,ik].png in the directory 
        mean_field/frame_{frame_index}/iteration_{mean_field_iteration}/player_{player_id}/
        where j ranges over the integers 0,...,num_particles and i1,...,ik are the neighbor ids.

        The image consists of the player mask of the player particle in blue, the player masks of the other players in white,
        and the obstruction mask of the player super imposed in red.
        '''

        output_directory = os.path.join(self._output_directory, self.MF_DIR, self.MF_FRAME_DIR % frame_index,
                                        self.MF_ITER_DIR % mean_field_iteration, self.MF_PLAYER_DIR % player_id)

        #if the output directory does not exist yet, create it
        if not os.path.isdir(output_directory):
            os.makedirs(output_directory)

        print 'writing particle obs masks to: ', output_directory
        
        neighbor_player_datas = [pf._local_player_data for pf in neighbor_particle_filters]
        neighbor_player_rectangles = [pd._player_rectangles.get_rectangle_objects()[0] for pd in neighbor_player_datas]
        neighbor_player_masks = [pd._player_mask for pd in neighbor_player_datas]

        player_data = player_particle_filter._local_player_data
        player_mask = player_data._player_mask
        player_rectangles = player_data._player_rectangles.get_rectangle_objects()
        
        particle_ids = np.arange(player_data.number_of_particles())
        particle_ids = np.random.choice(particle_ids, self.MF_NUM_PARTICLE_SAMPLES, replace=False)
        
        for particle_id in particle_ids:
            player_rectangle = player_rectangles[particle_id]
            obstruction_mask = player_data._obstruction_masks[particle_id]
            output_image = self._generate_mean_field_particle(player_rectangle, player_mask, obstruction_mask,
                                                              neighbor_player_rectangles, neighbor_player_masks)
            output_file_basename = self.MF_PARTICLE_BASENAME % (particle_id, neighbor_ids)
            output_file = os.path.join(output_directory, output_file_basename)

            print '   saving particle in: ', output_file
            cv2.imwrite(output_file, output_image)

        return
