import cv2
import os
import numpy as np
from ....source.dynamics.particle_filter_fast.frame_data import FrameData
from ....source.auxillary import planar_geometry
from ...auxillary import primitive_drawing

IMAGE_DIRECTORY = 'test_data/video7'
PREPROCESSING_NAME = 'video7'

PREPROCESSING_DATA_DIRECTORY = 'output/scripts/preprocessing/data'
BALL_TRACKER_DATA_DIRECTORY = 'output/scripts/ball_tracker/data'

ANNOTATION_DIRECTORY = os.path.join(PREPROCESSING_DATA_DIRECTORY, PREPROCESSING_NAME, 'annotation')
PLAYERS_DIRECTORY = os.path.join(PREPROCESSING_DATA_DIRECTORY, PREPROCESSING_NAME, 'players')
CAMERA_DIRECTORY = os.path.join(PREPROCESSING_DATA_DIRECTORY, PREPROCESSING_NAME, 'camera')
TEAM_LABELS_DIRECTORY = os.path.join(PREPROCESSING_DATA_DIRECTORY, PREPROCESSING_NAME, 'team_labels')

def test_load_frame_datas():
    reverse = True
    indexed_frame_data = FrameData.load_frame_datas(IMAGE_DIRECTORY, ANNOTATION_DIRECTORY, CAMERA_DIRECTORY,
                                                    PLAYERS_DIRECTORY, TEAM_LABELS_DIRECTORY, ball_trajectory_directory=None,
                                                    reverse=reverse)

    for interval, frame_data_in_interval in indexed_frame_data:
        print 'showing the frame data in interval %s:' % str(interval)
        for i,frame_data in enumerate(frame_data_in_interval):
            image = frame_data.get_image()
            camera_matrix = frame_data.get_camera_matrix()
            H = camera_matrix.get_homography_matrix()
            player_positions = np.vstack([pc.absolute_position for pc in frame_data.get_player_candidates()])
            player_image_positions = planar_geometry.apply_homography_to_points(H, player_positions)
            
            primitive_drawing.draw_points(image, player_image_positions, (255,0,0))
            cv2.imshow('image in frame %d' % i, image)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

            cv2.imshow('foreground in frame %d' % i, frame_data.get_foreground_mask().astype(np.float32))
            cv2.waitKey(0)
            cv2.destroyAllWindows()

            

if __name__ == '__main__':
    test_load_frame_datas()
                     
        
