import numpy as np
from ....source.dynamics.particle_filter_fast.tracked_balls import TrackedBalls, BallTrajectory

def test_join_forward_and_backward():
    trajectory_match_thresh = 0.1

    #trajectories:
    #frame:  8  9  10  11  12  13  14
    #0:     -2 -1  0 | 1   2   3   4
    #1:            4   3   2   1 | 0
    ft_dict = dict()
    ft_id = 0
    ft_start_frame = 10
    ft_positions = np.ones((4,3))
    ft_image_positions = np.array([[0,0], [1,1], [2,2], [3,3]])
    ft = BallTrajectory(ft_id, ft_start_frame, ft_positions, ft_image_positions, False)
    ft_dict[ft_id] = ft

    ft_id = 1
    ft_start_frame = 13
    ft_positions = np.ones((2,3))
    ft_image_positions = np.array([[1,1], [0,0]])
    ft = BallTrajectory(ft_id, ft_start_frame, ft_positions, ft_image_positions, False)
    ft_dict[ft_id] = ft
    
    forward_tracked_balls = TrackedBalls(ft_dict)

    bt_dict = dict()
    bt_id = 0
    bt_start_frame = 8
    bt_positions = np.ones((3,3))
    bt_image_positions = np.array([[-2,-2], [-1,-1], [0,0]])
    bt = BallTrajectory(bt_id, bt_start_frame, bt_positions, bt_image_positions, False)
    bt_dict[bt_id] = bt
    backward_tracked_balls = TrackedBalls(bt_dict)

    bt_id = 1
    bt_start_frame = 10
    bt_positions = np.ones((4,3))
    bt_image_positions = np.array([[4,4], [3,3], [2,2], [1,1]])
    bt = BallTrajectory(bt_id, bt_start_frame, bt_positions, bt_image_positions, False)
    bt_dict[bt_id] = bt
    backward_tracked_balls = TrackedBalls(bt_dict)

    joined_tracked_balls = TrackedBalls.join_forward_and_backward(forward_tracked_balls, backward_tracked_balls,
                                                                  trajectory_match_thresh)

    print 'joined tracked balls:'
    print joined_tracked_balls.get_ball_trajectory_dict()

if __name__ == '__main__':
    test_join_forward_and_backward()
