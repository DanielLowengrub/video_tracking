import os
import numpy as np
import cv2
import itertools
import pickle

from ....source.preprocessing import image_loader
from ....source.preprocessing.camera_samples_interpolator.camera_samples_interpolator import CameraSamplesInterpolator
from ....source.auxillary.soccer_field_geometry import SoccerFieldGeometry
from ....source.dynamics.particle_filter_fast.player_tracker_controller import PlayerTrackerController
from ....source.auxillary import planar_geometry, iterator_operations, np_operations
from ...auxillary import primitive_drawing
from ....source.preprocessing.team_labels_generator.team_label import TeamLabel
"""
This script is used to draw the players that have been tracked by a player tracker.
It loads the TrackedPlayer object that were generated by the tracker and draws them both on the absolute image 
and on the camera image.
"""

# OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts'
# IMAGE_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/test_data'
# INPUT_DATA_NAME = 'video7'

# OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
# OUTPUT_TESTING_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output_testing'
# IMAGE_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/images'
# INPUT_DATA_NAME = 'images-8_15-9_31'

# OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'
# IMAGE_PARENT_DIRECTORY = '/home/daniel/soccer/images'
# INPUT_DATA_NAME = 'images-8_15-9_31'

# OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
# INPUT_DATA_NAME = 'images-0_00-1_00'

OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'
INPUT_DATA_NAME = 'images-0_00-10_00'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_full', 'data', INPUT_DATA_NAME, 'stage_3')
#POSTPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'postprocessing', 'data', INPUT_DATA_NAME)
#POSTPROCESSING_VISUALIZATION_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'postprocessing', 'visualization',
#                                                      INPUT_DATA_NAME)

TRACKED_PLAYERS_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'player_tracker', 'data', INPUT_DATA_NAME)
PLAYER_IMAGES_DIRECTORY   = os.path.join(OUTPUT_PARENT_DIRECTORY, 'player_tracker', 'visualization', INPUT_DATA_NAME)
# TRACKED_PLAYERS_DIRECTORY = os.path.join(POSTPROCESSING_DIRECTORY, 'team_labels')
# PLAYER_IMAGES_DIRECTORY = os.path.join(POSTPROCESSING_VISUALIZATION_DIRECTORY, 'team_labels')
# TRACKED_PLAYERS_DIRECTORY = os.path.join(POSTPROCESSING_DIRECTORY, 'no_team_label_na')
# PLAYER_IMAGES_DIRECTORY = os.path.join(POSTPROCESSING_VISUALIZATION_DIRECTORY, 'no_team_label_na')
# TRACKED_PLAYERS_DIRECTORY = os.path.join(POSTPROCESSING_DIRECTORY, 'consolidated_team_labels')
# PLAYER_IMAGES_DIRECTORY = os.path.join(POSTPROCESSING_VISUALIZATION_DIRECTORY, 'consolidated_team_labels')


IMAGE_DIRECTORY   = os.path.join(PREPROCESSING_DIRECTORY, 'image')
CAMERA_DIRECTORY  = os.path.join(PREPROCESSING_DIRECTORY, 'camera')

INTERVAL_DIRECTORY = 'images_%d_%d'
OUTPUT_IMAGE_BASENAME = 'image_%04d.png'

TEAM_LABEL_COLORS = {TeamLabel.NA: (50,50,50), TeamLabel.UNDETERMINED: (50,50,50),
                     TeamLabel.TEAM_A:(255,0,0), TeamLabel.TEAM_B:(0,255,0)}
PLAYER_SIZE = 5

PLAYER_MASK_VARIANCE = np.array([2,2])
PLAYER_MASK_COLOR = np.array([255,0,0])
PLAYER_MASK_ALPHA = 0.4

FRAME_DEBUGGING_INTERVAL = None

def load_absolute_soccer_field():
    '''
    Return a tuple (soccer field, absolute image)
    '''
    ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)
    SCALE = 6
    FIELD_WIDTH = absolute_image.shape[1]-1
    FIELD_LENGTH = absolute_image.shape[0]-1
    GRID_WIDTH = 20
    GRID_LENGTH = 14
    soccer_field = SoccerFieldGeometry(SCALE, FIELD_WIDTH, FIELD_LENGTH, GRID_WIDTH, GRID_LENGTH)

    return absolute_image, soccer_field

# def load_player_data(player_data_file):
#     '''
#     player_data_file - a pickle file which contains the output of the PlayerTracker.

#     return a tuple (player_positions, player_labels)
#     player_positions - a dictionary with values: np array with shape (num frames, 2)
#     player_labels - a ditionary with the same keys as player positions, and values: np array with length num_frames
#     '''

#     f = open(player_data_file, 'rb')
#     player_data = pickle.load(f)
#     f.close()

#     player_positions = player_data[PLAYER_POSITIONS_KEY]
#     player_labels = player_data[PLAYER_LABELS_KEY]
#     #player_labels = dict((k, np.ones(positions.shape[0], np.int32) * -1) for k,positions in player_positions.iteritems())

#     nan_position = np.ones((1,2)) * np.nan
#     missing_label = np.array([-1])

#     player_positions = dict((k, np.vstack([nan_position, positions])) for k,positions in player_positions.iteritems())
#     player_labels = dict((k, np.hstack([missing_label, labels])) for k,labels in player_labels.iteritems())

#     return player_positions, player_labels


def draw_players_in_frame(frame_index, absolute_image, soccer_field, image, camera, tracked_players):
    '''
    image - an image
    absolute_image - an image
    tracked_players - a TrackedPlayers object
    '''
    absolute_image = absolute_image.copy()
    H = camera.get_homography_matrix()

    #draw the player mask
    height = soccer_field.get_player_height()
    player_mask = tracked_players.get_player_mask_in_frame(frame_index, image, camera, PLAYER_MASK_VARIANCE, height)

    #blend the player mask with the image
    colored_player_mask = image.copy()
    colored_player_mask[player_mask] = PLAYER_MASK_COLOR
    image = (PLAYER_MASK_ALPHA * colored_player_mask) + ((1 - PLAYER_MASK_ALPHA) * image)
    
    for player_key, (label, absolute_position) in tracked_players.get_labelled_positions_in_frame(frame_index).iteritems():
        if np.any(np.isnan(absolute_position)):
            continue

        #compute the image position
        image_position = planar_geometry.apply_homography_to_point(H, absolute_position)

        #draw the positions on the image and absolute image, where the color depends on the team label
        color = TEAM_LABEL_COLORS[label]
        primitive_drawing.draw_point(absolute_image, absolute_position, color, PLAYER_SIZE)
        primitive_drawing.draw_point(image, image_position, color, PLAYER_SIZE)

    #stack the image ontop of the absolute image
    output = np_operations.stack_images([image, absolute_image])

    return output


def draw_players_in_interval(absolute_image, soccer_field, pd_tuples, tracked_players, interval_directory):
    '''
    absolute_image - an image
    pd_tuples - an iterator of PreprocessingDataTuple objects
    tracked_players - a TrackedPlayers object
    interval_directory - a directory name

    draw the player in each frame and save the output to interval_directory/image_i
    '''

    for frame_index, pd_tuple in enumerate(pd_tuples):
        #If we are debugging a specific interval, then only draw frames in that interval
        if FRAME_DEBUGGING_INTERVAL is not None:
            if frame_index < FRAME_DEBUGGING_INTERVAL[0]:
                continue

            elif  frame_index > FRAME_DEBUGGING_INTERVAL[1]:
                break
            
            #if the frame is in the interval, subtract from it the first frame in the interval so that the
            #first frame in the interval will start from 0, then 1, and so on
            else:
                frame_index -= FRAME_DEBUGGING_INTERVAL[0]
                
        players_in_frame_image = draw_players_in_frame(frame_index, absolute_image, soccer_field,
                                                       pd_tuple.image, pd_tuple.camera, tracked_players)
        
        frame_file = os.path.join(interval_directory, OUTPUT_IMAGE_BASENAME % frame_index)
        print '   writing frame %d to %s' % (frame_index, frame_file)
        cv2.imwrite(frame_file, players_in_frame_image)

    return

def draw_players(tracked_players_directory, image_directory, camera_directory, output_directory):
    '''
    tracked_players_directory - a directory that can be loaded by the PlayerTrackerController
    image_directory - a directory with the images of each frame.
    output_directory - the directory where we save the player images
    '''
    from ....source.preprocessing_full.preprocessing_data_loader import PreprocessingDataLoader
    from ....source.preprocessing_full.preprocessing_data_type import PreprocessingDataType
        
    pd_directory_dict = {PreprocessingDataType.IMAGE:       image_directory,
                         PreprocessingDataType.CAMERA:      camera_directory}

    interval_data_iter = PreprocessingDataLoader.load_data(pd_directory_dict)
    indexed_pd_tuples  = ((interval_data.interval, (fd.build_data() for fd in interval_data))
                          for interval_data in interval_data_iter)
    
    absolute_image, soccer_field = load_absolute_soccer_field()

    #an iterator of elements of the form (interval, TrackedPlayers object)
    indexed_tracked_players = PlayerTrackerController.load_tracked_players(tracked_players_directory)
    
    #remove the intervals from the pd tuples
    intervals, pd_tuples = iterator_operations.iunzip(indexed_pd_tuples)
    
    #remove the intervals from the tracked players
    tracked_players = (tp_in_interval for interval,tp_in_interval in indexed_tracked_players)

    for (ik,jk), pd_tuples_in_I, tracked_players_in_I in itertools.izip(intervals, pd_tuples, tracked_players):
        interval_directory = os.path.join(output_directory, INTERVAL_DIRECTORY % (ik,jk))
        os.mkdir(interval_directory)
        print 'saving interval (%d,%d) in: %s' % (ik,jk,interval_directory)

        draw_players_in_interval(absolute_image, soccer_field, pd_tuples_in_I, tracked_players_in_I, interval_directory)

    return

if __name__ == '__main__':
    draw_players(TRACKED_PLAYERS_DIRECTORY, IMAGE_DIRECTORY, CAMERA_DIRECTORY, PLAYER_IMAGES_DIRECTORY)
