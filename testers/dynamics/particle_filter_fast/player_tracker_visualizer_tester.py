import numpy as np
import cv2
import os
from ....source.data_structures.contour import Contour
from ....source.auxillary.soccer_field_geometry import SoccerFieldGeometry
from ....source.image_processing.image_annotator.soccer_annotation import SoccerAnnotation
from ....source.auxillary.camera_matrix import CameraMatrix
from ....source.dynamics.particle_filter_fast.frame_data import FrameData, PlayerCandidate
from ....source.dynamics.particle_filter_fast.padded_frame_data import PaddedFrameData
from ....source.dynamics.particle_filter_fast.tracked_player import TrackedPlayer
from .player_tracker_visualizer import PlayerTrackerVisualizer

FRAME_INDEX = 100
IMAGE_FILE = 'test_data/video4/image_%d.png' % FRAME_INDEX
ANNOTATION_FILE = 'output/scripts/preprocessing/data/images_50_269/annotation/annotation_%d.npy'  % FRAME_INDEX
CAMERA_FILE = 'output/scripts/preprocessing/data/images_50_269/camera/camera_0_219/camera_%d.npy' % FRAME_INDEX
PLAYER_POSITIONS_FILE = 'output/scripts/preprocessing/data/images_50_269/players/players_0_219/players_%d/positions.npy'  % FRAME_INDEX
PLAYER_CONTOURS_FILE = 'output/scripts/preprocessing/data/images_50_269/players/players_0_219/players_%d/contours.npy'  % FRAME_INDEX

ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
ABSOLUTE_IMAGE = cv2.imread(ABSOLUTE_FILE_NAME, 1)

OUTPUT_DIR = 'output/tester/particle_filter_fast/player_tracker_visualizer/test_frame'

def load_absolute_soccer_field():
    '''
    Return a tuple (absolute shapes, absolute grid points)
    '''
    absolute_image = ABSOLUTE_IMAGE
    SCALE = 6
    FIELD_WIDTH = absolute_image.shape[1]-1
    FIELD_LENGTH = absolute_image.shape[0]-1
    GRID_WIDTH = 20
    GRID_LENGTH = 14
    soccer_field = SoccerFieldGeometry(SCALE, FIELD_WIDTH, FIELD_LENGTH, GRID_WIDTH, GRID_LENGTH)

    return soccer_field

def load_test_data():
    '''
    return a tuple (frame data, tracked_players) which we can use to test the player tracker visualizer
    '''
    
    image = cv2.imread(IMAGE_FILE, 1)
    soccer_annotation = SoccerAnnotation(np.load(ANNOTATION_FILE))
    foreground_mask = soccer_annotation.get_players_mask() > 0
    camera_matrix = CameraMatrix(np.load(CAMERA_FILE))
    positions = np.load(PLAYER_POSITIONS_FILE)
    contours =  [Contour(c, image) for c in np.load(PLAYER_CONTOURS_FILE)]
    
    player_candidates = [PlayerCandidate(contour, position) for contour,position in zip(contours,positions)]

    frame_data = FrameData(image, foreground_mask, camera_matrix, player_candidates)
    padding = 50
    frame_data = PaddedFrameData.from_frame_data(frame_data, padding)
    
    soccer_field = load_absolute_soccer_field()
    num_particles = 300
    
    tracked_players = [TrackedPlayer.from_player_candidate(i,pc,num_particles,soccer_field,frame_data)
                       for i,pc in enumerate(player_candidates)]

    return frame_data, tracked_players

def load_player_and_neighbors():
    '''
    return a tuple (player, lists of neighboring players)
    '''

    image = cv2.imread(IMAGE_FILE, 1)
    soccer_annotation = SoccerAnnotation(np.load(ANNOTATION_FILE))
    foreground_mask = soccer_annotation.get_players_mask() > 0
    camera_matrix = CameraMatrix(np.load(CAMERA_FILE))
    positions = np.load(PLAYER_POSITIONS_FILE)
    contours =  [Contour(c, image) for c in np.load(PLAYER_CONTOURS_FILE)]
    
    player_candidate = PlayerCandidate(contours[0], positions[0])
    neighbor_candidate = PlayerCandidate(contours[0], positions[0] + np.array([-2,8]))
    
    frame_data = FrameData(image, foreground_mask, camera_matrix, [])
    padding = 50
    frame_data = PaddedFrameData.from_frame_data(frame_data, padding)
    
    soccer_field = load_absolute_soccer_field()
    num_particles = 300

    player = TrackedPlayer.from_player_candidate(0, player_candidate, num_particles, soccer_field, frame_data)
    player._player_particle_filter._position_slice()[...] += 3*np.random.normal(size=(num_particles,2))
    player._player_particle_filter._update_local_player_data()
    neighbor = TrackedPlayer.from_player_candidate(0, neighbor_candidate, num_particles, soccer_field, frame_data)

    return player, [neighbor]

def player_ids_test():
    frame_data, tracked_players = load_test_data()
    frame_index = FRAME_INDEX
    
    visualizer = PlayerTrackerVisualizer(OUTPUT_DIR, ABSOLUTE_IMAGE)

    visualizer.generate_player_ids(frame_index, frame_data, tracked_players)

def image_and_absolute_positions_test():
    frame_data, tracked_players = load_test_data()
    frame_index = FRAME_INDEX
    
    visualizer = PlayerTrackerVisualizer(OUTPUT_DIR, ABSOLUTE_IMAGE)

    visualizer.generate_image_and_absolute_positions(frame_index, frame_data, tracked_players)

def player_particles_test():
    frame_data, tracked_players = load_test_data()
    frame_index = FRAME_INDEX
    
    visualizer = PlayerTrackerVisualizer(OUTPUT_DIR, ABSOLUTE_IMAGE)

    visualizer.generate_player_particles(frame_index, frame_data, tracked_players)

def player_masks_test():
    frame_data, tracked_players = load_test_data()
    frame_index = FRAME_INDEX
    
    visualizer = PlayerTrackerVisualizer(OUTPUT_DIR, ABSOLUTE_IMAGE)

    for player in tracked_players:
        player.refresh_expected_player_data()
        
    visualizer.generate_player_masks(frame_index, frame_data, tracked_players)

def mean_field_test():
    player, neighbors = load_player_and_neighbors()
    frame_index = FRAME_INDEX
    mean_field_iteration = 0
    
    visualizer = PlayerTrackerVisualizer(OUTPUT_DIR, ABSOLUTE_IMAGE)

    player_id = player.get_player_id()
    player_particle_filter = player.get_player_particle_filter()

    neighbor_ids = [p.get_player_id() for p in neighbors]
    neighbor_particle_filters = [p.get_player_particle_filter() for p in neighbors]

    #compute the obstructions
    #first we have to reset the obstruction masks in each of the player datas
    player_particle_filter._local_player_data.initialize_obstruction_masks()
    for pf in neighbor_particle_filters:
        pf._local_player_data.initialize_obstruction_masks()
        player_particle_filter._local_player_data.add_obstructions(pf._local_player_data)

    visualizer.generate_mean_field(frame_index, mean_field_iteration, player_id, player_particle_filter,
                                   neighbor_ids, neighbor_particle_filters)

    return

if __name__ == '__main__':
    #player_ids_test()
    #image_and_absolute_positions_test()
    #player_particles_test()
    #player_masks_test()
    mean_field_test()
