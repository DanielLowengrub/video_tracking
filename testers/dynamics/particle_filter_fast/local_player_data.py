import numpy as np
import cv2
import cProfile
import time
from ....source.auxillary.soccer_field_geometry import SoccerFieldGeometry
from ....source.dynamics.particle_filter_fast.local_player_data import LocalPlayerData
from ....source.dynamics.particle_filter_fast.local_player_data_list import LocalPlayerDataList
from ....source.image_processing.image_annotator.soccer_annotation import SoccerAnnotation
from ....source.auxillary.camera_matrix import CameraMatrix
from ....source.dynamics.particle_filter_fast.frame_data import FrameData

IMAGE_FILE = 'test_data/video4/image_0.png'
ANNOTATION_FILE = 'output/scripts/preprocessing/data/images_50_269/annotation/annotation_0.npy'
CAMERA_FILE = 'output/scripts/preprocessing/data/images_50_269/camera/camera_0_219/camera_0.npy'
PLAYER_POSITIONS_FILE = 'output/scripts/preprocessing/data/images_50_269/players/players_0_219/players_0/positions.npy'

def load_absolute_soccer_field():
    '''
    Return a tuple (absolute shapes, absolute grid points)
    '''
    ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)
    SCALE = 6
    FIELD_WIDTH = absolute_image.shape[1]-1
    FIELD_LENGTH = absolute_image.shape[0]-1
    GRID_WIDTH = 20
    GRID_LENGTH = 14
    soccer_field = SoccerFieldGeometry(SCALE, FIELD_WIDTH, FIELD_LENGTH, GRID_WIDTH, GRID_LENGTH)

    return soccer_field

def build_frame_data(num_players, num_particles):
    image = cv2.imread(IMAGE_FILE, 1)
    soccer_annotation = SoccerAnnotation(np.load(ANNOTATION_FILE))
    foreground_mask = soccer_annotation.get_players_mask() > 0
    camera_matrix = CameraMatrix(np.load(CAMERA_FILE))
    positions = np.load(PLAYER_POSITIONS_FILE)

    absolute_positions = positions[0] + 0*np.random.normal(size=(num_particles,2))
    frame_data = FrameData(image, foreground_mask, camera_matrix)

    soccer_field = load_absolute_soccer_field()
    soccer_rectangle = soccer_field.get_field_rectangle()

    # cv2.imshow('image', image)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    # cv2.imshow('fg mask', np.float32(foreground_mask))
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    # print 'positions:'
    # print absolute_positions

    print 'building %d players, each with %d particles...' % (num_players, num_particles)
    start = time.time()
    for i in range(num_players):
        local_data = LocalPlayerData.from_frame_and_positions(soccer_rectangle, frame_data, absolute_positions,
                                                                  number_of_particles=None)
        #print 'shape of region %d: ' % i, local_data._region.get_shape()
        
    print 'finished in %f seconds' % (time.time() - start)

    # cv2.imshow('cropped image', local_data._cropped_image)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    # cv2.imshow('fg mask', np.float32(local_data._cropped_foreground_mask))
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    # for m in local_data._cropped_player_masks:
    #     cv2.imshow('player mask', np.float32(m))
    #     cv2.waitKey(0)
    #     cv2.destroyAllWindows()

    #print 'cropped obstruction masks shape: ', local_data._cropped_obstruction_masks.shape
    #print 'degenerate particles: ', local_data._degenerate_particle_mask

def test_add_obstructions():
    image = cv2.imread(IMAGE_FILE, 1)
    soccer_annotation = SoccerAnnotation(np.load(ANNOTATION_FILE))
    foreground_mask = soccer_annotation.get_players_mask() > 0
    camera_matrix = CameraMatrix(np.load(CAMERA_FILE))
    camera_matrix.compute_camera_parameters()
    
    positions = np.load(PLAYER_POSITIONS_FILE)

    frame_data = FrameData(image, foreground_mask, camera_matrix)

    soccer_field = load_absolute_soccer_field()
    soccer_rectangle = soccer_field.get_field_rectangle()

    cv2.imshow('image', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    cv2.imshow('fg mask', np.float32(foreground_mask))
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    absolute_positions_0 = np.stack([positions[0] for i in range(100)])
    absolute_positions_1 = positions[:1] + np.array([-3,10])

    #absolute_positions_0 = positions[:1]
    #absolute_positions_1 = np.stack([positions[0] + np.array([1,1]) for i in range(5)])

    #absolute_positions_0 = positions[:1]
    #absolute_positions_1 = positions[:1] + np.array([1,1])
    
    print 'positions_0:'
    print absolute_positions_0
    print 'positions_1:'
    print absolute_positions_1

    local_datas = []
    local_datas.append(LocalPlayerData.from_frame_and_positions(soccer_rectangle, frame_data, absolute_positions_0,
                                                                number_of_particles=None))
    local_datas.append(LocalPlayerData.from_frame_and_positions(soccer_rectangle, frame_data, absolute_positions_1,
                                                                number_of_particles=100))


    for i,local_data in enumerate(local_datas):
        print 'displaying player %d:' % i
        
        cv2.imshow('cropped image', local_data._cropped_image)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

        cv2.imshow('fg mask', np.float32(local_data._cropped_foreground_mask))
        cv2.waitKey(0)
        cv2.destroyAllWindows()

        print '   first player mask:'
        player_mask = local_data._cropped_player_masks[0]
        cv2.imshow('first player mask', np.float32(player_mask))
        cv2.waitKey(0)
        cv2.destroyAllWindows()

        blue_mask = np.zeros(local_data._region.get_shape()+(3,), np.uint8)
        blue_mask[player_mask] = np.array([255,0,0])
        local_data._region.get_slice(image)[...] = blue_mask

    cv2.imshow('image with player masks', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    local_datas[0].add_obstructions(local_datas[1])

    print 'shape of player 0 obstructions: ', local_datas[0]._cropped_obstruction_masks.shape
    # for i,m in enumerate(local_datas[0]._cropped_obstruction_masks):
    #     cv2.imshow('obs mask %d' % i, np.float32(m))
    #     cv2.waitKey(0)
    #     cv2.destroyAllWindows()

    red_mask = np.zeros(local_datas[0]._region.get_shape()+(3,), np.uint8)
    red_mask[local_datas[0]._cropped_obstruction_masks[0]] = np.array([0,0,255])
    local_datas[0]._region.get_slice(image)[...] = red_mask

    cv2.imshow('obs mask in red', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

if __name__ == '__main__':
    #build_frame_data(20,300)
    cProfile.run('build_frame_data(20,300)')
    #test_add_obstructions()
