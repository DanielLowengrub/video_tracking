import os
import pickle
import cv2
import numpy as np
from ....source.dynamics.particle_filter.player_tracker import PlayerTracker
from ....source.data_structures.frame_sequence.frame_sequence import FrameSequence
from ....source.auxillary.camera_matrix import CameraMatrix
from .player_tracker_visualization import generate_player_tracker_visualization

##########
# In this script we load a frame sequence and feed it to the PlayerTracker,
# We then use player_tracker_visualization to output a visualization of the algorithm at work.
#########

# OUTPUT_DIRECTORY = 'output/tester/particle_filter/player_data/frame_sequence_50_79-0_29/'
# DEBUGGING_OUTPUT_DIRECTORY = 'output/tester/particle_filter/frame_sequences/frame_sequence_50_79-0_29/'
# FRAME_SEQUENCE_DIR = 'output/tester/frame_sequences/frame_sequence_test_3/frame_sequences_50_79-0_29'

#OUTPUT_DIRECTORY = 'output/tester/player_tracker/player_positions/frame_sequence_148_150-0_2/'
#DEBUGGING_OUTPUT_DIRECTORY = 'output/tester/player_tracker/frame_sequences/frame_sequence_148_150-0_2/'
#FRAME_SEQUENCE_DIR = 'output/tester/frame_sequences/frame_sequence_test_4/frame_sequences_148_150-0_2'

#OUTPUT_DIRECTORY = 'output/tester/player_tracker/frame_sequences/frame_sequence_50_109-0_59/'
#FRAME_SEQUENCE_DIR = 'output/tester/frame_sequences/frame_sequence_test_5/frame_sequences_50_109-0_59'

#OUTPUT_DIRECTORY = 'output/tester/player_tracker/frame_sequences/frame_sequence_50_148-0_98/'
#FRAME_SEQUENCE_DIR = 'output/tester/frame_sequences/frame_sequence_test_6/frame_sequences_50_148-0_98'

# OUTPUT_DIRECTORY = 'output/tester/player_tracker/player_positions/frame_sequence_50_199-0_149/'
# DEBUGGING_OUTPUT_DIRECTORY = 'output/tester/particle_filter/frame_sequences/frame_sequence_50_199-0_149/'
# FRAME_SEQUENCE_DIR = 'output/tester/frame_sequences/frame_sequence_test_7/frame_sequences_50_199-0_149'

OUTPUT_DIRECTORY = 'output/tester/particle_filter/player_data/frame_sequence_50_269-0_219/'
DEBUGGING_OUTPUT_DIRECTORY = 'output/tester/particle_filter/frame_sequences/frame_sequence_50_269-0_219/'
FRAME_SEQUENCE_DIR = 'output/tester/frame_sequences/frame_sequence_test_8/frame_sequences_50_269-0_219'


ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'

def save_player_data(player_tracker):
    data_dict = dict()
    data_dict['player_positions'] = player_tracker.get_player_positions()
    data_dict['player_position_variances'] = player_tracker.get_player_position_variances()
    data_dict['player_position_entropies'] = player_tracker.get_player_position_entropies()
    data_dict['player_obstructed_percentages'] = player_tracker.get_player_obstructed_percentages()
    data_dict['player_unobstructed_foreground_percentages'] = player_tracker.get_player_unobstructed_foreground_percentages()

    f = open(os.path.join(OUTPUT_DIRECTORY, 'player_data.pckl'), 'w')
    pickle.dump(data_dict, f)
    f.close()
        
    return

def test_player_tracker():
    #first load the frame sequence
    frame_sequence = FrameSequence.load_frame_sequence(FRAME_SEQUENCE_DIR, load_homography_to_abs=True, load_camera_matrix=True)
    frames = frame_sequence.get_frames()
    #create a player tracker
    images = [frame.get_image() for frame in frames]
    transformations_to_abs = [frame.get_homography_to_abs() for frame in frames]
    camera_matrices = [frame.get_camera_matrix() for frame in frames]

    #this is a HACK!
    #since the first camera matrix is None, we replace is with homograph_from_0_to_1 * camera_matrix_of_1
    camera_matrices[0] = CameraMatrix(np.dot(frames[0].get_homography_to_next(), camera_matrices[1].get_numpy_matrix()))
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)
    
    #compute the camera parameters
    for camera_matrix in camera_matrices:
        camera_matrix.compute_camera_parameters()
        
    #camera_matrices = [None for frame in frame_sequence.get_frames()]
    player_contours = [frame.get_player_contours() for frame in frames]
    foreground_masks = [frame.get_foreground_mask() for frame in frames]
    absolute_bounds = np.array([absolute_image.shape[1], absolute_image.shape[0]])
    
    player_tracker = PlayerTracker(images, transformations_to_abs, camera_matrices, player_contours, foreground_masks, absolute_bounds)
    
    #track players over time
    player_tracker.track_players()

    #save the player data that we recorded
    save_player_data(player_tracker)
    
    #generate the visualization
    generate_player_tracker_visualization(DEBUGGING_OUTPUT_DIRECTORY, player_tracker, absolute_image)

if __name__ == '__main__':
    test_player_tracker()
