import numpy as np
import cv2
from ....source.dynamics.particle_filter.local_player_data import LocalPlayerData
from ....source.dynamics.particle_filter.rectangle import Rectangle
from ....source.auxillary.fit_points_to_shape.ellipse import Ellipse

#############################
# Some basic unit tests for the LocalPlayerData class.
#############################

def test_get_cropped_image():
    #make an image with a blue square
    image = np.zeros((200,200,3), np.uint8)
    image[50:100, 50:100] = np.array([255, 0, 0])

    #construct a rectangle in the position of the blue square
    top_left = np.array([50, 50])
    bottom_right = np.array([100, 100])
    rectangle = Rectangle(top_left, bottom_right)

    #the player mask has a hole in the middle
    player_mask = np.ones(rectangle.get_shape(), np.float32)
    player_mask[10:40, 10:40] = 0.0
    
    #construct a local player data object
    player_data = LocalPlayerData(image, None, rectangle, player_mask)

    cropped_image = player_data.get_cropped_image()

    cv2.imshow('image', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    cv2.imshow('player mask', player_mask)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    cv2.imshow('cropped image', cropped_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def test_from_ellipse():
    #make an image with a blue square
    image = np.zeros((200,200,3), np.uint8)
    image[50:100, 50:100] = np.array([255, 0, 0])

    #make an ellipse
    center = np.array([50.5, 50.5])
    axes = np.array([20, 10])
    angle = 0.0
    ellipse = Ellipse.from_geometric_parameters(center, axes, angle)
    ellipse.compute_geometric_parameters()
    
    player_data = LocalPlayerData.from_ellipse(image, None, ellipse)

    #draw the cropped player mask on the image
    player_mask = player_data.get_cropped_player_mask()
    colored_player_mask = np.zeros((player_mask.shape[0], player_mask.shape[1], 3), np.uint8)
    colored_player_mask[player_mask > 0] = np.array([0,0,255])
    image_with_player_mask = image.copy()
    player_data._rectangle.insert_array(image_with_player_mask, colored_player_mask)

    cv2.imshow('image', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    cv2.imshow('player mask', player_data.get_cropped_player_mask())
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    cv2.imshow('image with player mask', image_with_player_mask)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    cv2.imshow('cropped image', player_data.get_cropped_image())
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def test_get_cropped_obstruction_from_other():
    image = np.zeros((200,200,3), np.uint8)

    #make two players from ellipses
    center_A = np.array([75,100])
    center_B = np.array([85,150])
    axes = np.array([40, 25])
    angle = 0

    ellipse_A = Ellipse.from_geometric_parameters(center_A, axes, angle)
    ellipse_B = Ellipse.from_geometric_parameters(center_B, axes, angle)
    ellipse_A.compute_geometric_parameters()
    ellipse_B.compute_geometric_parameters()
    
    player_data_A = LocalPlayerData.from_ellipse(image, None, ellipse_A)
    player_data_B = LocalPlayerData.from_ellipse(image, None, ellipse_B)

    #now get the obstruction on player A from player Bs mask
    obstruction_mask = player_data_A.get_cropped_obstruction_from_other(player_data_B)

    cv2.imshow('obstruction', obstruction_mask)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    cv2.imshow('A', player_data_A.get_cropped_player_mask())
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    cv2.imshow('B', player_data_B.get_cropped_player_mask())
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def test_get_intersection():
    image = np.zeros((200,200,3), np.uint8)

    #make two players from ellipses
    center_A = np.array([75,75])
    center_B = np.array([110,75])
    axes_A = np.array([40, 40])
    axes_B = np.array([40, 10])
    angle = 0

    ellipse_A = Ellipse.from_geometric_parameters(center_A, axes_A, angle)
    ellipse_B = Ellipse.from_geometric_parameters(center_B, axes_B, angle)
    ellipse_A.compute_geometric_parameters()
    ellipse_B.compute_geometric_parameters()
    
    player_data_A = LocalPlayerData.from_ellipse(image, None, ellipse_A)
    player_data_B = LocalPlayerData.from_ellipse(image, None, ellipse_B)

    #now get the obstruction on player A from player Bs mask
    intersection = player_data_A.get_intersection(player_data_B)

    print 'player A has rectangle: '
    print player_data_A._rectangle
    print 'player B has rectangle: '
    print player_data_B._rectangle
    print 'the intersection has the rectangle:'
    print intersection._rectangle

    #draw both ellipses on the image
    cv2.ellipse(image, tuple(np.int32(center_A)), tuple(np.int32(axes_A).tolist()), angle, 0, 360, (255,0,0), 2, cv2.CV_AA)
    cv2.ellipse(image, tuple(np.int32(center_B)), tuple(np.int32(axes_B).tolist()), angle, 0, 360, (0,255,0), 2, cv2.CV_AA)


    cv2.imshow('ellipses', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    
    cv2.imshow('intersection', intersection.get_cropped_player_mask())
    cv2.waitKey(0)
    cv2.destroyAllWindows()

if __name__ == '__main__':
    #test_get_cropped_image()
    #test_from_ellipse()
    #test_get_cropped_obstruction_from_other()
    test_get_intersection()
