import os
import numpy as np
import cv2

from ....source.auxillary.mask_operations import mix_images_with_mask
from ....source.data_structures.frame_sequence.frame_sequence import FrameSequence
from ....source.dynamics.particle_filter.player_tracker import PlayerTracker

##############################################################
# Here we test some of the methods in tracked player class
##############################################################

PARENT_DIR = 'output/tester/frame_sequences/frame_sequence_test_7'
FRAME_SEQUENCE_NAME = 'frame_sequence_50_199-0_149'
FRAME_SEQUENCE_DIR = os.path.join(PARENT_DIR, 'frame_sequences_50_199-0_149')
PLAYER_DATA_DIR = os.path.join('output/tester/player_tracker/player_positions', FRAME_SEQUENCE_NAME)

PLAYER_POSITIONS_FILE = os.path.join(PLAYER_DATA_DIR, 'player_positions.npy')

ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
FRAME_INDEX = 48
CHOSEN_ID = 8

def load_player_absolute_positions(frame_index=None):
    '''
    Return a list containing the positions in absolute coordinates at the given frame of the player with ids PLAYER_IDS
    '''
    absolute_positions = np.load(PLAYER_POSITIONS_FILE)

    if frame_index is None:
        return absolute_positions

    else:
        return absolute_positions[:,frame_index,:]

def display_players(image, players):
    output = image.copy()
    
    for player in players:
        #get a copy of the image with the player mask colored in blue
        player_data = player.get_expected_local_player_data()
        player_mask = player_data.get_cropped_player_mask()
        color = np.array([255,0,0])
        if player.get_player_id() == CHOSEN_ID:
            color = np.array([0,0,255])
            
        colored_player_mask = np.zeros((player_mask.shape[0], player_mask.shape[1], 3), np.uint8)
        colored_player_mask[player_mask > 0] = color
        image_with_player_mask = output.copy()

        player_data._rectangle.insert_array(image_with_player_mask, colored_player_mask)

        #mix this with the output image
        alpha = 0.5
        output = np.uint8((alpha*output) + ((1-alpha)*image_with_player_mask))

        # print 'processing player:'
        # cv2.imshow('image with player mask', image_with_player_mask)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

    
    cv2.imshow('local data', output)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def test_joint_log_probability():
    #load the frame sequence
    frame_sequence = FrameSequence.load_frame_sequence(FRAME_SEQUENCE_DIR, load_homography_to_abs=True, load_camera_matrix=True)
    
    #create a player tracker using just the given frame
    frames = frame_sequence.get_frames()
    images = [frame.get_image() for frame in frames]
    transformations_to_abs = [frame.get_homography_to_abs() for frame in frames]
    camera_matrices = [frame.get_camera_matrix() for frame in frames]
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)
    
    #compute the camera parameters
    for camera_matrix in camera_matrices:
        camera_matrix.compute_camera_parameters()
        
    #camera_matrices = [None for frame in frame_sequence.get_frames()]
    player_contours = [frame.get_player_contours() for frame in frames]
    foreground_masks = [frame.get_foreground_mask() for frame in frames]
    absolute_bounds = np.array([absolute_image.shape[1], absolute_image.shape[0]])
    
    player_tracker = PlayerTracker(images, transformations_to_abs, camera_matrices, player_contours, foreground_masks, absolute_bounds)
    player_tracker._initialize()
    
    #initialize the players to the given frame with specified positions
    tracked_players = player_tracker._tracked_players
    absolute_positions = load_player_absolute_positions(FRAME_INDEX)

    #perturb the position of player 6
    absolute_positions[5] = np.array([457, 257])
    #absolute_positions[6] = np.array([456,297])
    absolute_positions[8] = np.array([465,236])
    #absolute_positions[11] = np.array([532,194])
    
    image = player_tracker._images[FRAME_INDEX]
    foreground_mask = player_tracker._foreground_masks[FRAME_INDEX]
    camera_matrix = player_tracker._camera_matrices[FRAME_INDEX]
    
    for tracked_player in tracked_players:
        print 'initializing player %d...' % tracked_player.get_player_id()
        absolute_position = absolute_positions[tracked_player.get_player_id()]

        print 'setting position to: ', absolute_position
        
        tracked_player.set_frame_data(image, foreground_mask, camera_matrix)

        #set the player positions by updating the values of all of the particles it contains
        for particle in tracked_player.get_player_particle_filter()._particles:
            particle._position = absolute_position
            particle._local_player_data = particle._tracked_player.get_local_player_data(particle._position)
        #after setting the player position, set the obstruction mask, player data and particle data
        tracked_player.compute_expected_obstruction_mask()
        tracked_player.compute_expected_local_player_data()

    #with that out of the way, we now have a list of tracked players, whose particles are in the specified absolute positions.
    #before proceeding, we display the players to make sure that everything checks out
    display_players(image, tracked_players)

    #now we compute the joint probability of a particle of the first player w.r.t the second player
    other_players = []
    for player in tracked_players:
        if player.get_player_id() == CHOSEN_ID:
            chosen_player = player
        else:
            other_players.append(player)
            
    particle = chosen_player._player_particle_filter._particles[0]
    joint_log_probability = chosen_player.compute_joint_log_probability(particle._local_player_data, particle._position, other_players)

    print 'the joint log probability is: ', joint_log_probability
    
if __name__ == '__main__':
    test_joint_log_probability()
