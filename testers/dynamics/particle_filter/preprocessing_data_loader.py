import os
import pickle
import cv2
import numpy as np
import itertools
from ....source.dynamics.particle_filter.player_tracker import PlayerTracker
from .player_tracker_visualization import generate_player_tracker_visualization

from ....source.preprocessing import image_loader
from ....source.preprocessing.annotation_generator.annotation_generator import AnnotationGenerator
from ....source.preprocessing.absolute_homography_generator.absolute_homography_generator import AbsoluteHomographyGenerator
from ....source.preprocessing.camera_samples_interpolator.camera_samples_interpolator import CameraSamplesInterpolator
from ....source.preprocessing.players_generator.players_generator import PlayersGenerator
from ....source.auxillary import iterator_operations

##########
# In this script we load preprocessing data and feed it to the PlayerTracker,
# We then use player_tracker_visualization to output a visualization of the algorithm at work.
#########

IMAGE_DIRECTORY = 'test_data/video4'
PREPROCESSING_NAME = 'images_50_269'

ANNOTATION_DIRECTORY = os.path.join('output/scripts/preprocessing/data', PREPROCESSING_NAME, 'annotation')
ABSOLUTE_HOMOGRAPHY_DIRECTORY = os.path.join('output/scripts/preprocessing/data', PREPROCESSING_NAME, 'absolute_homography')
PLAYERS_DIRECTORY = os.path.join('output/scripts/preprocessing/data', PREPROCESSING_NAME, 'players')
CAMERA_DIRECTORY = os.path.join('output/scripts/preprocessing/data', PREPROCESSING_NAME, 'camera')

OUTPUT_DIRECTORY = os.path.join('output/tester/particle_filter/preprocessing_data_loader', PREPROCESSING_NAME, 'data')
VISUALIZATION_DIRECTORY = os.path.join('output/tester/particle_filter/preprocessing_data_loader', PREPROCESSING_NAME,
                                       'visualization')

ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'

def save_player_data(player_tracker):
    data_dict = dict()
    data_dict['player_positions'] = player_tracker.get_player_positions()
    data_dict['player_position_variances'] = player_tracker.get_player_position_variances()
    data_dict['player_position_entropies'] = player_tracker.get_player_position_entropies()
    data_dict['player_obstructed_percentages'] = player_tracker.get_player_obstructed_percentages()
    data_dict['player_unobstructed_foreground_percentages'] = player_tracker.get_player_unobstructed_foreground_percentages()

    f = open(os.path.join(OUTPUT_DIRECTORY, 'player_data.pckl'), 'w')
    pickle.dump(data_dict, f)
    f.close()
        
    return

def test_player_tracker(image_directory, absolute_homography_directory, camera_directory, players_directory,
                        annotation_directory):

    print 'loading the preprocessing data...'
    
    images = image_loader.load_images(image_directory)
    indexed_absolute_homographies = AbsoluteHomographyGenerator.load_homographies(absolute_homography_directory)
    indexed_cameras = CameraSamplesInterpolator.load_cameras(camera_directory)
    indexed_player_contours = PlayersGenerator.load_player_contours(players_directory)
    annotations = AnnotationGenerator.load_annotations(annotation_directory)

    indexed_absolute_homographies, iter_copy_1, iter_copy_2, iter_copy_3 = itertools.tee(indexed_absolute_homographies, 4)
    intervals   = (interval for interval,Hs in iter_copy_1)
    intervals_2 = (interval for interval,Hs in iter_copy_2)
    intervals_3 = (interval for interval,Hs in iter_copy_3)

    #all of the following iterators have the form (I0, I1, ...) where Ik is an iterator over the frames in the k-th interval
    images = iterator_operations.extract_intervals(images, intervals_2)
    annotations = iterator_operations.extract_intervals(annotations, intervals_3)
    absolute_homographies = (Hs_in_interval for interval,Hs_in_interval in indexed_absolute_homographies)
    cameras = (Cs_in_interval for interval,Cs_in_interval in indexed_cameras)
    player_contours = (contours_in_interval for interval,contours_in_interval in indexed_player_contours)

    #for the moment, we only use the first interval. To do this, we call next on each of the above iterators which returns
    #I0.
    interval = next(intervals)
    images = list(next(images))
    foreground_masks = list(annotation.get_players_mask() for annotation in next(annotations))
    transformations_to_abs = list(np.linalg.inv(H) for H in next(absolute_homographies))
    cameras = list(next(cameras))
    player_contours = list(next(player_contours))

    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)
    absolute_bounds = np.array([absolute_image.shape[1], absolute_image.shape[0]])

    for image, contours_in_frame in zip(images, player_contours):
        for contour in contours_in_frame:
            contour.set_image(image)

    for camera in cameras:
        camera.compute_camera_parameters()
        
    print 'initializing player tracker...'
    
    player_tracker = PlayerTracker(images, transformations_to_abs, cameras, player_contours, foreground_masks, absolute_bounds)
    
    #track players over time
    print 'tracking players in interval (%d,%d)...' % interval
    player_tracker.track_players()

    #save the player data that we recorded
    save_player_data(player_tracker)
    
    #generate the visualization
    generate_player_tracker_visualization(VISUALIZATION_DIRECTORY, player_tracker, absolute_image)

if __name__ == '__main__':
    test_player_tracker(IMAGE_DIRECTORY, ABSOLUTE_HOMOGRAPHY_DIRECTORY, CAMERA_DIRECTORY, PLAYERS_DIRECTORY,
                        ANNOTATION_DIRECTORY)
