import numpy as np
import cv2
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
from ....source.auxillary.mask_operations import mix_images_with_mask, get_union
from ....source.dynamics.particle_filter.tracked_player import TrackedPlayer

PLAYER_PARTICLE_IMAGE_RADIUS = 30
PLAYER_PARTICLE_ABSOLUTE_IMAGE_RADIUS = 30
PLAYER_PARTICLE_IMAGE_ALPHA = 0.3
PLAYER_PARTICLE_ABSOLUTE_IMAGE_ALPHA = 0.3

MIN_ARROW_LENGTH = 5.0

PLAYER_COLORS = ['m', 'g', 'k', 'w']

def stack_images(images, axis, border_size=2):
    '''
    Reshape the images so that they are all the same shape in the coordinates that are not the axis, then stack them on the axis coordinate.
    '''
    #if the axis is 0, other should be 1. If the axis is 1, other axis should be 0.
    other_axis = 1 - axis
    max_length_on_other_axis = max(image.shape[other_axis] for image in images)

    padded_images = []

    for image in images:
        extra_on_other_axis = max_length_on_other_axis - image.shape[other_axis]

        padding_shape = list(image.shape)
        padding_shape[other_axis] = extra_on_other_axis
        padding = np.zeros(padding_shape)
        padded_image = np.concatenate((image, padding), axis=other_axis)

        border_shape = list(padded_image.shape)
        border_shape[axis] = border_size
        border = np.ones(border_shape) * 255
        padded_image_with_border = np.concatenate((padded_image, border), axis=axis)
        
        padded_images.append(padded_image_with_border)

    return np.concatenate(tuple(padded_images), axis=axis)

def convert_figure_to_image(figure):
    '''
    figure - a matplotlib figure object.
    Return a numpy array of shape (n,m,3) and type np.uint8
    '''

    figure.tight_layout()
    figure.canvas.draw()
    image = np.fromstring(figure.canvas.tostring_rgb(), dtype=np.uint8, sep='')
    image = image.reshape(figure.canvas.get_width_height()[::-1] + (3,))
    plt.close()

    return image

def get_axis_on_image(image):
    '''
    image - numpy array of shape (n,m,3) and type np.uint8
    Return a matplotlib Axes object that has the image in the background.
    '''
    fig = plt.figure(figsize=(13.0,8.0))
    ax = fig.add_subplot(111)
    ax.set_xlim([0, image.shape[1]])
    ax.set_ylim([image.shape[0], 0])
    ax.imshow(image)

    return fig, ax

def draw_scatter_plot_on_axis(points, ax, scale, alpha):
    ax.scatter(points[:,0], points[:,1], s=scale, alpha=alpha)

def draw_player_particles(player_particles, ax_absolute_image, ax_image, transformation_from_abs):
    '''
    player_particles - a list of PlayerParticle objects
    ax_absolute_image, ax_image - matplotlib Axes objects
    transformation_from_abs - numpy array of shape (3,3)

    We draw the player particles on the given axes
    '''

    #first get a list of absolute and image positions
    absolute_positions = np.float32([particle.get_position() for particle in player_particles])
    image_positions = cv2.perspectiveTransform(absolute_positions.reshape(-1,1,2), transformation_from_abs).reshape((-1,2))

    #draw the positions on the given axes using a scatter plot
    ax_absolute_image.scatter(absolute_positions[:,0], absolute_positions[:,1], s=PLAYER_PARTICLE_ABSOLUTE_IMAGE_RADIUS, alpha=PLAYER_PARTICLE_ABSOLUTE_IMAGE_ALPHA)
    ax_image.scatter(image_positions[:,0], image_positions[:,1], s=PLAYER_PARTICLE_IMAGE_RADIUS, alpha=PLAYER_PARTICLE_IMAGE_ALPHA, zorder=1)
    
    return

def get_visualization_for_player(tracked_player, frame_index, transformation_from_abs, ax_absolute_image, ax_image):
    '''
    tracked_player - TrackedPlayer object
    ax_absolute_image, ax_image - matplotlib Axis object

    Draw a representation of the tracked player on the given axes
    '''
    if tracked_player._saved_player_states[frame_index] == TrackedPlayer.DORMANT:
        return

    mean_absolute_position = tracked_player._saved_mean_positions[frame_index]
    mean_absolute_velocity = tracked_player._saved_mean_velocities[frame_index]
    player_particles = tracked_player._saved_particles[frame_index]
    mean_image_position = cv2.perspectiveTransform(mean_absolute_position.reshape(-1,1,2), transformation_from_abs).reshape((2,))

    player_color = PLAYER_COLORS[tracked_player.get_player_id() % len(PLAYER_COLORS)]
    player_format = player_color + 'o'

    #plot the absolute position
    ax_absolute_image.plot(mean_absolute_position[0], mean_absolute_position[1], player_format, markersize=10)

    if np.linalg.norm(mean_absolute_velocity) >= MIN_ARROW_LENGTH:
        ax_absolute_image.arrow(mean_absolute_position[0], mean_absolute_position[1], mean_absolute_velocity[0], mean_absolute_velocity[1], head_width=15, head_length=20, fc='k', ec='b')
        
    draw_player_particles(player_particles, ax_absolute_image, ax_image, transformation_from_abs)

    #plot the position on the image
    ax_image.plot(mean_image_position[0], mean_image_position[1], player_format, markersize=10, zorder=2)

    return

def draw_expected_obstruction_masks(image, tracked_players, frame_index):
    obstruction_mask = np.zeros(image.shape[:2], np.float32)
    
    for tracked_player in tracked_players:
        if tracked_player._saved_player_states[frame_index] == TrackedPlayer.DORMANT:
            continue

        obstruction_mask = get_union(obstruction_mask, tracked_player._saved_expected_obstruction_masks[frame_index])

    #now blend the obstruction mask with the image
    image = np.float32(image)
    blue = np.zeros(image.shape, np.float32)
    blue[:,:,0] = 255
    image_with_blue = mix_images_with_mask(blue, image, obstruction_mask)
    alpha = 0.2
    image = np.uint8((alpha * image_with_blue) + ((1-alpha) * image))

    return image

def get_visualization_for_frame(player_tracker, frame_index, absolute_image):
    '''
    For each tracked player in the player tracker, we draw a circle at each of the player particles in this frame.
    We do this for both the absolute image and frame image, and then stack the two on top of each other.
    '''

    image = player_tracker._images[frame_index]
    absolute_image = absolute_image
    camera_matrix = player_tracker._camera_matrices[frame_index]
    transformation_to_abs = player_tracker._transformations_to_abs[frame_index]
    transformation_from_abs = np.linalg.inv(transformation_to_abs)

    #first draw the expected obstruction masks on the image
    image = draw_expected_obstruction_masks(image, player_tracker._all_players(), frame_index)
    
    #get axes to draw on
    fig_absolute_image, ax_absolute_image = get_axis_on_image(absolute_image)
    fig_image, ax_image = get_axis_on_image(image)

    #draw the camera position on the absolute image
    #camera_position = camera_matrix.get_camera_position()
    #ax_absolute_image.plot(camera_position[0], camera_position[1], 'ko', markersize=20)
    
    for tracked_player in player_tracker._all_players():
        get_visualization_for_player(tracked_player, frame_index, transformation_from_abs, ax_absolute_image, ax_image)

    #now convert the figure objects to images
    absolute_image = convert_figure_to_image(fig_absolute_image)
    image = convert_figure_to_image(fig_image)
    

    #now stack the image and absolute image
    frame_visualization = stack_images((image, absolute_image), 0)
    
    return frame_visualization

            
def generate_player_tracker_visualization(output_dir, player_tracker, absolute_image):
    '''
    Generate a visualization for each frame, and save the results to the output directory.
    '''

    number_of_frames = len(player_tracker._images)
    
    for frame_index in range(number_of_frames):
        visualization_for_frame = get_visualization_for_frame(player_tracker, frame_index, absolute_image)

        #save the visualization to the output directory
        output_path = ('%s/img_%3d.png' % (output_dir, frame_index)).replace(' ','0')
        print 'saving image to: ', output_path
        cv2.imwrite(output_path, visualization_for_frame)
