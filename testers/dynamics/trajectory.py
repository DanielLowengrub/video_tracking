import numpy as np
from ...source.dynamics.trajectory import Trajectory
from ...source.dynamics.movement_filter.player_movement_filter import PlayerMovementFilter

###################
# This is a tester for source/dynamics/trajectory
#
# We test the various methods of the Trajectory class.

def containment_test():
    max_speed = 20
    bmf = PlayerMovementFilter()

    frame_indices_1 = [0, 1, 2, 4, 5, 8]
    frame_indices_2 = [2,5,8]
    
    anchor_points_1 = [np.array([0,0]) for frame_index in frame_indices_1]
    anchor_points_2 = [np.array([0,0.0001]) for frame_index in frame_indices_2]

    trajectory_1 = Trajectory(bmf, max_speed, frame_indices_1, anchor_points_1)
    trajectory_2 = Trajectory(bmf, max_speed, frame_indices_2, anchor_points_2)

    print 'checking containment...'
    print trajectory_1.contains_other(trajectory_2)
    
def basic_test():
    #get some sample ball positions
    max_speed = 20
    n_obs = 2*30
    bmf = PlayerMovementFilter()
    initial_position = np.array([0,0])
    initial_velocity = np.array([5, 2])
    sample_positions = bmf.generate_sample_positions(n_obs, initial_position, initial_velocity, low_transition_cov=True)

    print 'the sample positions are:'
    print sample_positions

    frame_indices = [0, 1, 2, 10, 11, 12, 25, 26, 27, 55, 56, 57]
    anchor_points = [sample_positions[frame_index] for frame_index in frame_indices]

    trajectory = Trajectory(bmf, max_speed, frame_indices[:2], anchor_points[:2])

    for new_frame_index in frame_indices[2:]:
        print 'the frame indices are: ', trajectory._anchor_frame_indices
        print 'the anchor points are: ', trajectory._anchor_points
        print 'the maximum residue is: ', trajectory.get_max_residue()
        print 'the positions of this trajectory are:'
        print trajectory.get_actual_positions()
        print 'the velocities are:'
        print trajectory.get_actual_velocities()
        print 'trajectory valid: ', trajectory.is_valid_trajectory()
        
        new_anchor_point = sample_positions[new_frame_index]

        print '\n\n-------------------------\n'
        print 'adding a new anchor from frame ', new_frame_index
        trajectory = trajectory.add_anchor_point(new_frame_index, new_anchor_point)

        print 'properties of new anchor'

def prediction_test():
    max_speed = 20
    n_obs = 10
    bmf = PlayerMovementFilter()
    initial_position = np.array([0,0])
    initial_velocity = np.array([5, 2])
    sample_positions = bmf.generate_sample_positions(n_obs, initial_position, initial_velocity, low_transition_cov=True)

    print 'the sample positions are:'
    print sample_positions

    frame_indices = [1, 2, 4, 5, 6]
    anchor_points = [sample_positions[frame_index] for frame_index in frame_indices]

    print 'we constructed a trajectory using the positions in the following frames as anchor point:'
    print frame_indices
    
    trajectory = Trajectory(bmf, max_speed, frame_indices, anchor_points)
    #trajectory = trajectory.pad_trajectory(5, 10)
    
    frame_to_predict = 3
    predicted_position = trajectory.predict_position_at_frame(frame_to_predict)

    print 'the trajectory has %d frames in the range (%d,%d)' % (trajectory.get_number_of_frames(), trajectory.get_first_frame_index(), trajectory.get_last_frame_index())
    print 'the actual positions are:'
    print trajectory._actual_positions
    print 'actual position at frame %d: ' % frame_to_predict, sample_positions[frame_to_predict]
    print 'predicted_position at frame %d: ' % frame_to_predict, predicted_position
    
if __name__=='__main__':
    basic_test()
    #containment_test()
    #prediction_test()
