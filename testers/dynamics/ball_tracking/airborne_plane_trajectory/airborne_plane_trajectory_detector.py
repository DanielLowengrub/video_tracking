import numpy as np
import matplotlib.pyplot as plt
import os
import pickle
import functools
from .....source.preprocessing.camera_samples_interpolator.camera_samples_interpolator import CameraSamplesInterpolator
from .....source.dynamics.ball_tracking.airborne_plane_trajectory.airborne_plane_trajectory_detector import AirbornePlaneTrajectoryDetector

FPS = 25
LINE_THRESHOLDS = (0.01, 0.01)
PARABOLA_THRESHOLDS = (0.1, 0.1)
HEIGHT_ACCELERATION_BOUNDS = (-150, -40)


def load_image_positions_and_cameras():
    # frame_interval = (951, 1000)
    # ball_trajectory_ids = (687, 713)
    # frame_interval = (923, 942)
    # ball_trajectory_ids = (656,)
    # frame_interval = (957, 973)
    # ball_trajectory_ids = (687, 713)
    # frame_interval = (1040,1054) #one bounce 
    # ball_trajectory_ids = (746,)
    # frame_interval = (1040,1079) #two bounces
    # ball_trajectory_ids = (746,757)
    # frame_interval = (670, 709) #GROUND??
    # ball_trajectory_ids = (455,)
    # frame_interval = (1564, 1600) #GROUND??
    # ball_trajectory_ids = (926,)
    # frame_interval = (1736, 1744)
    # ball_trajectory_ids = (979,)
    # frame_interval = (1056, 1076)
    # ball_trajectory_ids = (757,)
    # frame_interval = (1150, 1190) #GROUND
    # ball_trajectory_ids = (807,)
    # frame_interval = (1243, 1266) 
    # ball_trajectory_ids = (826,)
    # frame_interval = (49,68)       #GROUND
    # ball_trajectory_ids = (5,)
    # frame_interval = (124,170)       #GROUND - change direction
    # ball_trajectory_ids = (12,54)
    # frame_interval = (235,275)     #GROUND - change direction
    # ball_trajectory_ids = (79,95)
    # frame_interval = (1007, 1030)
    # ball_trajectory_ids = (739,)

    # #load the ball image positions from tracked ball data
    # tracked_balls_file = os.path.join('/Users/daniel/Documents/soccer/output/postprocessing/data',
    #                                   'images-8_15-9_31/no_ball_trajectory_overlaps/tracked_balls_0_1874.pckl')

    frame_interval = (957, 973)
    ball_trajectory_ids = (686, 690)

    # frame_interval = (957, 997)
    # ball_trajectory_ids = (686, 690, 727)

    tracked_balls_file = os.path.join('/Users/daniel/Documents/soccer/output/ball_tracker/data',
                                      'images-8_15-9_31/tracked_balls_0_1874.pckl')
    


    f = open(tracked_balls_file, 'rb')
    tracked_balls = pickle.load(f)
    f.close()

    tracking_success_masks = dict()
    tracking_success_masks[686] = np.array([1,1,1,0,1,1,1,1,0,0,0,1,1,1,1], np.bool)
    tracking_success_masks[690] = np.array([1,1,0,0,1,1,1,1,1], np.bool)
    tracking_success_masks[727] = np.ones((21,), np.bool)
    enumerated_image_positions = np.vstack([tracked_balls.get_enumerated_image_positions(bt_id)#[tracking_success_masks[bt_id]]
                                            for bt_id in ball_trajectory_ids])
    
    print 'total number of image positions: ', len(enumerated_image_positions)
    print 'enumerated image positions:'
    print enumerated_image_positions
    
    #remove the image positions that come before the frame interval, and the ones that come after
    in_frame_interval = np.logical_and(enumerated_image_positions[:,0] >= frame_interval[0],
                                       enumerated_image_positions[:,0] <= frame_interval[1])
    enumerated_image_positions = enumerated_image_positions[in_frame_interval]
    enumerated_image_positions[:,0] -= frame_interval[0]
    
    print 'enumerated image positions in interval:'
    print enumerated_image_positions

    
    #load the cameras in the frame interval
    camera_directory = '/Users/daniel/Documents/soccer/output/preprocessing/data/images-8_15-9_31/camera'
    indexed_cameras = CameraSamplesInterpolator.load_cameras(camera_directory)
    cameras = list(next(indexed_cameras)[1])[frame_interval[0] : frame_interval[1]+1]

    return frame_interval[0], enumerated_image_positions, cameras

def test_detect_trajectory():
    start_frame, enumerated_image_positions, cameras = load_image_positions_and_cameras()

    td = AirbornePlaneTrajectoryDetector(FPS, LINE_THRESHOLDS, PARABOLA_THRESHOLDS, HEIGHT_ACCELERATION_BOUNDS)
    plane_trajectory = td.detect_trajectory(0, start_frame, enumerated_image_positions, cameras)

    print 'found the plane trajectory.'

    print 'image position errors:'
    print np.linalg.norm(enumerated_image_positions[:,1:] - plane_trajectory.get_image_positions(), axis=1)

if __name__ == '__main__':
    test_detect_trajectory()
      

