import numpy as np
import matplotlib.pyplot as plt
from numpy import ma
from ....source.dynamics.movement_filter.player_movement_filter import PlayerMovementFilter

####################
# This is a tester fot source/dynamics/movement_filter/player_movement_filter.py
#
# We generate some sample data, and then use the player movement filter to try and predict it.
###################

DELTA_T = 1/ 30.0
def basic_test():
    pmf = PlayerMovementFilter()

    n_obs_1 = 12
    initial_position_1 = np.array([0,0])
    initial_velocity_1 = np.array([50, 50])
    sample_positions_1 = pmf.generate_sample_positions(n_obs_1, initial_position_1, initial_velocity_1, low_transition_cov=False)

    sample_positions_2 = np.array([initial_position_1 + t * DELTA_T * initial_velocity_1 for t in range(n_obs_1)])
    sample_positions_2[-2] += np.array([0, -3])
    #sample_positions_2[-3] += np.array([0, -4])
    #sample_positions_2[-4] += np.array([0, -2])
    #sample_positions = np.vstack([sample_positions_1, sample_positions_2])
    sample_positions = sample_positions_2

    #mask out some of the sample positions
    #unmasked_positions = [0, 1, 2, 10, 11, 20, 21, 22, 29]
    #unmasked_positions = [0]
    unmasked_positions = range(n_obs_1 - 1)
    sample_positions_masked = ma.array(sample_positions)
    for i in range(len(sample_positions)):
        if i not in unmasked_positions:
            sample_positions_masked[i] = ma.masked

    
    print 'sample_positions: '
    print sample_positions

    print 'sample_positions masked: '
    print sample_positions_masked

    smoothed_positions, covariances = pmf.get_positions_from_observations(sample_positions_masked)

    print 'smoothed positions:'
    print smoothed_positions

    print 'covariances:'
    print covariances

    residues = np.array([np.linalg.norm(smoothed_positions[i] - sample_positions[i]) for i in range(len(smoothed_positions)) if i in unmasked_positions])
    
    print 'residue:'
    print residues

    print 'max residue:'
    print max(residues)
    
    plt.plot(sample_positions[:,0], sample_positions[:,1], 'bo')
    plt.plot(smoothed_positions[:,0], smoothed_positions[:,1], 'k+')
    #plt.xlim([0,50])
    #plt.ylim([0,50])
    plt.show()
    
if __name__ == '__main__':
    
    basic_test()
