import os
import numpy as np
import cv2

from .....source.dynamics.player_tracking.player_model.rectangle_player_model import RectanglePlayerModel

#######
# This is a script which generates sample data which can be used for testing the 2D player tracker.
#
# The parameters include:
#     - the size of the absolute field and of the images
#     - the homography matrix from absolute coordinates to image coordinates
#     - initial positions and velocities of the players
#     - the image we will use to draw the players
#
# The output is of the following form:
#     - an array with shape (number of frames, number of players, 2) containing the absolute player positions
#     - a numpy array with the size of the absolute field
#     - the homography matrix from last to abs
#     - a directory containing images with the players
#

OUTPUT_DIR = 'test_data/player_tracker/test_data_2D_1'

ABSOLUTE_FIELD_SHAPE = (400, 500, 3)
IMAGE_SHAPE = (400, 500, 3)
HOMOGRAPHY_TO_ABS = np.array([[1, 0, 0],
                                [0, 1, 0],
                                [0.005, 0, 1]])
HOMOGRAPHY_FROM_ABS = np.linalg.inv(HOMOGRAPHY_TO_ABS)

NUM_FRAMES = 10
#INITIAL_ABSOLUTE_POSITIONS = [np.array([30, 100]), np.array([30, 200]), np.array([100, 100]), np.array([110, 110])]
#INITIAL_ABSOLUTE_VELOCITIES = [np.array([50, 300]), np.array([50, -300]), np.array([50, -200]), np.array([10, -200])]
INITIAL_ABSOLUTE_POSITIONS = [np.array([100, 100]), np.array([110, 110])]
INITIAL_ABSOLUTE_VELOCITIES = [np.array([50, -200]), np.array([10, -200])]

ABSOLUTE_ACCELERATIONS = [ [np.zeros(2) for t in range(NUM_FRAMES)] for p in range(len(INITIAL_ABSOLUTE_POSITIONS))]
#set some specific accelerations
#ABSOLUTE_ACCELERATIONS[0][2] = np.array([0, -200])
#ABSOLUTE_ACCELERATIONS[0][3] = np.array([0, -200])
#ABSOLUTE_ACCELERATIONS[0][4] = np.array([0, -200])
#ABSOLUTE_ACCELERATIONS[0][5] = np.array([0, -200])
#ABSOLUTE_ACCELERATIONS[0][6] = np.array([0, -200])

accel = np.array([200, 0])
ABSOLUTE_ACCELERATIONS[0] = NUM_FRAMES * [accel]

DELTA_T = 1.0 / 30

PLAYER_IMAGE_NAME  = 'test_data/person.png'

OUTPUT_IMAGE_DIR =             os.path.join(OUTPUT_DIR, 'images')
OUTPUT_PLAYER_POSITIONS =      os.path.join(OUTPUT_DIR, 'player_positions.npy')
OUTPUT_ABSOLUTE_FIELD_SHAPE =  os.path.join(OUTPUT_DIR, 'absolute_field_shape.npy')
OUTPUT_HOMOGRAPHY_FROM_ABS =   os.path.join(OUTPUT_DIR, 'homography_from_abs.npy')


def get_player_model(player_image_name):
    '''
    Returns the player model we will use to draw th players on the images
    '''
    player_image = cv2.imread(player_image_name, 1)
    gs_player_image = cv2.cvtColor(player_image, cv2.COLOR_BGR2GRAY)
    retval, player_foreground_mask = cv2.threshold(gs_player_image, 5, 1, cv2.THRESH_BINARY)
    player_foreground_mask = np.float64(player_foreground_mask)

    player_model =  RectanglePlayerModel(player_image, player_foreground_mask)

    return player_model

def get_positions_of_player(initial_position, initial_velocity, accelerations, delta_t, number_of_frames, homography_from_abs):
    #print 'initial position: ', initial_position
    #print 'initial_velocity: ', initial_velocity
    #print 'accelerations: ', accelerations
    #print 'delta_t: ', delta_t
    #print 'number of frames: ', number_of_frames
    
    absolute_positions = [initial_position]
    absolute_velocities = [initial_velocity]

    #print 'accelerations: ', accelerations
    #print 'accelerations length: ', len(accelerations)
    
    for t in range(1,number_of_frames):
        print 't = %d' % t
        #print 'computing frame %d' % t
        #print 'position at time %d: ' % (t-1), absolute_positions[t-1]
        #print 'velocity at time %d: ' % (t-1), absolute_velocities[t-1]
        new_position = absolute_positions[t-1] + delta_t * absolute_velocities[t-1]
        new_velocity = absolute_velocities[t-1] + delta_t * accelerations[t]
        #print 'new position: ', new_position
        #print 'new velocity: ', new_velocity
        
        absolute_positions.append(new_position)
        absolute_velocities.append(new_velocity)

    print 'positions: ', absolute_positions
    print 'velocities: ', absolute_velocities
    positions_on_image = cv2.perspectiveTransform(np.float32(absolute_positions).reshape(-1,1,2), homography_from_abs).reshape((-1,2))

    return absolute_positions, positions_on_image

def get_positions_of_all_players(initial_absolute_positions, initial_absolute_velocities, absolute_accelerations,
                                 delta_t, number_of_frames, homography_from_abs):
    '''
    Return a tuple (absolute positions, positions on images) where 
    absolute_positions is a list whose length is the number of frames. For each frame t, absolute_positions[t] is a list of the positions of players at that time.
    positions_on_images is similar, except that it stores the positions of the players in image coordinates.
    '''
    num_players = len(initial_absolute_positions)
    
    #get the list of position sequences, one for each player. 
    position_sequences = [get_positions_of_player(initial_absolute_positions[i], initial_absolute_velocities[i], absolute_accelerations[i],
                                                  delta_t, number_of_frames, homography_from_abs)
                          for i in range(num_players)]

    #for each frame, create a list of positions present in that frame
    positions_on_images = [[position_sequences[p][1][frame_index]
                            for p in range(num_players)]
                           for frame_index in range(number_of_frames)]  

    #for each frame, create a list of the absolute positions
    absolute_positions = [[position_sequences[p][0][frame_index]
                           for p in range(num_players)]
                          for frame_index in range(number_of_frames)]  

    return absolute_positions, positions_on_images

def get_images(image_size, positions_on_images, player_model):
    '''
    Return a list of images of the given shape, where we use the player model to draw players in the given positions.
    '''
    base_image = np.zeros(image_size, np.uint8)
    images = []
    
    for positions_on_image in positions_on_images:
        #sort the players from back to front
        positions_on_image.sort(key=lambda x: x[1])
        image = base_image.copy()

        #draw players in each of the positions
        for position in positions_on_image:
            player_model.draw_on_image(image, position)

        images.append(image)

    return images

def generate_and_save_data():
    '''
    Use the input parameters to generate a sequence of images and absolute positions, then
    save them in the format described in the beginning of the file.
    '''

    #first get the absolute positions and image positions
    absolute_positions, positions_on_images = get_positions_of_all_players(INITIAL_ABSOLUTE_POSITIONS, INITIAL_ABSOLUTE_VELOCITIES, ABSOLUTE_ACCELERATIONS,
                                                                           DELTA_T, NUM_FRAMES, HOMOGRAPHY_FROM_ABS)

    #get the player model
    player_model = get_player_model(PLAYER_IMAGE_NAME)
    
    #use these to generate the images
    images = get_images(IMAGE_SHAPE, positions_on_images, player_model)

    #save the images
    os.mkdir(OUTPUT_IMAGE_DIR)
    for i, image in enumerate(images):
        output_path = ('%s/img_%3d.png' % (OUTPUT_IMAGE_DIR, i)).replace(' ','0')
        cv2.imwrite(output_path, image)

    #save the absolute player positions. to do this, turn the list of absolute positions into a 3D numpy array.
    #we first stack up the list of absolute positions,
    #and then reshape so that the shape is (number of frames, number of players, 2)
    num_players = len(INITIAL_ABSOLUTE_POSITIONS)
    output_absolute_positions = np.vstack(absolute_positions).reshape(-1, num_players, 2)
    np.save(OUTPUT_PLAYER_POSITIONS, output_absolute_positions)

    #save the absolute field shape
    absolute_field_shape = np.array(ABSOLUTE_FIELD_SHAPE)
    np.save(OUTPUT_ABSOLUTE_FIELD_SHAPE, absolute_field_shape)

    #save the homography from abs
    np.save(OUTPUT_HOMOGRAPHY_FROM_ABS, HOMOGRAPHY_FROM_ABS)
    
if __name__ == '__main__':
    generate_and_save_data()
