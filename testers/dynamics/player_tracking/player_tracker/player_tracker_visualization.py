import numpy as np
import cv2
import os

###
# Here we have code to visualize the behaviour of the basic 2D player tracker.
#
# We get as input a PlayerTracker object, and output a sequence of images showing how the important
# internal variables are updated.
#
# Specifically, for each frame we output an image of the following form:
# There are three images stacked ontop of each other.
# - The top one contains the original frame. Each tracked player is marked by a dot, together with a number. We also draw an outline of the possible balls mask.
# - The middle one contains numbered texture masks and foreground masks, one per player.
# - The bottom one is the absolute image, together with numbered points in the tracked player positions and possible balls masks.

#OUTPUT_DIR = 'player_tracker_visualization_1'

FONT_TYPE = cv2.FONT_HERSHEY_COMPLEX_SMALL
FONT_SCALE = 1.0
FONT_COLOR = (0, 255, 0)
LINE_THICKNESS = 2
LINE_TYPE = cv2.CV_AA

CONTOUR_COLORS = [(255,255,255), (255,0,0)]

def stack_images(images, axis, border_size=2):
    '''
    Reshape the images so that they are all the same shape in the coordinates that are not the axis, then stack them on the axis coordinate.
    '''
    #if the axis is 0, other should be 1. If the axis is 1, other axis should be 0.
    other_axis = 1 - axis
    max_length_on_other_axis = max(image.shape[other_axis] for image in images)

    padded_images = []

    for image in images:
        extra_on_other_axis = max_length_on_other_axis - image.shape[other_axis]

        padding_shape = list(image.shape)
        padding_shape[other_axis] = extra_on_other_axis
        padding = np.zeros(padding_shape)
        padded_image = np.concatenate((image, padding), axis=other_axis)

        border_shape = list(padded_image.shape)
        border_shape[axis] = border_size
        border = np.ones(border_shape) * 255
        padded_image_with_border = np.concatenate((padded_image, border), axis=axis)
        
        padded_images.append(padded_image_with_border)

    return np.concatenate(tuple(padded_images), axis=axis)

def draw_tracked_player_on_image_and_abs(tracked_player, image, abs_image, transformation_to_abs, frame_index):
    '''
    Draw the tracked player on both the image and the absolute image. Specifically, on each image we draw:
    - the position
    - a contour outlining the positions we searched in.
    '''
    #if this frame index is beyond the frame for which the player has been tracked, do nothing.
    if frame_index >= len(tracked_player.get_absolute_positions()):
        return

    transformation_from_abs = np.linalg.inv(transformation_to_abs)
    absolute_position = tracked_player.get_absolute_positions()[frame_index]
    position_on_image = cv2.perspectiveTransform(np.float32(absolute_position).reshape(-1,1,2), transformation_from_abs).reshape((2,))
    best_match_position = tracked_player._best_match_positions[frame_index]
    
    #possible_image_positions_contour = tracked_player._possible_image_positions_contours[frame_index]
    #possible_absolute_positions_contour= tracked_player._possible_absolute_positions_contours[frame_index]

    #if the possible_image_positions_contour exists, draw it in green
    #if possible_image_positions_contour is not None:
    #    cv2.drawContours(image, [possible_image_positions_contour], -1, (0,0,200), 1)

    #if the possible_absolute_positions_mask exists, draw it in green
    #if possible_absolute_positions_contour is not None:
    #    cv2.drawContours(abs_image, [possible_absolute_positions_contour], -1, (0,0,200), 1)

    image_position_probabilities = tracked_player._image_position_probabilities[frame_index]
    absolute_center_and_error = tracked_player._absolute_centers_and_errors[frame_index]
    head_position = tracked_player._head_positions[frame_index]
    
    #if the image position probabilities exist, draw a contour around them with one of the colors in CONTOUR_COLORS
    if image_position_probabilities is not None:
        #in all of the places where the probability is non zero, draw an outline of the positions with positive probability
        positive_probabilities = image_position_probabilities.copy()
        positive_probabilities[positive_probabilities > 0] = 1.0
        positive_probabilities = np.uint8(positive_probabilities)
        cv_contours, hierarchy = cv2.findContours(positive_probabilities, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
        contour_color = CONTOUR_COLORS[tracked_player.get_player_id() % len(CONTOUR_COLORS)]
        cv2.drawContours(image, cv_contours, -1, contour_color, 2)
        #image[image_position_probabilities > 0] = image_position_probabilities[image_position_probabilities > 0].reshape((-1,1)) * np.array([255,255,255])

    #if there is an absolute center and error, draw it as a red circle
    if absolute_center_and_error is not None:
        center = absolute_center_and_error[0]
        radius = absolute_center_and_error[1]

        print 'center: ', center
        print 'radius: ', radius
        cv2.circle(abs_image, tuple(np.int32(center)), radius, (0, 0 ,200), 1)
        
    #draw the position on the image in red
    cv2.circle(image, tuple(np.int32(position_on_image)), 3, (0, 0 ,255), -1)

    #if it exists, draw the best match position in green, and a white line between it and the image position.
    if best_match_position is not None:
        best_match_color = CONTOUR_COLORS[tracked_player.get_player_id() % len(CONTOUR_COLORS)]
        cv2.circle(image, tuple(np.int32(best_match_position)), 2, best_match_color, -1)
        cv2.line(image, tuple(np.int32(position_on_image)), tuple(np.int32(best_match_position)), (255, 255, 255), 1)

    if head_position is not None:
        head_color = CONTOUR_COLORS[tracked_player.get_player_id() % len(CONTOUR_COLORS)]
        cv2.circle(image, tuple(np.int32(head_position)), 2, head_color, -1)

    #draw the player index in green
    text_position = tuple(np.int32(position_on_image) + np.array([25, 0]))
    cv2.putText(image, str(tracked_player.get_player_id()), text_position, FONT_TYPE, FONT_SCALE, FONT_COLOR, LINE_THICKNESS, LINE_TYPE)
    
    #draw the position on the absolute image in red
    cv2.circle(abs_image, tuple(np.int32(absolute_position)), 3, (0, 0 ,255), -1)
    #draw the player index in green
    text_position = tuple(np.int32(absolute_position) + np.array([5, -5]))
    cv2.putText(abs_image, str(tracked_player.get_player_id()), text_position, FONT_TYPE, FONT_SCALE, FONT_COLOR, LINE_THICKNESS, LINE_TYPE)
    
def get_player_model_image(tracked_player, frame_index):
    '''
    Return an image describing the state of the player tracker at the given frame. Specifically,
    we stack the texture map on top of the foreground mask.
    '''

    print 'getting player model for player %d at frame %d.' % (tracked_player.get_player_id(), frame_index)
    
    #if this frame index is beyond the frame for which the player has been tracked, do nothing.
    if frame_index >= len(tracked_player.get_absolute_positions()):
        return None

    texture_map = tracked_player._player_model._texture_maps[frame_index]
    foreground_mask = tracked_player._player_model._foreground_masks[frame_index]

    #convert the fg mask to color/uint8 so we can save it as an image
    fg_shape = foreground_mask.shape
    foreground_mask = foreground_mask.reshape((fg_shape[0], fg_shape[1], 1)) * np.ones((fg_shape[0], fg_shape[1], 3))
    foreground_mask = np.uint8(foreground_mask * 255.0)

    text_padding_shape = list(texture_map.shape)
    text_padding_shape[0] = 20
    text_padding = np.zeros(text_padding_shape)
    text_position = (5, 15)
    cv2.putText(text_padding, str(tracked_player.get_player_id()), text_position, FONT_TYPE, FONT_SCALE, FONT_COLOR, LINE_THICKNESS, LINE_TYPE)
    
    stacked_images = np.vstack([text_padding, texture_map, foreground_mask])
    
    return stacked_images
        

def save_debugging_data(tracked_player, frame_index):

    #if this is the first frame, save data that is not frame dependent
    if frame_index == 0:
        #save a list of the probabilities of the best match
        probability_of_best_match = np.array(tracked_player._probability_of_best_match, dtype=np.float)
        output_path = ('output/tester/player_tracker/general_data_dump/player_%d/probability_of_best_match' % (tracked_player.get_player_id(),))
        np.save(output_path, probability_of_best_match)

        #save a list of absolute positions
        #absolute_positions = tracked_player.get_absolute_positions_npy()
        #output_path = ('output/tester/player_tracker/general_data_dump/player_%d/absolute_positions' % (tracked_player.get_player_id(),))
        #np.save(output_path, absolute_positions)

        #save a list of head image positions
        #head_image_positions = tracked_player.get_head_image_positions_npy()
        #output_path = ('output/tester/player_tracker/general_data_dump/player_%d/head_image_positions' % (tracked_player.get_player_id(),))
        #np.save(output_path, head_image_positions)
        

                       
    if frame_index >= len(tracked_player._obstruction_masks):
        print 'the frame index is out of bounds.'
        return
    
    #debugging_data = tracked_player._debugging_data[frame_index]

    #if debugging_data is None:
    #    return
        
    ##we write the debugging data
    #output_path = ('output/tester/player_tracker/general_data_dump/player_%d/image_%3d.png' % (tracked_player.get_player_id(), frame_index)).replace(' ','0')
    #print 'saving image to: ', output_path
    #cv2.imwrite(output_path, np.uint8(255 * debugging_data))

    ##we save the foreground mask and texture map
    foreground_mask = tracked_player._player_model._foreground_masks[frame_index]
    if foreground_mask is not None:
        output_path = ('output/tester/player_tracker/general_data_dump/player_%d/fg_mask_%3d.png' % (tracked_player.get_player_id(), frame_index)).replace(' ','0')
        #print 'saving foreground mask to: ', output_path
        cv2.imwrite(output_path, np.uint8(255 * foreground_mask))

        
    texture_map = tracked_player._player_model._texture_maps[frame_index]
    if texture_map is not None:
        output_path = ('output/tester/player_tracker/general_data_dump/player_%d/texture_map_%3d.png' % (tracked_player.get_player_id(), frame_index)).replace(' ','0')
        #print 'saving texture map to: ', output_path
        cv2.imwrite(output_path, texture_map)

    #we also save the obstruction mask
    obstruction_mask = tracked_player._obstruction_masks[frame_index]
    if obstruction_mask is not None:
        #we write the debugging data
        output_path = ('output/tester/player_tracker/general_data_dump/player_%d/obstruction_mask_%3d.png' % (tracked_player.get_player_id(), frame_index)).replace(' ','0')
        #print 'saving obstruction mask to: ', output_path
        cv2.imwrite(output_path, np.uint8(255 * obstruction_mask))

    #we also save the valid positions mask
    valid_positions_mask = tracked_player._valid_positions_masks[frame_index]
    if valid_positions_mask is not None:
        #we write the debugging data
        output_path = ('output/tester/player_tracker/general_data_dump/player_%d/valid_positions_mask_%3d.png' % (tracked_player.get_player_id(), frame_index)).replace(' ','0')
        cv2.imwrite(output_path, np.uint8(255 * valid_positions_mask))

    #we also save the histogram
    histogram = tracked_player._player_model._histograms[frame_index]
    if histogram is not None:
        #we write the debugging data
        output_path = ('output/tester/player_tracker/general_data_dump/player_%d/histogram_%3d' % (tracked_player.get_player_id(), frame_index)).replace(' ','0')
        #print 'saving histogram mask to: ', output_path
        np.save(output_path, histogram)

    return

def get_visualization_for_frame(player_tracker, frame_index, abs_image_shape, actual_absolute_positions_in_frame=None):
    '''
    Return the visualization of the tracker for the given frame.
    '''

    image = player_tracker._images[frame_index].copy()
    abs_image = np.zeros(abs_image_shape, np.uint8)
    
    #if there are actual position to draw, draw a big white dot on the actual positions
    if actual_absolute_positions_in_frame is not None:
        for actual_absolute_position in actual_absolute_positions_in_frame:
            cv2.circle(abs_image, tuple(np.int32(actual_absolute_position)), 5, (255, 255, 255), -1)

    transformation_to_abs = player_tracker._transformations_to_abs[frame_index]

    #draw all of the tracked players on both the image and the abs_image
    for tracked_player in player_tracker.get_tracked_players():
        draw_tracked_player_on_image_and_abs(tracked_player, image, abs_image, transformation_to_abs, frame_index)

    #now get the player model images for all the players
    player_model_images = [] 
    for tracked_player in player_tracker.get_tracked_players():
        player_model_image = get_player_model_image(tracked_player, frame_index)
        if player_model_image is not None:
            player_model_images.append(player_model_image)

    #stack the player model images side by side
    player_model_visualization = stack_images(player_model_images, 1)

    #THIS IS A DEBUGGING HACK
    for tracked_player in player_tracker.get_tracked_players():
        save_debugging_data(tracked_player, frame_index)

    #stack the image, player model visualization, and absolute image vertically
    return stack_images((image, player_model_visualization, abs_image), 0)

def create_debugging_data_directories(player_tracker):
    '''
    For each player id in the list of tracked players, create a directory player_{player id} in the directory
    output/tester/player_tracker/general_data_dump
    '''

    data_dump_dir = 'output/tester/player_tracker/general_data_dump'
    current_player_directories =  os.listdir(data_dump_dir)

    for tracked_player in player_tracker.get_tracked_players():
        player_dir = ('player_%d' % tracked_player.get_player_id())
        if player_dir not in current_player_directories:
            os.mkdir(os.path.join(data_dump_dir, player_dir))

    return
        
def generate_player_tracker_visualization(output_dir, player_tracker, abs_image_shape, actual_absolute_positions=None):
    '''
    Generate a visualization for each frame, and save the results to the output directory.
    '''

    number_of_frames = len(player_tracker._images)

    create_debugging_data_directories(player_tracker)
    
    for frame_index in range(number_of_frames):
        actual_absolute_positions_in_frame = None
        if actual_absolute_positions is not None:
            actual_absolute_positions_in_frame = actual_absolute_positions[frame_index]
            
        visualization_for_frame = get_visualization_for_frame(player_tracker, frame_index, abs_image_shape, actual_absolute_positions_in_frame)

        #save the visualization to the output directory
        output_path = ('%s/img_%3d.png' % (output_dir, frame_index)).replace(' ','0')
        print 'saving image to: ', output_path
        cv2.imwrite(output_path, visualization_for_frame)
