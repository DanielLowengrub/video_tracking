import os
import cv2
import numpy as np
from .....source.dynamics.player_tracking.player_tracker.player_tracker import PlayerTracker
from .....source.data_structures.frame_sequence.frame_sequence import FrameSequence
from .player_tracker_visualization import generate_player_tracker_visualization

##########
# In this script we load a frame sequence and feed it to the PlayerTracker,
# We then use player_tracker_visualization to output a visualization of the algorithm at work.
#########

#OUTPUT_DIRECTORY = 'output/tester/player_tracker/player_positions/frame_sequence_50_79-0_29/'
#DEBUGGING_OUTPUT_DIRECTORY = 'output/tester/player_tracker/frame_sequences/frame_sequence_50_79-0_29/'
#FRAME_SEQUENCE_DIR = 'output/tester/frame_sequences/frame_sequence_test_3/frame_sequences_50_79-0_29'

#OUTPUT_DIRECTORY = 'output/tester/player_tracker/player_positions/frame_sequence_148_150-0_2/'
#DEBUGGING_OUTPUT_DIRECTORY = 'output/tester/player_tracker/frame_sequences/frame_sequence_148_150-0_2/'
#FRAME_SEQUENCE_DIR = 'output/tester/frame_sequences/frame_sequence_test_4/frame_sequences_148_150-0_2'

#OUTPUT_DIRECTORY = 'output/tester/player_tracker/frame_sequences/frame_sequence_50_109-0_59/'
#FRAME_SEQUENCE_DIR = 'output/tester/frame_sequences/frame_sequence_test_5/frame_sequences_50_109-0_59'

#OUTPUT_DIRECTORY = 'output/tester/player_tracker/frame_sequences/frame_sequence_50_148-0_98/'
#FRAME_SEQUENCE_DIR = 'output/tester/frame_sequences/frame_sequence_test_6/frame_sequences_50_148-0_98'

#OUTPUT_DIRECTORY = 'output/tester/player_tracker/player_positions/frame_sequence_50_199-0_149/'
#DEBUGGING_OUTPUT_DIRECTORY = 'output/tester/player_tracker/frame_sequences/frame_sequence_50_199-0_149/'
#FRAME_SEQUENCE_DIR = 'output/tester/frame_sequences/frame_sequence_test_7/frame_sequences_50_199-0_149'

OUTPUT_DIRECTORY = 'output/tester/player_tracker/player_positions/frame_sequence_50_269-0_219/'
DEBUGGING_OUTPUT_DIRECTORY = 'output/tester/player_tracker/frame_sequences/frame_sequence_50_269-0_219/'
FRAME_SEQUENCE_DIR = 'output/tester/frame_sequences/frame_sequence_test_8/frame_sequences_50_269-0_219'

ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'

def save_player_data(player_tracker):
    player_positions = player_tracker.get_player_positions()
    head_image_positions = player_tracker.get_head_image_positions()

    player_positions_file = os.path.join(OUTPUT_DIRECTORY, 'player_positions')
    head_image_positions_file = os.path.join(OUTPUT_DIRECTORY, 'head_image_positions')

    np.save(player_positions_file, player_positions)
    np.save(head_image_positions_file, head_image_positions)

    return

def test_player_tracker():
    #first load the frame sequence
    frame_sequence = FrameSequence.load_frame_sequence(FRAME_SEQUENCE_DIR, load_homography_to_abs=True, load_camera_matrix=False)
    frames = frame_sequence.get_frames()
    #create a player tracker
    images = [frame.get_image() for frame in frames]
    transformations_to_abs = [frame.get_homography_to_abs() for frame in frames]
    #camera_matrices = [frame.get_camera_matrix() for frame in frames]
    camera_matrices = [None for frame in frames]
    player_contours = [frame.get_player_contours() for frame in frames]
    foreground_masks = [frame.get_foreground_mask() for frame in frames]
    
    player_tracker = PlayerTracker(images, transformations_to_abs, camera_matrices, player_contours, foreground_masks)
    
    #track players over time
    player_tracker.track_players()

    #save the player feet and head positions
    save_player_data(player_tracker)
    
    #generate the visualization
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)
    generate_player_tracker_visualization(DEBUGGING_OUTPUT_DIRECTORY, player_tracker, absolute_image.shape)

if __name__ == '__main__':
    test_player_tracker()
