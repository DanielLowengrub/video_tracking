import os
import cv2
import numpy as np
from .....source.dynamics.player_tracking.player_tracker.player_tracker import PlayerTracker
from .....source.data_structures.contour import Contour
from .player_tracker_visualization import generate_player_tracker_visualization

##########
# In this script we load data that we generated with the test_data_generator, feed it to the PlayerTracker,
# and then use player_tracker_visualization to output a visualization of the algorithm at work.
#########

OUTPUT_DIRECTORY = 'output/tester/player_tracker/test_data/player_tracker_visualization_1'

PLAYER_DATA_DIRECTORY = 'test_data/player_tracker/test_data_2D_1'
IMAGE_DIRECTORY = os.path.join(PLAYER_DATA_DIRECTORY, 'images')
ABSOLUTE_PLAYER_POSITIONS_FILENAME = os.path.join(PLAYER_DATA_DIRECTORY, 'player_positions.npy')
ABSOLUTE_FIELD_SHAPE_FILENAME = os.path.join(PLAYER_DATA_DIRECTORY, 'absolute_field_shape.npy')
HOMOGRAPHY_FROM_ABS_FILENAME = os.path.join(PLAYER_DATA_DIRECTORY, 'homography_from_abs.npy')

def load_images():
    '''
    image_directory_absolute_path is the absolute path to the image directory
    Return a list of the images from the given directory.
    '''
    images = []
    for image_file_name in os.listdir(IMAGE_DIRECTORY):
        image_absolute_path = os.path.join(IMAGE_DIRECTORY, image_file_name)
        print 'loading image: %s' % image_absolute_path
        images.append(cv2.imread(image_absolute_path, 1))

    return images

def extract_contours_and_foreground_masks(images):
    '''
    images is a list of images.
    Return a tuple (contours, foreground_masks)
    - contours: a sequence of contours corresponding to the players in the images
    - foreground masks: a sequence of foreground masks, also corresponding to the players in the images
    '''
    contours = []
    foreground_masks = []
    
    for image in images:
        #get the greyscale version of the image.
        gs_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        retval, gs_image = cv2.threshold(gs_image, 5, 255, cv2.THRESH_BINARY)

        #get the contours
        cv_contours_in_image, hierarchy = cv2.findContours(gs_image.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        contours_in_image = [Contour(cv_contour, image) for cv_contour in cv_contours_in_image]
        
        #get the foreground mask by truncating the gs_image
        #retval, foreground_mask = cv2.threshold(gs_image, 5, 1, cv2.THRESH_BINARY)
        foreground_mask = np.float64(gs_image) / 255

        contours.append(contours_in_image)
        foreground_masks.append(foreground_mask)

    return contours, foreground_masks

def load_absolute_positions():
    '''
    Load the list of absolute positions.
    '''
    
    return np.load(ABSOLUTE_PLAYER_POSITIONS_FILENAME)

def load_absolute_field_shape():
    return np.load(ABSOLUTE_FIELD_SHAPE_FILENAME)

def load_homography_to_abs():
    homography_from_abs = np.load(HOMOGRAPHY_FROM_ABS_FILENAME)
    homography_to_abs = np.linalg.inv(homography_from_abs)

    return homography_to_abs

def test_player_tracker():
    #first load the data
    images = load_images()
    player_contours, foreground_masks = extract_contours_and_foreground_masks(images)
    absolute_positions = load_absolute_positions()
    absolute_field_shape = load_absolute_field_shape()
    homography_to_abs = load_homography_to_abs()
    transformations_to_abs = [homography_to_abs for image in images]

    #create a player tracker
    player_tracker = PlayerTracker(images, transformations_to_abs, player_contours, foreground_masks)
    
    #track players over time
    player_tracker.track_players()

    #generate the visualization
    generate_player_tracker_visualization(OUTPUT_DIRECTORY, player_tracker, absolute_field_shape, absolute_positions)
    
if __name__ == '__main__':
    test_player_tracker()
