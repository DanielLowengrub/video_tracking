import numpy as np
import cv2

from ....source.auxillary.mask_operations import mix_images_with_mask
from ....source.dynamics.player_tracking.tracked_player import TrackedPlayer
from ....source.dynamics.player_tracking.player_model.rectangle_player_model import RectanglePlayerModel

################
# Test Functions
#
# These functions use the stupid player matcher to test the tracked player implementation.
# It generates a sequence of blue rectangles moving down and to the right that we try to track.
#
# In the output, there are there are two rows of images.
# The bottom row is in absolute coordinates. In each frame there is a red dot representing the position that the tracking algorithm found and a white dot representing
# the actual position. There is also a green box specifying where we looked to find this position.
#
# The top row is in image coordinates. In each frame, the blue rectangle is the object we are tracking. The green area represents all the positions on the image that we
# searched for to find the object. The blue dot represents the best position on the image. The red dot is the position the tracking algorithm found.
#
################

def draw_sequence(images, actual_absolute_positions, tracked_player, transformation_from_abs):

    #get the data from the tracked player
    tracked_player_absolute_positions = tracked_player.get_absolute_positions()
    tracked_player_positions_on_image = cv2.perspectiveTransform(np.float32(tracked_player_absolute_positions).reshape(-1,1,2), transformation_from_abs).reshape((-1,2))
    
    image_position_probabilities = tracked_player._image_position_probabilities
    absolute_centers_and_errors = tracked_player._absolute_centers_and_errors

    best_match_positions = tracked_player._best_match_positions

    number_of_tracked_positions = tracked_player._trajectory.get_number_of_frames()
    
    #first draw the masks and positions on the images
    for i in range(number_of_tracked_positions):
        
        #draw the mask in green
        image_position_probability = image_position_probabilities[i]
        green_image = np.zeros(images[i].shape, np.uint8)
        green_image[:,:,1] = 255
        
        if image_position_probability is not None:
            images[i] = mix_images_with_mask(green_image, images[i], image_position_probability)
            
            pos = tracked_player_positions_on_image[i-1]
            cv2.circle(images[i], (int(pos[0]),int(pos[1])), 3, (255, 255 ,255), -1)
            best_match_pos = best_match_positions[i]
            cv2.circle(images[i], (int(best_match_pos[0]),int(best_match_pos[1])), 3, (255, 0, 0), -1)

        #draw the position
        pos = tracked_player_positions_on_image[i]
        print 'drawing position: ', pos
        cv2.circle(images[i], (int(pos[0]),int(pos[1])), 3, (0, 0 ,255), -1)
            
    #now draw the absolute positions on new images
    absolute_images = []
    base_abs_image = np.zeros(images[0].shape)
    for i in range(number_of_tracked_positions):
        tracked_pos = tracked_player_absolute_positions[i]
        
        abs_image = base_abs_image.copy()
        center_and_error = absolute_centers_and_errors[i]
        
        if center_and_error is not None:
            center = center_and_error[0]
            error = center_and_error[1]
            cv2.circle(abs_image, (int(center[0]),int(center[1])), error, (255, 255, 255), -1)

        actual_pos = actual_absolute_positions[i]
        cv2.circle(abs_image, (int(actual_pos[0]),int(actual_pos[1])), 5, (255, 255, 255), -1)
        cv2.circle(abs_image, (int(tracked_pos[0]),int(tracked_pos[1])), 3, (0, 0, 255), -1)
        
        #draw a border on the image
        abs_image[:,(0,-1),:2] = 255
        abs_image[(0,-1),:,:2] = 255

        absolute_images.append(abs_image)

        
    row_of_images = np.hstack(images[:number_of_tracked_positions])
    row_of_abs_images = np.hstack(absolute_images)
    
    big_image = np.vstack([row_of_images, row_of_abs_images])
    print big_image.shape
    
    cv2.imshow('the sequence of images', big_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def get_sequence_of_player_images(image_size, positions, player_model):
    
    base_image = np.zeros(image_size)
    images = []

    for position in positions:
        image = base_image.copy()
        
        #x_start = position[0]
        #y_start = position[1]

        #x_end = x_start + rectangle_size[0]
        #y_end = y_start + rectangle_size[1]
        
        #image[y_start:y_end, x_start:x_end, 0] = 255
        player_model.draw_on_image(image, position)
                        
        #add a border
        image[:,(0,-1),:2] = 255
        image[(0,-1),:,:2] = 255
        
        images.append(image)

    return images
        
def rectangle_model_test():
    num_positions = 10
    PLAYER_IMAGE_NAME = 'test_data/person.png'
    
    image_size = (400, 150, 3)
    player_image = cv2.imread(PLAYER_IMAGE_NAME, 1)

    #the foreground mask consists of all the pixels whose blue value is bigger than 0
    foreground_mask = np.float64(player_image[:,:,0] > 0)

    player_model =  RectanglePlayerModel(player_image, foreground_mask)

    #generate positions
    initial_absolute_position = np.array([30, 100])
    velocity = np.array([50, 300])
    delta_t = 1.0 / 30.0
    transformation_to_abs = np.eye(3)
    transformation_to_abs[2][0] = 0.01
    transformation_from_abs = np.linalg.inv(transformation_to_abs)
    
    absolute_positions = [initial_absolute_position + t * delta_t * velocity for t in range(num_positions)]
    positions_on_image = cv2.perspectiveTransform(np.float32(absolute_positions).reshape(-1,1,2), transformation_from_abs).reshape((-1,2))

    print 'absolute positions: '
    print np.array(absolute_positions)

    print 'positions on image:'
    print np.array(positions_on_image)
    
    #use the player model to create a sequence of test images
    images = get_sequence_of_player_images(image_size, positions_on_image, player_model)
    
    #create a tracked player object
    initial_image = images[0]
    initial_position_on_image = positions_on_image[0]
    tracked_player = TrackedPlayer(player_model, initial_image, initial_position_on_image, initial_absolute_position)

    #we now update the tracked player with our list of images
    next_transformation_to_abs = transformation_to_abs
    obstruction_mask = np.zeros(initial_image.shape[:2], np.float64)

    for next_image in images[1:]:
        update_success = tracked_player.update_position_and_obstruction_mask(next_image, next_transformation_to_abs, obstruction_mask)
        if update_success == False:
            print 'not enough data to update player. Terminating tracking'
            break
        
    #print debugging output
    #tracked_absolute_positions = tracked_player.get_absolute_positions()
    #tracked_positions_on_image = cv2.perspectiveTransform(np.float32(tracked_absolute_positions).reshape(-1,1,2), transformation_from_abs).reshape((-1,2))
    
    draw_sequence(images, absolute_positions, tracked_player, transformation_from_abs)

def test_get_image_position_probabilities():
    print 'testing he get_image_position_probabilities function...'
    
    image_size = (300, 400, 3)
    rectangle_size = (30, 60)

    transformation_to_abs = np.eye(3)
    transformation_to_abs[2][0] = 0.015
    
    texture_map = np.zeros((rectangle_size[1], rectangle_size[0], 3))
    texture_map[:, :, 0] = 255

    #create a stupid player model with the blue square
    player_model = StupidPlayerModel(texture_map)

    #create a tracked player object
    print 'constructing the tracked player object'
    initial_image = np.zeros(image_size)
    initial_position_on_image = np.array([100, 50])
    initial_position_on_abs = cv2.perspectiveTransform(np.float32(initial_position_on_image).reshape(-1,1,2), transformation_to_abs).reshape((2,))
    tracked_player = TrackedPlayer(player_model, initial_image, initial_position_on_image, initial_position_on_abs)

    #now get the probabilities for the next frame
    print 'computing position probabilities'
    position_probabilities = tracked_player.get_image_position_probabilities(initial_image, transformation_to_abs)

    #truncate them
    #position_probabilities[position_probabilities < 0.5] = 0
    cv2.imshow('the position probabilities', position_probabilities)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

if __name__=='__main__':

    rectangle_model_test()
    #test_get_image_position_probabilities()
