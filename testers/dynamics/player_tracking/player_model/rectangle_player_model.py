import numpy as np
import cv2

from .....source.dynamics.player_tracking.player_model.rectangle_player_model import RectanglePlayerModel
from .....source.data_structures.contour import Contour

##################################
# A tester for source/dynamics/player_tracking/player_model/rectangel_player_model
##################################

def get_test_player_model():
    PLAYER_IMAGE_NAME = 'test_data/person.png'
    
    player_image = cv2.imread(PLAYER_IMAGE_NAME, 1)

    #the foreground mask consists of all the pixels whose blue value is bigger than 0
    foreground_mask = np.float64(player_image[:,:,0] > 0)

    return RectanglePlayerModel(player_image, foreground_mask)

def test_evaluate_position(player_model):
    #first create a background image with the player drawn in the middle
    image_shape = (400, 300, 3)
    #we start with a white background and an empty obstruction mask
    image = np.zeros(image_shape)
    image[:,:,1] = 255
    obstruction_mask = np.zeros(image_shape[:2])
    
    #and draw on the player
    initial_position = np.array([100, 200])
    player_model.draw_on_image(image, initial_position)
    pos = np.int32(initial_position)
    cv2.circle(image, tuple(pos), 3, (0, 0 ,255), -1)
    
    cv2.imshow('this is the image after gluing on the player model', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    image_position_probabilities = np.ones(image.shape[:2], np.float64)
    initial_evaluation = player_model._evaluate_position(image, initial_position, image_position_probabilities, obstruction_mask)
    print 'the evaluation of the actual position in: ', initial_evaluation

    shifted_position = initial_position + np.array([-10, -10])
    image_with_shift = image.copy()
    player_model.draw_on_image(image_with_shift, shifted_position)
    cv2.circle(image_with_shift, tuple(np.int32(shifted_position)), 3, (0, 0 ,255), -1)
    cv2.imshow('this is the image after drawing the shifted player', image_with_shift)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    evaluation = player_model._evaluate_position(image, shifted_position, image_position_probabilities, obstruction_mask)
    print 'the evaluation after a shift is: ', evaluation

def test_get_best_match(player_model):
    #first create a background image with the player drawn in the middle
    image_shape = (400, 300, 3)
    #we start with a white background and an empty obstruction mask
    image = np.zeros(image_shape)
    image[:,:,1] = 255
    obstruction_mask = np.zeros(image_shape[:2])

    initial_position = np.array([100, 200])
    image_position_probabilities = np.zeros(image_shape[:2], np.float64)
    search_size = 10
    image_position_probabilities[initial_position[1]-search_size:initial_position[1]+search_size, initial_position[0]-search_size:initial_position[0]+search_size] = 1.0
    
    #and draw on the player
    player_model.draw_on_image(image, initial_position)
    pos = np.int32(initial_position)
    cv2.circle(image, tuple(pos), 3, (0, 0 ,255), -1)
    
    cv2.imshow('this is the image after gluing on the player model', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    print 'starting test number 1'
    
    image_with_possible_positions = image.copy()
    image_with_possible_positions[image_position_probabilities == 1] = np.array([0, 0, 0])

    cv2.imshow('these are the possible positions', image_with_possible_positions)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    
    #now search for the best position
    best_position = player_model.get_best_match(image, image_position_probabilities, obstruction_mask)

    print 'the actual position is: ', initial_position
    print 'the calculated position is: ', best_position

    #we now do something a little more complicated
    print 'starting test number 2'

    #use the current player as an obstruction mask
    obstruction_mask = image[:, :, 0] / 255.0

    #now add another player with a shift
    shifted_position = initial_position + np.array([5, 0])
    player_model.draw_on_image(image, shifted_position)
    cv2.imshow('this is the image after drawing the shifted player', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    cv2.imshow('this is the unobstructed part', image * (1 - obstruction_mask).reshape((image_shape[0], image_shape[1], 1)))
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    #now search for the best position
    best_position = player_model.get_best_match(image, image_position_probabilities, obstruction_mask)

    print 'the actual position is: ', shifted_position
    print 'the calculated position is: ', best_position


def test_update(player_model):
    #first create a background image with the player drawn in the middle
    image_shape = (400, 300, 3)
    #we start with a white background and an empty obstruction mask
    image = np.zeros(image_shape)
    image[:,:,1] = 255
    obstruction_mask = np.zeros(image_shape[:2], np.float64)
    #use the current player as a foreground mask
    
    
    #and draw on the player
    initial_position = np.array([100, 200])
    player_model.draw_on_image(image, initial_position)
    pos = np.int32(initial_position)
    cv2.circle(image, tuple(pos), 3, (0, 0 ,255), -1)
    
    cv2.imshow('this is the image after gluing on the player model', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    print 'updating the player model with the current image, and with a shifted position.'

    foreground_mask = np.float64(image[:, :, 0]) / 255.0
    shifted_position = initial_position + np.array([20, 0])
    player_model.update(shifted_position, image, foreground_mask, obstruction_mask)

    cv2.imshow('this is the texture map after the update', player_model._texture_map)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    cv2.imshow('this is the foreground mask after the update', player_model._foreground_mask)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def test_obstruction_to_others(player_model):
    #first create a background image
    image_shape = (400, 300, 3)
    #we start with a white background and an empty obstruction mask
    image = np.zeros(image_shape)
    image[:,:,1] = 255

    position = np.array([100, 200])
    player_model.draw_on_image(image, position)

    cv2.imshow('this is the image after gluing on the player model', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    obstruction_to_others = player_model.get_obstruction_to_others(image, position)

    cv2.imshow('obstruction to others', obstruction_to_others)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def test_matches_contour(player_model):
    #first create a background image
    image_shape = (400, 300, 3)
    #we start with a white background and an empty obstruction mask
    image = np.zeros(image_shape)
    image[:,:,1] = 255

    position = np.array([100, 200])
    player_model.draw_on_image(image, position)

    cv2.imshow('this is the image after gluing on the player model', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    foreground_mask = np.uint8(image[:, :, 0] / 255.0)

    #use the foreground mask to get a contour
    contours, hierarchy = cv2.findContours(foreground_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cv_contour = contours[0]
    contour = Contour(cv_contour, image)
    
    #draw the contour on the image
    image_copy = image.copy()
    cv2.drawContours(image_copy, [cv_contour], -1, (0,0,255), 2)

    cv2.imshow('this is the contour', image_copy)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    print 'testing if the player matches itself...'
    shifted_position = position
    image_copy = image.copy()
    player_model.draw_on_image(image_copy, shifted_position)
    cv2.imshow('the shifted player', image_copy)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    print player_model.matches_contour(image, contour, shifted_position)

    print 'testing if the player matches itself shifted a little...'
    shifted_position = position + np.array([2, 0])
    image_copy = image.copy()
    player_model.draw_on_image(image_copy, shifted_position)
    cv2.imshow('the shifted player', image_copy)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    print player_model.matches_contour(image, contour, shifted_position)

    print 'testing if the player matches itself shifted a lot...'
    shifted_position = position + np.array([10, 0])
    image_copy = image.copy()
    player_model.draw_on_image(image_copy, shifted_position)
    cv2.imshow('the shifted player', image_copy)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    print player_model.matches_contour(image, contour, shifted_position)

    print 'testing if the player matches itself shifted a ton...'
    shifted_position = position + np.array([15, 0])
    image_copy = image.copy()
    player_model.draw_on_image(image_copy, shifted_position)
    cv2.imshow('the shifted player', image_copy)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    print player_model.matches_contour(image, contour, shifted_position)

def test_draw_player(player_model):
    #first create a background image
    image_shape = (400, 150, 3)
    #we start with a white background and an empty obstruction mask
    image = np.zeros(image_shape)
    image[:,:,1] = 255

    position = np.array([150, 30])
    player_model.draw_on_image(image, position)

    cv2.imshow('this is the image after drawing the player model', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def test_get_cropped_extent_one_coordinate(player_model):
    length = 10
    extents = np.array([2,3])
    positions = [-5, -3, -2, 0, 1, 2, 7, 8, 9, 10, 12, 11, 13, 15]

    for position in positions:
        print 'position = %d, length = %d, extents = (%d, %d)' % (position, length, extents[0], extents[1])
        cropped_extents = player_model.get_cropped_extent_one_coordinate(length, position, extents)
        print 'cropped extents: ', cropped_extents
        print 'old region = [%d, %d)' % (position - extents[0], position + extents[1]) 
        print 'new region = [%d, %d)' % (position - cropped_extents[0], position + cropped_extents[1]) 


def test_generate_player_model_from_contour(player_model):
    
    #first create a background image
    image_shape = (400, 300, 3)
    #we start with a white background and an empty obstruction mask
    image = np.zeros(image_shape)
    image[:,:,1] = 255

    position = np.array([100, 200])
    contour_image = image.copy()
    player_model.draw_on_image(contour_image, position)

    foreground_mask = np.uint8(contour_image[:, :, 0] / 255.0)

    #use the foreground mask to get a contour
    contours, hierarchy = cv2.findContours(foreground_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    contour = contours[0]

    #draw the contour on the image
    image_copy = image.copy()
    cv2.drawContours(image_copy, [contour], -1, (0,0,255), 2)

    cv2.imshow('this is the contour', image_copy)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    #now use the factory function with the contour, and an identity homography matrix
    gen_player_model, gen_position_on_image, gen_absolute_position = generate_player_model_from_contour(contour, contour_image, np.eye(3))

    print 'the position we found is: ', gen_position_on_image

    cv2.imshow('this is the texture map we found', gen_player_model._texture_map)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    output_image = image.copy()
    gen_player_model.draw_on_image(output_image, gen_position_on_image)

    cv2.imshow('this is the model we found', output_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    
def basic_test():
    import cv2

    #get the player model
    player_model = get_test_player_model()

    cv2.imshow('this is the player model', player_model._texture_map)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    #test_evaluate_position(player_model)
    #test_get_best_match(player_model)
    #test_update(player_model)
    #test_obstruction_to_others(player_model)
    test_matches_contour(player_model)
    #test_draw_player(player_model)
    #test_get_cropped_extent_one_coordinate(player_model)
    #test_generate_player_model_from_contour(player_model)
    
if __name__=='__main__':
    basic_test()


