import numpy as np
import matplotlib.pyplot as plt
from ....source.dynamics.extended_kalman_filter.extended_kalman_filter import ExtendedKalmanFilter
from ....source.auxillary import np_operations

dt = 1/25.0

def basic_test():
    '''
    this tests a basic 2d constant velocity model
    '''
    F = np.array([[1,0,dt,0 ],
                  [0,1,0 ,dt],
                  [0,0,1 ,0 ],
                  [0,0,0 ,1 ]])
    
    H = np.array([[1,0,0,0],
                  [0,1,0,0]])

    Q = np.array([[(dt**3)/3, 0        , (dt**2)/2, 0        ],
                  [0        , (dt**3)/3, 0        , (dt**2)/2],
                  [(dt**2)/2, 0        , dt       , 0        ],
                  [0        , (dt**2)/2, 0        , dt       ]])

    R = np.diag(np.array([5.0,5.0]))
    
    f  = lambda x: np_operations.batch_multiply_matrix_vector(F,x)
    h  = lambda x: np_operations.batch_multiply_matrix_vector(H,x)
    Jf = lambda x: F
    Jh = lambda x: H

    kf = ExtendedKalmanFilter(f, Jf, h, Jh, Q, R)

    initial_state = np.array([0,0,25,25], np.float64).reshape((1,-1))
    initial_cov   = 10*Q.reshape((1,) + Q.shape)

    print 'initial state: ', initial_state
    print 'initial_cov:'
    print initial_cov
    
    states = [initial_state]
    observations = [h(initial_state)]
    
    for i in range(1,100):
        new_state = f(states[-1]) + np.random.multivariate_normal(np.zeros(4), Q, size=(len(initial_state),))
        new_observation = h(new_state) + np.random.multivariate_normal(np.zeros(2), R, size=(len(initial_state),))
        states.append(new_state)
        observations.append(new_observation)

    states = np.vstack(states)
    observations = np.vstack(observations)

    print 'states: '
    print states
    print 'observations: '
    print observations

    initial_state = np.vstack([initial_state for i in range(1000)])
    initial_cov = np.vstack([initial_cov for i in range(1000)])
    initial_state[0,2] = 0
    initial_state[1,2] = 10
    filtered_states = [initial_state]
    filtered_covs   = [initial_cov]
    innovations  = [np.zeros((1000,2))]
    errors = [np.zeros((1000,4))]
    predicted_errors_sq = [np.zeros((1000,4))]

    print 'start filter...'
    for i in range(1,100):
        new_state, new_cov = kf.filter(filtered_states[-1], filtered_covs[-1], observations[i])
        new_innovation = observations[i] - h(new_state)
        new_error = states[i] - new_state
        S = kf.S()
        # print 'new state: ', new_state
        # print 'new cov:'
        # print new_cov
        # print 'S[0][0,0]:', S[0,0,0]
        # print 'diagonal(P[0]): ', np.diagonal(new_cov[0])
        filtered_states.append(new_state)
        filtered_covs.append(new_cov)
        innovations.append(new_innovation)
        errors.append(new_error)
        predicted_errors_sq.append(np.diagonal(new_cov,axis1=1,axis2=2))
        
    filtered_states = np.stack(filtered_states)
    innovations = np.stack(innovations)
    errors = np.stack(errors)
    predicted_errors_sq = np.stack(predicted_errors_sq)
    
    #print 'states vs filtered states'
    #print np.hstack([states[:,:2], filtered_states[:,:2]])

    #upper_bound = filtered_states[:,0,0] + 2*np.sqrt(predicted_errors_sq[:,0,0])
    #lower_bound = filtered_states[:,0,0] - 2*np.sqrt(predicted_errors_sq[:,0,0])
    plt.plot(range(len(states)), states[:,0], 'bo')
    plt.plot(range(len(states)), filtered_states[:,0,0])
    plt.plot(range(len(states)), filtered_states[:,1,0])
    plt.plot(range(len(states)), filtered_states[:,2,0])
    plt.show()

    # plt.plot(range(len(innovations)), innovations[:,0])
    # plt.show()

    # plt.plot(range(len(errors)), errors[:,0], 'ro')
    # plt.plot(range(len(errors)),  2*np.sqrt(predicted_errors_sq[:,0]), 'b-')
    # plt.plot(range(len(errors)), -2*np.sqrt(predicted_errors_sq[:,0]), 'b-')
    # plt.show()
    
if __name__ == '__main__':
    basic_test()
