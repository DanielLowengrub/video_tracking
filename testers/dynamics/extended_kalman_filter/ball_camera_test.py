import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import os
import pickle
import functools
from ....source.auxillary.camera_matrix import CameraMatrix
from ....source.auxillary import np_operations, planar_geometry
from ....source.preprocessing.camera_samples_interpolator.camera_samples_interpolator import CameraSamplesInterpolator
from ....source.dynamics.extended_kalman_filter.extended_kalman_filter import ExtendedKalmanFilter
from ....source.auxillary import world_to_image_projections, tf_operations
from ....source.auxillary.fit_points_to_shape.ellipse import Ellipse
from ....source.auxillary.tf_optimizer import TFOptimizer

g_coeff = 9.8*6.0
drag_coeff = 0.01 / 6.0
dt = 1/25.0
num_samples = 50
max_height = 30
max_height_change = 5
soccer_ball_radius = (4.3/36)*6.0

def compute_ball_plane(ball_world_positions):
    end_point_index = -1
    start_world_point = ball_world_positions[0]
    end_world_point = ball_world_positions[end_point_index]
    
    #get the plane which contains these points and is perpendicular to the ground
    #this is the normal vector to the plane
    n = np.cross(np.array([0,0,1], np.float64), end_world_point - start_world_point)
    n = n / np.linalg.norm(n)

    #now we plot the ball states on the ball plane. we set the origin on the plane to be the end point of the ball
    ball_plane_y_axis = np.array([0,0,1], np.float64)
    ball_plane_x_axis = np.cross(n, ball_plane_y_axis)

    if np.dot(ball_plane_x_axis, start_world_point - end_world_point) < 0:
        ball_plane_x_axis *= -1

    ball_plane_origin = end_world_point.copy()
    ball_plane_origin[2] = 0
    
    return ball_plane_origin, ball_plane_x_axis, ball_plane_y_axis

def project_world_points_to_plane(world_points, plane_origin, plane_x_axis, plane_y_axis):
    world_points = world_points - plane_origin
    plane_points_x = np.dot(plane_x_axis, world_points.transpose())
    plane_points_y = np.dot(plane_y_axis, world_points.transpose())

    return np.vstack([plane_points_x, plane_points_y]).transpose()

def map_plane_points_to_world(plane_points, plane_origin, plane_x_axis, plane_y_axis):
    world_points = (plane_origin +
                    (plane_points[:,0].reshape((-1,1)) * plane_x_axis) +
                    (plane_points[:,1].reshape((-1,1)) * plane_y_axis))
    return world_points

def load_image_positions_and_cameras():
    # frame_interval = (951, 1000)
    # ball_trajectory_ids = (687, 713)
    # frame_interval = (923, 942)
    # ball_trajectory_ids = (656,)
    # frame_interval = (952, 970)
    # ball_trajectory_ids = (687, 713)
    # frame_interval = (1040,1055) #one bounce 
    # ball_trajectory_ids = (746,)
    # frame_interval = (1040,1080) #two bounces
    # ball_trajectory_ids = (746,757)
    # frame_interval = (670, 709) #GROUND??
    # ball_trajectory_ids = (455,)
    # frame_interval = (1564, 1600) #GROUND??
    # ball_trajectory_ids = (926,)
    # frame_interval = (1736, 1744)
    # ball_trajectory_ids = (979,)
    # frame_interval = (1056, 1076)
    # ball_trajectory_ids = (757,)
    # frame_interval = (1150, 1190) #GROUND
    # ball_trajectory_ids = (807,)
    # frame_interval = (1243, 1266) 
    # ball_trajectory_ids = (826,)
    # frame_interval = (49,68)       #GROUND
    # ball_trajectory_ids = (5,)
    frame_interval = (124,170)       #GROUND - change direction
    ball_trajectory_ids = (12,54)
    # frame_interval = (235,276)     #GROUND - change direction
    # ball_trajectory_ids = (79,95)
    # frame_interval = (1007, 1030)
    # ball_trajectory_ids = (739,)


    #load the ball image positions from tracked ball data
    tracked_balls_file = os.path.join('/Users/daniel/Documents/soccer/output/postprocessing/data',
                                      'images-8_15-9_31/no_ball_trajectory_overlaps/tracked_balls_0_1874.pckl')
    f = open(tracked_balls_file, 'rb')
    tracked_balls = pickle.load(f)
    f.close()

    ball_trajectories = [tracked_balls.get_ball_trajectory_dict()[k] for k in ball_trajectory_ids]
    image_positions = np.vstack([bt.image_positions for bt in ball_trajectories])
    print 'total number of image positions: ', len(image_positions)
    
    #remove the image positions that come before the frame interval, and the ones that come after
    frame_interval_length = frame_interval[1] - frame_interval[0] + 1
    print 'frame interval length: ', frame_interval_length
    
    start_index = frame_interval[0] - ball_trajectories[0].start_frame
    print 'start index: ', start_index
    print 'end index  : ', start_index + frame_interval_length
    image_positions = image_positions[start_index : start_index + frame_interval_length]

    
    #load the cameras in the frame interval
    camera_directory = '/Users/daniel/Documents/soccer/output/preprocessing/data/images-8_15-9_31/camera'
    indexed_cameras = CameraSamplesInterpolator.load_cameras(camera_directory)
    cameras = list(next(indexed_cameras)[1])[frame_interval[0] : frame_interval[1]+1]

    return image_positions, cameras

def approximate_world_positions(image_positions, cameras):
    #now approximate the world plane that the ball trajectory lies on. To do this, we assume that the first and last
    #ball are 5 units off the ground
    end_point_heights = np.array([0,0])
    end_point_index = -1
    print 'end point heights: ', end_point_heights
    ground_planes = np.array([[0,0,1,-end_point_heights[0]], [0,0,1,-end_point_heights[1]]], np.float64)
    start_world_point = cameras[0].compute_preimage_on_plane(image_positions[0].astype(np.float64), ground_planes[0])
    end_world_point = cameras[end_point_index].compute_preimage_on_plane(image_positions[end_point_index].astype(np.float64), ground_planes[1])

    #get the plane which contains these points and is perpendicular to the ground
    #this is the normal vector to the plane
    n = np.cross(np.array([0,0,1], np.float64), end_world_point - start_world_point)
    n = n / np.linalg.norm(n)
    d = np.dot(n, start_world_point)
    ball_plane = np.append(n, -d)

    #now get the trajectory by computing the preimages of each of the image points that lie on the ball plane
    preimages = np.vstack([camera.compute_preimage_on_plane(p, ball_plane) for camera,p in zip(cameras, image_positions)])
    
    return preimages

def world_to_projective_image(C, p):
    '''
    C - a numpy array with shape (3,4)
    x - a numpy array with shape (l,6)
    return - a numpy array with shape (l,3)
    '''
    #print 'projecting point %s with camera:' % str(p)
    #print C
    
    projective_p = np.hstack([p[:,:3], np.ones((len(p),1))])
    projective_image = np_operations.batch_multiply_matrix_vector(C, projective_p)

    #print 'projective image: ', projective_image
    
    return projective_image

def random_sample_initial_states(image_positions, cameras):
    '''
    return a numpy array with shape (num states, 6)
    '''
    #get the homography matrices from the image to the field
    Hs = [np.linalg.inv(camera.get_homography_matrix()) for camera in cameras[:2]]
    
    #calculate the first and second field positions
    field_positions = [planar_geometry.apply_homography_to_point(H, p) for H,p in zip(Hs,image_positions[:2])]
    field_positions = [np.append(x,0) for x in field_positions]

    # print 'field positions:'
    # print field_positions
    
    #calculate the directions from the field positions to the camera
    directions_to_camera = np.vstack([(c.get_camera_position() - x) for c,x in zip(cameras[:2],field_positions)])
    directions_to_camera /= directions_to_camera[:,2:]

    print 'directions to camera:'
    print directions_to_camera

    #assume that the first position has height 0
    initial_heights = np.linspace(0,max_height,5)
    velocity_std = 20
    num_random_states = 500
    num_random_heights = 10
    initial_velocities = []
    initial_states = []

    #there are 5 initial heights
    for initial_height in initial_heights:
        initial_velocities = []
        initial_position = field_positions[0] + initial_height*directions_to_camera[0]
        print 'initial position:'
        print initial_position

        #there are 10 next heights
        for next_height in np.linspace(initial_height,initial_height+max_height_change,num_random_heights):
            print '   generating initial velocities for next height: ', next_height
            next_position = field_positions[1] + next_height*directions_to_camera[1]
            initial_velocity = (next_position - initial_position) / dt

            print '   mean velocity: ', initial_velocity
    
            #generate random velocities
            initial_velocities.append((initial_velocity.reshape((1,3)) +
                                       velocity_std*np.random.normal(size=(num_random_states,3))))

        initial_velocities = np.vstack(initial_velocities)
        initial_positions = np.vstack([initial_position for i in range(len(initial_velocities))])
        initial_states.append(np.hstack([initial_positions, initial_velocities]))
        
    initial_states = np.vstack(initial_states)
    return initial_states

def sample_initial_states(image_positions, cameras):
    '''
    return a numpy array with shape (num states, 6)
    '''
    #get the homography matrices from the image to the field
    Hs = [np.linalg.inv(camera.get_homography_matrix()) for camera in cameras[:2]]
    
    #calculate the first and second field positions
    field_positions = [planar_geometry.apply_homography_to_point(H, p) for H,p in zip(Hs,image_positions[:2])]
    field_positions = [np.append(x,0) for x in field_positions]

    # print 'field positions:'
    # print field_positions
    
    #calculate the directions from the field positions to the camera
    directions_to_camera = np.vstack([(c.get_camera_position() - x) for c,x in zip(cameras[:2],field_positions)])
    directions_to_camera /= directions_to_camera[:,2:]

    # print 'directions to camera:'
    # print directions_to_camera
    
    heights = np.linspace(0,max_height,num_samples)
    initial_states = []
    #for each position in the first frame sample positions for the second frame, and use them to create initial states
    for height in heights:
        # print 'height_0: ', height

        position = field_positions[0] + height*directions_to_camera[0]
        # print 'position: ', position
        
        next_heights = np.linspace(max(height-max_height_change,0), height+max_height_change, num_samples)
        for next_height in next_heights:
            # print '   next height: ', next_height

            next_position = field_positions[1] + next_height*directions_to_camera[1]
            # print '   next position: ', next_position
            
            velocity = (next_position - position) / dt
            # print '   velocity: ', velocity
            
            initial_states.append(np.hstack([position, velocity]))
            
    initial_states = np.vstack(initial_states)

    return initial_states

def build_h(C, p):
    '''
    C - a numpy array with shape (3,4)
    x - a numpy array with shape (l,6)
    return - a numpy array with shape (l,2)

    for each row (px,py,pz,vx,vy,vz), we multiply C*(px,py,pz,1) to get (qx,qy,qz) and then divide by qz.
    '''
    q = world_to_projective_image(C, p)
    return q[:,:2] / q[:,2:]

def build_simple_Jphi(p):
    '''
    p - a numpy array with shape (l,3)
    return - a numpy array with shape (l,2,3)

    Let phi:R^3 -> R^2 be the map (x,y,z) |-> (x/z,y/z)
    The i-th row of the output is the jacobian of phi at p[i]
    '''
    #we use a silly approximation of the jacoboan:
    #[1/z, 0, 0]
    #[0, 1/z, 0]
    z = p[:,2]

    #this is an array with shape (l,3). The i-th row is the 0-th row of the i-th jacobian
    J_row_0 = np.vstack([1/z             , np.zeros((2,len(p)))]).T
    #this is an array with shape (l,3). The i-th row is the 1-st row of the i-th jacobian
    J_row_1 = np.vstack([np.zeros(len(p)), 1/z, np.zeros(len(p))]).T

    #create an array with shape (l,2,3). J[i] is a matrix with shape (2,3).
    #The first row of J[i] is: J_row_0[i] 
    #and it's second is      : J_row_1[i]
    #note that after stacking J_row_0 and J_row_1, we get an array with shape (2,l,3). To get the correct array,
    #we must switch the 0-th and 1-st axes.
    J = np.transpose(np.stack([J_row_0, J_row_1]), (1,0,2))

    print 'z:'
    print z
    print 'J_phi:'
    print J
    
    return J

def build_Jphi(p):
    '''
    p - a numpy array with shape (l,3)
    return - a numpy array with shape (l,2,3)

    Let phi:R^3 -> R^2 be the map (x,y,z) |-> (x/z,y/z)
    The i-th row of the output is the jacobian of phi at p[i]
    '''
    #print 'building J_phi at the point: ', p
    
    #By the definition of phi, its jacobian at p=(x,y,z) is:
    #[1/z, 0  , -x/(z^2)]
    #[0  , 1/z, -y/(z^2)]
    x = p[:,0]
    y = p[:,1]
    z = p[:,2]

    #this is an array with shape (l,3). The i-th row is the 0-th row of the i-th jacobian
    J_row_0 = np.vstack([1/z             , np.zeros(len(p)), -x/(z*z)]).T
    #this is an array with shape (l,3). The i-th row is the 1-st row of the i-th jacobian
    J_row_1 = np.vstack([np.zeros(len(p)), 1/z             , -y/(z*z)]).T

    #create an array with shape (l,2,3). J[i] is a matrix with shape (2,3).
    #The first row of J[i] is: J_row_0[i] 
    #and it's second is      : J_row_1[i]
    #note that after stacking J_row_0 and J_row_1, we get an array with shape (2,l,3). To get the correct array,
    #we must switch the 0-th and 1-st axes.
    J = np.transpose(np.stack([J_row_0, J_row_1]), (1,0,2))

    #print 'J_phi:'
    #print J
    
    return J

def build_Jh(C, p):
    '''
    C - a numpy array with shape (3,4)
    x - a numpy array with shape (l,6)
    return - a numpy array with shape (2,6)
    '''
    #print 'building Jh at the point %s with camera: ' % str(p)
    #print C
    
    #first compute the jacobian at p of the map C:R^3 -> R^3 which sends (x,y,z) to C*(x,y,z,1)^T
    #this is a numpy array with shape (3,3)
    #note that this does not depend on the point x.
    JC_p = np.hstack([C[:,:3], np.zeros((3,3))])

    #then compute the jacobian at y=C(p) of the map phi:R^3 -> R^2 that sends (x,y,z) to (x/z,y/z)
    Cp = world_to_projective_image(C, p)
    
    #this is a numpy array with shape (l,2,3)
    Jphi_Cp = build_Jphi(Cp)
    #Jphi_Cp = build_simple_Jphi(Cp)
    
    #use the chain rule to comptue J(phi o C)
    Jh = np.matmul(Jphi_Cp, JC_p)

    #print 'Jh:'
    #print Jh

    return Jh
    
def build_kalman_filter():
    #build the transition matrix
    F = np.array([[1, 0, 0, dt               , 0                , 0                ],
                  [0, 1, 0, 0                , dt               , 0                ],
                  [0, 0, 1, 0                , 0                , dt               ],
                  [0, 0, 0, 1 - drag_coeff*dt, 0                , 0                ],
                  [0, 0, 0, 0                , 1 - drag_coeff*dt, 0                ],
                  [0, 0, 0, 0                , 0                , 1 - drag_coeff*dt]])
    b = np.array([0, 0, 0, 0, 0, -g_coeff*dt])
    
    f  = lambda x: np_operations.batch_multiply_matrix_vector(F,x) + b.reshape((1,6))
    Jf = lambda x: F

    transition_variance = 10.0
    Q = np.array([[(dt**3)/3, 0        , 0        , (dt**2)/2, 0        , 0         ],
                  [0        , (dt**3)/3, 0        , 0        , (dt**2)/2, 0         ],
                  [0        , 0        , (dt**3)/3, 0        , 0        , (dt**2)/2 ],
                  [(dt**2)/2, 0        , 0        , dt       , 0        , 0         ],
                  [0        , (dt**2)/2, 0        , 0        , dt       , 0         ],
                  [0        , 0        , (dt**2)/2, 0        , 0        , dt        ]])
    Q *= transition_variance
    R = np.diag(np.array([25.0, 25.0], np.float64))

    return ExtendedKalmanFilter(f=f, Jf=Jf, h=None, Jh=None, Q=Q, R=R)

def track_ball():
    image_positions, cameras = load_image_positions_and_cameras()
    world_positions = approximate_world_positions(image_positions, cameras)

    print 'image positions:'
    print image_positions
    
    print 'approximate world positions:'
    print world_positions

    initial_state = random_sample_initial_states(image_positions, cameras)
    print 'initial states: '
    print initial_state
    
    ekf = build_kalman_filter()
    
    # initial_position = world_positions[0]
    # initial_velocity = (world_positions[1] - world_positions[0]) / dt
    # initial_velocity[2] = 16.0
    # initial_velocity[0] = -105
    # initial_state = np.hstack([initial_position, initial_velocity]).reshape((1,6))
    initial_cov = np.stack([(10 * ekf._Q).reshape((6,6)) for i in range(len(initial_state))])

    print 'observation           : ', image_positions[0]
    print 'predicted observations: '
    print build_h(cameras[0].get_numpy_matrix(), initial_state)

    
    filtered_states = initial_state.reshape((1,) + initial_state.shape)
    loglikelihoods  = np.zeros((1,len(initial_state)))
    loglikelihood_sums = np.zeros(len(initial_state))
    
    #innovations = [np.zeros(len(initial_state))]
    
    current_state = initial_state
    current_cov = initial_cov
    print 'initial state shape: ', initial_state.shape
    print 'initial cov shape  : ', initial_cov.shape
    for i, (image_position,camera) in enumerate(zip(image_positions, cameras)[1:]):
        print '\n\nupdating to frame %d...' % (i+1)
        observation = image_position
        if i in ():
            observation = None
            
        current_h  = functools.partial(build_h, camera.get_numpy_matrix())
        current_Jh = functools.partial(build_Jh, camera.get_numpy_matrix())
        current_state, current_cov = ekf.filter(current_state, current_cov, observation,
                                                h=current_h, Jh=current_Jh)


        # print 'predicted positions   : '
        # print current_state[:5,:3]
        # print 'current observation   : ', image_position
        # print 'predicted observations: '
        # print current_h(current_state)[:5]
        # print 'current likelihood: ', ekf.loglikelihood()[:5]

        #current_innovations = np.linalg.norm(image_position.reshape((1,2)) - current_h(current_state), axis=1)
        
        #innovations.append(current_innovations)

        filtered_states = np.vstack([filtered_states, current_state.reshape((1,)+current_state.shape)])
        
        if observation is None:
            loglikelihoods = np.vstack([loglikelihoods, -1*np.ones((1,len(current_state)))])

        else:
            current_loglikelihoods = ekf.loglikelihood()
            loglikelihood_sums += current_loglikelihoods
            loglikelihoods = np.vstack([loglikelihoods, current_loglikelihoods.reshape((1,)+current_loglikelihoods.shape)])
            
            #remove the states whose loglikelihood is too small
            likelihood_mask = current_loglikelihoods <= 10
            if not np.any(likelihood_mask):
                print 'there are not likely states. terminating.'
                break
            
            current_state = current_state[likelihood_mask]
            current_cov = current_cov[likelihood_mask]
            print 'filtered states shape: ', filtered_states.shape
            print 'likelihood mask shape: ', likelihood_mask.shape
            filtered_states = filtered_states[:,likelihood_mask]
            loglikelihoods = loglikelihoods[:,likelihood_mask]
            loglikelihood_sums = loglikelihood_sums[likelihood_mask]
            
        # if i>15:
        #     best_state_index = np.argmin(loglikelihood_sums)
        #     if current_state[best_state_index, 2] < -1:
        #         print 'the best state hit the ground. terminating filter.'
        #         break
            
        # is_above_ground = current_state[:,2] > -2
        # print 'is above ground shape: ', is_above_ground.shape
        # current_state = current_state[is_above_ground]
        # current_cov = current_cov[is_above_ground]
        # filtered_states = filtered_states[:,is_above_ground]
        # loglikelihoods = loglikelihoods[:,is_above_ground]
        # loglikelihood_sums = loglikelihood_sums[is_above_ground]
        
        # bounce_mask = (filtered_states[-1,:,2] < 0) * (filtered_states[-2,:,2] > 0)
        # print 'number of bounces: ', bounce_mask.sum()
        # filtered_states[-1,bounce_mask,5] *= -0.9
        # current_state[bounce_mask,5] *= -0.9

        # #suppose 5000 - (current number of states) = N
        # #let n be the max such that 100*n < N
        # #take the bouncing balls with the n best likelihoods and add 100 random pertubations to the speed
        # max_states = 5000
        # num_perturbed_states_per_ball = 200
        # bounce_indices = np.where(bounce_mask)[0]
        # #print 'bounce indices: ', bounce_indices
        # bounce_loglikelihood_sums = loglikelihood_sums[bounce_mask]
        # sorted_bounce_indices = bounce_indices[np.argsort(bounce_loglikelihood_sums)]
        # num_perturbed_balls = (max_states - len(current_state)) / num_perturbed_states_per_ball

        # if num_perturbed_balls <= 0:
        #     continue
        
        # new_current_states = []
        # new_current_covs = []
        # new_filtered_states = []
        # new_loglikelihoods = []
        # new_loglikelihood_sums = []

        # print 'perturbing %d bounced balls...' % num_perturbed_balls
        # for perturbed_ball_index in sorted_bounce_indices[:num_perturbed_balls]:
        #     ball_position = current_state[perturbed_ball_index,:3]
        #     ball_velocity = current_state[perturbed_ball_index,3:].reshape((1,3))
        #     ball_velocity_std = (0.05 * np.abs(ball_velocity)).reshape((1,3))
        #     ball_cov = current_cov[perturbed_ball_index]
        #     ball_loglikelihood_sum = loglikelihood_sums[perturbed_ball_index]
        #     ball_loglikelihoods    = loglikelihoods[:,perturbed_ball_index]
        #     ball_filtered_states = filtered_states[:,perturbed_ball_index]
            
        #     perturbed_velocities = ball_velocity + ball_velocity_std*np.random.normal(size=(num_perturbed_states_per_ball, 3))
                                                       
        #     perturbed_positions  = ball_position.reshape((1,3)) * np.ones((num_perturbed_states_per_ball,3))
        #     perturbed_states = np.hstack([perturbed_positions, perturbed_velocities])

        #     cov_shape = ball_cov.shape
        #     perturbed_covs = np.ones((num_perturbed_states_per_ball,)+cov_shape)
        #     perturbed_covs *= ball_cov.reshape((1,)+cov_shape)

        #     filtered_shape = ball_filtered_states.shape
        #     perturbed_filtered_states = np.ones((filtered_shape[0]-1, num_perturbed_states_per_ball, filtered_shape[1]))
        #     perturbed_filtered_states *= ball_filtered_states[:-1].reshape((filtered_shape[0]-1, 1, filtered_shape[1]))
        #     perturbed_filtered_states = np.vstack([perturbed_filtered_states,
        #                                            perturbed_states.reshape((1,)+perturbed_states.shape)])

        #     perturbed_loglikelihoods = np.ones((len(ball_loglikelihoods), num_perturbed_states_per_ball))
        #     perturbed_loglikelihoods *= ball_loglikelihoods.reshape((-1,1))

        #     perturbed_loglikelihood_sums = ball_loglikelihood_sum * np.ones(num_perturbed_states_per_ball)

        #     print '* ball position: ', ball_position
        #     print '  ball velocity: ', ball_velocity
        #     print '  average log likelihood: ', ball_loglikelihood_sum / len(ball_loglikelihoods)
        #     print '  perturbed state 0: ', perturbed_states[0]
        #     # print '   perturbed states shape     : ', perturbed_states.shape
        #     # print '   perturbed_covs shape       : ', perturbed_covs.shape
        #     # print '   perturbed filt states shape: ', perturbed_filtered_states.shape
        #     # print '   perturbed lls shape        : ', perturbed_loglikelihoods.shape
        #     # print '   perturbed ll_sums shape    : ', perturbed_loglikelihood_sums.shape
            
        #     new_current_states.append(perturbed_states)
        #     new_current_covs.append(perturbed_covs)
        #     new_filtered_states.append(perturbed_filtered_states)
        #     new_loglikelihoods.append(perturbed_loglikelihoods)
        #     new_loglikelihood_sums.append(perturbed_loglikelihood_sums)

        # current_state = np.vstack([current_state] + new_current_states)
        # current_cov = np.vstack([current_cov] + new_current_covs)
        # filtered_states = np.concatenate([filtered_states] + new_filtered_states, axis=1)
        # loglikelihoods = np.hstack([loglikelihoods] + new_loglikelihoods)
        # loglikelihood_sums = np.hstack([loglikelihood_sums] + new_loglikelihood_sums)
        
    filtered_states = np.stack(filtered_states)
    loglikelihoods  = np.stack(loglikelihoods)
    # innovations     = np.stack(innovations)
    
    mean_loglikelihoods = np.mean(loglikelihoods[1:], axis=0)

    is_on_ground = ((filtered_states[0,:,2]  > -2) * 
                    #(filtered_states[0,:,2]  <  5) *
                    (filtered_states[-1,:,2] > -2)) 
                    #(filtered_states[-1,:,2] <  5))
                    
    #print 'is above ground shape: ', is_above_ground.shape
    print 'min,max: ', (mean_loglikelihoods.min(), mean_loglikelihoods.max())
    mean_loglikelihoods[np.logical_not(is_on_ground)] = mean_loglikelihoods.max()+1
    print 'min,max: ', (mean_loglikelihoods.min(), mean_loglikelihoods.max())
    
    sorted_trajectory_indices = np.argsort(mean_loglikelihoods)
    best_trajectory = sorted_trajectory_indices[0]

    print 'is_on_ground[best]: ', is_on_ground[best_trajectory]
    # print 'filtered states:'
    # print filtered_states
    # print 'best loglikelihood in each frame:'
    # print [x for x in loglikelihoods.min(axis=1)]
    
    print 'best loglikelihood: ', mean_loglikelihoods[best_trajectory]
    print 'top 10 ll: ', mean_loglikelihoods[sorted_trajectory_indices[:10]]
    print 'best initial state: ', filtered_states[0,best_trajectory]
    
    print 'best loglikelihoods:'
    print loglikelihoods[:,best_trajectory]
    print 'worst loglikelihoods:'
    print loglikelihoods[:,sorted_trajectory_indices[-1]]
    
    # print 'best states:'
    # print filtered_states[:,best_trajectory]
    
    print 'image ellipses at best trajectory'
    
    # for p, camera in zip(filtered_states[:,best_trajectory,:3], cameras):
    #     image_ellipse = world_to_image_projections.project_spheres_to_image(camera, soccer_ball_radius, p.reshape((1,3)))[0]
    #     image_ellipse = Ellipse.from_parametric_representation(image_ellipse)
    #     print image_ellipse
        
    fig = plt.figure()
    ax1 = fig.add_subplot('121')
    ax2 = fig.add_subplot('122')
    ax1.plot(np.arange(len(world_positions)), world_positions[:,2], 'bo')
    ax2.plot(world_positions[:,0], world_positions[:,1], 'bo')

    for i in sorted_trajectory_indices[:10]:
        ax1.plot(np.arange(len(filtered_states)), filtered_states[:,i,2], 'k-', alpha=0.2)
        ax2.plot(filtered_states[:,i,0], filtered_states[:,i,1], 'k-', alpha=0.2)

    ax1.plot(np.arange(len(filtered_states)), filtered_states[:,best_trajectory,2], 'ro')
    ax2.plot(filtered_states[:,best_trajectory,0], filtered_states[:,best_trajectory,1], 'ro')

    plt.show()

def tf_plane_trajectory_error(num_states, observed_positions, plane_states):
    positions = plane_states[:,:2]
    velocities = plane_states[:,2:]

    predicted_positions = positions + dt*velocities
    #velocity_norms = tf.sqrt(tf.reduce_sum(tf.pow(velocities, 2), 1))
    #norms_shape = tf.concat(0, [tf.shape(velocities)[:1], [1]])
    #velocity_norms = tf.reshape(velocity_norms, norms_shape)
    drag = -drag_coeff * velocities
    g = -g_coeff * np.array([[0,1]])
    predicted_velocities = velocities + dt*(drag + g)

    position_dynamics_error = tf.reduce_mean(tf.squared_difference(predicted_positions[:num_states-1,:], positions[1:,:]))
    position_observation_error = tf.reduce_mean(tf.squared_difference(observed_positions, positions))
    velocity_dynamics_error = tf.reduce_mean(tf.squared_difference(predicted_velocities[:num_states-1,:], velocities[1:,:]))

    return (1.0/25)*position_observation_error + (1/(10*(dt**3)))*position_dynamics_error + (1/(10*dt))*velocity_dynamics_error

def tf_trajectory_error(num_states, image_positions, cameras, states):
    positions = states[:,:3]
    velocities = states[:,3:]

    print 'positions:  ', positions
    print 'velocities: ', velocities
    
    predicted_positions = positions + dt*velocities
    #velocity_norms = tf.sqrt(tf.reduce_sum(tf.pow(velocities, 2), 1))
    #norms_shape = tf.concat(0, [tf.shape(velocities)[:1], [1]])
    #velocity_norms = tf.reshape(velocity_norms, norms_shape)
    drag = -drag_coeff * velocities
    g = -g_coeff * np.array([[0,0,1]])
    predicted_velocities = velocities + dt*(drag + g)

    position_dynamics_error = tf.reduce_mean(tf.squared_difference(predicted_positions[:num_states-1,:], positions[1:,:]))
    velocity_dynamics_error = tf.reduce_mean(tf.squared_difference(predicted_velocities[:num_states-1,:], velocities[1:,:]))

    observed_image_positions = tf_operations.apply_cameras_to_points(cameras, positions)
    position_observation_error = tf.reduce_mean(tf.squared_difference(observed_image_positions, image_positions))

    return ((1.0/25.0)*position_observation_error +
            (1.0/(10*(dt**3)/3.0))*position_dynamics_error +
            (1.0/(10*dt))*velocity_dynamics_error)

def check_ball_trajectory():
    image_positions, cameras = load_image_positions_and_cameras()
    world_positions = approximate_world_positions(image_positions, cameras)
    plane_origin, plane_x_axis, plane_y_axis = compute_ball_plane(world_positions)
    plane_positions = project_world_points_to_plane(world_positions, plane_origin, plane_x_axis, plane_y_axis)
    print 'plane origin: ', plane_origin
    print 'plane x axis: ', plane_x_axis
    
    #try to fit the z coordinates to a parabola. I.e:
    #z = -t^2 + bt + c <=> bt + c = z + t^2
    z  = world_positions[:,2].copy()
    z_scale = np.mean(np.abs(z))
    z /= z_scale
    x = plane_positions[:,0].copy()
    x_mean = x.mean()
    x -= x_mean
    x_std = x.std()
    x /= x_std
    
    t  = np.arange(len(z))
    t2 = t*t

    ones = np.ones(len(z))
    A_parabola = np.vstack([t2, t, ones]).transpose()
    y_parabola = z

    #solve A*[a b c]^T = y
    parabola_coeffs, parabola_res = np.linalg.lstsq(A_parabola,y_parabola)[:2]
    parabola_errors = np.matmul(A_parabola, parabola_coeffs) - y_parabola
    auto_corr = np.mean(parabola_errors[1:] * parabola_errors[:-1])
    print 'the parabola is: %ft^2 + %ft + %f' % tuple(parabola_coeffs.tolist())
    print 'avg residue: %f, auto correlation: %f' % (parabola_res/len(t),auto_corr)

    A_line = A_parabola[:,1:]
    y_line = x
    #try to find a line satisfying: a*t + b = x
    line_coeffs, line_res = np.linalg.lstsq(A_line,y_line)[:2]
    line_errors = np.matmul(A_line, line_coeffs) - y_line
    line_auto_corr = np.mean(line_errors[1:] * line_errors[:-1])
    print 'the line is: %ft + %f' % tuple(line_coeffs.tolist())
    print 'avg residue: %f, auto correlation: %f' % (line_res / len(t), line_auto_corr)

    #print 'calculating the drag...'
    #velocity = ((plane_positions[1:,0] - plane_positions[:-1,0])/2) / dt
    #acceleration = ((velocity[1:] - velocity[:-1])/2) / dt
    #we try to find a drag d that best approximates: v[t+1] = v[t] - dt*d*v[t]. I.e:
    #acceleration[t] = -d*velocity[t]
    #for example: acceleration[0] = (velocity[1]-velocity[0])/dt = -d*velocity[0]
    #so we try to find d that minimizes velocity^T * [d] = -acceleration^T
    #fitted_drag_coeff, drag_res = np.linalg.lstsq(velocity[:-1].reshape((-1,1)), -acceleration.reshape((-1,1)))[:2]
    #fitted_drag_coeff = fitted_drag_coeff[0,0]
    #print 'initial height : ', z_scale * parabola_coeffs[2]
    #print 'initial z speed: ', z_scale * parabola_coeffs[1] / dt
    print 'z acceleration : ', 2 * z_scale * parabola_coeffs[0] / (dt**2)
    print 'g_coeff        : ', -g_coeff
    #print 'approx drag    : ', fitted_drag_coeff
    #print 'drag_coeff     : ', drag_coeff
    #print 'drag residue   : ', drag_res

    #predict positions for all frames
    t  = np.arange(len(image_positions))
    t2 = t*t
    ones = np.ones(len(t))
    A_parabola = np.vstack([t2, t, ones]).transpose()
    A_line     = A_parabola[:,1:]
    predicted_z = z_scale * np.matmul(A_parabola, parabola_coeffs)
    predicted_x = x_mean + x_std*np.matmul(A_line, line_coeffs)
    predicted_plane_positions = np.vstack([predicted_x, predicted_z]).transpose()
    predicted_world_positions = map_plane_points_to_world(predicted_plane_positions, plane_origin, plane_x_axis, plane_y_axis)
    predicted_image_positions = np.vstack([c.project_point_to_image(p) for c,p in zip(cameras, predicted_world_positions)])

    print 'height at t=0: ', plane_positions[0,1]
    fig = plt.figure()
    ax1 = fig.add_subplot(511)
    ax2 = fig.add_subplot(512)
    ax3 = fig.add_subplot(513)
    ax4 = fig.add_subplot(514)
    #ax5 = fig.add_subplot(515)
    ax1.plot(plane_positions[:,0], plane_positions[:,1], 'bo')
    ax1.plot(predicted_plane_positions[:,0], predicted_plane_positions[:,1], 'r-')
    ax2.plot(range(len(image_positions)), image_positions[:,0], 'bo')
    ax2.plot(range(len(predicted_image_positions)), predicted_image_positions[:,0], 'r-')
    ax3.plot(range(len(image_positions)), image_positions[:,1], 'bo')
    ax3.plot(range(len(predicted_image_positions)), predicted_image_positions[:,1], 'r-')
    ax4.plot(range(len(plane_positions[:,0])), plane_positions[:,0], 'bo')
    ax4.plot(range(len(predicted_plane_positions[:,0])), predicted_plane_positions[:,0], 'r-')
    #ax5.plot(velocity[:-1], acceleration, 'bo')
    #v_bounds = np.array([velocity.min(), velocity.max()])
    #ax5.plot(v_bounds, -fitted_drag_coeff*v_bounds, 'r-')
    #ax5.plot(range(len(predicted_plane_positions[:,1])), predicted_plane_positions[:,1], 'r-')
    
    #ax5.bar(range(len(acceleration)), acceleration)
    # ax1.plot(t,z_scale*z,'bo')
    # ax2.bar(t, parabola_errors)
    # ax3.plot(t, x_scale*np.matmul(A_line, line_coeffs))
    # ax3.plot(t, x_scale*x, 'bo')
    plt.show()

    #now try to optimize the parabola points to a trajectory using tensor flow
    # print 'optimizing the plane positions...'
    # initial_plane_positions = np.vstack([plane_positions[:,0], z_scale*predicted_z]).transpose()
    # initial_plane_velocities = (initial_plane_positions[1:] - initial_plane_positions[:-1]) / dt
    # initial_plane_states = np.hstack([initial_plane_positions[:-1], initial_plane_velocities]).astype(np.float32)
    # cost = functools.partial(tf_plane_trajectory_error, len(initial_plane_states), np.float32(plane_positions[:-1]))
    # tf_optimizer = TFOptimizer(0.001, 5000, 10**(-5))
    # optimal_plane_states = tf_optimizer.minimize(initial_plane_states, cost, 100)

    # plt.plot(plane_positions[:,0], plane_positions[:,1], 'bo')
    # plt.plot(x, z_scale*predicted_z, 'b-')
    # plt.plot(optimal_plane_states[:,0], optimal_plane_states[:,1], 'r-')
    # plt.show()

    # print 'optimizing the trajectory...'
    # initial_positions = np.hstack([world_positions[:,:2], z_scale*predicted_z.reshape((-1,1))])
    # initial_velocities = (initial_positions[1:] - initial_positions[:-1]) / dt
    # initial_states = np.hstack([initial_positions[:-1], initial_velocities]).astype(np.float32)
    # camera_matrices = np.stack([camera.get_numpy_matrix() for camera in cameras]).astype(np.float32)
    # cost = functools.partial(tf_trajectory_error, len(initial_states), np.float32(image_positions[:-1]), camera_matrices[:-1])
    # tf_optimizer = TFOptimizer(0.001, 50000, 10**(-20))
    # optimal_states = tf_optimizer.minimize(initial_states, cost, 100)

    # print 'initial initial state: ', initial_states[0]
    # print 'optimal initial state: ', optimal_states[0]

    # initial_image_positions = np.vstack([c.project_point_to_image(p) for c,p in zip(cameras, initial_positions)])
    # optimal_image_positions = np.vstack([c.project_point_to_image(p) for c,p in zip(cameras, optimal_states[:,:3])])
    
    # fig = plt.figure()
    # ax1 = fig.add_subplot(221)
    # ax2 = fig.add_subplot(222)
    # ax3 = fig.add_subplot(223)
    # ax4 = fig.add_subplot(224)
    
    # ax1.plot(range(len(world_positions)), world_positions[:,2], 'bo')
    # ax1.plot(range(len(predicted_z)), z_scale*predicted_z, 'b-')
    # ax1.plot(range(len(optimal_states)), optimal_states[:,2], 'r-')
    # ax2.plot(world_positions[:,0], world_positions[:,1], 'bo')
    # ax2.plot(optimal_states[:,0], optimal_states[:,1], 'r-')
    # ax2.plot(optimal_states[:,0], optimal_states[:,1], 'ro')
    # ax3.plot(range(len(image_positions)), image_positions[:,0], 'bo')
    # ax3.plot(range(len(initial_image_positions)), initial_image_positions[:,0], 'b-')
    # ax3.plot(range(len(optimal_image_positions)), optimal_image_positions[:,0], 'r-')
    # ax4.plot(range(len(image_positions)), image_positions[:,1], 'bo')
    # ax4.plot(range(len(initial_image_positions)), initial_image_positions[:,1], 'b-')
    # ax4.plot(range(len(optimal_image_positions)), optimal_image_positions[:,1], 'r-')
    
    # plt.show()
    
    
if __name__ == '__main__':
    np.set_printoptions(precision=3)
    #track_ball()
    check_ball_trajectory()
