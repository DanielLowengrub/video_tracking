from ....source.preprocessing.camera_samples_generator.camera_samples_generator import CameraSamplesGenerator
from ....source.image_processing.image_annotator.soccer_annotation import SoccerAnnotation
import numpy as np
import cv2
import os

# OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
# IMAGE_PARENT_DIRECTORY =  '/Users/daniel/Documents/soccer/images'
# INPUT_DATA_NAME = 'images-8_15-9_31'
# FRAME_INTERVAL = (0,1874)
# FRAME_INDEX = 1846

OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts'
IMAGE_PARENT_DIRECTORY =  '/Users/daniel/Dropbox/soccer/daniel_refactored/test_data'
INPUT_DATA_NAME = 'video7'
FRAME_INTERVAL = (0,274)
FRAME_INDEX = 200

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing', 'data', INPUT_DATA_NAME)
IMAGE_DIRECTORY = os.path.join(IMAGE_PARENT_DIRECTORY, INPUT_DATA_NAME)

IMAGE_FILE = os.path.join(IMAGE_DIRECTORY, 'image_%04d' % FRAME_INDEX)
#IMAGE_FILE = os.path.join(IMAGE_DIRECTORY, 'image_%d' % FRAME_INDEX)
ANNOTATION_FILE = os.path.join(PREPROCESSING_DIRECTORY, 'annotation', 'annotation_%d.npy' % FRAME_INDEX)
ABSOLUTE_HOMOGRAPHY_FILE = os.path.join(PREPROCESSING_DIRECTORY, 'absolute_homography',
                                        'homography_%d_%d' % FRAME_INTERVAL,
                                        'homography_%d.npy' % FRAME_INDEX)

MIN_CAMERAS = 4
MIN_ELLIPSE_AREA = 100
MIN_ELLIPSE_MATCHING_SCORE = 0.6
MAX_ALPHA_SQ_ERROR = 0.1
MAX_ALPHA_STD = 0.2

def test():
    image = cv2.imread(IMAGE_FILE, 1)
    annotation = SoccerAnnotation(np.load(ANNOTATION_FILE))
    H = np.load(ABSOLUTE_HOMOGRAPHY_FILE)

    print 'homography in frame %d:' % FRAME_INDEX
    print H
    
    camera_generator = CameraSamplesGenerator(MIN_CAMERAS, MIN_ELLIPSE_AREA, MIN_ELLIPSE_MATCHING_SCORE,
                                              MAX_ALPHA_SQ_ERROR, MAX_ALPHA_STD)
    camera = camera_generator._compute_camera_matrix(H, annotation)

    return

if __name__ == '__main__':
    test()

