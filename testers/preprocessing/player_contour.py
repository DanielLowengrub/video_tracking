import numpy as np
import cv2
import os
from ...source.image_processing.image_annotator.soccer_annotation import SoccerAnnotation
from ...source.preprocessing.player_contour import PlayerContour
from ..auxillary import primitive_drawing

START_FRAME = 50
END_FRAME = 269
REL_FRAME_INDEX = 90
IMAGE_DIRECTORY = 'test_data/video4'
PREPROCESSING_NAME = 'images_%d_%d' % (START_FRAME, END_FRAME)
PREPROCESSING_BASE_DIR = os.path.join('output/scripts/preprocessing/data', PREPROCESSING_NAME)
ABSOLUTE_HOMOGRAPHY_BASENAME = 'absolute_homography/homography_0_%d/homography_%d.npy' % (END_FRAME-START_FRAME,
                                                                                          REL_FRAME_INDEX)

IMAGE_FILE = os.path.join(IMAGE_DIRECTORY, 'image_%d.png' % REL_FRAME_INDEX)
ABSOLUTE_HOMOGRAPHY_FILE = os.path.join(PREPROCESSING_BASE_DIR, ABSOLUTE_HOMOGRAPHY_BASENAME)
ANNOTATION_FILE = os.path.join(PREPROCESSING_BASE_DIR, 'annotation/annotation_%d.npy' % REL_FRAME_INDEX)

MIN_ELLIPSE_AREA = 100

def load_image_annotation_homography():
    '''
    Return a tuple (image, annotation, homography)

    image - a numpy array with shape (n,m,3) and type np.float32
    annotation - a SoccerAnnotation object
    homography - a numpy array with shape (3,3) and type np.float32
    '''

    image = cv2.imread(IMAGE_FILE, 1)
    annotation_array = np.load(ANNOTATION_FILE)
    annotation = SoccerAnnotation(annotation_array)
    homography = np.load(ABSOLUTE_HOMOGRAPHY_FILE)

    return image, annotation, homography

def find_players():
    image, annotation, H = load_image_annotation_homography()

    cv2.imshow('image', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    player_contours = PlayerContour.build_player_contours_from_annotation(annotation)

    image_with_contours = image.copy()
    cv_contours = [c.get_contour().get_cv_contour() for c in player_contours]
    cv2.drawContours(image_with_contours, cv_contours, -1, (255,0,0), 2)
    cv2.imshow('player contours', image_with_contours)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    cameras = [pc.compute_camera_matrix(H) for pc in player_contours]
    ellipses = [pc.compute_ellipse_approximation(C) for pc,C in zip(player_contours, cameras)]
    contours_ellipses_cameras = [(pc,E,C) for pc,E,C in zip(player_contours, ellipses, cameras)
                                 if E.get_area() >= MIN_ELLIPSE_AREA and
                                 not pc.touches_image_boundary()]

    alphas = []
    matching_scores = []
    for player_contour, ellipse, camera in contours_ellipses_cameras:
        matching_score = player_contour.evaluate_match(ellipse)
        if matching_score > 0.6:
            alphas.append(camera.get_numpy_matrix()[1,2])
            matching_scores.append(matching_score)
            primitive_drawing.draw_ellipse(image_with_contours, ellipse, (0,255,0), thickness=1)

    alphas = np.array(alphas)
    matching_scores = np.array(matching_scores)
    print 'alphas:          ', alphas
    print 'matching scores: ', matching_scores
    print 'alpha mean: ', alphas.mean()
    print 'alpha std:  ', alphas.std()
    print 'max error:  ', ((alphas - alphas.mean())**2).max()
    print 'index of max error: ', np.argmax((alphas - alphas.mean())**2)
    
    cv2.imshow('ellipses', image_with_contours)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

        

if __name__ == '__main__':
    find_players()

