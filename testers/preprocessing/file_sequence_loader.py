import os
import re
import itertools
from ...source.preprocessing import file_sequence_loader

DIRECTORY = 'test_data/video5'
BASENAME_REGEX = re.compile('image_([0-9]+)\.png')

def test_get_file_sequence():
    reverse = True
    file_sequence = file_sequence_loader.get_file_sequence(DIRECTORY, BASENAME_REGEX, reverse)

    print 'the files in directory %s are (reverse=%s):' % (DIRECTORY, reverse)
    print '\n'.join(file_sequence)

def test_get_file_sequence_in_intervals():
    intervals = [(0,3), (4,5)]
    reverse = True
    files_in_intervals = file_sequence_loader.get_file_sequence_in_intervals(DIRECTORY, BASENAME_REGEX, intervals, reverse)

    print 'the files in directory %s are (reverse=%s):' % (DIRECTORY, reverse)
    
    for interval, files_in_interval in itertools.izip(intervals, files_in_intervals):
        print 'the files in interval (%d,%d):' % interval
        files_in_interval = [('   ' + f) for f in files_in_interval]
        print '\n'.join(files_in_interval)
        
if __name__ == '__main__':
    #test_get_file_sequence()
    test_get_file_sequence_in_intervals()
