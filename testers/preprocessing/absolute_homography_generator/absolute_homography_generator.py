from ....source.preprocessing.absolute_homography_generator.absolute_homography_generator import AbsoluteHomographyGenerator
import numpy as np

def test_sample_homographies():
    MIN_FRAME_DISTANCE = 20
    
    H = np.eye(3)
    homographies = [(i,H) for i in range(3,21)] + [(60+i,H) for i in range(0,51,5)] + [(200+i,H) for i in range(0,70,12)]
    max_frame = homographies[-1][0]+100

    homography_generator = AbsoluteHomographyGenerator(MIN_FRAME_DISTANCE)

    print 'testing sample_homographies with:'
    print 'homography indices:'
    print [h[0] for h in homographies]

    sampled_homographies = homography_generator._sample_homographies(max_frame, homographies)

    print 'sampled homography indices:'
    print [h[0] for h in sampled_homographies]

def test_generate_absolute_homographies():
    MIN_FRAME_DISTANCE = 3
    H = np.eye(3)
    indexed_relative_homographies = [((2,10), [H for i in range(2,10)]), ((15,25), [H for H in range(15,25)])]
    indexed_absolute_homographies = [(3,H), (4,H), (8,H), (16,H), (20,H), (21,H)]

    homography_generator = AbsoluteHomographyGenerator(MIN_FRAME_DISTANCE)

    print 'testing generate_absolute_homographies with:'
    print 'relative homography indices:'
    print [h[0] for h in indexed_relative_homographies]
    print 'absolute homography indices:'
    print [h[0] for h in indexed_absolute_homographies]
    
    homography_generator._generate_absolute_homographies(iter(indexed_relative_homographies),
                                                         iter(indexed_absolute_homographies), None)

def test_from_soccer_data():
    RELATIVE_HOMOGRAPHY_DIRECTORY = 'output/scripts/preprocessing/data/images_50_269/relative_homography'
    ABSOLUTE_HOMOGRAPHY_DIRECTORY = 'output/scripts/preprocessing/data/images_50_269/optimized_homography'
    
    MIN_FRAME_DISTANCE = 15

    homography_generator = AbsoluteHomographyGenerator(MIN_FRAME_DISTANCE)
    homography_generator.generate_absolute_homographies(RELATIVE_HOMOGRAPHY_DIRECTORY,
                                                        ABSOLUTE_HOMOGRAPHY_DIRECTORY, None)

if __name__ == '__main__':
    #test_sample_homographies()
    #test_generate_absolute_homographies()
    test_from_soccer_data()
