import os
import itertools
from ...source.preprocessing.preprocessing_data_loader import PreprocessingDataType, PreprocessingDataLoader

OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts'
IMAGE_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/test_data/'
INPUT_DATA_NAME = 'images_50_55'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing', 'data', INPUT_DATA_NAME)

directory_dict = {PreprocessingDataType.IMAGE : os.path.join(IMAGE_PARENT_DIRECTORY, 'video5'),
                  PreprocessingDataType.ANNOTATION : os.path.join(PREPROCESSING_DIRECTORY, 'annotation'),
                  PreprocessingDataType.CAMERA : os.path.join(PREPROCESSING_DIRECTORY, 'camera')}
                  #PreprocessingDataType.PLAYERS_DIRECTORY : os.path.join(PREPROCESSING_DIRECTORY, 'players')}

def basic_test():
    intervals, frame_data = PreprocessingDataLoader.load_data(directory_dict)

    for (ik,jk), Ik in itertools.izip(intervals, frame_data):
        print 'frame data in interval (%d,%d):' % (ik,jk)
        for frame_index, x in enumerate(Ik):
            print '   frame %d: ' % frame_index
            print '      annotation: ', x.annotation
            print '      image shape: ', x.image.shape
            print '      camera: ', x.camera

if __name__ == '__main__':
    basic_test()
