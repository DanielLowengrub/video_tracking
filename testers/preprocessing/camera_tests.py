import numpy as np
import cv2
import os
from ...source.data_structures.contour import Contour
from ...source.image_processing.image_annotator.soccer_annotation import SoccerAnnotation
from ...source.image_processing.contour_generator.contour_from_mask_generator import ContourFromMaskGenerator
from ...source.auxillary.camera_matrix import CameraMatrix
from ...source.auxillary import planar_geometry
from ...source.auxillary import mask_operations
from ..auxillary import primitive_drawing

'''
In this file we test pieces of code that are used to upgrade a planar homography to a camera matrix
'''

START_FRAME = 50
END_FRAME = 269
REL_FRAME_INDEX = 160
IMAGE_DIRECTORY = 'test_data/video4'
PREPROCESSING_NAME = 'images_%d_%d' % (START_FRAME, END_FRAME)
PREPROCESSING_BASE_DIR = os.path.join('output/scripts/preprocessing/data', PREPROCESSING_NAME)
ABSOLUTE_HOMOGRAPHY_BASENAME = 'absolute_homography/homography_0_%d/homography_%d.npy' % (END_FRAME-START_FRAME,
                                                                                          REL_FRAME_INDEX)

IMAGE_FILE = os.path.join(IMAGE_DIRECTORY, 'image_%d.png' % REL_FRAME_INDEX)
ABSOLUTE_HOMOGRAPHY_FILE = os.path.join(PREPROCESSING_BASE_DIR, ABSOLUTE_HOMOGRAPHY_BASENAME)
ANNOTATION_FILE = os.path.join(PREPROCESSING_BASE_DIR, 'annotation/annotation_%d.npy' % REL_FRAME_INDEX)


PLAYER_HEIGHT = 13.0 #the players height in the coordinate system of the field relative to the size of the field. (i.e, in pixel units with respect to the image of the field)
PLAYER_WIDTH = 4.0

MIN_ELLIPSE_AREA = 100

def load_image_annotation_homography():
    '''
    Return a tuple (image, annotation, homography)

    image - a numpy array with shape (n,m,3) and type np.float32
    annotation - a SoccerAnnotation object
    homography - a numpy array with shape (3,3) and type np.float32
    '''

    image = cv2.imread(IMAGE_FILE, 1)
    annotation_array = np.load(ANNOTATION_FILE)
    annotation = SoccerAnnotation(annotation_array)
    homography = np.load(ABSOLUTE_HOMOGRAPHY_FILE)

    return image, annotation, homography

def compute_player_contour_candidates(image, annotation):
    '''
    image - a numpy array with shape (n,m,3) and type np.float32
    annotation - a SoccerAnnotation object

    Return a list of Contour objects which we think could be players.
    '''

    #get a mask containing the pixels which could be players
    players_mask = annotation.get_players_mask()

    #find contours in the mask
    contour_generator = ContourFromMaskGenerator()
    player_contours = contour_generator.generate_contours(image, players_mask)

    #remove all child contours
    player_contours = [Contour(c.get_cv_contour(), image) for c in player_contours]

    return player_contours

def compute_feet_and_head_positions_from_contour(contour):
    '''
    contour - a Contour object

    return a tuple (feet position, head position)
    feet_position, head_position - numpy array with shape (2,) and type np.float32
    '''

    contour_array = contour.get_cv_contour().reshape((-1,2))
    index_of_y_max = contour_array[:,1].argmax()
    feet_y_coord = contour_array[index_of_y_max,1]

    index_of_y_min = contour_array[:,1].argmin()
    head_y_coord = contour_array[index_of_y_min,1]

    mean_x_position = contour_array[:,0].mean()

    feet_position = np.array([mean_x_position, feet_y_coord])
    head_position = np.array([mean_x_position, head_y_coord])
    
    return feet_position, head_position

def compute_player_ellipsoid(absolute_feet_position):
    '''
    absolute_feet_position - a numpy array with shape (2,) and type np.float32
    
    return a tuple (ellipsoid position, ellipsoid axes)
    ellipsoid_position - a numpy array with shape (3,) and type float32
    ellipsoid_axes - a numpy array with shape (3,) and type float32
    '''

    ellipsoid_position = np.append(absolute_feet_position, PLAYER_HEIGHT/2.0)
    ellipsoid_axes = np.array([PLAYER_WIDTH / 2.0, PLAYER_WIDTH / 2.0, PLAYER_HEIGHT / 2.0], np.float32)

    return ellipsoid_position, ellipsoid_axes

def compute_ellipsoid_projection(H, contour):
    '''
    H - a (3,3) numpy array with shape (3,3) and type np.float32 which represents the homography from the absolute plane
        to the image that contains the contour
    contour - a Contour object

    return - an tuple (Ellipse object, CameraMatrix object)

    Assume that the contour is a player and that the lowest point of the contour is the players feet.
    Use the homography matrix H to compute the absolute position of the player. Then place an ellipsoid which approximates
    the average player at that point. Finally, project this ellipsoid onto the image to obtain an ellipse.

    The idea is that if the contour is indeed a player, then the ellipse should be a good approximation to the player contour.
    '''
    #the image is only used for debugging purposes
    image = contour.get_image().copy()
    
    H_inv = np.linalg.inv(H)
    
    #place a player ellipsoid at the feet of the player in absolute coordinates
    image_feet_position, image_head_position = compute_feet_and_head_positions_from_contour(contour)

    # primitive_drawing.draw_point(image, image_feet_position, (0,0,255))
    # primitive_drawing.draw_point(image, image_head_position, (255,0,0))
    # cv2.imshow('feet red, head blue', image)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    absolute_feet_position = planar_geometry.apply_homography_to_point(H_inv, image_feet_position)    
    ellipsoid_position, ellipsoid_axes = compute_player_ellipsoid(absolute_feet_position)

    #use the head positions to compute the camera matrix
    world_head_position = np.append(absolute_feet_position, PLAYER_HEIGHT)
    camera_matrix = CameraMatrix.from_planar_homography(H, world_head_position.reshape((-1,3)),
                                                        image_head_position.reshape((-1,2)))

    #use the camera matrix to project the ellipsoid to the image
    ellipse = camera_matrix.project_ellipsoid_to_image(ellipsoid_position, ellipsoid_axes)
    return ellipse, camera_matrix

def evaluate_match(contour, ellipse):
    '''
    contour - a Contour object
    ellipse - an Ellipse object

    Return a float between 0 and 1. the higher the number the better the ellipse fits the contour.
    '''
    contour_mask = contour.get_mask()
    ellipse_mask = ellipse.get_mask(contour.get_image())

    mask_union = mask_operations.get_union(contour_mask, ellipse_mask)
    mask_intersection = contour_mask * ellipse_mask

    total_area = mask_union.sum()
    overlap_area = mask_intersection.sum()

    overlap_percentage = overlap_area / total_area

    return overlap_percentage

def find_players():
    image, annotation, H = load_image_annotation_homography()

    cv2.imshow('image', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    player_contour_candidates = compute_player_contour_candidates(image, annotation)

    image_with_candidates = image.copy()
    cv_candidates = [c.get_cv_contour() for c in player_contour_candidates]
    cv2.drawContours(image_with_candidates, cv_candidates, -1, (255,0,0), 2)
    cv2.imshow('player candidates', image_with_candidates)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    ellipses_cameras = [compute_ellipsoid_projection(H, c) for c in player_contour_candidates]
    contours_ellipses_cameras = [(contour,ellipse,camera)
                                 for contour,(ellipse,camera) in zip(player_contour_candidates, ellipses_cameras)
                                 if ellipse.get_area() >= MIN_ELLIPSE_AREA]

    alphas = []
    matching_scores = []
    for contour, ellipse, camera in contours_ellipses_cameras:
        matching_score = evaluate_match(contour, ellipse)
        if matching_score > 0.6:
            alphas.append(camera.get_numpy_matrix()[1,2])
            matching_scores.append(matching_score)
            primitive_drawing.draw_ellipse(image_with_candidates, ellipse, (0,255,0), thickness=1)

    alphas = np.array(alphas)
    print 'alphas:          ', alphas
    print 'matching scores: ', np.array(matching_scores)
    print 'alpha mean: ', alphas.mean()
    print 'alpha std:  ', alphas.std()
    print 'max error:  ', ((alphas - alphas.mean())**2).max()
    cv2.imshow('ellipses', image_with_candidates)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

        

if __name__ == '__main__':
    find_players()

