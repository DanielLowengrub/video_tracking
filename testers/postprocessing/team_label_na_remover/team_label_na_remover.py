import numpy as np
from ....source.dynamics.particle_filter_fast.tracked_players import TrackedPlayers
from ....source.postprocessing.team_label_na_remover.team_label_na_remover import TeamLabelNARemover
from ....source.preprocessing.team_labels_generator.team_label import TeamLabel

def test():
    #setup a TrackedPlayers object
    positions = dict()
    positions[0] = np.array([[5,y] for y in range(10)], np.float32)
    positions[1] = np.array([[x,5] for x in range(15)], np.float32)

    #positions[1][9:12,:] = np.nan

    position_variances = dict()
    position_variances[0] = np.zeros((11,2))
    position_variances[1] = np.zeros((11,2))

    velocities = dict()
    velocities[0] = np.zeros((11,2))
    velocities[1] = np.zeros((11,2))

    team_labels = dict()
    A = TeamLabel.TEAM_A
    B = TeamLabel.TEAM_B
    NA = TeamLabel.NA
    
    team_labels[0] = np.array([A, A, NA, NA, NA, NA, NA, B, B, B])
    team_labels[1] = np.array([B, B, B , NA, NA, NA, A , A, A, NA, NA, NA, B, B, B])

    tracked_players = TrackedPlayers(positions, position_variances, velocities, team_labels)

    print 'labelled positions 0:\n', np.vstack([tracked_players.get_team_labels(0).reshape((1,-1)),
                                                tracked_players.get_positions(0).T])
    print 'labelled positions 1:\n', np.vstack([tracked_players.get_team_labels(1).reshape((1,-1)),
                                                tracked_players.get_positions(1).T])


    na_remover = TeamLabelNARemover(2.0)
    na_remover._process_tracked_players(tracked_players)

    print 'labelled positions 0:\n', np.vstack([tracked_players.get_team_labels(0).reshape((1,-1)),
                                                tracked_players.get_positions(0).T])
    print 'labelled positions 1:\n', np.vstack([tracked_players.get_team_labels(1).reshape((1,-1)),
                                                tracked_players.get_positions(1).T])

    return

if __name__ == '__main__':
    np.set_printoptions(precision=2, linewidth=400)
    test()
