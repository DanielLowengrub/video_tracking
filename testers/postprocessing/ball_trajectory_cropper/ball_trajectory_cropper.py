import numpy as np
from ....source.dynamics.particle_filter_fast.tracked_balls import TrackedBalls, BallTrajectory
from ....source.postprocessing.ball_trajectory_cropper.ball_trajectory_cropper import BallTrajectoryCropper

def test():
    #build a TrackedBalls object
    ball_trajectories = dict()

    bt_id = 0
    bt_start_frame = 0
    bt_positions = np.zeros((5,3))
    bt_image_positions = np.array([[x,0] for x in range(5)])

    ball_trajectories[0] = BallTrajectory(bt_id, bt_start_frame, bt_positions, bt_image_positions, False)
    
    bt_id = 1
    bt_start_frame = 4
    bt_positions = np.zeros((5,3))
    bt_image_positions = np.array([[x,0] for x in range(4,9)])

    ball_trajectories[1] = BallTrajectory(bt_id, bt_start_frame, bt_positions, bt_image_positions, False)

    print 'ball trajectories:'
    print '\n'.join(map(str, ball_trajectories.items()))

    tracked_balls = TrackedBalls(ball_trajectories)
    bt_cropper = BallTrajectoryCropper(2.0)
    cropped_balls = bt_cropper._process_tracked_balls(tracked_balls)

    print 'cropped ball trajectories:'
    print '\n'.join(map(str, cropped_balls.get_ball_trajectory_dict().items()))

if __name__ == '__main__':
    test()
