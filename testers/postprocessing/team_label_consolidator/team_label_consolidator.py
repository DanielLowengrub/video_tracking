import numpy as np
from ....source.dynamics.particle_filter_fast.tracked_players import TrackedPlayers
from ....source.postprocessing.team_label_consolidator.team_label_consolidator import TeamLabelConsolidator
from ....source.preprocessing.team_labels_generator.team_label import TeamLabel

def test():
    #setup a TrackedPlayers object
    positions = dict()
    positions[0] = np.array([[5,y] for y in range(15)], np.float32)
    positions[1] = np.array([[x,5] for x in range(15)], np.float32)

    position_variances = dict()
    position_variances[0] = np.zeros(positions[0].shape)
    position_variances[1] = np.zeros(positions[1].shape)

    velocities = dict()
    velocities[0] = np.zeros(positions[0].shape)
    velocities[1] = np.zeros(positions[0].shape)

    team_labels = dict()
    A = TeamLabel.TEAM_A
    B = TeamLabel.TEAM_B
    NA = TeamLabel.NA

    positions[0][5:7,:] = np.nan
    positions[0][11:13,:] = np.nan
    positions[1][6,:] = np.nan
    positions[1][10:12,:] = np.nan
    
    team_labels[0] = np.array([A, A, A, A, A, NA, NA, NA, NA, NA, NA, NA, NA, A, A])
    team_labels[1] = np.array([A, A, A, B, B, B , NA, A , A , A , NA, NA, B , B, B])

    tracked_players = TrackedPlayers(positions, position_variances, velocities, team_labels)

    print 'labelled positions 0:\n', np.vstack([tracked_players.get_team_labels(0).reshape((1,-1)),
                                                tracked_players.get_positions(0).T])
    print 'labelled positions 1:\n', np.vstack([tracked_players.get_team_labels(1).reshape((1,-1)),
                                                tracked_players.get_positions(1).T])


    tl_consolidator = TeamLabelConsolidator()
    tracked_players = tl_consolidator._process_tracked_players(tracked_players)

    for k in tracked_players.get_player_keys():
        print 'labelled positions %d:\n' % k, np.vstack([tracked_players.get_team_labels(k).reshape((1,-1)),
                                                        tracked_players.get_positions(k).T])
    return

if __name__ == '__main__':
    np.set_printoptions(precision=2, linewidth=400)
    test()
