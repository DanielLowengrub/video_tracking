import numpy as np
from ....source.preprocessing.team_labels_generator.team_label import TeamLabel
from ....source.postprocessing.team_label_interpolator.team_label_interpolator import TeamLabelInterpolator

def test_interpolator():
    A = TeamLabel.TEAM_A
    B = TeamLabel.TEAM_B
    NA = TeamLabel.NA
    labels = np.array([A,B,A,NA,NA,A,A])

    print 'labels:'
    print labels

    tli = TeamLabelInterpolator()
    interpolated_labels = tli.interpolate_labels(labels)

    print 'labels:'
    print labels
    print 'interpolated labels:'
    print interpolated_labels

if __name__ == '__main__':
    test_interpolator()
