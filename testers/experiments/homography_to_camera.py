import numpy as np
import cv2
import os
from ...source.preprocessing.soccer_field_embedding_generator.soccer_field_embedding_generator import SoccerFieldEmbeddingGenerator
from ...source.preprocessing.highlighted_shape_generator.highlighted_shape_generator import HighlightedShapeGenerator
from ...source.auxillary import camera_operations, planar_geometry, world_rotation
from ...source.auxillary.soccer_field_geometry import SoccerFieldGeometry
from ...source.auxillary.camera_matrix import CameraMatrix
from ..auxillary import primitive_drawing

#FRAME = 590
#FRAME = 1
#FRAME = 32
#FRAME = 152
#FRAME = 677
#FRAME = 937
#FRAME = 1066
FRAME_LIST = [85, 126, 136, 590, 677, 709, 723, 775, 848, 938, 973, 1026, 1104, 1358, 1644, 1846]
FRAME = FRAME_LIST[0]
IMAGE_DIR = '/Users/daniel/Documents/soccer/images/images-8_15-9_31'
PREPROCESSING_DIR = '/Users/daniel/Documents/soccer/output/preprocessing/data/images-8_15-9_31'

IMAGE_FILE = os.path.join(IMAGE_DIR, 'image_%d.png' % FRAME)
HIGHLIGHTED_SHAPES_DIR = os.path.join(PREPROCESSING_DIR, 'highlighted_shapes/highlighted_shapes_%d' % FRAME)
#SOCCER_EMBEDDING_DIR = os.path.join('output/scripts/preprocessing/data', PREPROCESSING_DIR,
#                                     'soccer_field_embedding/embedding_%d' % FRAME)
SOCCER_EMBEDDING_BASEDIR = os.path.join(PREPROCESSING_DIR,
                                        'soccer_field_embedding/embedding_%d')
SOCCER_EMBEDDING_DIR = SOCCER_EMBEDDING_BASEDIR % FRAME

SOURCE_SHAPE_COLOR = (0,255,0)
TARGET_SHAPE_COLOR = (255,0,0)
POINT_COLOR = (0,0,255)
POINT_RADIUS = 3

def load_absolute_soccer_field():
    '''
    Return a tuple (absolute shapes, absolute grid points)
    '''
    ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)
    SCALE = 6
    FIELD_WIDTH = absolute_image.shape[1]-1
    FIELD_LENGTH = absolute_image.shape[0]-1
    GRID_WIDTH = 20
    GRID_LENGTH = 14
    soccer_field = SoccerFieldGeometry(SCALE, FIELD_WIDTH, FIELD_LENGTH, GRID_WIDTH, GRID_LENGTH)
    
    return absolute_image, soccer_field

def padded_image(image):
    padding = 100
    padded_shape = (image.shape[0]+padding, image.shape[1]+padding, 3)
    padded_image = np.zeros(padded_shape, np.uint8)
    padded_image[:padded_shape[0]-padding, :padded_shape[1]-padding] = image
    return padded_image

def display_homography(image, H, source_shapes, target_shapes, source_grid_points):
    '''
    image - a numpy array with shape (n,m,3) and type np.uint8
    H - a numpy array with shape (3,3) and type np.float32
    source_shapes - a list of PlanarShape objects
    target_shapes - a list of PlanarShape objects
    source_grid_points - a list of points

    Use the homography to transfrom the source shapes to the target image and draw
    both the transformed shapes and the target shapes.
    '''
    image = padded_image(image.copy())
    for shape in target_shapes:
        primitive_drawing.draw_shape(image, shape, TARGET_SHAPE_COLOR)

    translated_shapes = (planar_geometry.apply_homography_to_shape(H,s) for s in source_shapes)
    for shape in translated_shapes:
        primitive_drawing.draw_shape(image, shape, SOURCE_SHAPE_COLOR)

    transformed_grid_points = cv2.perspectiveTransform(source_grid_points.reshape((-1,1,2)), H).reshape((-1,2))
    for point in transformed_grid_points:
        cv2.circle(image, tuple(np.int32(point).tolist()), POINT_RADIUS, POINT_COLOR, thickness=-1)
        
    cv2.imshow('target and transformed shapes', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    return

def load_homography_and_embedding():
    '''
    return a tuple (H, highlighted_shapes, embedding_dict)
    embedding_dict is a dictionary with items (source key, target key)
    '''
    H = np.load(ABSOLUTE_HOMOGRAPHY_FILE)
    return H, EMBEDDING_DICT

def load_homography_and_shapes(soccer_field):
    '''
    return a tuple (homography, source_shapes, target_shapes)
    '''
    H, embedding_dict = SoccerFieldEmbeddingGenerator._load_embedding(SOCCER_EMBEDDING_DIR)
    highlighted_shapes = HighlightedShapeGenerator._load_highlighted_shapes(HIGHLIGHTED_SHAPES_DIR)

    print 'embedding dict: ', embedding_dict
    source_shapes = []
    target_shapes = []

    for source_key, target_key in embedding_dict.iteritems():
        source_shapes.append(soccer_field.get_highlighted_shape(source_key).get_shape())
        target_shapes.append(highlighted_shapes[target_key].get_shape())

    return H, source_shapes, target_shapes

def build_equations(H):
    '''
    H - a homography matrix.
    return - a (2,4) numpy array A and a length 2 numpy array b. It represents two equations satisfied by the coefficients:
    w = [w02,w11,w12,w22]
    of the absolute conic. I.e, Aw^T = b
    '''
    eq0 = np.array([H[0,1]*H[2,0] + H[0,0]*H[2,1], H[1,0]*H[1,1], H[1,1]*H[2,0] + H[1,0]*H[2,1], H[2,0]*H[2,1]])
    eq1 = np.array([2*H[0,0]*H[2,0] - 2*H[0,1]*H[2,1], (H[1,0]**2) - (H[1,1]**2),
                    2*H[1,0]*H[2,0] - 2*H[1,1]*H[2,1], (H[2,0]**2) - (H[2,1]**2)])
    A = np.vstack([eq0, eq1])
    b = -np.array([H[0,0]*H[0,1], H[0,0]**2 - H[0,1]**2])

    print '* computing equations with H:'
    print H
    print 'A:'
    print A
    print 'b: ', b
    return A,b

def combine_equations(Abs):
    '''
    Abs - a list of n tuples (Ai,bi). Ai is a np array with shape (2,4), bi is a np array with shape (2,)

    return - a pair A,b. A is a np array with shape (2n,3+n). b has shape (2n)
    '''
    n = len(Abs)
    A = np.zeros((2*n, 3+n), np.float32)
    b = np.zeros((2*n,), np.float32)
    
    for i, (Ai,bi) in enumerate(Abs):
        A[2*i:2*i+2, :3]  = Ai[:,:3]
        A[2*i:2*i+2, 3+i] = Ai[:,3]
        b[2*i:2*i+2] = bi

    return A,b

def compute_intrinsic_parameters(Hs):
    Abs = [build_equations(H) for H in Hs]
    A,b = combine_equations(Abs)

    print 'combined A:'
    print A
    print 'combined b:'
    print b
    
    column_norms = np.linalg.norm(A, axis=0)
    print 'column norms: '
    print column_norms
    
    A = A / column_norms.reshape((1,-1))

    w, residual = np.linalg.lstsq(A,b)[:2]

    print 'scaled w: ', w
    print 'residual: ', residual
    print 'sq errors: ', (np.matmul(A,w) - b)**2

    w = w / column_norms
    print 'w:'
    print w
    
    aspect = np.sqrt(1 / w[1])
    uo     = -w[0]
    vo     = -w[2]/w[1]

    print 'camera_center: ', np.array([uo,vo])
    print 'aspect: ', aspect

    positions = []
    for w3,H in zip(w[3:],Hs):
        W = np.array([[1,    0,    w[0]],
                      [0,    w[1], w[2]],
                      [w[0], w[2], w3]])

        #print 'W:'
        #print W
        #print 'H^T * W * H:'
        M = np.matmul(H.transpose(), np.matmul(W, H))
        M = M / M[0,0]
        #print M

        f = np.sqrt((W[1,1]*W[2,2] - W[1,1]*(W[0,2]**2) - (W[1,2]**2)) / W[1,1])
        print 'f: ', f

        #print 'W[0,2]^2: ', W[0,2]**2
        #print 'W[1,2]^2: ', W[1,2]**2
        #print 't2^2: ',M[2,2] - (M[0,2]**2 + M[1,2]**2)
        t2 = np.sqrt(M[2,2] - (M[0,2]**2 + M[1,2]**2))
        t = np.array([-M[0,2], -M[1,2], t2])
        positions.append(t)
        print 't: ', t

        Cinv = np.array([[1/f,  0,            -uo/f         ],
                         [0,   -1/(aspect*f),  vo/(aspect*f)],
                         [0,    0,             1            ]])

        RT = np.matmul(Cinv, H)
        RT = RT / np.linalg.norm(RT[:,0])
        r2 = np.cross(RT[:,0], RT[:,1])
        R = np.hstack([RT[:,:2], r2.reshape((-1,1))])

        # print 'C^-1:'
        # print Cinv
        # print 'C^-tC^-1:'
        # X = np.matmul(Cinv.transpose(), Cinv)
        # print X / X[0,0]
        # print 'RT:'
        # print RT
        # print 'RT^t * RT:'
        # print np.matmul(RT.transpose(), RT)
        # print 'R:'
        # print R
        # print 'R col norms: ', np.linalg.norm(R, axis=0)
        # print 'r0 * r1: ', np.dot(R[:,0], R[:,1])
        # print '-R^T*RT[:,2]: ', np.matmul(-R.transpose(), RT[:,2])

    positions = np.vstack(positions)
    print 'mean position: ', positions.mean(axis=0)
    return

def homography_to_camera(H, camera_position):
    print 'converting homography to camera...'
    print 'H:'
    print H

    T = np.eye(3, dtype=np.float32)
    T[:,2] = -camera_position

    print 'T:'
    print T

    H = np.matmul(H, np.linalg.inv(T))
    print 'new H:'
    print H

    yx = H[2,:2]
    if yx[1] < 0:
        yx = -yx
    theta_z = -np.arctan2(yx[0], yx[1])

    print 'theta z: ', theta_z * 180/np.pi

    Rz_inv = world_rotation.rot_z(theta_z)
    H = np.matmul(H, Rz_inv)

    print 'new H:'
    print H

    zy = H[2,1:]
    if zy[0] > 0:
        zy = -zy
    print 'zy: ', zy
    theta_x = -np.arctan2(zy[0], zy[1])

    print 'theta_x: ', theta_x * 180/np.pi

    Rx_inv = world_rotation.rot_x(theta_x)
    H = np.matmul(H, Rx_inv)
    H = H / H[2,2]
    print 'new H:'
    print H
    camera_center = H[:2,2]
    print 'camera center: ', camera_center
    #camera_center = np.array([312, 176], np.float32)
    focus = H[0,0]
    aspect = -H[1,1]/H[0,0]
    
    camera_parameters = camera_operations.CameraParameters(theta_x, theta_z, focus, aspect,
                                                           camera_center, camera_position)

    print 'camera parameters:'
    print camera_parameters

    camera = camera_operations.build_camera(camera_parameters)
    return camera

def compute_rotation(H, camera_position, camera_center):
    print 'computing rotation with:'
    print 'camera position: ', camera_position
    print 'camera center: ', camera_center
    print 'H:'
    print H

    uo, vo = camera_center
    C = np.array([[1, 0, uo],
                  [0, 1, vo],
                  [0, 0, 1 ]])

    T= np.eye(3, dtype=np.float32)
    T[:,2] = -camera_position

    print 'C:'
    print C

    print 'T:'
    print T

    H = np.matmul(np.linalg.inv(C), np.matmul(H, np.linalg.inv(T)))
    H = H / np.linalg.norm(H[2])
    
    print 'new H:'
    print H

    fx = np.linalg.norm(H[0])
    fy = np.linalg.norm(H[1])

    print 'fx: %f, fy: %f' % (fx, fy)
    D = np.diag([fx, fy, 1])
    H = np.matmul(np.linalg.inv(D), H)

    print 'new H:'
    print H
    
    U, S, V = np.linalg.svd(H)

    print 'S:'
    print S
    
    #R = np.matmul(U, np.matmul(np.diag(S),V))
    R = np.matmul(U, V)

    print 'R:'
    print R

    RT = np.matmul(R,camera_position)
    R_RT = np.hstack([R, -RT.reshape((3,1))])
    P = np.matmul(C, np.matmul(D, R_RT))

    return CameraMatrix(P)

def test_homography_to_camera():
    #CAMERA_POSITION = np.array([513, 320, 328])
    CAMERA_POSITION = np.array([513, 500, 325])
    CAMERA_CENTER = np.array([300, 150])
    
    image = cv2.imread(IMAGE_FILE, 1)
    source_image, soccer_field = load_absolute_soccer_field()

    absolute_highlighted_shape_dict = soccer_field.get_highlighted_shape_dict(format_keys=True)
    all_source_shapes = [hs.get_shape() for hs in absolute_highlighted_shape_dict.itervalues()]
    source_grid_points = soccer_field.get_grid_points()

    original_H, source_shapes, target_shapes = load_homography_and_shapes(soccer_field)
    print 'original H:'
    print original_H / original_H[2,2]
    print 'image shape: ', image.shape
    print 'camera center: ', np.array(image.shape, np.float32)/2
    display_homography(image, original_H, all_source_shapes, target_shapes, source_grid_points)

    #camera = homography_to_camera(original_H, CAMERA_POSITION)
    camera = compute_rotation(original_H, CAMERA_POSITION, CAMERA_CENTER)
    
    new_H = camera.get_homography_matrix()
    new_H = new_H / new_H[2,2]

    print 'original H:'
    print original_H / original_H[2,2]

    print 'new H:'
    print new_H

    camera_parameters = camera_operations.compute_camera_parameters(camera)
    print 'camera parameters: '
    print camera_parameters
    
    display_homography(image, new_H, all_source_shapes, target_shapes, source_grid_points)

def test_intrinsic_parameters():
    Hs = []
    for frame in FRAME_LIST:
        H,embedding = SoccerFieldEmbeddingGenerator._load_embedding(SOCCER_EMBEDDING_BASEDIR % frame)
        Hs.append(H)

    compute_intrinsic_parameters(Hs)

    return

if __name__ == '__main__':
    np.set_printoptions(linewidth=150, precision=3)
    test_homography_to_camera()
    #test_intrinsic_parameters()
