import os
import numpy as np
import cv2
import collections
import matplotlib.pyplot as plt
from ..auxillary import primitive_drawing
from ...source.auxillary import mask_operations, planar_geometry, skeletonization
from ...source.auxillary.rectangle import Rectangle
from ...source.auxillary.fit_points_to_shape.ellipse import Ellipse
from ...source.auxillary.fit_points_to_shape.line import Line
from ...source.image_processing.image_annotator.soccer_annotation import SoccerAnnotation
from scipy import ndimage

THETA_VALUES = np.linspace(-np.pi, np.pi, 801)[:-1]
RADIUS_VALUES = np.linspace(0, 358.218, 301)[:-1]
LINE_COLORS = [(255,0,0), (0,255,0), (0,0,255), (150,150,150), (255,255,0)]

HoughBin = collections.namedtuple('HoughBin', ['theta_bin', 'radius_bin', 'theta', 'radius', 'count'])

def theta_bins_to_values(theta_bins):
    return THETA_VALUES[theta_bins - 1]

def radius_bins_to_values(radius_bins):
    return RADIUS_VALUES[radius_bins - 1]

def histogram_shape():
    return (len(THETA_VALUES)+1, len(RADIUS_VALUES)+1)

def display_hough_transform(mask, center, hough_transform, hough_bin_clusters):
    H = planar_geometry.translation_matrix(center)
    
    print 'theta radius clusters: (in degrees)'
    cluster_image = np.zeros(histogram_shape(), np.int32)
    image = mask_operations.convert_to_image(mask)
    mean_line_image = mask_operations.convert_to_image(mask)
    
    for i,cluster in enumerate(hough_bin_clusters):
        print '\ncluster %d:' % i
        color = LINE_COLORS[i % len(LINE_COLORS)]
        weights = np.array([hb.count for hb in cluster])
        total_count = sum(hb.count for hb in cluster)
        theta_bins = np.array([hb.theta_bin for hb in cluster])
        thetas = np.array([hb.theta for hb in cluster])
        radius_bins  = np.array([hb.radius_bin for hb in cluster])
        radii = np.array([hb.radius for hb in cluster])
        
        theta_bin_mean  = np.average(theta_bins, weights=weights)
        theta_mean = np.average(thetas, weights=weights)
        radius_bin_mean = np.average(radius_bins, weights=weights)
        radius_mean = np.average(radii,  weights=weights)
        
        theta_bin_std  = np.sqrt(np.average((theta_bins - theta_bin_mean)**2, weights=weights))
        radius_bin_std = np.sqrt(np.average((radius_bins - radius_bin_mean)**2, weights=weights))

        mean_line = Line(np.cos(theta_mean), np.sin(theta_mean), -radius_mean)
        mean_line = planar_geometry.apply_homography_to_line(H, mean_line)
        primitive_drawing.draw_line(mean_line_image, mean_line, color, 1)
        
        print 'color: ', color
        print 'total count: ', total_count
        print 'theta radius mean: [%f, %f]' % (theta_bin_mean, radius_bin_mean)
        print 'theta radius std:  [%f, %f]' % (theta_bin_std, radius_bin_std)
        print ''
        for hough_bin in cluster:
            cluster_image[hough_bin.theta_bin, hough_bin.radius_bin] = i+1
            line = Line(np.cos(hough_bin.theta), np.sin(hough_bin.theta), -hough_bin.radius)
            line = planar_geometry.apply_homography_to_line(H, line)
            primitive_drawing.draw_line(image, line, color, 1)
            print '   bin: (%d, %d), theta=%f, radius=%f, count=%d' % (hough_bin.theta_bin, hough_bin.radius_bin,
                                                                       hough_bin.theta * 180/np.pi, hough_bin.radius,
                                                                       hough_bin.count)
                
    cv2.imshow('hough transform', np.float32(hough_transform.transpose()) / hough_transform.max())
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    
    p = plt.imshow(hough_transform.transpose(), interpolation="nearest")
    plt.colorbar(p)
    plt.title('hough transform histogram')
    plt.xlabel('theta')
    plt.ylabel('radius')
    plt.show()

    p = plt.imshow(cluster_image.transpose(), interpolation="nearest")
    plt.colorbar(p)
    plt.title('hough transform clusters')
    plt.xlabel('theta')
    plt.ylabel('radius')
    plt.show()

    primitive_drawing.draw_point(image, center, (255,0,0), size=5)
    cv2.imshow('lines', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    primitive_drawing.draw_point(image, center, (255,0,0), size=5)
    cv2.imshow('mean lines', mean_line_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    return

# def display_hough_bin_clusters(hough_bin_clusters):
#     histogram = np.zeros(histogram_shape(), np.int32)
#     for bin_label, cluster in enumerate(hough_bin_clusters):
#         for hough_bin in cluster:
#             histogram[hough_bin.theta, hough_bin.radius] = bin_label

#     p = plt.imshow(histogram.transpose(), interpolation="nearest")
#     plt.colorbar(p)
#     plt.title('hough clusters')
#     plt.xlabel('theta')
#     plt.ylabel('radius')
#     plt.show()

#     return

def build_hough_line_transform(mask, center):
    '''
    mask - a numpy array with shape (n,m) and type np.bool

    return - a numpy array with shape (num theta bins, num radius bins) and type np.int32

    to each point (x,y) in the mask, we add one to the bins (theta,r) such that r = x*cos(theta) + y*sin(theta)
    '''
    theta_radius_hist = np.zeros((len(THETA_VALUES)+1, len(RADIUS_VALUES)+1), np.int32)

    theta = THETA_VALUES
    theta_bins = np.digitize(theta, THETA_VALUES)
    
    cos_theta = np.cos(theta)
    sin_theta = np.sin(theta)

    y_coords, x_coords = np.where(mask)
    points = np.vstack([x_coords, y_coords]).transpose()
    points -= center
    
    #build an array with shape (num points, num theta bins). The entry (i,j) contains x[i]*cos(theta[j]) + y[i]*sin(theta[j])
    for x,y in points:
        radius = x*cos_theta + y*sin_theta

        # print 'radius:'
        # print radius
        
        radius_bins = np.digitize(radius, RADIUS_VALUES)

        # print 'radius bins:'
        # print radius_bins
        
        theta_radius_bins = np.vstack([theta_bins, radius_bins]).transpose()
        theta_radius_bins = theta_radius_bins[radius >= 0]

        # print 'theta radius bins:'
        # print theta_radius_bins
        
        theta_radius_hist[theta_radius_bins[:,0], theta_radius_bins[:,1]] += 1

    return theta_radius_hist

def find_hough_bin_clusters(hough_transform, neighbor_radius, threshold):
    '''
    hough_transform - a numpy array with shape (num theta bins, num radius bins) and type np.int32
    neighbor_radius - a float. if two bins are less than this distance from one another they are considered neighbors
    threshold - a float. only consider bins whose value is at least this high

    return - a list of lists of HoughBin objects. Each list represents a cluster of bins.
    '''
    print 'finding clusters...'
    cv2.imshow('hough transform', np.float32(hough_transform.transpose()) / hough_transform.max())
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    r = 10
    x_coords = np.arange(-r,r+1,1)
    y_coords = np.arange(-r,r+1,1)
    XX,YY = np.meshgrid(x_coords, y_coords)
    footprint = (XX**2 + YY**2) < r**2
    print 'footprint'
    print footprint.astype(np.int32)

    hough_transform_max = ndimage.filters.maximum_filter(hough_transform, footprint=footprint)
    cv2.imshow('hough transform max', np.float32(hough_transform_max.transpose()) / hough_transform_max.max())
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    hough_peak_mask = np.logical_and(hough_transform_max == hough_transform, hough_transform > threshold)
    cv2.imshow('hough peaks', np.float32(hough_peak_mask.transpose()))
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    print 'local maxima values:'
    print hough_transform[hough_peak_mask]

    hough_peak_y, hough_peak_x = np.where(hough_peak_mask)
    hough_peaks = np.vstack([hough_peak_x, hough_peak_y]).transpose()
    hough_transform_rect = Rectangle.from_array(hough_transform)
    
    bin_clusters = []
    for hough_peak in hough_peaks:
        print 'processing peak: ', hough_peak
        #get the neighborhood of the peak
        peak_value = hough_transform[hough_peak[1], hough_peak[0]]
        peak_threshold = 0.75 * peak_value
        top_left = hough_peak - np.array([r,r])
        peak_nbd_rect = Rectangle.from_top_left_and_shape(top_left, footprint.shape)
        cropped_peak_nbd_rect = hough_transform_rect.intersection(peak_nbd_rect)
        print '   cropped nbd rect: ', cropped_peak_nbd_rect
        
        cropped_footprint = peak_nbd_rect.get_relative_slice(footprint, cropped_peak_nbd_rect)
        cropped_peak_nbd_mask = (cropped_peak_nbd_rect.get_slice(hough_transform) > peak_threshold) * cropped_footprint
        print '   cropped peak nbd mask:'
        print cropped_peak_nbd_mask.astype(np.int32)
        
        rel_theta_bins, rel_radius_bins = np.where(cropped_peak_nbd_mask)

        theta_bins = rel_theta_bins + cropped_peak_nbd_rect.get_top_left()[1]
        radius_bins = rel_radius_bins + cropped_peak_nbd_rect.get_top_left()[0]

        thetas = theta_bins_to_values(theta_bins)
        radii  = radius_bins_to_values(radius_bins)
        counts = hough_transform[theta_bins, radius_bins]

        bin_clusters.append([HoughBin(*args) for args in zip(theta_bins, radius_bins, thetas, radii, counts)])

    return bin_clusters
    #################################
    #this is a mask indicating which of the bins are above the threshold
    hough_mask = hough_transform > threshold

    #dilate the hough_mask so that bins that are within neighbor_radius are in the same cluster
    kernel = np.ones((2*neighbor_radius + 1, 2*neighbor_radius + 1), np.bool)
    dilated_mask = ndimage.morphology.binary_dilation(hough_mask, kernel)

    # p = plt.imshow(np.int32(dilated_mask.transpose()), interpolation="nearest")
    # plt.colorbar(p)
    # plt.title('dilated mask')
    # plt.xlabel('theta')
    # plt.ylabel('radius')
    # plt.show()

    
    #find connected components in the dilated hough mask
    #labels is a numpy array with the same shape as hough_mask. It has value 0 where the mask is zero, and values:
    #[1,...,num_labels] at each of the different labels
    structure = np.ones((3,3), np.bool)
    labels, num_labels = ndimage.measurements.label(dilated_mask, structure)
    labels *= np.uint64(hough_mask)
    
    print 'the labelled hough bins:'
    # p = plt.imshow(labels.transpose(), interpolation="nearest")
    # plt.colorbar(p)
    # plt.xlabel('theta')
    # plt.ylabel('radius')
    # plt.show()

    bin_clusters = []
    for bin_label in range(1,num_labels+1):
        theta_bins, radius_bins = np.where(labels == bin_label)
        print 'theta radius bins: ', zip(theta_bins, radius_bins)
        thetas = theta_bins_to_values(theta_bins)
        radii  = radius_bins_to_values(radius_bins)
        counts = hough_transform[theta_bins, radius_bins]

        bin_clusters.append([HoughBin(*args) for args in zip(theta_bins, radius_bins, thetas, radii, counts)])

    return bin_clusters

def remove_noisy_clusters(hough_bin_clusters, min_counts, max_theta_std, max_radius_std):
    '''
    hough_bin_clusters
    '''
    
def one_point_test():
    mask = np.zeros((400,600), np.bool)
    mask[200,200] = True
    center = np.array([mask.shape[1] / 2, mask.shape[0]/2])
    hough_transform = build_hough_line_transform(mask, center)

    cv2.imshow('hough transform', np.float32(hough_transform.transpose()))
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    
    p = plt.imshow(hough_transform.transpose(), interpolation="nearest")
    plt.colorbar(p)
    plt.show()

def one_line_test():
    mask = np.zeros((400,600), np.bool)
    #center = np.array([mask.shape[1] / 2, mask.shape[0]/2])
    center = np.array([0,0])
    mask[50:300,300] = True
    #mask[200,100:500] = True

    cv2.imshow('mask', np.float32(mask))
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    
    hough_transform = build_hough_line_transform(mask, center)

    display_hough_transform(mask, center, hough_transform, 200)
    return


def one_ellipse_test():
    mask = np.zeros((400,600), np.float32)
    center = np.array([200,200], np.float32)
    axes = np.array([150,50])
    angle = 0
    ellipse = Ellipse.from_geometric_parameters(center, axes, angle)
    print 'ellipse: ', ellipse
    primitive_drawing.draw_ellipse(mask, ellipse, 1, thickness=1)
    mask = mask > 0
    #mask[200,100:500] = True
    #mask[50:300,300] = True
    
    cv2.imshow('mask', np.float32(mask))
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    center = np.array([0,0])
    hough_transform = build_hough_line_transform(mask, center)

    display_hough_transform(mask, center, hough_transform, 200)
    return

def soccer_test():
    FRAME = 162 + 0
    #FRAME = 162 + 30
    #FRAME = 162 + 99
    #FRAME = 162 + 277
    #FRAME = 162 + 313
    #FRAME = 597 + 46
    #FRAME = 597 + 196
    #FRAME = 843 + 30
    #FRAME = 843 + 70
    #FRAME = 843 + 123
    #FRAME = 843 + 167
    #FRAME = 1141 + 0
    #FRAME = 1141 + 34
    #FRAME = 1141 + 56
    #FRAME = 1141 + 165
    #FRAME = 1141 + 206
    #FRAME = 1141 + 317
    ANNOTATION_FILENAME = os.path.join('/Users/daniel/Documents/soccer/output/preprocessing_full/data/images-0_00-1_00',
                                       'stage_0/annotation/interval_0_1498', 'frame_%d' % FRAME, 'annotation.npy')
    annotation = SoccerAnnotation(np.load(ANNOTATION_FILENAME))
    mask = annotation.get_sidelines_mask() > 0

    cv2.imshow('mask', np.float32(mask))
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    center = np.array([mask.shape[1] / 2, mask.shape[0]/2])
    mask = skeletonization.compute_skeleton(mask)
    #closing_kernel = np.ones((5,5), np.bool)
    #mask = ndimage.binary_closing(mask, closing_kernel)
    mask_color = mask_operations.convert_to_image(mask)
    primitive_drawing.draw_point(mask_color, center, (0,255,0), size=5)
    
    cv2.imshow('skeletonized mask with center', mask_color)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    hough_transform = build_hough_line_transform(mask, center)
    hough_bin_clusters = find_hough_bin_clusters(hough_transform, neighbor_radius=3, threshold=60)

    print 'the hough bin clusters:'
    print '\n'.join(map(str, hough_bin_clusters))
    
    display_hough_transform(mask, center, hough_transform, hough_bin_clusters)
    #display_hough_bin_clusters(hough_bin_clusters)
    return

if __name__ == '__main__':
    #one_point_test()
    #one_line_test()
    #one_ellipse_test()
    soccer_test()
