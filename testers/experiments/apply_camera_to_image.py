import cv2
import numpy as np
from ...source.auxillary.camera_matrix import CameraMatrix
from ...source.auxillary import planar_geometry

def build_camera(camera_position, camera_direction, focus, camera_position_image_coords):
    '''
    camera_position - a numpy array with length 3. It is the camera position in world coords.
    camera_direction - a numpy array with length 3 (and norm 1). It is the camera direction in world coords
    focus - a float
    camera_position_image_coords - a numpy array with length 2. It is the camera position in image coords

    We use the following coordinate systems:
    WORLD:               CAMERA:     IMAGE:

      Z                 y          ------- u
      |                 | z       |    y
      |____ X           |/        |    |
      /                  ---x     |     -- x
     /                            v 
    Y
    
    The world coords are (X,Y,Z), the camera coords are (x,y,z) and the image coords are (u,v)
    Note that XxY = -Z. I.e, we are using a coordinate system with orientation -1.
    '''
    Rinv_z = camera_direction
    Rinv_y = np.array([0,0,1])
    Rinv_x = np.cross(Rinv_y, Rinv_z)
    Rinv_y = -np.cross(Rinv_x, Rinv_z)

    Rinv_x /= np.linalg.norm(Rinv_x)
    Rinv_y /= np.linalg.norm(Rinv_y)
    Rinv_z /= np.linalg.norm(Rinv_z)
    Rinv = np.vstack([Rinv_x,Rinv_y,Rinv_z]).transpose()
    
    R = np.linalg.inv(Rinv)
    
    T  = -camera_position
    RT = np.matmul(R, T)
    R_RT = np.hstack([R, RT.reshape((3,1))])
    
    f = focus
    u0,v0 = camera_position_image_coords
    C = np.array([[f,   0,  u0],
                  [0,  -f,  v0],
                  [0,   0,  1]])

    P = np.matmul(C, R_RT)

    return CameraMatrix(P)

def apply_camera(image, camera, output_image):
    H = camera.get_homography_matrix()
    output_image = cv2.warpPerspective(image, H, output_image.shape[:2], dst=output_image)
    return output_image

def build_camera_direction(theta_x, theta_z):
    '''
    theta_x - rotation around the x axis
    theta_z - rotation around the z axis

    If both angles are zero, the returned vector is [0, -1, 0]
    theta_z rotates this vector clockwise around the z axis
    theta_x rotates this vector clockwise around the x axis
    return - a numpy array with length 3
    '''
    return np.array([np.cos(theta_x)*np.sin(theta_z),
                     -np.cos(theta_x)*np.cos(theta_z),
                     -np.sin(theta_x)])

def test_apply_camera():
    IMAGE_FILENAME = 'test_data/test_pattern.png'
    CAMERA_POSITION  = np.array([64, 150, 50])

    THETA_X = 45 * np.pi/180
    THETA_Z = -20 * np.pi/180
    FOCUS = 150
    OUTPUT_IMAGE = np.zeros((256, 256, 3), np.uint8)
    CAMERA_CENTER_IMAGE_COORDS = np.array(OUTPUT_IMAGE.shape[:2]) / 2

    image = cv2.imread(IMAGE_FILENAME, 1)
    camera_direction = build_camera_direction(THETA_X, THETA_Z)
    camera = build_camera(CAMERA_POSITION, camera_direction, FOCUS, CAMERA_CENTER_IMAGE_COORDS)
    output_image = apply_camera(image, camera, OUTPUT_IMAGE)

    # cv2.imshow('original and warped', np.hstack([image, output_image]))
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    cv2.imshow('warped', output_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    
    return

if __name__ == '__main__':
    test_apply_camera()
               
