import numpy as np
import cv2
import os
from ..auxillary import camera_generator
from ..auxillary.camera_matrix import CameraMatrix
from ...source.auxillary.soccer_field_geometry import SoccerFieldGeometry
from ...source.auxillary import fit_camera_to_homography, mask_operations

#SIDELINE_MASK_FILENAME = '/Users/daniel/Dropbox/soccer/daniel_refactored/test_data/sideline_mask.png'
#SIDELINE_MASK = cv2.imread(SIDELINE_MASK_FILENAME, 0)
SOCCER_FIELD = SoccerFieldGeometry(6, 743, 479, 20, 14)
SIDELINE_MASK = SOCCER_FIELD.get_sideline_mask(1)

OUTPUT_IMAGE_SHAPE = (624, 352)
# CAMERA_FILENAME = os.path.join('/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts/preprocessing/data/video7',
#                                'camera/camera_0_274/camera_42.npy')
FRAME = 700
IMAGE_FILENAME = os.path.join('/Users/daniel/Documents/soccer/images/images-8_15-9_31/image_%d.png' % FRAME)
CAMERA_FILENAME = os.path.join('/Users/daniel/Documents/soccer/output/preprocessing/data/images-8_15-9_31',
                               'camera/camera_0_1874/camera_%d.npy' % FRAME)

def get_camera_coordinate_change(C):
    #we want to transform the camera parameters so that
    #the diagonal of C is (focus, -focus, 1), where focus is some positive number.
    M = np.eye(3)
    if C[0,0] < 0:
        M[0,0] = -1

    if C[1,1] > 0:
        M[1,1] = -1

    if C[2,2] < 0:
        M[2,2] = -1

    return M

def compute_angles_from_camera_direction(camera_direction):
    theta_x = np.arctan2(-camera_direction[2], -camera_direction[1])
    theta_z = np.arctan2(camera_direction[0],  -camera_direction[1])
    return theta_x, theta_z

def compute_camera_parameters(camera):
    '''
    return - a tuple (position, theta_x, theta_z, focus, camera_center_in_image_coords)
    '''
    position = camera.get_camera_position()
    C = camera.get_internal_parameters()
    R = camera.get_world_to_camera_rotation_matrix()

    # print 'C:'
    # print C

    M = get_camera_coordinate_change(C)

    # print 'M:'
    # print M
    
    C = np.matmul(C, M)
    R = np.matmul(M, R)

    #scale C so that C[2,2]=1
    C /= C[2,2]

    # print 'C:'
    # print C
    # print 'R:'
    # print R
    
    #the camera direction is where the z axis is pointing
    Rinv = np.linalg.inv(R)
    camera_direction = Rinv[:,2]

    # print 'Rinv:'
    # print Rinv

    # print 'camera direction: ', camera_direction
    
    #get the angles from the camera direction
    theta_x, theta_z = compute_angles_from_camera_direction(camera_direction)

    camera_center = C[:2, 2]
    focus = np.array([C[0,0], -C[1,1]])
    
    return position, theta_x, theta_z, focus, camera_center

def apply_camera_to_sideline_mask(camera):
    sideline_image = SIDELINE_MASK.astype(np.uint8)*255
    output_image = camera_generator.apply_camera(SIDELINE_MASK, camera, OUTPUT_IMAGE_SHAPE, pad_with_zero=False)
    return output_image

if __name__ == '__main__':
    image  = cv2.imread(IMAGE_FILENAME, 1)
    camera = CameraMatrix(np.load(CAMERA_FILENAME))
    old_C = camera.get_internal_parameters()
    old_R = camera.get_world_to_camera_rotation_matrix()
    # old_T = camera.get_camera_position()
    # old_H = camera.get_homography_matrix()
    # C_opt, R_opt = fit_camera_to_homography.fit_camera_to_homography(old_C, old_R, old_T, old_H)
    # camera_opt = CameraMatrix.from_position_rotation_internal_parameters(old_T, R_opt, C_opt)

    output_mask = apply_camera_to_sideline_mask(camera)

    position, theta_x, theta_z, focus, camera_center = compute_camera_parameters(camera)

    print 'position: ', position
    print 'theta_x: %f, theta_z: %f, focus: %s' % (theta_x*180/np.pi, theta_z*180/np.pi, focus)
    print 'camera_center: ', camera_center
    print 'aspect: ', focus[1] / focus[0]
    
    new_camera_direction = camera_generator.build_camera_direction(theta_x, theta_z)
    new_camera = camera_generator.build_camera(position, new_camera_direction, focus, camera_center)
    
    new_C = new_camera.get_internal_parameters()
    new_R = new_camera.get_world_to_camera_rotation_matrix()
    #new_C[0,1]= 88
    #new_camera = CameraMatrix.from_position_rotation_internal_parameters(position, new_R, new_C)
    #new_C = new_camera.get_internal_parameters()
    #new_R = new_camera.get_world_to_camera_rotation_matrix()
    print 'new P:'
    print new_camera._camera_matrix / new_camera._camera_matrix[2,3]
    print new_camera._camera_matrix[1,:2] / new_camera._camera_matrix[2,:2]
    print 'new C:'
    print new_C
    print 'new R:'
    print new_R
    print 'old P:'
    print camera._camera_matrix
    print camera._camera_matrix[1,:2] / camera._camera_matrix[2,:2]
    print 'old C:'
    print old_C / old_C[2,2]
    print 'old R:'
    print old_R
    # print 'opt P:'
    # print camera_opt._camera_matrix / camera_opt._camera_matrix[2,3]
    # print 'opt C:'
    # print C_opt / C_opt[2,2]
    # print 'opt R:'
    # print R_opt

    new_output_mask = apply_camera_to_sideline_mask(new_camera)
    output_image = image.copy()
    new_output_image = image.copy()
    mask_operations.draw_mask(output_image, output_mask, (255,0,0), 0.5)
    mask_operations.draw_mask(new_output_image, new_output_mask, (255,0,0), 0.5)
    
    cv2.imshow('sidelines', np.hstack([output_image, new_output_image]))
    cv2.waitKey(0)
    cv2.destroyAllWindows()

