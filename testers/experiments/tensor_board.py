import numpy as np
import tensorflow as tf

def build_loss_op(X, Y):
    with tf.name_scope('linear_regression'):
        W = tf.Variable(tf.random_normal([1]), name='weights')
        tf.summary.scalar('weights', W[0])

        b = tf.Variable(tf.zeros([1]), name='biases')
        tf.summary.scalar('biases', b[0])
            
        Y_pred = tf.add(tf.multiply(X, W), b)

    with tf.name_scope('loss'):
        loss_op = tf.reduce_mean(tf.pow(Y_pred - Y, 2), name='loss_op')
        tf.summary.scalar('cost', loss_op)
    
    return loss_op

def test():
    log_path = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/tester/tensorboard/linear_regression'
    n_observations = 100
    xs = np.linspace(-3, 3, n_observations)
    ys = 3*xs - 2  + np.random.uniform(-0.5, 0.5, n_observations)

    with tf.name_scope('input'):
        X = tf.placeholder(tf.float32, name='X')
        Y = tf.placeholder(tf.float32, name='Y')

    loss_op = build_loss_op(X,Y)
    
    learning_rate = 0.001
    optimizer = tf.train.GradientDescentOptimizer(learning_rate, name='optimizer').minimize(loss_op)

    #build the summary op and a summary writer
    summary_op = tf.summary.merge_all()
    writer = tf.summary.FileWriter(log_path, graph=tf.get_default_graph())
                                    
    n_epochs = 1000
    with tf.Session() as sess:
        # Here we tell tensorflow that we want to initialize all
        # the variables in the graph so we can use them
        sess.run(tf.global_variables_initializer())

        # Fit all training data
        prev_training_cost = 0.0
        for epoch_i in range(n_epochs):
            for (x, y) in zip(xs, ys):
                _, cost, summary = sess.run([optimizer, loss_op, summary_op], feed_dict={X: x, Y: y})

            if epoch_i % 1 == 0:
                print 'epoch %d: cost=%f' % (epoch_i, cost)
                writer.add_summary(summary, epoch_i)

            # Allow the training to quit if we've reached a minimum
            if np.abs(prev_training_cost - cost) < 0.000001:
                break
        
            prev_training_cost = cost

if __name__ == '__main__':
    test()
