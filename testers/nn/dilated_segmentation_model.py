import numpy as np
import cv2
import matplotlib as mpl
import matplotlib.pyplot as plt
from ...source.auxillary.fit_points_to_shape.line import Line
from ...source.auxillary import iterator_operations
from ..auxillary import primitive_drawing
from ...source.nn.tf_model_controller import TFModelController
from ...source.nn.dilated_segmentation_model import DilatedSegmentationModel
from ...source.nn.synthetic_data_set import SyntheticDataSet
from ...source.auxillary import planar_geometry

class ToyDataSet(SyntheticDataSet):
    IMAGE_SIZE = 64
    NUM_LINES_RANGE = (2,3)
    NUM_ELLIPSES_RANGE = (0,3)
    
    LINE_THICKNESS = 1
    LINE_RADIUS_RANGE = (0,IMAGE_SIZE/2 - 10)
    ELLIPSE_RADIUS_RANGE = (4,10)
    
    LINE_COLOR = 255
    ELLIPSE_COLOR = 255
    
    BACKGROUND = 0
    LINE = 1
    ELLIPSE = 2
    UNKNOWN = 3

    def __init__(self, batch_size, batches_per_epoch):
        super(ToyDataSet, self).__init__(batch_size, batches_per_epoch)

    def _build_training_pair(self):
        #initialize the image to the background color, and the annotation to "unknown"
        image = np.zeros((self.IMAGE_SIZE, self.IMAGE_SIZE), np.uint8)
        annotation = np.ones((self.IMAGE_SIZE, self.IMAGE_SIZE), np.int32) * self.UNKNOWN
        center = np.array([self.IMAGE_SIZE, self.IMAGE_SIZE], np.float32) / 2
        
        #first draw some random lines on the image and on the annotation
        line_mask = np.zeros(annotation.shape, np.uint8)
        num_lines = np.random.randint(*self.NUM_LINES_RANGE)
        radius = np.random.uniform(*self.LINE_RADIUS_RANGE, size=(num_lines,))
        angle  = np.random.uniform(-np.pi, np.pi, size=(num_lines,))
        direction_perp = np.vstack([np.cos(angle), np.sin(angle)]).transpose()
        position = center + direction_perp * radius.reshape((-1,1))

        for pos, dir_perp in zip(position, direction_perp):
            direction = planar_geometry.perp(dir_perp)
            line = Line.from_point_and_direction(pos, direction)
            primitive_drawing.draw_line(image, line, self.LINE_COLOR, self.LINE_THICKNESS)
            primitive_drawing.draw_line(line_mask, line, 1, self.LINE_THICKNESS)

        #now draw some random ellipses
        ellipse_mask   = np.zeros(annotation.shape, np.uint8)
        num_ellipses   = np.random.randint(*self.NUM_ELLIPSES_RANGE)
        center_x       = np.random.randint(0, image.shape[1]-1, size=(num_ellipses,))
        center_y       = np.random.randint(0, image.shape[0]-1, size=(num_ellipses,))
        axes_array     = np.random.randint(*self.ELLIPSE_RADIUS_RANGE, size=(num_ellipses,2))
        angle_array    = np.random.randint(0, 90, size=(num_ellipses,))
        for x,y,axes,angle in zip(center_x, center_y, axes_array, angle_array):
            cv2.ellipse(image, (x,y), tuple(axes.tolist()), angle, 0, 360, self.ELLIPSE_COLOR, thickness=-1)
            cv2.ellipse(ellipse_mask, (x,y), tuple(axes.tolist()), angle, 0, 360, 1, thickness=-1)

        line_mask = line_mask > 0
        ellipse_mask = ellipse_mask > 0
        
        #now choose some random pixels that will be labelles
        num_pixels_per_label = min(np.sum(line_mask), np.sum(ellipse_mask))
        
        foreground_mask = np.logical_or(line_mask, ellipse_mask)
        background_mask = np.logical_not(foreground_mask)

        line_indices = np.where(line_mask.reshape((-1,)))[0]
        if len(line_indices) > num_pixels_per_label:
            line_indices = np.random.choice(line_indices, num_pixels_per_label)

        ellipse_indices = np.where(ellipse_mask.reshape((-1,)))[0]
        if len(ellipse_indices) > num_pixels_per_label:
            ellipse_indices = np.random.choice(ellipse_indices, num_pixels_per_label)
        
        background_indices = np.where(background_mask.reshape((-1,)))[0]
        background_indices = np.random.choice(background_indices, num_pixels_per_label)

        annotation_flat = annotation.reshape((-1,))
        annotation_flat[line_indices] = self.LINE
        annotation_flat[ellipse_indices] = self.ELLIPSE
        annotation_flat[background_indices] = self.BACKGROUND
        
        return image, annotation

def display_test_data(image, segmentation):     
    fig = plt.figure()
    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)

    cmap = mpl.colors.ListedColormap(['k', 'w', 'b', '0.5'])
    norm = mpl.colors.BoundaryNorm(range(5), cmap.N)
    
    ax1.imshow(image, interpolation='nearest')
    ax1.set_ylim([image.shape[0], 0])
    ax2.imshow(segmentation, cmap=cmap, norm=norm, interpolation='nearest')
    ax2.set_ylim([segmentation.shape[0], 0])

    plt.show()

def display_segmentations(images, segmentations):
    num_per_row = 4
    max_rows = 4
    num_rows = min(4, len(images) / num_per_row)

    cmap = mpl.colors.ListedColormap(['k', 'w', 'b', '0.5'])
    norm = mpl.colors.BoundaryNorm(range(5), cmap.N)

    fig, axes_array = plt.subplots(num_rows, 2*num_per_row, sharex='col', sharey='row')

    for row in range(num_rows):
        axes_in_row = axes_array[row]
        
        for col in range(4):
            img = images[num_per_row*row + col]
            seg = segmentations[num_per_row*row + col]
            
            ax_img = axes_in_row[2*col]
            ax_seg = axes_in_row[2*col + 1]

            ax_img.imshow(img, interpolation='nearest')
            ax_img.set_ylim([img.shape[0], 0])
            ax_img.set_axis_off()
            ax_seg.imshow(seg, cmap=cmap, norm=norm, interpolation='nearest')
            ax_seg.set_ylim([seg.shape[0], 0])
            ax_seg.set_axis_off()
            
    plt.axis('off')
    #plt.savefig('/Users/daniel/Documents/predicted_sideline_type.png', bbox_inches='tight')
    plt.show()

def test_training_data_set():
    training_data_set = ToyDataSet(20, 1)
    images, segmentations = training_data_set.next_batch()
    print 'images shape: ', images.shape
    print 'segmentations shape: ', segmentations.shape
    
    display_segmentations(images, segmentations)
    return

def train_model():
    model_file = '/home/daniel/Documents/tensorflow/models/dilated_segmentation_model/model.chk'
    #model_file = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/tester/nn_models/dilated_segmentation_model/model.chk'
    log_dir    = '/home/daniel/Documents/tensorflow/logs/dilated_segmentation_model'
    #log_dir    = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/tester/tf_logs/dilated_segmentation_model'

    batch_size = 100
    training_data_set   = ToyDataSet(batch_size, 100)
    validation_data_set = ToyDataSet(batch_size, 5)
    
    learning_rate = 0.0005
    max_training_epochs = 20
    num_batches_per_summary = 20
    decay_rate = 0.7 #every 1000 steps, lr = lr*0.5
    decay_step = 1000
    model_controller = TFModelController(learning_rate, max_training_epochs, num_batches_per_summary)
    segmentation_model = DilatedSegmentationModel(batch_size)

    #train the model
    model_controller.train(model_file, log_dir, segmentation_model, training_data_set, validation_data_set)

    return

def predict():
    model_file = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/tester/nn_models/dilated_segmentation_model/model.chk'
    batch_size = 100
    learning_rate = 0.0001
    max_training_epochs = 5000
    num_epochs_per_summary = 100
    model_controller = TFModelController(learning_rate, max_training_epochs, num_epochs_per_summary)
    segmentation_model = DilatedSegmentationModel(batch_size)

    print 'generating data...'
    images_and_segs = [generate_training_data() for i in xrange(batch_size)]
    images = [x[0] for x in images_and_segs]
    segmentations =  [x[1] for x in images_and_segs]

    print 'predicting...'
    predicted_segmentations = model_controller.predict(model_file, segmentation_model, iter(images))

    print 'drawing...'
    display_segmentations(images, predicted_segmentations)
    # for image, segmentation in zip(images[:20], predicted_segmentations[:20]):
    #     print segmentation
    #     display_segmentation(image, segmentation)
    
    return

if __name__ == '__main__':
    #test_training_data_set()
    train_model()
    #predict()
