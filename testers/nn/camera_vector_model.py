import numpy as np
import cv2
from ..auxillary import camera_generator
from ...source.nn.camera_vector_model import CameraVectorModel
from ...source.nn.tf_model_controller import TFModelController
from ...source.auxillary import iterator_operations

IMAGE_FILENAME = 'test_data/test_pattern_tiny.png'
IMAGE = cv2.imread(IMAGE_FILENAME, 1)
IMAGE = cv2.cvtColor(IMAGE, cv2.COLOR_BGR2GRAY)

CAMERA_POSITION  = np.array([16, 42, 20])

THETA_X = 45 * np.pi/180
THETA_Z_RANGE = (-20 * np.pi/180, 20 * np.pi/180)
FOCUS = 25
OUTPUT_IMAGE_SHAPE = (64,64)

MAX_BALLS = 10
BALL_RADIUS_RANGE = (2, 5)

CAMERA_CENTER_IMAGE_COORDS = np.array(OUTPUT_IMAGE_SHAPE) / 2

def generate_random_data():
    '''
    return - image, camera_vector
    image - a gray image
    camera_vector - a length 3 vector
    '''
    theta_z = np.random.uniform(*THETA_Z_RANGE)
    camera_direction = camera_generator.build_camera_direction(THETA_X, theta_z)
    camera = camera_generator.build_camera(CAMERA_POSITION, camera_direction, FOCUS, CAMERA_CENTER_IMAGE_COORDS)

    output_image = camera_generator.apply_camera(IMAGE, camera, OUTPUT_IMAGE_SHAPE)

    num_balls = np.random.randint(MAX_BALLS+1)
    if num_balls > 0:
        ball_center_x = np.random.randint(0, output_image.shape[1]-1, size=(num_balls,))
        ball_center_y = np.random.randint(0, output_image.shape[0]-1, size=(num_balls,))
        ball_radius   = np.random.randint(*BALL_RADIUS_RANGE, size=(num_balls,))

        for x,y,r in zip(ball_center_x, ball_center_y, ball_radius):
            cv2.circle(output_image, (x,y), r, 255, -1)
            
    return output_image, FOCUS*camera_direction

def display_data(image, camera_vector):
    focus = np.linalg.norm(camera_vector)
    camera_direction = camera_vector / focus
    camera = camera_generator.build_camera(CAMERA_POSITION, camera_direction, focus, CAMERA_CENTER_IMAGE_COORDS)

    image = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)
    predicted_image = camera_generator.apply_camera(IMAGE, camera, OUTPUT_IMAGE_SHAPE)
    image[predicted_image > 0] = np.array([0,255,0], np.uint8)

    print 'camera vector   : ', camera_vector
    print 'camera direction: ', camera_direction
    print 'focus: ', focus
    
    cv2.imshow('predicted image in green', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    
    return

def train_model():
    #model_file = '/home/daniel/Dropbox/soccer/daniel_refactored/output/tester/simple_circle_model/model.chk'
    #model_file = '/home/daniel/Documents/tensorflow/models/conv_circle_model/model.chk'
    model_file = '/home/daniel/Documents/tensorflow/models/camera_vector_model/model.chk'
    #model_file = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/tester/camera_vector_model/model.chk'
    batch_size = 100
    evaluation_thresh = 1.0
    learning_rate = 0.01
    max_training_epochs = 10000
    num_epochs_per_summary = 100
    model_controller = TFModelController(learning_rate, max_training_epochs, num_epochs_per_summary)
    camera_vector_model = CameraVectorModel(batch_size, evaluation_thresh)

    images_and_vectors = (generate_random_data() for i in xrange(batch_size * max_training_epochs))
    images, vectors = iterator_operations.iunzip(images_and_vectors)
    
    #train the model
    model_controller.train(model_file, camera_vector_model, images, vectors)

    return


if __name__ == '__main__':
    np.set_printoptions(precision=1, linewidth=200, threshold=64*64)
    image, camera_vector = generate_random_data()
    display_data(image, camera_vector)
    # train_model()
