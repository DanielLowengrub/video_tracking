import numpy as np
import cv2
import os
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy import ndimage
from ...source.auxillary import camera_operations, iterator_operations
from ...source.auxillary.camera_matrix import CameraMatrix
from ...source.auxillary.soccer_field_geometry import SoccerFieldGeometry
from ...source.auxillary import mask_operations
from ...source.nn.simple_segmentation_model import SimpleSegmentationModel
from ...source.nn.tf_model_controller import TFModelController

SOCCER_FIELD = SoccerFieldGeometry(6, 743, 479, 20, 14)
SIDELINE_THICKNESS = 10
SIDELINE_KEYS = [SoccerFieldGeometry.CENTER_CIRCLE, SoccerFieldGeometry.CENTER_LINE,
                 (SoccerFieldGeometry.SIDELINE, SoccerFieldGeometry.BOTTOM)]

SIDELINE_COLORS = [(255,255,255), (255,0,0), (0,255,0)]

BACKGROUND =    0
UNKNOWN =       len(SIDELINE_KEYS) + 1

POSITION_ORIGIN = np.array([SOCCER_FIELD.field_width(), SOCCER_FIELD.field_length()], np.float32) / 2
POSITION_RADIUS = 460.0
POSITION_HEIGHT = 400

POSITION_ANGLE_RANGE = (20, 160)
THETA_X_RANGE = (90+45,90+50)
FOCUS_RANGE = (60,90)
ASPECT = 1.0

OUTPUT_IMAGE_SHAPE = (64,64)
CAMERA_CENTER = np.array(OUTPUT_IMAGE_SHAPE[::-1], np.float32) / 2.0

MAX_ELLIPSES = 10
ELLIPSE_RADIUS_RANGE = (2,7)

def build_sideline_masks(soccer_field, sideline_keys, thickness):
    sideline_masks = []

    for sideline_key in sideline_keys:
        sideline_mask = np.zeros((soccer_field.field_length()+1, soccer_field.field_width()+1), np.float32)
        hs = soccer_field.get_highlighted_shape(sideline_key)
        sideline_masks.append(hs.generate_mask(sideline_mask, thickness=thickness) > 0)

    return sideline_masks

def apply_camera_to_mask(camera, mask, output_image_shape):
    img = np.uint8(255 * np.int32(mask))
    output = camera_operations.apply_camera(img, camera, output_image_shape)
    return output > 0

def get_camera(position_origin, position_radius, position_angle, height, theta_x, focus, aspect, camera_center):
    theta_z = -(np.pi/2 - position_angle)

    camera_position = position_origin + position_radius*np.array([np.cos(position_angle), np.sin(position_angle)])
    camera_position = np.append(camera_position, height)
    camera_parameters = camera_operations.CameraParameters(theta_x, theta_z, focus, aspect, camera_center, camera_position)

    camera = camera_operations.build_camera(camera_parameters)
    return camera

def generate_random_camera():
    position_angle = np.random.uniform(*POSITION_ANGLE_RANGE) * np.pi/180
    theta_x        = np.random.uniform(*THETA_X_RANGE) * np.pi/180
    focus          = np.random.uniform(*FOCUS_RANGE)
    
    camera = get_camera(POSITION_ORIGIN, POSITION_RADIUS, position_angle, POSITION_HEIGHT,
                        theta_x, focus, ASPECT, CAMERA_CENTER)
    
    return camera

def generate_noise():
    noise_mask = np.zeros(OUTPUT_IMAGE_SHAPE, np.float32)

    #draw ellipses
    num_ellipses = np.random.randint(MAX_ELLIPSES+1)
    if num_ellipses > 0:
        center_x       = np.random.randint(0, noise_mask.shape[1]-1, size=(num_ellipses,))
        center_y       = np.random.randint(0, noise_mask.shape[0]-1, size=(num_ellipses,))
        axes_array     = np.random.randint(*ELLIPSE_RADIUS_RANGE, size=(num_ellipses,2))
        angle_array    = np.random.randint(0, 90, size=(num_ellipses,))
        for x,y,axes,angle in zip(center_x, center_y, axes_array, angle_array):
            cv2.ellipse(noise_mask, (x,y), tuple(axes.tolist()), angle, 0, 360, color=1.0, thickness=-1)

    #add random noise
    uniform_noise = np.random.choice(2, size=OUTPUT_IMAGE_SHAPE, p=[0.25, 0.75])
    return np.logical_and(noise_mask, uniform_noise)
    #return noise_mask

def generate_random_test_data():
    SIDELINE_VALUE = 255
    NOISE_VALUE    = 127
    sideline_masks = build_sideline_masks(SOCCER_FIELD, SIDELINE_KEYS, thickness=SIDELINE_THICKNESS)
    
    #generate the camera
    camera = generate_random_camera()

    #map the sideline masks to the image
    warped_sideline_masks = [apply_camera_to_mask(camera, mask, OUTPUT_IMAGE_SHAPE) for mask in sideline_masks]

    #the test image is initialized to "unknown"
    test_image =        np.zeros(warped_sideline_masks[0].shape, np.int32)
    test_segmentation = np.ones(warped_sideline_masks[0].shape, np.int32) * UNKNOWN

    for i,mask in enumerate(warped_sideline_masks):
        test_image[mask] = SIDELINE_VALUE
        test_segmentation[mask] = i+1

    sideline_mask = (test_image == SIDELINE_VALUE)

    #set some of the bg pixels to "background"
    num_sideline_pixels = sideline_mask.sum()
    #choose num_sideline_pixels from the background at random
    sideline_mask_flat = sideline_mask.reshape((-1,))
    background_indices = np.where(np.logical_not(sideline_mask_flat))[0]
    background_indices = np.random.choice(background_indices, num_sideline_pixels)
    test_segmentation_flat = test_segmentation.reshape((-1,))
    test_segmentation_flat[background_indices] = BACKGROUND
    
    #generate some noise
    noise_mask = generate_noise()
    noise_on_sideline = np.logical_and(noise_mask, sideline_mask)
    noise_off_sideline = np.logical_and(noise_mask, np.logical_not(sideline_mask))
    test_image[noise_mask] = NOISE_VALUE
    test_segmentation[noise_on_sideline]  = UNKNOWN
    test_segmentation[noise_off_sideline] = BACKGROUND

    #set the nbd of the sidelines to "unknown"
    unknown_mask = ndimage.binary_dilation(sideline_mask, iterations = 5)
    unknown_mask = np.logical_and(unknown_mask, np.logical_not(sideline_mask))
    test_segmentation[unknown_mask] = UNKNOWN

    
    return test_image, test_segmentation
    
def test_apply_camera_to_mask():
    sideline_masks = build_sideline_masks(SOCCER_FIELD, SIDELINE_KEYS, thickness=SIDELINE_THICKNESS)

    sideline_img = np.zeros(sideline_masks[0].shape + (3,), dtype=np.uint8)
    for i,mask in enumerate(sideline_masks):
        sideline_img[mask] = np.array(SIDELINE_COLORS[i % len(SIDELINE_COLORS)])

    print 'sideline image shape: ', sideline_img.shape
    cv2.imshow('absolute sidelines', sideline_img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    # position_angle = POSITION_ANGLE_RANGE[0] * np.pi/180
    # theta_x        = THETA_X_RANGE[0] * np.pi/180
    # focus          = FOCUS_RANGE[0]
    
    position_angle = np.random.uniform(*POSITION_ANGLE_RANGE) * np.pi/180
    theta_x        = np.random.uniform(*THETA_X_RANGE) * np.pi/180
    focus          = np.random.uniform(*FOCUS_RANGE)
    
    camera = get_camera(POSITION_ORIGIN, POSITION_RADIUS, position_angle, POSITION_HEIGHT,
                        theta_x, focus, ASPECT, CAMERA_CENTER)
    
    output_masks = [apply_camera_to_mask(camera, mask, OUTPUT_IMAGE_SHAPE) for mask in sideline_masks]
    output_img = np.zeros(OUTPUT_IMAGE_SHAPE + (3,), dtype=np.uint8)
    for i,mask in enumerate(output_masks):
        output_img[mask] = np.array(SIDELINE_COLORS[i % len(SIDELINE_COLORS)])

    cv2.imshow('image of sidelines', output_img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def test_generate_noise():
    noise_mask = generate_noise()

    cv2.imshow('noise', noise_mask.astype(np.float32))
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def display_segmentation(image, segmentation):     
    fig = plt.figure()
    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)

    cmap = mpl.colors.ListedColormap(['k', 'b', 'g', 'w', '0.5'])
    norm = mpl.colors.BoundaryNorm(range(len(SIDELINE_KEYS)+3), cmap.N)
    
    ax1.imshow(image, interpolation='nearest')
    ax1.set_ylim([image.shape[0], 0])
    ax2.imshow(segmentation, cmap=cmap, norm=norm, interpolation='nearest')
    ax2.set_ylim([segmentation.shape[0], 0])

    plt.show()

def display_segmentations(images, segmentations):
    num_per_row = 4
    max_rows = 4
    num_rows = min(4, len(images) / num_per_row)

    cmap = mpl.colors.ListedColormap(['k', 'b', 'g', 'w', '0.5'])
    norm = mpl.colors.BoundaryNorm(range(len(SIDELINE_KEYS)+3), cmap.N)

    fig, axes_array = plt.subplots(num_rows, 2*num_per_row, sharex='col', sharey='row')

    for row in range(num_rows):
        axes_in_row = axes_array[row]
        
        for col in range(4):
            img = images[num_per_row*row + col]
            seg = segmentations[num_per_row*row + col]
            
            ax_img = axes_in_row[2*col]
            ax_seg = axes_in_row[2*col + 1]

            ax_img.imshow(img, interpolation='nearest')
            ax_img.set_ylim([img.shape[0], 0])
            ax_img.set_axis_off()
            ax_seg.imshow(seg, cmap=cmap, norm=norm, interpolation='nearest')
            ax_seg.set_ylim([seg.shape[0], 0])
            ax_seg.set_axis_off()
            
    plt.axis('off')
    plt.savefig('/Users/daniel/Documents/predicted_sideline_type.png', bbox_inches='tight')
    plt.show()

def test_generate_random_test_data():
    print 'generating data...'
    image, segmentation = generate_random_test_data()
    print 'done.'
    display_segmentation(image, segmentation)
    return

def train_model():
    model_file = '/home/daniel/Documents/tensorflow/models/simple_segmentation_model/model.chk'
    #model_file = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/tester/nn_models/simple_segmentation_model/model.chk'
    log_dir    = '/home/daniel/Documents/tensorflow/logs/simple_segmentation_model'
    batch_size = 100
    learning_rate = 0.0005
    max_training_epochs = 10000
    num_epochs_per_summary = 100
    decay_rate = 0.7 #every 1000 steps, lr = lr*0.5
    decay_step = 1000
    model_controller = TFModelController(learning_rate, max_training_epochs, num_epochs_per_summary,
                                         decay_rate=decay_rate, decay_step=decay_step)
    segmentation_model = SimpleSegmentationModel(batch_size)

    images_and_segs = (generate_random_test_data() for i in xrange(batch_size * max_training_epochs))
    images, segmentations = iterator_operations.iunzip(images_and_segs)
    
    #train the model
    model_controller.train(model_file, log_dir, segmentation_model, images, segmentations)

    return

def predict():
    model_file = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/tester/nn_models/simple_segmentation_model/model.chk'
    batch_size = 100
    learning_rate = 0.0001
    max_training_epochs = 5000
    num_epochs_per_summary = 100
    model_controller = TFModelController(learning_rate, max_training_epochs, num_epochs_per_summary)
    segmentation_model = SimpleSegmentationModel(batch_size)

    print 'generating data...'
    images_and_segs = [generate_random_test_data() for i in xrange(batch_size)]
    images = [x[0] for x in images_and_segs]
    segmentations =  [x[1] for x in images_and_segs]

    print 'predicting...'
    predicted_segmentations = model_controller.predict(model_file, segmentation_model, iter(images))

    print 'drawing...'
    display_segmentations(images, predicted_segmentations)
    # for image, segmentation in zip(images[:20], predicted_segmentations[:20]):
    #     print segmentation
    #     display_segmentation(image, segmentation)
    
    return

if __name__ == '__main__':
    #test_apply_camera_to_mask()
    #test_generate_noise()
    #test_generate_random_test_data()
    #train_model()
    predict()
