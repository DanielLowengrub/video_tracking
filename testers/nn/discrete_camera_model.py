import numpy as np
import cv2
import matplotlib.pyplot as plt
from ..auxillary import camera_generator
from ...source.nn.discrete_camera_model import DiscreteCameraModel
from ...source.nn.tf_model_controller import TFModelController
from ...source.auxillary import iterator_operations
from ...source.auxillary import nn_operations

#IMAGE_FILENAME = 'test_data/test_pattern_tiny.png'
IMAGE_FILENAME = 'test_data/soccer_field_square.png'
IMAGE = cv2.imread(IMAGE_FILENAME, 1)
IMAGE = cv2.cvtColor(IMAGE, cv2.COLOR_BGR2GRAY)

CAMERA_POSITION  = np.array([64, 180, 70])

THETA_X = 30 * np.pi/180
THETA_Z_RANGE = (-20 * np.pi/180, 20 * np.pi/180)
FOCUS_RANGE = (250, 350)
OUTPUT_IMAGE_SHAPE = (128,128)

MAX_BALLS = 15
BALL_RADIUS_RANGE = (2, 10)

CAMERA_CENTER_IMAGE_COORDS = np.array(OUTPUT_IMAGE_SHAPE) / 2

NUM_Z_BINS = 16
ANGLE_Z_BINS = np.linspace(THETA_Z_RANGE[0], THETA_Z_RANGE[1], NUM_Z_BINS)
NUM_FOCUS_BINS = 8
FOCUS_BINS = np.linspace(FOCUS_RANGE[0], FOCUS_RANGE[1], NUM_FOCUS_BINS)

print 'angle z bins:'
print ANGLE_Z_BINS

print 'focus bins:'
print FOCUS_BINS

def display_transformed_image(camra_position, theta_x, theta_z, focus):
    camera_direction = camera_generator.build_camera_direction(theta_x, theta_z)
    camera = camera_generator.build_camera(camera_position, camera_direction, focus, CAMERA_CENTER_IMAGE_COORDS)
    output_image = camera_generator.apply_camera(IMAGE, camera, OUTPUT_IMAGE_SHAPE)

    # output_image[:,64] = 255
    # output_image[:,256-64] = 255
    # output_image[64,:] = 255
    # output_image[256-64,:] = 255
    
    cv2.imshow('transformed image', output_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    return

def generate_random_data():
    '''
    return - image, camera_vector
    image - a gray image
    camera_vector - a length 3 vector
    '''
    theta_z = np.random.uniform(*THETA_Z_RANGE)
    focus   = np.random.uniform(*FOCUS_RANGE)
    camera_direction = camera_generator.build_camera_direction(THETA_X, theta_z)
    camera = camera_generator.build_camera(CAMERA_POSITION, camera_direction, np.array([focus,focus]),
                                           CAMERA_CENTER_IMAGE_COORDS)
    output_image = camera_generator.apply_camera(IMAGE, camera, OUTPUT_IMAGE_SHAPE)

    num_balls = np.random.randint(MAX_BALLS+1)
    if num_balls > 0:
        ball_center_x = np.random.randint(0, output_image.shape[1]-1, size=(num_balls,))
        ball_center_y = np.random.randint(0, output_image.shape[0]-1, size=(num_balls,))
        ball_radius   = np.random.randint(*BALL_RADIUS_RANGE, size=(num_balls,))
        color = np.random.randint(0,2,size=(num_balls,))*255
        for x,y,r,c in zip(ball_center_x, ball_center_y, ball_radius, color):
            cv2.circle(output_image, (x,y), r, c, -1)

    #print 'theta_z: %f, focus: %f' % (theta_z, focus)
    prob_z =     nn_operations.weighted_one_hot(theta_z, ANGLE_Z_BINS)
    prob_focus = nn_operations.weighted_one_hot(focus,   FOCUS_BINS)
    prob = np.hstack([prob_z, prob_focus])
    prob = prob / prob.sum()

    return output_image, prob

def display_all_bin_combinations(step=1):
    vertical_boundary   = 255*np.ones((OUTPUT_IMAGE_SHAPE[0],2), np.uint8)
    horizontal_boundary = 255*np.ones((2,(NUM_Z_BINS/step)*(OUTPUT_IMAGE_SHAPE[1]+2)), np.uint8)

    output_rows = []
    
    for focus in FOCUS_BINS[::step]:
        output_row = []
        for theta_z in ANGLE_Z_BINS[::step]:
            camera_direction = camera_generator.build_camera_direction(THETA_X, theta_z)
            camera = camera_generator.build_camera(CAMERA_POSITION, camera_direction, focus, CAMERA_CENTER_IMAGE_COORDS)
            image = camera_generator.apply_camera(IMAGE, camera, OUTPUT_IMAGE_SHAPE)
            output_row += [image, vertical_boundary]

        output_rows += [np.hstack(output_row), horizontal_boundary]

    output_image = np.vstack(output_rows)

    cv2.imshow('all bin combinations', output_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def display_data(image, prob):
    cv2.imshow('input image', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    prob_z =     prob[:NUM_Z_BINS]
    prob_focus = prob[NUM_Z_BINS:]

    prob_z = prob_z / prob_z.sum()
    prob_focus = prob_focus / prob_focus.sum()
    
    theta_z = nn_operations.inv_weighted_one_hot(prob_z, ANGLE_Z_BINS)
    focus   = nn_operations.inv_weighted_one_hot(prob_focus, FOCUS_BINS)
    camera_direction = camera_generator.build_camera_direction(THETA_X, theta_z)
    camera = camera_generator.build_camera(CAMERA_POSITION, camera_direction, focus, CAMERA_CENTER_IMAGE_COORDS)

    image = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)
    predicted_image = camera_generator.apply_camera(IMAGE, camera, OUTPUT_IMAGE_SHAPE)
    image[predicted_image > 0] = np.array([0,255,0], np.uint8)

    print 'prob      : ', prob
    print 'prob_z    : ', prob_z
    print 'prob_focus: ', prob_focus
    print 'theta_z   : ', theta_z
    print 'focus     : ', focus

    plt.bar(range(len(prob)), prob)
    plt.xlim([0, len(prob)])
    plt.show()
    
    cv2.imshow('predicted image in green', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    
    return

def display_data_batch(images, probs, output_filename=None):
    images_per_row = 10
    vertical_boundary   = 255*np.ones((OUTPUT_IMAGE_SHAPE[0],2,3), np.uint8)
    horizontal_boundary = 255*np.ones((2,images_per_row*(OUTPUT_IMAGE_SHAPE[1]+2),3), np.uint8)

    output_rows = []
    num_rows = len(images) / images_per_row
    
    for row in range(num_rows):
        output_row = []
        for col in range(images_per_row):
            image =  images[row*images_per_row + col]
            if row == 0:
                prob = probs[0]
            else:
                prob  =   probs[row*images_per_row + col]

            prob_z =     prob[:NUM_Z_BINS]
            prob_focus = prob[NUM_Z_BINS:]
            
            prob_z = prob_z / prob_z.sum()
            prob_focus = prob_focus / prob_focus.sum()
                
            theta_z = nn_operations.inv_weighted_one_hot(prob_z, ANGLE_Z_BINS)
            focus   = nn_operations.inv_weighted_one_hot(prob_focus, FOCUS_BINS)
                
            camera_direction = camera_generator.build_camera_direction(THETA_X, theta_z)
            camera = camera_generator.build_camera(CAMERA_POSITION, camera_direction, np.array([focus,focus]),
                                                   CAMERA_CENTER_IMAGE_COORDS)
            image = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)
            predicted_image = camera_generator.apply_camera(IMAGE, camera, OUTPUT_IMAGE_SHAPE)
            image[predicted_image > 0] = np.array([0,255,0], np.uint8)

            output_row += [image, vertical_boundary]

        output_rows += [np.hstack(output_row), horizontal_boundary]

    output_image = np.vstack(output_rows)

    cv2.imshow('predicted homography in green', output_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    if output_filename is not None:
        cv2.imwrite(output_filename, output_image)
        
    return

def train_model():
    model_file = '/home/daniel/Documents/tensorflow/models/discrete_camera_model/model.chk'
    batch_size = 100
    learning_rate = 0.001
    max_training_epochs = 5000
    num_epochs_per_summary = 100
    model_controller = TFModelController(learning_rate, max_training_epochs, num_epochs_per_summary)
    camera_model = DiscreteCameraModel(batch_size)

    images_and_probs = (generate_random_data() for i in xrange(batch_size * max_training_epochs))
    images, probs = iterator_operations.iunzip(images_and_probs)
    
    #train the model
    model_controller.train(model_file, camera_model, images, probs)

    return

def predict():
    #model_file = '/home/daniel/Documents/tensorflow/models/discrete_camera_model/model.chk'
    model_file = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/tester/discrete_camera_model/model.chk'
    batch_size = 100
    learning_rate = 0.01
    max_training_epochs = 1000
    num_epochs_per_summary = 100
    model_controller = TFModelController(learning_rate, max_training_epochs, num_epochs_per_summary)
    camera_model = DiscreteCameraModel(batch_size)

    print 'generating data...'
    images_and_probs = [generate_random_data() for i in xrange(batch_size)]
    images = [x[0] for x in images_and_probs]
    probs =  [x[1] for x in images_and_probs]

    print 'predicting...'
    predicted_probs = model_controller.predict(model_file, camera_model, iter(images))

    for prob, predicted_prob in zip(probs, predicted_probs):
        print '\n\n'
        print 'params: (%d, %d), predicted_params: (%d, %d)' % (np.argmax(prob[:NUM_Z_BINS]), np.argmax(prob[NUM_Z_BINS:]),
                                                                np.argmax(predicted_prob[:NUM_Z_BINS]),
                                                                np.argmax(predicted_prob[NUM_Z_BINS:]))
        print 'prob:           [%s]' % ', '.join(['%2.2f' % x for x in prob])
        print 'predicted_prob: [%s]' % ', '.join(['%2.2f' % x for x in predicted_prob])

    display_data_batch(images, probs, output_filename='/Users/daniel/Documents/predicted_sidelines.png')
    
    return


if __name__ == '__main__':
    np.set_printoptions(precision=1, linewidth=200, threshold=64*64)
    # image, prob = generate_random_data()
    # display_data(image, prob)
    # display_transformed_image(camera_position, theta_x, theta_z, focus)
    # display_all_bin_combinations(step=2)
    # train_model()
    predict()
