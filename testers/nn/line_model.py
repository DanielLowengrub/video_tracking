import numpy as np
from ...source.nn.line_model import LineModel
from ...source.nn.tf_model_controller import TFModelController

def train_model():
    model_file = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/tester/line_model/model.chk'
    batch_size = 10
    evaluation_thresh = 1.0
    learning_rate = 0.01
    max_training_epochs = 10000
    num_epochs_per_summary = 1000
    model_controller = TFModelController(learning_rate, max_training_epochs, num_epochs_per_summary)
    line_model = LineModel(batch_size, evaluation_thresh)

    print 'line model loss: ', line_model._loss_op
    #generate some data
    a = 2
    b = 1
    
    xs = np.random.rand(max_training_epochs * batch_size)
    ys = a*xs + b + 0.01*np.random.normal(size=xs.shape)


    #train the model
    model_controller.train(model_file, line_model, iter(xs), iter(ys))

    return

def predict():
    model_file = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/tester/line_model/model.chk'
    batch_size = 10
    evaluation_thresh = 1.0
    learning_rate = 0.01
    max_training_epochs = 1000
    num_epochs_per_summary = 50
    model_controller = TFModelController(learning_rate, max_training_epochs, num_epochs_per_summary)
    line_model = LineModel(batch_size, evaluation_thresh)

    #generate some input data
    xs = np.linspace(-10,10,10)

    #predict the output
    ys = model_controller.predict(model_file, line_model, iter(xs))

    print 'input:'
    print xs

    print 'output:'
    print ys

    print 'a*xs + b:'
    print 2*xs + 1
    
if __name__ == '__main__':
    #train_model()
    predict()
    
