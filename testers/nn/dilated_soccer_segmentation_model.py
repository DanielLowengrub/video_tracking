import numpy as np
import cv2
import os
import re
import random
import time
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from ...source.auxillary.fit_points_to_shape.line import Line
from ...source.auxillary import iterator_operations
from ..auxillary import primitive_drawing
from ...source.nn.tf_model_controller import TFModelController
from ...source.nn.dilated_soccer_segmentation_model import DilatedSoccerSegmentationModel
from ...source.nn.filename_data_set import FilenameDataSet
from ...source.preprocessing_full.annotation_training_data_controller.annotation_training_data_controller import AnnotationTrainingValues
from ...source.preprocessing_full import filename_loader

"""
THINGS TO TRY

* we currently subtract 127 from the pixel values. Is this the true mean? Maybe we should subtract the true
  mean... ditto for std
* on this note, add histograms showing the values of the input RGB channels
* or (and), compute the mean of the entire training dataset
* visualize a histogram of each activation layer, as well as the gradients. see:
http://cs231n.github.io/neural-networks-3/#loss
to do the gradients, call compute_gradients and then apply_gradients. then, add a summary op to write the gradients
* also visualize ratio weights:updates
* also visualize each channel in the intermediate layers. http://cs231n.github.io/understanding-cnn/
* bigger or smaller learning rate
* draw the ground truth as well to make sure everything is in order
* more and bigger filters (3x3x32)
* bigger batches
* use tf queue to load images
* IMPORTANT: do this for every technique we check! try to overfit on 20 images. Also, remove the validation
  set when overfitting...
* Also, do not flip images when overfitting!
* try to overfit, and then train on each of the classes individually
* instead of randomly selecting a subset of pixels, give each class a weight.
  in order to prevent weights from being too small, maybe first select at most e.g 10000 pixels from each class,
  then compute the weights.
  the weights can be assigned by adding a channel to the ground truth array.
* if we can not overfit on all classes (or all pairs of classes), then add more (dilated) layers.
* it may be a good idea to give the "harder" classes (players and sidelines) a higher weight for the first few epochs, to prevent the NN from prematurely optimizing the filters for the easier classes
* print more informative starup info
* do not worry about adding more layers. overfitting can be prevented with dropout and regularization
* initialization: (sqrt(2/n)*N(0,1))? batch normalization? https://arxiv.org/abs/1502.03167
* when classifying with softmax, note that the initial loss should be ln(number of classes)
"""
#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'

OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'
TF_PARENT_DIR = '/home/daniel/Documents/tensorflow'

# OUTPUT_PARENT_DIRECTORY = '/home/ubuntu/soccer/'
# TF_PARENT_DIR = '/home/ubuntu/nn_output'

NN_PARENT_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'nn_training_data', 'image_annotation', 'dataset_0')
NN_IMAGE_DIRECTORY  = os.path.join(NN_PARENT_DIRECTORY, 'image')
NN_GT_DIRECTORY = os.path.join(NN_PARENT_DIRECTORY, 'true')

#these are loaded for prediction
# IMAGE_INTERVAL = (6800, 6810)
# IMAGE_DIRECTORY = os.path.join('/home/daniel/soccer', 'images', 'images-0_00-10_00')

IMAGE_INTERVAL = (90, 110)
IMAGE_DIRECTORY = os.path.join('/home/daniel/soccer', 'images', 'video-FrxUfp0B3pA', 'images-9_17-9_22')

IMG_REGEX = re.compile('(.*)\.png')
IMG_BASENAME_REGEX = re.compile('image_([0-9]+)\.png')

LABEL_COLOR_DICT = {AnnotationTrainingValues.BACKGROUND: (0,0,0),
                    AnnotationTrainingValues.GRASS:      (0,255,0),
                    AnnotationTrainingValues.SIDELINE:   (255,255,255),
                    AnnotationTrainingValues.PLAYER:     (255,0,0),
                    AnnotationTrainingValues.UNKNOWN:    (127, 127, 127)}

VALIDATION_FRACTION = 0.1
BATCH_SIZE = 2
MIN_PIXELS_PER_LABEL = 1000

class SoccerSegmentationDataSet(FilenameDataSet):
    def __init__(self, batch_size, training_pair_filenames, min_pixels_per_label):
        super(SoccerSegmentationDataSet, self).__init__(batch_size, training_pair_filenames)
        self._min_pixels_per_label = min_pixels_per_label

    def _load_training_pair(self, input_filename, gt_filename):        
        image = cv2.imread(input_filename, 1)
        segmentation = np.load(gt_filename)
        
        formatted_seg = np.ones(segmentation.shape, np.int32) * AnnotationTrainingValues.UNKNOWN
        segmentation_flat = segmentation.reshape((-1,))
        formatted_seg_flat = formatted_seg.reshape((-1,))

        num_pixels_per_label = min(np.sum(segmentation == label) for label in AnnotationTrainingValues.labels)
        num_pixels_per_label = max(num_pixels_per_label, self._min_pixels_per_label)
        
        for label in AnnotationTrainingValues.labels:
            #choose a subset of the pixels with this label
            label_indices = np.where(segmentation_flat == label)[0]
            if len(label_indices) > num_pixels_per_label:
                label_indices = np.random.choice(label_indices, num_pixels_per_label)
            
            formatted_seg_flat[label_indices] = label

        #now decide whether to flip the image
        if np.random.randint(2) == 1:
            image          = image[:,::-1]
            formatted_seg  = formatted_seg[:,::-1]

        return image, formatted_seg

def load_training_pair_filenames(image_dir, segmentation_dir):
    training_pair_filenames = []
    for basename in os.listdir(image_dir):
        m = IMG_REGEX.match(basename)
        if m is None:
            continue
        
        basename_prefix = m.groups()[0]
        image_filename = os.path.join(image_dir, basename_prefix + '.png')
        segmentation_filename = os.path.join(segmentation_dir, basename_prefix + '.npy')

        training_pair_filenames.append((image_filename, segmentation_filename))

    return training_pair_filenames
            

def compute_image_channel_mean(training_pair_filenames):
    '''
    training_pair_filenames - a list of pairs of filenames.
    return - a numpy array with length 3. It represents the mean value for each channel.
    '''
    num_images = len(training_pair_filenames)
    channel_mean = np.zeros(3)
    
    for tp_filename in training_pair_filenames:
        img = cv2.imread(tp_filename[0], 1)
        channel_mean += np.mean(img, axis=(0,1)) / num_images

    return channel_mean
        
def build_data_sets(batch_size, image_dir, segmentation_dir):
    '''
    batch_size - an integer
    image_dir - a directory name
    segmentation_dir - a directory name

    return a pair of iterators: training_pair_iter, validation_pair_iter
    both are iterators of tuples (image, segmentation).
    '''
    print 'loading training pair filenames.'
    print 'image_dir: ', image_dir
    print 'seg_dir  : ', segmentation_dir

    training_pair_filenames = load_training_pair_filenames(image_dir, segmentation_dir)
    num_validation_pairs = int(VALIDATION_FRACTION * len(training_pair_filenames))
    #do this when overfitting
    #num_validation_pairs = BATCH_SIZE
    
    random.shuffle(training_pair_filenames)
    validation_pair_filenames = training_pair_filenames[:num_validation_pairs]
    training_pair_filenames   = training_pair_filenames[num_validation_pairs:]

    #do this when overfitting
    #training_pair_filenames = training_pair_filenames[:20]

    training_mean = compute_image_channel_mean(training_pair_filenames)
    
    print 'loaded %d validation pairs and %d training pairs.' % (len(validation_pair_filenames),
                                                                 len(training_pair_filenames))
    print 'the training data has mean: %s' % training_mean

    training_data_set   = SoccerSegmentationDataSet(batch_size, training_pair_filenames, MIN_PIXELS_PER_LABEL)
    validation_data_set = SoccerSegmentationDataSet(batch_size, validation_pair_filenames, MIN_PIXELS_PER_LABEL)

    #do this when overfitting
    # validation_data_set = None
    
    return training_mean, training_data_set, validation_data_set

def display_training_pair(image, segmentation):     
    fig = plt.figure()
    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)

    cmap = mpl.colors.ListedColormap(['k', 'g', 'w', 'b', '0.5'])
    norm = mpl.colors.BoundaryNorm(range(6), cmap.N)
    
    ax1.imshow(image, interpolation='nearest')
    ax1.set_ylim([image.shape[0], 0])
    ax2.imshow(segmentation, cmap=cmap, norm=norm, interpolation='nearest')
    ax2.set_ylim([segmentation.shape[0], 0])

    plt.show()

def display_training_pairs(images, segmentations):
    num_per_row = 1
    max_rows = 2
    num_rows = min(max_rows, len(images) / num_per_row)

    cmap = mpl.colors.ListedColormap(['k', 'g', 'w', 'b', '0.3'])
    norm = mpl.colors.BoundaryNorm(range(6), cmap.N)

    fig, axes_array = plt.subplots(num_rows, 2*num_per_row, sharex='col', sharey='row')

    for row in range(num_rows):
        axes_in_row = axes_array[row]
        
        for col in range(num_per_row):
            img = images[num_per_row*row + col]
            seg = segmentations[num_per_row*row + col]
            
            ax_img = axes_in_row[2*col]
            ax_seg = axes_in_row[2*col + 1]

            ax_img.imshow(img, interpolation='nearest')
            ax_img.set_ylim([img.shape[0], 0])
            ax_img.set_axis_off()
            ax_seg.imshow(seg, cmap=cmap, norm=norm, interpolation='nearest')
            ax_seg.set_ylim([seg.shape[0], 0])
            ax_seg.set_axis_off()
            
    plt.axis('off')
    plt.savefig('/home/daniel/Documents/Pictures/dilated_soccer_segmentation_run_12_predictions.png', bbox_inches='tight')
    #plt.show()

def test_build_data_sets():
    training_mean, training_data_set, validation_data_set = build_data_sets(BATCH_SIZE,
                                                                            NN_IMAGE_DIRECTORY, NN_GT_DIRECTORY)

    print 'training mean: ', training_mean
    print 'training batch:'
    images, segmentations = training_data_set.next_batch()
    display_training_pairs(images, segmentations)

    print 'validation batch:'
    images, segmentations = validation_data_set.next_batch()
    display_training_pairs(images, segmentations)

    return

def train_model():
    RUN = 13 #run 12 is the best one so far
    # model_file = os.path.join('/home/daniel/Documents/tensorflow/models/dilated_soccer_segmentation_model',
    #                          'run_%d' % RUN, 'model.chk')
    # log_dir    = os.path.join('/home/daniel/Documents/tensorflow/logs/dilated_soccer_segmentation_model',
    #                           'run_%d' % RUN)

    # init_file = os.path.join('/home/daniel/Documents/tensorflow/models/dilated_soccer_segmentation_model',
    #                           'run_%d' % RUN, 'model.chk-100')

    # model_file = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/tester/nn_models/dilated_soccer_segmentation_model/model.chk'
    # log_dir    = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/tester/tf_logs/dilated_soccer_segmentation_model'


    model_file = os.path.join(TF_PARENT_DIR, 'models', 'dilated_soccer_segmentation_model',
                             'run_%d' % RUN, 'model.chk')

    log_dir    = os.path.join(TF_PARENT_DIR, 'logs', 'dilated_soccer_segmentation_model',
                              'run_%d' % RUN)
    

    learning_rate = 0.0001 #0.0001
    max_training_epochs = 50
    num_batches_per_summary = 5
    num_batches_per_checkpoint = 1000
    decay_rate = 0.7 #every 1000 steps, lr = lr*0.5
    decay_step = 1000
    model_controller = TFModelController(learning_rate, max_training_epochs, num_batches_per_summary,
                                         num_batches_per_checkpoint,
                                         decay_rate=decay_rate, decay_step=decay_step)

    training_mean, training_data_set, validation_data_set = build_data_sets(BATCH_SIZE,
                                                                            NN_IMAGE_DIRECTORY, NN_GT_DIRECTORY)

    segmentation_model = DilatedSoccerSegmentationModel(BATCH_SIZE, training_mean)

    #train the model
    model_controller.train(model_file, log_dir, segmentation_model,
                           training_data_set, validation_data_set,
                           initialization_file=None)

    return

def load_prediction_images(image_directory, image_interval):
    filenames = filename_loader.load_filenames(image_directory, IMG_BASENAME_REGEX)
    filenames = filenames[image_interval[0] : image_interval[1]]
    print 'loading %d images...' % len(filenames)
    return np.stack([cv2.imread(f[1],1) for f in filenames])

def predict():
    RUN = 12
    model_file = os.path.join('/home/daniel/Documents/tensorflow/models/dilated_soccer_segmentation_model',
                             'run_%d' % RUN, 'model.chk-1000')
    #model_file = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/tester/nn_models/dilated_soccer_segmentation_model/model.chk'
    learning_rate = 0.0005
    max_training_epochs = 60
    num_batches_per_summary = 5
    num_batches_per_checkpoint = 50
    decay_rate = 0.7 #every 1000 steps, lr = lr*0.5
    decay_step = 1000
    channel_mean = np.array([63.08715057, 106.04039001, 83.88618469])
    
    model_controller = TFModelController(learning_rate, max_training_epochs, num_batches_per_summary,
                                         num_batches_per_checkpoint,
                                         decay_rate=decay_rate, decay_step=decay_step)
    segmentation_model = DilatedSoccerSegmentationModel(BATCH_SIZE, channel_mean)

    images = load_prediction_images(IMAGE_DIRECTORY, IMAGE_INTERVAL)
    
    print 'predicting...'
    start = time.time()
    predicted_segmentations = model_controller.predict(model_file, segmentation_model, iter(images))
    print 'finished in %.2f seconds' % (time.time() - start)
    
    print 'drawing predictions...'
    display_training_pairs(images, predicted_segmentations)
    # for image, segmentation in zip(images[:20], predicted_segmentations[:20]):
    #     print segmentation
    #     display_segmentation(image, segmentation)
    
    return

if __name__ == '__main__':
    #test_build_data_sets()
    train_model()
    #predict()
