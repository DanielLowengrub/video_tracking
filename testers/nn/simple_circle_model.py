import numpy as np
import cv2
import matplotlib.pyplot as plt
from ...source.nn.simple_circle_model import SimpleCircleModel
from ...source.nn.conv_circle_model import ConvCircleModel
from ...source.nn.tf_model_controller import TFModelController
from ...source.auxillary import iterator_operations

IMAGE_SIZE = 64
MIN_RADIUS = 10
PADDING    = 5
MAX_NOISE_POINTS = 20

def generate_random_circle():
    image = np.zeros((IMAGE_SIZE, IMAGE_SIZE), np.float32)

    center = np.random.randint(PADDING + MIN_RADIUS + 1, IMAGE_SIZE - 1 - MIN_RADIUS - PADDING, size=(2,))
    max_radius = min([center[0], IMAGE_SIZE - center[0], center[1], IMAGE_SIZE-center[1]]) - PADDING
    radius = np.random.randint(MIN_RADIUS, max_radius)

    cv2.circle(image, tuple(center.tolist()), int(radius), 1.0, 1)

    num_noise_points = np.random.randint(1, MAX_NOISE_POINTS)
    noise_points = np.random.randint(0, IMAGE_SIZE-1, size=(num_noise_points, 2))
    image[noise_points[:,1], noise_points[:,0]] = 1.0
    
    return image, center.astype(np.float32)

def display_data(images, centers, output_filename=None):
    images_per_row = 20
    vertical_boundary   = 255*np.ones((IMAGE_SIZE,2,3), np.uint8)
    horizontal_boundary = 255*np.ones((2,images_per_row*(IMAGE_SIZE+2),3), np.uint8)

    output_rows = []
    num_rows = len(images) / images_per_row
    
    for row in range(num_rows):
        output_row = []
        for col in range(images_per_row):
            image =  images[row*images_per_row + col]
            center = centers[row*images_per_row + col]
            image = cv2.cvtColor(255*image, cv2.COLOR_GRAY2BGR)
            cv2.circle(image, tuple(center.astype(np.int32).tolist()), 1, (0,255,0), -1)
            output_row += [image, vertical_boundary]

        output_rows += [np.hstack(output_row), horizontal_boundary]

    output_image = np.vstack(output_rows)
    cv2.imshow('circles and centers', output_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    if output_filename is not None:
        cv2.imwrite(output_filename, output_image)
        
    return

def train_model():
    #model_file = '/home/daniel/Dropbox/soccer/daniel_refactored/output/tester/simple_circle_model/model.chk'
    model_file = '/home/daniel/Documents/tensorflow/models/conv_circle_model/model.chk'
    batch_size = 100
    evaluation_thresh = 1.0
    learning_rate = 0.01
    max_training_epochs = 10000
    num_epochs_per_summary = 100
    model_controller = TFModelController(learning_rate, max_training_epochs, num_epochs_per_summary)
    #circle_model = SimpleCircleModel(batch_size, evaluation_thresh)
    circle_model = ConvCircleModel(batch_size, evaluation_thresh)

    images_and_centers = (generate_random_circle() for i in xrange(batch_size * max_training_epochs))
    images, centers = iterator_operations.iunzip(images_and_centers)
    
    #train the model
    model_controller.train(model_file, circle_model, images, centers)

    return

def predict():
    #model_file = '/home/daniel/Dropbox/soccer/daniel_refactored/output/tester/simple_circle_model/model.chk'
    #model_file = '/home/daniel/Documents/tensorflow/models/conv_circle_model/model.chk'
    model_file = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/tester/conv_circle_model/model.chk'
    batch_size = 100
    evaluation_thresh = 1.0
    learning_rate = 0.05
    max_training_epochs = 10000
    num_epochs_per_summary = 100
    model_controller = TFModelController(learning_rate, max_training_epochs, num_epochs_per_summary)
    #circle_model = SimpleCircleModel(batch_size, evaluation_thresh)
    circle_model = ConvCircleModel(batch_size, evaluation_thresh)
    
    images_and_centers = [generate_random_circle() for i in range(100)]
    images = [x[0] for x in images_and_centers]
    centers = [x[1] for x in images_and_centers]
    
    predicted_centers = model_controller.predict(model_file, circle_model, iter(images))

    print 'true_centers vs predicted_centers:'
    print np.hstack([np.vstack(centers), predicted_centers])[:20]

    display_data(images, predicted_centers, output_filename='/Users/daniel/Documents/conv_circles.png')
    
if __name__ == '__main__':
    # images_centers = [generate_random_circle() for i in range(20)]
    # images  = [x[0] for x in images_centers]
    # centers = [x[1] for x in images_centers]
    # display_data(images, centers)

    # train_model()
    predict()
    
