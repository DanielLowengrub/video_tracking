import numpy as np
import cv2
import os
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy import ndimage
from ...source.auxillary import camera_operations, iterator_operations
from ...source.auxillary.camera_matrix import CameraMatrix
from ...source.auxillary.soccer_field_geometry import SoccerFieldGeometry
from ...source.auxillary import mask_operations
from ...source.nn.simple_segmentation_model import SimpleSegmentationModel
from ...source.nn.tf_model_controller import TFModelController

SOCCER_FIELD_WIDTH  = 743
SOCCER_FIELD_HEIGHT = 479
GRID_WIDTH          = 20
GRID_LENGTH         = 14
SCALE = 6
SOCCER_FIELD = SoccerFieldGeometry(SCALE, SOCCER_FIELD_WIDTH, SOCCER_FIELD_HEIGHT, GRID_WIDTH, GRID_HEIGHT)
SIDELINE_THICKNESS = 10

CAMERA_POSITION = np.array([850, 240, 400], np.float32) / 2.0

THETA_X_RANGE = (90+10,90+30)
FOCUS_RANGE = (100,150)
ASPECT = 1.0

IMAGE_SHAPE = (64,100)
RESCALED_IMAGE_SHAPE = (64, 64)
CAMERA_CENTER = np.array(IMAGE_SHAPE[::-1], np.float32) / 2.0


