import numpy as np
import cv2
import matplotlib as mpl
import matplotlib.pyplot as plt
from ...source.auxillary.fit_points_to_shape.line import Line
from ..auxillary import primitive_drawing

IMAGE_SIZE = 64
MAX_LINES = 10
MAX_ELLIPSES = 10

LINE_THICKNESS = 2
LINE_RADIUS_RANGE = (0,IMAGE_SIZE/2 - 10)
ELLIPSE_RADIUS_RANGE = (4,10)

LINE_COLOR = 255
ELLIPSE_COLOR = 127

BACKGROUND = 0
LINE = 1
ELLIPSE = 2
UNKNOWN = 3

def generate_training_data():
    #initialize the image to the background color, and the annotation to "unknown"
    image = np.zeros((IMAGE_SIZE, IMAGE_SIZE), np.uint8)
    annotation = np.ones((IMAGE_SIZE, IMAGE_SIZE), np.int32) * UNKNOWN
    
    #first draw some random lines on the image and on the annotation
    num_lines = np.random.randint(MAX_LINES + 1)
    radius = np.random.uniform(*LINE_RADIUS_RANGE, size=(num_lines,))
    angle  = np.random.uniform(-np.pi, np.pi, size=(num_lines,))
    parametric_reps = np.vstack([np.cos(angle), np.sin(angle), -radius]).transpose()
    for parametric_rep in parametric_reps:
        line = Line.from_parametric_representation(parametric_rep)
        primitive_drawing.draw_line(image, line, LINE_COLOR, LINE_THICKNESS)
        primitive_drawing.draw_line(annotation, line, LINE, LINE_THICKNESS)

    #now draw some random ellipses
    num_ellipses   = np.random.randint(MAX_ELLIPSES+1)
    center_x       = np.random.randint(0, image.shape[1]-1, size=(num_ellipses,))
    center_y       = np.random.randint(0, image.shape[0]-1, size=(num_ellipses,))
    axes_array     = np.random.randint(*ELLIPSE_RADIUS_RANGE, size=(num_ellipses,2))
    angle_array    = np.random.randint(0, 90, size=(num_ellipses,))
    for x,y,axes,angle in zip(center_x, center_y, axes_array, angle_array):
        cv2.ellipse(image, (x,y), tuple(axes.tolist()), angle, 0, 360, ELLIPSE_COLOR, thickness=-1)
        cv2.ellipse(annotation, (x,y), tuple(axes.tolist()), angle, 0, 360, ELLIPSE, thickness=-1)

    #now choose some random pixels that will be marked as background
    foreground_mask = np.logical_or(annotation == LINE, annotation == ELLIPSE)
    background_mask = np.logical_not(foreground_mask)

    num_fg = np.sum(foreground_mask)
    print 'setting %d pixels to background' % num_fg
    background_indices = np.where(background_mask.reshape((-1,)))[0]
    background_indices = np.random.choice(background_indices, num_fg)

    annotation_flat = annotation.reshape((-1,))
    annotation_flat[background_indices] = BACKGROUND

    return image, annotation

def display_test_data(image, annotation):     
    fig = plt.figure()
    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)

    cmap = mpl.colors.ListedColormap(['k', 'w', 'b', '0.5'])
    norm = mpl.colors.BoundaryNorm(range(5), cmap.N)
    
    ax1.imshow(image, interpolation='nearest')
    ax1.set_ylim([image.shape[0], 0])
    ax2.imshow(annotation, cmap=cmap, norm=norm, interpolation='nearest')
    ax2.set_ylim([annotation.shape[0], 0])

    plt.show()

def test_training_data_generator():
    image, annotation = generate_training_data()
    display_test_data(image, annotation)
    return

if __name__ == '__main__':
    test_training_data_generator()
    
    
