import numpy as np
import cv2
import matplotlib.pyplot as plt

from ....source.auxillary.soccer_field_geometry import SoccerFieldGeometry
from ....source.auxillary.camera_matrix import CameraMatrix
from ....source.auxillary import planar_geometry, world_to_image_projections
from ....source.auxillary.rectangle import Rectangle
from ....source.auxillary.fit_points_to_shape.ellipse import Ellipse
from ...auxillary import primitive_drawing

from .ball_generator import load_absolute_soccer_field, draw_soccer_balls

"""
We use this to generate trajectories of balls to check the dynamical model.
"""

SCALE = 6 #conversion from meters to world coordinates
FPS = 30.0
DELTA_T = 1/FPS
DRAG_COEFF = 0.015 * (1.0 / SCALE) #alpha is usually 0.015m^(-1), so we multiply by 1/scale
GRAVITY_COEFF = 9.83 * SCALE       #g as units m/s^2 so we multiple by the scale

IMAGE_BASENAME = 'test_data/images_0_269/image_%d.png'
CAMERA_BASENAME = 'output/scripts/preprocessing/data/images_0_269/camera/camera_0_269/camera_%d.npy'

TARGET_POSITION = np.array([495, 133, 0], np.float32)

def load_absolute_soccer_field():
    '''
    Return a SoccerFieldGeometry object
    '''
    ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)
    SCALE = 6
    FIELD_WIDTH = absolute_image.shape[1]-1
    FIELD_LENGTH = absolute_image.shape[0]-1
    GRID_WIDTH = 20
    GRID_LENGTH = 14
    soccer_field = SoccerFieldGeometry(SCALE, FIELD_WIDTH, FIELD_LENGTH, GRID_WIDTH, GRID_LENGTH)

    return absolute_image, soccer_field

def load_images_and_cameras(frame_indices):
    '''
    return a tuple (image, camera from absolute coordinates)
    '''

    images = [cv2.imread(IMAGE_BASENAME % i, 1) for i in frame_indices]
    camera_matricies = [CameraMatrix(np.load(CAMERA_BASENAME % i)) for i in frame_indices]

    return images, camera_matricies

def generate_trajectory(initial_position, initial_velocity, number_of_frames):
    '''
    initial_position - length 3 np array representing the position in world coords
    initial_velocity - length 3 np array representing the velocity in world coords / second
    number_of_frames - integer

    return a numpy array with shape (num frames, 3) representing the ball trajectory.
    '''

    trajectory = [initial_position.copy()]
    position = initial_position
    velocity = initial_velocity

    gravity_term = np.array([0,0,-GRAVITY_COEFF])
    
    for i in range(number_of_frames-1):
        position += DELTA_T * velocity

        drag_term = -DRAG_COEFF * np.linalg.norm(velocity) * velocity
        velocity += DELTA_T * (drag_term + gravity_term)

        print 'position: %s, velocity: %s' % (position, velocity)
        trajectory.append(position.copy())

    return np.vstack(trajectory)

def plot_trajectory(initial_velocity, trajectory):

    initial_position = trajectory[0,:2]
    direction_xy = initial_velocity[:2] / np.linalg.norm(initial_velocity[:2])
    trajectory_xy = trajectory[:,:2] - initial_position
    trajectory_z = trajectory[:,2]

    target_xy = TARGET_POSITION[:2] - initial_position
    target_z =  TARGET_POSITION[2]

    trajectory_xy = np.dot(direction_xy, trajectory_xy.transpose())
    target_xy = np.dot(direction_xy, target_xy)

    print target_xy
    print target_z
    
    plt.plot(trajectory_xy, trajectory_z)
    plt.plot(target_xy, target_z, 'bo')
    plt.show()
    
def draw_trajectory(images, camera_matricies, initial_world_position, initial_world_velocity):
    absolute_image, soccer_field = load_absolute_soccer_field()
    ball_radius = soccer_field.get_soccer_ball_radius()
    num_frames = len(images)
    
    trajectory = generate_trajectory(initial_world_position, initial_world_velocity, num_frames)

    print 'the trajectory is:'
    print trajectory

    primitive_drawing.draw_points(absolute_image, trajectory[:,:2], (255,0,0))
    primitive_drawing.draw_point(absolute_image, TARGET_POSITION[:2], (255,0,0))
    cv2.imshow('absolute_trajectory', absolute_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    plot_trajectory(initial_world_velocity, trajectory)
    
    for i, (image, camera_matrix, position) in enumerate(zip(images, camera_matrices, trajectory)):
        draw_soccer_balls(image, camera_matrix, ball_radius, position.reshape((1,3)))

        H = camera_matrix.get_homography_matrix()
        projection_to_field = position[:2]
        projection_to_field = planar_geometry.apply_homography_to_point(H, projection_to_field)
        primitive_drawing.draw_point(image, projection_to_field, (0,0,0))
        
        cv2.imshow('frame %d' % i, image)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    return
        
if __name__ == '__main__':
    frame_indices = [i for i in range(96,117) if i not in (99,105,111)]
    images, camera_matrices = load_images_and_cameras(frame_indices)
    #initial_position = np.array([438, 229, 3.38])
    #next_position = np.array([441.84, 222.55, 3.71])

    #initial_position = np.array([439, 226, 2.26])
    #next_position =    np.array([442.7, 219.8, 2.65])

    initial_position = np.array([439, 223, 1.41])
    next_position =    np.array([442.7, 217, 1.85])
    initial_velocity = (next_position - initial_position) * FPS

    draw_trajectory(images, camera_matrices, initial_position, initial_velocity)
