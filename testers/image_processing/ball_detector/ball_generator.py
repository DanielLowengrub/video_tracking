import numpy as np
import cv2
from ....source.auxillary.soccer_field_geometry import SoccerFieldGeometry
from ....source.image_processing.image_annotator.soccer_annotation import SoccerAnnotation
from ....source.auxillary.camera_matrix import CameraMatrix
from ....source.auxillary import planar_geometry, world_to_image_projections
from ....source.auxillary.rectangle import Rectangle
from ....source.auxillary.fit_points_to_shape.ellipse import Ellipse
from ....source.auxillary.fit_points_to_shape.line import Line
from ...auxillary import primitive_drawing

"""
This file contains code to generate balls on images, based on the absolute position or image position of the ball.
"""

# FRAME_INDEX = 97
# IMAGE_BASENAME = 'test_data/images_0_269/image_%d.png'
# CAMERA_BASENAME = 'output/scripts/preprocessing/data/images_0_269/camera/camera_0_269/camera_%d.npy'
# ANNOTATION_BASENAME = 'output/scripts/preprocessing/data/images_0_269/annotation/annotation_%d.npy'

FRAME_INDEX = 975
IMAGE_BASENAME = '/Users/daniel/Documents/soccer/images/images-8_15-9_31/image_%d.png'
CAMERA_BASENAME ='/Users/daniel/Documents/soccer/output/preprocessing/data/images-8_15-9_31/camera/camera_0_1874/camera_%d.npy'
ANNOTATION_BASENAME = '/Users/daniel/Documents/soccer/output/preprocessing/data/images-8_15-9_31/annotation/annotation_%d.npy'

IMAGE_FILE = IMAGE_BASENAME % FRAME_INDEX
CAMERA_FILE = CAMERA_BASENAME % FRAME_INDEX

FRAMES_AND_POSITIONS = [(239, np.array([273,165])), (233,np.array([224,147])), (230,np.array([201,143])),
                        (101,np.array([227,168])), (14,np.array([242,178])), (12,np.array([237,182])),
                        (114,np.array([266,140])), (113,np.array([263,140])), (35,np.array([234,170])),
                        (37,np.array([229,172])), (46,np.array([210,180])), (118,np.array([272,131])),
                        (107, np.array([245,151])), (975, np.array([121,111])), (975, np.array([121,110]))]

BAD_FRAMES_AND_POSITIONS = [(239, np.array([133,285])), (101, np.array([85,141])), (37,np.array([239,171])),
                            (5,np.array([201,149]))]

GRASS_FRAMES_AND_POSITIONS = [(5,np.array([201,149])), (5,np.array([242,213])), (5,np.array([231,200])),
                              (5,np.array([79,81])), (107, np.array([378,235])), (98, np.array([359,221]))]

# IMAGE_BASENAME = 'test_data/video7/image_%d.png'
# CAMERA_BASENAME = 'output/scripts/preprocessing/data/video7/camera/camera_0_274/camera_%d.npy'
# ANNOTATION_BASENAME = 'output/scripts/preprocessing/data/video7/annotation/annotation_%d.npy'

# FRAMES_AND_POSITIONS = [(10, np.array([197,197])), (80, np.array([231,171])), (239, np.array([165,140])),
#                         (20, np.array([247,202])), (30, np.array([282,207])), (19, np.array([242,201])),
#                         (15, np.array([222,199]))]

# GRASS_FRAMES_AND_POSITIONS = [(20, np.array([335,129])), (30, np.array([323,173]))]

def load_absolute_soccer_field():
    '''
    Return a SoccerFieldGeometry object
    '''
    ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)
    SCALE = 6
    FIELD_WIDTH = absolute_image.shape[1]-1
    FIELD_LENGTH = absolute_image.shape[0]-1
    GRID_WIDTH = 20
    GRID_LENGTH = 14
    soccer_field = SoccerFieldGeometry(SCALE, FIELD_WIDTH, FIELD_LENGTH, GRID_WIDTH, GRID_LENGTH)

    return soccer_field

def load_image_and_camera(image_file, camera_file):
    '''
    return a tuple (image, camera from absolute coordinates)
    '''
    image = cv2.imread(image_file, 1)
    camera_matrix = CameraMatrix(np.load(camera_file))

    return image, camera_matrix

def draw_soccer_balls(image, camera_matrix, ball_radius, ball_centers, alpha=0.7):
    '''
    image - the image on which to draw the balls
    camera_matrix - a CameraMatrix object
    ball_radius - the radius of the ball in absolute coords
    world_positions - a numpy array with shape (num balls, 3)
    '''

    #draw the ellipses on the image
    ball_ellipses = world_to_image_projections.project_spheres_to_image(camera_matrix, ball_radius, ball_centers)

    ellipse_rectangles, ellipse_masks = planar_geometry.build_ellipse_masks(ball_ellipses)
    image_rectangle = Rectangle.from_array(image)
    
    for E, rectangle, mask in zip(ball_ellipses, ellipse_rectangles, ellipse_masks):
        if not rectangle.contained_in(image_rectangle):
            continue
        
        image_slice = rectangle.get_slice(image)
        white_image = image_slice.copy()
        white_image[mask] = np.array([255,0,0])
        image_slice[...] = alpha*white_image + (1-alpha)*image_slice

        ellipse = Ellipse.from_parametric_representation(E)
        ellipse.compute_geometric_parameters()
        print ellipse


    return

def draw_soccer_balls_on_grid(image_file, camera_file):
    '''
    Draw a ball at each grid point of the soccer field.
    '''
    image, camera_matrix = load_image_and_camera(image_file, camera_file)
    soccer_field = load_absolute_soccer_field()
    
    ball_radius = soccer_field.get_soccer_ball_radius()
    grid_points = soccer_field.get_grid_points()

    #compute the ellipses which are the projections of the balls to the field.
    ball_heights = np.ones(len(grid_points)).reshape((-1,1)) * ball_radius
    ball_centers = np.hstack([grid_points, ball_heights])

    draw_soccer_balls(image, camera_matrix, ball_radius, ball_centers)

    cv2.imshow('image with balls', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    return

def draw_soccer_balls_at_heights(image_file, camera_file, position_on_image):
    '''
    Draw a ball at each grid point of the soccer field.
    '''
    image, camera_matrix = load_image_and_camera(image_file, camera_file)
    soccer_field = load_absolute_soccer_field()
    
    ball_radius = soccer_field.get_soccer_ball_radius()
    H = camera_matrix.get_homography_matrix()
    position_on_field = planar_geometry.apply_homography_to_point(np.linalg.inv(H), position_on_image)
    
    #compute the ellipses which are the projections of the balls to the field.
    ball_heights = np.linspace(ball_radius, 70*ball_radius, 10).reshape((-1,1))
    print 'heights: ', ball_heights
    positions_on_field = np.ones((len(ball_heights),2)) * position_on_field   
    ball_centers = np.hstack([positions_on_field, ball_heights])

    draw_soccer_balls(image, camera_matrix, ball_radius, ball_centers)
    
    cv2.imshow('image with balls', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    cv2.imwrite('ball_test.png', image)
    return

def get_segment_on_field(camera_matrix, soccer_field, position_on_field):
    '''
    camera_matrix - a CameraMatrix object
    soccer_field - a SoccerFieldGeometry object
    position_on_field - a numpy array with length 2

    return - a numpy array with shape (2,3). The first row is append(position_on_field, 0).
    The second point is the point on the line connecting the position_on_field and the camera center which is
    furthest from the position on the field out of all of the points on the line that are above the field.
    '''
    #the line that we want is the line connecting the camera to the position on the field.
    #in addition, we only want the part of the line that is above the soccer field
    camera_matrix.compute_camera_parameters()
    camera_position = camera_matrix.get_camera_position()

    #get the line on the field conneting the position on the field to the (x,y) coords of the camera
    line_on_field_direction = camera_position[:2] - position_on_field
    line_on_field_direction /= np.linalg.norm(line_on_field_direction)
    print 'line on field direction: ', line_on_field_direction
    line_on_field = Line.from_point_and_direction(position_on_field, line_on_field_direction)

    print 'camera_position: ', camera_position
    print 'line on field: ', line_on_field
    
    #intersect this line with the field rectangle
    field_rectangle = soccer_field.get_field_rectangle()
    field_region = np.vstack([field_rectangle.get_top_left(), field_rectangle.get_bottom_right()])
    intersection_points = planar_geometry.line_region_intersection(line_on_field, field_region)

    print 'intersection points: ', intersection_points
    
    #check which of the intersection points lies between the point on the field and the camera position
    #store the number alpha so that this intersection point is equal to
    #point_on_field + alpha*line_on_field_direction
    alphas = np.matmul(line_on_field_direction, intersection_points.transpose())
    print 'alphas: ', alphas

    index_of_point = np.argwhere(alphas > 0)
    alpha = alphas[index_of_point]
    print 'alpha: ', alpha
    
    #now get the direction of the 3d line, normalized to that the norm of the x,y coords is one
    world_position = np.append(position_on_field, 0) 
    world_line_direction = camera_position - world_position
    world_line_direction /= np.linalg.norm(world_line_direction[:2])

    print 'world_line_direction: ', world_line_direction
    
    #use alpha and this direction to get the 3d coords of the last point on the line from the field to the camra that is
    #above the field
    last_point_on_line = world_position + alpha*world_line_direction

    return np.vstack([world_position, last_point_on_line])

def draw_ball_at_preimages_of_point(position_on_image, image_file, camera_file):
    '''
    position_on_image - a position on the image
    there is a line in 3D whose camera projection is the given point on the image.
    Draw a ball at these points.
    '''
    image, camera_matrix = load_image_and_camera(image_file, camera_file)
    soccer_field = load_absolute_soccer_field()
    
    ball_radius = soccer_field.get_soccer_ball_radius()
    H = camera_matrix.get_homography_matrix()
    position_on_field = planar_geometry.apply_homography_to_point(np.linalg.inv(H), position_on_image)

    print 'position on field: ', position_on_field
    
    segment_on_field = get_segment_on_field(camera_matrix, soccer_field, position_on_field)
    print 'segement_on_field:'
    print segment_on_field
    
    points_on_segment = [((1-alpha)*segment_on_field[0] + alpha*segment_on_field[1])# + np.array([0,0,ball_radius])
                         for alpha in np.linspace(0,1,10)]

    print 'points in segement: '
    print np.vstack(points_on_segment)

    for p in points_on_segment:
        image_copy = image.copy()
        draw_soccer_balls(image_copy, camera_matrix, ball_radius, p.reshape((1,3)))
    
        cv2.imshow('image with balls', image_copy)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    return

def generate_radius_sq_map(ellipse, max_radius):
    '''
    ellipse - an Ellipse object
    max_radius - an integer

    return a numpy array with shape (2*max_radius + 1, 2*max_radius + 1) and type int32.
    It the value of the entry (y,x) represents the value of the ellipse on the point center_of_ellipse+(x,y)
    '''
    center, axes, angle = ellipse.get_geometric_parameters()
    M = ellipse._get_normalized_form()

    print 'normalized ellipse: '
    print M
    print 'center: ', center

    rectangle_center = np.rint(center).astype(np.int32)
    center_offset = rectangle_center - center
    
    x = np.arange(-max_radius, max_radius+1,1) + center_offset[0]
    y = np.arange(-max_radius, max_radius+1,1) + center_offset[1]

    XX, YY = np.meshgrid(x,y)
    #ellipse_values = planar_geometry.evaluate_ellipse_on_grid(ellipse, XX, YY)
    ellipse_values = XX*XX*M[0,0] + 2*XX*YY*M[0,1] + YY*YY*M[1,1]

    print 'ellipse values sq < 4:'
    #print np.int32(ellipse_values)
    #print ellipse_values < 0.25

    top_left = np.int32(center) - np.array([max_radius, max_radius])
    rectangle = Rectangle.from_top_left_and_shape(top_left, ellipse_values.shape)
    
    #return np.int32(ellipse_values)
    return rectangle, ellipse_values

def compute_average_saturation(image_sat, camera_matrix, ball_radius, ball_position):
    max_radius = 5

    ball_ellipse = world_to_image_projections.project_spheres_to_image(camera_matrix, ball_radius, ball_position)[0]
    E = Ellipse.from_parametric_representation(ball_ellipse)
    E.compute_geometric_parameters()
    #print 'the ball ellipse: ', E

    rectangle, radius_sq_map = generate_radius_sq_map(E, max_radius)

    # print 'rectangle: '
    # print rectangle
    # print 'radius sq map: '
    # print radius_sq_map
    
    image_sat = rectangle.get_slice(image_sat)

    average_saturation = []
    radius_regions = np.zeros(radius_sq_map.shape, np.int32)
    region_boundaries = np.arange(0, max_radius+1)**2
    
    for i in range(len(region_boundaries)-1):
        start = region_boundaries[i]
        end = region_boundaries[i+1]
        region_mask = (start <= radius_sq_map) * (radius_sq_map < end)

        if region_mask.sum() == 0:
            break
        
        radius_regions[region_mask] = i
        average_saturation.append(image_sat[region_mask].mean())

    print 'radius regions:'
    print radius_regions

    print 'image sat: '
    print image_sat
    
    average_saturation = np.array(average_saturation)

    return average_saturation

def compute_std(edges, camera_matrix, ball_radius, ball_position):
    max_radius = 6
    
    inside_max = 1.5
    outside_min = 4
    outside_max = 10
    
    ball_ellipse = world_to_image_projections.project_spheres_to_image(camera_matrix, ball_radius, ball_position)[0]
    E = Ellipse.from_parametric_representation(ball_ellipse)
    E.compute_geometric_parameters()

    #edges = cv2.Canny(image_hs[:,:,1], 80, 150)
        
    rectangle, radius_sq_map = generate_radius_sq_map(E, max_radius)
    # image_sat = rectangle.get_slice(image_hs[:,:,1])
    # image_hue = rectangle.get_slice(image_hs[:,:,0])
    edges = rectangle.get_slice(edges)

    
    #print 'image sat:'
    #print image_sat

    height_map = radius_sq_map.copy()
    # print 'height map:'
    # print height_map

    inside = (radius_sq_map <= inside_max)
    outside = np.logical_and(outside_min <= radius_sq_map, radius_sq_map <= outside_max)
    
    # print 'ellipse_boundary:'
    # print np.int32(ellipse_boundary)
    print 'edges:'
    print np.int32(edges > 0)
    # print 'edges * inside:'
    # print (edges > 0) * (radius_sq_map < 4)
    print 'edges, inside, outside:'
    m = np.int32(inside.copy())
    m[outside] = 3
    
    m[np.logical_and(edges > 0, inside)] = 2
    m[np.logical_and(edges > 0, outside)] = 4
    print m
    # print 'height map on edges:'
    # height_map[edges == 0] = 0
    # print height_map

    # print 'height map on boundary:'
    # height_map = radius_sq_map.copy()
    # height_map[ellipse_boundary == 0] = 0
    # print height_map

    # ball_mask = radius_sq_map <= 1
    # grass_mask = (radius_sq_map > 1) * (radius_sq_map) <= 25

    # ball_sat_std = image_sat[ball_mask].std()
    # ball_sat_mean = image_sat[ball_mask].mean()

    # hue_std = image_hue.std()
    # hue_mean = image_hue.mean()

    # ball_log_prob =  -((image_sat[ball_mask] - 70.0)**2 / (15**2)).mean()
    # hue_log_prob = -((image_hue - 44)**2 / (2.0**2)).mean()
    
    # print 'ball mean: ', ball_sat_mean
    # print 'ball std:  ', ball_sat_std

    # print 'hue mean: ', hue_mean
    # print 'hue std: ', hue_std

    #print 'ball values: ', image_sat[ball_mask]
    #print 'ball exponents: ', ((image_sat[ball_mask] - 70.0)**2 / (10**2))
    # print 'ball log prob: ', ball_log_prob
    #print 'ball prob: ', ball_prob
    #print 'grass values: ', image_sat[grass_mask]
    #print 'grass exponents: ', ((image_sat[grass_mask] - 113.0)**2 / (20**2))
    # print 'hue log prob: ', hue_log_prob
    #print 'grass prob: ', grass_prob
    # print 'total log prob: ', ball_log_prob + hue_log_prob
    #print 'mean diff: ', image_sat[grass_mask].mean() - image_sat[ball_mask].mean()

    inside_area = float(inside.sum())
    outside_area = float(outside.sum())
    inside_ratio = ((edges > 0) * inside).sum() / inside_area
    outside_ratio = ((edges > 0) * outside).sum() / outside_area

    print 'num edges inside: ', ((edges > 0) * inside).sum()
    print 'inside area: ', inside_area
    print 'inside ratio: ', inside_ratio

    print 'num edges outside: ', ((edges > 0) * outside).sum()
    print 'outside area: ', outside_area
    print 'outside ratio: ', outside_ratio
    
    print 'edges score: ', inside_ratio - 3*outside_ratio
    #return ball_mean, ball_std, grass_mean, grass_std
    #return ball_log_prob + hue_log_prob
    return inside_ratio - 3*outside_ratio

def view_std(position_on_image, edges, camera_matrix):
    #image, camera_matrix = load_image_and_camera(image_file, camera_file)
    soccer_field = load_absolute_soccer_field()

    ball_radius = soccer_field.get_soccer_ball_radius()
    H = camera_matrix.get_homography_matrix()
    position_on_field = planar_geometry.apply_homography_to_point(np.linalg.inv(H), position_on_image)
    position_on_field = np.append(position_on_field, 0).reshape((1,3))

    print 'position on field: ', position_on_field

    #image_hs = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)[:,:,:2]
    
    #ball_mean, ball_std, grass_mean, grass_std = compute_std(image_sat, camera_matrix, ball_radius, position_on_field)
    #log_prob = compute_std(image_hs, camera_matrix, ball_radius, position_on_field)
    edges_score = compute_std(edges, camera_matrix, ball_radius, position_on_field)
    
    image_color = cv2.cvtColor(edges, cv2.COLOR_GRAY2BGR)
    primitive_drawing.draw_point(image_color, position_on_image, (255,0,0))
    cv2.imshow('image with position', image_color)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    #return ball_mean, ball_std, grass_mean, grass_std
    #return log_prob
    return edges_score

def view_radius_sq_map(position_on_image, image_file, camera_file):
    image, camera_matrix = load_image_and_camera(image_file, camera_file)
    image_copy = image.copy()
    primitive_drawing.draw_point(image_copy, position_on_image, (255,0,0))
    cv2.imshow('image with position', image_copy)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    soccer_field = load_absolute_soccer_field()

    ball_radius = soccer_field.get_soccer_ball_radius()
    H = camera_matrix.get_homography_matrix()
    position_on_field = planar_geometry.apply_homography_to_point(np.linalg.inv(H), position_on_image)
    position_on_field = np.append(position_on_field, 0).reshape((1,3))
    #position_on_field = np.array([ 469.92,  228.56  , 23.42])
    print 'position on field: ', position_on_field

    image_sat = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)[:,:,1]
    average_saturation = compute_average_saturation(image_sat, camera_matrix, ball_radius, position_on_field)

    print 'average saturation: ', average_saturation

    ball_mean = average_saturation.min()
    grass_mean = average_saturation.max()
    
    #saturation_ratios = (average_saturation - grass_mean) / (ball_mean - grass_mean)
    saturation_ratios = 1 / (1 + (average_saturation - ball_mean))
    print 'saturation ratios: ', saturation_ratios
    #expected_ratios = np.array([1,0.2] + [0 for x in saturation_ratios[2:]])
    expected_ratios = np.array([1, 0.03] + [0.02 for x in saturation_ratios[2:]])
    print 'score: ', np.linalg.norm(saturation_ratios - expected_ratios)

    return saturation_ratios

if __name__ == '__main__':
    np.set_printoptions(precision=2, linewidth=200)
    #draw_soccer_balls_on_grid(IMAGE_FILE, CAMERA_FILE)
    #draw_soccer_balls_at_heights(IMAGE_FILE, CAMERA_FILE, np.array([268,328], np.float32))
    #image_position = np.array([214,174], np.float32) #97
    #image_position = np.array([209, 177], np.float32) #96
    #image_position = np.array([271, 135], np.float32) #116

    # frame, image_position = FRAMES_AND_POSITIONS[0]
    # image_file = IMAGE_BASENAME % frame
    # camera_file = CAMERA_BASENAME % frame
    # draw_ball_at_preimages_of_point(image_position, image_file, camera_file)

    frame, position = FRAMES_AND_POSITIONS[13]
    image_file = IMAGE_BASENAME % frame
    camera_file = CAMERA_BASENAME % frame
    annotation_file = ANNOTATION_BASENAME % frame
    image, camera_matrix = load_image_and_camera(image_file, camera_file)
    annotation = SoccerAnnotation(np.load(annotation_file))
    sidelines_mask = (annotation.get_sidelines_mask() > 0)
    
    size = 2
    tx = np.arange(-size,size+1,1)
    ty = np.arange(-size,size+1,1)
    TX,TY = np.meshgrid(tx,ty)
    translations = np.dstack([TX,TY])

    cv2.imshow('image', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    edges = cv2.Canny(image, 150, 200)
    edges[sidelines_mask] = 0
    cv2.imshow('edges minus sidelines', edges)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    # kernel = np.ones((3,3))
    # edges = cv2.morphologyEx(edges, cv2.MORPH_CLOSE, kernel)
    
    # cv2.imshow('edges filled in', edges)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    
    edge_scores = []
    for i,t in enumerate(translations.reshape((-1,2))):
        print '\n\ncomputing translation %d:' % i
        edge_scores.append(view_std(position+t, edges, camera_matrix))

    edge_scores = np.array(edge_scores).reshape(translations.shape[:2])
    print 'edge scores:'
    print edge_scores

    print 'best: ', edge_scores.max()
