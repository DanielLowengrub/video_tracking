import os
import itertools
import numpy as np
import cv2
import matplotlib.pyplot as plt

from ....source.image_processing.image_annotator.soccer_annotation import SoccerAnnotation
from ....source.data_structures.contour import Contour
from ....source.auxillary.fit_points_to_shape.fit_points_to_line import FitPointsToLine
from ....source.auxillary.fit_points_to_shape.fit_points_to_ellipse_stable import FitPointsToEllipseStable
from ....source.image_processing.hough_transform.tangent_detector import TangentDetector, WeightedTangentLines
from ...auxillary.fit_points_to_shape.fit_points_to_ellipse import generate_points as generate_points_on_ellipse

def plot_histogram(weighted_tangent_lines_dict, theta_bins, radius_bins):
    hist = np.zeros((len(theta_bins)+1, len(radius_bins)+1), np.float32)

    for key, weighted_tangent_lines in weighted_tangent_lines_dict.iteritems():
        print 'weight(%d, %d) = %f' % (key[0], key[1], weighted_tangent_lines.weight)
        hist[key[0], key[1]] = weighted_tangent_lines.weight

    p = plt.imshow(hist, interpolation="nearest")
    plt.colorbar(p)
    plt.show()
    
def draw_tangent_lines(ax, tangent_lines):
    '''
    draw a tangent line by drawing the point in blue, and an arrow in the direction of the line.
    '''
    #tangent_lines = [tangent_line for tangent_line in tangent_lines if (-60<tangent_line.line.get_angle_radius(degrees=True)[1]<-20 and 106<tangent_line.line.get_angle_radius(degrees=True)[0]<117)]

    TANGENT_LENGTH = 10
    centers = np.vstack(tangent_line.center for tangent_line in tangent_lines)
    line_directions = np.vstack(tangent_line.line.get_tangent_vector(tangent_line.center) for tangent_line in tangent_lines)
    
    #plot the center in blue
    ax.plot(centers[:,0], centers[:,1], 'bo')

    #draw arrows in the directions of the lines
    ax.quiver(centers[:,0], centers[:,1], 10*line_directions[:,0], 10*line_directions[:,1], scale_units='xy', angles='xy', scale=1)

    return

def draw_contours(ax, cv_contours):
    for cv_contour in cv_contours:
        ax.plot(cv_contour[:,0,0], cv_contour[:,0,1])

    return

def display_matching_summary(image, contours, weighted_tangent_lines_dict):
    fig = plt.figure()
    ax = fig.add_subplot(111, aspect='equal')
    
    #draw the contours
    draw_contours(ax, [contour.get_cv_contour() for contour in contours])
    
    #then draw the tangent lines
    tangent_lines = list(itertools.chain(*(weighted_tangent_lines.tangent_lines for weighted_tangent_lines in weighted_tangent_lines_dict.itervalues())))
    draw_tangent_lines(ax, tangent_lines)

    ax.set_xlim(-10, image.shape[1] + 10)
    ax.set_ylim(image.shape[0] + 10, -10)
    
    plt.show()
    
    return

def generate_basic_contour():
    '''
    We generate a single line:
    (50,100)----->------------>--(450,100)
    (50,100+h)----------<--------(450,100+h)
    '''
    
    h=10
    points_in_top_row = 10
    points_in_bottom_row = 10
    
    x_coords_top = np.linspace(50, 450, points_in_top_row)
    coords_top = np.vstack([x_coords_top, 100*np.ones(len(x_coords_top))]).transpose()

    x_coords_bottom = np.linspace(50, 450, points_in_bottom_row)
    coords_bottom = np.vstack([x_coords_bottom, (100 + h)*np.ones(len(x_coords_bottom))]).transpose()
    coords_bottom = coords_bottom[::-1]
    
    contour = np.vstack([coords_top, coords_bottom]).reshape((-1,1,2))

    #add noise
    #contour += 0.1*np.random.randn(*contour.shape)

    point_mask = np.ones(len(contour), np.bool)
    point_mask[len(coords_top)/2] = False
    point_mask[-len(coords_bottom)/2] = False
               
    return np.int32(contour), point_mask

def basic_test():
    MIN_LINE_LENGTH = 50
    MAX_ALGEBRAIC_RESIDUE = 1
    THETA_BINS = np.arange(0,181,2)
    L = np.linalg.norm(np.array([500,500]))
    RADIUS_BINS = np.arange(-L, L+1,5)
    SHAPE_FITTER = FitPointsToLine()
    
    image = np.zeros((500, 500, 3))
    cv_contour, point_mask = generate_basic_contour()
    contour = Contour(cv_contour, image)
    
    image_with_contour = image.copy()
    radius = 3
    color = (255,0,0)
    masked_color = (50,50,50)
    tangent_point_color = (0,255,0)
    thickness = -1

    print cv_contour.shape
    
    cv2.drawContours(image_with_contour, [cv_contour], -1, (255,0,0), 1)
    for i,point in enumerate(cv_contour):
       if point_mask[i]:
           cv2.circle(image_with_contour, tuple(np.int32(point[0]).tolist()), radius, color, thickness=thickness)
       else:
           cv2.circle(image_with_contour, tuple(np.int32(point[0]).tolist()), radius, masked_color, thickness=thickness)
            
    print 'we are starting with the following contour: '
    cv2.imshow('image with contours and points', image_with_contour)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    tangent_detector = TangentDetector(SHAPE_FITTER, MIN_LINE_LENGTH, MAX_ALGEBRAIC_RESIDUE, THETA_BINS, RADIUS_BINS)
    
    print 'looking for lines in the contour...'
    weighted_tangent_lines_dict = tangent_detector.get_weighted_tangent_lines_dict(contour, point_mask)
    tangent_point_mask = tangent_detector.get_point_mask()
    print 'tangent point mask: ', tangent_point_mask
    for i,point in enumerate(cv_contour):
       if tangent_point_mask[i]:
           cv2.circle(image_with_contour, tuple(np.int32(point[0]).tolist()), radius, tangent_point_color, thickness=thickness)

    cv2.imshow('tangent points in green', image_with_contour)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    display_matching_summary(image, [contour], weighted_tangent_lines_dict)
    plot_histogram(weighted_tangent_lines_dict, THETA_BINS, RADIUS_BINS)
    
    return

def basic_ellipse_test():
    MIN_LINE_LENGTH = 50
    MAX_ALGEBRAIC_RESIDUE = 200
    THETA_BINS = np.arange(0,181,2)
    L = np.linalg.norm(np.array([500,500]))
    RADIUS_BINS = np.arange(-L, L+1,5)
    SHAPE_FITTER = FitPointsToEllipseStable()
    
    center = np.array([200, 150])
    axes = np.array([80, 50])
    angle = 3*np.pi/5.0
    start_angle = 0
    end_angle = 2*np.pi
    points = generate_points_on_ellipse(center, axes, angle, start_angle, end_angle, num_points=50, noise_std=0.1)
    point_mask = np.ones(len(points), np.bool)

    image = np.zeros((300, 500, 3), np.uint8)
    contour = Contour(points.reshape((-1,1,2)), image)
    
    print 'fitting an ellipse to the following points:'

    radius = 2
    color = (255, 0, 0)
    thickness = -1

    for point in points:
        translated_point = point[0]
        cv2.circle(image, tuple(np.int32(translated_point).tolist()), radius, color, thickness=thickness)

    cv2.imshow('points', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    tangent_detector = TangentDetector(SHAPE_FITTER, MIN_LINE_LENGTH, MAX_ALGEBRAIC_RESIDUE, THETA_BINS, RADIUS_BINS)
    
    print 'looking for lines in the contour...'
    weighted_tangent_lines_dict = tangent_detector.get_weighted_tangent_lines_dict(contour, point_mask)

    display_matching_summary(image, [contour], weighted_tangent_lines_dict)
    plot_histogram(weighted_tangent_lines_dict, THETA_BINS, RADIUS_BINS)
    
    return

def test_on_soccer_image():
    from ....source.image_processing.contour_generator.edges_contour_generator import EdgesContourGenerator
    #from ....source.image_processing.field_extractor.stupid_field_extractor import StupidFieldExtractor

    #MIN_LINE_LENGTH = 40
    #MAX_ALGEBRAIC_RESIDUE = 3
    MIN_LINE_LENGTH = 20
    MAX_ALGEBRAIC_RESIDUE = 1.5
    SHAPE_FITTER = FitPointsToLine()
    
    #interesting test images
    #215
    #141
    #2
    #96

    #TEST_IMAGE_NAME = 'test_data/video3/out255.png'
    IMAGE_FILENAME = os.path.join('/Users/daniel/Documents/soccer/output/preprocessing_full/data/images-0_00-1_00',
                                  'stage_0/image/interval_0_1498', 'frame_481', 'image.png')
    ANNOTATION_FILENAME = os.path.join('/Users/daniel/Documents/soccer/output/preprocessing_full/data/images-0_00-1_00',
                                       'stage_0/annotation/interval_0_1498', 'frame_481', 'annotation.npy')

    print 'loading image: ', IMAGE_FILENAME
    image = cv2.imread(IMAGE_FILENAME, 1)
    annotation = SoccerAnnotation(np.load(ANNOTATION_FILENAME))
    field_mask = annotation.get_soccer_field_mask()

    masked_image = image.copy()
    masked_image[field_mask == 0] = np.array([0,0,0])
    cv2.imshow('masked image', masked_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    
    THETA_BINS = np.arange(-180,181,2)
    L = np.linalg.norm(np.array(image.shape))
    RADIUS_BINS = np.arange(-L, L+1,5)

    tangent_detector = TangentDetector(SHAPE_FITTER, MIN_LINE_LENGTH, MAX_ALGEBRAIC_RESIDUE, THETA_BINS, RADIUS_BINS)
    

    #print 'getting the field mask...'
    #field_extractor = StupidFieldExtractor()
    #field_mask = field_extractor.get_field_mask(image)
    
    print 'generating contours...'
    contourGenerator = EdgesContourGenerator()
    contours = contourGenerator.generate_contours(image, mask=field_mask)
    contours += itertools.chain(*(contour.get_child_contours() for contour in contours))
    
    weighted_tangent_lines_dict = dict()
    
    print 'finding lines in contours...'
    #cv_contours = cv_contours[14:15]
    
    for contour in contours:
        #print 'finding lines in this contour'
        #image_copy = image.copy()
        cv_contour = contour.get_cv_contour()
        #first draw the contours on the image in white
        cv2.drawContours(image, [cv_contour], -1, (255, 255, 255), 1)

        #then draw all the points in blue
        points = cv_contour.reshape((-1,2))
        color = (255,0,0)
    
        for point in points:
            image[point[1],point[0]] = np.array([255,0,0])
        
        new_weighted_tangent_lines_dict = tangent_detector.get_weighted_tangent_lines_dict(contour)

        #update the values in the weighted_tangent_lines_dict of all of the contours together
        for key, new_weighted_tangent_lines in new_weighted_tangent_lines_dict.iteritems():
            if key in weighted_tangent_lines_dict:
                weighted_tangent_lines = weighted_tangent_lines_dict[key]
                tangent_lines = weighted_tangent_lines.tangent_lines + new_weighted_tangent_lines.tangent_lines
                weight = weighted_tangent_lines.weight + new_weighted_tangent_lines.weight
                weighted_tangent_lines_dict[key] = WeightedTangentLines(tangent_lines, weight)
                
            else:
                weighted_tangent_lines_dict[key] = new_weighted_tangent_lines
        
    cv2.imshow('image with contour and points', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    for key, weighted_tangent_lines in weighted_tangent_lines_dict.iteritems():
        print 'bin (%d, %d) has %d lines.' % (key[0], key[1], len(weighted_tangent_lines.tangent_lines))
                                              
    display_matching_summary(image, contours, weighted_tangent_lines_dict)
    plot_histogram(weighted_tangent_lines_dict, THETA_BINS, RADIUS_BINS)
    
if __name__ == '__main__':
    #basic_test()
    #basic_ellipse_test()
    test_on_soccer_image()
