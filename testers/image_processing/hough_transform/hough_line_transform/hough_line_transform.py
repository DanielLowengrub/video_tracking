import os
import numpy as np
import cv2
import collections
import matplotlib.pyplot as plt
from ....auxillary import primitive_drawing
from .....source.auxillary import mask_operations, skeletonization
from .....source.image_processing.image_annotator.soccer_annotation import SoccerAnnotation
from .....source.image_processing.hough_transform.hough_line_transform.hough_line_transform import HoughLineTransform

NUM_THETA_BINS  = 800
NUM_RADIUS_BINS = 300
PEAK_THRESHOLD = 60
PEAK_THRESHOLD_RATIO = 0.75
CLUSTERING_RADIUS = 10

MAX_THETA_STD = 1.0
MAX_RADIUS_STD = 3.0

LINE_COLORS = [(255,0,0), (0,255,0), (0,0,255), (150,150,150), (255,255,0)]

def display_hough_transform(mask, center, hough_transform, hough_bin_clusters):
    image = mask_operations.convert_to_image(mask)
    mean_line_image = mask_operations.convert_to_image(mask)
    
    for i,cluster in enumerate(hough_bin_clusters):
        print '\ncluster %d:' % i
        color = LINE_COLORS[i % len(LINE_COLORS)]

        mean_line = cluster.get_mean_line()
        mean_line_color = (0,255,0)
        if (cluster.get_theta_value_std()*180/np.pi > MAX_THETA_STD) or (cluster.get_radius_value_std() > MAX_RADIUS_STD):
            mean_line_color = (0,0,255)
        primitive_drawing.draw_line(mean_line_image, mean_line, mean_line_color, 1)
        
        print 'color: ', color
        print 'total count: ', cluster.get_total_count()
        print 'theta radius value mean:  [%f, %f]' % (cluster.get_theta_value_mean()*180/np.pi,
                                                      cluster.get_radius_value_mean())
        print 'theta radius value std :  [%f, %f]' % (cluster.get_theta_value_std()*180/np.pi,
                                                      cluster.get_radius_value_std())
        
        print ''
        
        for line in cluster.get_lines():
            primitive_drawing.draw_line(image, line, color, 1)

        for i,hough_bin in enumerate(cluster._hough_bins):
            print '   bin %d: bin=(%d, %d), value=(%f,%f),  count=%d' % (i, hough_bin.theta_bin, hough_bin.radius_bin,
                                                                         hough_bin.theta*180/np.pi, hough_bin.radius,
                                                                         hough_bin.count)
            
    # cv2.imshow('hough transform', np.float32(hough_transform.transpose()) / hough_transform.max())
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    
    # p = plt.imshow(hough_transform.transpose(), interpolation="nearest")
    # plt.colorbar(p)
    # plt.title('hough transform histogram')
    # plt.xlabel('theta')
    # plt.ylabel('radius')
    # plt.show()

    primitive_drawing.draw_point(image, center, (255,0,0), size=5)
    cv2.imshow('lines', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    primitive_drawing.draw_point(mean_line_image, center, (255,0,0), size=5)
    cv2.imshow('mean lines', mean_line_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    return

def soccer_test():
    #FRAME = 33 + 50
    #FRAME = 162 + 0
    #FRAME = 162 + 31
    #FRAME = 162 + 48
    #FRAME = 162 + 99
    #FRAME = 300
    #FRAME = 162 + 277
    #FRAME = 162 + 288
    #FRAME = 162 + 313
    #FRAME = 597 + 15
    #FRAME = 597 + 36
    #FRAME = 597 + 135
    #FRAME = 597 + 196
    #FRAME = 843 + 1
    #FRAME = 843 + 113
    #FRAME = 843 + 35
    #FRAME = 843 + 65
    #FRAME = 843 + 123
    #FRAME = 843 + 167
    #FRAME = 1141 + 0
    #FRAME = 1141 + 34
    #FRAME = 1141 + 56
    FRAME = 1141 + 63
    #FRAME = 1141 + 70
    #FRAME = 1141 + 106
    #FRAME = 1141 + 139
    #FRAME = 1141 + 165
    #FRAME = 1141 + 206
    #FRAME = 1141 + 317
    ANNOTATION_FILENAME = os.path.join('/Users/daniel/Documents/soccer/output/preprocessing_full/data/images-0_00-1_00',
                                       'stage_0/annotation/interval_0_1498', 'frame_%d' % FRAME, 'annotation.npy')
    annotation = SoccerAnnotation(np.load(ANNOTATION_FILENAME))
    mask = annotation.get_sidelines_mask() > 0

    cv2.imshow('mask', np.float32(mask))
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    mask = skeletonization.compute_skeleton(mask)
    center = np.array([mask.shape[1] / 2, mask.shape[0]/2])
    mask_color = mask_operations.convert_to_image(mask)
    primitive_drawing.draw_point(mask_color, center, (0,255,0), size=5)
    
    cv2.imshow('skeletonized mask with center', mask_color)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    ht_controller = HoughLineTransform(NUM_THETA_BINS, NUM_RADIUS_BINS, mask.shape, PEAK_THRESHOLD, PEAK_THRESHOLD_RATIO,
                                       CLUSTERING_RADIUS)
    hough_transform = ht_controller.build_hough_line_transform(mask)
    hough_bin_clusters = ht_controller.find_hough_bin_clusters(hough_transform)

    print 'the hough bin clusters:'
    print '\n'.join(map(str, hough_bin_clusters))
    
    display_hough_transform(mask, center, hough_transform, hough_bin_clusters)

    return

if __name__ == '__main__':
    soccer_test()

    
