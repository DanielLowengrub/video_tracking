import itertools
import numpy as np
import cv2
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse as EllipsePlotter

from ....source.data_structures.contour import Contour
from ....source.image_processing.hough_transform.ellipse_detector import EllipseDetector
from ....source.image_processing.hough_transform.line_detector import LineDetector
from ...auxillary.fit_points_to_shape.fit_points_to_ellipse import generate_points
from ....source.auxillary.contour_shape_overlap.line_with_position import LineWithPosition
from ...auxillary.contour_shape_overlap.highlighted_shape_generator import draw_line, draw_interval_mask
from ....source.auxillary.contour_shape_overlap.highlighted_shape_generator import HighlightedShapeGenerator
from ....source.auxillary.fit_points_to_shape.fit_points_to_ellipse_stable import FitPointsToEllipseStable

def display_ellipses(image, ellipses):
    for ellipse in ellipses:
        center, axes, angle = ellipse.get_geometric_parameters(in_degrees=True)
        cv2.ellipse(image, tuple(np.int32(center).tolist()), tuple(np.int32(axes).tolist()), angle, 0, 360, (0,255,0), 1, cv2.CV_AA)


    cv2.imshow('ellipses', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def draw_contours(ax, cv_contours):
    for cv_contour in cv_contours:
        ax.plot(cv_contour[:,0,0], cv_contour[:,0,1])

    return

def draw_tangent_lines(image, tangent_lines):
    '''
    draw a tangent line by drawing the point in blue, and an arrow in the direction of the line.
    '''
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.imshow(image)
    TANGENT_LENGTH = 10
    centers = np.vstack(tangent_line.center for tangent_line in tangent_lines)
    line_directions = np.vstack(tangent_line.line.get_tangent_vector(tangent_line.center) for tangent_line in tangent_lines)
    
    #plot the center in blue
    ax.plot(centers[:,0], centers[:,1], 'bo')

    #draw arrows in the directions of the lines
    ax.quiver(centers[:,0], centers[:,1], 10*line_directions[:,0], 10*line_directions[:,1], scale_units='xy', angles='xy', scale=1)

    ax.set_xlim(-10, image.shape[1] + 10)
    ax.set_ylim(image.shape[0] + 10, -10)

    plt.show()
    
    return

def generate_basic_contour():
    '''
    We generate a single ellipse

    '''
    number_of_outer_points = 50
    number_of_inner_points = 50
    noise = 0.1
    
    center = np.array([200, 200])
    rotation_angle = np.pi/4.0

    outer_axes = np.array([200, 150])
    outer_start_angle = 0
    outer_end_angle = 2*np.pi

    inner_axes = np.array([180, 110])
    inner_start_angle = 2*np.pi
    inner_end_angle = 0
    
    outer_ellipse = generate_points(center, outer_axes, rotation_angle, outer_start_angle, outer_end_angle, num_points=number_of_outer_points, noise_std=noise)
    inner_ellipse = generate_points(center, inner_axes, rotation_angle, inner_start_angle, inner_end_angle, num_points=number_of_inner_points, noise_std=noise)

    #contour = np.vstack([outer_ellipse, inner_ellipse])
    contour = outer_ellipse

    return np.int32(contour)

def basic_test():
    MIN_TANGENT_ARC_LENGTH = 10
    MAX_TANGENT_ALGEBRAIC_RESIDUE = 1
    THETA_BIN_WIDTH = 2
    RADIUS_BIN_WIDTH = 5
    MAX_THETA_DEVIATION = 3
    MAX_RADIUS_DEVIATION = 4
    MIN_ARC_LENGTH = 100
    MAX_AVERAGE_ALGEBRAIC_RESIDUE = 10

    CENTER_BIN_WIDTH = 5
    MIN_THETA_RADIUS_DISTANCE = 5
    MAX_TRIPLE_INTERSECTION_STD = 10
    MAX_CENTER_BIN_STD = 3
    MIN_POINTS_ON_ELLIPSE = 20
    MAX_ELLIPSE_RESIDUE = 10
    
    image = np.zeros((500, 500, 3))
    cv_contour = generate_basic_contour()
    contour = Contour(cv_contour, image)
    
    image_with_contour = image.copy()
    radius = 3
    color = (255,0,0)
    masked_color = (50,50,50)
    thickness = -1

    print cv_contour.shape
    
    cv2.drawContours(image_with_contour, [cv_contour], -1, (255,0,0), 1)
    cv2.circle(image_with_contour, (200,200), 3, (0,0,255), -1)        
    print 'we are starting with the following contour: '
    cv2.imshow('image with contours and points', image_with_contour)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    line_detector = LineDetector(MIN_TANGENT_ARC_LENGTH, MAX_TANGENT_ALGEBRAIC_RESIDUE, THETA_BIN_WIDTH, RADIUS_BIN_WIDTH, \
                                 MAX_THETA_DEVIATION, MAX_RADIUS_DEVIATION, MIN_ARC_LENGTH, MAX_AVERAGE_ALGEBRAIC_RESIDUE)
    ellipse_detector = EllipseDetector(CENTER_BIN_WIDTH, MIN_THETA_RADIUS_DISTANCE, MAX_TRIPLE_INTERSECTION_STD, MAX_CENTER_BIN_STD, \
                                       MIN_POINTS_ON_ELLIPSE, MAX_ELLIPSE_RESIDUE)
    
    print 'looking for lines in the contour...'
    lines = line_detector.get_lines(image, [contour])

    weighted_tangent_lines_dict = line_detector.get_disgarded_tangent_lines()
    tangent_lines_dict = dict()
    tl_id = 0
    for key, value in weighted_tangent_lines_dict.iteritems():
        tangent_line = value.tangent_lines[0]
        tangent_line.set_id(tl_id)
        tl_id += 1
        tangent_lines_dict[key] = [tangent_line]

    ellipses = ellipse_detector.get_ellipses(image_with_contour, tangent_lines_dict)

    display_ellipses(image, ellipses)
    
    return

def test_on_soccer_image():
    from ....source.image_processing.contour_generator.edges_contour_generator import EdgesContourGenerator
    from ....source.image_processing.field_extractor.stupid_field_extractor import StupidFieldExtractor

    MIN_TANGENT_TO_LINE_ARC_LENGTH = 20
    MIN_TANGENT_TO_ELLIPSE_ARC_LENGTH = 50
    MAX_TANGENT_TO_LINE_GEOMETRIC_RESIDUE = 1.5
    MAX_TANGENT_TO_ELLIPSE_GEOMETRIC_RESIDUE = 10
    MIN_TANGENT_TO_ELLIPSE_AREA = 500

    RANSAC_MIN_POINTS_TO_FIT = 20
    RANSAC_NUM_ITERATIONS = 20
    RANSAC_DISTANCE_TO_SHAPE_THRESHOLD = 3
    RANSAC_MIN_PERCENTAGE_IN_SHAPE = 0.9

    THETA_BIN_WIDTH = 2
    RADIUS_BIN_WIDTH = 5
    MAX_THETA_DEVIATION = 0.7
    MAX_RADIUS_DEVIATION = 5
    MIN_ARC_LENGTH = 300
    MAX_AVERAGE_ALGEBRAIC_RESIDUE = 10

    CENTER_BIN_WIDTH = 5
    MIN_THETA_RADIUS_DISTANCE = 2
    MAX_TRIPLE_INTERSECTION_STD = 1
    MAX_CENTER_BIN_STD = 10
    MIN_POINTS_ON_ELLIPSE = 15
    MAX_ELLIPSE_RESIDUE = 10

    MAX_TANGENT_LINES_PER_BUCKET = 1
    THICKNESS = 3
    #interesting test images
    #215
    #141
    #2
    #96
    #250
    #5
    
    TEST_IMAGE_NAME = 'test_data/video3/out223.png'

    print 'loading image: ', TEST_IMAGE_NAME
    image = cv2.imread(TEST_IMAGE_NAME, 1)

    print 'getting the field mask...'
    field_extractor = StupidFieldExtractor()
    field_mask = field_extractor.get_field_mask(image)
    
    print 'generating contours...'
    contourGenerator = EdgesContourGenerator()
    base_contours = contourGenerator.generate_contours(image, mask=field_mask)#[15:16]
    base_contours_with_children = [([base_contour] + base_contour.get_child_contours()) for base_contour in base_contours]
    all_contours = list(itertools.chain(*(base_contour_with_children for base_contour_with_children in base_contours_with_children)))
    #all_contours = base_contours + list(itertools.chain(*(base_contour.get_child_contours() for base_contour in base_contours)))
    
    line_detector = LineDetector(MIN_TANGENT_TO_LINE_ARC_LENGTH, MAX_TANGENT_TO_LINE_GEOMETRIC_RESIDUE, THETA_BIN_WIDTH, RADIUS_BIN_WIDTH,
                                 MAX_THETA_DEVIATION, MAX_RADIUS_DEVIATION, MIN_ARC_LENGTH, MAX_AVERAGE_ALGEBRAIC_RESIDUE)

    ellipse_detector = EllipseDetector(MIN_TANGENT_TO_LINE_ARC_LENGTH, MAX_TANGENT_TO_LINE_GEOMETRIC_RESIDUE,
                                       MIN_TANGENT_TO_ELLIPSE_ARC_LENGTH, MAX_TANGENT_TO_ELLIPSE_GEOMETRIC_RESIDUE, MIN_TANGENT_TO_ELLIPSE_AREA,
                                       RANSAC_MIN_POINTS_TO_FIT, RANSAC_NUM_ITERATIONS, RANSAC_DISTANCE_TO_SHAPE_THRESHOLD, RANSAC_MIN_PERCENTAGE_IN_SHAPE,
                                       THETA_BIN_WIDTH, RADIUS_BIN_WIDTH, MAX_TANGENT_LINES_PER_BUCKET,
                                       CENTER_BIN_WIDTH, MAX_TRIPLE_INTERSECTION_STD, MAX_CENTER_BIN_STD,
                                       MIN_POINTS_ON_ELLIPSE, MAX_ELLIPSE_RESIDUE)

    image_with_contours = image.copy()
    for contour in all_contours:
        cv_contour = contour.get_cv_contour()
        cv2.drawContours(image_with_contours, [cv_contour], -1, (255, 255, 255), 1)

    cv2.imshow('image with contours', image_with_contours)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    print 'looking for lines in the contour...'
    lines = line_detector.get_lines(image, all_contours)
    lines_with_position = [LineWithPosition(line) for line in lines]
    
    highlighted_shape_generator = HighlightedShapeGenerator()
    highlighted_lines_and_contour_masks = [highlighted_shape_generator.generate_highlighted_shape(l, THICKNESS, all_contours) for l in lines_with_position]

    for line in lines:
        draw_line(image_with_contours, line)

    for highlighted_line, contour_masks in highlighted_lines_and_contour_masks:
        draw_interval_mask(image_with_contours, highlighted_line)

    cv2.imshow('the intervals on the lines', image_with_contours)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    print 'looking for ellipses in the remaining lines...'
    #the contour mask of each contour is obtained as the intersection over the complement of the contour mask of each of the lines
    intersection_of_contour_masks = [(1 - contour_mask_on_line) for contour_mask_on_line in highlighted_lines_and_contour_masks[0][1]]
    for highlighted_line, contour_masks_on_line in highlighted_lines_and_contour_masks[1:]:
        for i, contour_mask in enumerate(contour_masks_on_line):
            intersection_of_contour_masks[i] *= (1 - contour_mask)

    contour_masks = intersection_of_contour_masks

    print 'the contour masks are:'
    print contour_masks
    
    image_with_contours = image.copy()
    contour_points = []
    for contour, contour_mask in zip(all_contours, contour_masks):
        #print 'contour mask: ', contour_mask
        for i, point in enumerate(contour.get_cv_contour().reshape((-1,2))):
            color = np.array([255,0,0]) if contour_mask[i] else np.array([0,0,255])
            image_with_contours[point[1],point[0]] = color

            if contour_mask[i]:
                contour_points.append(point)
            
    cv2.imshow('image with remaning points on the contours', image_with_contours)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    # #HACK!!!
    # contour_points = np.vstack(contour_points)
    # contour_points_mask = (contour_points[:,0] > 450) * (contour_points[:,1] > 150)
    # contour_points = contour_points[contour_points_mask]
    # #contour_points = np.vstack([contour.get_cv_contour().reshape((-1,2))[contour_mask>0] for contour,contour_mask in zip(contours, contour_masks)])
    # image[contour_points[:,1], contour_points[:,0]] = np.array([255,0,0])
    # cv2.imshow('image with remaining contour points', image)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    
    # ellipse_fitter = FitPointsToEllipseStable()
    # fitted_ellipse, total_algebraic_residue = ellipse_fitter.fit_points(contour_points)
    # print 'geometric residue: ', fitted_ellipse.get_geometric_residue(contour_points)
    # fitted_ellipse.compute_geometric_parameters()
    # fitted_center, axis_lengths, rotation = fitted_ellipse.get_geometric_parameters(in_degrees=True)

    # fig = plt.figure()
    # ax = fig.add_subplot('111')
    # ax.imshow(image)
    # ax.plot(contour_points[:,0], contour_points[:,1], 'bo')
    # ell = EllipsePlotter(xy = fitted_center, width = 2*axis_lengths[0], height= 2*axis_lengths[1], angle=rotation)
    # ax.add_artist(ell)
    
    # ell.set_clip_box(ax.bbox)
    # ell.set_alpha(0.8)
    # ell.set_facecolor((1,0,0))
    
    # ax.set_xlim(-10, image.shape[1] + 10)
    # ax.set_ylim(image.shape[0] + 10, -10)
    
    # plt.show()
           
    # display_ellipses(image_with_contours, [fitted_ellipse])

    #we now detect ellipses on each of the base contours seperately
    contour_index = 0
    ellipses = []
    for i, base_contour_with_children in enumerate(base_contours_with_children):
        print 'processing base contour: ', i
        print 'number of contours: ', len(base_contour_with_children)
        
        base_contour_with_children_masks = contour_masks[contour_index : contour_index + len(base_contour_with_children)]

        print 'number of contour masks: ', len(base_contour_with_children_masks)
        
        contour_index += len(base_contour_with_children)

        #draw the points and the masks
        on_line_color = np.array([0,0,255])
        not_on_line_color = np.array([255,0,0])
        image_with_points = image.copy()
        for contour, contour_mask in zip(base_contour_with_children, base_contour_with_children_masks):
            points = contour.get_cv_contour().reshape((-1,2))
            print 'shape of contour mask: ', contour_mask.shape
            print 'shape of contour: ', points.shape
            
            points_on_line = points[(contour_mask == 0)]
            points_not_on_line = points[contour_mask > 0]
            
            image_with_points[points_on_line[:,1], points_on_line[:,0]] = on_line_color
            image_with_points[points_not_on_line[:,1], points_not_on_line[:,0]] = not_on_line_color

        cv2.imshow('image with remaining contour points', image_with_points)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
    
        ellipses += ellipse_detector.get_ellipses(image, base_contour_with_children, base_contour_with_children_masks)
        
    display_ellipses(image, ellipses)
    
if __name__ == '__main__':
    #basic_test()
    test_on_soccer_image()
