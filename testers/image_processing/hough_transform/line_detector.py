import itertools
import numpy as np
import cv2
import matplotlib.pyplot as plt

from ....source.data_structures.contour import Contour
from ....source.image_processing.hough_transform.line_detector import LineDetector

def display_lines(image, lines):
    for line in lines:
        if np.abs(line._b) > 10**(-2):
            x = np.array([0, image.shape[1]])
            y = (-line._a / line._b)*x - (line._c / line._b)

        else:
            y = np.array([0, image.shape[0]])
            x = -(line._b / line._a)*y - (line._c / line._a)
        
        points = np.int32(np.vstack([x,y]).transpose().reshape((-1, 2)))

        cv2.line(image, tuple(points[0].tolist()), tuple(points[1].tolist()), (0, 255, 0), thickness=2)

    cv2.imshow('lines', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def draw_contours(ax, cv_contours):
    for cv_contour in cv_contours:
        ax.plot(cv_contour[:,0,0], cv_contour[:,0,1])

    return

def display_center_points(image, center_points):
    radius = 10
    color = (255,0,0)
    thickness = -1
    for point in center_points:
        cv2.circle(image, tuple(np.int32(point).tolist()), radius, color, thickness=thickness)

    cv2.imshow('center points', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def generate_basic_contour():
    '''
    We generate a single line:
    (50,100)----->------------>--(450,100)
    (50,100+h)----------<--------(450,100+h)
    '''
    
    h=20
    points_in_top_row = 10
    points_in_bottom_row = 10
    
    x_coords_top = np.linspace(50, 450, points_in_top_row)
    coords_top = np.vstack([x_coords_top, 100*np.ones(len(x_coords_top))]).transpose()

    x_coords_bottom = np.linspace(50, 450, points_in_bottom_row)
    coords_bottom = np.vstack([x_coords_bottom, (100 + h)*np.ones(len(x_coords_bottom))]).transpose()
    coords_bottom = coords_bottom[::-1]
    
    contour = np.vstack([coords_top, coords_bottom]).reshape((-1,1,2))

    #add noise
    contour += 0.1*np.random.randn(*contour.shape)

    point_mask = np.ones(len(contour), np.bool)
    #point_mask[len(coords_top):] = 1
    
    return np.int32(contour), point_mask

def basic_test():
    MIN_TANGENT_ARC_LENGTH = 10 #20
    MAX_TANGENT_ALGEBRAIC_RESIDUE = 1 #1.5
    THETA_BIN_WIDTH = 2
    RADIUS_BIN_WIDTH = 5
    MAX_THETA_DEVIATION = 3
    MAX_RADIUS_DEVIATION = 4
    MIN_ARC_LENGTH = 100
    MAX_AVERAGE_ALGEBRAIC_RESIDUE = 10
    
    image = np.zeros((500, 500, 3))
    cv_contour, point_mask = generate_basic_contour()
    contour = Contour(cv_contour, image)
    
    image_with_contour = image.copy()
    radius = 3
    color = (255,0,0)
    masked_color = (50,50,50)
    thickness = -1

    print cv_contour.shape
    
    cv2.drawContours(image_with_contour, [cv_contour], -1, (255,0,0), 1)
    #for i,point in enumerate(cv_contour):
    #    if point_mask[i] == 1:
    #        cv2.circle(image_with_contour, tuple(np.int32(point[0]).tolist()), radius, color, thickness=thickness)
    #    else:
    #        cv2.circle(image_with_contour, tuple(np.int32(point[0]).tolist()), radius, masked_color, thickness=thickness)
            
    print 'we are starting with the following contour: '
    cv2.imshow('image with contours and points', image_with_contour)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    line_detector = LineDetector(MIN_TANGENT_ARC_LENGTH, MAX_TANGENT_ALGEBRAIC_RESIDUE, THETA_BIN_WIDTH, RADIUS_BIN_WIDTH, \
                                 MAX_THETA_DEVIATION, MAX_RADIUS_DEVIATION, MIN_ARC_LENGTH, MAX_AVERAGE_ALGEBRAIC_RESIDUE)
    
    print 'looking for lines in the contour...'
    lines = line_detector.get_lines(image, [contour])

    display_lines(image_with_contour, lines)
    
    return

def test_on_soccer_image():
    from ....source.image_processing.contour_generator.edges_contour_generator import EdgesContourGenerator
    from ....source.image_processing.field_extractor.stupid_field_extractor import StupidFieldExtractor

    MIN_TANGENT_ARC_LENGTH = 20
    MAX_TANGENT_ALGEBRAIC_RESIDUE = 1.5
    THETA_BIN_WIDTH = 2
    RADIUS_BIN_WIDTH = 5
    MAX_THETA_DEVIATION = 0.7
    MAX_RADIUS_DEVIATION = 5
    MIN_ARC_LENGTH = 300
    MAX_AVERAGE_ALGEBRAIC_RESIDUE = 10

    #interesting test images
    #215
    #141
    #2
    #96

    TEST_IMAGE_NAME = 'test_data/video7/image_15.png'

    print 'loading image: ', TEST_IMAGE_NAME
    image = cv2.imread(TEST_IMAGE_NAME, 1)

    print 'getting the field mask...'
    field_extractor = StupidFieldExtractor()
    field_mask = field_extractor.get_field_mask(image)
    
    print 'generating contours...'
    contourGenerator = EdgesContourGenerator()
    contours = contourGenerator.generate_contours(image, mask=field_mask)
    contours += itertools.chain(*(contour.get_child_contours() for contour in contours))

    line_detector = LineDetector(MIN_TANGENT_ARC_LENGTH, MAX_TANGENT_ALGEBRAIC_RESIDUE, THETA_BIN_WIDTH, RADIUS_BIN_WIDTH, \
                                 MAX_THETA_DEVIATION, MAX_RADIUS_DEVIATION, MIN_ARC_LENGTH, MAX_AVERAGE_ALGEBRAIC_RESIDUE)

    image_with_contours = image.copy()
    for contour in contours:
        cv_contour = contour.get_cv_contour()
        cv2.drawContours(image, [cv_contour], -1, (255, 255, 255), 1)

    cv2.imshow('image with contour and points', image_with_contours)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    print 'looking for lines in the contour...'
    lines = line_detector.get_lines(image, contours)
    disgarded_tangent_lines = line_detector.get_disgarded_tangent_lines()

    num_disgarded = sum(len(wtl.tangent_lines) for wtl in disgarded_tangent_lines.values())
    print 'there were %d disgarded tangent lines distributed among %d bins' % (num_disgarded, len(disgarded_tangent_lines))

    plt.bar(range(len(disgarded_tangent_lines)), [len(wtl.tangent_lines) for wtl in disgarded_tangent_lines.values()])
    plt.show()
    
    display_lines(image_with_contours, lines)

    #image_with_center_points = image.copy()
    #center_points = line_detector.get_tangent_line_centers()
    #display_center_points(image_with_center_points, center_points)
    
if __name__ == '__main__':
    #basic_test()
    test_on_soccer_image()
