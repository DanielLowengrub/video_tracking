import cv2
import numpy as np
from ....source.auxillary.planar_geometry import apply_homography_to_line
from ...auxillary import primitive_drawing
from ....source.auxillary import planar_geometry

class CorrespondenceHomographyVisualization(object):
    '''
    This class is used to debug the correspondence homography calculator.

    Let I1 and I2 be images, S1, S2 a collection of shapes on each one, and H a homography between them. 
    This class draws the shapes S1 on the image S2 using the homography matrix.

    It also has an option to compute the homography of a point grid from I1 to I2 and draw that as well.
    '''

    SOURCE_SHAPE_COLOR = (255,0,0)
    TARGET_SHAPE_COLOR = (0,255,0)

    POINT_RADIUS = 3
    POINT_COLOR = (0,0,255)
    
    @classmethod
    def get_visualization(cls, target_image, homography_matrix, shape_correspondences, source_points=None):
        '''
        target_image - a numpy array with shape (n,m,3) and type np.uint8
        homography_matrix - a numpy array with shape (3,3)
        shape_correspondences - a list of tuples (source shape, target shape)
        source_points - a numpy array with shape (num pts, 2)

        Return an image with shape (n,m)

        We use the homography matrix to transform the source shapes to the target image.
        We then draw the transformed source shapes and target shapes on the image.

        If there are source points, we transform them as well and draw them.
        '''
        target_image = target_image.copy()
        
        target_shapes = [sc[1] for sc in shape_correspondences]
        transformed_source_shapes = map(lambda x: cls.apply_homography_to_shape(homography_matrix, x[0]), shape_correspondences)

        transformed_source_points = []
        if source_points is not None:
            transformed_source_points = cv2.perspectiveTransform(source_points.reshape((-1,1,2)), homography_matrix).reshape((-1,2))

        # print 'transformed shapes:'
        # print transformed_source_shapes
        # print ', '.join(map(str, transformed_source_shapes))
        
        #draw the transformed shapes on the image
        map(lambda x: cls.draw_shape(target_image, cls.TARGET_SHAPE_COLOR, x), target_shapes)
        map(lambda x: cls.draw_shape(target_image, cls.SOURCE_SHAPE_COLOR, x), transformed_source_shapes)

        #and draw the points
        for point in transformed_source_points:
            cv2.circle(target_image, tuple(np.int32(point).tolist()), cls.POINT_RADIUS, cls.POINT_COLOR, thickness=-1)

        return target_image
        
    @classmethod
    def draw_shape(cls, image, color, shape):
        if shape.is_line():
            primitive_drawing.draw_line(image, shape, color)

        elif shape.is_ellipse():
            primitive_drawing.draw_ellipse(image, shape, color)

        return

    @classmethod
    def apply_homography_to_shape(cls, H, shape):
        if shape.is_line():
            return planar_geometry.apply_homography_to_line(H, shape)

        elif shape.is_ellipse():
            return planar_geometry.apply_homography_to_ellipse(H, shape)

        return
        
