import cv2
import numpy as np

##########################################################################
# This is a list of function that we use to visualize homography detection
##########################################################################

def pad_image_bottom(image, padding):
    '''
    image - a numpy array with shape (n,m) or (n,m,3) and type np.uint8.

    We add padding amount of black pixels to the bottom of the image.
    '''

    padding_shape = [padding] + list(image.shape[1:])
    padding = np.zeros(padding_shape, np.uint8)
    padded_image = np.vstack([image, padding])
    
    return padded_image

def draw_images_side_by_side(img1, img2, title):
    '''
    Draw a pair of images side by side
    '''

    #if they do not both have the same height, add some black pixels to the shorter one
    height = max(img1.shape[0], img2.shape[0])

    if img1.shape[0] < height:
        img1 = pad_image_bottom(img1, height - img1.shape[0])
    if img2.shape[0] < height:
        img2 = pad_image_bottom(img2, height - img2.shape[0])

    
    out_img = np.hstack((img1, img2))
    cv2.imshow(title, out_img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def draw_keypoints_side_by_side(img1, kp1, img2, kp2):
    '''
    Draw a pair of images side by side, together with their keypoints.
    The keyoints are the output of the ORB feature detector.
    '''
    
    #first draw the keypoints on the images
    out1 = img1.copy()
    out2 = img2.copy()
    
    for kp in kp1:
        x = kp.pt[0]
        y = kp.pt[1]
        cv2.circle(out1, (int(x),int(y)), 4, (255, 0, 0), 1)

    for kp in kp2:
        x = kp.pt[0]
        y = kp.pt[1]
        cv2.circle(out2, (int(x),int(y)), 4, (255, 0, 0), 1)

    draw_images_side_by_side(out1, out2, 'keypoints in images')

def draw_source_and_destination_points(source_image, source_points, destination_image, destination_points):
    '''
    source_image, destination_image - numpy arrays of shape (n,m,3) and type np.uint8
    Draw a the given points on each of the images and display them side by side.
    '''
    
    #first draw the points on the images
    source_output = source_image.copy()
    destination_output = destination_image.copy()
    
    for point in source_points:
        x = point[0][0]
        y = point[0][1]
        cv2.circle(source_output, (int(x),int(y)), 4, (255, 0, 0), 1)

    for point in destination_points:
        x = point[0][0]
        y = point[0][1]
        cv2.circle(destination_output, (int(x),int(y)), 4, (255, 0, 0), 1)

    draw_images_side_by_side(source_output, destination_output, 'source and destination points')
