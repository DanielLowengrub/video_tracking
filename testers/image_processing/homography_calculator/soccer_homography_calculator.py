import os
import cv2
import numpy as np
from ....source.image_processing.homography_calculator.soccer_homography_calculator import SoccerHomographyCalculator
from ....source.preprocessing_full.image_controller import ImageController
from ....source.preprocessing_full.annotation_controller.annotation_controller import AnnotationController
from ....source.preprocessing_full.highlighted_shapes_controller.highlighted_shapes_controller import HighlightedShapesController
from ....source.preprocessing_full.soccer_field_embedding_controller.soccer_field_embedding_controller import SoccerFieldEmbedding
from ....source.preprocessing_full.soccer_field_embedding_controller.soccer_field_embedding_visualization_controller import SoccerFieldEmbeddingVisualizationController
from ....source.auxillary.soccer_field_geometry import SoccerFieldGeometry
from ...auxillary import primitive_drawing, label_shapes, highlighted_shape_drawing

# OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
# IMAGE_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/images'

# # OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'
# # IMAGE_PARENT_DIRECTORY = '/home/daniel/soccer/images'

# #INPUT_DATA_NAME = 'images-8_15-9_31'
# INPUT_DATA_NAME = 'images-0_00-1_00'

# PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_full', 'data', INPUT_DATA_NAME)

# # interval = (162,483)
# # frame = 292
# # interval = (597,793)
# # frame = 36
# # interval = (843,1013)
# # frame = 62
# interval = (1141,1498)
# frame = 204

# IMAGE_DIRECTORY              = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'image',
#                                             'interval_%d_%d' % interval, 'frame_%d' % frame)
# ANNOTATION_DIRECTORY         = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'annotation',
#                                             'interval_%d_%d' % interval, 'frame_%d' % frame)
# HIGHLIGHTED_SHAPES_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'highlighted_shapes',
#                                             'interval_%d_%d' % interval, 'frame_%d' % frame)

interval = (12356, 14353)
frame = 944
TEST_DIR = os.path.join('/Users/daniel/Dropbox/soccer/daniel_refactored/test_data/10_min_investigation',
                        'interval_%d_%d' % interval, 'frame_%d' % frame)

#parameters for the embedding generator
MIN_LINES_IN_CORRESPONDENCE = 5
HIGHLIGHTED_ELLIPSE_THRESHOLD = 200
LINE_SEGMENT_THRESHOLD = 10
ELLIPSE_SEGMENT_THRESHOLD = 2
PARALLEL_LINES_THRESHOLD = 1 - np.cos(np.pi / 10)
IWASAWA_THRESHOLD = 0.2
CORRESPONDENCE_THRESHOLD = 0.1

#the absolute image that we compute the homography to
ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
PIXELS_PER_YARD = 6
absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)

SHAPE_COLOR = (0,255,0)
GRID_COLOR = (0,0,255)
GRID_RADIUS = 3

def load_data_from_test_dir():
    image = cv2.imread(os.path.join(TEST_DIR, 'image.png'), 1)
    annotation = AnnotationController.load_data_from_frame_dir(TEST_DIR)
    highlighted_shapes = HighlightedShapesController.load_data_from_frame_dir(os.path.join(TEST_DIR,'highlighted_shapes'))

    return image, annotation, highlighted_shapes

def load_data():
    image_dir = os.readlink(IMAGE_DIRECTORY)
    annotation_dir = os.readlink(ANNOTATION_DIRECTORY)

    image = ImageController.load_data_from_frame_dir(image_dir)
    annotation = AnnotationController.load_data_from_frame_dir(annotation_dir)
    highlighted_shapes = HighlightedShapesController.load_data_from_frame_dir(HIGHLIGHTED_SHAPES_DIRECTORY)

    return image, annotation, highlighted_shapes

def load_absolute_soccer_field():
    '''
    Return a SoccerFieldGeometry object
    '''
    FIELD_WIDTH = absolute_image.shape[1]-1
    FIELD_LENGTH = absolute_image.shape[0]-1
    GRID_WIDTH = 20
    GRID_LENGTH = 14
    soccer_field = SoccerFieldGeometry(PIXELS_PER_YARD, FIELD_WIDTH, FIELD_LENGTH, GRID_WIDTH, GRID_LENGTH)
    
    return soccer_field

def test():
    image, annotation, highlighted_shapes = load_data_from_test_dir()
    soccer_field = load_absolute_soccer_field()

    highlighted_image = image.copy()
    for hs in highlighted_shapes:
        highlighted_shape_drawing.draw_highlighted_shape(highlighted_image, hs)

    cv2.imshow('highlighted shapes', highlighted_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    homography_calculator = SoccerHomographyCalculator(absolute_image, PIXELS_PER_YARD,
                                                       MIN_LINES_IN_CORRESPONDENCE,
                                                       LINE_SEGMENT_THRESHOLD, ELLIPSE_SEGMENT_THRESHOLD,
                                                       PARALLEL_LINES_THRESHOLD,
                                                       CORRESPONDENCE_THRESHOLD, IWASAWA_THRESHOLD)

    H, embedding_dict = homography_calculator.compute_homography(image, annotation, dict(enumerate(highlighted_shapes)))

    labelled_image = image.copy()
    labelled_shapes_with_position = dict((str(i),hs.get_shape_with_position()) for i,hs in enumerate(highlighted_shapes))
    label_shapes.label_shapes(labelled_image, labelled_shapes_with_position)
    cv2.imshow('labelled image', labelled_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    if H is None:
        print 'could not find an embedding.'
        return

    soccer_field_embedding = SoccerFieldEmbedding(H, embedding_dict)

    visualization_controller = SoccerFieldEmbeddingVisualizationController(0, soccer_field, SHAPE_COLOR,
                                                                           GRID_COLOR, GRID_RADIUS)
    
    visualization_controller._draw_soccer_field_embedding(image, highlighted_shapes, soccer_field_embedding)

    cv2.imshow('embedding', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    return
    
if __name__ == '__main__':
    test()
