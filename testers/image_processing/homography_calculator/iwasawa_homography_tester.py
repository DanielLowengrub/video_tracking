import numpy as np
import cv2

from ....source.auxillary.fit_points_to_shape.line import Line
from ....source.auxillary.fit_points_to_shape.ellipse import Ellipse
from ....source.image_processing.homography_calculator.iwasawa_homography_calculator import IwasawaHomographyCalculator
from ....source.auxillary.planar_geometry import rotation_matrix, apply_homography_to_line, apply_homography_to_ellipse
from ....source.auxillary.camera_matrix import CameraMatrix

def generate_test_homography():
    #position of the camera
    P = np.array([-5, 20,15])

    #the camera Z axis. I.e, where the camera is pointing.
    CZ = np.array([1, -5, -3])
    CZ = CZ / np.linalg.norm(CZ)

    print 'the camera is pointing in direction: ', CZ

    #now find the camera rotation matrix. We already have the Z axis
    #The new y axis will be the projection of the old z axis onto the plane defined by n
    z_axis = np.array([0,0,1])
    CY = z_axis - np.dot(z_axis, CZ)*CZ
    CY = CY / np.linalg.norm(CY)

    print 'the camera Y axis: ', CY
    #the camera X axis is orthogonal to CY and CZ
    CX = np.cross(CY, CZ)

    print 'the camera X axis: ', CX
    
    R = np.vstack([CX, CY, CZ]).transpose()

    print 'R:'
    print R
    
    f = 2
    
    camera_matrix = CameraMatrix.from_camera_position(P, R, f)

    return camera_matrix.get_homography_matrix()

def basic_test():
    
    y_axis = Line.from_point_and_direction(np.zeros(2), np.array([0,1]))
    unit_circle = Ellipse.from_geometric_parameters(np.zeros(2), np.ones(2), 0)
    origin = np.zeros(2)
    
    homography = generate_test_homography()
    
    print 'using the homography matrix:'
    print homography

    transformed_line = apply_homography_to_line(homography, y_axis)
    transformed_circle = apply_homography_to_ellipse(homography, unit_circle)
    transformed_point = cv2.perspectiveTransform(origin.reshape((-1,1,2)), homography).reshape(2)

    transformed_circle.compute_geometric_parameters()
    center, axes, angle = transformed_circle.get_geometric_parameters(in_degrees=True)
    print 'transformed circle: center = %s, axes = %s, angle = %s' % (center, axes, angle)
    print 'transformed line: ', str(transformed_line)
    print 'transformed point: ', transformed_point

    homography_calculator = IwasawaHomographyCalculator()
    homography_matrices = homography_calculator.compute_homography(transformed_ellipse, transformed_line, transformed_point)

    print 'we found %d homography matrices.' % len(homography_matrices)

    for i, H in enumerate(homography_matrices):
        print 'the %d-th homography matrix is:'
        print H
        
        transformed_line = apply_homography_to_line(HKAN, y_axis)
        transformed_circle = apply_homography_to_ellipse(HKAN, unit_circle)
        transformed_point = cv2.perspectiveTransform(origin.reshape((-1,1,2)), HKAN).reshape(2)

        transformed_circle.compute_geometric_parameters()
        center, axes, angle = transformed_circle.get_geometric_parameters(in_degrees=True)
        print ' '*4 + 'transformed circle: center = %s, axes = %s, angle = %s' % (center, axes, angle)
        print ' '*4 + 'transformed line: ', str(transformed_line)
        print ' '*4 + 'transformed point: ', transformed_point

if __name__ == '__main__':
    basic_test()
