import numpy as np
import cv2

from ....source.auxillary.fit_points_to_shape.line import Line
from ....source.auxillary.fit_points_to_shape.ellipse import Ellipse
from ....source.image_processing.homography_calculator.ellipse_homography_calculator import EllipseHomographyCalculator
from ....source.auxillary.planar_geometry import rotation_matrix, apply_homography_to_line, apply_homography_to_ellipse
from ....source.auxillary.camera_matrix import CameraMatrix

def generate_test_homography():
    #position of the camera
    P = np.array([-5, 20,15])

    #columns are the x,y,z coords of the camera
    #R = np.array([[1,  0,  0],
    #              [0, -1, -1],
    #              [0,  1,  -1]])
    ##normalize R
    #column_norms = np.linalg.norm(R, axis=0)
    #R = (R.transpose() / column_norms[:,np.newaxis]).transpose()

    #the camera Z axis. I.e, where the camera is pointing.
    CZ = np.array([1, -5, -3])
    CZ = CZ / np.linalg.norm(CZ)

    print 'the camera is pointing in direction: ', CZ

    #now find the camera rotation matrix. We already have the Z axis
    #The new y axis will be the projection of the old z axis onto the plane defined by n
    z_axis = np.array([0,0,1])
    CY = z_axis - np.dot(z_axis, CZ)*CZ
    CY = CY / np.linalg.norm(CY)

    print 'the camera Y axis: ', CY
    #the camera X axis is orthogonal to CY and CZ
    CX = np.cross(CY, CZ)

    print 'the camera X axis: ', CX
    
    R = np.vstack([CX, CY, CZ]).transpose()

    print 'R:'
    print R
    
    f = 2
    
    camera_matrix = CameraMatrix.from_camera_position(P, R, f)

    return camera_matrix.get_homography_matrix()

def identity_test():
    #the correspondence contains two pairs of lines, one vertical and one horizontal.
    y_axis = Line.from_point_and_direction(np.zeros(2), np.array([0,1]))
    unit_circle = Ellipse.from_geometric_parameters(np.zeros(2), np.ones(2), 0)
    origin = np.zeros(2)

    H = generate_test_homography()
    
    print 'using the homography matrix:'
    print H

    transformed_line = apply_homography_to_line(H, y_axis)
    transformed_circle = apply_homography_to_ellipse(H, unit_circle)
    transformed_point = cv2.perspectiveTransform(origin.reshape((-1,1,2)), H).reshape(2)

    transformed_circle.compute_geometric_parameters()
    center, axes, angle = transformed_circle.get_geometric_parameters(in_degrees=True)
    print 'transformed circle: center = %s, axes = %s, angle = %s' % (center, axes, angle)
    print 'transformed line: ', str(transformed_line)
    print 'transformed point: ', transformed_point
    
    homography_calculator = EllipseHomographyCalculator()

    homography1, homography2 = homography_calculator.compute_homography(transformed_circle, transformed_line, transformed_point)

    print 'actual homography:'
    print H
    print 'computed homography1:'
    print homography1
    print 'computed homography2:'
    print homography2

    print 'transforming shapes with computed homography...'
    transformed_line = apply_homography_to_line(homography1, y_axis)
    transformed_circle = apply_homography_to_ellipse(homography1, unit_circle)
    transformed_point = cv2.perspectiveTransform(origin.reshape((-1,1,2)), homography1).reshape(2)

    transformed_circle.compute_geometric_parameters()
    center, axes, angle = transformed_circle.get_geometric_parameters(in_degrees=True)
    print 'transformed circle: center = %s, axes = %s, angle = %s' % (center, axes, angle)
    print 'transformed line: ', str(transformed_line)
    print 'transformed point: ', transformed_point

def compute_K(theta):
    return np.array([[np.cos(theta), -np.sin(theta), 0],
                     [np.sin(theta), np.cos(theta),  0],
                     [0,             0,              1]])

def compute_A(r):
    a = 0.5 * r
    b = 0.5 * 1/r
    return np.array([[1,   0,    0],
                     [0,  a+b, -a+b],
                     [0, -a+b,  a+b]])

def compute_N(x):
    a = 0.5*(x**2)
    return np.array([[ 1, x,    x],
                     [-x, 1-a, -a],
                     [ x, a,  1+a]])
def iwasawa_test():
    PERMUTATION = np.array([[0, 1, 0],
                            [0, 0, 1],
                            [1, 0, 0]])
    EPSILON = 10**(-5)
    
    y_axis = Line.from_point_and_direction(np.zeros(2), np.array([0,1]))
    unit_circle = Ellipse.from_geometric_parameters(np.zeros(2), np.ones(2), 0)
    origin = np.zeros(2)
    
    homography = generate_test_homography()
    
    print 'using the homography matrix:'
    print homography

    print 'its inverse is:'
    print np.linalg.inv(homography)
    
    transformed_line = apply_homography_to_line(homography, y_axis)
    transformed_circle = apply_homography_to_ellipse(homography, unit_circle)
    x = cv2.perspectiveTransform(origin.reshape((-1,1,2)), homography).reshape(2)

    transformed_circle.compute_geometric_parameters()
    center, axes, angle = transformed_circle.get_geometric_parameters(in_degrees=True)
    print 'transformed circle: center = %s, axes = %s, angle = %s' % (center, axes, angle)
    print 'transformed line: ', str(transformed_line)
    print 'transformed point: ', x

    L = transformed_line.get_parametric_representation()
    C = transformed_circle.get_parametric_representation()
    x = np.append(x,1)
    
    #get a matrix H such that H^T * C * H = [[1,0,0],[0,1,0],[0,0,-1]]
    vals, O = np.linalg.eigh(C)
    
    #fix O so that O^T*diag(vals)*O = C
    O = O.transpose()

    #swap the first and last eigenvalus
    vals = vals[np.array([1,2,0])]
    #vals = vals[np.array([2,1,0])]
    O = np.dot(PERMUTATION, O)

    H = np.float64(np.dot(O.transpose(), np.diag(1 / np.sqrt(np.abs(vals)))))

    print 'H = '
    print H

    #print 'H^T*C*H = '
    #print np.dot(H.transpose(), np.dot(C, H))

    H_inv = np.linalg.inv(H)
    u = np.dot(H_inv, x)
    v = np.dot(H.transpose(), L)

    #print 'u = ', u
    #print 'v = ', v

    denom = v[0]**2 + v[1]**2 - v[2]**2
    #print 'vx^2 + vy^2 - vz^2 = ', denom

    mu_squared = 1 / denom

    mu_1 = np.sqrt(1/np.abs(denom))
    mu_2 = -mu_1

    mu_cos_theta_pairs = []

    #print '-----------------------'
    #print 'computing cos(theta)...'
    #print '-----------------------'
    for mu in (mu_1, mu_2):
        #print ' '*4 + 'finding the angle between %s and %s...' % (str(np.array([mu*v[2], 1])), str(mu*np.array([v[1],v[0]])))
        cos_theta = (mu_squared*v[1]*v[2] + mu*v[0]) / (1 + mu_squared*(v[2]**2))
        mu_cos_theta_pairs.append((mu, cos_theta))

    #print 'mu cos_theta pairs: ', mu_cos_theta_pairs

    mu_theta_pairs = []

    for mu,cos_theta in mu_cos_theta_pairs:
        #print 'computing the angle for mu=%f, cos_theta=%f...' % (mu, cos_theta)
        theta = np.arccos(cos_theta)
        # if mu_squared*v[1]*v[2] - mu*v[0] < EPSILON:
        #     print 'the angle is convex, adding arccos(cos(theta))'
        #     mu_theta_pairs.append((mu,theta))

        # elif mu_squared*v[1]*v[2] - mu*v[0] > EPSILON:
        #     print 'the angle is concave, adding -arccos(cos(theta))'
        #     mu_theta_pairs.append((mu, -theta))

        # else:
        #print 'could not determine if the angle is convex of concave'
        mu_theta_pairs += [(mu, theta), (mu, -theta)]


    #print 'the mu and theta pairs are: ', mu_theta_pairs

    r_mu_theta_pairs = []

    #print '----------------------'
    #print 'computing r...'
    #print '----------------------'
    for mu,theta in mu_theta_pairs:
        print '* computing r and x for mu=%f, theta=%f...' % (mu, theta)
        #print ' '*4 + '-mu*vz = ', -mu * v[2]
        R = np.array([[np.cos(theta), np.sin(theta)], [-np.sin(theta), np.cos(theta)]])

        #print ' '*4 + 'checking theta...'
        #rx = -mu*v[2]
        #print ' '*4 + 'K^-T*A^-T*N^-T*(1,0,0) = ', np.array([np.sin(theta)*rx + np.cos(theta), -np.cos(theta)*rx + np.sin(theta), -rx])
        #print ' '*4 + 'mu*v = ', mu*v

        w = np.append(np.dot(R, u[:2]), u[2])

        #print ' '*4 + 'w = ', w
        #print ' '*4 + '-mu*vz(wy+wz) = ', -mu*v[2]*(w[1]+w[2])
        #print ' '*4 + 'wx = ', w[0]

        if np.abs(w[0] - (-mu*v[2]*(w[1]+w[2]))) > EPSILON:
            #print ' '*4 + 'wx =/= -mu*vz(wy+wz). skipping...'
            continue
        
        numerator = (w[1]+w[2])*(1 - mu_squared*(v[2]**2)) - 2*w[1]
        denominator = w[1] + w[2]

        #print ' '*4 + 'numerator: ', numerator
        #print ' '*4 + 'denominator: ', denominator

        r_squared = numerator / denominator

        #print ' '*4 + 'r_squared = ', r_squared
        if r_squared < 0:
            #print ' '*4 + 'r^2 is negative, continuing...'
            continue
        
        r_mu_theta_pairs.append((np.sqrt(r_squared), mu, theta))
        r_mu_theta_pairs.append((-np.sqrt(r_squared), mu, theta))

    #print 'r mu theta pairs: ', r_mu_theta_pairs

    print '--------------------'
    print 'computing homographies...'
    print '--------------------'
    
    for r,mu,theta in r_mu_theta_pairs:
        print '* constructing homography from r=%f, mu=%f, theta=%f...' % (r,mu,theta)

        x = -mu*v[2] / r

        #print ' '*4 + 'x = ', x

        #print ' '*4 + 'checking r and x'
        #print ' '*4 + 'r*x = ', r*x
        #print ' '*4 + 'it should be: ', -mu*v[2]

        K = compute_K(theta)
        A = compute_A(r)
        N = compute_N(x)
        KAN = np.dot(K, np.dot(A, N))

        # print ' '*4 + 'K = '
        # print K
        # print ' '*4 + 'A = '
        # print A
        # print ' '*4 + 'N = '
        # print N

        # print ' '*4 + 'KAN = '
        # print KAN

        #print ' '*4 + 'checking KAN:'
        #print ' '*4 + 'K^-T*A^-T*N^-T*(1,0,0) = ', np.dot(np.dot(np.linalg.inv(K).transpose(), np.dot(np.linalg.inv(A).transpose(), np.linalg.inv(N).transpose())), np.array([1,0,0]))
        #print ' '*4 + 'K^-T*A^-T*N^-T*(1,0,0) = ', np.dot(np.linalg.inv(KAN).transpose(), np.array([1,0,0]))
        #print ' '*4 + ' it should be: ', np.array([np.sin(theta)*r*x + np.cos(theta), -np.cos(theta)*r*x + np.sin(theta), -r*x])
        #print ' '*4 + 'and also equal to mu*v = ', mu*v
        
        HKAN = np.dot(H, KAN)

        print ' '*4 + 'we found the homography:'
        print HKAN

        print ' '*4 + 'testing the homography...'
        
        transformed_line = apply_homography_to_line(HKAN, y_axis)
        transformed_circle = apply_homography_to_ellipse(HKAN, unit_circle)
        transformed_point = cv2.perspectiveTransform(origin.reshape((-1,1,2)), HKAN).reshape(2)

        transformed_circle.compute_geometric_parameters()
        center, axes, angle = transformed_circle.get_geometric_parameters(in_degrees=True)
        print ' '*4 + 'transformed circle: center = %s, axes = %s, angle = %s' % (center, axes, angle)
        print ' '*4 + 'transformed line: ', str(transformed_line)
        print ' '*4 + 'transformed point: ', transformed_point

if __name__ == '__main__':
    #identity_test()
    iwasawa_test()
