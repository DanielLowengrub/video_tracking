import numpy as np
import cv2

from ....source.auxillary.fit_points_to_shape.line import Line
from ....source.auxillary.fit_points_to_shape.ellipse import Ellipse
from ....source.image_processing.homography_calculator.iwasawa_homography_calculator import IwasawaHomographyCalculator
from ....source.auxillary import planar_geometry
from ...auxillary import primitive_drawing
from ....source.auxillary.camera_matrix import CameraMatrix
from ....source.auxillary.contour_shape_overlap.highlighted_shape import HighlightedShape
from ...auxillary.soccer_field_geometry import SoccerFieldGeometry
from .correspondence_homography_visualization import CorrespondenceHomographyVisualization

def generate_test_homography():
    #position of the camera
    P = np.array([-5, 20,15])

    #the camera Z axis. I.e, where the camera is pointing.
    CZ = np.array([1, -5, -3])
    CZ = CZ / np.linalg.norm(CZ)

    print 'the camera is pointing in direction: ', CZ

    #now find the camera rotation matrix. We already have the Z axis
    #The new y axis will be the projection of the old z axis onto the plane defined by n
    z_axis = np.array([0,0,1])
    CY = z_axis - np.dot(z_axis, CZ)*CZ
    CY = CY / np.linalg.norm(CY)

    print 'the camera Y axis: ', CY
    #the camera X axis is orthogonal to CY and CZ
    CX = np.cross(CY, CZ)

    print 'the camera X axis: ', CX
    
    R = np.vstack([CX, CY, CZ]).transpose()

    print 'R:'
    print R
    
    f = 2
    
    camera_matrix = CameraMatrix.from_camera_position(P, R, f)

    return camera_matrix.get_homography_matrix()

def basic_test():
    
    y_axis = Line.from_point_and_direction(np.zeros(2), np.array([0,1]))
    unit_circle = Ellipse.from_geometric_parameters(np.zeros(2), np.ones(2), 0)
    origin = np.zeros(2)
    
    homography = generate_test_homography()
    
    print 'using the homography matrix:'
    print homography

    transformed_line = planar_geometry.apply_homography_to_line(homography, y_axis)
    transformed_circle = planar_geometry.apply_homography_to_ellipse(homography, unit_circle)
    transformed_point = cv2.perspectiveTransform(origin.reshape((-1,1,2)), homography).reshape(2)

    transformed_circle.compute_geometric_parameters()
    center, axes, angle = transformed_circle.get_geometric_parameters(in_degrees=True)
    print 'transformed circle: center = %s, axes = %s, angle = %s' % (center, axes, angle)
    print 'transformed line: ', str(transformed_line)
    print 'transformed point: ', transformed_point

    homography_calculator = IwasawaHomographyCalculator()
    homography_matrices = homography_calculator.compute_homography(transformed_circle, transformed_line, transformed_point)

    print 'we found %d homography matrices.' % len(homography_matrices)

    for i, H in enumerate(homography_matrices):
        print '-------------------------------'
        print 'the %d-th homography matrix is:' % i
        print H
        
        transformed_line = planar_geometry.apply_homography_to_line(H, y_axis)
        transformed_circle = planar_geometry.apply_homography_to_ellipse(H, unit_circle)
        transformed_point = cv2.perspectiveTransform(origin.reshape((-1,1,2)), H).reshape(2)

        transformed_circle.compute_geometric_parameters()
        center, axes, angle = transformed_circle.get_geometric_parameters(in_degrees=True)
        print ' '*4 + 'transformed circle: center = %s, axes = %s, angle = %s' % (center, axes, angle)
        print ' '*4 + 'transformed line: ', str(transformed_line)
        print ' '*4 + 'transformed point: ', transformed_point

def basic_correspondence_test():
    source_image = np.zeros((300,600,3), np.uint8)
    target_image = np.zeros((300,600,3), np.uint8)
    y_axis = Line.from_point_and_direction(np.zeros(2), np.array([0,1]))
    unit_circle = Ellipse.from_geometric_parameters(np.zeros(2), np.ones(2), 0)
    source_lines = [Line(1, 0, 5), Line(0,1,5)]
    
    homography = generate_test_homography()
    
    print 'using the homography matrix:'
    print homography

    transformed_y_axis = planar_geometry.apply_homography_to_line(homography, y_axis)
    transformed_circle = planar_geometry.apply_homography_to_ellipse(homography, unit_circle)
    target_lines = [planar_geometry.apply_homography_to_line(homography, l) for l in source_lines]

    #perturb the target lines
    target_lines = [Line.from_parametric_representation(l.get_parametric_representation() + 0.01*np.random.normal(size=(3,))) for l in target_lines]
    
    transformed_circle.compute_geometric_parameters()
    center, axes, angle = transformed_circle.get_geometric_parameters(in_degrees=True)
    print 'transformed circle: center = %s, axes = %s, angle = %s' % (center, axes, angle)
    print 'transformed y axis: ', str(transformed_y_axis)
    print '\n'.join('transformed line %d: %s' % (i, str(l)) for i,l in enumerate(target_lines))

    ellipse_correspondence = (unit_circle, transformed_circle)
    line_through_center_correspondence = (y_axis, transformed_y_axis)
    line_correspondences = zip(source_lines, target_lines)

    homography_calculator = IwasawaHomographyCalculator()
    homography_matrices, errors = homography_calculator.compute_homography(source_image, target_image,
                                                                           ellipse_correspondence, line_through_center_correspondence, line_correspondences)

    print 'we found %d homography matrices.' % len(homography_matrices)

    center, axes, angle = transformed_circle.get_geometric_parameters(in_degrees=True)
    print 'transformed circle: center = %s, axes = %s, angle = %s' % (center, axes, angle)
    print 'transformed y axis: ', str(transformed_y_axis)
    print '\n'.join('transformed line %d: %s' % (i, str(l)) for i,l in enumerate(target_lines))

    for i, H in enumerate(homography_matrices):
        print '-------------------------------'
        print 'the %d-th homography matrix is:' % i
        print H
        print 'with errors: ', errors[i]
        
        transformed_y_axis = planar_geometry.apply_homography_to_line(H, y_axis)
        transformed_circle = planar_geometry.apply_homography_to_ellipse(H, unit_circle)
        transformed_source_lines = [planar_geometry.apply_homography_to_line(H, l) for l in source_lines]

        transformed_circle.compute_geometric_parameters()
        center, axes, angle = transformed_circle.get_geometric_parameters(in_degrees=True)
        print ' '*4 + 'computed circle: center = %s, axes = %s, angle = %s' % (center, axes, angle)
        print ' '*4 + 'computed y axis: ', str(transformed_y_axis)
        print '\n'.join(' '*4 + 'computed line %d: %s' % (i, str(l)) for i,l in enumerate(transformed_source_lines))

def scaling_test():
    source_image = np.zeros((300,600,3), np.uint8)
    target_image = np.zeros((300,600,3), np.uint8)
    y_axis = Line.from_point_and_direction(np.zeros(2), np.array([0,1]))
    unit_circle = Ellipse.from_geometric_parameters(np.zeros(2), np.ones(2), 0)
    source_lines = [Line(1, 0, 5)]#, Line(0,1,5)]
    target_lines = [Line(1, 0, 6)]#, Line(0, 1, 4)]

    ellipse_correspondence = (unit_circle, unit_circle)
    line_through_center_correspondence = (y_axis, y_axis)
    line_correspondences = zip(source_lines, target_lines)

    homography_calculator = IwasawaHomographyCalculator()
    homography_matrices, errors = homography_calculator.compute_homography(source_image, target_image,
                                                                           ellipse_correspondence, line_through_center_correspondence, line_correspondences)

    print 'we found %d homography matrices.' % len(homography_matrices)
    print 'errors: ', errors
    for i,H in enumerate(homography_matrices):
        print 'evaluating homograpy %d.' % i
        print 'target lines:'
        print '\n'.join(map(str, target_lines))
        print 'transformed source lines:'
        print '\n'.join(map(str, [planar_geometry.apply_homography_to_line(H, l) for l in source_lines]))
    
def load_soccer_field_geometry():
    ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)

    SCALE = 6
    FIELD_WIDTH = absolute_image.shape[1]
    FIELD_LENGTH = absolute_image.shape[0]
    GRID_WIDTH = 20
    GRID_LENGTH = 14
    soccer_field = SoccerFieldGeometry(SCALE, FIELD_WIDTH, FIELD_LENGTH, GRID_WIDTH, GRID_LENGTH)

    return soccer_field

def compute_center_point(transformed_center_circle, transformed_center_line, line_correspondences):
    '''
    transformed_center_circle, transformed_center_line - the transformations of the center unit circle and the line (x=0)
    line_correspondences - a list of pairs (line, transformed line)

    Return the transformation of the center point of the circle.
    We do this by using the line correspondence to find the transformation of one of the points on (x=0). The intersection with the ellipse gives 2 more.
    We then use the cross ratio to find the center point
    '''

    center_line = Line(1,0,0)

    #first get the intersection points of the transformed center line with the transformed center circle
    transformed_ellipse_line_intersection_points = planar_geometry.line_ellipse_intersection(transformed_center_line, transformed_center_circle)
    #sort the intersection points from top to bottom
    a = transformed_ellipse_line_intersection_points[:,1].argsort()
    transformed_ellipse_line_intersection_points = transformed_ellipse_line_intersection_points[a]

    print '   transformed ellipse line intersection points:'
    print transformed_ellipse_line_intersection_points
    
    #add the ellipse line intersection points to the projective point correspondences
    ellipse_line_intersection_points = np.array([[0,-1],[0,1]])

    projective_point_correspondences = [(np.append(p1,1), np.append(p2,1)) for p1,p2 in zip(ellipse_line_intersection_points, transformed_ellipse_line_intersection_points)]
    
    #now create point correspondences from the line correspondences
    for source_line, transformed_line in line_correspondences:
        print '   getting intersection point from source line: ', source_line
        print '   and transformed line: ', transformed_line
        
        #the source point is the intersection of the source line and the x=0 axis
        source_point = planar_geometry.projective_line_line_intersection(center_line, source_line)
        transformed_point = planar_geometry.projective_line_line_intersection(transformed_center_line, transformed_line)

        print '   source point: ', source_point
        print '   transformed point: ', transformed_point
        projective_point_correspondences.append((source_point, transformed_point))

    #now use the point correspondences to get a homography
    H = planar_geometry.compute_homography_on_line(center_line, projective_point_correspondences)

    #use the homography to compute the image of the origin
    projective_transformed_center = np.dot(H, np.array([0,0,1]))

    return projective_transformed_center[:2] / projective_transformed_center[2]

        
    
    
def soccer_test():
    #We load shapes from a highlighted shape directory, and create a correspondence with the shapes in a SoccerFieldGeometry object
    #LINE_DIRECTORY_NAMES = ['output/tester/highlighted_shapes/saved_data/frames_50_269/image_0/highlighted_line_%d' % i for i in range(2)]
    #ELLIPSE_DIRECTORY_NAMES = ['output/tester/highlighted_shapes/saved_data/frames_50_269/image_0/highlighted_ellipse_%d' % 2]
    LINE_DIRECTORY_NAMES = ['output/tester/highlighted_shapes/saved_data/frames_1_19/image_7/highlighted_line_%d' % i for i in range(3)]
    ELLIPSE_DIRECTORY_NAMES = ['output/tester/highlighted_shapes/saved_data/frames_1_19/image_7/highlighted_ellipse_%d' % 3]
    
    IMAGE_NAME = 'test_data/video3/out8.png'
    ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)
    image = cv2.imread(IMAGE_NAME, 1)

    lines = []
    for line_directory_name in LINE_DIRECTORY_NAMES:
        print 'loading highlighted shape from: ', line_directory_name
        highlighted_shape = HighlightedShape.load_from_directory(line_directory_name, HighlightedShape.LINE)
        lines.append(highlighted_shape.get_shape())

        image_with_line = image.copy()
        primitive_drawing.draw_line(image_with_line, lines[-1], (0,255,0))
        cv2.imshow('the line', image_with_line)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    ellipses = []
    for ellipse_directory_name in ELLIPSE_DIRECTORY_NAMES:
        print 'loading highlighted shape from: ', ellipse_directory_name
        highlighted_shape = HighlightedShape.load_from_directory(ellipse_directory_name, HighlightedShape.ELLIPSE)
        ellipses.append(highlighted_shape.get_shape())

        image_with_ellipse = image.copy()
        primitive_drawing.draw_ellipse(image_with_ellipse, ellipses[-1], (0,255,0))

        cv2.imshow('the ellipse', image_with_ellipse)
        cv2.waitKey(0)
        cv2.destroyAllWindows()


    soccer_field = load_soccer_field_geometry()
    
    #for the moment, we hardcode shape correspondences
    transformed_center_circle = ellipses[0]
    center_circle = soccer_field.get_highlighted_shape(SoccerFieldGeometry.CENTER_CIRCLE).get_shape()
    center_circle.compute_geometric_parameters()
    center_circle_origin, axes, angle = center_circle.get_geometric_parameters()
    
    transformed_center_line = lines[0]
    center_line = soccer_field.get_highlighted_shape(SoccerFieldGeometry.CENTER_LINE).get_shape()
    
    transformed_bottom_sideline = lines[2]
    bottom_sideline = soccer_field.get_highlighted_shape((SoccerFieldGeometry.SIDELINE, SoccerFieldGeometry.BOTTOM)).get_shape()
    #bottom_sideline = soccer_field.get_highlighted_shape((SoccerFieldGeometry.PENALTY_AREA, SoccerFieldGeometry.BOTTOM)).get_shape()

    transformed_penalty_line = lines[1]
    penalty_line = soccer_field.get_highlighted_shape((SoccerFieldGeometry.PENALTY_AREA, SoccerFieldGeometry.RIGHT)).get_shape()
    #penalty_line = soccer_field.get_highlighted_shape((SoccerFieldGeometry.SIDELINE, SoccerFieldGeometry.RIGHT)).get_shape()
    
    # #since the iwasawa calculator need the center circle to be the unit circle, we first construct a homography M that takes the center circle
    # #on the soccer field to the unit circle.
    # #we then compute the homography H from this new coordinate system to the image, and the final homography matrix will be HM.
    # M = planar_geometry.compute_homography_to_unit_circle(center_circle)

    # #transform the source shapes by M
    # new_center_circle = planar_geometry.apply_homography_to_ellipse(M, center_circle)
    # new_center_line = planar_geometry.apply_homography_to_line(M, center_line)
    # #new_bottom_sideline = planar_geometry.apply_homography_to_line(M, bottom_sideline)
    # new_penalty_line = planar_geometry.apply_homography_to_line(M, penalty_line)
    # origin = np.array([0,0,1])

    # new_center_circle.compute_geometric_parameters()
    # center, axes, angle = new_center_circle.get_geometric_parameters(in_degrees=True)
    # print 'new center circle: center = %s, axes = %s, angle = %s' % (center, axes, angle)
    # print 'new center line: ', str(new_center_line)
    # #print 'new bottom sideline: ', str(new_bottom_sideline)
    # print 'new penalty line: ', str(new_penalty_line)
    
    # #we now use the intersection points of the new center line with the new center circle and bottom sideline to compute the transformed center point
    # #line_correspondences = [(new_bottom_sideline, transformed_bottom_sideline)]
    # line_correspondences = [(new_penalty_line, transformed_penalty_line)]
    # transformed_center_point = compute_center_point(transformed_center_circle, transformed_center_line, line_correspondences)
    # #transformed_center_point = np.array([147,137], np.float32)
    # transformed_center_point = transformed_center_line.get_closest_point_on_line(transformed_center_point)
    # print 'transformed center point: ', transformed_center_point
    # print 'distance from line: ', transformed_center_line.get_algebraic_distance_from_point(transformed_center_point)
    # image_with_center_point = image.copy()
    # primitive_drawing.draw_line(image_with_center_point, transformed_center_line, (0,255,0))
    # primitive_drawing.draw_point(image_with_center_point, transformed_center_point, (255,0,0))
    # cv2.imshow('the center of the ellipse', image_with_center_point)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    line_correspondences = [(penalty_line, transformed_penalty_line), (bottom_sideline, transformed_bottom_sideline)]
    #line_correspondences = [(penalty_line, transformed_penalty_line)]
    #line_correspondences = [(bottom_sideline, transformed_bottom_sideline)]
    
    ellipse_correspondence = (center_circle, transformed_center_circle)
    line_through_center_correspondence = (center_line, transformed_center_line)
    homography_calculator = IwasawaHomographyCalculator()
    #homographies = homography_calculator.compute_homography(transformed_center_circle, transformed_center_line, transformed_center_point)
    H, errors = homography_calculator.compute_homography(absolute_image, image, ellipse_correspondence, line_through_center_correspondence, line_correspondences)
    
    print 'homography:'
    print H

    print 'errors: ', errors

    H = H[0]
    
    transformed_center_circle.compute_geometric_parameters()
    center, axes, angle = transformed_center_circle.get_geometric_parameters(in_degrees=True)
    print 'target center circle: center = %s, axes = %s, angle = %s' % (center, axes, angle)
    print 'target center line: ', str(transformed_center_line)
    print 'target bottom sideline: ', str(transformed_bottom_sideline)
    print 'target penalty line: ', str(transformed_penalty_line)
    #print 'target center point: ', transformed_center_point
    
    #shape_correspondences = [(center_line, transformed_center_line), (bottom_sideline, transformed_bottom_sideline)]
    #shape_correspondences = [(center_line, transformed_center_line), (penalty_line, transformed_penalty_line)]
    shape_correspondences = [(center_line, transformed_center_line), (penalty_line, transformed_penalty_line), (bottom_sideline, transformed_bottom_sideline)]
    #for homography in homographies:
    #    H = np.dot(homography, M)
    #    print 'computed homography:'
    #    print H

    computed_center_circle = planar_geometry.apply_homography_to_ellipse(H, center_circle)
    computed_center_line = planar_geometry.apply_homography_to_line(H, center_line)
    computed_bottom_sideline = planar_geometry.apply_homography_to_line(H, bottom_sideline)
    computed_penalty_line = planar_geometry.apply_homography_to_line(H, penalty_line)
    computed_center_point = cv2.perspectiveTransform(center_circle_origin.reshape((-1,1,2)), H).reshape(2)
        
    computed_center_circle.compute_geometric_parameters()
    center, axes, angle = computed_center_circle.get_geometric_parameters(in_degrees=True)
    print 'computed center circle: center = %s, axes = %s, angle = %s' % (center, axes, angle)
    print 'computed center line: ', str(computed_center_line)
    print 'computed bottom sideline: ', str(computed_bottom_sideline)
    print 'computed penalty line: ', str(computed_penalty_line)
    #print 'computed center point: ', computed_center_point
        
    chv = CorrespondenceHomographyVisualization()
    big_image = np.zeros((image.shape[0]+100,image.shape[1],3), np.uint8)
    big_image[0:image.shape[0], 0:image.shape[1]] = image
    visualization = chv.get_visualization(big_image, H, shape_correspondences, soccer_field.get_grid_points())
    
    cv2.imshow('the transformed shapes', visualization)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    cv2.imwrite('output/distortion_visualization.png', visualization)
    
if __name__ == '__main__':
    #basic_test()
    #soccer_test()
    #basic_correspondence_test()
    scaling_test()
