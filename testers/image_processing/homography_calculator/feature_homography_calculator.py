import numpy as np
import cv2

from ...image_processing.contour_generator.edges_contour_generator import EdgesContourGenerator
from ....source.image_processing.homography_calculator.feature_homography_calculator import FeatureHomographyCalculator
#from ....source.image_processing.image_annotator.gmm_soccer_field_annotator import GMMSoccerFieldAnnotator
from ....source.image_processing.image_annotator.image_annotation import ImageAnnotation
from ....source.image_processing.image_annotator.soccer_annotation import SoccerAnnotation
from ....source.auxillary import planar_geometry, mask_operations
from .homography_visualization import draw_images_side_by_side

#######
# This is a tester for the file code/source/image_processing/homography_calculator/feature_hmography_calculator
#
# We load 2 images of a soccer field and compute the homography between them
######

#interesting test images
#215
#141
#2
#96

frame_index = 373

IMAGE_NAME_1 = '/Users/daniel/Documents/soccer/images/images-0_00-1_00/image_%d.png' % (frame_index)
IMAGE_NAME_2 = '/Users/daniel/Documents/soccer/images/images-0_00-1_00/image_%d.png' % (frame_index+1)

ANNOTATION_NAME_1 = '/Users/daniel/Documents/soccer/output/preprocessing_full/data/images-0_00-1_00/stage_0/annotation/interval_0_1498/frame_%d/annotation.npy' % (frame_index)
ANNOTATION_NAME_2 = '/Users/daniel/Documents/soccer/output/preprocessing_full/data/images-0_00-1_00/stage_0/annotation/interval_0_1498/frame_%d/annotation.npy' % (frame_index+1)

# FIELD_MASK_NAME_1 = 'output/scripts/preprocessing/data/video7/field_mask/mask_%d.npy' % (frame_index)
# FIELD_MASK_NAME_2 = 'output/scripts/preprocessing/data/video7/field_mask/mask_%d.npy' % (frame_index + 1)

X_SUB = 3
Y_SUB = 3

PLAYERS_KERNEL = np.ones((20,20), np.uint8)
SIDELINES_KERNEL = np.ones((10,10), np.uint8)
# def process_annotation(annotation):
#     players_kernel = np.ones((20,20), np.uint8)
#     sidelines_kernel = np.ones((10, 10), np.uint8)
#     x_subdivision = 3
#     y_subdivision = 3
    
#     new_annotation = annotation.copy()

#     #first dilate the players
#     players_cv_mask = np.uint8(GMMSoccerFieldAnnotator.get_players_mask(annotation))
#     players_cv_mask = cv2.dilate(players_cv_mask, players_kernel, iterations=1)

#     #then set all of the player pixels to 0
#     new_annotation[players_cv_mask == 1] = 0

#     #now dilate the sidelines
#     sidelines_cv_mask = np.uint8(GMMSoccerFieldAnnotator.get_sidelines_mask(new_annotation))
#     sidelines_cv_mask = cv2.dilate(sidelines_cv_mask, sidelines_kernel, iterations=1)
#     new_annotation[sidelines_cv_mask == 1] = GMMSoccerFieldAnnotator.SIDELINES

#     #now subdivide the annotation
#     new_annotation = GMMSoccerFieldAnnotator.subdivide_annotation(new_annotation, x_subdivision, y_subdivision)
    
#     return new_annotation

def load_image_contours_and_annotation(image_file, annotation_file):
    print 'loading image from: ', image_file
    image = cv2.imread(image_file, 1)
    #field_mask = np.load(field_mask_file)
    annotation = SoccerAnnotation(np.load(annotation_file))
    field_mask = annotation.get_soccer_field_mask()
    annotation_array = annotation.get_annotation_array()

    #dilate the player mask then remove it from the annotation
    players_cv_mask = mask_operations.convert_to_cv_mask(annotation.get_players_mask())
    players_cv_mask = cv2.dilate(players_cv_mask, PLAYERS_KERNEL, iterations=1)
    players_mask = mask_operations.convert_from_cv_mask(players_cv_mask)
    annotation_array[players_mask > 0] = SoccerAnnotation.irrelevant_value()

    #also dilate the sidelines mask
    sidelines_cv_mask = mask_operations.convert_to_cv_mask(annotation.get_sidelines_mask())
    sidelines_cv_mask = cv2.dilate(sidelines_cv_mask, SIDELINES_KERNEL, iterations=1)
    sidelines_mask = mask_operations.convert_from_cv_mask(sidelines_cv_mask)
    annotation_array[sidelines_mask > 0] = SoccerAnnotation.sidelines_value()

    annotation = SoccerAnnotation(annotation_array)

    print 'annotation values before subdividing: ', np.unique(annotation.get_annotation_array())
    
    #subdivide the annotation
    relevant_annotation_values = (SoccerAnnotation.grass_value(), SoccerAnnotation.sidelines_value())
    annotation = annotation.subdivide_annotation(X_SUB, Y_SUB, relevant_annotation_values)

    print 'annotation values after subdividing: ', np.unique(annotation.get_annotation_array())
    
    #generate the contours
    contour_generator = EdgesContourGenerator()
    contours = contour_generator.generate_contours(image, field_mask)

    return image, contours, annotation

# def transform_cv_contours(cv_contours, homography_matrix):
#     transformed_cv_contours = []
       
#     for cv_contour in cv_contours:
#         cv_contour = np.float32(cv_contour)
#         new_cv_contour = cv2.perspectiveTransform(cv_contour, homography_matrix)
#         transformed_cv_contours.append(np.int32(new_cv_contour))
        
#     return transformed_cv_contours

# def get_cv_contours_and_annotation(image):

#     print 'generating annotations'
#     image_annotator = GMMSoccerFieldAnnotator()
#     annotation = image_annotator.annotate_image(image)

#     max_value = annotation.max()
#     cv2.imshow('the annotation before processing', np.uint8(annotation * (255 / max_value)))
#     cv2.waitKey(0)
#     cv2.destroyAllWindows()

#     #process the annotation before sending it to the homography detector
#     processed_annotation = process_annotation(annotation)

#     max_value = processed_annotation.max()
#     cv2.imshow('the annotation after processing', np.uint8(processed_annotation * (255 / max_value)))
#     cv2.waitKey(0)
#     cv2.destroyAllWindows()

#     #get the contours on the image
#     field_mask = GMMSoccerFieldAnnotator.get_soccer_field_mask(annotation)

#     contour_generator = EdgesContourGenerator()
#     cv_contours = [contour.get_cv_contour() for contour in contour_generator.generate_contours(image, field_mask)]

#     return cv_contours, processed_annotation

if __name__=='__main__':
    
    print 'starting feature homography test'
    print 'loading images %s and %s: ' % (IMAGE_NAME_1, IMAGE_NAME_2)
    
    # image_1 = cv2.imread(TEST_IMAGE_NAME_1, 1)
    # image_2 = cv2.imread(TEST_IMAGE_NAME_2, 1)

    # cv_contours_1, annotation_1 = get_cv_contours_and_annotation(image_1)
    # cv_contours_2, annotation_2 = get_cv_contours_and_annotation(image_2)

    image_1, contours_1, annotation_1 = load_image_contours_and_annotation(IMAGE_NAME_1, ANNOTATION_NAME_1)
    image_2, contours_2, annotation_2 = load_image_contours_and_annotation(IMAGE_NAME_2, ANNOTATION_NAME_2)

    annotation_array_1 = annotation_1.get_annotation_array()
    max_value_1 = annotation_array_1.max()
    annotation_array_2 = annotation_2.get_annotation_array()
    max_value_2 = annotation_array_2.max()

    #print 'annotation values 1: ', set(annotation_array_1.flatten().tolist())
    #print 'annotation values 2: ', set(annotation_array_2.flatten().tolist())
    
    draw_images_side_by_side(np.uint8(annotation_array_1 * (255 / max_value_1)),
                             np.uint8(annotation_array_2 * (255 / max_value_2)),
                             'annotations')

    # cv2.imshow('the first annotation', np.uint8(annotation_array_1 * (255 / max_value)))
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    #compute the homography
    homography_calculator = FeatureHomographyCalculator()
    homography_calculator.set_annotations(annotation_1, annotation_2)
    homography_matrix = homography_calculator.calculate_homography(image_1, image_2)

    print 'the homography matrix is:'
    print homography_matrix

    #transform the contours from the source image
    #transformed_cv_contours = transform_cv_contours(cv_contours_1, homography_matrix)
    transformed_contours = planar_geometry.apply_homography_to_contours(homography_matrix, contours_1)
    transformed_cv_contours = [c.get_cv_contour() for c in transformed_contours]
    #and draw them on the target image
    cv2.drawContours(image_2, transformed_cv_contours, -1, (255,0,0), 1)

    cv2.imshow('the transformed contours', image_2)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
