import tensorflow as tf
import numpy as np
import cv2
import os
from ....source.preprocessing.soccer_field_embedding_generator.soccer_field_embedding_generator import SoccerFieldEmbeddingGenerator
from ....source.preprocessing.highlighted_shape_generator.highlighted_shape_generator import HighlightedShapeGenerator
from ....source.image_processing.homography_calculator.camera_shape_distance_minimizer import CameraShapeDistanceMinimizer
from ....source.auxillary import camera_operations, planar_geometry
from ....source.auxillary.soccer_field_geometry import SoccerFieldGeometry
from ....source.auxillary.camera_matrix import CameraMatrix
from ...auxillary import primitive_drawing

#FRAME = 590
#FRAME = 1
#FRAME = 32
#FRAME = 152
#FRAME = 677
#FRAME = 937
FRAME = 1066
IMAGE_DIR = '/Users/daniel/Documents/soccer/images/images-8_15-9_31'
PREPROCESSING_DIR = '/Users/daniel/Documents/soccer/output/preprocessing/data/images-8_15-9_31'

IMAGE_FILE = os.path.join(IMAGE_DIR, 'image_%d.png' % FRAME)
HIGHLIGHTED_SHAPES_DIR = os.path.join(PREPROCESSING_DIR, 'highlighted_shapes/highlighted_shapes_%d' % FRAME)
#SOCCER_EMBEDDING_DIR = os.path.join('output/scripts/preprocessing/data', PREPROCESSING_DIR,
#                                     'soccer_field_embedding/embedding_%d' % FRAME)
SOCCER_EMBEDDING_DIR = os.path.join(PREPROCESSING_DIR,
                                     'soccer_field_embedding/embedding_%d' % FRAME)

SOURCE_SHAPE_COLOR = (0,255,0)
TARGET_SHAPE_COLOR = (255,0,0)
POINT_COLOR = (0,0,255)
POINT_RADIUS = 3

def test_basic_ops():
    CAMERA_POSITION  = np.array([64, 200, 60], np.float32)
    THETA_X = (90-10) * np.pi/180
    THETA_Z = 10 * np.pi/180
    FOCUS = 200.0
    ASPECT = 1.0
    OUTPUT_IMAGE_SHAPE = (256, 256)
    CAMERA_CENTER = np.array(OUTPUT_IMAGE_SHAPE, np.float32) / 2

    fixed_param_dict = {'camera_position':CAMERA_POSITION, 'aspect':ASPECT}
    shape_distance_minimizer = CameraShapeDistanceMinimizer(fixed_param_dict, 0, 0, 0)

    camera_parameters = camera_operations.CameraParameters(THETA_X, THETA_Z, FOCUS, ASPECT, CAMERA_CENTER, CAMERA_POSITION)
    camera = camera_operations.build_camera(camera_parameters)
    parameter_array = shape_distance_minimizer._camera_parameters_to_parameter_array(camera_parameters)

    print 'camera_parameters:'
    print camera_parameters
    print 'parameter_array:'
    print parameter_array

    print 'converting back to camera parameters:'
    print shape_distance_minimizer._parameter_tensor_to_camera_parameters(parameter_array)

    print 'building homography matrix...'
    sess = tf.Session()
    H = shape_distance_minimizer._build_homography_matrix(parameter_array)
    print 'got the tensor: ', H
    print 'H:'
    print sess.run(H)

    print 'true H:'
    print camera.get_homography_matrix()

def test_normalize_camera_parameters():
    CAMERA_POSITION  = np.array([64, 200, 60], np.float32)
    THETA_X = (90-10) * np.pi/180
    THETA_Z = 10 * np.pi/180
    FOCUS = 200.0
    ASPECT = 1.0
    OUTPUT_IMAGE_SHAPE = (256, 256)
    CAMERA_CENTER = np.array(OUTPUT_IMAGE_SHAPE, np.float32) / 2

    camera_parameters = camera_operations.CameraParameters(THETA_X, THETA_Z, FOCUS, ASPECT, CAMERA_CENTER, CAMERA_POSITION)
    camera = camera_operations.build_camera(camera_parameters)
    H = camera.get_homography_matrix()

    print 'camera parameters:'
    print camera_parameters
    print 'H:'
    print H
    
    N_source = planar_geometry.translation_matrix(np.array([-100,100]))
    T_target = planar_geometry.translation_matrix(np.array([-50,70]))
    R_target = np.eye(3)
    #R_target[:2,:2] = planar_geometry.rotation_matrix(np.pi/5)
    N_target = np.matmul(R_target,T_target)
    N_world = CameraShapeDistanceMinimizer._compute_world_normalization_matrix(N_source, camera_parameters)

    print 'N_source:'
    print N_source
    print 'N_target:'
    print N_target
    print 'N_world:'
    print N_world
    
    normalized_camera = CameraShapeDistanceMinimizer._normalize_camera_parameters(camera_parameters, N_world, N_target)
    normalized_parameters = camera_operations.compute_camera_parameters(normalized_camera)

    print 'normalized camera:'
    print normalized_camera.get_numpy_matrix()
    print 'normalized_parameters:'
    print normalized_parameters
    
    normalized_camera = camera_operations.build_camera(normalized_parameters)
    normalized_parameters = camera_operations.compute_camera_parameters(normalized_camera)
    print 'normalized camera:'
    print normalized_camera.get_numpy_matrix()
    print 'normalized_parameters:'
    print normalized_parameters

    normalized_camera = camera_operations.build_camera(normalized_parameters)
    normalized_parameters = camera_operations.compute_camera_parameters(normalized_camera)
    print 'normalized camera:'
    print normalized_camera.get_numpy_matrix()
    print 'normalized_parameters:'
    print normalized_parameters

    normalized_H = normalized_camera.get_homography_matrix()
    
    print 'normalized camera parameters:'
    print normalized_parameters
    print 'normalized H:'
    print normalized_H
    print normalized_H/normalized_H[2,2]
    print 'N_target * H * N_source^-1:'
    H_test = np.matmul(N_target, np.matmul(H, np.linalg.inv(N_source)))
    print H_test
    print H_test / H_test[2,2]

def load_absolute_soccer_field():
    '''
    Return a tuple (absolute shapes, absolute grid points)
    '''
    ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)
    SCALE = 6
    FIELD_WIDTH = absolute_image.shape[1]-1
    FIELD_LENGTH = absolute_image.shape[0]-1
    GRID_WIDTH = 20
    GRID_LENGTH = 14
    soccer_field = SoccerFieldGeometry(SCALE, FIELD_WIDTH, FIELD_LENGTH, GRID_WIDTH, GRID_LENGTH)
    
    return absolute_image, soccer_field

def padded_image(image):
    padding = 100
    padded_shape = (image.shape[0]+padding, image.shape[1]+padding, 3)
    padded_image = np.zeros(padded_shape, np.uint8)
    padded_image[:padded_shape[0]-padding, :padded_shape[1]-padding] = image
    return padded_image

def display_homography(image, H, source_shapes, target_shapes, source_grid_points):
    '''
    image - a numpy array with shape (n,m,3) and type np.uint8
    H - a numpy array with shape (3,3) and type np.float32
    source_shapes - a list of PlanarShape objects
    target_shapes - a list of PlanarShape objects
    source_grid_points - a list of points

    Use the homography to transfrom the source shapes to the target image and draw
    both the transformed shapes and the target shapes.
    '''
    image = padded_image(image.copy())
    for shape in target_shapes:
        primitive_drawing.draw_shape(image, shape, TARGET_SHAPE_COLOR)

    translated_shapes = (planar_geometry.apply_homography_to_shape(H,s) for s in source_shapes)
    for shape in translated_shapes:
        primitive_drawing.draw_shape(image, shape, SOURCE_SHAPE_COLOR)

    transformed_grid_points = cv2.perspectiveTransform(source_grid_points.reshape((-1,1,2)), H).reshape((-1,2))
    for point in transformed_grid_points:
        cv2.circle(image, tuple(np.int32(point).tolist()), POINT_RADIUS, POINT_COLOR, thickness=-1)
        
    cv2.imshow('target and transformed shapes', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    return

def load_homography_and_embedding():
    '''
    return a tuple (H, highlighted_shapes, embedding_dict)
    embedding_dict is a dictionary with items (source key, target key)
    '''
    H = np.load(ABSOLUTE_HOMOGRAPHY_FILE)
    return H, EMBEDDING_DICT

def load_homography_and_shapes(soccer_field):
    '''
    return a tuple (homography, source_shapes, target_shapes)
    '''
    H, embedding_dict = SoccerFieldEmbeddingGenerator._load_embedding(SOCCER_EMBEDDING_DIR)
    highlighted_shapes = HighlightedShapeGenerator._load_highlighted_shapes(HIGHLIGHTED_SHAPES_DIR)

    print 'embedding dict: ', embedding_dict
    source_shapes = []
    target_shapes = []

    for source_key, target_key in embedding_dict.iteritems():
        source_shapes.append(soccer_field.get_highlighted_shape(source_key).get_shape())
        target_shapes.append(highlighted_shapes[target_key].get_shape())

    return H, source_shapes, target_shapes

def convert_to_camera_parameters(H):
    camera = CameraMatrix.from_homography_and_alpha(H, -1)
    camera_parameters = camera_operations.compute_camera_parameters(camera)

    return camera_parameters

def compare_camera_parameters(cp_dict):
    for cp_name,cp in cp_dict.iteritems():
        print '%s:' % cp_name
        print 'position: ', cp.camera_position
        print 'theta_x: %f, theta_z: %f, focus: %f, aspect: %f' % (cp.theta_x*180/np.pi, cp.theta_z*180/np.pi,
                                                                   cp.focus, cp.aspect)
        print 'camera_center: ', cp.camera_center

def test_optimize_homography():
    LEARNING_RATE = 0.01
    MAX_EPOCHS = 50000
    LOCAL_MINIMUM_THRESHOLD = 10**(-12)
    
    image = cv2.imread(IMAGE_FILE, 1)
    source_image, soccer_field = load_absolute_soccer_field()

    absolute_highlighted_shape_dict = soccer_field.get_highlighted_shape_dict(format_keys=True)
    all_source_shapes = [hs.get_shape() for hs in absolute_highlighted_shape_dict.itervalues()]
    source_grid_points = soccer_field.get_grid_points()

    original_H, source_shapes, target_shapes = load_homography_and_shapes(soccer_field)
    print 'original H:'
    print original_H / original_H[2,2]
        
    display_homography(image, original_H, all_source_shapes, target_shapes, source_grid_points)

    source_image_shape = np.array(source_image.shape, np.int32)
    target_image_shape = np.array(image.shape, np.int32)

    initial_camera_parameters = convert_to_camera_parameters(original_H)
    print 'initial_camera_parameters:'
    print initial_camera_parameters
    d = initial_camera_parameters._asdict()
    d['camera_position'] = np.array([372, 900, 250], np.float32)
    initial_camera_parameters = camera_operations.CameraParameters(**d)
    print 'new camera parameters:'
    print initial_camera_parameters
    
    initial_camera = camera_operations.build_camera(initial_camera_parameters)
    print 'initial H:'
    print initial_camera.get_homography_matrix() / initial_camera.get_homography_matrix()[2,2]
    print 'initial C:'
    print initial_camera.get_internal_parameters()
    print 'initial R:'
    print initial_camera.get_world_to_camera_rotation_matrix()
    
    display_homography(image, initial_camera.get_homography_matrix(), all_source_shapes, target_shapes, source_grid_points)

    #we fix theta_x=pi/2
    #fixed_parameter_dict = {'theta_x': np.pi/2}#, 'aspect':initial_camera_parameters.aspect}
    fixed_parameter_dict = {'camera_position': np.array([372, 900, 250], np.float32)}
    initial_parameter_dict = initial_camera_parameters._asdict()
    initial_parameter_dict.update(fixed_parameter_dict)

    print 'fixed parameter dict:'
    print fixed_parameter_dict
    print 'initial_parameter_dict:'
    print initial_parameter_dict
    
    optimizer = CameraShapeDistanceMinimizer(fixed_parameter_dict, LEARNING_RATE, MAX_EPOCHS, LOCAL_MINIMUM_THRESHOLD)

    optimizer.initialize_session()
    optimal_camera_parameters = optimizer.optimize_homography(initial_parameter_dict,
                                                              source_image_shape, source_shapes,
                                                              target_image_shape, target_shapes)
    optimizer.close_session()

    compare_camera_parameters({'initial_parameters':initial_camera_parameters,
                               'optimal_parameters:':optimal_camera_parameters})

    optimal_camera = camera_operations.build_camera(optimal_camera_parameters)
    display_homography(image, optimal_camera.get_homography_matrix(), all_source_shapes, target_shapes, source_grid_points)
    

if __name__ == '__main__':
    #test_basic_ops()
    #test_normalize_camera_parameters()
    test_optimize_homography()
