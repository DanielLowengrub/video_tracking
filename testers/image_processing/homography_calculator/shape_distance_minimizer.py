"""
Here we optimize a homography based on a collection of source and target shapes
"""
import numpy as np
from ....source.preprocessing.highlighted_shape_generator.highlighted_shape_generator import HighlightedShapeGenerator
from ....source.preprocessing.soccer_field_embedding_generator.soccer_field_embedding_generator import SoccerFieldEmbeddingGenerator
from ....source.auxillary.soccer_field_geometry import SoccerFieldGeometry
from ....source.image_processing.homography_calculator.shape_distance_minimizer import ShapeDistanceMinimizer
from ....source.image_processing.homography_calculator.simple_camera_shape_distance_minimizer import SimpleCameraShapeDistanceMinimizer
from ...auxillary import primitive_drawing
from ....source.auxillary.fit_points_to_shape.line import Line
from ....source.auxillary.fit_points_to_shape.ellipse import Ellipse
from ....source.auxillary import planar_geometry
from ....source.auxillary.camera_matrix import CameraMatrix
from ...auxillary import camera_generator
import cv2
import os

# FRAME = 12
# INTERVAL = (0,274)

# IMAGE_DIR = 'test_data/video7'
# PREPROCESSING_DIR = 'video7'

# IMAGE_FILE = os.path.join(IMAGE_DIR, 'image_%d.png' % FRAME)
# HIGHLIGHTED_SHAPES_DIR = os.path.join('output/scripts/preprocessing/data', PREPROCESSING_DIR,
#                                       'highlighted_shapes/highlighted_shapes_%d' % FRAME)
# SOCCER_EMBEDDING_DIR = os.path.join('output/scripts/preprocessing/data', PREPROCESSING_DIR,
#                                     'soccer_field_embedding/embedding_%d' % FRAME)

# ABSOLUTE_HOMOGRAPHY_FILE = os.path.join('output/scripts/preprocessing/data', PREPROCESSING_DIR,
#                                         'absolute_homography/homography_%d_%d' % INTERVAL,
#                                         'homography_%d.npy' % FRAME)

# EMBEDDING_DICT = {SoccerFieldGeometry.CENTER_LINE : 0,
#                   SoccerFieldGeometry.CENTER_CIRCLE : 1}

#FRAME = 590
#FRAME = 1
#FRAME = 32
#FRAME = 152
#FRAME = 677
#FRAME = 937
FRAME = 1066
IMAGE_DIR = '/Users/daniel/Documents/soccer/images/images-8_15-9_31'
PREPROCESSING_DIR = '/Users/daniel/Documents/soccer/output/preprocessing/data/images-8_15-9_31'

IMAGE_FILE = os.path.join(IMAGE_DIR, 'image_%d.png' % FRAME)
HIGHLIGHTED_SHAPES_DIR = os.path.join(PREPROCESSING_DIR, 'highlighted_shapes/highlighted_shapes_%d' % FRAME)
#SOCCER_EMBEDDING_DIR = os.path.join('output/scripts/preprocessing/data', PREPROCESSING_DIR,
#                                     'soccer_field_embedding/embedding_%d' % FRAME)
SOCCER_EMBEDDING_DIR = os.path.join(PREPROCESSING_DIR,
                                     'soccer_field_embedding/embedding_%d' % FRAME)

SOURCE_SHAPE_COLOR = (0,255,0)
TARGET_SHAPE_COLOR = (255,0,0)
POINT_COLOR = (0,0,255)
POINT_RADIUS = 3

def load_absolute_soccer_field():
    '''
    Return a tuple (absolute shapes, absolute grid points)
    '''
    ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)
    SCALE = 6
    FIELD_WIDTH = absolute_image.shape[1]-1
    FIELD_LENGTH = absolute_image.shape[0]-1
    GRID_WIDTH = 20
    GRID_LENGTH = 14
    soccer_field = SoccerFieldGeometry(SCALE, FIELD_WIDTH, FIELD_LENGTH, GRID_WIDTH, GRID_LENGTH)
    
    return absolute_image, soccer_field

def padded_image(image):
    padding = 100
    padded_shape = (image.shape[0]+padding, image.shape[1]+padding, 3)
    padded_image = np.zeros(padded_shape, np.uint8)
    padded_image[:padded_shape[0]-padding, :padded_shape[1]-padding] = image
    return padded_image

def display_homography(image, H, source_shapes, target_shapes, source_grid_points):
    '''
    image - a numpy array with shape (n,m,3) and type np.uint8
    H - a numpy array with shape (3,3) and type np.float32
    source_shapes - a list of PlanarShape objects
    target_shapes - a list of PlanarShape objects
    source_grid_points - a list of points

    Use the homography to transfrom the source shapes to the target image and draw
    both the transformed shapes and the target shapes.
    '''
    image = padded_image(image.copy())
    for shape in target_shapes:
        primitive_drawing.draw_shape(image, shape, TARGET_SHAPE_COLOR)

    translated_shapes = (planar_geometry.apply_homography_to_shape(H,s) for s in source_shapes)
    for shape in translated_shapes:
        primitive_drawing.draw_shape(image, shape, SOURCE_SHAPE_COLOR)

    transformed_grid_points = cv2.perspectiveTransform(source_grid_points.reshape((-1,1,2)), H).reshape((-1,2))
    for point in transformed_grid_points:
        cv2.circle(image, tuple(np.int32(point).tolist()), POINT_RADIUS, POINT_COLOR, thickness=-1)
        
    cv2.imshow('target and transformed shapes', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    return

def load_homography_and_embedding():
    '''
    return a tuple (H, highlighted_shapes, embedding_dict)
    embedding_dict is a dictionary with items (source key, target key)
    '''
    H = np.load(ABSOLUTE_HOMOGRAPHY_FILE)
    return H, EMBEDDING_DICT

def load_homography_and_shapes(soccer_field):
    '''
    return a tuple (homography, source_shapes, target_shapes)
    '''
    H, embedding_dict = SoccerFieldEmbeddingGenerator._load_embedding(SOCCER_EMBEDDING_DIR)
    #H, embedding_dict = load_homography_and_embedding()
    highlighted_shapes = HighlightedShapeGenerator._load_highlighted_shapes(HIGHLIGHTED_SHAPES_DIR)

    print 'embedding dict: ', embedding_dict
    source_shapes = []
    target_shapes = []

    for source_key, target_key in embedding_dict.iteritems():
        source_shapes.append(soccer_field.get_highlighted_shape(source_key).get_shape())
        target_shapes.append(highlighted_shapes[target_key].get_shape())

    return H, source_shapes, target_shapes

def convert_to_camera_homography(H):
    camera = CameraMatrix.from_homography_and_alpha(H, -1)
    position, theta_x, theta_z, focus, camera_center = camera_generator.compute_camera_parameters(camera)
    
    # print 'position: ', position
    # print 'theta_x: %f, theta_z: %f, focus: %s' % (theta_x*180/np.pi, theta_z*180/np.pi, focus)
    # print 'camera_center: ', camera_center
    
    new_camera_direction = camera_generator.build_camera_direction(0, theta_z)
    # print 'new camera direction: ', new_camera_direction
    new_camera = camera_generator.build_camera(position, new_camera_direction, focus, camera_center)
    new_H = new_camera.get_homography_matrix()
    
    old_C = camera.get_internal_parameters()
    old_R = camera.get_world_to_camera_rotation_matrix()
    new_C = new_camera.get_internal_parameters()
    new_R = new_camera.get_world_to_camera_rotation_matrix()

    print 'old C:'
    print old_C / old_C[2,2]
    print 'old R:'
    print old_R

    print 'new C:'
    print new_C / new_C[2,2]
    print 'new R:'
    print new_R
    
    return new_H / new_H[2,2]

def compare_camera_parameters(H_dict):
    for H_name,H in H_dict.iteritems():
        print '%s:' % H_name
        camera = CameraMatrix.from_homography_and_alpha(H, -1)
        camera = CameraMatrix.from_homography_and_alpha(H, -1)
        position, theta_x, theta_z, focus, camera_center = camera_generator.compute_camera_parameters(camera)
    
        print 'position: ', position
        print 'theta_x: %f, theta_z: %f, focus: %s' % (theta_x*180/np.pi, theta_z*180/np.pi, focus)
        print 'camera_center: ', camera_center

def test_optimize_homography():
    LEARNING_RATE = 0.005
    MAX_EPOCHS = 10000
    LOCAL_MINIMUM_THRESHOLD = 10**(-12)
    
    image = cv2.imread(IMAGE_FILE, 1)
    source_image, soccer_field = load_absolute_soccer_field()

    absolute_highlighted_shape_dict = soccer_field.get_highlighted_shape_dict(format_keys=True)
    all_source_shapes = [hs.get_shape() for hs in absolute_highlighted_shape_dict.itervalues()]
    source_grid_points = soccer_field.get_grid_points()

    initial_H, source_shapes, target_shapes = load_homography_and_shapes(soccer_field)
    print 'initial H:'
    print initial_H
    initial_H = convert_to_camera_homography(initial_H)
    print 'camera H:'
    print initial_H
    
    display_homography(image, initial_H, all_source_shapes, target_shapes, source_grid_points)

    source_image_shape = np.array(source_image.shape, np.int32)
    target_image_shape = np.array(image.shape, np.int32)
    
    optimizer = SimpleCameraShapeDistanceMinimizer(LEARNING_RATE, MAX_EPOCHS, LOCAL_MINIMUM_THRESHOLD)
    #optimizer = ShapeDistanceMinimizer(LEARNING_RATE, MAX_EPOCHS, LOCAL_MINIMUM_THRESHOLD)
    optimizer.initialize_session()
    optimal_H = optimizer.optimize_homography(initial_H,
                                              source_image_shape, source_shapes,
                                              target_image_shape, target_shapes)
    optimizer.close_session()

    print 'the initial H is:'
    print initial_H
    print 'the optimal H is:'
    print optimal_H

    # target_image_center = np.array([image.shape[1]-1, image.shape[0]-1], np.float32) / 2.0
    # T = planar_geometry.translation_matrix(-target_image_center)
    # centered_target_shapes = [planar_geometry.apply_homography_to_shape(T, t) for t in target_shapes]
    # centered_initial_transformed_shapes = [planar_geometry.apply_homography_to_shape(np.dot(T,initial_H), s) for s in source_shapes]
    # centered_optimal_transformed_shapes = [planar_geometry.apply_homography_to_shape(np.dot(T,optimal_H), s) for s in source_shapes]
    # print 'centered target shapes:'
    # print '\n'.join(map(str, centered_target_shapes))
    # print 'centered originally transformed source shapes:'
    # print '\n'.join(map(str, centered_initial_transformed_shapes))
    # print 'centered optimally transformed source shapes:'
    # print '\n'.join(map(str, centered_optimal_transformed_shapes))

    # for (target, initial, optimal) in zip(centered_target_shapes, centered_initial_transformed_shapes,
    #                                                               centered_optimal_transformed_shapes):
    #     if target.is_ellipse():
    #         continue

    #     initial_norm_difference = min(np.linalg.norm(target.get_normal_vector() - initial.get_normal_vector()),
    #                                   np.linalg.norm(target.get_normal_vector() + initial.get_normal_vector()))
    #     optimal_norm_difference = min(np.linalg.norm(target.get_normal_vector() - optimal.get_normal_vector()),
    #                                   np.linalg.norm(target.get_normal_vector() + optimal.get_normal_vector()))
        
    #     print 'considering the image of: ', target
    #     print 'initial:                  ', initial
    #     print 'optimal:                  ', optimal
    #     print '   initial difference of normal vectors: ', initial_norm_difference
    #     print '   optimal difference of normal vectors: ', optimal_norm_difference

    compare_camera_parameters({'initial_H':initial_H, 'optimal_H:':optimal_H})
    display_homography(image, optimal_H, all_source_shapes, target_shapes, source_grid_points)
    
if __name__ == '__main__':
    #test_apply_inverse_homography_to_shapes()
    test_optimize_homography()
    
