import numpy as np
import cv2
from ....source.image_processing.homography_calculator.correspondence_homography_calculator import CorrespondenceHomographyCalculator
from ....source.auxillary.fit_points_to_shape.line import Line
from ....source.auxillary.planar_geometry import rotation_matrix, apply_homography_to_line
from .correspondence_homography_visualization import CorrespondenceHomographyVisualization
from ...auxillary.contour_shape_overlap.highlighted_shape_generator import draw_line
from ....source.auxillary.contour_shape_overlap.highlighted_shape import HighlightedShape
from ...auxillary.soccer_field_geometry import SoccerFieldGeometry

def get_homography(translation, rotation):
    R = rotation_matrix(rotation)

    homography_matrix = np.zeros((3,3))
    homography_matrix[:2,:2] = R
    homography_matrix[:2,2] = translation.transpose()
    homography_matrix[2,2] = 1

    return homography_matrix

def identity_test():
    #the correspondence contains two pairs of lines, one vertical and one horizontal.
    lines_source = []
    lines_source.append(Line.from_point_and_direction(np.array([100,100]), np.array([0,1])))
    lines_source.append(Line.from_point_and_direction(np.array([150,100]), np.array([0,1])))
    lines_source.append(Line.from_point_and_direction(np.array([100,100]), np.array([1,0])))
    lines_source.append(Line.from_point_and_direction(np.array([100,150]), np.array([1,0])))

    translation = np.array([0,0])
    rotation = np.pi/6
    H = get_homography(translation, rotation)
    print 'using the homography matrix:'
    print H

    shape_correspondences = [(l,apply_homography_to_line(H,l)) for l in lines_source]

    print 'using the shape correspondences:'
    print ', '.join(['(%s, %s)' % tuple(map(str,sc)) for sc in shape_correspondences])

    source_image = np.zeros((500,500,3), np.uint8)
    target_image = np.zeros((500,500,3), np.uint8)
    homography_calculator = CorrespondenceHomographyCalculator()

    homography = homography_calculator.compute_homography(source_image, target_image, shape_correspondences)

    print 'actual homography:'
    print H
    print 'computed homography:'
    print homography

    chv = CorrespondenceHomographyVisualization()
    visualization = chv.get_visualization(target_image, homography, shape_correspondences)

    cv2.imshow('the transformed shapes', visualization)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def load_soccer_field_geometry():
    ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)

    SCALE = 6
    FIELD_WIDTH = absolute_image.shape[1]
    FIELD_LENGTH = absolute_image.shape[0]
    GRID_WIDTH = 20
    GRID_LENGTH = 14
    soccer_field = SoccerFieldGeometry(SCALE, FIELD_WIDTH, FIELD_LENGTH, GRID_WIDTH, GRID_LENGTH)

    return soccer_field

def soccer_test():
    #189, 216: penalty bottom, sideline top, penalty top, sideline right, goal side, penalty side
    #We load shapes from a highlighted shape directory, and create a correspondence with the shapes in a SoccerFieldGeometry object
    LINE_DIRECTORY_NAMES = ['output/tester/highlighted_shapes/saved_data/frames_1_19/image_7/highlighted_line_%d' % i for i in range(3)]
    IMAGE_NAME = 'test_data/video3/out8.png'
    ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)
    image = cv2.imread(IMAGE_NAME, 1)

    lines = []
    for line_directory_name in LINE_DIRECTORY_NAMES:
        print 'loading highlighted shape from: ', line_directory_name
        highlighted_shape = HighlightedShape.load_from_directory(line_directory_name, HighlightedShape.LINE)
        lines.append(highlighted_shape.get_shape())

        image_with_line = image.copy()
        draw_line(image_with_line, lines[-1])
        cv2.imshow('the line', image_with_line)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    soccer_field = load_soccer_field_geometry()
    
    #for the moment, we hardcode the correspondences
    side = SoccerFieldGeometry.RIGHT
    
    shape_correspondences = []
    # shape_correspondences.append((soccer_field.get_highlighted_shape((SoccerFieldGeometry.GOAL_AREA, SoccerFieldGeometry.BOTTOM)).get_shape(), lines[0]))
    # shape_correspondences.append((soccer_field.get_highlighted_shape((SoccerFieldGeometry.PENALTY_AREA, SoccerFieldGeometry.BOTTOM)).get_shape(), lines[1]))
    # shape_correspondences.append((soccer_field.get_highlighted_shape((SoccerFieldGeometry.PENALTY_AREA, SoccerFieldGeometry.TOP)).get_shape(), lines[2]))
    # shape_correspondences.append((soccer_field.get_highlighted_shape((SoccerFieldGeometry.SIDELINE, SoccerFieldGeometry.TOP)).get_shape(), lines[3]))
    # shape_correspondences.append((soccer_field.get_highlighted_shape((SoccerFieldGeometry.SIDELINE, SoccerFieldGeometry.RIGHT)).get_shape(), lines[4]))
    # shape_correspondences.append((soccer_field.get_highlighted_shape((SoccerFieldGeometry.GOAL_AREA, SoccerFieldGeometry.RIGHT)).get_shape(), lines[5]))
    # shape_correspondences.append((soccer_field.get_highlighted_shape((SoccerFieldGeometry.PENALTY_AREA, SoccerFieldGeometry.RIGHT)).get_shape(), lines[6]))
    shape_correspondences.append((soccer_field.get_highlighted_shape(SoccerFieldGeometry.CENTER_LINE).get_shape(), lines[0]))
    shape_correspondences.append((soccer_field.get_highlighted_shape((SoccerFieldGeometry.PENALTY_AREA, SoccerFieldGeometry.RIGHT)).get_shape(), lines[1]))
    shape_correspondences.append((soccer_field.get_highlighted_shape((SoccerFieldGeometry.SIDELINE, SoccerFieldGeometry.BOTTOM)).get_shape(), lines[2]))
     
    
    homography_calculator = CorrespondenceHomographyCalculator()
    homography_calculator.set_source_and_target_images(absolute_image, image)
    homography, matching_errors = homography_calculator.compute_homography(absolute_image, image, shape_correspondences)

    print 'computed homography:'
    print homography
    print 'errors:'
    print matching_errors
    
    chv = CorrespondenceHomographyVisualization()
    visualization = chv.get_visualization(image, homography, shape_correspondences, soccer_field.get_grid_points())

    cv2.imshow('the transformed shapes', visualization)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    cv2.imwrite('output/homography_visualization.png', visualization)
    
if __name__ == '__main__':
    #identity_test()
    soccer_test()
    
