import numpy as np
import cv2
from ....source.image_processing.contour_generator.edges_contour_generator import EdgesContourGenerator

###########
# This is a tester for code.image_processing.contour_generator.edges_contour_generator
#
# We generate an image with various shapes, and then call the edge contour generator to generate contours.
##########

def generate_test_image():
    '''
    We create a couple of shapes.
    '''

    image = np.zeros((400, 600, 3), np.uint8)

    #draw the outline of a circle
    center = (200, 200)
    radius = 100
    thickness = 5
    color = (255, 0, 0)
    cv2.circle(image, center, radius, color, thickness)

    #draw a rectangle
    pt1 = (400, 20)
    pt2 = (500, 24)
    thickness = -1 # filled in
    color = (0, 255, 0)
    cv2.rectangle(image, pt1, pt2, color, thickness)

    return image

def test():

    image = generate_test_image()

    print 'displaying the test image'
    
    cv2.imshow('test image', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    print 'generating the contours'

    contourGenerator = EdgesContourGenerator()
    contours = contourGenerator.generate_contours(image)
    cv_contours = [contour.get_cv_contour() for contour in contours]

    #first draw the contours on the image in white
    cv2.drawContours(image, cv_contours, -1, (255, 255, 255), 3)

    print 'displaying image with contours...'

    cv2.imshow('image with contours', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    #now draw the outer masks.
    print 'drawing outer masks...'

    for i, contour in enumerate(contours):
        print 'this contour has %d child contours' % len(contour._children_contours)
        cv2.imshow('outer mask of contours %d' % i, contour.get_outer_mask())
        cv2.waitKey(0)
        cv2.destroyAllWindows()


def test_on_soccer_image():
    from ....source.image_processing.field_extractor.stupid_field_extractor import StupidFieldExtractor
    
    #interesting test images
    #215
    #141
    #2
    #96

    TEST_IMAGE_NAME = 'test_data/video3/out71.png'

    print 'loading image: ', TEST_IMAGE_NAME
    image = cv2.imread(TEST_IMAGE_NAME, 1)

    print 'getting the field mask...'
    field_extractor = StupidFieldExtractor()
    field_mask = field_extractor.get_field_mask(image)
    
    print 'generating contours...'
    contourGenerator = EdgesContourGenerator()
    contours = contourGenerator.generate_contours(image, mask=field_mask)
    cv_contours = [contour.get_cv_contour() for contour in contours]
    
    #first draw the contours on the image in white
    cv2.drawContours(image, cv_contours, -1, (255, 255, 255), 1)

    #then draw all the points in blue
    points = np.vstack(cv_contours).reshape((-1,2))
    radius = 1
    color = (255,0,0)
    thickness = -1
    
    for point in points:
        #cv2.circle(image, tuple(np.int32(point).tolist()), radius, color, thickness=thickness)
        image[point[1],point[0]] = np.array([255,0,0])
        
    cv2.imshow('image with contours and points', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


if __name__ == '__main__':
    #test()
    test_on_soccer_image()
