import numpy as np
import cv2
from ....source.image_processing.foreground_extractor.simple_foreground_extractor import SimpleForegroundExtractor
from ....source.image_processing.field_extractor.stupid_field_extractor import StupidFieldExtractor

#######
# This is a tester for the file code/image_processing/foreground_extractor/simple_foreground_extractor.py
#
# We load an image and display the part of the image that the field extractor declares as a field.
######

#interesting test images
#215
#141
#2
#96

TEST_IMAGE_NAME = 'test_data/video3/out2.png'

if __name__=='__main__':
    
    print 'starting stupid foreground extractor tester'
    print 'we load an image and then display the part of the image that is classified as a field'
    
    print 'loading image: ', TEST_IMAGE_NAME
    image = cv2.imread(TEST_IMAGE_NAME, 1)

    print 'generating the field mask'
    field_extractor = StupidFieldExtractor()
    field_mask = field_extractor.get_field_mask(image)

    print 'creating the foreground extractor'
    foreground_extractor = SimpleForegroundExtractor()

    print 'generating the foreground mask'
    foreground_mask = foreground_extractor.get_foreground_mask(image, field_mask)
    mask_shape = foreground_mask.shape
    masked_image = np.uint8(image * foreground_mask.reshape((mask_shape[0], mask_shape[1], 1)))

    cv2.imshow('the original image', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    cv2.imshow('the foreground mask', foreground_mask)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    cv2.imshow('the masked image', masked_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


