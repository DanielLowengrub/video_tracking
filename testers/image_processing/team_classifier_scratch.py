import numpy as np
import cv2
import matplotlib.pyplot as plt
import itertools

from ...source.auxillary.soccer_field_geometry import SoccerFieldGeometry
from ...source.dynamics.particle_filter_fast.local_player_data_small import LocalPlayerDataSmall
from ...source.image_processing.image_annotator.soccer_annotation import SoccerAnnotation
from ...source.auxillary.camera_matrix import CameraMatrix
from ...source.dynamics.particle_filter_fast.frame_data import FrameData
from ...source.dynamics.particle_filter_fast.padded_frame_data import PaddedFrameData
from ...source.auxillary import histogram_operations, planar_geometry

FRAME_INDEX = 33
IMAGE_FILE = 'test_data/video4/image_%d.png' % FRAME_INDEX
ANNOTATION_FILE = 'output/scripts/preprocessing/data/images_50_269/annotation/annotation_%d.npy' % FRAME_INDEX
CAMERA_FILE = 'output/scripts/preprocessing/data/images_50_269/camera/camera_0_219/camera_%d.npy' % FRAME_INDEX
PLAYER_POSITIONS_FILE = 'output/scripts/preprocessing/data/images_50_269/players/players_0_219/players_%d/positions.npy' % FRAME_INDEX

NUM_HUE_VALS = 181
NUM_SAT_VALS = 256

def load_absolute_soccer_field():
    '''
    Return a tuple (absolute shapes, absolute grid points)
    '''
    ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)
    SCALE = 6
    FIELD_WIDTH = absolute_image.shape[1]-1
    FIELD_LENGTH = absolute_image.shape[0]-1
    GRID_WIDTH = 20
    GRID_LENGTH = 14
    soccer_field = SoccerFieldGeometry(SCALE, FIELD_WIDTH, FIELD_LENGTH, GRID_WIDTH, GRID_LENGTH)

    return soccer_field

def build_local_player_datas():
    '''
    return a list of LocalPlayerDataSmall objects
    '''
    image = cv2.imread(IMAGE_FILE, 1)
    soccer_annotation = SoccerAnnotation(np.load(ANNOTATION_FILE))
    foreground_mask = soccer_annotation.get_players_mask() > 0
    camera_matrix = CameraMatrix(np.load(CAMERA_FILE))
    positions = np.load(PLAYER_POSITIONS_FILE)

    frame_data = FrameData(image, foreground_mask, camera_matrix, [])
    padding = 50
    frame_data = PaddedFrameData.from_frame_data(frame_data, padding)
    
    soccer_field = load_absolute_soccer_field()
    soccer_rectangle = soccer_field.get_field_rectangle()

    # cv2.imshow('image', image)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    # cv2.imshow('fg mask', np.float32(foreground_mask))
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    # print 'positions:'
    # print absolute_positions

    local_datas = [LocalPlayerDataSmall.from_frame_and_positions(soccer_rectangle, frame_data, position.reshape((1,2)))
                   for position in positions]

    H = camera_matrix.get_homography_matrix()
    image_positions = np.int32(planar_geometry.apply_homography_to_points(H, positions))
    
    return local_datas, image_positions


def load_player_mask():
    '''
    return a tuple (image, player mask)
    image is a np array with shape (n,m,3) and player mask has shape (n,m)
    '''
    image = cv2.imread(IMAGE_FILE, 1)
    soccer_annotation = SoccerAnnotation(np.load(ANNOTATION_FILE))
    player_mask = soccer_annotation.get_players_mask() > 0

    return image, player_mask

def load_player_mask_and_ellipses():
    '''
    return a tuple (image, player mask, player rectangles, player ellipse masks)
    '''

    
def display_hs():
    image, player_mask = load_player_mask()
    image_hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

    #get the hue/saturation values from the player pixels
    player_hs = image_hsv[player_mask][:,:2]
    player_gr = image[player_mask][:,1:]
    
    hs_map = np.zeros((NUM_HUE_VALS, NUM_SAT_VALS), np.float32)
    hs_map[player_hs[:,0], player_hs[:,1]] = 1

    gr_map = np.zeros((256, 256), np.float32)
    gr_map[player_gr[:,0], player_gr[:,1]] = 1
    
    cv2.imshow('hue and saturation values', hs_map)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    cv2.imshow('green and red values', gr_map)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def display_hs_histogram():
    image, player_mask = load_player_mask()

    # image_copy = image.copy()
    # image_copy[np.logical_not(player_mask)] = 0
    # cv2.imshow('masked image', image_copy)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    #player_mask = np.float32(player_mask)
    #histogram = histogram_operations.get_normalized_hs_histogram(image, player_mask, nbins=(180,256))

    image_hsv = np.int32(cv2.cvtColor(image, cv2.COLOR_BGR2HSV))
    hue = image_hsv[player_mask][:,0] * 2
    sat = image_hsv[player_mask][:,1]
    histogram, xbins, ybins = np.histogram2d(hue,sat,[180*2,256],[[0,180*2],[0,256]])
    
    # player_mask = np.uint8(player_mask) * 255
    # image_gr = image[:,:,(1,2)]

    # cv2.imshow('green', image_gr[:,:,0])
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    # cv2.imshow('red', image_gr[:,:,1])
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    # red = image_gr[:,:,1][player_mask>0]
    # green = image_gr[:,:,0][player_mask>0]
    # histogram, xbins, ybins = np.histogram2d(green,red,[256,256],[[0,256],[0,256]])
    #histogram = cv2.calcHist( [image_gr], [0, 1], None, [256, 256], [0, 256, 0, 256])
    #histogram = np.float32(histogram) / histogram.max()
    
    plt.imshow(histogram, interpolation='nearest', aspect='auto')
    plt.colorbar()
    plt.show()

def generate_local_player_data_histogram(local_player_data):
    '''
    local_player_data - a LocalPlayerData object
    return - a hue-saturation histogram of the region of the image that is contained in the local player datas histogram.
    '''
    image = local_player_data._frame_data.get_image()
    player_rectangle = local_player_data._player_rectangles.get_rectangle_objects()[0]
    foreground_mask = local_player_data._foreground_masks[0]
    player_mask = local_player_data._player_mask

    local_image = player_rectangle.get_slice(image)
    local_mask = np.float32(foreground_mask * player_mask)

    # cv2.imshow('local image', local_image)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    # cv2.imshow('local mask', local_mask)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    local_histogram = histogram_operations.get_normalized_hs_histogram(local_image, local_mask, nbins=(10,10))

    return local_histogram

def display_local_data_histograms():
    '''
    load a list of local player data objects and display a histogram for each one.
    '''

    local_player_datas, image_positions = build_local_player_datas()

    local_player_data_histograms = [generate_local_player_data_histogram(lpd) for lpd in local_player_datas]

    #print out the pairwise distances
    histogram_vectors = np.stack([h.ravel() for h in local_player_data_histograms])
    print 'Histogram distances: '
    print '      ', (' '*6).join(map(str,range(len(histogram_vectors))))

    for i,v in enumerate(histogram_vectors):
        distances = np.linalg.norm(v - histogram_vectors, axis=1)
        print '%2d: ' % i, distances
        
    #we tile the histograms in a square
    num_tiles_on_edge = int(np.sqrt(len(local_player_datas))) + 1
    print num_tiles_on_edge
    figure = plt.figure()
    figure, axes = plt.subplots(num_tiles_on_edge, num_tiles_on_edge)
    figure.tight_layout()
    
    for row, col in itertools.product(range(num_tiles_on_edge), range(num_tiles_on_edge)):
        player_index = (num_tiles_on_edge * row) + col
        if player_index >= len(local_player_datas):
            break
        
        ax = axes[row][col]
        histogram = local_player_data_histograms[player_index]
        image_position = image_positions[player_index]
        
        ax.imshow(histogram, interpolation='nearest', aspect='auto')
        ax.set_title('%d: [%d,%d]' % ((player_index,) + tuple(image_position.tolist())))
        
    plt.show()
    
if __name__ == '__main__':
    np.set_printoptions(precision=3, linewidth=200)
    #display_hs()
    #display_hs_histogram()
    display_local_data_histograms()
