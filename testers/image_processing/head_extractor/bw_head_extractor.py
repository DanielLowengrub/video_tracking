import numpy as np
import cv2

from ....source.image_processing.head_extractor.bw_head_extractor import BWHeadExtractor

###########
# This is a tester for BWHeadExtractor
#
# We load an image and draw a dot on the position that it marks as a head.

TEST_IMAGE_NAME = 'output/tester/player_tracker/combined_output_8/general_data_dump/player_3/fg_mask_020.png'

if __name__ == '__main__':

    #load a greyscale image
    image = cv2.imread(TEST_IMAGE_NAME, 0)

    cv2.imshow('original', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    head_extractor = BWHeadExtractor()
    head_positions = head_extractor.get_head_positions(image)

    if len(head_positions) == 0:
        print 'we did not find any heads'

    else:
        head_position = head_positions[0]
        print 'the head position is: ', head_position
        #draw the head on the image
        image = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)
        cv2.circle(image, tuple(head_position), 2, (255, 0, 0), -1)
        
        cv2.imshow('head', image)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

