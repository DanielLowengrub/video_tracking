import numpy as np
import cv2
from ....source.image_processing.image_annotator.gmm_soccer_field_annotator import GMMSoccerFieldAnnotator

#######
# This is a tester for the file code/image_processing/image_annotator.gmm_soccer_field_annotator
#
# We load an image and color it based on the annotations
######

#interesting test images
#215
#141
#2
#96
#159

TEST_IMAGE_NAME = 'test_data/video3/out159.png'
SIDELINES_COLOR = np.array((255,255,255))
PLAYERS_COLOR = np.array((255, 0, 0))

if __name__=='__main__':
    
    print 'starting the field annotator'
    print 'we load an image and then color the sidelines and players'
    
    print 'loading image: ', TEST_IMAGE_NAME
    image = cv2.imread(TEST_IMAGE_NAME, 1)

    print 'generating the annotation'
    image_annotator = GMMSoccerFieldAnnotator()
    annotation = image_annotator.annotate_image(image)


    sidelines_mask = GMMSoccerFieldAnnotator.get_sidelines_mask(annotation)
    players_mask = GMMSoccerFieldAnnotator.get_players_mask(annotation)
    colored_image = image.copy()
    colored_image[sidelines_mask == 1.0] = SIDELINES_COLOR
    colored_image[players_mask == 1.0] = PLAYERS_COLOR
    
    cv2.imshow('the original image', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    cv2.imshow('sidelines in white, players in blue', colored_image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    #we now test the subdivision algorithm
    subdivided_annotation = GMMSoccerFieldAnnotator.subdivide_annotation(annotation, 3, 3)

    max_value = subdivided_annotation.max()

    cv2.imshow('the subdivided annotation', np.uint8(subdivided_annotation * (255 / max_value)))
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    
