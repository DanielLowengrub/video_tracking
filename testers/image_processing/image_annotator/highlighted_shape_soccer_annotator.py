import numpy as np
import cv2
from ....source.image_processing.image_annotator.highlighted_shape_soccer_annotator import HighlightedShapeSoccerAnnotator
from ....source.preprocessing.highlighted_shape_generator.highlighted_shape_generator import HighlightedShapeGenerator
from ....source.preprocessing.generate_annotations_visualization import draw_visualization

IMAGE_NAME = 'test_data/video4/image_124.png'
FIELD_MASK_NAME = 'output/scripts/preprocessing/data/images_50_269/field_mask/mask_124.npy'
HIGHLIGHTED_SHAPES_DIRECTORY = 'output/scripts/preprocessing/data/images_50_269/highlighted_shapes/highlighted_shapes_124'

def test():
    print 'starting test...'
    image = cv2.imread(IMAGE_NAME, 1)

    print 'annotating image:'
    cv2.imshow('image', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    
    field_mask = np.load(FIELD_MASK_NAME)
    highlighted_shapes = HighlightedShapeGenerator._load_highlighted_shapes(HIGHLIGHTED_SHAPES_DIRECTORY)

    annotator = HighlightedShapeSoccerAnnotator()
    annotation = annotator.annotate_image(image, field_mask, highlighted_shapes)
    draw_visualization(image, annotation)
    
    cv2.imshow('annotated image', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

if __name__ == '__main__':
    test()
