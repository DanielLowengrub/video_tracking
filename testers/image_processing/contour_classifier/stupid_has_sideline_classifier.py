import numpy as np
import cv2
from ....source.image_processing.contour_classifier.stupid_has_sideline_classifier import StupidHasSidelineClassifier
from ....source.image_processing.contour_generator.edges_contour_generator import EdgesContourGenerator

###########
# This is a tester for code.image_processing.contour_classifier.stupid_has_sideline_classifier
#
# We generate an image with various shapes, and use the classifier to highlight the ones that may contain sidelines.
##########

def generate_test_image():
    '''
    We create a couple of shapes.
    '''

    image = np.zeros((400, 600, 3), np.uint8)

    #draw the outline of a circle.
    #This should be detected as a candidate for a contour.
    center = (200, 200)
    radius = 100
    thickness = 5
    color = (255, 0, 0)
    cv2.circle(image, center, radius, color, thickness)

    #draw a rectangle
    #This should not be detected
    pt1 = (400, 20)
    pt2 = (405, 30)
    thickness = -1 # filled in
    color = (255, 0, 0)
    cv2.rectangle(image, pt1, pt2, color, thickness)

    return image

def test():

    image = generate_test_image()

    print 'displaying the test image'
    
    cv2.imshow('test image', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    print 'generating the contours'

    contourGenerator = EdgesContourGenerator()
    contours = contourGenerator.generate_contours(image)

    print 'detecting sidelines'
    sideline_classifier = StupidHasSidelineClassifier()
    cv_sideline_contours = [contour.get_cv_contour() for contour in contours if sideline_classifier.is_match(contour)]
    cv_other_contours = [contour.get_cv_contour() for contour in contours if not sideline_classifier.is_match(contour)]

    print 'showing contours that may contain sidelines in green, and the rest in red'
    cv2.drawContours(image, cv_sideline_contours, -1, (0, 255, 0), 3)
    cv2.drawContours(image, cv_other_contours, -1, (0, 0, 255), 3)
    
    cv2.imshow('green = has sideline, red = does not', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

if __name__ == '__main__':

    test()

    
