import numpy as np
import cv2
from ....source.image_processing.contour_classifier.has_sideline_classifier import SimpleHasSidelineClassifier

###########
# This is a tester for code.image_processing.contour_classifier.has_sideline_classifier
#
# We generate an image with various shapes, and then call the edge contour generator to generate contours.
##########
