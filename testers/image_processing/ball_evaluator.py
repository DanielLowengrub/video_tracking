import numpy as np
import cv2
from ...source.image_processing.ball_evaluator import BallEvaluator
from ...source.image_processing.gmm_ball_evaluator import GMMBallEvaluator
from ...source.image_processing.edge_ball_evaluator import EdgeBallEvaluator
from ...source.auxillary import planar_geometry
from ...source.auxillary.soccer_field_geometry import SoccerFieldGeometry
from ...source.auxillary.camera_matrix import CameraMatrix
from ...source.auxillary.rectangles import Rectangles
from ..auxillary import primitive_drawing

# IMAGE_BASENAME = 'test_data/images_0_269/image_%d.png'
# CAMERA_BASENAME = 'output/scripts/preprocessing/data/images_0_269/camera/camera_0_269/camera_%d.npy'

# FRAMES_AND_POSITIONS = [(239, np.array([273,165])), (233,np.array([224,147])), (230,np.array([201,143])),
#                         (101,np.array([228,168])), (14,np.array([241,178])), (12,np.array([236,182])),
#                         (114,np.array([266,140])), (113,np.array([263,140])), (35,np.array([234,170])),
#                         (37,np.array([228,172])), (46,np.array([209,180])), (118,np.array([272,131]))]

# BAD_FRAMES_AND_POSITIONS = [(239, np.array([133,285])), (101, np.array([85,141])), (37,np.array([239,171])),
#                             (5,np.array([201,149]))]

# GRASS_FRAMES_AND_POSITIONS = [(5,np.array([201,149])), (5,np.array([242,213])), (5,np.array([231,200])),
#                               (5,np.array([79,81])), (107, np.array([378,235])), (98, np.array([359,221]))]




# IMAGE_BASENAME = 'test_data/video7/image_%d.png'
# CAMERA_BASENAME = 'output/scripts/preprocessing/data/video7/camera/camera_0_274/camera_%d.npy'

# FRAMES_AND_POSITIONS = [(10, np.array([197,197])), (80, np.array([231,171])), (239, np.array([165,140])),
#                         (20, np.array([247,202])), (30, np.array([282,207])), (19, np.array([242,201])),
#                         (15, np.array([222,199]))]

# GRASS_FRAMES_AND_POSITIONS = [(212, np.array([318,127]))]
# BAD_FRAMES_AND_POSITIONS = [(134, np.array([362, 232]))]

IMAGE_BASENAME = '/Users/daniel/Documents/soccer/images/images-8_15-9_31/image_%d.png'
CAMERA_BASENAME ='/Users/daniel/Documents/soccer/output/preprocessing/data/images-8_15-9_31/camera/camera_0_1874/camera_%d.npy'

FRAMES_AND_POSITIONS = [(239, np.array([273,165])), (233,np.array([224,147])), (230,np.array([201,143])),
                        (101,np.array([227,168])), (14,np.array([242,178])), (12,np.array([237,182])),
                        (114,np.array([266,140])), (113,np.array([263,140])), (35,np.array([234,170])),
                        (37,np.array([229,172])), (46,np.array([210,180])), (118,np.array([272,131])),
                        (107, np.array([245,151])), (975, np.array([121,111])), (975, np.array([122,110]))]

def load_absolute_soccer_field():
    '''
    Return a SoccerFieldGeometry object
    '''
    ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)
    SCALE = 6
    FIELD_WIDTH = absolute_image.shape[1]-1
    FIELD_LENGTH = absolute_image.shape[0]-1
    GRID_WIDTH = 20
    GRID_LENGTH = 14
    soccer_field = SoccerFieldGeometry(SCALE, FIELD_WIDTH, FIELD_LENGTH, GRID_WIDTH, GRID_LENGTH)

    return soccer_field

def load_image_and_camera(frame):
    '''
    frame - an integer
    return a tuple (image, camera from absolute coordinates)
    '''
    image_file = IMAGE_BASENAME % frame
    camera_file = CAMERA_BASENAME % frame
    
    image = cv2.imread(image_file, 1)
    camera_matrix = CameraMatrix(np.load(camera_file))

    return image, camera_matrix

def test_evaluate_position(image, camera_matrix, position_on_image):
    image_copy = image.copy()
    primitive_drawing.draw_point(image_copy, position_on_image, (255,0,0))
    cv2.imshow('image with position', image_copy)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    image_sat = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)[:,:,1]
    soccer_field = load_absolute_soccer_field()
    ball_radius = soccer_field.get_soccer_ball_radius()

    H = camera_matrix.get_homography_matrix()
    H = np.linalg.inv(H)
    print 'H:'
    print H
    print 'applying homography to: ', position_on_image
    ball_position = planar_geometry.apply_homography_to_point(H, position_on_image)
    ball_position = np.append(ball_position, 0).reshape((1,3))
    
    print 'position on field: ', ball_position
    
    ball_evaluator = GMMBallEvaluator()
    evaluation = ball_evaluator.evaluate_ball_position(image_sat, camera_matrix, ball_radius, ball_position)

    print 'evaluation: ', evaluation

    return

def test_evaluate_radius_sq_masks(image, camera_matrix, position_on_image):
    
    image_copy = image.copy()
    primitive_drawing.draw_point(image_copy, position_on_image, (255,0,0))
    cv2.imshow('image with position', image_copy)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    edges = cv2.Canny(image, 150, 200)

    cv2.imshow('edges before closing', np.float32(edges > 0))
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    kernel = np.ones((3,3))
    edges = cv2.morphologyEx(edges, cv2.MORPH_CLOSE, kernel)
    edge_mask = edges > 0

    cv2.imshow('edges', np.float32(edge_mask))
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    soccer_field = load_absolute_soccer_field()
    ball_radius = soccer_field.get_soccer_ball_radius()

    H = camera_matrix.get_homography_matrix()
    H = np.linalg.inv(H)

    ball_position = planar_geometry.apply_homography_to_point(H, position_on_image)
    ball_position = np.append(ball_position, 0)
    #ball_position = np.array([562.34881592, 135.37979126, 0.71667856])
    print 'ball position: ', ball_position
    
    ball_evaluator = EdgeBallEvaluator()
    rectangle, radius_sq_masks = ball_evaluator.compute_radius_sq_masks(camera_matrix, ball_radius, ball_position)

    print 'rectangle: ', rectangle

    inside_mask, outside_mask = radius_sq_masks
    m = np.int32(inside_mask.copy())
    m[outside_mask] = 2
    print 'inside=1, outside=2:'
    print m

    size = 0
    tx = np.arange(-size,size+1,1)
    ty = np.arange(-size,size+1,1)
    TX,TY = np.meshgrid(tx,ty)
    rectangle_translations = np.dstack([TX,TY])
    #rectangle_translations = np.vstack([np.array([x,x]) for x in range(0,1,1)])
    top_left_corners = rectangle.get_top_left() + rectangle_translations.reshape((-1,2))

    rectangles = Rectangles(rectangle.get_shape(), top_left_corners)

    for i, edge_slice in enumerate(rectangles.get_slices(edge_mask)):
        print 'edge slice %d:' % i
        print np.int32(edge_slice)

    evaluations = ball_evaluator.evaluate_radius_sq_masks(rectangles, radius_sq_masks, edge_mask)

    print 'evaluations: '
    print evaluations.reshape(rectangle_translations.shape[:2])

    print evaluations.reshape(rectangle_translations.shape[:2]) < -0.7
    
if __name__ == '__main__':
    np.set_printoptions(precision=3, linewidth=200)

    frame, position_on_image = FRAMES_AND_POSITIONS[13]
    #frame, position_on_image = BAD_FRAMES_AND_POSITIONS[0]
    #frame, position_on_image = GRASS_FRAMES_AND_POSITIONS[4]
    #frame = 35
    #position_on_image = np.array([302,208])
    image, camera_matrix = load_image_and_camera(frame)

    #test_evaluate_position(image, camera_matrix, position_on_image)
    test_evaluate_radius_sq_masks(image, camera_matrix, position_on_image)
