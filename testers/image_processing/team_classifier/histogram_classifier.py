import numpy as np
import cv2
import os
from sklearn.cluster import KMeans
from ....source.preprocessing import image_loader
import matplotlib.pyplot as plt

"""
Load a numpy array containing a list of histograms and cluster them into 2 or 3 clusters.
"""

#PREPROCESSING_NAME = 'images_50_55'
PREPROCESSING_NAME = 'images_50_269'

PLAYER_IMAGE_DIRECTORY = os.path.join('output/tester/team_classifier/player_histograms', PREPROCESSING_NAME, 'player_images')
HISTOGRAM_FILENAME = os.path.join('output/tester/team_classifier/player_histograms', PREPROCESSING_NAME,
                                  'histograms/histograms.npy')

LABELLED_PLAYERS_DIRECTORY = os.path.join('output/tester/team_classifier/player_histograms', PREPROCESSING_NAME,
                                          'labelled_players')
LABEL_DIRECTORY_BASENAME = 'label_%d'
PLAYER_IMAGE_BASENAME = 'image_%d.png'

N_CLUSTERS = 2

def classify_histograms(histogram_filename, player_image_directory, labelled_players_directory):
    '''
    histogram_filename - the name of a file storing a numpy array with shape (num histograms, num bins in histogram)
    player_image_directory - a directory containing the player images that generated the histograms
    labelled_player_directory - the directory where we store the labelled images.
    '''

    est = KMeans(n_clusters = N_CLUSTERS)

    histograms = np.load(histogram_filename)

    print 'loaded %d histograms' % len(histograms)
    print 'clustering histograms...'
    est.fit(histograms)
    print 'done.'

    #now save the player images in directories label_i, where i is the label of the players histogram
    label_directories = []
    for label in range(N_CLUSTERS):
        label_directory = os.path.join(LABELLED_PLAYERS_DIRECTORY, LABEL_DIRECTORY_BASENAME % label)
        label_directories.append(label_directory)
        os.mkdir(label_directory)

    #load the player images
    player_images = image_loader.load_images(player_image_directory)

    distances_to_center = []
    for player_index, player_image in enumerate(player_images):
        label = est.labels_[player_index]
        label_directory = label_directories[label]
        image_file = os.path.join(label_directory, PLAYER_IMAGE_BASENAME % player_index)
        
        print 'saving player %d in file %s' % (player_index, image_file)
        cv2.imwrite(image_file, player_image)

        distances_to_center.append(np.linalg.norm(histograms[player_index] - est.cluster_centers_[label]))

    #display a histogram of distances from the cluster centers
    distances_to_center = np.array(distances_to_center)
    plt.hist(distances_to_center, bins='auto')
    plt.show()
    
    return

if __name__ == '__main__':
    classify_histograms(HISTOGRAM_FILENAME, PLAYER_IMAGE_DIRECTORY, LABELLED_PLAYERS_DIRECTORY)
