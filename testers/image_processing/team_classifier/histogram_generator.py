import os
import pickle
import cv2
import numpy as np
import itertools
import matplotlib.pyplot as plt
from ....source.dynamics.particle_filter_fast.local_player_data_small import LocalPlayerDataSmall
from ....source.dynamics.particle_filter_fast.frame_data import PlayerCandidate, FrameData
from ....source.dynamics.particle_filter_fast.padded_frame_data import PaddedFrameData
from ....source.preprocessing import image_loader
from ....source.preprocessing.annotation_generator.annotation_generator import AnnotationGenerator
from ....source.preprocessing.absolute_homography_generator.absolute_homography_generator import AbsoluteHomographyGenerator
from ....source.preprocessing.camera_samples_interpolator.camera_samples_interpolator import CameraSamplesInterpolator
from ....source.preprocessing.players_generator.players_generator import PlayersGenerator
from ....source.auxillary.soccer_field_geometry import SoccerFieldGeometry
from ....source.auxillary import histogram_operations, planar_geometry
from ...dynamics.particle_filter_fast.preprocessing_data_loader import load_frame_data_iterator

##########
# In this script we load preprocessing data and feed it to the PlayerTracker,
# We then use player_tracker_visualization to output a visualization of the algorithm at work.
#########

IMAGE_DIRECTORY = 'test_data/video4'
PREPROCESSING_NAME = 'images_50_269'

# IMAGE_DIRECTORY = 'test_data/video5'
# PREPROCESSING_NAME = 'images_50_55'

DATA_DIRECTORY = 'output/scripts/preprocessing/data'

ANNOTATION_DIRECTORY = os.path.join(DATA_DIRECTORY, PREPROCESSING_NAME, 'annotation')
PLAYERS_DIRECTORY = os.path.join(DATA_DIRECTORY, PREPROCESSING_NAME, 'players')
CAMERA_DIRECTORY = os.path.join(DATA_DIRECTORY, PREPROCESSING_NAME, 'camera')

HISTOGRAM_FILENAME = os.path.join('output/tester/team_classifier/player_histograms', PREPROCESSING_NAME,
                                  'histograms/histograms.npy')
LOCAL_IMAGE_DIRECTORY = os.path.join('output/tester/team_classifier/player_histograms', PREPROCESSING_NAME, 'player_images')
LOCAL_IMAGE_BASENAME = 'image_%d.png'
HISTOGRAM_IMAGE_DIRECTORY = os.path.join('output/tester/team_classifier/player_histograms', PREPROCESSING_NAME,
                                         'histogram_images')
HISTOGRAM_IMAGE_BASENAME = 'histogram_%d.png'

PADDING = 50

"""
This file is used to generate histograms of players from preprocessing data.
It loads the preprocessing data, calculates histograms of the players, and stores the result in a numpy array.
It also stores the image patches used to create the histograms in a seperate directory.
"""

def load_absolute_soccer_field():
    '''
    Return a tuple (soccer field, absolute image)
    '''
    ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)
    SCALE = 6
    FIELD_WIDTH = absolute_image.shape[1]-1
    FIELD_LENGTH = absolute_image.shape[0]-1
    GRID_WIDTH = 20
    GRID_LENGTH = 14
    soccer_field = SoccerFieldGeometry(SCALE, FIELD_WIDTH, FIELD_LENGTH, GRID_WIDTH, GRID_LENGTH)

    return soccer_field

def build_local_datas_from_frame_data(soccer_rectangle, frame_data):
    '''
    FrameData - a PaddedFrameData object
    
    Return a list of LocalPlayerData object which represent the players in the frame data.
    '''
    positions = [pc.absolute_position for pc in frame_data.get_player_candidates()]

    #print 'positions: ', positions
    
    local_datas = [LocalPlayerDataSmall.from_frame_and_positions(soccer_rectangle, frame_data, position.reshape((1,2)))
                   for position in positions]

    return local_datas

def load_local_player_data_iterator(image_directory, annotation_directory, camera_directory, players_directory):
    '''
    Return an iterator of LocalPlayerDataSmall objects.
    '''

    #load the frame data
    frame_data_iterator = load_frame_data_iterator(image_directory, annotation_directory, camera_directory, players_directory)

    #use only the first interval
    interval, frame_data_in_interval = next(frame_data_iterator)

    frame_data_in_interval = (PaddedFrameData.from_frame_data(fd, PADDING) for fd in frame_data_in_interval)
    soccer_field = load_absolute_soccer_field()
    soccer_rectangle = soccer_field.get_field_rectangle()
    
    return itertools.chain.from_iterable((build_local_datas_from_frame_data(soccer_rectangle, fd)
                                          for fd in frame_data_in_interval))

def generate_local_player_histogram(player_index, local_player_data, histogram_image_directory, local_image_directory):
    '''
    player_index - an integer
    local_player_data - a LocalPlayerDataSmall object
    local_image_directory - a directory where we save the image patch corresponding to this local player data object.

    return a numpy array with length (number of bins).
    '''
    #print 'generating histogram %d...' % player_index
    image = local_player_data._frame_data.get_image()
    player_rectangle = local_player_data._player_rectangles.get_rectangle_objects()[0]
    foreground_mask = local_player_data._foreground_masks[0]
    player_mask = local_player_data._player_mask

    local_image = player_rectangle.get_slice(image)
    local_mask = np.float32(foreground_mask * player_mask)

    # cv2.imshow('local image', local_image)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    # cv2.imshow('local mask', local_mask)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    local_histogram = histogram_operations.get_normalized_hs_histogram(local_image, local_mask, nbins=(10,10))

    #before returning the histogram, save the local image
    local_image[np.logical_not(local_mask)] = 0
    local_image_file = os.path.join(local_image_directory, LOCAL_IMAGE_BASENAME % player_index)
    print 'saving local image to: ', local_image_file
    cv2.imwrite(local_image_file, local_image)

    #and an image of the histogram itself
    histogram_file = os.path.join(histogram_image_directory, HISTOGRAM_IMAGE_BASENAME % player_index)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.imshow(local_histogram, interpolation='nearest', aspect='auto')
    fig.savefig(histogram_file, bbox_inches='tight')
    fig.clf()
    plt.close()
        
    return local_histogram.ravel()

def generate_histograms(histogram_filename,  histogram_image_directory, local_image_directory,
                        image_directory, annotation_directory, camera_directory, players_directory):
    '''
    histgoram_directory - the directory where we save the histograms
    image_patch_directory - the directory where we save the image patches that are used to create the histograms
    image_directory - directory with the frame images
    annotation_directory - directory with the image annotations
    camera_directory - directory storing the cameras
    player_directory - directory storing the players

    Load the players from the players directory and for each player, extract a patch from the image that it is in.
    Save this patch in image_patch_directory and use it to create a histogram.

    At the end, we save all of the histograms in the histogram directory.
    '''

    #build an iterator of LocalPlayerDataSmall objects
    local_player_datas = (load_local_player_data_iterator(image_directory, annotation_directory,
                                                          camera_directory, players_directory))

    local_histograms = np.vstack(generate_local_player_histogram(i, lpd, histogram_image_directory, local_image_directory)
                                 for i,lpd in enumerate(local_player_datas))

    np.save(histogram_filename, local_histograms)

    return

if __name__ == '__main__':
    
    generate_histograms(HISTOGRAM_FILENAME, HISTOGRAM_IMAGE_DIRECTORY, LOCAL_IMAGE_DIRECTORY,
                        IMAGE_DIRECTORY, ANNOTATION_DIRECTORY, CAMERA_DIRECTORY, PLAYERS_DIRECTORY)
    
