import numpy as np
import cv2
from ....source.image_processing.sidelines_extractor.gmm_sidelines_extractor import GMMSidelinesExtractor
from ....source.image_processing.contour_generator.edges_contour_generator import EdgesContourGenerator


###########
# This is a tester for code/source/image_processing/sidelines_extractor/gmm_sidelines_extractor
#
# We generate an image containing a solid line with a square of noise on top of it. The GMMSidelineGenerator
# should be able to pick out the solid line.
###########

def generate_test_image():
    '''
    We create a thin blue horizontal line, and a noisy rectangle in the middle of it.
    '''

    image = np.zeros((400, 600, 3), np.uint8)

    #draw a long blue rectangle
    pt1 = (10, 200)
    pt2 = (590, 205)
    thickness = -1 # filled in
    color = (255, 0, 0)
    cv2.rectangle(image, pt1, pt2, color, thickness)

    #now make a noisy square
    square = np.zeros((20, 20, 3), np.uint8)
    mean = np.array([100, 100, 200])
    stddev = np.array([20, 20, 20])
    cv2.randn(square, mean, stddev)
    image[190:210, 290:310, :] = square 

    return image

def test():

    image = generate_test_image()

    print 'displaying the test image'
    
    cv2.imshow('test image', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    print 'generating the contours'

    contourGenerator = EdgesContourGenerator()
    contours = contourGenerator.generate_contours(image)

    #get the mask in which the sideline could lie
    has_sidelines_mask = contours[0].get_outer_mask()
    
    print 'displaying the possible sidelines mask'
    cv2.imshow('possible sidelines mask', has_sidelines_mask)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    print 'extracting the sidelines'
    sidelines_extractor = GMMSidelinesExtractor()
    sidelines_mask = sidelines_extractor.get_sidelines_mask(image, has_sidelines_mask)

    print 'displaying the sidelines mask'
    cv2.imshow('sidelines mask', sidelines_mask)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


if __name__ == '__main__':

    test()
