from .soccer_field_embedding_controller.shape_graph_embedding_controller import ShapeGraphEmbeddingController
from .preprocessing_data_type import PreprocessingDataType
import os
import numpy as np
import cv2

#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts'
#IMAGE_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/test_data/'

# OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
# IMAGE_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/images'

OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'

#INPUT_DATA_NAME = 'images-8_15-9_31'
#INPUT_DATA_NAME = 'images-0_00-1_00'
INPUT_DATA_NAME = 'images-0_00-10_00'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_full', 'data', INPUT_DATA_NAME)

IMAGE_DIRECTORY              = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'image')
ANNOTATION_DIRECTORY         = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'annotation')
HIGHLIGHTED_SHAPES_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'highlighted_shapes')
EMBEDDING_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'soccer_field_embedding')

PD_DIRECTORY_DICT = {PreprocessingDataType.IMAGE:              IMAGE_DIRECTORY,
                     PreprocessingDataType.ANNOTATION:         ANNOTATION_DIRECTORY,
                     PreprocessingDataType.HIGHLIGHTED_SHAPES: HIGHLIGHTED_SHAPES_DIRECTORY}

NUM_PROC = 8

#parameters for the embedding generator
MIN_LINES_IN_CORRESPONDENCE = 5
HIGHLIGHTED_ELLIPSE_THRESHOLD = 200
LINE_SEGMENT_THRESHOLD = 10
ELLIPSE_SEGMENT_THRESHOLD = 2
PARALLEL_LINES_THRESHOLD = 1 - np.cos(np.pi / 10)
IWASAWA_THRESHOLD = 0.2
CORRESPONDENCE_THRESHOLD = 0.1

#the absolute image that we compute the homography to
ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
PIXELS_PER_YARD = 6
absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)

def generate_data(output_parent_directory, pd_directory_dict):
    '''
    output_parent_directory - the directory in which we will save the annotations
    pd_directory_dict - a dictionary with items (PreprocessingDataType attr, directory name
    '''
    if not os.path.isdir(output_parent_directory):
        print 'making directory: ', output_parent_directory
        os.mkdir(output_parent_directory)

    embedding_controller = ShapeGraphEmbeddingController(NUM_PROC, absolute_image, PIXELS_PER_YARD,
                                                         MIN_LINES_IN_CORRESPONDENCE,
                                                         HIGHLIGHTED_ELLIPSE_THRESHOLD,
                                                         LINE_SEGMENT_THRESHOLD, ELLIPSE_SEGMENT_THRESHOLD,
                                                         PARALLEL_LINES_THRESHOLD,
                                                         CORRESPONDENCE_THRESHOLD, IWASAWA_THRESHOLD)
    
    embedding_controller.generate_data(output_parent_directory, pd_directory_dict)

    return

if __name__ == '__main__':
    generate_data(EMBEDDING_DIRECTORY, PD_DIRECTORY_DICT)
