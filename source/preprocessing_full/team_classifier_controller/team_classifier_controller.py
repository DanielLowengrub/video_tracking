import os
import itertools
import numpy as np
import pickle
from sklearn import metrics
from sklearn.cluster import KMeans

class TeamClassifierController(object):
    '''
    The job of a team classifier is to determine which players are on the same team and which are on different teams.
    This class provides a method for loading the player histograms and fitting a KMeans clusterer which divides them into
    two clusters.

    We store the resulting classifier, and for each label,
    we store the mean distance of a histogram in that cluster to the center, as well as the std.
    '''

    CLASSIFIER_BASENAME = 'kmeans_classifier.pckl'
    MEAN_STD_BASENAME = 'mean_std_distances.npy'
    
    def __init__(self, n_clusters=2):
        '''
        num_clusters - the number of clusters we try to find in the player histograms. This should be 2 if we exclude
        refs, and 3 if we include them.
        '''
        self._n_clusters = n_clusters

    def _compute_mean_std_distances(self, est, player_histograms):
        '''
        est - a KMeans classifier which has been fitted to player_histograms
        player_histograms - a numpy array with shape (num histograms, num bins)

        return a numpy array with shape (number of clusters, 2)
        The i-th row stores (mean distance to cluster center, std of distances to cluster center)
        '''

        mean_std_distances = []
        histogram_labels = est.labels_
        
        for label in range(self._n_clusters):
            histograms_in_cluster = player_histograms[histogram_labels == label]
            cluster_center = est.cluster_centers_[label]
            distances_to_center = np.linalg.norm(histograms_in_cluster - cluster_center, axis=1)
            mean_std_distances.append(np.array([distances_to_center.mean(), distances_to_center.std()]))

        return np.vstack(mean_std_distances)
    
    def _load_player_histograms(self, pd_directory_dict):
        '''
        preprocessing_directory_dict - a dictionary with items (PreprocessingDataType property, directory name)
        '''
        from ..preprocessing_data_loader import PreprocessingDataLoader
        interval_data_iter = PreprocessingDataLoader.load_data(pd_directory_dict)
        
        #extract the histograms
        histograms_in_intervals = ((frame_data.build_data().player_histograms for frame_data in interval_data)
                                   for interval_data in interval_data_iter)

        
        #concatenate all intervals and stack them into an array
        player_histograms = itertools.chain.from_iterable(histograms_in_intervals)
        player_histograms = np.vstack(list(player_histograms))

        return player_histograms

    @classmethod
    def load_classifier(cls, classifier_directory):
        '''
        classifier_directory - a directory containing two files: kmeans_classifier.pckl and mean_std_distances.npy

        return a tuple (kmeans_classifier, mean_std_distances)
        kmeans_classifier - a KMeans object
        mean_std_distances - a np array with shape (num clusters, 2)
        '''
        kmeans_file = os.path.join(classifier_directory, cls.CLASSIFIER_BASENAME)
        mean_std_file = os.path.join(classifier_directory, cls.MEAN_STD_BASENAME)
        
        with open(kmeans_file, 'rb') as f:
            kmeans_classifier = pickle.load(f)
        
        mean_std_distances = np.load(mean_std_file)

        return kmeans_classifier, mean_std_distances
        
    def generate_classifier(self, classifier_directory, pd_directory_dict):
        '''
        preprocessing_directory_dict - a dictionary with items (PreprocessingDataType property, directory name)
        classifier_directory - a directory name.

        We fit the KMeans classifier to the data and save it as classifier_directory/kmeans_model.pckl.
        We also store a file classifier_directory/mean_and_std_distances.npy. The i-th row contains two numbers
        (mean distance of elem of cluster i to the center, std of distances of elem of cluster i to center)
        '''
        
        player_histograms = self._load_player_histograms(pd_directory_dict)

        print 'loaded %d histograms.' % len(player_histograms)
        
        #cluster them with k means
        est = KMeans(n_clusters=self._n_clusters)

        print 'clustering histograms...'
        est.fit(player_histograms)
        score = metrics.silhouette_score(player_histograms, est.labels_, metric='euclidean')

        print 'finished with a score of (best=1, worst=-1):', score

        #save the estimator
        est_file = os.path.join(classifier_directory, self.CLASSIFIER_BASENAME)
        with open(est_file, 'wb') as f:
            pickle.dump(est, f)

        #save the mean/std distances
        mean_std_distances = self._compute_mean_std_distances(est, player_histograms)
        mean_std_file = os.path.join(classifier_directory, self.MEAN_STD_BASENAME)
        np.save(mean_std_file, mean_std_distances)
        
        return
        
