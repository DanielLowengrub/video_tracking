from .annotation_controller.tracking_subsequence_controller import TrackingSubsequenceController
import os
import numpy as np
import cv2

#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts'
#IMAGE_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/test_data/'

# OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'

INPUT_DATA_NAME = 'images-8_15-9_31'
#INPUT_DATA_NAME = 'images-0_00-1_00'
#INPUT_DATA_NAME = 'images-0_00-10_00'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_full_0', 'data', INPUT_DATA_NAME)

#ANNOTATION_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'stage_0', 'annotation')
#OUTPUT_PARENT_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'stage_1', 'annotation')
ANNOTATION_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'stage_0', 'field_mask')
OUTPUT_PARENT_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'stage_1', 'field_mask')

NUM_PROC = 8

GRASS_THRESHOLD = 0.4
MIN_SUBSEQUENCE_LENGTH = 3*25

def generate_data(output_parent_directory, annotation_directory):
    '''
    output_parent_directory - the directory in which we will save the new annotations
    annotation_directory - the directory containing the old annotations (i.e, even ones that can not be used for tracking
    '''
    if not os.path.isdir(output_parent_directory):
        print 'making directory: ', output_parent_directory
        os.mkdir(output_parent_directory)

    ts_controller = TrackingSubsequenceController(NUM_PROC, GRASS_THRESHOLD, MIN_SUBSEQUENCE_LENGTH)
    ts_controller.generate_data(output_parent_directory, annotation_directory)

    return

if __name__ == '__main__':
    generate_data(OUTPUT_PARENT_DIRECTORY, ANNOTATION_DIRECTORY)
