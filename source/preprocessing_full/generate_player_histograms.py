from .player_histograms_controller.player_histograms_controller import PlayerHistogramsController
from .preprocessing_data_type import PreprocessingDataType
import os
import numpy as np
import cv2

#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts'
#IMAGE_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/test_data/'

OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'

#INPUT_DATA_NAME = 'images-8_15-9_31'
#INPUT_DATA_NAME = 'images-0_00-1_00'
INPUT_DATA_NAME = 'images-0_00-10_00'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_full', 'data', INPUT_DATA_NAME)

IMAGE_DIRECTORY      = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'image')
ANNOTATION_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'annotation')
CAMERA_DIRECTORY     = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'camera')
PLAYERS_DIRECTORY    = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'players')

PLAYER_HISTOGRAMS_DIRECTORY    = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'player_histograms')

PD_DIRECTORY_DICT = {PreprocessingDataType.IMAGE:      IMAGE_DIRECTORY,
                     PreprocessingDataType.ANNOTATION: ANNOTATION_DIRECTORY,
                     PreprocessingDataType.CAMERA:     CAMERA_DIRECTORY,
                     PreprocessingDataType.PLAYERS:    PLAYERS_DIRECTORY}

NUM_PROC = 8

def generate_data(output_parent_directory, pd_directory_dict):
    '''
    output_parent_directory - the directory in which we will save the annotations
    pd_directory_dict - a dictionary with items (PreprocessingDataType attr, directory name
    '''
    if not os.path.isdir(output_parent_directory):
        print 'making directory: ', output_parent_directory
        os.mkdir(output_parent_directory)

    player_histograms_controller = PlayerHistogramsController(NUM_PROC)
    player_histograms_controller.generate_data(output_parent_directory, pd_directory_dict)

    return

if __name__ == '__main__':
    generate_data(PLAYER_HISTOGRAMS_DIRECTORY, PD_DIRECTORY_DICT)
