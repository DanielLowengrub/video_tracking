import numpy as np
from ...image_processing.contour_generator.edges_contour_generator import EdgesContourGenerator
from .hough_highlighted_shapes_controller import HoughHighlightedShapesController
from ...image_processing.image_annotator.soccer_annotation import SoccerAnnotation
import itertools
import cv2

class ContourHoughController(HoughHighlightedShapesController):
    '''
    This implementation of HighlightedShapesController finds lines via a hough transform.
    '''
    def __init__(self, num_processors,
                 canny_min, canny_max,
                 line_closing_radius, ellipse_closing_radius,
                 num_theta_bins, num_radius_bins, mask_shape, peak_threshold, peak_threshold_ratio, clustering_radius,
                 max_theta_std, max_radius_std, min_long_line_length,
                 min_points_in_ellipse,
                 ransac_min_points_to_fit, ransac_num_iterations, ransac_distance_to_shape_threshold,
                 ransac_min_percentage_in_shape, max_average_ellipse_residue,
                 thickness,
                 min_interval_on_line_length, min_interval_on_ellipse_length,
                 min_total_line_length, min_ellipse_axis, min_total_ellipse_length):
        
        super(ContourHoughController, self).__init__(num_processors,
                                                     line_closing_radius, ellipse_closing_radius,
                                                     num_theta_bins, num_radius_bins, mask_shape,
                                                     peak_threshold, peak_threshold_ratio, clustering_radius,
                                                     max_theta_std, max_radius_std, min_long_line_length,
                                                     min_points_in_ellipse,
                                                     ransac_min_points_to_fit, ransac_num_iterations,
                                                     ransac_distance_to_shape_threshold, ransac_min_percentage_in_shape,
                                                     max_average_ellipse_residue,
                                                     thickness,
                                                     min_interval_on_line_length, min_interval_on_ellipse_length,
                                                     min_total_line_length, min_ellipse_axis, min_total_ellipse_length)

        #we use this to look for contours in an image
        self._edges_contour_generator = EdgesContourGenerator(canny_min, canny_max)

    def _find_sidelines_and_foreground(self, image, annotation):
        base_contours, sidelines_mask = self._edges_contour_generator.generate_contours(image,
                                                                                        annotation.get_soccer_field_mask())
        # sidelines_mask = reduce(np.logical_or, (contour.get_outer_mask()>0 for contour in base_contours[1:]),
        #                         base_contours[0].get_outer_mask()>0)
        
        return sidelines_mask, sidelines_mask, base_contours
