import numpy as np
from ..sequential_visualization_controller import SequentialVisualizationController
from ...auxillary import mask_operations
from ....testers.auxillary import primitive_drawing

class HighlightedShapesVisualizationController(SequentialVisualizationController):
    '''
    This is a SVC that generates visualizations of highlighted shapes.
    '''

    def __init__(self, num_processors, shape_color, highlighted_color, highlighted_alpha):
        '''
        num_processors - the number of available processors
        shape_color - the color of the shapes
        highlighted_color - the color of the highighted region of the shapes
        highlighted_alpha - the alpha of the highlighted region of the shapes
        '''
        super(HighlightedShapesVisualizationController, self).__init__(num_processors)

        self._shape_color = shape_color
        self._highlighted_color = highlighted_color
        self._highlighted_alpha = highlighted_alpha

    def _draw_highlighted_shapes(self, image, highlighted_shapes):
        '''
        image - a numpy array with shape (n,m,3) and type np.uint8
        highlighted_shapes - a list of HighlightedShape objects
        
        draw the highlighted shapes on the image
        '''
        #first draw the shapes on the image
        for hs in highlighted_shapes:
            primitive_drawing.draw_shape(image, hs.get_shape(), self._shape_color)

        #now draw the highlighted regions
        for hs in highlighted_shapes:
            mask = hs.generate_mask(image)
            mask_operations.draw_mask(image, mask, np.array(self._highlighted_color), self._highlighted_alpha)

        return
    
    def _build_frame_data_visualization(self, frame_data):
        '''
        frame_data - a FrameDataTuple object which provides access to an image and highlighted shapes
        
        return - an image which represents a visualization of the data
        '''
        #build a PreprocessingDataTuple object. It is assumed to contain an image and an annotation
        pd_tuple = frame_data.build_data()
        image = pd_tuple.image
        highlighted_shapes = pd_tuple.highlighted_shapes

        self._draw_highlighted_shapes(image, highlighted_shapes)

        return image
        
        
        
