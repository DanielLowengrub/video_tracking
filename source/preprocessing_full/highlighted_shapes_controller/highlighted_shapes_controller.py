import numpy as np
import os
import re
from ..sequential_pd_controller import SequentialPDController
from ..frame_data.frame_data_loader import FrameDataLoader
from ...auxillary.contour_shape_overlap.highlighted_shape import HighlightedShape
from ...preprocessing_full import filename_loader

class HighlightedShapesController(SequentialPDController):
    '''
    This is a SequentialPDController that deals with generating and loading HighilghtedShapes objects in each frame.
    This baseclass only deals with the general process of saving and loading HighlightedShapes. The way in which the shapes
    are computed is implemented in the subclasses.

    Specifically, all subclasses must implement the method _find_highlighted_shapes.
    '''

    #This dictionary determines the directory name of each type of shape
    SHAPE_TYPE_TO_DIRECTORY = {HighlightedShape.LINE:'highlighted_line', HighlightedShape.ELLIPSE:'highlighted_ellipse'}

    #This records the shape type that is stored in each kind of directory
    DIRECTORY_TO_SHAPE_TYPE = dict((shape_dir, shape_type) for shape_type,shape_dir in SHAPE_TYPE_TO_DIRECTORY.iteritems())

    #we use this regex to find the highlighted shape directories in a directory that contains highlighted shapes
    #We search for strings of the form highlighted_line_i or highlighted_ellipse_i where i is an integer
    HIGHLIGHTED_SHAPE_LOAD_DIRECTORY_REGEX = re.compile('(?:%s)_([0-9]+)' % '|'.join(SHAPE_TYPE_TO_DIRECTORY.values()))

    #use this regex to determine if a directory contains a line or an ellipse
    HIGHLIGHTED_SHAPE_TYPE_DIRECTORY_REGEX = re.compile('(%s)_[0-9]+' % '|'.join(SHAPE_TYPE_TO_DIRECTORY.values()))

    def __init__(self, num_processors):
        super(HighlightedShapesController, self).__init__(num_processors)
        
    @classmethod
    def _get_highlighted_shape_directory(cls, frame_dir, highlighted_shape_index, highlighted_shape):
        '''
        frame_dir - the name of a directory containing the highlighted shapes in a given frame
        highlighted_shape_index - the index of the shape in the list of highlighted shapes of this frame
        highlighted_shape - a HighlightedShape object

        return - the name of the directory in which we should store the highlighted shape
        '''
        highlighted_shape_dir = '%s_%d' % (cls.SHAPE_TYPE_TO_DIRECTORY[highlighted_shape.get_shape_type()],
                                           highlighted_shape_index)
        
        highlighted_shape_dir = os.path.join(frame_dir, highlighted_shape_dir)
        
        return highlighted_shape_dir

    def _save_data_in_frame_dir(self, frame_dir, highlighted_shapes):
        '''
        frame_dir - a frame directory name
        highlighted_shapes - a list of HighlightedShape objects

        save the highlighted shapes in the given directory
        '''
        for i, highlighted_shape in enumerate(highlighted_shapes):
            highlighted_shape_directory = self._get_highlighted_shape_directory(frame_dir, i, highlighted_shape)
            os.mkdir(highlighted_shape_directory)
            
            #print '   saving highlighted shape %d to: %s' % (i, highlighted_shape_directory)

            highlighted_shape.save_to_directory(highlighted_shape_directory)
            
        return

    @classmethod
    def _get_highlighted_shape_directory_type(cls, highlighted_shape_directory):
        '''
        highlighted_shape_directory - a directory storing a highlighted shape
        Return HighlightedShape.LINE or HighlightedShape.ELLIPSE depending on the directory name
        '''
        match = cls.HIGHLIGHTED_SHAPE_TYPE_DIRECTORY_REGEX.match(highlighted_shape_directory)
        
        if match is None:
            return None

        shape_string = match.groups()[0]

        if shape_string in cls.DIRECTORY_TO_SHAPE_TYPE:
            return cls.DIRECTORY_TO_SHAPE_TYPE[shape_string]

        else:
            return None

        return
    
    @classmethod
    def load_data_from_frame_dir(cls, frame_dir):
        '''
        frame_dir - a directory name
        return - a list of HighlightedShape objects
        '''
        enumerated_highlighted_shape_directories = filename_loader.load_filenames(frame_dir,
                                                                                  cls.HIGHLIGHTED_SHAPE_LOAD_DIRECTORY_REGEX)
        highlighted_shapes = []
        
        for hs_index,hs_directory in enumerated_highlighted_shape_directories:
            #print '   loading highlighted shape from: ', hs_directory
            hs_basename = os.path.basename(hs_directory)
            #print '   shape directory basename: ', hs_basename
            
            shape_type = cls._get_highlighted_shape_directory_type(hs_basename)
            #print '   shape type: ', shape_type
            
            if shape_type is None:
                continue
        
            highlighted_shape = HighlightedShape.load_from_directory(hs_directory, shape_type)
            highlighted_shapes.append(highlighted_shape)

        return highlighted_shapes

    def _find_highlighted_shapes(self, image, annotation):
        '''
        image - a numpy array with shape (n,m,3) and type np.uint8
        annotation - a SoccerAnnotation object

        return a list of HighlightedShape objects
        '''
        raise NotImplementedError('_find_highlighted_shapes has not been implemented')
    
    def _build_data_from_pd_tuple(self, pd_tuple):
        '''
        pd_tuple - a PreprocessingDataTuple which stores an image and an annotation
        return - a list of HighlightedShape objects
        '''
        return self._find_highlighted_shapes(pd_tuple.image, pd_tuple.annotation)
