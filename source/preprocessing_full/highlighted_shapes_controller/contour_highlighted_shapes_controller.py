import numpy as np
import itertools
from ...auxillary.contour_shape_overlap.highlighted_shape_generator import HighlightedShapeGenerator
from ...auxillary.contour_shape_overlap.highlighted_shape import HighlightedShape
from ...image_processing.hough_transform.line_detector import LineDetector
from ...image_processing.contour_generator.edges_contour_generator import EdgesContourGenerator
from ...auxillary.contour_shape_overlap.line_with_position import LineWithPosition
from ...auxillary.contour_shape_overlap.ellipse_with_position import EllipseWithPosition
from ...image_processing.hough_transform.single_ellipse_detector import SingleEllipseDetector
from .highlighted_shapes_controller import HighlightedShapesController

class ContourHighlightedShapesController(HighlightedShapesController):
    '''
    This implementation of HighlightedShapesController finds shapes by fitting shapes to contour lines.
    '''
    def __init__(self, num_processors,
                 min_tangent_to_line_arc_length, max_tangent_to_line_residue,
                 theta_bin_width, radius_bin_width,
                 max_theta_deviation, max_radius_deviation,
                 min_line_arc_length, max_average_line_residue,
                 min_tangent_to_ellipse_arc_length, max_tangent_to_ellipse_geometric_res, min_tangent_to_ellipse_area,
                 ransac_min_points_to_fit, ransac_num_iterations, ransac_distance_to_shape_threshold,
                 ransac_min_percentage_in_shape,
                 max_average_ellipse_residue,
                 thickness,
                 min_interval_on_line_length, min_interval_on_ellipse_length,
                 min_ellipse_axis, min_total_ellipse_length):
        '''
        min_tangent_to_line_arc_length - the minimum arc length that is needed to determine a tangent vector to a line
        min_tangent_to_line_residue - the minimum algebraic residue that is allowed when fitting a tangent vector

        theta_bin_width - the width of the bins we use to discritize the angles of the lines
        radius_bin_width - the width of the bins we use to discritize the radii of the lines
        max_theta_deviation - the maximum std that we allow among the angles of tangent vectors that are on the same line. 
           This should be given in the units of "bins"
        max_radius_deviation - " " radii " "

        min_line_arc_length - the minimum arc length required for a sequence of points to be considered on a line
        min_average_line_residue - we use this to test if a list of points lie on a line.

        min_tangent_to_ellipse_arc_length - the minimum arc length that is needed to determine a tangent vector to an ellipse
        max_tanget_to_ellipse_algebraic_residue -  the minimum algebraic residue that is allowed when fitting a tangent vector
        min_tangent_to_ellipse_area - when we fit an ellipse to a neighborhood of a points to compute the tangent to that 
           point, we throw out ellipses with less area than this
        
        ransac_min_points_to_fit - the smallest number of points needed to fit an ellipse with the ransac algorithm
        ransac_num_iterations - the number of iterations in the ransac algorithm
        ransac_distance_to_shape_threshold - the threshold for considering a point to lie on an ellipse during the ransac 
           algorithm
        ransac_min_percentage_in_shape - the threshold for declaring a shape to fit a collection of points during the ransac 
           algorithm

        max_average_ellipse_residue - the maximum residue that is allowed when fitting points to an ellipse

        thickness - the thickness of the highighted lines (in pixels)
        
        min_interval_on_line_length - we throw out intervals on a line whose length is less than this
        min_interval_on_ellipse_length - we throw out intervals on an ellipse whose length is less than this
        min_ellipse_axis - throw out ellipses that have an axis whose length is less than this
        min_total_ellipse_length - throw out ellipses whose sum of interval lengths is less than this
        '''
        super(ContourHighlightedShapesController, self).__init__(num_processors)
        
        #we use this to look for contours in an image
        self._contour_generator = EdgesContourGenerator()

        #we use this to find lines in a list of contours
        self._line_detector = LineDetector(min_tangent_to_line_arc_length, max_tangent_to_line_residue,
                                           theta_bin_width, radius_bin_width, max_theta_deviation, max_radius_deviation,
                                           min_line_arc_length, max_average_line_residue)

        #we use this to look for an ellipse in a list of contours
        self._ellipse_detector = SingleEllipseDetector(min_tangent_to_line_arc_length, max_tangent_to_line_residue,
                                                       min_tangent_to_ellipse_arc_length, max_tangent_to_ellipse_geometric_res,
                                                       min_tangent_to_ellipse_area,
                                                       ransac_min_points_to_fit, ransac_num_iterations,
                                                       ransac_distance_to_shape_threshold,
                                                       ransac_min_percentage_in_shape,
                                                       max_average_ellipse_residue)

        #we use this to highlight the parts of a shape that overlap with a list of contours
        self._hs_generator = HighlightedShapeGenerator()

        #this is the thickness of the highlighted parts of a shape
        self._thickness = thickness
        self._min_interval_on_line_length = min_interval_on_line_length
        self._min_interval_on_ellipse_length = min_interval_on_ellipse_length
        self._min_ellipse_axis = min_ellipse_axis
        self._min_total_ellipse_length = min_total_ellipse_length

    def _highlighted_ellipse_is_degenerate(self, highlighted_ellipse):
        '''
        highlighted_ellipse - a HighlightedShape object

        return True iff the highlighted ellipse is degenerate
        '''
        total_length = highlighted_ellipse.highlighted_arc_length()

        center, axes, angle = highlighted_ellipse.get_shape().get_geometric_parameters()

        is_degenerate = (total_length < self._min_total_ellipse_length) or np.any(axes < self._min_ellipse_axis)

        #print '   total length: %f, axes: %s. is degenerate: %s' % (total_length, axes, is_degenerate)
        
        return is_degenerate
    
    def _find_highlighted_ellipses(self, base_contour_components):
        '''
        base_contour_components - a list of lists of Contour object. 
           Each list of Contour objects corresponds to a connected component of an image.
           Note that there can be more than one base contour in a component because the 
           connected components are calculated before we start removing shapes from the image.

        Return a list of HighlightedEllipse objects. We try to fit an ellipse to each conencted component and 
        return the ellipses we find.
        '''
        highlighted_ellipses = []

        #look for an ellipse in each of the connected components
        for base_contour_component in base_contour_components:
            #use the child contours as well as the base contours
            all_contours_in_component = list(itertools.chain(*([base_contour] + base_contour.get_child_contours()
                                                               for base_contour in base_contour_component)))

            
            #try to fit an ellipse to all of the contours in this connected component
            fitted_ellipse = self._ellipse_detector.find_ellipse(all_contours_in_component)

            #if we succeeded, use this ellipse to create a highlighted shape
            if fitted_ellipse is not None:
                fitted_ellipse.compute_geometric_parameters()
                ellipse_with_position = EllipseWithPosition(fitted_ellipse)
                highlighted_ellipse, contour_masks = self._hs_generator.generate_highlighted_shape(ellipse_with_position,
                                                                                                   self._thickness,
                                                                                                   all_contours_in_component)

                #add the highlighted ellipse if it isn't degenerate
                if not self._highlighted_ellipse_is_degenerate(highlighted_ellipse):
                    highlighted_ellipses.append(highlighted_ellipse)

        return highlighted_ellipses
    
    def _find_highlighted_shapes(self, image, annotation):
        '''
        image - a numpy array of shape (n,m,3) and type np.uint8
        annotation - a SoccerAnnotation object

        Return a list of HighlightedShape objects.
        '''
        
        #find the contours in part of the image corresponding to the soccer field
        #the base contours are the outer contours. A base contour may contain child contours.
        #For example, if there is an annulus in the image, then the outer circle will be a base contour,
        #and the inner circle will be it's child.
        base_contours = self._contour_generator.generate_contours(image, annotation.get_soccer_field_mask())
        base_contours_with_children = [([base_contour] + base_contour.get_child_contours()) for base_contour in base_contours]
        all_contours = list(itertools.chain(*(base_contour_with_children
                                              for base_contour_with_children in base_contours_with_children)))

        #find lines in the list of contours
        #print '   looking for lines in the contours...'
        lines = self._line_detector.get_lines(image, all_contours)
        lines_with_position = [LineWithPosition(line) for line in lines]

        #highlight the parts of each line that overlap with the contours
        #print '   highlighting the lines...'        
        highlighted_lines_and_contour_masks = [self._hs_generator.generate_highlighted_shape(l, self._thickness, all_contours)
                                               for l in lines_with_position]
        highlighted_lines = [x[0] for x in highlighted_lines_and_contour_masks]

        #remove the lines from the image to make it easier to find ellipses
        #new_base_contour_components is a list of lists of contours.
        #Let image_minus_lines be the image after removing the lines.
        #Each list of contours corresponds to the contours in a connected component of image_without_lines
        #We say that two contours of image_without_lines are connected if one of the removed lines connects them.
        #The point is that even if removing a line splits an ellipse in half, we want to search for an ellipse in
        #both pieces together.
        print '   removing the lines from the image...'
        new_base_contour_components = HighlightedShape.remove_shapes_from_contours(highlighted_lines, base_contours)

        print '   finding ellipses...'
        highlighted_ellipses = self._find_highlighted_ellipses(new_base_contour_components)    

        #remove the intervals that are too small
        for hl in highlighted_lines:
            hl.remove_small_intervals(self._min_interval_on_line_length)

        for he in highlighted_ellipses:
            he.remove_small_intervals(self._min_interval_on_ellipse_length)

        highlighted_shapes = highlighted_lines + highlighted_ellipses

        #remove the shapes that do not have any highlighted intervals
        highlighted_shapes = [hs for hs in highlighted_shapes if hs.number_of_intervals() > 0]

        return highlighted_shapes

