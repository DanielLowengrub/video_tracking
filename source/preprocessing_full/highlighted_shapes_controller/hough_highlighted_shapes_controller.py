import numpy as np
from scipy import ndimage
import mahotas
import itertools
from ...auxillary.contour_shape_overlap.highlighted_shape_generator import HighlightedShapeGenerator as HighlightedShapeFromContoursGenerator
from ...image_processing.contour_generator.contour_from_mask_generator import ContourFromMaskGenerator
from ...auxillary.contour_shape_overlap.highlighted_shape import HighlightedShape
from ...auxillary.contour_shape_overlap.line_with_position import LineWithPosition
from ...auxillary.contour_shape_overlap.ellipse_with_position import EllipseWithPosition
from ...image_processing.hough_transform.single_ellipse_detector import SingleEllipseDetector
from ...image_processing.hough_transform.hough_line_transform.hough_line_transform import HoughLineTransform
from .highlighted_shapes_controller import HighlightedShapesController
from ...auxillary import skeletonization, mask_operations
from ...auxillary.fit_points_to_shape.fit_points_to_ellipse_stable import FitPointsToEllipseStable
from ...auxillary.fit_points_to_shape.ransac_fit_points import RansacFitPoints
import matplotlib.pyplot as plt
import cv2

DEBUG = False

class HoughHighlightedShapesController(HighlightedShapesController):
    '''
    This implementation of HighlightedShapesController finds lines via a hough transform.
    '''
    def __init__(self, num_processors,
                 line_closing_radius, ellipse_closing_radius,
                 num_theta_bins, num_radius_bins, mask_shape, peak_threshold, peak_threshold_ratio, clustering_radius,
                 max_theta_std, max_radius_std, min_long_line_length,
                 min_points_in_ellipse,
                 ransac_min_points_to_fit, ransac_num_iterations, ransac_distance_to_shape_threshold,
                 ransac_min_percentage_in_shape, max_average_ellipse_residue,
                 thickness,
                 min_interval_on_line_length, min_interval_on_ellipse_length,
                 min_total_line_length, min_ellipse_axis, min_total_ellipse_length):
        '''
        num_theta_bins,...,clustering_radius - parameters to HoughLineTransform
        max_theta_std - the maximum allowed std of the theta value in a cluster of bins. (in radians)
        max_radius_std - the maximum allowes std of the radius values in a cluster of bins. (in pixels)
        min_long_line_length - a float. We assume that lines that are this long are not tangent lines to ellipses
        min_points_in_ellipse - an integer. The minimum number of points that can be used to fit an ellipse
        ransac_min_points_to_fit,...,ransac_min_percentage_in_shape - parameters to RansacFitPoints
        max_average_ellipse_residue - the maximum residue that is allowed when fitting points to an ellipse
        thickness - the thickness of the highighted lines (in pixels)
        min_interval_on_line_length - we throw out intervals on a line whose length is less than this
        min_interval_on_ellipse_length - we throw out intervals on an ellipse whose length is less than this
        min_total_line_length - throw out lines whose total length is less than this
        min_ellipse_axis - throw out ellipses that have an axis whose length is less than this
        min_total_ellipse_length - throw out ellipses whose sum of interval lengths is less than this
        '''
        super(HoughHighlightedShapesController, self).__init__(num_processors)

        self._line_closing_radius = line_closing_radius
        self._ellipse_closing_radius = ellipse_closing_radius
        
        self._hough_line_transform_controller = HoughLineTransform(num_theta_bins, num_radius_bins, mask_shape,
                                                                   peak_threshold, peak_threshold_ratio, clustering_radius)

        self._max_theta_std = max_theta_std
        self._max_radius_std = max_radius_std
        self._min_long_line_length = min_long_line_length
        
        ellipse_fitter = FitPointsToEllipseStable()
        self._min_points_in_ellipse = min_points_in_ellipse
        self._ransac_ellipse_fitter = RansacFitPoints(ellipse_fitter, ransac_min_points_to_fit, ransac_num_iterations,
                                                      ransac_distance_to_shape_threshold, ransac_min_percentage_in_shape)
        self._max_average_ellipse_residue = max_average_ellipse_residue
        #we use this to highlight the parts of a shape that overlap with a list of contours
        self._hs_generator = HighlightedShapeFromContoursGenerator()
        self._contour_generator = ContourFromMaskGenerator()
        
        #this is the thickness of the highlighted parts of a shape
        self._thickness = thickness
        self._min_interval_on_line_length = min_interval_on_line_length
        self._min_interval_on_ellipse_length = min_interval_on_ellipse_length
        self._min_total_line_length = min_total_line_length
        self._min_ellipse_axis = min_ellipse_axis
        self._min_total_ellipse_length = min_total_ellipse_length

        self._component_structure = np.ones((3,3))
        
    def _ellipse_is_degenerate(self, ellipse):
        '''
        ellipse - an Ellipse

        return True iff the ellipse is degenerate
        '''
        center, axes, angle = ellipse.get_geometric_parameters()
        is_degenerate = np.any(axes < self._min_ellipse_axis)

        #print '   axes=%s, is degenerate: %s' % (axes, is_degenerate)
        
        return is_degenerate
    
    def _find_ellipses(self, sideline_labels, num_labels):
        '''
        sideline_labels - a numpy array with shape (n,m) and type np.int32. It represents a labelling of the 
           the connected components of sideline pixels
        num_labels = the number of values in sideline_labels that are not 0.
        '''
        ellipses = []

        #look for an ellipse in each of the labelled regions of the sidelines mask
        for label in range(1,num_labels+1):            
            y_coords, x_coords = np.where(sideline_labels == label)
            points = np.vstack([x_coords, y_coords]).transpose()
            if len(points) < self._min_points_in_ellipse:
                continue
            
            #try to fit an ellipse to all of the contours in this connected component
            if DEBUG:
                print '   fitting ellipse to %d points...' % len(points)
                cv2.imshow('points', np.float32(sideline_labels == label))
                cv2.waitKey(0)
                cv2.destroyAllWindows()
            
            fitted_ellipse, total_geometric_residue, inlier_mask = self._ransac_ellipse_fitter.fit_points(points)

            if fitted_ellipse is None:
                if DEBUG:
                    print '   could not find an ellipse'
                continue

            average_ellipse_residue = total_geometric_residue / sum(inlier_mask)
            if DEBUG:
                print '   found ellipse with average residue: ', average_ellipse_residue
                
            if average_ellipse_residue > self._max_average_ellipse_residue:
                if DEBUG:
                    print '   the average residue is too large.'
                continue

            #if we succeeded, use this ellipse to create a highlighted shape
            if not self._ellipse_is_degenerate(fitted_ellipse):
                ellipses.append(fitted_ellipse)

        return ellipses

    def _find_sideline_contours(self, image, sidelines_mask):
        '''
        image - a numpy array with shape (n,m,3) and type np.uint8
        sidelines_mask - a numpy array of shape (n,m) and type np.bool
        return a list of Contour objects. These represent contours around the sideline part of the image
        '''
        #the base contours are the outer contours. A base contour may contain child contours.
        #For example, if there is an annulus in the image, then the outer circle will be a base contour,
        #and the inner circle will be it's child.
        base_contours = self._contour_generator.generate_contours(image, sidelines_mask)
        base_contours_with_children = [([base_contour] + base_contour.get_child_contours()) for base_contour in base_contours]
        all_contours = list(itertools.chain(*(base_contour_with_children
                                              for base_contour_with_children in base_contours_with_children)))
        return all_contours

    def _extract_lines_from_hough_transform(self, hough_line_transform):
        hough_bin_clusters = self._hough_line_transform_controller.find_hough_bin_clusters(hough_line_transform)
        lines = [cluster.get_mean_line() for cluster in hough_bin_clusters
                 if ((cluster.get_theta_value_std()  <= self._max_theta_std) and
                     (cluster.get_radius_value_std() <= self._max_radius_std))]
        return lines

    def _highlight_lines(self, lines, contours):
        '''
        lines - a list of Line objects
        contours - a list of Contour objects

        return - a list of HighlightedShape objects. These are highlighted shapes whose underlying shapes are the input lines.
           Each line is highighted at its intersections with the contours.
        '''
        lines_with_position = [LineWithPosition(line) for line in lines]

        #highlight the parts of each line that overlap with the contours
        highlighted_lines_and_contour_masks = [self._hs_generator.generate_highlighted_shape(l, self._thickness, contours)
                                               for l in lines_with_position]
        highlighted_lines = [x[0] for x in highlighted_lines_and_contour_masks]
        return highlighted_lines

    def _highlight_ellipses(self, ellipses, contours):
        '''
        ellipses - a list of Ellipse objects
        contours - a list of Contour objects

        return - a list of HighlightedShape objects. 
           These are highlighted shapes whose underlying shapes are the input ellipses.
           Each line is highighted at its intersections with the contours.
        '''
        ellipses_with_position = [EllipseWithPosition(ellipse) for ellipse in ellipses]

        #highlight the parts of each ellipse that overlap with the contours
        highlighted_ellipses_and_contour_masks = [self._hs_generator.generate_highlighted_shape(e, self._thickness,
                                                                                                contours)
                                                  for e in ellipses_with_position]
        highlighted_ellipses = [x[0] for x in highlighted_ellipses_and_contour_masks]
        return highlighted_ellipses

    def _build_labelled_foreground_mask(self, foreground_mask, highlighted_lines):
        '''
        foreground_mask - a numpy array with shape (n,m) and type np.bool
        highlighted_lines - a list of HighilghtedLine objects.

        return - (labels, num_labels)
           labels - a numpy array with shape (n,m) and type np.int32 
           num_labels - an int

           It represents the parts of the annotated image that are in
           the foreground, and are not contained in "long" lines.
        '''
        #get a list of the lines which are long enough to certainly not be an ellipse
        long_lines = [hl for hl in highlighted_lines if hl.highlighted_arc_length() >= self._min_long_line_length]
        #print 'line lengths: ', [hl.highlighted_arc_length() for hl in highlighted_lines]
        if len(long_lines) > 0:
            long_lines_mask = np.logical_or.reduce([hl.generate_mask(foreground_mask) for hl in long_lines])

            if DEBUG:
                cv2.imshow('long lines', long_lines_mask.astype(np.float32))
                cv2.waitKey(0)
                cv2.destroyAllWindows()

                cv2.imshow('foreground', foreground_mask.astype(np.float32))
                cv2.waitKey(0)
                cv2.destroyAllWindows()

            #remove the long lines from the foregound mask, and then close up the holes
            max_thickness = max(hl.get_thickness() for hl in long_lines)
            padding = 0#10
            closing_kernel = np.ones((max_thickness+padding,max_thickness+padding), np.bool)
            foreground_mask = np.logical_and(foreground_mask, np.logical_not(long_lines_mask))

            if DEBUG:
                cv2.imshow('foreground mask without lines before closing', foreground_mask.astype(np.float32))
                cv2.waitKey(0)
                cv2.destroyAllWindows()
            
            foreground_mask = ndimage.morphology.binary_closing(foreground_mask, closing_kernel)

            if DEBUG:
                cv2.imshow('foreground mask without lines after closing', foreground_mask.astype(np.float32))
                cv2.waitKey(0)
                cv2.destroyAllWindows()

        #label the foreground mask by connected components
        #labels, num_labels = ndimage.measurements.label(foreground_mask, structure=self._component_structure)
        labels, num_labels = mahotas.label(foreground_mask, Bc=self._component_structure)

        # if DEBUG:
        #     fig = plt.figure()
        #     ax1 = fig.add_subplot(121)
        #     ax2 = fig.add_subplot(122)
        #     ax1.imshow(foreground_mask.astype(np.float32), interpolation="nearest")
        #     ax2.imshow(labels, interpolation="nearest")
        #     ax1.set_ylim([labels.shape[0], 0])
        #     ax2.set_ylim([labels.shape[0], 0])
        #     plt.show()

        return labels, num_labels

    def _ellipses_mask_intersection(self, highlighted_ellipses, mask):
        '''
        highlighted_ellipses - a list of HighlightedEllipse objects
        mask - a numpy array with shape (n,m) and type np.bool

        return - a numpy array with shape (n,m) and type np.bool. 
           The output mask contains points that are both in the input mask, and in an ellipse
        '''
        ellipses_mask = np.logical_or.reduce([he.generate_mask(mask) for he in highlighted_ellipses])
        return np.logical_and(mask, ellipses_mask)

    def _clean_shapes(self, highlighted_lines, highlighted_ellipses):
        '''
        highlighted_lines - a list of HighlightedLine objects
        highlighted_ellipses - a list of HighlightedEllipse objects

        return - a list of HighlightedShape objects. 
           We first remove the short intervals in the highlighted lines and ellipses.
           then remove the shapes whose total highlighted region is too small.
        '''
        #remove the intervals that are too small
        new_highlighted_lines = []
        new_highlighted_ellipses = []
        for hl in highlighted_lines:
            #print 'interval lengths: ',
            closing = hl.interval_closing(self._line_closing_radius)
            closing.remove_small_intervals(self._min_interval_on_line_length)
            new_highlighted_lines.append(closing)

        for he in highlighted_ellipses:
            closing = he.interval_closing(self._ellipse_closing_radius)
            closing.remove_small_intervals(self._min_interval_on_ellipse_length)
            new_highlighted_ellipses.append(closing)
            
        #removes the shapes that do not have enough of them highlighted
        #print 'line arc lengths: ', [hl.highlighted_arc_length() for hl in highlighted_lines]
        new_highlighted_lines = [hl for hl in new_highlighted_lines
                                 if hl.highlighted_arc_length() >= self._min_total_line_length]
        new_highlighted_ellipses = [he for he in new_highlighted_ellipses
                                    if he.highlighted_arc_length() >= self._min_total_ellipse_length]
        
        new_highlighted_shapes = new_highlighted_lines + new_highlighted_ellipses

        #remove the shapes that do not have any highlighted intervals
        #highlighted_shapes = [hs for hs in highlighted_shapes if hs.number_of_intervals() > 0]

        return new_highlighted_shapes

    def _find_sidelines_and_foreground(self, image, annotation):
        '''
        image - a numpy array of shape (n,m,3) and type np.uint8
        annotation - a SoccerAnnotation object

        return - a tuple (sidelines_mask, base_contours)
           sidelines_mask - a numpy array with shape (n,m) and type np.bool.
           base_contours  - a list of Contour objects
        '''
        #find the contours in part of the image corresponding to the soccer field
        sidelines_mask = annotation.get_sidelines_mask() > 0
        foreground_mask = np.logical_or(sidelines_mask > 0, annotation.get_players_mask() > 0)
        base_contours = self._contour_generator.generate_contours(image, sidelines_mask)

        return sidelines_mask, foreground_mask, base_contours
    
    def _find_highlighted_shapes(self, image, annotation):
        '''
        image - a numpy array of shape (n,m,3) and type np.uint8
        annotation - a SoccerAnnotation object

        Return a list of HighlightedShape objects.
        '''
        if DEBUG:
            from .highlighted_shapes_visualization_controller import HighlightedShapesVisualizationController
            hs_vis_controller = HighlightedShapesVisualizationController(1, (0,255,0), (255,0,0), 0.6)

        sidelines_mask, foreground_mask, base_contours = self._find_sidelines_and_foreground(image, annotation)

        if DEBUG:
            cv2.imshow('sidelines mask', sidelines_mask.astype(np.float32))
            cv2.waitKey(0)
            cv2.destroyAllWindows()

        base_contours_with_children = [([base_contour] + base_contour.get_child_contours())
                                       for base_contour in base_contours]
        contours = list(itertools.chain(*(base_contour_with_children
                                          for base_contour_with_children in base_contours_with_children)))

        sidelines_skeleton = skeletonization.compute_skeleton(sidelines_mask, thickness_map=True)
        sidelines_skeleton_mask = np.logical_and(sidelines_skeleton >= 1, sidelines_skeleton <= 3)

        if DEBUG:
            cv2.imshow('sidelines skeleton mask', sidelines_skeleton_mask.astype(np.float32))
            cv2.waitKey(0)
            cv2.destroyAllWindows()

        #find lines in the annotated image
        if DEBUG:
            print '   finding lines...'
            
        hough_line_transform = self._hough_line_transform_controller.build_hough_line_transform(sidelines_skeleton_mask)
        lines = self._extract_lines_from_hough_transform(hough_line_transform)

        if DEBUG:
            print 'highlighting lines...'
            sidelines_mask_color = np.zeros(sidelines_mask.shape + (3,), np.uint8)
            cv2.drawContours(sidelines_mask_color, [c.get_cv_contour() for c in contours], -1, (255,0,0), 1)
            for contour in contours:
                points = contour.get_cv_contour().reshape((-1,2))
                sidelines_mask_color[points[:,1],points[:,0]] = np.array([0,255,0])
            
            cv2.imshow('sidelines mask with contours', sidelines_mask_color)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

        highlighted_lines = self._highlight_lines(lines, contours)

        if DEBUG:
            print 'line lengths: ', [hl.highlighted_arc_length() for hl in highlighted_lines]
            mask_image = mask_operations.convert_to_image(sidelines_mask)
            hs_vis_controller._draw_highlighted_shapes(mask_image, highlighted_lines)
            cv2.imshow('highlighted lines', mask_image)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

        #find ellipses in the image
        if DEBUG:
            print '   finding ellipses...'
            
        labelled_foreground, num_labels = self._build_labelled_foreground_mask(foreground_mask, highlighted_lines)
        labelled_foreground[np.logical_not(sidelines_skeleton_mask)] = 0
        ellipses = self._find_ellipses(labelled_foreground, num_labels)
        highlighted_ellipses = self._highlight_ellipses(ellipses, contours)

        # mask_image = mask_operations.convert_to_image(sidelines_mask)
        # hs_vis_controller._draw_highlighted_shapes(mask_image, highlighted_ellipses)
        # cv2.imshow('highlighted ellipses', mask_image)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        #remove the ellipses from the hough_line_transform, and extract lines from the remaining transform
        ellipses_skeleton_mask = self._ellipses_mask_intersection(highlighted_ellipses, sidelines_skeleton_mask)
        new_hough_line_transform = self._hough_line_transform_controller.build_hough_line_transform(ellipses_skeleton_mask)
        lines = self._extract_lines_from_hough_transform(hough_line_transform - new_hough_line_transform)
        highlighted_lines = self._highlight_lines(lines, contours)

        # mask_image = mask_operations.convert_to_image(sidelines_mask)
        # hs_vis_controller._draw_highlighted_shapes(mask_image, highlighted_lines)
        # cv2.imshow('highlighted lines', mask_image)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        highlighted_shapes = self._clean_shapes(highlighted_lines, highlighted_ellipses)

        return highlighted_shapes

