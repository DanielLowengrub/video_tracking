class TrackingSubsequenceGenerator(object):
    '''
    The goal of this class is to break up a sequence of image annotations into subsequences that could potentially be used for
    tracking.
    '''
    TEMP_DIRECTORY = 'temp'
    
    def __init__(self, min_subsequence_length):
        '''
        min_subsequence_length - an integer. The minimal allowed length of a subsequence
        '''
        self._min_subsequence_length = min_subsequence_length

    
    def generate_tracking_subsequences(self, preprocessing_data_dict, annotation_subsequence_directory):
        '''
        annotation_directory - a directory with files annotation_0.npy,...,annotation_N.npy
        annotation_subsequence_directory - the output directory. It will contain subdirectories:
           annotation_i0_j0, annotation_i1_j1, ...
        '''

        indexed_frame_data = self._load_preprocessing_data(preprocessing_data_dict)
        enumerated_annotations = enumerate(annotations)

        #make a directory to store annotations which fit into a subsequence
        temp_dir = self._create_temp_directory(annotation_subsequence_directory)

        start_frame = 0
        end_frame = 0
        
        for frame_index, annotation in enumerated_annotations:
            #check if the annotation can be used for tracking
            if self._is_tracking_annotation(annotation):
                #copy the annotation to the temp directory
                subsequence_index = frame_index - start_frame
                AnnotationGenerator.save_annotation(temp_dir, annotation, subsequence_index)
                end_frame += 1

            #if it does not, then save the temp dir and 
        
