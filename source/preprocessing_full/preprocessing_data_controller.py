import numpy as np
import os
import re
import functools
import multiprocessing as mp
from multiprocessing import Event
from datetime import datetime
import itertools
import traceback
import sys
import signal
from ..auxillary import np_operations
from ..auxillary.iterator_len import IteratorLen
from .interval_data import IntervalData
from .frame_data.frame_dir_link import FrameDirLink
from .frame_data.frame_data_loader import FrameDataLoader
from ..preprocessing_full import filename_loader
from ..auxillary.pool import Pool, DataProcessor

class PreprocessingDataController(object):
    '''
    A PreprocessingDataController is in charge of generating, saving and loading a specific type of preprocessing data.

    All of the data controlled by this object is stored in the following format:
    interval_i0_j0/
       frame_l0/
       frame_l1/
       ...

    interval_i1_j1/
       frame_l0/
       frame_l1/
       ...

    ...

    interval_ik_jk is a directory which stores frames starting from frame ik and ending with frame jk.

    A collection of such directories is loaded into an IteratorLen object which stores an iterator of IntervalData objects. 
    This base class provides methods:
    * _save_interval_data_iter: subclasses can use this to save an IteratorLen of IntervalData objects
    * load_data: this can load an IteratorLen of IntervalData object

    In order for these to work, the subclass must implement:
    _save_data_in_frame_dir: this specifies how data should be saved in a frame directory
    _load_data_from_frame_dir: this specifies how to load data from a frame directory

    In addition, all subclasses must implement generate_data.

    The typical way to implement this class is as follows:
    * implement _save_data_in_frame_dir and _load_data_from_frame_dir which save and load the data type we are generating
    * to implement generate_data, first create in IteratorLen of IntervalData objects, one per interval which represent the
      data that we generated in each interval. Then pass this to _save_interval_data_iter to save it.
    '''
    
    INTERVAL_BASENAME = 'interval_%d_%d'
    FRAME_BASENAME = 'frame_%d'
    
    INTERVAL_REGEX = re.compile('interval_([0-9]+)_([0-9]+)')
    FRAME_REGEX = re.compile('frame_([0-9]+)')

    #multiprocessing options
    NO_MP       = 0 #only a single processor is used
    INTERVAL_MP = 1 #the intervals are sent to different processors,
                    #but the frames in each interval are sent to the same processor
    FRAME_MP    = 2 #IF there is a single interval, then the frames in that interval are sent to different processors

    TIMEOUT = 3600*24*2
    
    def __init__(self, num_processors, multiprocessing_mode=INTERVAL_MP, add_to_existing_intervals=False):
        '''
        num_proccessors - the number of available processors.
        multiprocessing_mode - either NO_MP, INTERVAL_MP or FRAME_MP
           While generating data, we typically send out the intervals to difference threads to processed concurrently.
           Knowing how many processors there are lets us know how many intervals to send to each processor.
        add_to_existing_intervals - if True, then we process frames that are missing from existing intervals
        '''

        self._num_processors = num_processors
        self._multiprocessing_mode = multiprocessing_mode
        self._add_to_existing_intervals = add_to_existing_intervals

    @classmethod
    def __get_frame_directory_name(cls, interval_dir, frame):
        '''
        interval_dir - the directory of an interval
        frame - an integer l representing the index of a frame in the interval

        return the name of the directory in which the frame should be saved.
        '''
        frame_dir = os.path.join(interval_dir, cls.FRAME_BASENAME % frame)
        return frame_dir

    @classmethod
    def __get_interval_directory_name(cls, base_dir, interval):
        '''
        base_dir - a directory
        interval - an IntervalData objec
        
        return the name of the directory in which the interval should be saved
        '''
        interval_dir = os.path.join(base_dir, cls.INTERVAL_BASENAME % interval)
        return interval_dir
    

    @classmethod
    def __make_frame_directory(cls, interval_dir, frame):
        '''
        base_dir - a directory_name
        interval - a tuple (i,j) representing a frame interval
        frame - an integer representing the index of a frame in the interval
        
        make a the directory in which the frame data should be saved
        '''
        frame_dir = cls.__get_frame_directory_name(interval_dir, frame)
        os.mkdir(frame_dir)
        return frame_dir

    def __make_interval_directory(self, base_dir, interval):
        '''
        base_dir - a directory_name
        interval - a tuple (ik,jk)
        
        make the directory in which the interval should be saved.
        
        return - success
           If the directory already existed, then success==True iff _add_to_existing_intervals.
           If the directory did not exist, make it and return True.
        '''
        interval_dir = self.__get_interval_directory_name(base_dir, interval)
        if os.path.isdir(interval_dir):
            #print 'the directory %s already exists' % interval_dir
            return self._add_to_existing_intervals
        
        #print 'making directory: ', interval_dir
        os.mkdir(interval_dir)
        return True
    
    @classmethod
    def __get_interval_directories(cls, base_dir):
        '''
        base_dir - a directory name. The directory is assumed to contain subdirectories of the form: interval_ik_jk

        return - a list of tuples: (((i0,j0), interval_dir_0), ((i1,j1), interval_dir_1)...)
           the ik's are in increasing order, and ik<jk
        '''
        return filename_loader.load_filenames(base_dir, cls.INTERVAL_REGEX)

    @classmethod
    def __get_frame_directories(cls, interval_dir, reverse):
        '''
        interval_dir - the directory in which the data of a frame interval is stored
        reverse - a boolean. If true, the frames will be returned in decreasing order.
        return an iterator of tuples: ((l0, frame_dir_0), (l1, frame_dir_1),...) 
           the li's are in increasing order and represent the frames in this interval that we have saved.
        '''
        return filename_loader.load_filenames(interval_dir, cls.FRAME_REGEX)

    @classmethod
    def __load_frame_data(cls, frame_dir, frame, load_frame_dir_link):
        '''
        frame_dir - a directory storing a frame
        frame - the index of the frame in its interval
        load_frame_dir_link - a boolean. If true, only load the names of the frame directories, not the frame data itself

        return - a FrameData object that contains the data in the frame

        Note - if load_frame_dir_link is False, then this method will raise a NotImplementedError if _load_frame_data
        has not been implemented. The reason is that without that method, we only know the frame directory name, but we do
        not have access to it's contents.
        '''

        #if the frame_dir is a symlink to a different frame directory, resolve the symlink before loading the data
        if os.path.islink(frame_dir):
            frame_dir = os.readlink(frame_dir)

        #if we are only supposed to load the name of the directory containing the data
        if load_frame_dir_link:
            frame_data = FrameDirLink(frame, frame_dir)

        #load the data contained in the directory
        else:
            frame_data = FrameDataLoader(frame, cls, frame_dir)
            
        return frame_data
    
    @classmethod
    def __load_frame_datas(cls, interval_dir, reverse, load_frame_dir_link):
        '''
        interval_dir - a directory name
        reverse  - a boolean. If true, the frames in each intervals will be loaded in reverse order.
        load_frame_dir_link - a boolean. If true, only load the names of the frame directories, not the frame data itself

        return - a list of FrameData objects
        '''
        #print '   loading interval from: %s' % interval_dir

        indexed_frame_dirs = cls.__get_frame_directories(interval_dir, reverse)
        return [cls.__load_frame_data(frame_dir, frame, load_frame_dir_link) for frame,frame_dir in indexed_frame_dirs]

    def __save_frame_data(self, interval_dir, frame_data):
        '''
        output_parent_dir - a directory name. this is where we store the frame data
        frame_data - an FrameData object
        
        save the frame data in the specified output directory.
        '''
        #if the frame already exists, then skip it
        frame_dir = self.__get_frame_directory_name(interval_dir, frame_data.frame)
        if os.path.isdir(frame_dir):
            return
        
        #if the frame data is a link to a frame dir,
        #then frame_data holds a directory name, and we store a symlink to that directory
        if isinstance(frame_data, FrameDirLink):
            frame_symlink_src = self.__get_frame_directory_name(interval_dir, frame_data.frame)
            frame_symlink_dst = frame_data.build_data()
            #print '      creating symlink: %s <- %s' % (frame_symlink_dst, frame_symlink_src)
            os.symlink(frame_symlink_dst, frame_symlink_src)

        #otherwise, store the frame data in a new directory
        else:
            try:
                # print '[%s] using pdc: %d' % (mp.current_process().name, id(self))
                # print '[%s] save_frame_data: source_lines_ph: %s' % (mp.current_process().name, self._shape_distance_minimizer._source_lines_ph)
                # sys.stdout.flush()

                data = frame_data.build_data(pd_controller=self)
            except:
                print 'error when building data for frame %d.' % frame_data.frame
                sys.stdout.flush()
                raise
            
            #if we could not build data for this frame, quit the saving method
            if data is None:
                #print '   could not build data.'
                pass
            
            else:
                frame_dir = self.__make_frame_directory(interval_dir, frame_data.frame)
                #print '      saving frame %d in directory: %s' % (frame_data.frame, frame_dir)
                self._save_data_in_frame_dir(frame_dir, data)
            
        return

    def __save_interval_data(self, output_parent_dir, interval_data, exit_event):
        '''
        output_parent_dir - a directory name. this is where we store the interval data
        interval_data - an IntervalData object
        exit_event - an Event object. If it is None, we ignore it.

        save the interval data in the given directory
        '''
        interval_dir = self.__get_interval_directory_name(output_parent_dir, interval_data.interval)
        print '[%s] saving interval (%d,%d) in directory: %s' % ((mp.current_process().name,) + 
                                                                 interval_data.interval + (interval_dir,))
        sys.stdout.flush()
        
        for frame_data in interval_data:
            if (exit_event is not None) and exit_event.is_set():
                break
            
            #print '[%s]    saving frame %d' % (mp.current_process().name, frame_data.frame)
            self.__save_frame_data(interval_dir, frame_data)
            #print '[%s]    finished saving frame %d' % (mp.current_process().name, frame_data.frame)

        return
    
    @classmethod
    def load_data_from_frame_dir(self, frame_dir):
        '''
        frame_dir - the name of a directory containing frame data.
        return - the data stored in the directory

        If this method is implemented then the class can be used to construct a FrameDataLoader object. The FrameDataLoader
        will then use the load_data_from_frame_dir method to load the data when it is asked for.
        '''
        raise NotImplementedError('_load_data_from_frame_dir has not been implemented')
        
    @classmethod
    def load_data(cls, base_dir, reverse=False, load_frame_dir_link=False):
        '''
        base_dir - a directory name
        reverse  - a boolean. If true, each of the intervals will be loaded in reverse order.
        load_frame_dir_link - a boolean. If true, only load the names of the frame directories, not the frame data itself

        return an IteratorLen of IntervalData objects

        Note: If _load_data_in_frame is not implemented by the subclass, then the default behavior is to load an 
        IteratorLen of IntervalData objects whose FrameData objects contain symlinks to frame_dir. 
        I.e, the FrameData objects
        do not store the actual data in the frame directories (since we do not know how to load it), 
        but rather the directory names themselves.
        '''
        print 'loading data from: ', base_dir

        #get a list of the frame intervals contained in the base directory
        indexed_interval_dirs = cls.__get_interval_directories(base_dir)
        interval_data_iter = (IntervalData(interval, cls.__load_frame_datas(interval_dir, reverse, load_frame_dir_link))
                              for interval,interval_dir in indexed_interval_dirs)

        return IteratorLen(len(indexed_interval_dirs), interval_data_iter)

    def _save_data_in_frame_dir(self, frame_dir, data):
        '''
        frame_dir - the directory where the frame data should be stored
        data - the dats we want to save

        This method should be implemented by all subclasses.  After it is implemented, the subclass can call the _save_frame
        method and it will store the frame in the correct directory.
        '''
        raise NotImplementedError('_save_data_in_frame_dir must be implemented by the subclass')

    def __save_multiple_interval_datas(self, output_parent_dir, interval_data_iter):
        '''
        output_parent_dir - a directory name. this is where we store the interval data
        interval_data_iter - an IteratorLen of IntervalData objects

        save the interval data objects in the output directory
        '''
        print 'saving multiple interval datas...'
        sys.stdout.flush()
        
        data_writer = PreprocessingDataWriter(self, output_parent_dir, make_interval_dir=True)

        #if we are supposed to process the intervals on different processors
        if self._multiprocessing_mode == self.INTERVAL_MP:
            p = Pool(self._num_processors, data_writer, interval_data_iter)

        #process the intervals in the same processor (0 workers are spawned)
        else:
            p = Pool(0, data_writer, interval_data_iter)

        print 'running data writing pool...'
        sys.stdout.flush()
        
        p.run()
        return

    def __save_single_interval_data(self, output_parent_dir, interval_data):
        '''
        output_parent_dir - a directory name. this is where we store the interval data
        interval_data - an IntervalData object

        save the interval data, possibly using multiple processors.
        '''
        success = self.__make_interval_directory(output_parent_dir, interval_data.interval)
        if not success:
            print '   skipping interval (%d,%d) since it already exists' % interval_data.interval
            return
                    
        #set up the data writer so that it does not try to make the interval directory again
        data_writer = PreprocessingDataWriter(self, output_parent_dir, make_interval_dir=False)

        #if we should processes the frames in this interval on different processors
        if self._multiprocessing_mode == self.FRAME_MP:
            sub_interval_datas = interval_data.chunks(self._num_processors)
            p = Pool(self._num_processors, data_writer, sub_interval_datas)
        else:
            sub_interval_datas = interval_data.chunks(1)
            p = Pool(1, data_writer, sub_interval_datas)

        p.run()

    def _save_interval_data_iter(self, output_parent_dir, interval_data_iter):
        '''
        output_parent_dir - a directory name. this is where we store the interval data
        interval_data_iter - an IteratorLen of IntervalData objects

        save the interval data objects in the output directory
        '''
        print 'saving interval data iterator (num intervals=%d, multiprocessing_mode=%d)' % (len(interval_data_iter),
                                                                                             self._multiprocessing_mode)

        #if there is more than one interval, send them out to multiple processors
        if len(interval_data_iter) > 1:
            self.__save_multiple_interval_datas(output_parent_dir, interval_data_iter)
            
        #there is only one interval
        else:
            interval_data = next(interval_data_iter, None)
            if interval_data is None:
                return
            
            self.__save_single_interval_data(output_parent_dir, interval_data)
                
        return

    def _initialize_controller(self):
        '''
        This performs any initialization that is needed after the PDC has been serialized and sent to an 
        interval_data_worker.
        For example, if the controller needs a tensorflow session, then this method could create the session.
        '''
        pass

    def _close_controller(self):
        '''
        This performs any post processing that is needed after the PDC has been serialized and sent to an interval_data_worker.
        For example, if the controller needs a tensorflow session, then this method could close the session.
        '''
        pass

    def build_data(self, *args):
        '''
        This method is used to construct frame data out of some set of arguments. These arguments depend on the subclass.
        
        If this method is implemented, then an instance x of this class together with a tuple of arguments args can be passed 
        to a ControllerFrameData object, and that object will call x.build_data(*args) when it is asked for the data.
        '''
        raise NotImplementedError('build_data has not been implemented.')
    
    def generate_data(self, output_parent_dir, *input_data):
        '''
        output_parent_dir - the directory where we should save the generated data
        input_data - a number of arguments that will be used as input data
        '''
        raise NotImplementedError('generate_data must be implemented by subclasses')

    @classmethod
    def __is_sub_interval(cls, interval_a, interval_b):
        '''
        interval_a - a tuple of integers (i,j)
        interval_b - a tuple of integers (a,b)
        return True iff interval_a is a sub interval of interval b. I.e: a <= i < j <= b
        '''
        is_sub_interval = interval_b[0] <= interval_a[0] < interval_a[1] <= interval_b[1]
        #print '%s is a subinterval of %s: %s' % (interval_a, interval_b, is_sub_interval)
        return is_sub_interval
        
    @classmethod
    def __build_sub_interval_data_iter(cls, interval_data_iter, sub_interval_iter):
        '''
        interval_data_iter - an iterator of IntervalData objects, whose frames contain FrameDirLink objects
        sub_interval_iter - an iterator of intervals: ((i0,j0), (i1,j1), ...). We assume that each of the intervals
           is contained in the interval of one of the IntervalData objects in interval_data_iter

        return an iterator of IntervalData objects, whose intervals are equal to those in the the refined_interval_data_iter
        '''
        #we search the interval_data_iter for an IntervalData that contains this refined interval.
        #when we find one, we use it to 
        current_sub_interval = next(sub_interval_iter)
        #print '   current sub interval: ', current_sub_interval
        
        for interval_data in interval_data_iter:
            #print '   finding sub intervals of interval: ', interval_data.interval
            #in order to break up the interval_data (which is an iterator over frames) into sub intervals, we need to load
            #the frames into a list. But we only do this after there is at least one sub interval to build
            frame_datas = None
            frames = None

            #go through all of the subintervals that are contained in the interval_data's interval
            while cls.__is_sub_interval(current_sub_interval, interval_data.interval):
                if frame_datas is None:
                    frame_datas = list(interval_data)
                    abs_frames = np.array([(interval_data.interval[0] + fd.frame) for fd in frame_datas])

                #build a new IntervalData object whose interval is current_sub_interval and whose frames consist of all of the
                #frames in frame_data_list whose frame index is in the current subinterval
                frame_data_slice = np_operations.find_slice(current_sub_interval, abs_frames)
                #print '   frame_data_slice: ', frame_data_slice
                sub_frame_datas = frame_datas[frame_data_slice]

                #the frames in the sub_frame_data_list should be shifted so that frame zero corresponds to the first frame
                #in the current sub interval
                frame_offset = current_sub_interval[0] - interval_data.interval[0]
                sub_frame_datas = [FrameDirLink(fd.frame-frame_offset, fd.build_data()) for fd in sub_frame_datas]
                sub_interval_data = IntervalData(current_sub_interval, sub_frame_datas)

                yield sub_interval_data

                #now build an IntervalData object using the next subinterval
                current_sub_interval = next(sub_interval_iter)
                #print '   current sub interval: ', current_sub_interval

    @classmethod
    def __build_link_to_interval_data(cls, interval_data):
        '''
        interval_data - an IntervalData object whose FrameData objects store directory names
        return - an IntervalData object whose FrameData objects are links to the frames in the input interval data
        '''
        frame_datas = [FrameDirLink(fd.frame, fd.build_data()) for fd in interval_data]
        return IntervalData(interval_data.interval, frame_datas)
    
    def refine_intervals(self, output_parent_dir, input_parent_dir, refined_interval_iter):
        '''
        output_parent_dir - the directory where we should save the refined intervals
        input_data_dir - the directory containing the data we want to refine
        refined_interval_iter - an IteratorLen of tuples: ((i0,j0),(i1,j1),...), sorted by ik

        Suppose that the data in input_data_dir has intervals ((a0,b0), (a1,b1),...)
        We assume that each of the input intervals (ik,jk) is contained in an interval (al,bl).
        
        The method creates a new set of data whose intervals are the (ik,jk). 
        The frames in interval (ik,jk) are the ones in the corresponding subset of (al,bl)
        '''
        print 'refining intervals...'
        #this loads an IteratorLen of IntervalData objects, whose frames are represented by FrameDirLink objects
        interval_data_iter = self.load_data(input_parent_dir, reverse=False, load_frame_dir_link=True)

        # #Debugging
        # interval_data_iter = (interval_data for interval_data in interval_data_iter
        #                             if interval_data.interval[0] == 12356)
        # interval_data_iter = IteratorLen(1, interval_data_iter)
        # #############
        
        #this is an iterator of IntervalData objects whose intervals are equal to those in refined_interval_iter
        refined_interval_data_iter = self.__build_sub_interval_data_iter(interval_data_iter, refined_interval_iter)

        #upgrade it to an IteratorLen object with the same number of elems as refined_interval_iter
        refined_interval_data_iter = IteratorLen(len(refined_interval_iter), refined_interval_data_iter)

        #save the refined data in the specified directory
        self._save_interval_data_iter(output_parent_dir, refined_interval_data_iter)
        
        return

    def refine_intervals_from_directory(self, output_parent_dir, input_parent_dir, refined_parent_dir):
        '''
        output_parent_dir - the directory where we should save the refined intervals
        input_data_dir - the directory containing the data we want to refine
        refined_parent_dir - the name of a directory whose intervals are a refinement of the intervals in input_data_dir

        refine the intervals in input_data_dir to those in refined_interval_dir, and save them in output_parent_dir
        '''
        #extract the intervals from the refined parent directory
        interval_data_iter = self.load_data(refined_parent_dir, reverse=False, load_frame_dir_link=True)

        # #DEBUGGING
        # interval_data_iter = (interval_data for interval_data in interval_data_iter
        #                    if interval_data.interval[0] == 12356)
        # interval_data_iter = IteratorLen(1, interval_data_iter)
        # ######
        
        num_intervals = len(interval_data_iter)
        refined_interval_iter = (interval_data.interval for interval_data in interval_data_iter)
        refined_interval_iter = IteratorLen(num_intervals, refined_interval_iter)
        
        #use these intervals to refine the intervals in the input parent directory
        self.refine_intervals(output_parent_dir, input_parent_dir, refined_interval_iter)
        
        return

    def remove_empty_intervals(self, output_parent_dir, input_parent_dir):
        '''
        output_parent_dir - the directory where we save the non empty intervals
        input_parent_dir - a directory with intervals
        '''
        #load IntervalData objects whose frame store links to their frame directories
        interval_data_iter = self.load_data(input_parent_dir, reverse=False, load_frame_dir_link=True)

        #build links to the non empty interval data objects
        non_empty_interval_data_iter = (self.__build_link_to_interval_data(interval_data)
                                        for interval_data in interval_data_iter if interval_data.number_of_frame_datas()>0)

        #note that this IntertorLen could have less intervals than it thinks it does
        non_empty_interval_data_iter = IteratorLen(len(interval_data_iter), non_empty_interval_data_iter)
        
        #save links to the intervals in the output directory
        self._save_interval_data_iter(output_parent_dir, non_empty_interval_data_iter)
        
        return

class PreprocessingDataWriter(DataProcessor):
    '''
    This data writer uses a PreprocessingDataController object to write data to the disk.
    '''
    def __init__(self, pd_controller, output_parent_dir, make_interval_dir):
        '''
        pd_controller - a PreprocessingDataController object
        output_base_dir - the base directory in which we store the data generated for each of the intervals
        make_interval_dir - if True, then make the directory where the interval will be saved. 
           If False, assume the directory has already been created
        '''
        super(PreprocessingDataWriter, self).__init__()
        self._pd_controller = pd_controller
        self._output_parent_dir = output_parent_dir
        self._make_interval_dir = make_interval_dir

    def initialize(self):
        print '[%s] initializing writer...' % mp.current_process().name
        self._pd_controller._initialize_controller()
        return

    def close(self):
        print '[%s] closing writer...' % mp.current_process().name
        sys.stdout.flush()
        self._pd_controller._close_controller()
        return

    def process_data(self, interval_data, exit_event):
        '''
        interval_data - an IntervalData object
        exit_event - an Event
        '''
        if self._make_interval_dir:
            success = self._pd_controller._PreprocessingDataController__make_interval_directory(self._output_parent_dir,
                                                                                                interval_data.interval)
            if not success:
               print '[%s] write_data: skipping interval (%d,%d) since it already exists' % ((mp.current_process().name,) +
                                                                                             interval_data.interval)
               return

        self._pd_controller._PreprocessingDataController__save_interval_data(self._output_parent_dir, interval_data,
                                                                             exit_event)
        return
        
