import numpy as np
from .annotation_controller.stupid_field_mask_controller import StupidFieldMaskController
from .preprocessing_data_type import PreprocessingDataType
import os

#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts'
#IMAGE_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/test_data/'

# OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
# IMAGE_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/images'

OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'
IMAGE_PARENT_DIRECTORY = '/home/daniel/soccer/images'

INPUT_DATA_NAME = 'images-8_15-9_31'
#INPUT_DATA_NAME = 'images-0_00-1_00'
#INPUT_DATA_NAME = 'images-0_00-10_00'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_full_0', 'data', INPUT_DATA_NAME)

IMAGE_DIRECTORY              = os.path.join(PREPROCESSING_DIRECTORY, 'stage_0', 'image')
FIELD_MASK_DIRECTORY         = os.path.join(PREPROCESSING_DIRECTORY, 'stage_0', 'field_mask')

PD_DIRECTORY_DICT = {PreprocessingDataType.IMAGE: IMAGE_DIRECTORY}

NUM_PROC = 8

#This is a mask containing the portions of the screen dedicated to heads up display
HUD_MASK = np.zeros((352,624), np.bool)
HUD_MASK[20:39, 113:216] = True
HUD_MASK[20:40, 489:580] = True

#The range of hue values that are considered grass
HUE_MIN = 36
HUE_MAX = 54

def generate_data(output_parent_directory, pd_directory_dict):
    '''
    output_parent_directory - the directory in which we will save the annotations
    pd_directory_dict - a dictionary with items (PreprocessingDataType attr, directory name
    '''
    if not os.path.isdir(output_parent_directory):
        print 'making directory: ', output_parent_directory
        os.mkdir(output_parent_directory)

    fm_controller = StupidFieldMaskController(NUM_PROC, HUD_MASK, HUE_MIN, HUE_MAX)
    fm_controller.generate_data(output_parent_directory, pd_directory_dict)

    return

if __name__ == '__main__':
    generate_data(FIELD_MASK_DIRECTORY, PD_DIRECTORY_DICT)
