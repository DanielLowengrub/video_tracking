import os
import functools
import itertools
import multiprocessing as mp
from datetime import datetime
import numpy as np
from ..preprocessing_data_controller import PreprocessingDataController
from ..interval_data import IntervalData
from ...auxillary.iterator_len import IteratorLen
from ...auxillary import iterator_operations
from ...auxillary.interval import Interval, IntervalList
from ..frame_data.raw_frame_data import RawFrameData
from ...auxillary.pool import Pool, DataProcessor

class RelativeHomographyController(PreprocessingDataController):
    '''
    This PDC is in charge of generating relative homographies between consecutive frames.
    In addition, it breaks up the frames into interval such that there is a relative homography between each pair of 
    consecutive frames in a given interval.
    '''

    #this is how we store the homographies in their frame directory
    HOMOGRAPHY_BASENAME = 'homography.npy'

    def __init__(self, num_processors, min_subsequence_length):
        super(RelativeHomographyController, self).__init__(num_processors, multiprocessing_mode=self.NO_MP)

        self._min_subsequence_length = min_subsequence_length
        
    @classmethod
    def _get_homography_filename(cls, frame_dir):
        '''
        frame_dir - the name of a directory containing a relative homography
        
        return - the filename of the homography
        '''
        homography_filename = os.path.join(frame_dir, cls.HOMOGRAPHY_BASENAME)
        return homography_filename
    
    def _save_data_in_frame_dir(self, frame_dir, homography):
        '''
        frame_dir - a frame directory name
        homography - a numpy array with shape (3,3) and type np.float64

        save the soccer annotation in the frame directory
        '''
        homography_filename = self._get_homography_filename(frame_dir)
        #print '      saving homography to: %s' % homography_filename
        np.save(homography_filename, homography)
        return

    @classmethod
    def load_data_from_frame_dir(cls, frame_dir):
        '''
        frame_dir - a directory name
        return - a numpy array with shape (3,3) and type np.float64 representing a homography matrix
        '''
        homography_filename = cls._get_homography_filename(frame_dir)
        #print '   loading homography from: %s' % homography_filename
        homography = np.load(homography_filename)
        return homography

    def _build_relative_homography(self, image_0, annotation_0, image_1, annotation_1):
        '''
        image_i - a numpy array with shape (n,m) and type np.uint8
        annotation_i - a SoccerAnnotation object

        return - a numpy array with shape (3,3 and type np.float64 that represents a homography between the frames in the
           frame_data_pair
        '''
        raise NotImplementedError('_build_relative_homography is not implemented')
    
    def _build_relative_homography_from_pd_tuples(self, pd_tuple_pair):
        '''
        pd_tuple_pair - a pair (pd_tuple_0, pd_tuple_1). 
           pd_tuple_i is a PreprocessingDataTuple object that contains an image and an annotation
        return - a numpy array with shape (3,3 and type np.float64 that represents a homography between the frames in the
           frame_data_pair
        '''
        (frame_0, pd_tuple_0), (frame_1, pd_tuple_1) = pd_tuple_pair
        # if not frame_0 > 211:
        #    return np.ones((3,3))

        #print 'computing homography from frame %d to frame %d...' % (frame_0, frame_1)
        H = self._build_relative_homography(pd_tuple_0.image, pd_tuple_0.annotation,
                                            pd_tuple_1.image, pd_tuple_1.annotation)

        #print H
        if np.any(np.isnan(H)):
            #print '* could not find homography from frame %d to frame %d. returning nan' % (frame_0, frame_1)
            pass
        
        else:
            #print '  found homography from frame %d to frame %d' % (frame_0, frame_1)
            pass
        
        return H
    
    def _find_relative_homography_intervals(self, relative_homographies):
        '''
        relative_homographies - a numpy array with shape (n, 3, 3). The i-th 3x3 matrix is the homography
                                from image i to image i+1. If the homography doesn't exist, the matrix has np.nan values.
        
        return - a list of tuples (i,j) which represent the maximal subintervals of [0,n-1] for which 
        the value of relative_homographies[i] is not np.nan
        '''

        #this is a numpy array with type np.bool and the same length as relative_homograhies.
        #The i-th entry has the value True iff relative_homographies[i] has np.nan values
        homography_is_missing = np.any(np.isnan(relative_homographies), axis=(1,2))

        #print 'could not find homographies from frames:'
        print np.where(homography_is_missing)
        
        #find the maximal intervals of valid homographies
        intervals = IntervalList.from_boolean_array(np.logical_not(homography_is_missing)).get_intervals()
        intervals = [(interval.first_element(), interval.last_element()) for interval in intervals]
        
        return intervals

    def _generate_relative_homography_subsequences(self, output_parent_dir, interval_data, exit_event):
        '''
        output_parent_dir - the directory in which to store the relative homographies
        interval_data - an IntervalData objects whose frames are FrameDataTuple objects that contain an image and 
        an annotation

        save a list of IntervalData objects. Each one represents a subsequence of frames in the initial 
        interval data that are connected by a relative homography. 
        Each frame in these IntervalData objects is a RawFrameData object that stores a homography to the next frame.
        '''
        print '[%s] building relative homographies in interval: (%d,%d)' % ((mp.current_process().name,) +
                                                                            interval_data.interval)
        first_frame = interval_data.interval[0]
        
        #build an iterator of pairs of FrameDataTuple objects
        pd_tuple_iter = ((frame_data.frame, frame_data.build_data()) for frame_data in interval_data)
        pd_tuple_pair_iter = iterator_operations.pairwise_iterator(pd_tuple_iter)

        #use each pair to generate a homography. If there is no homography, then we store an array of np.nan values
        relative_homographies = []
        for pd_tuple_pair in pd_tuple_pair_iter:
            if exit_event.is_set():
                return
            
            relative_homographies.append(self._build_relative_homography_from_pd_tuples(pd_tuple_pair))
            
        relative_homographies = np.stack(relative_homographies)

        #find the boundaries of the maximal intervals of relative_homographies that do not contain np.nan matrices.
        #note that in each of these intervals (i,j), the j-1th homography is not nan, but the j-th one is.
        #The point is that if the j-1th homography exists, then the j-1th frame is connected to the j-th frame,
        #so the j-th frame should also be in this interval
        relative_homography_intervals = self._find_relative_homography_intervals(relative_homographies)

        #print '   found the relative homography intervals: ', relative_homography_intervals
        sub_interval_datas = []
        for sub_interval in relative_homography_intervals:
            if sub_interval[1] - sub_interval[0] < self._min_subsequence_length:
                continue

            #recall that rel_hom[sub_interval[1]-1] is not nan, but rel_hom[sub_interval[1]]. So when we take the slice,
            #we get only homographies that are not nan
            frame_datas = [RawFrameData(frame, H)
                           for frame,H in enumerate(relative_homographies[sub_interval[0]:sub_interval[1]])]

            #sub_interval is in the "relative coordinate" system, in which the first frame of interval_data.interval has
            #index 0. convert to "absolute coordinates", in which the first frame in the video has index 0
            sub_interval_abs = (first_frame + sub_interval[0], first_frame + sub_interval[1])
            #print '   building IntervalData with interval: (%d, %d)' % sub_interval_abs
            sub_interval_datas.append(IntervalData(sub_interval_abs, frame_datas))

        sub_interval_data_iter = IteratorLen.from_list(sub_interval_datas)
        self._save_interval_data_iter(output_parent_dir, sub_interval_data_iter)

        return
    
    def generate_data(self, output_parent_dir, pd_directory_dict):
        '''
        output_parent_dir - a directory name
        pd_directory_dict - a dictionary with items (PreprocessingDataType attr, directory name).

        generate relative homographies between the frames in the pd directories, and save them in output_parent_dir
        '''
        from ..preprocessing_data_loader import PreprocessingDataLoader
        
        #get an IteratorLen of IntervalData objects. Each frame provides access to an image and an annotation
        input_interval_data_iter = PreprocessingDataLoader.load_data(pd_directory_dict)

        #for each interval, build an iterator of IntervalData objects which store subsequences of frames that are connected
        #by relative homographies. Save these ID objects in the output parent directory

        # p = Pool(self._num_processors)
        # pool_worker = functools.partial(generate_relative_homography_subsequences_worker, self, output_parent_dir)
        # #for retval in p.imap(pool_worker, input_interval_data_iter):
        # for retval in map(pool_worker, input_interval_data_iter):
        #     continue

        rh_calculator = RelativeHomographyCalculator(self, output_parent_dir)
        p = Pool(self._num_processors, rh_calculator, input_interval_data_iter)
        p.run()
        
        return

class RelativeHomographyCalculator(DataProcessor):
    '''
    This DataProcessor is in charge of calculating the relative homography between frames in an interval
    '''
    def __init__(self, rh_controller, output_parent_dir):
        self._rh_controller = rh_controller
        self._output_parent_dir = output_parent_dir

    def process_data(self, interval_data, exit_event):
        self._rh_controller._generate_relative_homography_subsequences(self._output_parent_dir, interval_data,
                                                                       exit_event)
        return
    
def generate_relative_homography_subsequences_worker(rh_controller, output_parent_dir, interval_data):
    '''
    output_parent_dir - the directory where we store the interval subsequences
    rh_controller - a RelativeHomographyController object
    interval_data - an IntervalData objects whose frames are FrameDataTuple objects that contain an image and an annotation

    return a list of IntervalData objects. Each one represents a subsequence of frames in the initial interval data that
    are connected by a relative homography. Each frame in these IntervalData objects is a RawFrameData object that stores a
    homography to the next frame
    '''
    time_str = datetime.now().isoformat()
    print '[%s] generating relative homographies in interval (%d,%d) ...' % ((time_str,) + interval_data.interval)
    rh_controller._generate_relative_homography_subsequences(output_parent_dir, interval_data)
    return

