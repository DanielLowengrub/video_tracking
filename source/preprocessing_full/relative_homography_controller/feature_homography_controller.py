import cv2
import numpy as np
from .relative_homography_controller import RelativeHomographyController
from ...image_processing.homography_calculator.feature_homography_calculator import FeatureHomographyCalculator
from ...image_processing.image_annotator.image_annotation import ImageAnnotation
from ...image_processing.image_annotator.soccer_annotation import SoccerAnnotation
from ...auxillary import mask_operations

class FeatureHomographyController(RelativeHomographyController):
    '''
    This implementation of the relative homography generator uses feature detection to
    compute the homography between two images.
    '''

    def __init__(self, num_processors, min_subsequence_length,
                 x_subdivision=3, y_subdivision=3, players_kernel_radius=20, sidelines_kernel_radius=10):
        '''
        x_subdivision, y_subdivision - integers.
        players_kernel_radius, sidelines_kernel_radius - integers

        Subdivide the x axis and y axis into these many pieces and obtain a tiling of the images.
        Then dilate the player mask by the given player kernel radius and remove it from the annotation.
        We also dilate the sidelines mask by the given kernel.
        We then try to match features tile by tile. In each tile, we seperately mask the grass and sidelines.
        '''
        super(FeatureHomographyController, self).__init__(num_processors, min_subsequence_length)
        
        self._homography_calculator = FeatureHomographyCalculator()
        self._x_subdivision = x_subdivision
        self._y_subdivision = y_subdivision

        self._players_kernel = np.ones((players_kernel_radius, players_kernel_radius), np.uint8)
        self._sidelines_kernel = np.ones((sidelines_kernel_radius, sidelines_kernel_radius), np.uint8)
        
    def _get_grass_sideline_annotation(self, annotation):
        '''
        annotation - a SoccerAnnotation object

        We return a new SoccerAnnotation where all of the players have been put in the background.
        '''
        annotation_array = annotation.get_annotation_array().copy()

        #dilate the player mask then remove it from the annotation
        players_cv_mask = mask_operations.convert_to_cv_mask(annotation.get_players_mask())
        players_cv_mask = cv2.dilate(players_cv_mask, self._players_kernel, iterations=1)
        annotation_array[players_cv_mask > 0] = SoccerAnnotation.irrelevant_value()

        #also dilate the sidelines mask
        sidelines_cv_mask = mask_operations.convert_to_cv_mask(annotation.get_sidelines_mask())
        sidelines_cv_mask = cv2.dilate(sidelines_cv_mask, self._sidelines_kernel, iterations=1)
        annotation_array[sidelines_cv_mask > 0] = SoccerAnnotation.sidelines_value()
        
        annotation = SoccerAnnotation(annotation_array)
    
        #subdivide the annotation
        relevant_annotation_values = (SoccerAnnotation.grass_value(), SoccerAnnotation.sidelines_value())
        annotation = annotation.subdivide_annotation(self._x_subdivision, self._y_subdivision, relevant_annotation_values)
        
        return annotation

    def _build_relative_homography(self, image_0, annotation_0, image_1, annotation_1):
        '''
        image_0, image_1 - numpy arrays with shape (n,m,3) and type uint8
        annotation_0, annotation_1 - SoccerAnnotation objects annotating image_0 and image_1 resp.

        Return a homography between the soccer fields in image_0 and image_1.
        If we could not find one, return a numpy array with shape (3,3) with values np.nan
        '''
        # image_0 = image_0.copy()
        # image_1 = image_1.copy()
        # annotation_0 = SoccerAnnotation(annotation_0.get_annotation_array().copy())
        # annotation_1 = SoccerAnnotation(annotation_1.get_annotation_array().copy())
        
        #we first remove the players from the annotations since we don't want to use them, and also subdivide the annotations
        annotation_0 = self._get_grass_sideline_annotation(annotation_0)
        annotation_1 = self._get_grass_sideline_annotation(annotation_1)

        #now compute the homography between the images using these annotations
        self._homography_calculator.set_annotations(annotation_0, annotation_1)
        H = self._homography_calculator.calculate_homography(image_0, image_1)

        if H is not None:
            return H

        else:
            return np.empty((3,3)) * np.nan
