import itertools
import cv2
import collections
from ..preprocessing_visualization_controller import PreprocessingVisualizationController
from ..image_controller import ImageController
from ..annotation_controller.annotation_controller import AnnotationController
from .relative_homography_controller import RelativeHomographyController
from ...image_processing.contour_generator.edges_contour_generator import EdgesContourGenerator
from ..frame_data.frame_data import FrameData
from ..interval_data import IntervalData
from ...auxillary import planar_geometry
from ...auxillary.iterator_len import IteratorLen

RHVisualizationTuple = collections.namedtuple('RHVisualizationTuple', ['H', 'src_image', 'src_annotation', 'tgt_image'])

class RHVisualizationData(FrameData):
    '''
    This gives access to the data required to visualize the relative homography from frame i to frame i+1.
    In particular, it stores the frame data associated to the images from frame i and i+1, and the annotation in frame i.
    '''
    def __init__(self, frame, homography_fd, src_image_fd, src_annotation_fd, tgt_image_fd):
        '''
        frame - a frame index. this is the index of the frame which is the source of the homography
        homography_fd - a FrameData object with access to a homography matrix
        src_image_fd - a FrameData object with access to the image which is the source of the homography
        src_annotation_fd - a FrameData object with access to the annotation of the source of the homography
        tgt_image_fd - a FrameData object with access to the image which is the target of the homography
        '''
        super(RHVisualizationData, self).__init__(frame)

        self._homography_fd = homography_fd
        self._src_image_fd = src_image_fd
        self._src_annotation_fd = src_annotation_fd
        self._tgt_image_fd = tgt_image_fd

    def build_data(self):
        '''
        return - an RHVisualizationTuple
        '''
        H = self._homography_fd.build_data()
        src_image = self._src_image_fd.build_data()
        src_annotation = self._src_annotation_fd.build_data()
        tgt_image = self._tgt_image_fd.build_data()

        return RHVisualizationTuple(H, src_image, src_annotation, tgt_image)
        
class RelativeHomographyVisualizationController(PreprocessingVisualizationController):
    '''
    This is a SVC that generates visualizations of annotations.
    '''

    def __init__(self, num_processors, contour_color, contour_thickness):
        '''
        num_processors - the number of available processors
        grass_color - the viz color for grass
        sidelines_color - the vis color for sidelines
        players_color - the viz color for sidelines
        alpha - all the visualization colors are applied to the image with this alpha
        '''
        super(RelativeHomographyVisualizationController, self).__init__(num_processors)

        self._contour_color     = contour_color
        self._contour_thickness = contour_thickness
        
        self._contour_generator = EdgesContourGenerator()
        
    def _build_frame_data_visualization(self, frame_data):
        '''
        frame_data - a RHVisualizationData object
        
        return - an image which represents a visualization of the data
        '''
        print '   drawing visualization for frame %d.' % frame_data.frame
        
        #load the frame data
        rh_visualization_tuple = frame_data.build_data()
        H =              rh_visualization_tuple.H
        src_image =      rh_visualization_tuple.src_image
        src_annotation = rh_visualization_tuple.src_annotation
        tgt_image =      rh_visualization_tuple.tgt_image

        #find the contours in the source image
        src_contours = self._contour_generator.generate_contours(src_image, src_annotation.get_soccer_field_mask())

        #transform them to the target image via H
        transformed_contours = planar_geometry.apply_homography_to_contours(H, src_contours)
        transformed_cv_contours = [c.get_cv_contour() for c in transformed_contours]
        
        #draw them on the target image
        cv2.drawContours(tgt_image, transformed_cv_contours, -1, self._contour_color, self._contour_thickness)

        return tgt_image
    
    def _build_rh_visualization_interval_data_iter(self, image_interval_data_iter, annotation_interval_data_iter,
                                                   rh_interval_data_iter):
        '''
        image_interval_data_iter - an iterator of IntervalData objects storing images
        annotation_interval_data_iter - an iterator of IntervalData objects storing annotations
        rh_interval_data_iter - an iterator of IntervalData objects storing relative homographies

        return - an iterator of IntervalData objects whose frames are RHVisualizationData objects
        '''
        interval_datas_iter = itertools.izip(image_interval_data_iter, annotation_interval_data_iter, rh_interval_data_iter)
        for image_interval_data, annotation_interval_data, rh_interval_data in interval_datas_iter:
            #build in IntervalData object out of the three IntervalData objects
            #we assume that all three interval data objects have the same interval
            interval = image_interval_data.interval

            rh_visualization_datas = []
            start_image_fd = next(image_interval_data)
            frame_datas_iter = itertools.izip(image_interval_data, annotation_interval_data, rh_interval_data)
            for end_image_fd, start_annotation_fd, rh_fd in frame_datas_iter:
                #rh_fd stores a homography from start_image_data to end_image_data
                frame = start_image_fd.frame
                rh_visualization_data = RHVisualizationData(frame, rh_fd, start_image_fd, start_annotation_fd, end_image_fd)
                rh_visualization_datas.append(rh_visualization_data)

                #update the start frame data
                start_image_fd = end_image_fd

            yield IntervalData(interval, rh_visualization_datas)
            
    def generate_visualizations(self, visualization_parent_dir, image_directory, annotation_directory,
                               relative_homography_directory):
        '''
        visualization_parent_directory - the directory in which to store the visualizations
        image_directory - the directory containing the images
        annotation_directory - the directory containing the annotations
        relative_homography_directory - the directory containing the relative homographies

        we assume that all of the data directories have the same intervals

        save visualizations of the relative homographies in output_parent_dir
        '''
        print 'image directory: ', image_directory
        image_interval_data_iter = ImageController.load_data(image_directory)
        annotation_interval_data_iter = AnnotationController.load_data(annotation_directory)
        rh_interval_data_iter = RelativeHomographyController.load_data(relative_homography_directory)

        #use the interval data iterators to build an iterator of interval data objects whose frame contain RHVisualizationData
        #objects
        rh_visualization_interval_data_iter = self._build_rh_visualization_interval_data_iter(image_interval_data_iter,
                                                                                              annotation_interval_data_iter,
                                                                                              rh_interval_data_iter)
        
        rh_interval_data_iter = IteratorLen(len(image_interval_data_iter), rh_visualization_interval_data_iter)

        #generate visualizations for each of the RHVisualizationData objects in rh_interval_data_iter
        self._generate_interval_data_iter_visualization(visualization_parent_dir, rh_interval_data_iter)

        return
        
