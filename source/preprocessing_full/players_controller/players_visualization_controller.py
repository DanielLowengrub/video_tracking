import numpy as np
import cv2
from ..sequential_visualization_controller import SequentialVisualizationController
from ....testers.auxillary import primitive_drawing
from ...auxillary import planar_geometry
from ...auxillary.soccer_field_geometry import SoccerFieldGeometry
from ....testers.auxillary import primitive_drawing, label_shapes
from ...auxillary import world_to_image_projections

class PlayersVisualizationController(SequentialVisualizationController):
    '''
    This is a SVC that generates visualizations of highlighted shapes.
    '''

    def __init__(self, num_processors, contour_color, ellipse_color, ellipse_thickness):
        '''
        num_processors - the number of available processors
        contour_color - the color of the player contours
        ellipse_color - the color of the ellipses around the players
        ellipse_thickness - the thickness of the ellipses
        '''
        super(PlayersVisualizationController, self).__init__(num_processors)

        self._contour_color = contour_color
        self._ellipse_color = ellipse_color
        self._ellipse_thickness = ellipse_thickness

    def _draw_players(self, image, camera, players):
        '''
        image - a numpy array with shape (n,m,3) and type np.uint8
        camera - a CameraMatrix object
        players - a list of PlayerData objects
        '''
        #first draw the contours on the image
        cv_contours = [player.contour.get_cv_contour() for player in players]
        cv2.drawContours(image, cv_contours, -1, self._contour_color, 2)

        #now draw the ellipses
        ellipses = [world_to_image_projections.project_player_to_image(camera, player.position) for player in players]
        for ellipse in ellipses:
            primitive_drawing.draw_ellipse(image, ellipse, self._ellipse_color, self._ellipse_thickness)

        return
    
    def _build_frame_data_visualization(self, frame_data):
        '''
        frame_data - a FrameDataTuple object which provides access to an image, camera, and list of PlayerDatas
        
        return - an image which represents a visualization of the data
        '''
        #build a PreprocessingDataTuple object. It is assumed to contain an image and an annotation
        pd_tuple = frame_data.build_data()
        image = pd_tuple.image
        camera = pd_tuple.camera
        players = pd_tuple.players

        self._draw_players(image, camera, players)

        return image
        
        
        
