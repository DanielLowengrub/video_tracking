from ..player_contour import PlayerContour
from .players_controller import PlayersController, PlayerData

class EllipseMatchingPlayersController(PlayersController):
    '''
    This PlayersController finds players by trying to match contors in the image to player sized ellipses.
    '''

    def __init__(self, num_processors, min_ellipse_area, min_ellipse_matching_score):
        '''
        min_ellipse_area - the minimum area that an ellipse can have to be considered a player
        min_ellipse_matching_score - the ellipse matching score is a number between 0 and 1 that measures how well an ellipse
                                     fits a contour. The higher the number the better the fit. 
                                     We only consider a contour to be a player if it matches a player shaped ellipse with
                                     a score of at least min_ellipse_matching_score.

        '''
        super(EllipseMatchingPlayersController, self).__init__(num_processors)
        
        self._min_ellipse_area = min_ellipse_area
        self._min_ellipse_matching_score = min_ellipse_matching_score

    def _build_players(self, annotation, camera):
        '''
        annotation - a SoccerAnnotation object
        camera - a CameraMatrix object
    
        return - a list of PlayerData objects
        '''
        #first extract the potential players contours from the annotation
        player_contours = PlayerContour.build_player_contours_from_annotation(annotation)
        
        #use each player contour to compute an ellipse.
        #The ellipse is obtained by projecting an ellipsoid from absolue world coords to the image using the camera.
        #The ellipsoid is an approximation to a player that is standing at the
        #feet position of the player contour
        ellipses = [pc.compute_ellipse_approximation(camera) for pc in player_contours]

        #filter out the player contours that do not match with their ellipse, or whose ellipse is too small
        player_contours = [pc for pc,E in zip(player_contours,ellipses)
                           if (E.get_area() >= self._min_ellipse_area and
                               pc.evaluate_match(E) >= self._min_ellipse_matching_score and
                               not pc.touches_image_boundary())]

        # print '   found %d players' % len(player_contours)
        
        H = camera.get_homography_matrix()
        player_positions = [pc.compute_feet_absolute_position(H) for pc in player_contours]

        player_datas = [PlayerData(player_contour.get_contour(),position)
                        for player_contour,position in zip(player_contours, player_positions)]

        return player_datas
