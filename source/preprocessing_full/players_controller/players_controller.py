import numpy as np
import os
import collections
import pickle
from ..sequential_pd_controller import SequentialPDController
from ...data_structures.contour import Contour

'''
PlayerData: This stores information related to a player.
contour: a Contour object
position: a numpy array with shape (2,) and type np.float32
'''
PlayerData = collections.namedtuple('PlayerData', ['contour', 'position'])

class PlayersController(SequentialPDController):
    '''
    This is a SequentialPDController that is in charge of finding players in the soccer field
    '''

    CONTOUR_BASENAME = 'contour.npy'
    POSITION_BASENAME = 'position.npy'
    
    def __init__(self, num_processors):
        super(PlayersController, self).__init__(num_processors)
        
    def _save_data_in_frame_dir(self, frame_dir, player_datas):
        '''
        frame_dir - a frame directory name
        player_datas - a list of PlayerData objects

        save the player data objects in the frame directory
        '''
        if len(player_datas) > 0:
            player_cv_contours = np.array([pd.contour.get_cv_contour() for pd in player_datas])
            player_positions = np.vstack([pd.position for pd in player_datas])

        else:
            player_cv_contours = np.array([])
            player_positions = np.array([])
            
        contour_filename = os.path.join(frame_dir, self.CONTOUR_BASENAME)
        position_filename = os.path.join(frame_dir, self.POSITION_BASENAME)

        np.save(contour_filename, player_cv_contours)
        np.save(position_filename, player_positions)

        return
    
    @classmethod
    def load_data_from_frame_dir(cls, frame_dir):
        '''
        frame_dir - a directory name
        return - a CameraMatrix
        '''
        #print '   loading players from: %s' % frame_dir
        
        contour_filename  = os.path.join(frame_dir, cls.CONTOUR_BASENAME)
        position_filename = os.path.join(frame_dir, cls.POSITION_BASENAME)

        player_cv_contours = np.load(contour_filename)
        player_positions = np.load(position_filename)

        player_datas = [PlayerData(Contour(cv_contour, None), position)
                        for cv_contour,position in zip(player_cv_contours,player_positions)]

        return player_datas
    
    def _build_players(self, annotation, camera):
        '''
        annotation - a SoccerAnnotation object
        camera - a CameraMatrix object
    
        return - a list of PlayerData objects
        '''
        raise NotImplementedError('_build_players has not been implemented')
    
    def _build_data_from_pd_tuple(self, pd_tuple):
        '''
        pd_tuple - a PreprocessingDataTuple object
        return - a list of PlayerData objects.
        '''
        return self._build_players(pd_tuple.annotation, pd_tuple.camera)

