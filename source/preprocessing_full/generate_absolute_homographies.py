import os
import cv2
import numpy as np
from .absolute_homography_controller.linear_interpolator_controller import LinearInterpolatorController
from .preprocessing_data_type import PreprocessingDataType

#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts'
#IMAGE_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/test_data/'

#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'

OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'

#INPUT_DATA_NAME = 'images-8_15-9_31'
#INPUT_DATA_NAME = 'images-0_00-1_00'
INPUT_DATA_NAME = 'images-0_00-10_00'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_full', 'data', INPUT_DATA_NAME)

RELATIVE_HOMOGRAPHY_DIRECTORY   = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'relative_homography')
OPTIMIZED_HOMOGRAPHY_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'optimized_homography')

ABSOLUTE_HOMOGRAPHY_DIRECTORY   = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'absolute_homography')

NUM_PROC = 8
MIN_FRAME_DISTANCE = 15

def generate_data(output_parent_directory, relative_homography_directory, absolute_homography_directory):
    '''
    output_parent_directory - the directory in which we will save the annotations
    relative_homography_directory - a directory storing relative homographies
    absolute_homography_directory - a directory storing samples of absolute homographies
    '''
    if not os.path.isdir(output_parent_directory):
        print 'making directory: ', output_parent_directory
        os.mkdir(output_parent_directory)

    absolute_homography_controller = LinearInterpolatorController(NUM_PROC, MIN_FRAME_DISTANCE)
    absolute_homography_controller.generate_data(output_parent_directory,
                                                 relative_homography_directory, absolute_homography_directory)

    return

if __name__ == '__main__':
    generate_data(ABSOLUTE_HOMOGRAPHY_DIRECTORY, RELATIVE_HOMOGRAPHY_DIRECTORY, OPTIMIZED_HOMOGRAPHY_DIRECTORY)
        
        
