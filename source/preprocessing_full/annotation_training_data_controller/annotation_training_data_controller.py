import numpy as np
import os
from ..sequential_pd_controller import SequentialPDController
from ..frame_data.frame_data_loader import FrameDataLoader

class AnnotationTrainingValues(object):
    '''
    This stores the possible values that a pixel can be annotated with in the training dataset.
    '''
    BACKGROUND = 0
    GRASS      = 1
    SIDELINE   = 2
    PLAYER     = 3
    UNKNOWN    = 4

    labels = [BACKGROUND, GRASS, SIDELINE, PLAYER]

class AnnotationTrainingDataController(SequentialPDController):
    '''
    This is a SequentialPDController that deals with training data for image annotation NNs
    '''

    #this is how we store the annotations in their frame directory
    ANNOTATION_BASENAME = 'annotation.npy'

    def __init__(self, num_processors, soccer_field):
        super(AnnotationTrainingDataController, self).__init__(num_processors)

        self._soccer_field = soccer_field
        
    @classmethod
    def _get_annotation_filename(cls, frame_dir):
        '''
        frame_dir - the name of a directory containing an annotation
        
        return - the filename of the annotation
        '''
        annotation_filename = os.path.join(frame_dir, cls.ANNOTATION_BASENAME)
        return annotation_filename    

    def _save_data_in_frame_dir(self, frame_dir, annotation):
        '''
        frame_dir - a frame directory name
        annotation - a numpy array with shape (n,m) and type np.int32.

        save the soccer annotation in the frame directory
        '''
        filename = self._get_annotation_filename(frame_dir)
        #print '      saving annotation to: %s' % filename
        np.save(filename, annotation)
        return

    @classmethod
    def load_data_from_frame_dir(cls, frame_dir):
        '''
        frame_dir - a directory name
        return - a numpy array with shape (n,m) and type np.int32.
        '''
        filename = cls._get_annotation_filename(frame_dir)
        #print '   loading annotation from: %s' % filename
        annotation = np.load(filename)
        return annotation

    def _build_training_data(self, H, annotation, highlighted_shapes, labelled_shapes, players):
        '''
        H - a numpy array with shape (3,3). It represents a homography from abs coords to img coords
        annotation - a SoccerAnnotation
        highlighted_shapes - a list of HighlightedShape objects
        lablled_shapes - dictionary with items (soccer shape key, highlighted shape index)
        players - a list of PlayerData objects

        return - a numpy array with shape (n,m) and type np.int32. The values of the array are the values defined
           in AnnotationTrainingValues

        If we could not build training data, return None
        '''
        raise NotImplementedError('_build_training_data has not been implemented')
    
    def _build_data_from_pd_tuple(self, pd_tuple):
        '''
        pd_tuple - a PreprocessingDataTuple which stores an annotation, highlighted shapes, labelled shapes and players
        return - a list of HighlightedShape objects

        If there was not enough information to build training data, return None
        '''
        return self._build_training_data(pd_tuple.absolute_homography, pd_tuple.annotation,
                                         pd_tuple.highlighted_shapes, pd_tuple.labelled_shapes, pd_tuple.players)

