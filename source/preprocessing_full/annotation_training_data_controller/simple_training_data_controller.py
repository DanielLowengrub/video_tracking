import numpy as np
import cv2
from scipy.ndimage import morphology
from .annotation_training_data_controller import AnnotationTrainingDataController, AnnotationTrainingValues
from ...auxillary.soccer_field_geometry import SoccerFieldGeometry
from ...auxillary.contour_shape_overlap.highlighted_shape import HighlightedShape
from ...auxillary import planar_geometry

DEBUG = False

class SimpleTrainingDataController(AnnotationTrainingDataController):
    '''
    This implementation of AnnotationTrainingDataController works as follows:
    0) the annotation grass mask is marked GRASS
    1) the highlighted parts of the labelled shapes are filled in.
       the predicted highlighted parts are marked UNKNOWN
       the actual highlighted parts are marked SIDELINE
    2) the unlabelled shapes are marked as UNKNOWN (with large thickness, ~25)
    3) the pixels in each player contour are labelled as PLAYER
    4) the rest of the annotation player mask is labelled as UNKNOWN
    5) the annotation background is marked BACKGROUND
    6) the pixels between different labels are marked as UNKNOWN
    '''

    def __init__(self, num_processors, soccer_field, matched_line_thickness, matched_ellipse_thickness,
                 unmatched_thickness, predicted_shape_dilation):
        super(SimpleTrainingDataController, self).__init__(num_processors, soccer_field)

        self._matched_thickness_dict = {HighlightedShape.LINE: matched_line_thickness,
                                        HighlightedShape.ELLIPSE: matched_ellipse_thickness}
        
        self._unmatched_thickness = unmatched_thickness
        self._predicted_shape_dilation = predicted_shape_dilation
        
    def _mark_matched_sidelines(self, training_annotation, H, highlighted_shapes, labelled_shapes,
                                image_region, anchor_sign):
        '''
        training_annotation - a numpy array with shape (n,m) and type np.int32
        H - a numpy array with shape (3,3). It represents a homography from abs coords to img coords
        highlighted_shapes - a list of HighlightedShape objects
        lablled_shapes - dictionary with items (soccer shape key, highlighted shape index)
        
        mark the sidelines on training_annotation
        '''
        for k,i in labelled_shapes.iteritems():
            if DEBUG:
                print 'labelling the matched shape: ', k
                
            highlighted_shape = highlighted_shapes[i]
                
            soccer_hs = self._soccer_field.get_highlighted_shape(k)
            predicted_hs = soccer_hs.apply_homography(H, image_region, anchor_sign)

            if DEBUG:
                from ...auxillary.fit_points_to_shape.line import Line
                from ....testers.auxillary import primitive_drawing, highlighted_shape_drawing

                image = np.zeros(training_annotation.shape + (3,), np.uint8)
                highlighted_shape_drawing.draw_highlighted_shape(image, highlighted_shape)
                cv2.imshow('highlighted shape', image)
                cv2.waitKey(0)
                cv2.destroyAllWindows()

                image = np.zeros(training_annotation.shape + (3,), np.uint8)
                highlighted_shape_drawing.draw_highlighted_shape(image, predicted_hs)
                cv2.imshow('predicted highlighted shape', image)
                cv2.waitKey(0)
                cv2.destroyAllWindows()

                abs_image = cv2.imread('/Users/daniel/Dropbox/soccer/daniel_refactored/test_data/sideline_mask.png', 1)
                line = Line.from_parametric_representation(H[2])
                primitive_drawing.draw_line(abs_image, line, (0,255,0))
                cv2.imshow('line at infinity', abs_image)
                cv2.waitKey(0)
                cv2.destroyAllWindows()

            predicted_hs = predicted_hs.snap_to_shape(highlighted_shape.get_shape_with_position())
            predicted_hs = predicted_hs.interval_dilation(self._predicted_shape_dilation)

            highlighted_shape = highlighted_shape.intersect_intervals(predicted_hs)
            highlighted_shape = highlighted_shape.fill_interval_gaps()
            
            thickness = self._matched_thickness_dict[highlighted_shape.get_shape_type()]
            predicted_mask = predicted_hs.generate_mask(training_annotation, thickness=thickness) > 0
            sideline_mask  = highlighted_shape.generate_mask(training_annotation, thickness=thickness) > 0

            if DEBUG:
                cv2.imshow('predicted mask', np.float32(predicted_mask))
                cv2.waitKey(0)
                cv2.destroyAllWindows()
                cv2.imshow('sideline mask', np.float32(sideline_mask))
                cv2.waitKey(0)
                cv2.destroyAllWindows()

            training_annotation[predicted_mask] = AnnotationTrainingValues.UNKNOWN
            training_annotation[sideline_mask]  = AnnotationTrainingValues.SIDELINE
            
        return

    def _mark_unmatched_sidelines(self, training_annotation, H, labelled_shapes, image_region, anchor_sign):
        '''
        training_annotation - a numpy array with shape (n,m) and type np.int32
        H - a numpy array with shape (3,3). It represents a homography from abs coords to img coords
        lablled_shapes - dictionary with items (soccer shape key, highlighted shape index)
        
        mark the sidelines which have not been labelled on training_annotation
        '''
        soccer_shape_dict = self._soccer_field.get_highlighted_shape_dict().copy()

        #these shapes will never be matched to a highlighted shape
        del soccer_shape_dict[(SoccerFieldGeometry.PENALTY_CIRCLE, SoccerFieldGeometry.RIGHT)]
        del soccer_shape_dict[(SoccerFieldGeometry.PENALTY_CIRCLE, SoccerFieldGeometry.LEFT)]

        for k, soccer_shape in soccer_shape_dict.iteritems():
            #skip the shapes that have been matched to image shapes
            if k in labelled_shapes:
                continue
                
            #map the shape to the image
            soccer_shape = soccer_shape.apply_homography(H, image_region, anchor_sign)                

            #if it is not possible to apply the homography to the shape
            if soccer_shape is None:
                continue
            
            soccer_shape = soccer_shape.interval_dilation(self._unmatched_thickness)
            shape_mask = soccer_shape.generate_mask(training_annotation, thickness=self._unmatched_thickness) > 0

            if DEBUG:
                print 'drawing the unmatched shape: ', k
                print '   shape params: ', soccer_shape
                cv2.imshow('shape mask', np.float32(shape_mask))
                cv2.waitKey(0)
                cv2.destroyAllWindows()

            training_annotation[shape_mask] = AnnotationTrainingValues.UNKNOWN

        return
    
    def _mark_players(self, training_annotation, annotation, players):
        '''
        training_annotation - a numpy array with shape (n,m) and type np.int32
        annotation - a SoccerAnnotation
        players - a list of PlayerData objects
        '''
        #use the players to get an accurate players mask. (as opposed to the players mask in the annotation)
        accurate_players_mask = np.zeros(annotation.get_image_shape()[:2], np.float32)
        for player in players:
            player.contour.add_mask(accurate_players_mask)

        training_annotation[annotation.get_players_mask() > 0] = AnnotationTrainingValues.UNKNOWN
        training_annotation[accurate_players_mask > 0]         = AnnotationTrainingValues.PLAYER

        return

    def _compute_boundary(self, mask):
        '''
        mask - a numpy array with shape (n,m) and type np.bool
        return - a numpy array with shape (n,m) and type np.bool. It contains the elements of the mask that touch
           pixels that are not in the mask.
        '''
        complement = np.logical_not(mask)

        kernel = np.ones((3,3), np.bool)
        dilated_complement = morphology.binary_dilation(complement, kernel)

        boundary = np.logical_and(mask, dilated_complement)

        return boundary
    
    def _mark_boundaries(self, training_annotation):
        '''
        training_annotation - a numpy array with shape (n,m) and type np.int32
        
        set pixels on the boundary between two different training values to UNKNOWN
        '''
        for label in (AnnotationTrainingValues.BACKGROUND, AnnotationTrainingValues.GRASS,
                      AnnotationTrainingValues.SIDELINE, AnnotationTrainingValues.PLAYER):
            boundary_mask = self._compute_boundary(training_annotation == label)
            training_annotation[boundary_mask] = AnnotationTrainingValues.UNKNOWN

        return
    
    def _build_training_data(self, H, annotation, highlighted_shapes, labelled_shapes, players):
        '''
        H - a numpy array with shape (3,3). It represents a homography from abs coords to img coords
        annotation - a SoccerAnnotation
        highlighted_shapes - a list of HighlightedShape objects
        lablled_shapes - dictionary with items (soccer shape key, highlighted shape index)
        players - a list of PlayerData objects

        return - a numpy array with shape (n,m) and type np.int32. The values of the array are the values defined
           in AnnotationTrainingValues

        If we could not build training data, return None
        '''
        image_shape = annotation.get_image_shape()
        image_region = planar_geometry.get_image_region(annotation.get_annotation_array())
        anchor_sign = planar_geometry.compute_anchor_sign_from_target_shape(H, image_shape)
        
        #make sure that all of the highlighted shapes are labelled
        labelled_shape_indices = set(labelled_shapes.values())
        if not len(labelled_shape_indices) == len(highlighted_shapes):
            print 'not all highlighted shapes are labelled. can not build training data'
            return None

        #intialize the training annotation
        training_annotation = np.ones(annotation.get_image_shape()[:2], np.int32) * AnnotationTrainingValues.UNKNOWN

        training_annotation[annotation.get_grass_mask()>0] = AnnotationTrainingValues.GRASS
        self._mark_unmatched_sidelines(training_annotation, H, labelled_shapes, image_region, anchor_sign)
        self._mark_matched_sidelines(training_annotation, H, highlighted_shapes, labelled_shapes,
                                     image_region, anchor_sign)
        self._mark_players(training_annotation, annotation, players)
        training_annotation[annotation.get_background_mask()>0] = AnnotationTrainingValues.BACKGROUND

        self._mark_boundaries(training_annotation)

        return training_annotation
