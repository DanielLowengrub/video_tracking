from ...auxillary import mask_operations
from ..sequential_visualization_controller import SequentialVisualizationController

class TrainingDataVisualizationController(SequentialVisualizationController):
    '''
    This is a SVC that generates visualizations of annotation training data
    '''

    def __init__(self, num_processors, label_color_dict, alpha):     
        '''
        num_processors - the number of available processors
        label_color_dict - a dictionary with items (annotation training data label, BGR)
        alpha - all the visualization colors are applied to the image with this alpha
        '''
        super(TrainingDataVisualizationController, self).__init__(num_processors)

        self._label_color_dict = label_color_dict
        self._alpha = alpha

    def _build_frame_data_visualization(self, frame_data):
        '''
        frame_data - a FrameDataTuple object which provides access to an image and training data
        
        return - an image which represents a visualization of the data
        '''
        #build a PreprocessingDataTuple object. It is assumed to contain an image and an annotation
        pd_tuple = frame_data.build_data()
        image = pd_tuple.image
        training_data = pd_tuple.annotation_training_data

        for label, color in self._label_color_dict.iteritems():
            mask = (training_data == label)
            mask_operations.draw_mask(image, mask, color, self._alpha)

        return image
        
        
        
