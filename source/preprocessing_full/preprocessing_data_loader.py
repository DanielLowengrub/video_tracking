import itertools
from ..auxillary import iterator_operations
from .preprocessing_data_type import get_data_controller
from .preprocessing_data_tuple import PreprocessingDataTuple
from .interval_data import IntervalData
from .frame_data.frame_data_tuple import FrameDataTuple
from ..auxillary.iterator_len import IteratorLen

class PreprocessingDataLoader(object):
    '''
    This class is in charge of loading an IteratorLen of IntervalData objects from a dictionary of directories.
    It is a convenience class that deals with the headache of merging IteratorLen objects associated with different data
    types into a single IteratorLen object
    Specifically, it is given a dictionary with items (preprocessing data type, directory name). It loads the different
    data types and packages them into an IteratorLen of IntervalData objects, whose frames contain FrameDataTuple objects.
    
    Each FrameDataTuple object stores a dictionary with items (preprocessing data type, FrameData object), where the FrameData
    objects are loaded from each of the directory.
    '''
    
    @classmethod
    def _merge_interval_data_dict(cls, interval_data_dict):
        '''
        interval_data_dict - a dictionary of IntervalData objects. The keys are PreprocessingDataType attributes. All of the
           IntervalDatas must have the same interval, but not necessarily the same frames.
        
        return - an IntervalData object. It has the same interval as the IDs in the ID dictionary. It's frames are the
           frames which appear in ALL of the IntervalDatas
           Each frame of this IntervalData contains a FrameDataTuple object.

        Example: interval_data_dict = 
        {IMAGE: IntervalData((0,100), (image_fd_0, image_fd_2,...))
         ANNOTATION: IntervalData((0,100), (annotation_fd_0, annotation_fd_1,annotation_fd_2,...))}

        return:
        IntervalData((0,100), (FrameDataTuple(0, {IMAGE:image_fd_0, ANNOTATION:annotation_fd_0})),
                               FrameDataTuple(2, {IMAGE:image_fd_2, ANNOTATION:annotation_fd_2})),...))
        '''
        #all of the IntervalData objects have this interval
        interval = interval_data_dict.values()[0].interval

        #get a list of the frames which are in all of the IntervalData objects
        frames = list(set.intersection(*[set(interval_data.get_frames())
                                         for interval_data in interval_data_dict.itervalues()]))

        #replace each of the interval data objects with a new object which only has
        #FrameData objects for the frames in "frames"
        interval_data_dict = dict((k,interval_data.build_sub_interval_data(frames))
                                  for k,interval_data in interval_data_dict.iteritems())
        
        #this is an iterator of dictionaries. Each dictionary has items (PreprocessingDataType attr, FrameData object)
        #all of the FrameData objects in a given dictionary have the same frame
        frame_data_dict_iter = iterator_operations.izip_dictionary(interval_data_dict)
        
        #this is a list of dictionaries of FrameDataTuple objects. There is one FDT object for each dictionary in
        #frame_data_dict_iter. Since all of the FDs in a dictionary have the same frame, we use the first FrameData object in
        #the dictionary to figure out the frame
        frame_data_tuples = list(FrameDataTuple(fd_dict.values()[0].frame, fd_dict) for fd_dict in frame_data_dict_iter)

        interval_data = IntervalData(interval, frame_data_tuples)

        return interval_data
    
    @classmethod
    def load_data(cls, directory_dictionary):
        '''
        directory_dictionary - a dictionary with items (data_type_key, data_directory)
                               data_type_key is a PreprocessingDataType attribute.
                               data_directory is a directory name

        return an IntervalLen of IntervalData objects. The IntervalData objects store FrameDataTuple objects in each frame

        We assume that the directories store the same intervals.
        '''
        print 'loading data from:'
        print directory_dictionary

        #this is a dictionary with items (data type, IteratorLen of IntervalData objects)
        interval_data_iter_dict = dict((data_type,get_data_controller(data_type).load_data(data_directory))
                                       for data_type,data_directory in directory_dictionary.iteritems())
        
        num_intervals = len(interval_data_iter_dict.values()[0])
            
        #this is an iterator of dictionaries, one dictionary per interval.
        #Each dictionary has items (data type, IntervalData object)
        interval_data_dict_iter = iterator_operations.izip_dictionary(interval_data_iter_dict)

        #for each interval, merge the dictionary of IntervalData objects into a single IntervalData object
        interval_data_iter = itertools.imap(cls._merge_interval_data_dict, interval_data_dict_iter)

        #create an IntervalLen object that stores the iterator length
        interval_data_iter = IteratorLen(num_intervals, interval_data_iter)

        return interval_data_iter
