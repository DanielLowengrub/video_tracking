class TeamLabel(object):
    '''
    a TeamLabel is a label that a player holds which determines what team it is on.
    '''

    TEAM_A = 0  
    TEAM_B = 1
    UNDETERMINED  = -1 #This is the label we assign if the team classifier did not return conclusive results
    NA     = -2        #This is the label we assign if there was not enough data to feed into the classifier
    
    _TEAM_STRINGS = {TEAM_A:'team_A', TEAM_B:'team_B', UNDETERMINED: 'undetermined', NA:'NA'}
    
    @classmethod
    def to_string(cls, team):
        return cls._TEAM_STRINGS[team]
    
