import numpy as np
import cv2
from ..sequential_visualization_controller import SequentialVisualizationController
from ....testers.auxillary import primitive_drawing
from ...auxillary import planar_geometry
from ....testers.auxillary import primitive_drawing

class TeamLabelsVisualizationController(SequentialVisualizationController):
    '''
    This is a SVC that generates visualizations of team lables
    '''
    TEAM_LABEL_COLORS = {-1: (50,50,50), 0:(255,0,0), 1:(0,255,0)}
    
    def __init__(self, num_processors):
        '''
        num_processors - the number of available processors
        '''
        super(TeamLabelsVisualizationController, self).__init__(num_processors)

    def _draw_team_labels(self, image, camera, players, team_labels):
        '''
        image - a numpy array with shape (n,m,3) and type np.uint8
        camera - a CameraMatrix object
        players - a list of PlayerData objects
        team_labels - a numpy array with shape (num players,) and type np.int32
        '''
        #get the player positions on the image
        H = camera.get_homography_matrix()

        if len(players) == 0:
            return
    
        player_positions = np.vstack([player.position for player in players])
        positions_on_image = planar_geometry.apply_homography_to_points(H, player_positions)

        #draw the points at the positions, whose color depends on the label
        for position, label in zip(positions_on_image, team_labels):
            color = self.TEAM_LABEL_COLORS[label]
            primitive_drawing.draw_point(image, position, color)

        return

    def _build_frame_data_visualization(self, frame_data):
        '''
        frame_data - a FrameDataTuple object which provides access to an image, camera, a list of PlayerDatas, and team labels
        
        return - an image which represents a visualization of the data
        '''
        #build a PreprocessingDataTuple object. It is assumed to contain an image and an annotation
        pd_tuple = frame_data.build_data()
        image = pd_tuple.image
        camera = pd_tuple.camera
        players = pd_tuple.players
        team_labels = pd_tuple.team_labels

        self._draw_team_labels(image, camera, players, team_labels)

        return image
        
        
        
