import numpy as np
import os
from ..sequential_pd_controller import SequentialPDController
from ..team_classifier_controller.team_classifier_controller import TeamClassifierController

class TeamLabelsController(SequentialPDController):
    '''
    This is a SequentialPDController that is in charge labelling players
    '''
    OUTLIER_LABEL = -1
    TEAM_LABELS_BASENAME = 'histgorams.npy'
    
    def __init__(self, num_processors, team_classifier_directory):
        '''
        team_labels_directory - a directory containing the output of TeamClassifierController
        '''
        super(TeamLabelsController, self).__init__(num_processors)
        
        self._classifier_dir = team_classifier_directory

        #only load the classifier before building the labels
        self._kmeans_classifier = None
        self._mean_std_distances = None
        
    def _save_data_in_frame_dir(self, frame_dir, team_labels):
        '''
        frame_dir - a frame directory name
        team_labels - a numpy array with shape (num players, ) and type np.int32.

        save the labels in the frame directory
        '''            
        team_labels_filename = os.path.join(frame_dir, self.TEAM_LABELS_BASENAME)
        np.save(team_labels_filename, team_labels)

        return
    
    @classmethod
    def load_data_from_frame_dir(cls, frame_dir):
        '''
        frame_dir - a directory name
        return - a numpy array with shape (num players, ) and type np.int32.
        '''
        print '   loading team labels from: %s' % frame_dir
        team_labels_filename = os.path.join(frame_dir, cls.TEAM_LABELS_BASENAME)
        return np.load(team_labels_filename)
        
    def _build_team_labels(self, player_histograms):
        '''
        player_histograms - a numpy array with shape (num players, num bins)

        return a np array with shape (num players in frame,) and type np.int32
        '''
        
        if len(player_histograms) == 0:
            return np.empty((0,), np.int32)
        
        team_labels = self._kmeans_classifier.predict(player_histograms)
        cluster_centers = self._kmeans_classifier.cluster_centers_

        #compute the distance of each histogram from it's cluster center
        distances_to_center = np.linalg.norm(player_histograms - cluster_centers[team_labels], axis=1)

        #the maximum allowed distance from each center. The max allowed distance from the i-th center is the mean distance
        #plus the std of the distances
        max_distances = self._mean_std_distances.sum(axis=1)
        
        #mark the histograms that are too far away from their centers
        histogram_outliers = distances_to_center > max_distances[team_labels]
        team_labels[histogram_outliers] = self.OUTLIER_LABEL

        return team_labels

    def _initialize_controller(self):
        '''
        load the KMeans classifier
        '''
        self._kmeans_classifier, self._mean_std_distances = TeamClassifierController.load_classifier(self._classifier_dir)
        return
    
    def _build_data_from_pd_tuple(self, pd_tuple):
        '''
        pd_tuple - a PreprocessingDataTuple object
        return - a numpy array with shape (num players,) and type np.int32.
        '''
        return self._build_team_labels(pd_tuple.player_histograms)

