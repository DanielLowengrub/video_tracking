import numpy as np
import cv2
from scipy.ndimage import morphology
from ...auxillary import mask_operations
from .annotation_controller import AnnotationController
from ...image_processing.image_annotator.soccer_annotation import SoccerAnnotation

class StupidFieldMaskController(AnnotationController):
    '''
    This implementation of AnnotationController uses a hard coded hue values to find part of the image that contains
    the soccer field.

    This information is stored in a SoccerAnnotation object. 
    The soccer field is marked as "grass". 
    Note that this is different than a typical annotation in which some of the field is marked as 
    "sidelines" and "players". 
    At this stage, we do not know where the sidelines and players are, so everything is marked as "grass".
    '''
    OPENING_RADIUS = 7
    CLOSING_RADIUS = 14
    
    def __init__(self, num_processors, hud_mask, hue_min, hue_max):
        '''
        hud_mask - a mask that indicates which portions of the image are in a HUD
        hue_min, hue_max - integers. This defines the range of hues that are declared to be grass
        '''
        super(StupidFieldMaskController, self).__init__(num_processors, multiprocessing_mode=self.FRAME_MP)

        self._hud_mask = hud_mask
        
        self._hue_min = hue_min
        self._hue_max = hue_max
        
        self._kernel_opening = np.ones((2*self.OPENING_RADIUS+1, 2*self.OPENING_RADIUS+1), np.bool)
        self._kernel_closing = np.ones((2*self.CLOSING_RADIUS+1, 2*self.CLOSING_RADIUS+1), np.bool)

    def _build_data_from_pd_tuple(self, pd_tuple):
        '''
        pd_tuple - a PreprocessingDataTuple that stores an image
        return - a SoccerAnnotation object that marks all the points in the soccer field as "grass"
        '''
        #extract the hue channel
        image = pd_tuple.image
        img_hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
        img_h = img_hsv[:,:,0]
        
        field_mask = (img_h >= self._hue_min) * (img_h <= self._hue_max)

        # cv2.imshow('raw field mask', np.float32(field_mask))
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()
        
        field_mask = morphology.binary_opening(field_mask, self._kernel_opening)
        field_mask = mask_operations.apply_closing(field_mask, self._kernel_closing)
        
        annotation_array = np.ones(field_mask.shape, np.int32) * SoccerAnnotation.irrelevant_value()
        annotation_array[field_mask] = SoccerAnnotation.grass_value()
        annotation_array[self._hud_mask] = SoccerAnnotation.irrelevant_value()
        
        annotation = SoccerAnnotation(annotation_array)
        
        return annotation


        
