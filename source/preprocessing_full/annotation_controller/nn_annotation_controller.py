import numpy as np
import cv2
import scipy.ndimage
import itertools
import functools
import time
import re
import operator
from datetime import datetime
from multiprocessing import Pool
from ...preprocessing_full import filename_loader
from ...image_processing.image_annotator.soccer_annotation import SoccerAnnotation
from .annotation_controller import AnnotationController
from ..interval_data import IntervalData
from ..frame_data.controller_data import ControllerData
from ...auxillary.iterator_len import IteratorLen
    
class NNAnnotationController(AnnotationController):
    '''
    This class generates SoccerAnnotation objects based on the output of a NN image annotator.
    The NN annotator annotates each pixel with one of the following values:
    0: background 
    1: sideline 
    2: grass 
    3: player

    It also applies closing operations to the background and player masks.
    '''

    NN_TO_SOCCER_ANNOTATION_DICT = {0: SoccerAnnotation.irrelevant_value(),
                                    1: SoccerAnnotation.sidelines_value(),
                                    2: SoccerAnnotation.grass_value(),
                                    3: SoccerAnnotation.players_value()}

    NN_REGEX = re.compile('image_([0-9]+)\.npy')
    
    def __init__(self, num_processors, hud_mask, background_closure_radius, player_closure_radius):
        '''
        num_processors - the number of available processors
        hud_mask - a mask that indicates which portions of the image are in a HUD
        background_closure_radius - an integer. We apply a closure kernel with this radius to the background mask
        player_closure_radius - an integer. We apply a closure kernel with this radius to the player mask 
           (after the bg closure)
        '''
        super(NNAnnotationController, self).__init__(num_processors)

        self._hud_mask = hud_mask
        
        r = background_closure_radius
        self._background_kernel = np.ones((r,r), np.bool)

        r = player_closure_radius
        self._players_kernel = np.ones((r,r), np.bool)

    def build_data(self, nn_mask_filename):
        '''
        nn_mask_filename - the name of a file containing a nn mask
        return - a SoccerAnnotation object
        '''
        nn_mask = np.load(nn_mask_filename)
        annotation = np.zeros(nn_mask.shape, np.int32)
        
        for nn_mask_value, soccer_annotation_value in self.NN_TO_SOCCER_ANNOTATION_DICT.items():
            annotation[nn_mask == nn_mask_value] = soccer_annotation_value

        # #add the hud mask to the background
        # cv2.imshow('hud mask', np.float32(hud_mask))
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        # print 'putting hud in background...'
        annotation[self._hud_mask] = SoccerAnnotation.irrelevant_value()
        
        # cv2.imshow('bg', np.float32(annotation == SoccerAnnotation.irrelevant_value()))
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        #close the background
        background_mask = (annotation == SoccerAnnotation.irrelevant_value())
        background_mask = scipy.ndimage.morphology.binary_closing(background_mask, structure=self._background_kernel)
        annotation[background_mask] = SoccerAnnotation.irrelevant_value()

        # cv2.imshow('bg after closing background', np.float32(annotation == SoccerAnnotation.irrelevant_value()))
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        #close the players
        players_mask = (annotation == SoccerAnnotation.players_value())
        players_mask = scipy.ndimage.morphology.binary_closing(players_mask, structure=self._players_kernel)
        annotation[players_mask] = SoccerAnnotation.players_value()

        # cv2.imshow('bg after closing players', np.float32(annotation == SoccerAnnotation.irrelevant_value()))
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        annotation = SoccerAnnotation(annotation)

        return annotation
    
    def generate_data(self, output_parent_dir, nn_directory):
        '''
        output_parent_directory - the directory in which we will save the annotations
        nn_directory - a directory containing files image_0.npy,...,image_N.npy. 
           Each numpy array has shape (n,m) and type int32.
           The values at each pixel represent one of: grass, player, sideline, background
        '''

        print 'generating annotations from the nn masks in:'
        print nn_directory
                
        enumerated_nn_mask_filenames = filename_loader.load_filenames(nn_directory, self.NN_REGEX)
        num_nn_masks = len(enumerated_nn_mask_filenames)
        
        #all of the annotations will be saved in a single interval
        interval = (0, num_nn_masks - 1)
        print 'interval: ', interval

        frame_datas = [ControllerData(frame, self, (nn_filename,)) for frame,nn_filename in enumerated_nn_mask_filenames]
        interval_data = IntervalData(interval, frame_datas)
        interval_data_iter = IteratorLen(1, iter([interval_data]))

        self._save_interval_data_iter(output_parent_dir, interval_data_iter)
        
        return

        
