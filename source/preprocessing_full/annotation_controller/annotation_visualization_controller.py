from ...auxillary import mask_operations
from ..sequential_visualization_controller import SequentialVisualizationController

class AnnotationVisualizationController(SequentialVisualizationController):
    '''
    This is a SVC that generates visualizations of annotations.
    '''

    def __init__(self, num_processors, grass_color, sidelines_color, players_color, alpha):
        '''
        num_processors - the number of available processors
        grass_color - the viz color for grass
        sidelines_color - the vis color for sidelines
        players_color - the viz color for sidelines
        alpha - all the visualization colors are applied to the image with this alpha
        '''
        super(AnnotationVisualizationController, self).__init__(num_processors)

        self._grass_color = grass_color
        self._sidelines_color = sidelines_color
        self._players_color = players_color
        self._alpha = alpha

    def _build_frame_data_visualization(self, frame_data):
        '''
        frame_data - a FrameDataTuple object which provides access to an image and an annotation
        
        return - an image which represents a visualization of the data
        '''
        #build a PreprocessingDataTuple object. It is assumed to contain an image and an annotation
        pd_tuple = frame_data.build_data()
        image = pd_tuple.image
        annotation = pd_tuple.annotation
        
        mask_operations.draw_mask(image, annotation.get_grass_mask(),     self._grass_color,     self._alpha)
        mask_operations.draw_mask(image, annotation.get_sidelines_mask(), self._sidelines_color, self._alpha)
        mask_operations.draw_mask(image, annotation.get_players_mask(),   self._players_color,   self._alpha)

        return image
        
        
        
