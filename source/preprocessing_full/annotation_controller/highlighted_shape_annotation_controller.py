import numpy as np
import cv2
from .annotation_controller import AnnotationController
from ...image_processing.image_annotator.highlighted_shape_soccer_annotator import HighlightedShapeSoccerAnnotator

class HighlightedShapeAnnotationController(AnnotationController):
    '''
    This implementation of AnnotationController uses a "bad" annotation which contains only grass information,
    and a list of highlighted shapes to compute a better annotation of the image.
    '''
    
    def __init__(self, num_processors, canny_min, canny_max):
        super(HighlightedShapeAnnotationController, self).__init__(num_processors)

        #this object is used to annotate the image based on the field mask and highlighted shapes
        self._soccer_annotator = HighlightedShapeSoccerAnnotator(canny_min, canny_max)

    def _build_data_from_pd_tuple(self, pd_tuple):
        '''
        pd_tuple - a PreprocessingDataTuple that stores an image, a field mask and a list of highlighted shapes
        return - a SoccerAnnotation object
        '''
        field_mask = pd_tuple.annotation.get_soccer_field_mask()
        annotation = self._soccer_annotator.annotate_image(pd_tuple.image, field_mask, pd_tuple.highlighted_shapes)
        return annotation

        
