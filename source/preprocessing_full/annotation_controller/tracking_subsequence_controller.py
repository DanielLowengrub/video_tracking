import os
import re
import functools
import itertools
from multiprocessing import Pool
from datetime import datetime
import numpy as np
from .annotation_controller import AnnotationController
from ...auxillary.iterator_len import IteratorLen

class TrackingSubsequenceController(AnnotationController):
    '''
    This implementation of AnnotationController is in charge of filtering out annotations that can not be used for 
    tracking.
    In the current version, this is done by checking if they contain a high enough percentage of grass.
    '''
    CHUNKSIZE = 20 #this is the number of frames to load onto a processor at a time.
                   #Since all of the frames store annotations,
                   #we don't want to load too many at a time so as to save memory
    
    def __init__(self, num_processors, grass_percentage_threshold, min_subsequence_length):
        '''
        num_processors - the number of available processors
        grass_percentage_threshold - only track frames in which the percentage of grass (between 0 and 1) exceeds this number
        min_subsequence_length - the minimum length of a tracking sequence
        '''
    
        super(AnnotationController, self).__init__(num_processors)

        self._grass_percentage_threshold = grass_percentage_threshold
        self._min_subsequence_length = min_subsequence_length
        
    def _is_tracking_frame_data(self, frame_data):
        '''
        frame_data - a ControllerData object associated to an annotation
        return True iff this frame data should be used for tracking
        '''
        annotation = frame_data.build_data()
        grass_mask = annotation.get_grass_mask()

        grass_area = np.sum(grass_mask)
        total_area = grass_mask.shape[0] * grass_mask.shape[1]
        grass_percentage = grass_area / total_area
        #print '   grass percentage in frame %d: %f' % (frame_data.frame, grass_percentage)
        return grass_percentage > self._grass_percentage_threshold
                            
    def _find_sub_intervals(self, interval_data):
        '''
        interval_data - an IntervalData object whose FrameData objects contain SoccerAnnotations.
        return - a list of tuples (ik,jk) that represent sub intervals of interval_data.interval that can be used for tracking
        '''
        print 'finding subintervals...'
        sub_intervals = []
        start = 0
        end = 0
        
        pool_worker = functools.partial(is_tracking_frame_data_worker, self)
        p = Pool(self._num_processors)
        for is_tracking_frame in p.imap(pool_worker, interval_data, self.CHUNKSIZE):
            #if the frame we just processed was a tracking frame, increment the interval end point so that the interval
            #includes that frame
            if is_tracking_frame:
                #print '   found a tracking frame. start: %d, end: %d' % (start, end)
                end += 1

            #if not, save the current interval (if there is one) and set both the start and end point to the next frame
            else:
                #print '   did not find an interval'
                if (start < end) and (end - start > self._min_subsequence_length):
                    #print '      * adding interval: (%d,%d)' % (start, end-1)
                    sub_intervals.append((start, end - 1))

                end += 1
                start = end
                #print '   start: %d, end: %d' % (start, end)

        #add the last interval
        if (start < end) and (end - start > self._min_subsequence_length):
            sub_intervals.append((start, end - 1))
            
        return sub_intervals
    
    def generate_data(self, output_parent_dir, annotation_directory):
        '''
        output_parent_dir - the directory in which to store the tracking subsequences
        annotation_directory - a directory containing the annotation data that we are going to take subsequences of.
           we assume that the annotation data has only one interval

        find subsequences of the annotation data that can be used for tracking and save them in the 
        '''

        #load the annotation data. We assume it has exactly one IntervalData object
        interval_data_iter = self.load_data(annotation_directory)
        interval_data = next(interval_data_iter)
        #print 'interval data: ', interval_data
        
        #find the sub intervals that can be used for tracking
        sub_intervals = self._find_sub_intervals(interval_data)
        #print 'found sub intervals:'
        #print sub_intervals
        sub_interval_iter = IteratorLen.from_list(sub_intervals)

        #use the sub interval iterator to refine the original annotation directory
        self.refine_intervals(output_parent_dir, annotation_directory, sub_interval_iter)

        return

def is_tracking_frame_data_worker(ts_controller, frame_data):
    '''
    ts_controller - a TrackingSubsequenceController object
    frame_data - a FrameData object

    return True iff the frame data object should be used for tracking
    '''
    time_str = datetime.now().isoformat()
    #print '[%s] checking frame %d...' % (time_str, frame_data.frame)

    return ts_controller._is_tracking_frame_data(frame_data)

        
    
