import numpy as np
import os
from ..sequential_pd_controller import SequentialPDController
from ...image_processing.image_annotator.soccer_annotation import SoccerAnnotation
from ..frame_data.frame_data_loader import FrameDataLoader

class AnnotationController(SequentialPDController):
    '''
    This is a SequentialPDController that deals with generating and loading sequences of SoccerAnnotation objects.
    '''

    #this is how we store the annotations in their frame directory
    ANNOTATION_BASENAME = 'annotation.npy'

    def __init__(self, num_processors, multiprocessing_mode=SequentialPDController.INTERVAL_MP):
        super(AnnotationController, self).__init__(num_processors, multiprocessing_mode=multiprocessing_mode)
        
    @classmethod
    def _get_annotation_filename(cls, frame_dir):
        '''
        frame_dir - the name of a directory containing an annotation
        
        return - the filename of the annotation
        '''
        annotation_filename = os.path.join(frame_dir, cls.ANNOTATION_BASENAME)
        return annotation_filename    

    def _save_data_in_frame_dir(self, frame_dir, annotation):
        '''
        frame_dir - a frame directory name
        annotation - a SoccerAnnotation object

        save the soccer annotation in the frame directory
        '''
        annotation_filename = self._get_annotation_filename(frame_dir)
        #print '      saving annotation to: %s' % annotation_filename
        annotation_array = annotation.get_annotation_array()
        np.save(annotation_filename, annotation_array)
        return

    @classmethod
    def load_data_from_frame_dir(cls, frame_dir):
        '''
        frame_dir - a directory name
        return - a SoccerAnnotation object
        '''
        annotation_filename = cls._get_annotation_filename(frame_dir)
        #print '   loading annotation from: %s' % annotation_filename
        annotation = np.load(annotation_filename)
        return SoccerAnnotation(annotation)
