from .frame_data import FrameData

class RawFrameData(FrameData):
    '''
    This implementation saves the data in the frame directly, as opposed to building it on the fly.
    '''

    def __init__(self, frame, data):
        '''
        frame - an integer
        data  - the data stored in this object
        '''
        super(RawFrameData, self).__init__(frame)

        self._data = data

    def build_data(self, pd_controller=None):
        #print '[RawFrameData] returning raw data from frame: %d' % self._frame
        return self._data
