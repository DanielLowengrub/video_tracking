class FrameData(object):
    '''
    This class provides access to the data at a given frame.
    In order to save memory, we do not typically store the data itself, but rather we store "lightweight" objects such as
    filenames and other parameters, and build the data upon request.

    The exact way in which the data is built depends on the specific implementation of FrameData
    '''
    def __init__(self, frame):
        '''
        frame - an integer representing the frame index
        '''
        self._frame = frame
        
    @property
    def frame(self):
        return self._frame

    def build_data(self, pd_controller=None):
        '''
        return the data contained in this frame
        '''
        raise NotImplementedError('build_data must be implemented by the subclass')
