from .frame_data import FrameData
from ..preprocessing_data_tuple import PreprocessingDataTuple

class FrameDataTuple(FrameData):
    '''
    This implementation of FrameData stores a dictionary whose keys are PreprocessingDataType attributes and whose values
    are FrameData objects. The build_data method returns a PreprocessingDataTuple that stores the data built from each of 
    the dict values.
    '''
    
    def __init__(self, frame, frame_data_dict):
        '''
        frame - a frame index
        frame_data_dict - a dictionary whose keys are PreprocessingDataType attributes and whose values are FrameData objects
        '''
        super(FrameDataTuple, self).__init__(frame)
        
        self._frame_data_dict = frame_data_dict

    def build_data(self, pd_controller=None):
        '''
        return - a PreprocessingDataTuple object.
        '''
        #print '[FrameDataTuple] building data in frame %d' % self._frame
        data_dict = dict((key,fd.build_data()) for key,fd in self._frame_data_dict.iteritems())
        return PreprocessingDataTuple(data_dict)
