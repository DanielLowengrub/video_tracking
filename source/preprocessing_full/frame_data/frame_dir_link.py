from .frame_data import FrameData

class FrameDirLink(FrameData):
    '''
    This implementation of FrameData stores only the name of a directory in which frame data is contained. 
    It does not know how to load the data itself.
    '''
    def __init__(self, frame, frame_dir):
        '''
        frame_dir - a directory name
        '''
        super(FrameDirLink, self).__init__(frame)
        self._frame_dir = frame_dir

    def build_data(self, pd_controller=None):
        '''
        return the frame directory that is stored in this FrameData object
        '''
        #print '[FrameDirLink] loading frame directory link from frame: %d' % self._frame
        return self._frame_dir

    
    
