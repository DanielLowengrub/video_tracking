from .frame_data import FrameData

class FrameDataLoader(FrameData):
    '''
    This implementation of FrameData uses a PreprocessingDataController to load data from a frame directory
    It stores a reference to a PDC class, as well as the arguments that the PDC uses to load the data.
    '''
    def __init__(self, frame, pd_controller, frame_dir):
        '''
        frame - an integer
        pd_controller - a subclass of PreprocessingDataController (the class itself, not an instance).
           The subclass must implement load_data_from_frame_dir
        frame_dir - the directory that the data is loaded from
        '''
        super(FrameDataLoader, self).__init__(frame)

        self._pd_controller = pd_controller
        self._frame_dir = frame_dir

    def build_data(self):
        data = self._pd_controller.load_data_from_frame_dir(self._frame_dir)
        return data
