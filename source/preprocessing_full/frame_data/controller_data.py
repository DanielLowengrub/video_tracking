from .frame_data import FrameData

class ControllerData(FrameData):
    '''
    This implementation of FrameData uses a PreprocessingDataController to build the frame data.
    It stores a reference to a PDC object, as well as the arguments that the PDC uses to build the data.
    '''
    def __init__(self, frame, pd_args):
        '''
        frame - an integer
        pd_controller - an instance of a subclass of PreprocessingDataController. 
           The subclass must implement the method build_data.
        pd_args - a tuple of arguments that should be passed to the PDC objects "build_data" method 
        '''
        super(ControllerData,self).__init__(frame)

        self._pd_args = pd_args

    def build_data(self, pd_controller):
        data = pd_controller.build_data(*self._pd_args)
        return data
