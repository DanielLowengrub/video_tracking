from .camera_sample_controller.players_camera_sample_controller import PlayersCameraSampleController
from .preprocessing_data_type import PreprocessingDataType
import os
import numpy as np
import cv2

#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts'
#IMAGE_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/test_data/'

#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'

#INPUT_DATA_NAME = 'images-8_15-9_31'
#INPUT_DATA_NAME = 'images-0_00-1_00'
INPUT_DATA_NAME = 'images-0_00-10_00'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_full', 'data', INPUT_DATA_NAME)

ANNOTATION_DIRECTORY          = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'annotation')
ABSOLUTE_HOMOGRAPHY_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'absolute_homography')
CAMERA_SAMPLE_DIRECTORY       = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'camera_sample')

PD_DIRECTORY_DICT = {PreprocessingDataType.ANNOTATION:          ANNOTATION_DIRECTORY,
                     PreprocessingDataType.ABSOLUTE_HOMOGRAPHY: ABSOLUTE_HOMOGRAPHY_DIRECTORY}

NUM_PROC = 8

MIN_CAMERAS = 4
MIN_ELLIPSE_AREA = 100
MIN_ELLIPSE_MATCHING_SCORE = 0.6
MAX_ALPHA_SQ_ERROR = 0.1
MAX_ALPHA_STD = 0.2

def generate_data(output_parent_directory, pd_directory_dict):
    '''
    output_parent_directory - the directory in which we will save the annotations
    pd_directory_dict - a dictionary with items (PreprocessingDataType attr, directory name
    '''
    if not os.path.isdir(output_parent_directory):
        print 'making directory: ', output_parent_directory
        os.mkdir(output_parent_directory)

    camera_sample_controller = PlayersCameraSampleController(NUM_PROC,
                                                             MIN_CAMERAS, MIN_ELLIPSE_AREA, MIN_ELLIPSE_MATCHING_SCORE,
                                                             MAX_ALPHA_SQ_ERROR, MAX_ALPHA_STD)
            
    camera_sample_controller.generate_data(output_parent_directory, pd_directory_dict)

    return

if __name__ == '__main__':
    generate_data(CAMERA_SAMPLE_DIRECTORY, PD_DIRECTORY_DICT)
