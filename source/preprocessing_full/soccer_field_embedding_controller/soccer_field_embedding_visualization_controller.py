import numpy as np
import cv2
from ..sequential_visualization_controller import SequentialVisualizationController
from ....testers.auxillary import primitive_drawing
from ...auxillary import planar_geometry
from ...auxillary.soccer_field_geometry import SoccerFieldGeometry
from ....testers.auxillary import primitive_drawing, label_shapes

class SoccerFieldEmbeddingVisualizationController(SequentialVisualizationController):
    '''
    This is a SVC that generates visualizations of highlighted shapes.
    '''

    def __init__(self, num_processors, soccer_field, shape_color, grid_color, grid_radius):
        '''
        num_processors - the number of available processors
        shape_color - the color of the shapes
        highlighted_color - the color of the highighted region of the shapes
        highlighted_alpha - the alpha of the highlighted region of the shapes
        '''
        super(SoccerFieldEmbeddingVisualizationController, self).__init__(num_processors)

        self._soccer_field = soccer_field
        self._shape_color = shape_color
        self._grid_color = grid_color
        self._grid_radius = grid_radius

    def _draw_soccer_field_embedding(self, image, highlighted_shapes, soccer_field_embedding):
        '''
        image - a numpy array with shape (n,m,3) and type np.uint8
        soccer_field_embedding - a SoccerFieldEmbeddingy object
        '''
        H = soccer_field_embedding.homography
        absolute_highlighted_shape_dict = self._soccer_field.get_highlighted_shape_dict(format_keys=True)
        absolute_shapes = [hs.get_shape() for hs in absolute_highlighted_shape_dict.itervalues()]
        absolute_grid_points = self._soccer_field.get_grid_points()

        translated_shapes = (planar_geometry.apply_homography_to_shape(H,s) for s in absolute_shapes)
        for shape in translated_shapes:
            if shape.is_ellipse() and shape.is_degenerate():
                continue

            primitive_drawing.draw_shape(image, shape, self._shape_color)

        transformed_grid_points = planar_geometry.apply_homography_to_points(H, absolute_grid_points)
        for point in transformed_grid_points:
            cv2.circle(image, tuple(np.int32(point).tolist()), self._grid_radius, self._grid_color, thickness=-1)

        labelled_shapes_with_position = dict((SoccerFieldGeometry.format_key(source_key),
                                              highlighted_shapes[target_key].get_shape_with_position())
                                         for source_key, target_key in soccer_field_embedding.embedding_dict.iteritems())
    
        label_shapes.label_shapes(image, labelled_shapes_with_position)

        return
    
    def _build_frame_data_visualization(self, frame_data):
        '''
        frame_data - a FrameDataTuple object which provides access to an image and SoccerFieldEmbedding
        
        return - an image which represents a visualization of the data
        '''
        #build a PreprocessingDataTuple object. It is assumed to contain an image and an annotation
        pd_tuple = frame_data.build_data()
        image = pd_tuple.image
        highlighted_shapes = pd_tuple.highlighted_shapes
        soccer_field_embedding = pd_tuple.soccer_field_embedding

        self._draw_soccer_field_embedding(image, highlighted_shapes, soccer_field_embedding)

        return image
        
        
        
