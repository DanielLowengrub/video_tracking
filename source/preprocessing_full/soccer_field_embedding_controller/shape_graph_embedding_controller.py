from .soccer_field_embedding_controller import SoccerFieldEmbeddingController, SoccerFieldEmbedding
from ...image_processing.homography_calculator.soccer_homography_calculator import SoccerHomographyCalculator

class ShapeGraphEmbeddingController(SoccerFieldEmbeddingController):
    '''
    In this implementation of ShapeGraphController, we use shape graphs to compute the absolute homography
    from a collection of shapes.
    '''

    def __init__(self, num_processors,
                 absolute_image, pixels_per_yard, min_lines_in_correspondence, highlighted_ellipse_threshold,
                 line_segment_threshold, ellipse_segment_threshold, parallel_lines_threshold,
                 correspondence_threshold, iwasawa_threshold):
        '''
        absolute_image - an image of the absolute soccer field
        pixels_per_yard - each yard in the soccer field corresponds to this many pixels in the absolute image
        min_lines_in_correspondence - the smallest number of lines that can be used to compute a line correspondence homography
        highlighted_ellipse_threshold - we throw out ellipses whose highlighted region has less arc length than this
        line_segment_threshold - we ignore line segments that are shorter than this number of pixels
        ellipse_segment_threshold - we ignore ellipse segements that are shorter than this angle in degrees
        parallel_lines_threshold - we consider two lines to be parallel if 1 - cos(angle between the lines) < threshold
        correspondence_threshold - the maximum error allowed when calculating homographies based on a correspondence of lines
        iwasawa_threshold - the maximum error allowed when using the iwasawa algorithm to compute a homography based on an ellipse and a center line
        '''
        super(ShapeGraphEmbeddingController, self).__init__(num_processors)
        
        self._highlighted_ellipse_threshold = highlighted_ellipse_threshold
        self._homography_calculator = SoccerHomographyCalculator(absolute_image, pixels_per_yard,
                                                                 min_lines_in_correspondence,
                                                                 line_segment_threshold, ellipse_segment_threshold,
                                                                 parallel_lines_threshold,
                                                                 correspondence_threshold, iwasawa_threshold)

    def _is_clean(self, highlighted_shape):
        '''
        highlighted_shape - a HighlightedShape object
        
        Return True if this shape should be used for shape matching.
        '''
        if highlighted_shape.is_line():
            return True
        
        elif highlighted_shape.is_ellipse():
            return highlighted_shape.highlighted_arc_length() > self._highlighted_ellipse_threshold
        
        return False
    
    def _clean_shapes(self, highlighted_shape_dict):
        '''
        highlighted_shape_dict - a dictionary whose values are HighlightedShape objects
        
        Return a new dictionary whose values are HighlightedShape objects.
        We remove the highlighted ellipses whose highlighted region has arc length less than _highlighted_ellipse_threshold
        '''
        
        return dict((k,hs) for k,hs in highlighted_shape_dict.iteritems() if self._is_clean(hs))
    
    def _build_soccer_field_embedding(self, image, annotation, highlighted_shapes):
        '''
        image - a numpy array with shape (n,m,3) and type np.uint8
        annotation - a SoccerAnnotation object
        highlighted_shapes - a list of HighlightedShape objects
        
        Return a tuple (H, embedding_dict) where H is a homgoraphy matrix from absolute coordinates to the image,
        and embedding_dict is a dictionary with items (SoccerFieldGeometry shape key, highlighted shape index).
        If we could not an embedding, return None.
        '''

        highlighted_shape_dict = dict(enumerate(highlighted_shapes))
        highlighted_shape_dict = self._clean_shapes(highlighted_shape_dict)
        
        #H is the absolute homography and
        #embedding is a dictionary with items (key of absolute shape, key of image shape)
        H, embedding = self._homography_calculator.compute_homography(image, annotation, highlighted_shape_dict)

        if H is None:
            return None

        else:
            return SoccerFieldEmbedding(H, embedding)
