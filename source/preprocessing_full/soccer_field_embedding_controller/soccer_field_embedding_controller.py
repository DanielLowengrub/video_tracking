import numpy as np
import pickle
import os
import re
import collections
from ..sequential_pd_controller import SequentialPDController

SoccerFieldEmbedding = collections.namedtuple('SoccerFieldEmbedding', ['homography', 'embedding_dict'])

class SoccerFieldEmbeddingController(SequentialPDController):
    '''
    This is a PreprocessingDataController that is in charge of matching the highlighted shapes in an image to the sidelines
    of a soccer field.
    '''

    HOMOGRAPHY_BASENAME = 'homography.npy'
    EMBEDDING_DICT_BASENAME = 'soccer_keys.pkl'
    
    def __init__(self, num_processors):
        super(SoccerFieldEmbeddingController, self).__init__(num_processors)
        
    def _save_data_in_frame_dir(self, frame_dir, soccer_field_embedding):
        '''
        frame_dir - a frame directory name
        soccer_field_embedding - a SoccerFieldEmbedding object

        save the soccer field embedding in the given directory
        '''
        homography_filename = os.path.join(frame_dir, self.HOMOGRAPHY_BASENAME)
        np.save(homography_filename, soccer_field_embedding.homography)

        embedding_dict_filename = os.path.join(frame_dir, self.EMBEDDING_DICT_BASENAME)
        with open(embedding_dict_filename, 'wb') as embedding_dict_file:
            pickle.dump(soccer_field_embedding.embedding_dict, embedding_dict_file)

        return
    
    @classmethod
    def load_data_from_frame_dir(cls, frame_dir):
        '''
        frame_dir - a directory name
        return - a SoccerFieldEmbedding object
        '''
        homography_filename = os.path.join(frame_dir, cls.HOMOGRAPHY_BASENAME)
        homography = np.load(homography_filename)

        embedding_dict_filename = os.path.join(frame_dir, cls.EMBEDDING_DICT_BASENAME)
        with open(embedding_dict_filename, 'rb') as embedding_dict_file:
            embedding_dict = pickle.load(embedding_dict_file)

        return SoccerFieldEmbedding(homography, embedding_dict)

    def _build_soccer_field_embedding(self, image, annotation, highlighted_shapes):
        '''
        annotation - a SoccerAnnotation object
        highlighted_shapes - a list of HighlightedShape objects
    
        return - a SoccerFieldEmbedding object
        '''
        raise NotImplementedError('_build_soccer_field_embedding has not been implemented')
    
    def _build_data_from_pd_tuple(self, pd_tuple):
        '''
        pd_tuple - a PreprocessingDataTuple object
        return - a SoccerFieldEmbedding object
        '''
        return self._build_soccer_field_embedding(pd_tuple.image, pd_tuple.annotation, pd_tuple.highlighted_shapes)

