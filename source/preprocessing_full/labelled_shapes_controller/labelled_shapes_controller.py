import numpy as np
import os
import re
import pickle
from ..sequential_pd_controller import SequentialPDController
from ..frame_data.frame_data_loader import FrameDataLoader
from ...preprocessing_full import filename_loader

class LabelledShapesController(SequentialPDController):
    '''
    This is a SequentialPDController that is in charge of matching highilghted shapes to absolute shapes.
    It saves this information as a dictionary whose keys are absolute shape types and whose values are highilghted shape
    indices.
    '''
    
    LABELLED_SHAPES_BASENAME = 'soccer_keys.pkl'
    
    def __init__(self, num_processors, soccer_field):
        '''
        soccer_field - a SoccerFieldGeometry object
        image_shape - a tuple (height, width)
        '''
        super(LabelledShapesController, self).__init__(num_processors)

        self._soccer_field = soccer_field
        
    def _save_data_in_frame_dir(self, frame_dir, labelled_shapes):
        '''
        frame_dir - a frame directory name
        labelled_shapes - a dictionary with items (abs shape, highlighted shape index)

        save the labelled shapes in the given directory
        '''
        filename = os.path.join(frame_dir, self.LABELLED_SHAPES_BASENAME)
        with open(filename, 'wb') as f:
            pickle.dump(labelled_shapes, f)

        return
    
    @classmethod
    def load_data_from_frame_dir(cls, frame_dir):
        '''
        frame_dir - a directory name
        return - a dictionary with items (absolute shape type, highlighted shape index)
        '''
        filename = os.path.join(frame_dir, cls.LABELLED_SHAPES_BASENAME)
        with open(filename, 'rb') as f:
            labelled_shapes = pickle.load(f)

        return labelled_shapes

    def _build_labelled_shapes(self, highlighted_shapes, absolute_homography):
        '''
        highlighted_shapes - a list of HighlightedShape objects
        absolute_homography - a homography from absolute coordinates to image coordinates
    
        return - a dictionary with items (absolute shape type, highlighted shape index)
        '''
        raise NotImplementedError('_build_soccer_field_embedding has not been implemented')
    
    def _build_data_from_pd_tuple(self, pd_tuple):
        '''
        pd_tuple - a PreprocessingDataTuple object that contains highlighted shapes and an absolute homography
        return - a dictionary with items (absolute shape type, highlighted shape index)
        '''
        return self._build_labelled_shapes(pd_tuple.highlighted_shapes, pd_tuple.absolute_homography)
