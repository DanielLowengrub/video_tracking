import numpy as np
from ..sequential_visualization_controller import SequentialVisualizationController
from ...auxillary.soccer_field_geometry import SoccerFieldGeometry
from ....testers.auxillary import primitive_drawing, label_shapes

class LabelledShapesVisualizationController(SequentialVisualizationController):
    '''
    This is a SVC that generates visualizations of labelled shapes.
    '''

    def __init__(self, num_processors, shape_color):
        '''
        num_processors - the number of available processors
        shape_color - the color of the shapes
        '''
        super(LabelledShapesVisualizationController, self).__init__(num_processors)

        self._shape_color = shape_color

    def _draw_labelled_shapes(self, image, labelled_shapes, highlighted_shapes):
        '''
        image - a numpy array with shape (n,m,3) and type np.uint8
        labelled_shapes - a dictionary with items (label, highlighted shape index)
        highlighted_shapes - a list of HighlightedShape objects
        
        draw the shapes on the image together with the labels
        '''
        #first draw the shapes on the image
        for hs in highlighted_shapes:
            primitive_drawing.draw_shape(image, hs.get_shape(), self._shape_color)

        #now draw the labels
        formatted_labels = dict()
        for label, hs_index in labelled_shapes.iteritems():
            f_label = SoccerFieldGeometry.format_key(label)
            formatted_labels[f_label] = highlighted_shapes[hs_index].get_shape_with_position()
            
        label_shapes.label_shapes(image, formatted_labels)

        return image
    
    def _build_frame_data_visualization(self, frame_data):
        '''
        frame_data - a FrameDataTuple object which provides access to an image, highlighted shapes and labelled shapes
        
        return - an image which represents a visualization of the data
        '''
        #build a PreprocessingDataTuple object. It is assumed to contain an image and an annotation
        pd_tuple = frame_data.build_data()
        image = pd_tuple.image
        highlighted_shapes = pd_tuple.highlighted_shapes
        labelled_shapes = pd_tuple.labelled_shapes
        
        self._draw_labelled_shapes(image, labelled_shapes, highlighted_shapes)

        return image
        
        
        
