import numpy as np
import collections
from ...auxillary import planar_geometry
from ...auxillary.soccer_field_geometry import SoccerFieldGeometry
from ...auxillary.contour_shape_overlap.highlighted_shape import HighlightedShape
from .labelled_shapes_controller import LabelledShapesController

class SimpleLabelledShapesController(LabelledShapesController):
    '''
    This implementation of LabelledShapesController matches absolute shapes and highlightes shapes by
    computing shape similarity.
    '''
    def __init__(self, num_processors, soccer_field, image_shape, line_threshold, ellipse_threshold):
        '''
        line_threshold - a float. If the similarity metric between two lines is greater than this the lines are not
           similar
        ellipse_threshold - a float. If the similarity metric between two ellipses is greater than this the ellipses
        are not similar
        '''
        super(SimpleLabelledShapesController, self).__init__(num_processors, soccer_field)

        self._shape_threshold_dict = {HighlightedShape.LINE: line_threshold,
                                      HighlightedShape.ELLIPSE: ellipse_threshold}

        #this is used to normalize the shapes in the image before comparing them
        self._normalization_matrix = planar_geometry.compute_homography_to_unit_square(*image_shape)

    def _build_soccer_shape_dicts(self, absolute_homography):
        '''
        absolute_homography - a homography from absolute coordinates to image coordinates
        
        return - a tuple (soccer_line_dict, soccer_ellipse_dict)
        '''
        soccer_shape_dict = self._soccer_field.get_highlighted_shape_dict().copy()

        #these shapes will never be matched to a highlighted shape
        del soccer_shape_dict[(SoccerFieldGeometry.PENALTY_CIRCLE, SoccerFieldGeometry.RIGHT)]
        del soccer_shape_dict[(SoccerFieldGeometry.PENALTY_CIRCLE, SoccerFieldGeometry.LEFT)]

        #apply the homography to all the shapes, and normalize them
        for k,hs in soccer_shape_dict.items():
            shape = hs.get_shape()
            tr_shape = planar_geometry.apply_homography_to_shape(absolute_homography,shape)
            norm_shape  = planar_geometry.apply_homography_to_shape(self._normalization_matrix, tr_shape)
            soccer_shape_dict[k] = norm_shape

        #split the dictionary according to shape type
        soccer_line_dict = dict((k,s) for k,s in soccer_shape_dict.iteritems()
                                if s.is_line())
        
        soccer_ellipse_dict = dict((k,s) for k,s in soccer_shape_dict.iteritems()
                                   if s.is_ellipse())
        
        return soccer_line_dict, soccer_ellipse_dict

    def _compute_shape_distance(self, shape_A, shape_B):
        if shape_A.is_line():
            param_A = shape_A.get_parametric_representation()
            param_B = shape_B.get_parametric_representation()
            dist_pos = np.linalg.norm(param_A - param_B)
            dist_neg = np.linalg.norm(param_A + param_B)
            return min(dist_pos, dist_neg)
        
        else:
            return shape_A.compute_shape_similarity(shape_B)
        
    def _build_shape_matching_graph(self, soccer_shape_dict, image_shape_dict, absolute_homography):
        '''
        soccer_shape_dict - a dictionary whose values are PlanarShape objects
        image_shape_dict - a dictionary whose values are PlanarShape objects
        absolute_homography - a homography from absolute coordinates to image coordinates
        
        all of the HighlightedShape should be of the same type (line or ellipse)

        return - a dictionary with items (soccer shape key, [list of matching highlighted shapes])
        '''
        shape_matching_graph = collections.defaultdict(list)

        for soccer_key, soccer_shape in soccer_shape_dict.iteritems():
            for image_key, image_shape in image_shape_dict.iteritems():
                thresh = self._shape_threshold_dict[soccer_shape.get_shape_type()]
                #similarity = soccer_shape.compute_shape_similarity(image_shape)
                shape_distance = self._compute_shape_distance(soccer_shape, image_shape)
                # print '   soccer shape %s and image shape %s have distance: %f' % (soccer_key, image_key,
                #                                                                    shape_distance)

                if shape_distance < thresh:
                    # print '   there is a match!'
                    shape_matching_graph[soccer_key].append(image_key)

        return shape_matching_graph
    
    def _build_labelled_shapes(self, highlighted_shapes, absolute_homography):
        '''
        highlighted_shapes - a list of HighlightedShape objects
        absolute_homography - a homography from absolute coordinates to image coordinates

        return - a dictionary with items (absolute shape type, highlighted shape index)
        '''
        image_shape_dict = dict()
        for k,hs in enumerate(highlighted_shapes):
            shape = hs.get_shape()
            image_shape_dict[k] = planar_geometry.apply_homography_to_shape(self._normalization_matrix, shape)

        image_line_dict = dict((k,s) for k,s in image_shape_dict.iteritems() if s.is_line())
        image_ellipse_dict = dict((k,s) for k,s in image_shape_dict.iteritems() if s.is_ellipse())

        soccer_line_dict, soccer_ellipse_dict = self._build_soccer_shape_dicts(absolute_homography)
        
        #build a graph that has an edge between each pair (soccer shape key, image shape key)
        #it is stored as a dictionary with items (soccer shape key, [list of matching highlighted shapes])
        line_matching_graph    = self._build_shape_matching_graph(soccer_line_dict, image_line_dict,
                                                               absolute_homography)
        ellipse_matching_graph = self._build_shape_matching_graph(soccer_ellipse_dict, image_ellipse_dict,
                                                                  absolute_homography)

        #merge the two graphs
        shape_matching_graph = line_matching_graph
        shape_matching_graph.update(ellipse_matching_graph)

        labelled_shapes = dict()
        for soccer_key, image_keys in shape_matching_graph.iteritems():
            if len(image_keys) == 1:
                labelled_shapes[soccer_key] = image_keys[0]

        return labelled_shapes
            
