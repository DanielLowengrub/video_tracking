import numpy as np
import cv2
import scipy.ndimage
import itertools
import functools
import time
from datetime import datetime
from multiprocessing import Pool
from ..preprocessing import file_sequence_loader
import re
from ..image_processing.image_annotator.soccer_annotation import SoccerAnnotation
from .annotation_generator import AnnotationGenerator

class NNAnnotationGenerator(AnnotationGenerator):
    '''
    This class generates SoccerFieldAnnotation objects based on the output of a NN image annotator.
    The NN annotator annotates each pixel with one of the following values:
    0: background 
    1: sideline 
    2: grass 
    3: player

    It also applies closing operations to the background and player masks.
    '''

    NN_TO_SOCCER_ANNOTATION_DICT = {0: SoccerAnnotation.irrelevant_value(),
                             1: SoccerAnnotation.sidelines_value(),
                             2: SoccerAnnotation.grass_value(),
                             3: SoccerAnnotation.players_value()}

    NN_REGEX = re.compile('image_([0-9]+)\.npy')
    
    def __init__(self, background_closure_radius, player_closure_radius):
        '''
        background_closure_radius - an integer. We apply a closure kernel with this radius to the background mask
        player_closure_radius - an integer. We apply a closure kernel with this radius to the player mask 
           (after the bg closure)
        '''
        r = background_closure_radius
        self._background_kernel = np.ones((r,r), np.bool)

        r = player_closure_radius
        self._players_kernel = np.ones((r,r), np.bool)

    def _generate_annotation(self, nn_mask, hud_mask):
        '''
        nn_mask - a numpy array with shape (n,m) and type np.int32. It represents the output of a NN annotator.
        hud_mask - a numpy array with shape (n,m) and type np.bool. It represents portions of the image that are used for
           heads up display

        return - a SoccerAnnotation object
        '''
        annotation = np.zeros(nn_mask.shape, np.int32)
        for nn_mask_value, soccer_annotation_value in self.NN_TO_SOCCER_ANNOTATION_DICT.items():
            annotation[nn_mask == nn_mask_value] = soccer_annotation_value

        #add the hud mask to the background
        cv2.imshow('hud mask', np.float32(hud_mask))
        cv2.waitKey(0)
        cv2.destroyAllWindows()

        print 'putting hud in background...'
        annotation[hud_mask] = SoccerAnnotation.irrelevant_value()
        
        cv2.imshow('bg', np.float32(annotation == SoccerAnnotation.irrelevant_value()))
        cv2.waitKey(0)
        cv2.destroyAllWindows()

        #close the background
        background_mask = (annotation == SoccerAnnotation.irrelevant_value())
        background_mask = scipy.ndimage.morphology.binary_closing(background_mask, structure=self._background_kernel)
        annotation[background_mask] = SoccerAnnotation.irrelevant_value()

        cv2.imshow('bg after closing background', np.float32(annotation == SoccerAnnotation.irrelevant_value()))
        cv2.waitKey(0)
        cv2.destroyAllWindows()

        #close the players
        players_mask = (annotation == SoccerAnnotation.players_value())
        players_mask = scipy.ndimage.morphology.binary_closing(players_mask, structure=self._players_kernel)
        annotation[players_mask] = SoccerAnnotation.players_value()

        cv2.imshow('bg after closing players', np.float32(annotation == SoccerAnnotation.irrelevant_value()))
        cv2.waitKey(0)
        cv2.destroyAllWindows()

        annotation = SoccerAnnotation(annotation)

        return annotation

    def _save_frame_data_in_file(self, filename, annotation):
        '''
        filename - the file to save the annotation in
        annotation - a SoccerAnnotation object
        '''
        annotation_array = annotation.get_annotation_array()
        np.save(filename, annotation_array)
        return
    
    def _generate_and_save_annotation(self, temp_dir, frame_index, nn_mask, hud_mask):
        '''
        temp_dir - a directory name
        frame_index - an integer
        nn_mask - a numpy array with shape (n,m) and type np.int32. It represents the output of a NN annotator.
        hud_mask - a numpy array with shape (n,m) and type np.bool. It represents portions of the image that are used for
           heads up display


        Create a SoccerAnnotation object and save it in the given directory.
        '''
        annotation = self._generate_annotation(nn_mask, hud_mask)
        self._save_frame_data_in_interval_dir(temp_dir, frame_index, annotation)
        return

    def _load_nn_masks(self, nn_directory):
        '''
        nn_directory - a directory containing files image_0.npy,...,image_N.npy. 
           Each numpy array has shape (n,m) and type int32.
           The values at each pixel represent one of: grass, player, sideline, background

        return an iterator of np arrays: image_0, image_1,...
        '''
        nn_mask_filenames = file_sequence_loader.get_file_sequence(nn_directory, self.NN_REGEX)
        nn_masks = itertools.imap(np.load, nn_mask_filenames)

        return nn_masks
    
    def generate_annotations(self, nn_directory, hud_mask, annotation_directory):
        '''
        nn_directory - a directory containing files image_0.npy,...,image_N.npy. 
           Each numpy array has shape (n,m) and type int32.
           The values at each pixel represent one of: grass, player, sideline, background

        hud_mask - a numpy array with shape (n,m) and type np.bool. It represents portions of the image that are used for
           heads up display
        annotation_directory - a directory name

        For each i, save a SoccerAnnotation object in annotation_directory which is an annotation represented by image_i.npy
        '''

        print 'generating annotations from the nn masks in:'
        print nn_directory
                
        nn_masks = self._load_nn_masks(nn_directory)
        enumerated_nn_masks = enumerate(nn_masks)
        
        #create a worker pool and use it to build the annotations
        #we put the annotations in a temporary directory before we know how many of them there are
        temp_dir = self._create_temp_dir(annotation_directory)
        num_annotations = 0
        
        p = Pool(None)
        generate_and_save = functools.partial(worker_pool_generate_and_save, self, temp_dir, hud_masl)
        for retval in p.imap(generate_and_save, enumerated_nn_masks):
            num_annotations += 1

        interval = (0, num_annotations - 1)
        self._move_temp_dir_to_interval_dir(annotation_directory, interval)

        return
    
def worker_pool_generate_and_save(annotation_generator, temp_dir, hud_mask, enumerated_nn_mask):
    '''
    annotation_generator - an NNGenerator object
    hud_mask - a numpy array with shape (n,m) and type np.bool
    temp_dir - a directory name. This is a temporary directory that we store the annotations in before we know how many there
       are
    enumerated_nn_mask - a tuple (integer, nn_mask)
    
    create a SoccerAnnotation from the nn mask and save it in the given directory.

    This function is called by the workers in a Pool.
    '''
    frame_index, nn_mask = enumerated_nn_mask

    time_str = datetime.now().isoformat()
    print '[%s] generating annotation for frame %d ...' % (time_str, frame_index)
    annotation_generator._generate_and_save_annotation(temp_dir, frame_index, nn_mask, hud_mask)

    return

        
