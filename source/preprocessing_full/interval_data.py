import collections

class IntervalData(collections.Iterator):
    '''
    This is an Iterator that stores data associated to a frame interval (i,j). Note that this includes frame j.
    It stores an interval, together with a list of FrameData objects.
    Note that typically the FrameData objects do not store the data itself, but rather a bunch of lightweight variables such
    as frame directory names which it then uses to construct the data if necessary.
    '''

    def __init__(self, interval, frame_data_list):
        '''
        interval - a tuple of integers (i,j)
        frame_data_list - a list of FrameData objects
        '''
        self._interval = interval
        self._frame_data_list = frame_data_list
        self._current_frame_data_index = 0

    @property
    def interval(self):
        return self._interval

    def next(self):
        if self._current_frame_data_index >= len(self._frame_data_list):
            raise StopIteration
        
        self._current_frame_data_index += 1
        return self._frame_data_list[self._current_frame_data_index - 1]

    def length(self):
        '''
        return - the number of frames in the interval. 
        WARNING: Note that there may be less FrameData objects than this, since not all frames necessarily have a FD object.
        '''
        return self._interval[1] - self._interval[0] + 1

    def number_of_frame_datas(self):
        return len(self._frame_data_list)
    
    def get_frames(self):
        '''
        return - a list of integers corresponding to the frames which have an associated FrameData object
        '''
        return [fd.frame for fd in self._frame_data_list]

    def build_sub_interval_data(self, sub_frames):
        '''
        sub_frames - a list of frames which is a subset of the frames for which this object has an IntervalData object
        return - an IntervalData object which only has FrameData objects at the frames in "sub_frames"
        '''
        sub_frame_set = set(sub_frames)
        sub_frame_data_list = [fd for fd in self._frame_data_list if fd.frame in sub_frame_set]
        return IntervalData(self._interval, sub_frame_data_list)

    def chunks(self, num_chunks):
        '''
        num_chunks - an integer
        return - a list of num_chunks IntervalData objects which all have the same interval as this IntervalData.

        The frames in this IntervalData are distributed evenly among the output IntervalDatas. In particular, for each
        output IntervalData id_k, we have: id_k.number_of_frames() = self.number_of_frames()/num_chunks.
        '''

        #we want the chunk size to be at least one, even if the number of frames is less than num_chunks
        num_frame_datas = self.number_of_frame_datas()
        chunksize = max(1, num_frame_datas / num_chunks)

        return [IntervalData(self._interval, self._frame_data_list[i:i+chunksize])
                for i in range(0, num_frame_datas, chunksize)]
