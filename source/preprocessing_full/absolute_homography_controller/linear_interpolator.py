import numpy as np
import itertools
from .absolute_homography_controller import AbsoluteHomographyController

class LinearInterpolatorController(AbsoluteHomographyController):
    '''
    This implementation of an absolute homography generator uses a simple linear interpolation to combine
    an incomplete list of absolute homographies.

    For example, suppose we have a sequence of relative homographies R1,R2 and and absolute homographies H1 and H3.
    Then we interpolate to: 
    H2 = (H0*(R1^-1) + H3*R2*R1) / 2
    '''

    def __init__(self, min_frame_distance, max_interpolation_distance):
        super(LinearInterpolator, self).__init__(min_frame_distance, max_interpolation_distance)


    @classmethod
    def _interpolate_forwards(cls, H_start, relative_homographies):
        '''
        H_start - a numpy array with shape (3,3) and type np.float32
        relative_homographies - a list of numpy arrays with shape (3,3) and type np.float32

        Suppose the relative homographies are R_0,...,R_{N-1}. We return a list of homography matrices
        H_0,...,H_N where 
        * H0      = H_start
        * for 0 < i <= N, H_i = R_{i-1}*...R_0*H_start
        '''

        if DEBUG:
            print 'interpolating forwards with %d relative homographies...' % len(relative_homographies)

        #H_0 should be equal to H_start
        interpolated_homographies = [H_start]

        for R in relative_homographies:
            #to get the homography from absolute coordinates to the current frame,
            #first apply the homography to the previous frame and then apply the homography from the
            #previous frame to the current frame.
            H = np.dot(R, interpolated_homographies[-1])
            interpolated_homographies.append(H)

        return interpolated_homographies
            
    @classmethod
    def _interpolate_backwards(cls, H_end, relative_homographies):
        '''
        H_end - a numpy array with shape (3,3) and type np.float32
        relative_homographies - a list of numpy arrays with shape (3,3) and type np.float32

        Suppose the relative homographies are R_0,...,R_{N-1}. We return a list of homography matrices
        H_0,...,H_N where 
        * HN = H_end
        * H_i =  (R_i * ... R_{N-1})^{-1} * H_end
        '''
        
        #this list records the interpolated homographies in reverse order: [H_N,...,H_0]
        #we initialize it with H_N = H_end
        interpolated_homographies = [H_end]

        #iterate over the relative homographies in reverse order: R_{N-1},...,R_1,R_0
        for R in reversed(relative_homographies):
            #to get the homography from absolute coordinates to the current frame,
            #first apply the homography to the next frame and then apply the homography from the
            #next frame to the current frame.
            H = np.dot(np.linalg.inv(R), interpolated_homographies[-1])
            interpolated_homographies.append(H)

        #since interpolated homographies stores H_N,...,H_0, reverse it before returning it
        interpolated_homographies.reverse()
        
        return interpolated_homographies

    
    def _interpolate_homographies(self, relative_homographies, indexed_absolute_homographies):
        '''
        relative_homographies - a list of homography matrices R_0,...,R_{N-1}. R_i corresponds to a homography matrix from
                                image i to image i+1
        indexed_absolute_homographies - a list of tuples (i, H) such that
                                  i is an integer satisfying 0 <= i <= N and H is (3,3) numpy array with type np.float32
                                  H represents a homography FROM absolute coordinated TO the i-th image
                                  The list is sorted by the homography index i.

        Return a list of absolute homography matrices H_0,H_1,...,H_N interpolating the given ones.
        H_k is the homography FROM absolute coordinates TO the k-th image.
        '''

        if DEBUG:
            print 'interpolating homographies with:'
            print '%d relative homographies.' % len(relative_homographies)
            print 'indices of absolute homographies:'
            print [h[0] for h in indexed_absolute_homographies]
        
        interpolated_homographies = []
        
        #build a list of tuples ((ik,Hk),(i{k+1},H{k+1}))
        #if i0>0 add a tuple ((0,None),(i0,H0)) to the beginning.
        #if iN<max_frame, add a tuple ((iN,HN),(max_frame,None)) to the end
        max_frame = len(relative_homographies)
        absolute_H_intervals = self._build_intervals(max_frame, indexed_absolute_homographies)

        #interpolate between the endpoints of each of the intervals
        for (start,H_start),(end,H_end) in absolute_H_intervals:
            if DEBUG:
                print 'interpolating homographies in interval: ', (start, end)
            
            relative_Hs_in_interval = relative_homographies[start:end]

            if DEBUG:
                print 'there are %d relative homographies' % len(relative_Hs_in_interval)
            
            interpolated_forwards = None
            interpolated_backwards = None

            if H_start is not None:
                interpolated_forwards = self._interpolate_forwards(H_start, relative_Hs_in_interval)

            if H_end is not None:
                interpolated_backwards = self._interpolate_backwards(H_end, relative_Hs_in_interval)

            #this should only happen if the first interval is ((0,None),(i0,H0))
            #so we can not interpolate forwards
            if interpolated_forwards is None:
                if DEBUG:
                    print 'can not interpolate forwards. adding %d homographies' % len(interpolated_backwards)
                interpolated_Hs_in_interval = interpolated_backwards

            #this should only happen if the last interval is ((iN,HN),(max_frame,None))
            #so we can not interpolate backwards
            elif interpolated_backwards is None:
                if DEBUG:
                    print 'can not interpolate backwards. adding %d homographies' % len(interpolated_forwards)
                interpolated_Hs_in_interval = interpolated_forwards

            else:
                #if both interpolations exist, take weighted averages at each frame
                #let wf be the forward weight and wb be the backwards weight. then:
                #wf_0 = 1.0, wf_1 = 1.0-1/N, ..., wf_{N-1} = 1.0-(N-1)/N, wf_N = 0.0
                #wb_0 = 0.0, wb_1 = 1/N    , ..., wb_{N-1} = (N-1)/N    , wb_N = 1.0
                N = end-start
                wf = 1 - np.arange(N+1).astype(np.float64)/N #list(1.0-float(i)/N for i in xrange(N+1))
                wb = np.arange(N+1).astype(np.float64)/N     #list(float(i)/N for i in xrange(N+1))

                # print 'wf + wb: '
                # print wf + wb
                average_interpolation = [(a*H1 + b*H2) for a,H1,b,H2 in
                                         itertools.izip(wf, interpolated_forwards, wb, interpolated_backwards)]

                if DEBUG:
                    print 'averaging over forward and backwards interpolation.'
                    print 'adding %d homographies' % len(average_interpolation)
                interpolated_Hs_in_interval = average_interpolation

            #do not add the last interpolated element since this will be equal to the first interpolated element of the
            #next interval
            interpolated_homographies += interpolated_Hs_in_interval[:-1]
            
        #we have not yet added the last interpolated homography from the last interval
        interpolated_homographies.append(interpolated_Hs_in_interval[-1])

        #normalize the homographies
        interpolated_homographies = [H/H[2,2] for H in interpolated_homographies]
        
        return interpolated_homographies
        
