import os
import itertools
import numpy as np
from bintrees import FastAVLTree
from ..preprocessing_data_controller import PreprocessingDataController
from ..interval_data import IntervalData
from ...auxillary.iterator_len import IteratorLen
from ...auxillary import iterator_operations
from ..frame_data.raw_frame_data import RawFrameData
from ..soccer_field_embedding_controller.soccer_field_embedding_controller import SoccerFieldEmbedding

DEBUG = False

class AbsoluteHomographyController(PreprocessingDataController):
    '''
    This PDC is in charge of generating absolute homographies. 
    It interpolates between the current abs homographies using relative "frame to frame" homographies.
    '''

    #this is how we store the homographies in their frame directory
    HOMOGRAPHY_BASENAME = 'homography.npy'

    def __init__(self, num_processors, min_frame_distance, max_interpolation_distance):
        '''
        num_shapes_bandwidth - this is the window size we use to see if the number of shapes in an embedding is a local
        maximum.
        min_frame_distance - we do not inerpolate between absolute homographies H_i and H_j if 
        (j-i) < min_interval_length
        '''
        super(AbsoluteHomographyController, self).__init__(num_processors)
        
        self._min_frame_distance = min_frame_distance
        self._max_interpolation_distance = max_interpolation_distance
        
    @classmethod
    def _get_homography_filename(cls, frame_dir):
        '''
        frame_dir - the name of a directory containing a optimized homography
        
        return - the filename of the homography
        '''
        homography_filename = os.path.join(frame_dir, cls.HOMOGRAPHY_BASENAME)
        return homography_filename
    
    def _save_data_in_frame_dir(self, frame_dir, homography):
        '''
        frame_dir - a frame directory name
        homography - a numpy array with shape (3,3) and type np.float64

        save the homography in the frame directory
        '''
        homography_filename = self._get_homography_filename(frame_dir)
        #print '      saving homography to: %s' % homography_filename
        np.save(homography_filename, homography)
        return

    @classmethod
    def load_data_from_frame_dir(cls, frame_dir):
        '''
        frame_dir - a directory name
        return - a numpy array with shape (3,3) and type np.float64 representing a homography matrix
        '''
        homography_filename = cls._get_homography_filename(frame_dir)
        #print '   loading homography from: %s' % homography_filename
        homography = np.load(homography_filename)
        return homography

    def _interpolate_homographies(self, relative_homographies, enumerated_absolute_homographies):
        '''
        relative_homographies - a list of homography matrices R_0,...,R_{N-1}. R_i corresponds to a homography matrix from
                                image i to image i+1
        enumerated_absolute_homographies - a list of tuples (i, H) such that
                                  i is an integer satisfying 0 <= i <= N and H is (3,3) numpy array with type np.float32
                                  The list is sorted by the homography index i.

        Return a list of absolute homography matrices H_0,H_1,...,H_N interpolating the given ones.
        '''
            
        raise NotImplementedError('_interpolate_homographies has not been implemented')

    def _homography_filter(self, i, H, sampled_homographies):
        '''
        i - an integer
        H - a numpy array with shape (3,3) and type np.float32
        sampled_homographies - a FastAVLTree of integers.

        Return False iff H is None, or if there exists an item (j,G) in sampled_homographies such that
        |j - i| < min_frame_distance and
        '''
        #we never allow pairs (i,H) where H is None
        if H is None:
            return False
        
        if sampled_homographies.is_empty():
            return True

        #if i is smaller than all of the indices in sampled_homographies, then we just have to check how far it is from the
        #smallest sampled frame
        min_sampled_frame = sampled_homographies.min_key()
        if i < min_sampled_frame:
            return (min_sampled_frame - i) >= self._min_frame_distance

        #similarly if frame is larger than all sampled frames
        max_sampled_frame = sampled_homographies.max_key()
        if i > max_sampled_frame:
            return (i - max_sampled_frame) >= self._min_frame_distance

        #set floor_frame to the largest sampled frame that is smaller than frame        
        floor_frame = sampled_homographies.floor_key(i)
        #set ceiling_frame to the smallest sampled frame that is larger than frame        
        ceiling_frame = sampled_homographies.ceiling_key(i)

        #check if it is far enough from the floor and ceiling
        return min(i - floor_frame, ceiling_frame - i) >= self._min_frame_distance

    def _build_intervals(self, max_frame, enumerated_homographies):
        '''
        max_frame - a positive integer
        homographies - a list of tuples (ik,Hk) where the ik are integers 0 <= i0 < i1 < ... < iN <= max_frame
                       and the Hk are numpy arrays with shape (3,3) and type np.float32

        return a list of tuples ((ik,Hk),(i{k+1},H{k+1})). If 0<i0 then we add a tuple ((0,None),(i0,H0)) to
        the beginning. Similarly, if iN<max_frame then we add a tuple ((iN,HN),(max_frame,None)) to the end.
        '''
        intervals = itertools.izip(enumerated_homographies[:-1], enumerated_homographies[1:])

        if enumerated_homographies[0][0] > 0:
            first_interval = ((0,None),enumerated_homographies[0])
            intervals = itertools.chain([first_interval], intervals)

        if enumerated_homographies[-1][0] < max_frame:
            last_interval = (enumerated_homographies[-1], (max_frame,None))
            intervals = itertools.chain(intervals, [last_interval])

        return list(intervals)

    def _sample_homographies(self, max_frame, enumerated_homographies):
        '''
        max_frame - a positive integer
        enumerated_abs_homographies - a list of tuples (ik,Hk) where the ik are integers 
        0 <= i0 < i1 < ... < iN <= max_frame
        and the Hk are numpy arrays with shape (3,3) and type np.float32

        Return a subset of tuples (jk,Hk), j0 < ... < jM such that for all k, |j{k+1} - jk| >= _min_frame_distance
        We try to minimize the interval lengths |j{k+1} - jk|, |j0 - 0| and |max_frame - jM|
        '''
        #store the sparse frames in an AVL tree since we want fast insertion, and to be able to quickly find
        #the smallest value larger than n, and the biggest value smaller than n for a given integer n.
        #the items in this tree are (ik,Hk)
        sampled_homographies = FastAVLTree()
        
        #get a list of intervals of the form ((ik,Hk), (i{k+1},H{k+1}))
        #if i0>0 we also include the intervals ((0, None),(i0,H0))
        #if iN<max_frame we also include ((iN,HN),(max_frame,None))
        intervals = self._build_intervals(max_frame, enumerated_homographies)
        
        #sort the intervals by length from biggest to smallest
        INDEX=0
        intervals.sort(key=lambda x: x[1][INDEX]-x[0][INDEX], reverse=True)

        #print 'sorted intervals:'
        #print [(i[0][0],i[1][0]) for i in intervals]
        
        #try to add the endpoints of the intervals to the sampled positions, starting from the largest interval
        for (j1,H1),(j2,H2) in intervals:
            #if j1 is far enough from the current sampled frames, add it to the tree
            if self._homography_filter(j1,H1,sampled_homographies):
                sampled_homographies.set_default(j1,H1)

            #if j2 is far enough from the current sampled frames, add it to the tree
            if self._homography_filter(j2,H2,sampled_homographies):
                sampled_homographies.set_default(j2,H2)

        #return an ordered list of the sampled positions
        return list(sampled_homographies.items())

    def _find_interpolation_intervals(self, max_frame, enumerated_abs_homographies):
        '''
        enumerated_abs_homographies - a list of tuples: (i0,H0), ..., (iN,HN), i0 < i1 < ... < iN
        max_frame - an integer
        return - a list of tuples. Each element in the list has the form: ((i,j), [(l0,H0),...,(lM,HM)])

        This method divides the enumerated homographies into intervals (i,j) such that in any given interval,
        each frame is no more than _max_interpolation_distance from an abs homography
        '''
        indexed_Hs = []
        
        #start the first interval
        i,H = enumerated_abs_homographies[0]
        interval_start = max(0, i - self._max_interpolation_distance)
        interval_end = i
        Hs_in_interval = [(i-interval_start, H)]

        for i,H in enumerated_abs_homographies[1:]:
            #check if this homography can be added to the current interval
            if i - interval_end <= 2*self._max_interpolation_distance + 1:
                interval_end = i
                Hs_in_interval.append((i - interval_start, H))

            #if not, finish up with the current interval and start a new one
            else:
                interval_end = min(max_frame, interval_end + self._max_interpolation_distance)
                interval = (interval_start, interval_end)
                indexed_Hs.append((interval, Hs_in_interval))

                interval_start = i - self._max_interpolation_distance
                interval_end = i
                Hs_in_interval = [(i-interval_start, H)]

        #finish up with the last interval
        #print 'finishing with last interval...'
        #print 'max_frame: %d, interval_start: %d, interval_end: %d' % (max_frame, interval_start, interval_end)
        interval_end = min(max_frame, interval_end + self._max_interpolation_distance)
        #print '   new interval end: %d' % (interval_end,)
        interval = (interval_start, interval_end)
        indexed_Hs.append((interval, Hs_in_interval))

        return indexed_Hs

    def _build_interval_datas(self, rel_homography_interval_data, embedding_interval_data):
        '''
        rel_homography_interval_data - an IntervalData object
        embedding_interval_data - an IntervalData object
        return - an list of IntervalData objects
        '''
        enumerated_abs_homographies = [(fd.frame, fd.build_data().soccer_field_embedding.homography)
                                       for fd in embedding_interval_data]

        relative_homographies = [fd.build_data().relative_homography for fd in rel_homography_interval_data]

        if DEBUG:
            print 'building interval datas with abs homography frames:'
            print [i for i,H in enumerated_abs_homographies]
        
        if len(enumerated_abs_homographies) == 0:
            return IntervalData(rel_homography_interval_data.interval, [])
        
        #find a subset of the absolute homographies that will be used for interpolation
        max_frame = embedding_interval_data.length()-1
        enumerated_abs_homographies = self._sample_homographies(max_frame, enumerated_abs_homographies)

        if DEBUG:
            print 'after sampling (min_frame_distance=%d):' % self._min_frame_distance
            print [i for i,H in enumerated_abs_homographies]

        #divide the absolute homographies into intervals that can be interpolated
        indexed_abs_homographies = self._find_interpolation_intervals(max_frame, enumerated_abs_homographies)

        if DEBUG:
            print 'interpolation intervals (max_interpolation_distance=%d):' % self._max_interpolation_distance
            for interval, enumerated_Hs in indexed_abs_homographies:
                print 'interval: %s, frames: %s' % (interval, [i for i,H in enumerated_Hs])
            
        interval_datas = []
        start_frame = rel_homography_interval_data.interval[0]
        for sub_interval, enumerated_abs_Hs in indexed_abs_homographies:
            #build an IntervalData object corresponding to this sub interval
            #this is the interval in global coordinates
            interval = (start_frame + sub_interval[0], start_frame + sub_interval[1])
            rel_Hs = relative_homographies[sub_interval[0] : sub_interval[1]]
            interpolated_homographies = self._interpolate_homographies(rel_Hs, enumerated_abs_Hs)
            interpolated_frame_datas = [RawFrameData(frame, H) for frame,H in enumerate(interpolated_homographies)]
            interval_datas.append(IntervalData(interval, interpolated_frame_datas))
            
        return interval_datas
    
    def _build_interval_data_iter(self, rel_homography_interval_data_iter, embedding_interval_data_iter):
        '''
        rel_homography_interval_data_iter - an IteratorLen of IntervalData objects
        embedding_interval_data_iter - an IteratorLen of IntervalData objects

        return - an IteratorLen of IntervalData objects
        '''
        num_intervals = len(rel_homography_interval_data_iter)
        rel_embedding_interval_data_iter = itertools.izip(rel_homography_interval_data_iter,
                                                          embedding_interval_data_iter)

        #use each IntervalData object to build a list of IntervalData objects
        interval_data_lists = (self._build_interval_datas(rel_interval_data, embedding_interval_data)
                               for rel_interval_data, embedding_interval_data in rel_embedding_interval_data_iter)

        #chain all the lists together
        interval_datas = list(itertools.chain.from_iterable(interval_data_lists))
        return IteratorLen.from_list(interval_datas)
    
    def generate_data(self, output_parent_dir, relative_homography_directory, soccer_field_embedding_directory):
        '''
        output_parent_dir - a directory name
        relative_homography_directory - a directory storing relative homographies between all frames in each interval
        optimized_homography_directory - a directory storing SoccerFieldEmbedding objects in some of the frames of each 
           interval

        generate absolute homographies for all frames
        '''
        from ..preprocessing_data_loader import PreprocessingDataLoader
        from ..preprocessing_data_type import PreprocessingDataType

        rel_homography_dict = {PreprocessingDataType.RELATIVE_HOMOGRAPHY:  relative_homography_directory}
        embedding_dict      = {PreprocessingDataType.SOCCER_FIELD_EMBEDDING: soccer_field_embedding_directory}
        
        #load an IteratorData of IntervalData objects. We do not load them at once since we want rel homographies
        #in all frames, but there are only embeddings in some of the frames
        rel_homography_interval_data_iter = PreprocessingDataLoader.load_data(rel_homography_dict)
        embedding_interval_data_iter = PreprocessingDataLoader.load_data(embedding_dict)

        #Debugging
        # rel_homography_interval_data_iter = (interval_data for interval_data in rel_homography_interval_data_iter
        #                                      if interval_data.interval[0] == 12356)
        # rel_homography_interval_data_iter = IteratorLen(1, rel_homography_interval_data_iter)

        # embedding_interval_data_iter = (interval_data for interval_data in embedding_interval_data_iter
        #                                 if interval_data.interval[0] == 12356)
        # embedding_interval_data_iter = IteratorLen(1, embedding_interval_data_iter)

        # iid = next(rel_homography_interval_data_iter)
        # iid = next(embedding_interval_data_iter)
        # iid = next(rel_homography_interval_data_iter)
        # iid = next(embedding_interval_data_iter)
        # iid = next(rel_homography_interval_data_iter)
        # iid = next(embedding_interval_data_iter)
        ###############
        
        #build absolute homographies in each interval
        output_interval_data_iter = self._build_interval_data_iter(rel_homography_interval_data_iter,
                                                                   embedding_interval_data_iter)

        #save the output data in the output directory
        self._save_interval_data_iter(output_parent_dir, output_interval_data_iter)

        return

