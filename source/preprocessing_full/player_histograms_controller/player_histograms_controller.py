import numpy as np
import os
from ..sequential_pd_controller import SequentialPDController
from ...auxillary import iterator_operations, world_to_image_projections, histogram_operations

class PlayerHistogramsController(SequentialPDController):
    '''
    This is a SequentialPDController that is in charge of finding players histograms
    '''

    HISTOGRAMS_BASENAME = 'histgorams.npy'
    
    def __init__(self, num_processors, nbins=(10,10)):
        '''
        n_bins - a tuple (number of hue bins, number of saturation bins)
        '''
        super(PlayerHistogramsController, self).__init__(num_processors)
        self._nbins = nbins
        
    def _save_data_in_frame_dir(self, frame_dir, histograms):
        '''
        frame_dir - a frame directory name
        histograms - a numpy array with shape (num players, num hue bins * num sat bins)

        save the histgorams in the frame directory
        '''            
        histograms_filename = os.path.join(frame_dir, self.HISTOGRAMS_BASENAME)
        np.save(histograms_filename, histograms)

        return
    
    @classmethod
    def load_data_from_frame_dir(cls, frame_dir):
        '''
        frame_dir - a directory name
        return - a numpy array with shape (num players, num hue bins * num sat bins)
        '''
        histograms_filename = os.path.join(frame_dir, cls.HISTOGRAMS_BASENAME)
        return np.load(histograms_filename)

    
    def _ravelled_histogram_length(self):
        '''
        return - an integer. This is the length of the vector obtained by ravelling a histogram.
        '''
        return self._nbins[0] * self._nbins[1]
        
    def _build_player_histograms(self, image, annotation, camera, players):
        '''
        image - a numpy array with shape (n,m,3) and type np.uint8
        annotation - a SoccerAnnotation object
        camera - a CameraMatrix object
        players - a list of PlayerData objects

        return a np array with shape (num players in frame, num hue bins * num sat bins).
        It represents the histograms of the players in the frame
        '''
        foreground_mask = annotation.get_players_mask()
        
        #player_rectangle is the bounding rectangle of the ellipse surrounding the player.
        #player_mask is a mask with the same shape as the rectangle, and the points in the ellipse are True
        if len(players) == 0:
            return np.empty((0,self._ravelled_histogram_length()), np.int32)

        player_positions = np.vstack([player.position for player in players])
        player_rectangles, player_masks = world_to_image_projections.build_player_masks(camera, player_positions)

        player_histograms = []
        for player_rectangle, player_mask in zip(player_rectangles, player_masks):
            #remove the part of the player mask that is not in the foreground
            player_foreground_mask = player_rectangle.get_slice(foreground_mask)
            player_mask = player_mask.astype(np.float32) * player_foreground_mask

            #get the portion of the image that is contained in the player rectangle
            player_image = player_rectangle.get_slice(image)
            player_histogram = histogram_operations.get_normalized_hs_histogram(player_image, player_mask, self._nbins)

            #ravel the histogram so it is a vector
            player_histograms.append(player_histogram.ravel())
            
        return np.vstack(player_histograms)

    def _build_data_from_pd_tuple(self, pd_tuple):
        '''
        pd_tuple - a PreprocessingDataTuple object
        return - a numpy array with shape (num histgorams, num hue bins * num saturation bins)
        '''
        return self._build_player_histograms(pd_tuple.image, pd_tuple.annotation, pd_tuple.camera, pd_tuple.players)

