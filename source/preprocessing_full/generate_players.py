from players_controller.ellipse_matching_players_controller import EllipseMatchingPlayersController
from .preprocessing_data_type import PreprocessingDataType
import os
import numpy as np
import cv2

#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts'
#IMAGE_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/test_data/'

#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'

#INPUT_DATA_NAME = 'images-8_15-9_31'
#INPUT_DATA_NAME = 'images-0_00-1_00'
INPUT_DATA_NAME = 'images-0_00-10_00'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_full', 'data', INPUT_DATA_NAME)

ANNOTATION_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'annotation')
CAMERA_DIRECTORY     = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'camera')
PLAYERS_DIRECTORY    = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'players')

PD_DIRECTORY_DICT = {PreprocessingDataType.ANNOTATION: ANNOTATION_DIRECTORY,
                     PreprocessingDataType.CAMERA:     CAMERA_DIRECTORY}

NUM_PROC = 8

MIN_ELLIPSE_AREA = 100
MIN_ELLIPSE_MATCHING_SCORE = 0.6

def generate_data(output_parent_directory, pd_directory_dict):
    '''
    output_parent_directory - the directory in which we will save the annotations
    pd_directory_dict - a dictionary with items (PreprocessingDataType attr, directory name
    '''
    if not os.path.isdir(output_parent_directory):
        print 'making directory: ', output_parent_directory
        os.mkdir(output_parent_directory)

    players_controller = EllipseMatchingPlayersController(NUM_PROC, MIN_ELLIPSE_AREA, MIN_ELLIPSE_MATCHING_SCORE)
    players_controller.generate_data(output_parent_directory, pd_directory_dict)

    return

if __name__ == '__main__':
    generate_data(PLAYERS_DIRECTORY, PD_DIRECTORY_DICT)
