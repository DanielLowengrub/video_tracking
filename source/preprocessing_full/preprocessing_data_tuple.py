from .preprocessing_data_type import PreprocessingDataType

class PreprocessingDataTuple(object):
    '''
    This stores the data pertaining to a single frame.
    It is similar to a namedtuple object, except it is initialized with a dictionary with items: 
    (preprocessing data type, object with that type)

    The reason that we use this, as opposed to a namedtuple with attributes ['image', 'annotation',...] is that
    1) we want to be able to easily add and remove data types
    2) we will typically generate a PFT from a dictionary with the same keys, but whose arguments are directories. This set up
       makes it easy to associate the directory to the corresponding data type
    '''

    def __init__(self, preprocessing_data_dict):
        '''
        preprocessing_data_dict - a dictionary with items (data_type, object).
           data_type is a property of PreprocessingDataTypes
           object is an object whose type depends on data_type
        '''
        self._data_dict = preprocessing_data_dict
    
    @property
    def image(self):
        return self._data_dict[PreprocessingDataType.IMAGE]

    @property
    def annotation(self):
        return self._data_dict[PreprocessingDataType.ANNOTATION]
    
    @property
    def highlighted_shapes(self):
        return self._data_dict[PreprocessingDataType.HIGHLIGHTED_SHAPES]
    
    @property
    def relative_homography(self):
        return self._data_dict[PreprocessingDataType.RELATIVE_HOMOGRAPHY]
    
    @property
    def absolute_homography(self):
        return self._data_dict[PreprocessingDataType.ABSOLUTE_HOMOGRAPHY]
    
    @property
    def camera(self):
        return self._data_dict[PreprocessingDataType.CAMERA]
    
    @property
    def players(self):
        return self._data_dict[PreprocessingDataType.PLAYERS]
    
    @property
    def player_histograms(self):
        return self._data_dict[PreprocessingDataType.PLAYER_HISTOGRAMS]
    
    @property
    def team_labels(self):
        return self._data_dict[PreprocessingDataType.TEAM_LABELS]

    @property
    def ball_candidates(self):
        return self._data_dict[PreprocessingDataType.BALLS]
    
    @property
    def ball_trajectory_candidates(self):
        return self._data_dict[PreprocessingDataType.BALL_TRAJECTORIES]

    @property
    def soccer_field_embedding(self):
        return self._data_dict[PreprocessingDataType.SOCCER_FIELD_EMBEDDING]

    @property
    def optimized_homography(self):
        return self._data_dict[PreprocessingDataType.OPTIMIZED_HOMOGRAPHY]

    @property
    def camera_sample(self):
        return self._data_dict[PreprocessingDataType.CAMERA_SAMPLE]

    @property
    def labelled_shapes(self):
        return self._data_dict[PreprocessingDataType.LABELLED_SHAPES]

    @property
    def annotation_training_data(self):
        return self._data_dict[PreprocessingDataType.ANNOTATION_TRAINING_DATA]
