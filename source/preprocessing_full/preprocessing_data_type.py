from .image_controller import ImageController
from .annotation_controller.annotation_controller import AnnotationController
from .highlighted_shapes_controller.highlighted_shapes_controller import HighlightedShapesController
from .relative_homography_controller.relative_homography_controller import RelativeHomographyController
from .absolute_homography_controller.absolute_homography_controller import AbsoluteHomographyController
from .camera_controller.camera_controller import CameraController
from .players_controller.players_controller import PlayersController
from .player_histograms_controller.player_histograms_controller import PlayerHistogramsController
from .team_labels_controller.team_labels_controller import TeamLabelsController
from .soccer_field_embedding_controller.soccer_field_embedding_controller import SoccerFieldEmbeddingController
from .optimized_homography_controller.optimized_homography_controller import OptimizedHomographyController
from .camera_sample_controller.camera_sample_controller import CameraSampleController
from .labelled_shapes_controller.labelled_shapes_controller import LabelledShapesController
from .annotation_training_data_controller.annotation_training_data_controller import AnnotationTrainingDataController

class PreprocessingDataType(object):
    '''
    This is stores the possible types of preprocessing data in an "Enum" like format.
    '''
    IMAGE = 0
    ANNOTATION = 1
    HIGHLIGHTED_SHAPES = 2
    RELATIVE_HOMOGRAPHY = 3
    ABSOLUTE_HOMOGRAPHY = 4
    CAMERA = 5
    PLAYERS = 6
    PLAYER_HISTOGRAMS = 7
    TEAM_LABELS = 8
    BALLS = 9
    BALL_TRAJECTORIES = 10
    SOCCER_FIELD_EMBEDDING = 11
    OPTIMIZED_HOMOGRAPHY = 12
    CAMERA_SAMPLE = 13
    LABELLED_SHAPES = 14
    ANNOTATION_TRAINING_DATA = 15
    
#This is a dictionary that stores the name of the PreprocessingDataController associated to each PreprocessingDataType
_DATA_CONTROLLER_DICT = {PreprocessingDataType.IMAGE:      ImageController,
                         PreprocessingDataType.ANNOTATION: AnnotationController,
                         PreprocessingDataType.HIGHLIGHTED_SHAPES: HighlightedShapesController,
                         PreprocessingDataType.RELATIVE_HOMOGRAPHY: RelativeHomographyController,
                         PreprocessingDataType.ABSOLUTE_HOMOGRAPHY: AbsoluteHomographyController,
                         PreprocessingDataType.CAMERA : CameraController,
                         PreprocessingDataType.PLAYERS: PlayersController,
                         PreprocessingDataType.PLAYER_HISTOGRAMS: PlayerHistogramsController,
                         PreprocessingDataType.TEAM_LABELS: TeamLabelsController,
                         PreprocessingDataType.SOCCER_FIELD_EMBEDDING: SoccerFieldEmbeddingController,
                         PreprocessingDataType.OPTIMIZED_HOMOGRAPHY: OptimizedHomographyController,
                         PreprocessingDataType.CAMERA_SAMPLE: CameraSampleController,
                         PreprocessingDataType.LABELLED_SHAPES: LabelledShapesController,
                         PreprocessingDataType.ANNOTATION_TRAINING_DATA: AnnotationTrainingDataController}

def get_data_controller(data_type):
    '''
    data_type - an attribute of PreprocessingDataType
    return - a subclass of PreprocessingDataController
    '''
    return _DATA_CONTROLLER_DICT[data_type]
