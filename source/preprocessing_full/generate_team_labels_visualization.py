import numpy as np
import cv2
import os
from .preprocessing_data_type import PreprocessingDataType
from .team_labels_controller.team_labels_visualization_controller import TeamLabelsVisualizationController

# OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'
# IMAGE_PARENT_DIRECTORY = '/home/daniel/soccer/images'
# INPUT_DATA_NAME = 'images-8_15-9_31'

#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'

#INPUT_DATA_NAME = 'images-0_00-1_00'
INPUT_DATA_NAME = 'images-0_00-10_00'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_full', 'data', INPUT_DATA_NAME, 'stage_2')
VISUALIZATION_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_full', 'visualization', INPUT_DATA_NAME,
                                       'stage_2')

IMAGE_DIRECTORY       = os.path.join(PREPROCESSING_DIRECTORY, 'image')
CAMERA_DIRECTORY      = os.path.join(PREPROCESSING_DIRECTORY, 'camera')
PLAYERS_DIRECTORY     = os.path.join(PREPROCESSING_DIRECTORY, 'players')
TEAM_LABELS_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'team_labels')

PD_DIRECTORY_DICT = {PreprocessingDataType.IMAGE:       IMAGE_DIRECTORY,
                     PreprocessingDataType.CAMERA:      CAMERA_DIRECTORY,
                     PreprocessingDataType.PLAYERS:     PLAYERS_DIRECTORY,
                     PreprocessingDataType.TEAM_LABELS: TEAM_LABELS_DIRECTORY}

VISUALIZATION_OUTPUT_DIRECTORY = os.path.join(VISUALIZATION_DIRECTORY, 'team_labels')

NUM_PROC = 8

def generate_visualizations(visualization_output_directory, pd_directory_dict):
    '''
    visualization_output_directory - a directory name. this is where we store the visualization
    pd_directory_dict - a dictionary with items (PreprocessingDataType attribute, directory name)

    Draw the visualization of the highlighted shapes in the preprocessing data contained in the pd directories
    '''

    if not os.path.isdir(visualization_output_directory):
        print 'making directory: ', visualization_output_directory
        os.mkdir(visualization_output_directory)

    visualization_controller = TeamLabelsVisualizationController(NUM_PROC)
    visualization_controller.generate_visualizations(visualization_output_directory, pd_directory_dict)
    return

if __name__ == '__main__':
    generate_visualizations(VISUALIZATION_OUTPUT_DIRECTORY, PD_DIRECTORY_DICT)
