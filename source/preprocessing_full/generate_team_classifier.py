from .team_classifier_controller.team_classifier_controller import TeamClassifierController
from .preprocessing_data_type import PreprocessingDataType
import os

#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts'
#IMAGE_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/test_data/'

#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'

#INPUT_DATA_NAME = 'images-8_15-9_31'
#INPUT_DATA_NAME = 'images-0_00-1_00'
INPUT_DATA_NAME = 'images-0_00-10_00'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_full', 'data', INPUT_DATA_NAME)

PLAYER_HISTOGRAMS_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'player_histograms')
TEAM_CLASSIFIER_DIRECTORY   = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'team_classifier')

PD_DIRECTORY_DICT = {PreprocessingDataType.PLAYER_HISTOGRAMS:    PLAYER_HISTOGRAMS_DIRECTORY}

N_CLUSTERS = 2

def generate_team_classifier(team_classifier_directory, preprocessing_directory_dict):
    '''
    preprocessing_directory_dict - a dictionary with items (PreprocessingDataType property, directory name)
    team_classifier_directory - a directory name

    We load the player histograms, fit them to a KMeans classifier, and save the classifier in the specified directory.
    '''
    
    if not os.path.isdir(team_classifier_directory):
        print 'making directory: ', team_classifier_directory
        os.mkdir(team_classifier_directory)

    team_classifier_controller = TeamClassifierController(N_CLUSTERS)
    team_classifier_controller.generate_classifier(team_classifier_directory, preprocessing_directory_dict)

    return

if __name__ == '__main__':
    generate_team_classifier(TEAM_CLASSIFIER_DIRECTORY, PD_DIRECTORY_DICT)
