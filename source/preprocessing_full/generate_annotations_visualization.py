import numpy as np
import os
from .preprocessing_data_type import PreprocessingDataType
from .annotation_controller.annotation_visualization_controller import AnnotationVisualizationController

# OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'
# IMAGE_PARENT_DIRECTORY = '/home/daniel/soccer/images'
# INPUT_DATA_NAME = 'images-8_15-9_31'

# OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
# INPUT_DATA_NAME = 'images-0_00-1_00'

OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'
INPUT_DATA_NAME = 'images-0_00-10_00'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_full', 'data', INPUT_DATA_NAME, 'stage_0')
VISUALIZATION_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_full', 'visualization', INPUT_DATA_NAME,
                                       'stage_0')

# PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_full', 'data', INPUT_DATA_NAME, 'stage_1')
# VISUALIZATION_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_full', 'visualization', INPUT_DATA_NAME,
#                                        'stage_1')

PD_DIRECTORY_DICT = {PreprocessingDataType.IMAGE:              os.path.join(PREPROCESSING_DIRECTORY, 'image'),
                     PreprocessingDataType.ANNOTATION: os.path.join(PREPROCESSING_DIRECTORY, 'annotation')}

VISUALIZATION_OUTPUT_DIRECTORY = os.path.join(VISUALIZATION_DIRECTORY, 'annotation')

NUM_PROC = 8

GRASS_COLOR = np.array([0,255,0])
SIDELINES_COLOR = np.array([255,255,255])
PLAYERS_COLOR = np.array([255,0,0])

ALPHA = 0.7

def generate_visualizations(visualization_output_directory, pd_directory_dict):
    '''
    visualization_output_directory - a directory name. this is where we store the visualization
    pd_directory_dict - a dictionary with items (PreprocessingDataType attribute, directory name)

    Draw the visualization of the annotations in the preprocessing data contained in the pd directories
    '''

    if not os.path.isdir(visualization_output_directory):
        print 'making directory: ', visualization_output_directory
        os.mkdir(visualization_output_directory)

    visualization_controller = AnnotationVisualizationController(NUM_PROC, GRASS_COLOR, SIDELINES_COLOR, PLAYERS_COLOR, ALPHA)
    visualization_controller.generate_visualizations(visualization_output_directory, pd_directory_dict)
    return

if __name__ == '__main__':
    generate_visualizations(VISUALIZATION_OUTPUT_DIRECTORY, PD_DIRECTORY_DICT)
