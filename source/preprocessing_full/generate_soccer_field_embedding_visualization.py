import numpy as np
import cv2
import os
from .preprocessing_data_type import PreprocessingDataType
from .soccer_field_embedding_controller.soccer_field_embedding_visualization_controller import SoccerFieldEmbeddingVisualizationController
from ..auxillary.soccer_field_geometry import SoccerFieldGeometry

# OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'
# IMAGE_PARENT_DIRECTORY = '/home/daniel/soccer/images'
# INPUT_DATA_NAME = 'images-8_15-9_31'

#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'

#INPUT_DATA_NAME = 'images-0_00-1_00'
INPUT_DATA_NAME = 'images-0_00-10_00'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_full', 'data', INPUT_DATA_NAME, 'stage_2')
VISUALIZATION_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_full', 'visualization', INPUT_DATA_NAME,
                                       'stage_2')

IMAGE_DIRECTORY =              os.path.join(PREPROCESSING_DIRECTORY, 'image')
HIGHLIGHTED_SHAPES_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'highlighted_shapes')
#SOCCER_FIELD_EMBEDDING_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'soccer_field_embedding')
SOCCER_FIELD_EMBEDDING_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'optimized_homography')

PD_DIRECTORY_DICT = {PreprocessingDataType.IMAGE:              IMAGE_DIRECTORY,
                     PreprocessingDataType.HIGHLIGHTED_SHAPES: HIGHLIGHTED_SHAPES_DIRECTORY,
                     PreprocessingDataType.SOCCER_FIELD_EMBEDDING: SOCCER_FIELD_EMBEDDING_DIRECTORY}

#VISUALIZATION_OUTPUT_DIRECTORY = os.path.join(VISUALIZATION_DIRECTORY, 'soccer_field_embedding')
VISUALIZATION_OUTPUT_DIRECTORY = os.path.join(VISUALIZATION_DIRECTORY, 'optimized_homography')

NUM_PROC = 8

SHAPE_COLOR = (0,255,0)
GRID_COLOR = (0,0,255)
GRID_RADIUS = 3

def load_absolute_soccer_field():
    '''
    Return a SoccerFieldGeometry object
    '''
    ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)
    SCALE = 6
    FIELD_WIDTH = absolute_image.shape[1]-1
    FIELD_LENGTH = absolute_image.shape[0]-1
    GRID_WIDTH = 20
    GRID_LENGTH = 14
    soccer_field = SoccerFieldGeometry(SCALE, FIELD_WIDTH, FIELD_LENGTH, GRID_WIDTH, GRID_LENGTH)
    
    return soccer_field

def generate_visualizations(visualization_output_directory, pd_directory_dict):
    '''
    visualization_output_directory - a directory name. this is where we store the visualization
    pd_directory_dict - a dictionary with items (PreprocessingDataType attribute, directory name)

    Draw the visualization of the highlighted shapes in the preprocessing data contained in the pd directories
    '''

    if not os.path.isdir(visualization_output_directory):
        print 'making directory: ', visualization_output_directory
        os.mkdir(visualization_output_directory)

    soccer_field = load_absolute_soccer_field()
    visualization_controller = SoccerFieldEmbeddingVisualizationController(NUM_PROC, soccer_field, SHAPE_COLOR,
                                                                           GRID_COLOR, GRID_RADIUS)
    visualization_controller.generate_visualizations(visualization_output_directory, pd_directory_dict)
    return

if __name__ == '__main__':
    generate_visualizations(VISUALIZATION_OUTPUT_DIRECTORY, PD_DIRECTORY_DICT)
