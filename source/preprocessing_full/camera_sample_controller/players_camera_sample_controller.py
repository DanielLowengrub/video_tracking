import numpy as np
from ..player_contour import PlayerContour
from ...auxillary.camera_matrix import CameraMatrix
from .camera_sample_controller import CameraSampleController

class PlayersCameraSampleController(CameraSampleController):
    '''
    This CameraSampleController generates camera samples by searching for players in the image and using their heights
    '''
    def __init__(self, num_processors, min_cameras, min_ellipse_area, min_ellipse_matching_score,
                 max_alpha_sq_error, max_alpha_std):
        '''
        min_cameras - the minimum number of cameras in a frame required to estimate the camera matrix by taking the their avg.
        min_ellipse_area - do not consider ellipses whose area is less than this
        min_ellipse_matching_score - do not consider ellipses whose match with the contour they are approximating 
                                     has a worse score than this.
        max_alpha_sq_error - after computing a camera matrix from each ellipse,contour pair in the image, disgard the
                             image if the sq distance of one of the cameras from the mean is larger than this.
        max_alpha_std - same as max_alpha_sq_error, but this time for the std of the camera matrices
        '''
        super(PlayersCameraSampleController, self).__init__(num_processors)
        
        self._min_cameras = min_cameras
        self._min_ellipse_area = min_ellipse_area
        self._min_ellipse_matching_score = min_ellipse_matching_score
        self._max_alpha_sq_error = max_alpha_sq_error
        self._max_alpha_std = max_alpha_std
        
    def _build_camera(self, annotation, H):
        '''
        H - a numpy array with shape (3,3) and type np.float32. It represents a homography from the absolute plane to an
            image plane
        annotation - a SoccerAnnotation object

        return - a CameraMatrix object. If we did not manage to find one, return None.
        '''

        #find the player contours in the annotation
        player_contours = PlayerContour.build_player_contours_from_annotation(annotation)

        
        #use each player contour to compute a camera matrix
        cameras = [pc.compute_camera_matrix(H) for pc in player_contours]

        # #######################
        # #DEBUGGING
        # image = np.zeros(annotation.get_image_shape(), np.uint8)
        # image[annotation.get_players_mask() > 0] = np.array([255,255,255])
        # cv2.drawContours(image, [pc.get_contour().get_cv_contour() for pc in player_contours], -1, (0,255,0), 1)
        # cv2.imshow('player contours', image)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()
        # #######################
        
        #use each player/camera pair to compute an ellipse. The ellipse is obtained by projecting an ellipsoid from absolue
        #world coords to the image using the camera. The ellipsoid is an approximation to a player that is standing at the
        #feet position of the player contour
        ellipses = [pc.compute_ellipse_approximation(C) for pc,C in zip(player_contours, cameras)]

        # #######################
        # #DEBUGGING
        # image_copy = image.copy()
        # for ellipse in ellipses:
        #     primitive_drawing.draw_ellipse(image_copy, ellipse, (255,0,0), 2)
            
        # cv2.imshow('all ellipses', image_copy)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()
        # #######################
        
        #collect triples of (contour, camera, ellipse) that satisfy:
        #1) the area of the ellipse is not too small
        #2) the ellipse is a good approximation to the contour
        
        contours_ellipses_cameras = [(pc,E,C) for pc,E,C in zip(player_contours, ellipses, cameras)
                                     if ((not E.is_degenerate()) and
                                         E.get_area() >= self._min_ellipse_area and
                                         pc.evaluate_match(E) >= self._min_ellipse_matching_score and
                                         not pc.touches_image_boundary())]

        # #######################
        # #DEBUGGING
        # image_copy = image.copy()
        # for (pc,E,C) in contours_ellipses_cameras:
        #     primitive_drawing.draw_ellipse(image_copy, E, (255,0,0), 2)
            
        # cv2.imshow('good ellipses', image_copy)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()
        # #######################

        #if we do not have enough cameras, return None
        if len(contours_ellipses_cameras) < self._min_cameras:
            return None

        #the cameras only differ in the value C[1,2]. record these values in the array alphas and check if they are similar
        #enough to be confident that their average is close to the correct value
        alphas = np.array([C.get_numpy_matrix()[1,2] for pc,E,C in contours_ellipses_cameras])
        sq_error = (alphas - alphas.mean())**2

        print 'alphas: ', alphas
        print '   square errors: ', sq_error
        print '   std:           ', alphas.std()
        
        if (sq_error.max() > self._max_alpha_sq_error) or (alphas.std() > self._max_alpha_std):
            print 'either the max error or std was too big'
            return None

        alpha = alphas.mean()

        print 'alpha: ', alpha
        
        return CameraMatrix.from_homography_and_alpha(H, alpha)
