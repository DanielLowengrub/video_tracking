import numpy as np
import os
import re
from ...auxillary.camera_matrix import CameraMatrix
from ..sequential_pd_controller import SequentialPDController


class CameraSampleController(SequentialPDController):
    '''
    This is a SequentialPDController that is in charge of generating camera matricies using an annotation and an absolute
    homography
    of a soccer field.
    '''

    CAMERA_BASENAME = 'camera.npy'
    
    def __init__(self, num_processors):
        super(CameraSampleController, self).__init__(num_processors)
        
    def _save_data_in_frame_dir(self, frame_dir, camera):
        '''
        frame_dir - a frame directory name
        camera - a CameraMatrix object

        save the camera in the given directory
        '''
        camera_filename = os.path.join(frame_dir, self.CAMERA_BASENAME)
        np.save(camera_filename, camera.get_numpy_matrix())

        return
    
    @classmethod
    def load_data_from_frame_dir(cls, frame_dir):
        '''
        frame_dir - a directory name
        return - a CameraMatrix
        '''
        camera_filename = os.path.join(frame_dir, cls.CAMERA_BASENAME)
        camera = CameraMatrix(np.load(camera_filename))

        return camera
        embedding_dict_filename = os.path.join(frame_dir, cls.EMBEDDING_DICT_BASENAME)

    def _build_camera(self, annotation, absolute_homography):
        '''
        annotation - a SoccerAnnotation object
        absolute_homography - a numpy array with shape (3,3) and type np.float32
    
        return - a CameraMatrix object
        '''
        raise NotImplementedError('_build_camera has not been implemented')
    
    def _build_data_from_pd_tuple(self, pd_tuple):
        '''
        pd_tuple - a PreprocessingDataTuple object
        return - a CameraMatrix object. If we did not manage to build a camera, return None.
        '''
        return self._build_camera(pd_tuple.annotation, pd_tuple.absolute_homography)

