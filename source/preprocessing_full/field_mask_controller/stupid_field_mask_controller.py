import numpy as np
import cv2
from scipy.ndimage import morphology
from .field_mask_controller import FieldMaskController

class StupidFieldMaskController(FieldMaskController):
    '''
    This implementation of FieldMaskController uses a hard coded hue values to find the field
    '''
    def __init__(self, num_processors, hue_min, hue_max):
        '''
        hue_min, hue_max - integers. This defines the range of hues that are declared to be grass
        '''
        super(StupidFieldMaskController, self).__init__(num_processors)
        self._hue_min = hue_min
        self._hue_max = hue_max
        
        self._kernel_open = np.ones((15,15), np.bool)
        self._kernel_close = np.ones((30,30), np.bool)

    def _build_field_mask(self, image):
        '''
        image - a numpy array with shape (n,m,3) and type np.uint8
        return - a numpy array with shape (n,m) and type np.bool
        '''
        #extract the hue channel
        img_hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
        img_h = img_hsv[:,:,0]
        
        grass_filter = (img_h >= self._hue_min) * (img_h <= self._hue_max)
        grass_filter = morphology.binary_closing(grass_filter, self._kernel_open)
        grass_filter = morphology.binary_opening(grass_filter, self._kernel_close)

        return grass_filter


        
