import numpy as np
import os
from ..sequential_pd_controller import SequentialPDController
from ..frame_data.frame_data_loader import FrameDataLoader

class FieldMaskController(SequentialPDController):
    '''
    This is a SequentialPDController that deals with generating and loading sequences of field masks.
    A field mask is a numpy array with type np.bool. It indicates which parts of an image contain a soccer field.
    '''

    #this is how we store the annotations in their frame directory
    FIELD_MASK_BASENAME = 'field_mask.npy'

    def __init__(self, num_processors):
        super(FieldMaskController, self).__init__(num_processors, multiprocessing_mode=self.FRAME_MP)
        
    @classmethod
    def _get_field_mask_filename(cls, frame_dir):
        '''
        frame_dir - the name of a directory containing a field mask
        
        return - the filename of the field mask
        '''
        annotation_filename = os.path.join(frame_dir, cls.FIELD_MASK_BASENAME)
        return annotation_filename    

    def _save_data_in_frame_dir(self, frame_dir, field_mask):
        '''
        frame_dir - a frame directory name
        field_mask - a numpy array with shape (n,m) and type np.bool

        save the frame mask in the directory
        '''
        filename = self._get_field_mask_filename(frame_dir)
        print '      saving field mask to: %s' % filename
        np.save(filename, field_mask)
        return

    @classmethod
    def load_data_from_frame_dir(cls, frame_dir):
        '''
        frame_dir - a directory name
        return - a numpy array with shape (n,m) and type np.bool
        '''
        filename = cls._get_field_mask_filename(frame_dir)
        print '   loading field mask from: %s' % filename
        return np.load(filename)

    def _build_field_mask(self, image):
        '''
        image - a numpy array with shape (n,m,3) and type np.uint8
        return - a numpy array with shape (n,m) and type np.bool
        '''
        raise NotImplementedError('_build_field_mask has not been implemented')
    
    def _build_data_from_pd_tuple(self, pd_tuple):
        '''
        pd_tuple - a PreprocessingDataTuple that stores an image
        return - a numpy array with shape (n,m) and type np.bool
        '''
        return self._build_field_mask(pd_tuple.image)
