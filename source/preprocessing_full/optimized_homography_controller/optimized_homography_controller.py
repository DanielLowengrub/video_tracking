import os
import numpy as np
import pickle
import multiprocessing as mp
import sys
from ..preprocessing_data_controller import PreprocessingDataController
from ..interval_data import IntervalData
from ...auxillary.iterator_len import IteratorLen
from ...auxillary import iterator_operations
from ..frame_data.controller_data import ControllerData
from ..soccer_field_embedding_controller.soccer_field_embedding_controller import SoccerFieldEmbedding

DEBUG = False

class OptimizedHomographyController(PreprocessingDataController):
    '''
    This PDC is in charge of optimizing absolute homographies.
    '''

    HOMOGRAPHY_BASENAME = 'homography.npy'
    EMBEDDING_DICT_BASENAME = 'soccer_keys.pkl'

    def __init__(self, num_processors, num_shapes_bandwidth):
        '''
        num_shapes_bandwidth - this is the window size we use to see if the number of shapes in an embedding is a local
        maximum.
        '''
        super(OptimizedHomographyController, self).__init__(num_processors)

        self._num_shapes_bandwidth = num_shapes_bandwidth

    def _save_data_in_frame_dir(self, frame_dir, soccer_field_embedding):
        '''
        frame_dir - a frame directory name
        soccer_field_embedding - a SoccerFieldEmbedding object

        save the soccer field embedding in the given directory
        '''
        homography_filename = os.path.join(frame_dir, self.HOMOGRAPHY_BASENAME)
        np.save(homography_filename, soccer_field_embedding.homography)

        embedding_dict_filename = os.path.join(frame_dir, self.EMBEDDING_DICT_BASENAME)
        with open(embedding_dict_filename, 'wb') as embedding_dict_file:
            pickle.dump(soccer_field_embedding.embedding_dict, embedding_dict_file)

        return
    
    @classmethod
    def load_data_from_frame_dir(cls, frame_dir):
        '''
        frame_dir - a directory name
        return - a SoccerFieldEmbedding object
        '''
        homography_filename = os.path.join(frame_dir, cls.HOMOGRAPHY_BASENAME)
        homography = np.load(homography_filename)

        embedding_dict_filename = os.path.join(frame_dir, cls.EMBEDDING_DICT_BASENAME)
        with open(embedding_dict_filename, 'rb') as embedding_dict_file:
            embedding_dict = pickle.load(embedding_dict_file)

        return SoccerFieldEmbedding(homography, embedding_dict)

    def _build_optimized_homography(self, highlighted_shapes, soccer_field_embedding):
        '''
        annotation - a SoccerAnnotation object
        highlighted_shapes - a list of HighlightedShape objects
        soccer_field_embedding - a SoccerFieldEmbedding object

        return - a numpy array with shape (3,3) and type np.float32
        '''
        raise NotImplementedError('_build_optimized_homography is not implemented')
    
    def build_data(self, highlighted_shapes, soccer_field_embedding):
        '''
        annotation - a SoccerAnnotation object
        highlighted_shapes - a list of HighlightedShape objects
        soccer_field_embedding - a SoccerFieldEmbedding object

        return - a numpy array with shape (3,3) and type np.float32
        '''
        # print '[%s] using pdc: %d' % (mp.current_process().name, id(self))
        # print '[%s] build_data: source_lines_ph: %s' % (mp.current_process().name, self._shape_distance_minimizer._source_lines_ph)
        # sys.stdout.flush()

        optimized_homography = self._build_optimized_homography(highlighted_shapes, soccer_field_embedding)
        return SoccerFieldEmbedding(optimized_homography, soccer_field_embedding.embedding_dict)
    
    def _filter_pd_tuples(self, enumerated_pd_tuples):
        '''
        enumerated_pd_tuples - a list of tuples (i, PreprocessingDataTuple object). 
           Each pd tuple stores a SoccerAnnotation, a SoccerFieldEmbedding and a list of HighlightedShape objects
        return - a sublist of the input list. They represent the pd tuples whose SoccerFieldEmbedding has a large number
           of shapes. "large" means that the number is a local maximum.
        '''
        #find local maxima
        key = lambda x: len(x.soccer_field_embedding.embedding_dict)
        return list(iterator_operations.extract_local_maxima(iter(enumerated_pd_tuples), key, self._num_shapes_bandwidth))
    
    def _build_interval_data(self, input_interval_data):
        '''
        input_interval_data - an IntervalData object
        return - an IntervalData object
        '''
        #each PreprocessingDataTuple stores a SoccerFieldEmbedding and a list of HighlightedShape objects.
        enumerated_pd_tuples = [(frame_data.frame, frame_data.build_data()) for frame_data in input_interval_data]

        #find the pd_tuples such that the number if shapes in their SoccerFieldEmbedding is a local maximum among all
        #pd_tuples.
        filtered_pd_tuples = self._filter_pd_tuples(enumerated_pd_tuples)

        if DEBUG:
            print 'frame shape counts: '
            print [(frame, len(x.soccer_field_embedding.embedding_dict)) for frame,x in enumerated_pd_tuples]
            print 'filtered frame shape counts: '
            print [(frame, len(x.soccer_field_embedding.embedding_dict)) for frame,x in filtered_pd_tuples]
        
        output_frame_datas = [ControllerData(frame, (pd_tuple.highlighted_shapes, pd_tuple.soccer_field_embedding))
                              for frame,pd_tuple in filtered_pd_tuples]

        return IntervalData(input_interval_data.interval, output_frame_datas)
    
    def _build_interval_data_iter(self, input_interval_data_iter):
        '''
        input_interval_data_iter - an IteratorLen of IntervalData objects
        return - an IteratorLen of IntervalData objects
        '''
        num_intervals = len(input_interval_data_iter)
        output_interval_data_iter = (self._build_interval_data(input_interval_data)
                                     for input_interval_data in input_interval_data_iter)
        
        return IteratorLen(num_intervals, output_interval_data_iter)
    
    def generate_data(self, output_parent_dir, pd_directory_dict):
        '''
        output_parent_dir - a directory name
        pd_directory_dict - a dictionary with items (PreprocessingDataType attr, directory name).

        generate optimized homographies
        '''
        from ..preprocessing_data_loader import PreprocessingDataLoader
        
        #get an IteratorData of IntervalData objects. Each frame provides access to an image and an annotation
        input_interval_data_iter = PreprocessingDataLoader.load_data(pd_directory_dict)

        #Debugging
        # input_interval_data_iter = (interval_data for interval_data in input_interval_data_iter
        #                             if interval_data.interval[0] == 12356)
        # input_interval_data_iter = IteratorLen(1, input_interval_data_iter)
        # iid = next(input_interval_data_iter)
        # iid = next(input_interval_data_iter)
        # iid = next(input_interval_data_iter)
        # iid = next(input_interval_data_iter)
        #############
        
        #build optimized homographies in each interval
        output_interval_data_iter = self._build_interval_data_iter(input_interval_data_iter)

        #save the output data in the output directory
        self._save_interval_data_iter(output_parent_dir, output_interval_data_iter)

        return

