from .optimized_homography_controller import OptimizedHomographyController
from ...image_processing.homography_calculator.shape_distance_minimizer import ShapeDistanceMinimizer
import numpy as np
import multiprocessing as mp

class ShapeDistanceMinimizationController(OptimizedHomographyController):
    '''
    This implementation of OptimizedHomographyController optimizes homographies by trying to minimize the distance
    between the images of the source shapes and the target shapes.
    '''

    def __init__(self, num_processors, soccer_field, image_shape, num_shapes_bandwidth,
                 learning_rate, max_epochs, local_minimum_threshold):
        '''
        num_shapes_bandwidth - this is the window size we use to see if the number of shapes in an embedding is a local
        soccer_field - a SoccerFieldGeometry object
        learning_rate - the step sizes in the gradient direction that we take in each iteration
        max_epochs - the maximum number of iterations
        local_minimum_threshold - we decide that we are in a local minimum if the cost function changes by less than this.
        '''
        super(ShapeDistanceMinimizationController, self).__init__(num_processors, num_shapes_bandwidth)
        self._soccer_field = soccer_field
        self._image_shape = image_shape
        self._shape_distance_minimizer = ShapeDistanceMinimizer(learning_rate, max_epochs, local_minimum_threshold)

    def _build_optimized_homography(self, highlighted_shapes, soccer_field_embedding):
        '''
        highlighted_shapes - a list of HighlightedShape objects
        soccer_field_embedding - a SoccerFieldEmbedding object

        Return a numpy array with shape (3,3) and type np.float32.
        This is an optimized version of the given homography matrix H.
        '''

        #record the shapes of the source and target images. Note that the first coordinate is the length of the y axis
        #and the second is the length of the x axis
        #we add one to the length and width because the length of the y axis of an image is image.shape[0]-1,
        #and the length of the x axis of an image is image.shape[1]-1
        source_image_shape = np.array([self._soccer_field.field_length()+1, self._soccer_field.field_width()+1], np.int32)
        target_image_shape = self._image_shape

        #Build the list of source and target shapes using the given embedding
        source_shapes = []
        target_shapes = []

        for source_key, target_key in soccer_field_embedding.embedding_dict.iteritems():
            source_shapes.append(self._soccer_field.get_highlighted_shape(source_key).get_shape())
            target_shapes.append(highlighted_shapes[target_key].get_shape())

        #use the source and target shapes to optimize the embedding
        optimized_H = self._shape_distance_minimizer.optimize_homography(soccer_field_embedding.homography,
                                                                         source_image_shape, source_shapes,
                                                                         target_image_shape, target_shapes)

        return optimized_H

    def _initialize_controller(self):
        '''
        This performs any initialization that is needed after the PDC has been serialized and sent to an interval_data_worker.
        For example, if the controller needs a tensorflow session, then this method could create the session.
        '''
        print 'initializing shape distance minimizer tf session...'
        self._shape_distance_minimizer.initialize_session()
        return

    def _close_controller(self):
        '''
        This performs any post processing that is needed after the PDC has been serialized and sent to an interval_data_worker.
        For example, if the controller needs a tensorflow session, then this method could close the session.
        '''
        print 'cloding shape distance minimizer tf session'
        self._shape_distance_minimizer.close_session()
        return

