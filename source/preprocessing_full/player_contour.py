import numpy as np
import cv2
import os
from ..data_structures.contour import Contour
from ..image_processing.contour_generator.contour_from_mask_generator import ContourFromMaskGenerator
from ..auxillary.camera_matrix import CameraMatrix
from ..auxillary import planar_geometry
from ..auxillary import mask_operations
from ..auxillary import world_to_image_projections
from ...testers.auxillary import primitive_drawing

class PlayerContour(object):
    '''
    A player contour is a wrapper around a Contour object which we assume is the outline of a player.
    It adds functionality such as:
    * Compute the positions of the head and feet
    * Generate a CameraMatrix from a planar homography matrix
    * Project an ellipsoid from absolute coordinates using a CameraMatrix
    * Evaluate how well the contour matches an Ellipse
    '''

    MARGIN = 5 #a pixel is defined to be on the image boundary if it is within MARGIN of the boundary.
    def __init__(self, contour):
        '''
        contour - a Contour object
        '''

        self._contour = contour

        #we calculate these if needed
        self._feet_image_position = None
        self._head_image_position = None

    @classmethod
    def build_player_contours_from_annotation(cls, annotation):
        '''
        annotation - a SoccerAnnotation object

        return - a list of PlayerContour objects
        '''
        #get a mask containing the pixels which could be players
        players_mask = annotation.get_players_mask()

        #build a blank image that is the same size as the annotation mask
        #the reason is that the contour generator requires us to pass the image that the contours are on,
        #but we only care about the coordinates of the contours, not the image.
        image = np.zeros(annotation.get_image_shape(), np.uint8)
        
        #find contours in the mask
        contour_generator = ContourFromMaskGenerator()
        contours = contour_generator.generate_contours(image, players_mask)

        #remove all child contours
        contours = (Contour(c.get_cv_contour(), image) for c in contours)

        #and cast the contours to PlayerContour objects
        player_contours = [PlayerContour(c) for c in contours]

        return player_contours

    def get_contour(self):
        '''
        return - a Contour object
        '''
        return self._contour
    
    def _compute_feet_and_head_image_positions(self):
        '''
        compute _feet_image_position and _head_image_position
        feet_image_position, head_image_position - numpy array with shape (2,) and type np.float32
        '''
        #if we have already computed the feet and head positions, do not do it again
        if self._feet_image_position is not None:
            return
        
        contour_array = self._contour.get_cv_contour().reshape((-1,2))
        index_of_y_max = contour_array[:,1].argmax()
        feet_y_coord = contour_array[index_of_y_max,1]
        
        index_of_y_min = contour_array[:,1].argmin()
        head_y_coord = contour_array[index_of_y_min,1]
        
        mean_x_position = contour_array[:,0].mean()
        
        self._feet_image_position = np.array([mean_x_position, feet_y_coord])
        self._head_image_position = np.array([mean_x_position, head_y_coord])
        
        return

    def touches_image_boundary(self):
        '''
        return True iff the contour touches the image boundary.
        '''
        #the coordinates of the bottom right pixel of the image
        image_shape = self._contour.get_image().shape
        top_left = self.MARGIN*np.ones(2)
        bottom_right = np.array([image_shape[1], image_shape[0]]) - self.MARGIN*np.ones(2)

        np_contour = self._contour.get_cv_contour().reshape((-1,2))
        min_coords = np_contour.min(axis=0)
        max_coords = np_contour.max(axis=0)

        # print 'max coords: ', max_coords
        # print 'bottom right: ', bottom_right
        return np.any(min_coords <= top_left) or np.any(max_coords >= bottom_right)
    
    def compute_feet_absolute_position(self, H):
        '''
        H - a numpy array with shape (3,3) and type np.float32. 
        It represents a homography matrix from the world z=0 plane to an image plane.

        return - a numpy array with shape (2,) and type np.float32. It represents the position of the players feet
        in absolute coordinates.
        '''
        #H_inv is a homography from the image plane to the absolute plane
        H_inv = np.linalg.inv(H)
        self._compute_feet_and_head_image_positions()

        feet_absolute_position = planar_geometry.apply_homography_to_point(H_inv, self._feet_image_position)    

        return feet_absolute_position
    
    def compute_camera_matrix(self, H):
        '''
        H - a numpy array with shape (3,3) and type np.float32. 
            It represents a homography matrix from the world z=0 plane to an image plane.

        return - a CameraMatrix object.
        We return a camera matrix that maps an average player in world coordinates to this player contour.
        '''
        self._compute_feet_and_head_image_positions()
        feet_absolute_position = self.compute_feet_absolute_position(H)
        head_world_position = np.append(feet_absolute_position, world_to_image_projections.PLAYER_HEIGHT)

        camera_matrix = CameraMatrix.from_planar_homography(H, head_world_position.reshape((-1,3)),
                                                            self._head_image_position.reshape((-1,2)))

        return camera_matrix

    def compute_ellipse_approximation(self, camera_matrix):
        '''
        camera_matrix - a CameraMatrix object

        return - an Ellipse object. 
                 We obtain the ellipse by first computing the position of the players feet in the world
                 coordinates. We then place an ellipsoid at that position whose height and width approximates that of a player.
                 Finally, project the ellipsoid to the image.
        '''
        self._compute_feet_and_head_image_positions()

        #compute the absolute position of the feet
        H = camera_matrix.get_homography_matrix()
        feet_absolute_position = self.compute_feet_absolute_position(H)

        #project an ellipsoid that is "standing" at this absolute position
        ellipse = world_to_image_projections.project_player_to_image(camera_matrix, feet_absolute_position)

        return ellipse

    def evaluate_match(self, ellipse):
        '''
        ellipse - an Ellipse object
        
        Return a float between 0 and 1. the higher the number the better the ellipse fits this contour.
        '''
        contour_mask = self._contour.get_mask()
        ellipse_mask = ellipse.get_mask(self._contour.get_image())

        mask_union = mask_operations.get_union(contour_mask, ellipse_mask)
        mask_intersection = contour_mask * ellipse_mask

        total_area = mask_union.sum()
        overlap_area = mask_intersection.sum()
        
        overlap_percentage = overlap_area / total_area
        
        return overlap_percentage

    
