from .preprocessing_data_controller import PreprocessingDataController
from .frame_data.controller_data import ControllerData
from .interval_data import IntervalData
from ..auxillary.iterator_len import IteratorLen

class SequentialPDController(PreprocessingDataController):
    '''
    This is a subclass of PreprocessingDataController which handles data that exists in each frame of each interval.
    I.e, for each interval (ik,jk) we store a *sequence* of objects: object_0,object_1,...,object_{jk-ik}

    All of the data controlled by this object is stored in the following format:
    interval_i0_j0/
       frame_0/
       frame_1/
       ...
       frame_{j0-i0}/

    interval_i1_j1/
       frame_0/
       frame_1/
       ...
       frame_{i1-j1}/

    ...

    interval_ik_jk is a directory which stores frames starting from frame ik and ending with frame jk.
    frame_l is a file or directory which stores the data contained in the l-th frame of the corresponding interval.

    The important methods that are provided in addition to those in PreprocessingDataController are:
    * _build_interval_data_iter: this takes as input an IteratorLen of IntervalData objects which represent input data in each
       frame. This method uses the data in each frame to create output data for that frame. It returns a new IteratorLen of
       IntervalData.
    * generate_data: this takes as input a dictionary of directories containing PreprocessingFrameData objects. It loads
       an IteratorLen of IntervalData objects which hold the PreprocessingFrameData in each frame, and use it to generate
       output data in each frame.

    Both of these methods rely on the method _build_data_from_pd_tuple which must be implemented by the user. 
    The upshot is that if the data generated in each frame depends only on the PreprocessingDataTuple object in that frame, 
    then the subclass only has to implement _build_data_from_pd_tuple which takes a PreprocessingDataTuple as input.
    Once that is done, generate_data will take case of loading the PDT objects, passing them to _build_data_from_pd_tuple,
    and saving the output in the appropriate directory.

    The important methods (in addition to the ones in PreprocessingDataController) to implement in subclasses are:
    * _build_data_from_pd_tuple: this specifies how to use input PreprocessingDataTuple to generate output data 
    '''
    def __init__(self, num_processors, multiprocessing_mode=PreprocessingDataController.INTERVAL_MP):
        #print 'sequential pd controller: mp mode = %d' % multiprocessing_mode
        
        super(SequentialPDController, self).__init__(num_processors, multiprocessing_mode,
                                                     add_to_existing_intervals=True)

    def _build_data_from_pd_tuple(self, pd_tuple):
        '''
        pd_tuple - a PreprocessingDataTuple object
        return - the data build out of the of tuple
        '''
        raise NotImplementedError('_build_data_from_pd_tuple must be implemented by the subclass')

    def build_data(self, frame_data_tuple):
        '''
        frame_data_tuple - a FrameDataTuple object
        return - the data we want to build for this frame
        '''
        pd_tuple = frame_data_tuple.build_data()
        return self._build_data_from_pd_tuple(pd_tuple)
    
    def _build_interval_data(self, input_interval_data):
        '''
        input_interval_data - an IntervalData object that stores data associated to this frame
        return - an IntervalData object which represents the data we generated for this frame
        '''
        interval = input_interval_data.interval

        #note that input_fd is a FrameDataTuple object
        frame_datas = [ControllerData(input_fd.frame, (input_fd,)) for input_fd in input_interval_data]

        return IntervalData(interval, frame_datas)

    def _build_interval_data_iter(self, input_interval_data_iter):
        '''
        input_interval_data_iter - an IteratorLen of IntervalData objects which stores input data in each frame

        return - an IteratorLen of IntervalData objects which stores the data we generated in each frame
        '''

        num_intervals = len(input_interval_data_iter)
        output_interval_data_iter = (self._build_interval_data(input_interval_data)
                                     for input_interval_data in input_interval_data_iter)
        
        return IteratorLen(num_intervals, output_interval_data_iter)
    
    def generate_data(self, output_parent_dir, pd_directory_dict):
        '''
        output_parent_dir - a directory name. This is where we store the output
        pd_directory_dict - a dictionary with items (PreprocessingDataType attribute, directory name)

        Assume that the directories in the pd dict all contain the same intervals.
        For each of those intervals, use the data they contain as input to generate new interval data. The new interval
        data is stored in output_base_dir.
        '''
        from .preprocessing_data_loader import PreprocessingDataLoader

        #get an iterator of input data in each of the frames
        input_interval_data_iter = PreprocessingDataLoader.load_data(pd_directory_dict)

        #Debugging
        # input_interval_data_iter = (interval_data for interval_data in input_interval_data_iter
        #                            if interval_data.interval[0] == 43756)
        # input_interval_data_iter = IteratorLen(1, input_interval_data_iter)
        #iid = next(input_interval_data_iter)
        #iid = next(input_interval_data_iter)
        #iid = next(input_interval_data_iter)
        #iid = next(input_interval_data_iter)
        #################
        
        #use the input data to build an iterator of output data
        output_interval_data_iter = self._build_interval_data_iter(input_interval_data_iter)

        #save the output data in the output directory
        self._save_interval_data_iter(output_parent_dir, output_interval_data_iter)
        
        return


        
    
