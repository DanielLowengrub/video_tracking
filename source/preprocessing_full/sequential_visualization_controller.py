from .preprocessing_visualization_controller import PreprocessingVisualizationController

class SequentialVisualizationController(PreprocessingVisualizationController):
    '''
    This is a PVC that assumes that the data being visualized is saved in every frame in every interval. I.e, there are no
    jumps in the frames in which the data is stored.
    '''

    def __init__(self, num_processors):
        super(SequentialVisualizationController, self).__init__(num_processors)

    def generate_visualizations(self, visualization_parent_dir, pd_directory_dict):
        '''
        visualization_parent_dir - a directory name. This is where we store the visualization
        pd_directory_dict - a dictionary with items (PreprocessingDataType attribute, directory name)

        Assume that the directories in the pd dict all contain the same intervals.
        For each of those intervals, create visualizations for the data they contain and save the images in the specified
        directory.
        '''
        from .preprocessing_data_loader import PreprocessingDataLoader
        
        #get an iterator of input data in each of the frames
        interval_data_iter = PreprocessingDataLoader.load_data(pd_directory_dict)

        #Debugging
        # from ..auxillary.iterator_len import IteratorLen
        # interval_data_iter = (interval_data for interval_data in interval_data_iter
        #                             if interval_data.interval[0] == 12356)
        # interval_data_iter = IteratorLen(1, interval_data_iter)
        ############
        
        #generate visualizations for the intervals in the iterator
        self._generate_interval_data_iter_visualization(visualization_parent_dir, interval_data_iter)
        
        return


