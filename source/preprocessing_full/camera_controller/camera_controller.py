import os
import itertools
import numpy as np
from ..preprocessing_data_controller import PreprocessingDataController
from ..interval_data import IntervalData
from ...auxillary.iterator_len import IteratorLen
from ...auxillary import iterator_operations
from ..frame_data.raw_frame_data import RawFrameData
from ...auxillary.camera_matrix import CameraMatrix

class CameraController(PreprocessingDataController):
    '''
    This PDC is in charge of generating cameras. It interpolates between camera samples using
    absolute_homographies
    '''

    #this is how we store the homographies in their frame directory
    CAMERA_BASENAME = 'homography.npy'

    def __init__(self, num_processors):
        super(CameraController, self).__init__(num_processors)

    def _save_data_in_frame_dir(self, frame_dir, camera):
        '''
        frame_dir - a frame directory name
        camera - a CameraMatrix object

        save the camera in the given directory
        '''
        camera_filename = os.path.join(frame_dir, self.CAMERA_BASENAME)
        np.save(camera_filename, camera.get_numpy_matrix())

        return
    
    @classmethod
    def load_data_from_frame_dir(cls, frame_dir):
        '''
        frame_dir - a directory name
        return - a CameraMatrix
        '''
        camera_filename = os.path.join(frame_dir, cls.CAMERA_BASENAME)
        #print '   loading camera from: %s' % frame_dir
        camera = CameraMatrix(np.load(camera_filename))

        return camera


    def _interpolate_cameras(self, absolute_homographies, enumerated_camera_samples):
        '''
        absolute_homographies - a length n list of np arrays with shape (3,3)
        enumerated_camera_samples - a list of tuples (integer, CameraMatrix)

        return - a length n list of CameraMatrix objects
        '''
        raise NotImplementedError('_interpolate_cameras has not been implemented')
    
    def _build_interval_data(self, abs_homography_interval_data, camera_sample_interval_data):
        '''
        abs_homography_interval_data - an IntervalData object
        camera_sample_interval_data - an IntervalData object
        return - an IntervalData object
        '''
        enumerated_camera_samples = [(fd.frame, fd.build_data().camera_sample) for fd in camera_sample_interval_data]
                                     
        if len(enumerated_camera_samples) == 0:
            return IntervalData(abs_homography_interval_data.interval, [])
        
        #interpolate between the cameras using the absolute homographies
        absolute_homographies = [fd.build_data().absolute_homography for fd in abs_homography_interval_data]
        interpolated_cameras = self._interpolate_cameras(absolute_homographies, enumerated_camera_samples)

        output_frame_datas = [RawFrameData(frame, camera) for frame,camera in enumerate(interpolated_cameras)]
        return IntervalData(abs_homography_interval_data.interval, output_frame_datas)
    
    def _build_interval_data_iter(self, abs_homography_interval_data_iter, camera_sample_interval_data_iter):
        '''
        abs_homography_interval_data_iter - an IteratorLen of IntervalData objects
        camera_sample_interval_data_iter - an IteratorLen of IntervalData objects

        return - an IteratorLen of IntervalData objects
        '''
        num_intervals = len(abs_homography_interval_data_iter)
        abs_camera_interval_data_iter = itertools.izip(abs_homography_interval_data_iter, camera_sample_interval_data_iter)
        output_interval_data_iter = (self._build_interval_data(abs_interval_data, camera_interval_data)
                                     for abs_interval_data, camera_interval_data in abs_camera_interval_data_iter)
        
        return IteratorLen(num_intervals, output_interval_data_iter)
    
    def generate_data(self, output_parent_dir, absolute_homography_directory, camera_sample_directory):
        '''
        output_parent_dir - a directory name
        absolute_homography_directory - a directory storing absolute homographies in all frames
        camera_sample_directory - a directory storing CameraMatrix objects in some of the frames of each interval

        generate cameras for all frames
        '''
        from ..preprocessing_data_loader import PreprocessingDataLoader
        from ..preprocessing_data_type import PreprocessingDataType

        abs_homography_dict = {PreprocessingDataType.ABSOLUTE_HOMOGRAPHY: absolute_homography_directory}
        camera_sample_dict  = {PreprocessingDataType.CAMERA_SAMPLE:       camera_sample_directory}
        
        #load an IteratorData of IntervalData objects. We do not load them at once since we want abs homographies
        #in all frames, but there are only camera samples in some of the frames
        abs_homography_interval_data_iter = PreprocessingDataLoader.load_data(abs_homography_dict)
        camera_sample_interval_data_iter  = PreprocessingDataLoader.load_data(camera_sample_dict)

        # #Debugging
        # abs_homography_interval_data_iter = (interval_data for interval_data in abs_homography_interval_data_iter
        #                                      if interval_data.interval[0] == 12356)
        # abs_homography_interval_data_iter = IteratorLen(1, abs_homography_interval_data_iter)

        # camera_sample_interval_data_iter = (interval_data for interval_data in camera_sample_interval_data_iter
        #                                 if interval_data.interval[0] == 12356)
        # camera_sample_interval_data_iter = IteratorLen(1, camera_sample_interval_data_iter)

        # iid = next(rel_homography_interval_data_iter)
        # iid = next(embedding_interval_data_iter)
        # iid = next(rel_homography_interval_data_iter)
        # iid = next(embedding_interval_data_iter)
        # iid = next(rel_homography_interval_data_iter)
        # iid = next(embedding_interval_data_iter)
        ##################
        
        #build absolute homographies in each interval
        output_interval_data_iter = self._build_interval_data_iter(abs_homography_interval_data_iter,
                                                                   camera_sample_interval_data_iter)

        #save the output data in the output directory
        self._save_interval_data_iter(output_parent_dir, output_interval_data_iter)

        return

