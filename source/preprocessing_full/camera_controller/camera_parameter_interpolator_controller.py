import numpy as np
import itertools
import collections
from ...auxillary.camera_matrix import CameraMatrix
from ...auxillary import planar_geometry, fit_camera_to_homography
from .camera_controller import CameraController

CameraParameters = collections.namedtuple('CameraParameters', ['camera', 'C', 'R', 'T'])

class CameraParameterInterpolatorController(CameraController):
    '''
    This implementation of CameraController interpolates the camera positions, and uses that, together with the
    homography of the target frame to compute the camera matrix
    '''
    def __init__(self, num_processors):
        super(CameraParameterInterpolatorController, self).__init__(num_processors)
        
    def _build_camera_parameters(self, camera):
        '''
        camera - a CameraMatrix object
        is_sample - boolean

        return a CameraParameters object which records the parameters of the camera.
        '''
        C = camera.get_internal_parameters()
        R = camera.get_world_to_camera_rotation_matrix()
        T = camera.get_camera_position()

        #R[1,2] should be 1 or -1. If it is -1, then multiply both R and C by -1. This flips R[1,2] to 1, and does not change
        #the camera that these parameters represent
        if R[1,2] < 0:
            C = -1*C
            R = -1*R
            
        return CameraParameters(camera,C,R,T)

    def _vectors_to_rotation_matrices(self, vectors):
        '''
        vectors - a numpy array with shape (n,2)
        return - a numpy array with shape (n,3,3)
        
        In this method we deal with rotation matrices that have the following form:
        [r0 -r1 0]
        [0  0   1]
        [r1 r0  0]
    
        The input is a list of vectors (r0,r1), and the output is an array containing to corresponding matrices.
        '''
        rotation_matrices = np.zeros((len(vectors),3,3), np.float64)
        rotation_matrices[:,0,0] =  vectors[:,0]
        rotation_matrices[:,2,0] =  vectors[:,1]
        rotation_matrices[:,0,1] = -vectors[:,1]
        rotation_matrices[:,2,1] =  vectors[:,0]
        rotation_matrices[:,1,2] =  1

        return rotation_matrices
    
    def _interpolate_camera_parameters(self, start_frame, end_frame, start_parameters, end_parameters):
        '''
        start_frame, end_frame - integers
        start_camera_parameters, end_camera_parameters - CameraParameters objects

        return a length (end_frame - start_frame - 1) list of CameraParameters objects, which interpolate between
        start/end_camera_parameters in frames start_frame < k < end_frame
        '''
        # print '   interpolating cameras from frame %d to frame %d.' % (start_frame, end_frame)
        # print '   start parameters:\nC:%s\nR:\n%s\nT:%s' % (start_parameters.C, start_parameters.R, start_parameters.T)
        # print '   end parameters:\nC:%s\nR:\n%s\nT:%s' % (end_parameters.C, end_parameters.R, end_parameters.T)
        
        #if start_frame=0 then N=end_frame. In general, there are N-1 frames between start_frame and end_frame
        N = end_frame - start_frame

        #the positions and internal camera parameters are linearly interpolated
        #the interpolation weights for the last frame are: 1/N, 2/N, ..., (N-1)/N
        #E.g, the parameters of frame start_frame+1 are (1 - 1/N)*parameters[start_frame] + (1/N)*parameters[last_frame]
        end_weights = np.arange(1, N).astype(np.float64) / N
        start_weights = 1 - end_weights

        #this is a numpy array with shape (N-1, 3, 3)
        interpolated_Cs = ((start_weights.reshape((-1,1,1))*start_parameters.C) +
                           (end_weights.reshape((-1,1,1))*end_parameters.C))

        # print '   interpolated Cs:'
        # print '\n'.join(map(str,interpolated_Cs))
        
        #this is a numpy array with shape (N-1, 3)
        interpolated_Ts = ((start_weights.reshape((-1,1))*start_parameters.T) +
                           (end_weights.reshape((-1,1))*end_parameters.T))

        # print '    interpolated Ts:'
        # print '\n'.join(map(str,interpolated_Ts))

        #to interpolate the rotation matrices, we assume that they are all of the form:
        #[r0 -r1 0]
        #[0  0   1]
        #[r1 r0  0]
        #in particular, vector=(r0,r1) is a point on a circle so we can interpolate the angles they make with the x axis
        start_vector = np.array([start_parameters.R[0,0], start_parameters.R[2,0]])
        end_vector = np.array([end_parameters.R[0,0], end_parameters.R[2,0]])

        # print '   start vector: ', start_vector
        # print '   end vector:   ', end_vector
        
        #this is an array with shape (N-1, 2)
        interpolated_vectors = planar_geometry.interpolate_on_circle(start_vector, end_vector, start_weights)

        # print '   interpolated vectors:'
        # print '\n'.join(map(str,interpolated_vectors))

        #use the vectors to create a list of rotation matrices
        interpolated_Rs = self._vectors_to_rotation_matrices(interpolated_vectors)

        # print 'interpolated Rs:'
        # print '\n'.join(map(str,interpolated_Rs))

        #zip up the interpolated C,R and T arrays into a list of camera parameters
        return [CameraParameters(None,C,R,T)
                for C,R,T in itertools.izip(interpolated_Cs, interpolated_Rs, interpolated_Ts)]

                           
    def _compute_interpolated_camera_parameters(self, enumerated_camera_samples, num_frames):
        '''
        enumerated_camera_samples - a list of tuples (l,Cl) where l<num_frames in an integer and Cl is a CameraMatrix object,

        return - output = a list of CameraParameters of length num_frames. 
        C and R are numpy arrays with shape (3,3), T is a numpy array with shape (3,).
        
        if l is the index of one of the camera samples, then output[l] represents the parameters of camera Cl and
        output[l].is_sample = True.
        otherwise, if l[i] < k < l[i+1] then output[k] is obtained by interpolating the parameters of the camera sample
        at frame l[i] and the camera sample at frame l[i+1]. output[k].is_sample = False
        '''
        # print 'computing interpolated camera parameters...'
        interpolated_camera_parameters = []

        start_frame, start_camera = enumerated_camera_samples[0]
        start_camera_parameters = self._build_camera_parameters(start_camera)

        #interpolate backwards from the first sample to frame 0 by just copying the first camera sample
        #note that if the first frame is zero, this just adds an empty list
        C,R,T = start_camera_parameters.C, start_camera_parameters.R, start_camera_parameters.T
        interpolated_camera_parameters.append([CameraParameters(None,C,R,T) for i in range(start_frame)])

        #add an entry for the first sample camera itself.
        interpolated_camera_parameters.append([start_camera_parameters])

        for end_frame, end_camera in enumerated_camera_samples[1:]:
            #interpolate camera parameters for frames: start_frame < k < end_frame
            end_camera_parameters = self._build_camera_parameters(end_camera)
            interval_camera_parameters = self._interpolate_camera_parameters(start_frame, end_frame,
                                                                             start_camera_parameters, end_camera_parameters)
            interpolated_camera_parameters.append(interval_camera_parameters)
            interpolated_camera_parameters.append([end_camera_parameters])

            start_frame, start_camera_parameters = end_frame, end_camera_parameters
            
        #check of we have to interpolate forwards from the final camera sample to the last frame
        if start_frame < num_frames-1:
            C,R,T = start_camera_parameters.C, start_camera_parameters.R, start_camera_parameters.T
            interpolated_camera_parameters.append([CameraParameters(None,C,R,T)
                                                   for i in range(start_frame+1, num_frames)])

        #chain together all of the interpolated parameters
        interpolated_camera_parameters = itertools.chain.from_iterable(interpolated_camera_parameters)
        return interpolated_camera_parameters

    def _compute_camera(self, H, camera_parameters):
        '''
        H - a numpy array with shape (3,3)
        camera_parameters - a CameraParameters object

        return a CameraMatrix object
        '''
        #if camera_parameters already has a camera, then just return that one
        if camera_parameters.camera is not None:
            return camera_parameters.camera

        #otherwise, start from the given camera paramaters, and perform gradient descent to make the camera that they represent
        #have a homography close to H
        C_initial, R_initial, T = camera_parameters.C, camera_parameters.R, camera_parameters.T
        C_opt, R_opt = fit_camera_to_homography.fit_camera_to_homography(C_initial, R_initial, T, H)
        camera = CameraMatrix.from_position_rotation_internal_parameters(T, R_opt, C_opt)

        alpha = camera.get_alpha()
        camera = CameraMatrix.from_homography_and_alpha(H, alpha)

        #print 'frame %d: alpha=%f: ' % (frame_index, alpha)
        
        return camera

    def _interpolate_cameras(self, homographies, enumerated_camera_samples):
        '''
        enumerated_camera_samples - an list of tuples (l,Cl) where l is an integer and Cl is a camera matrix
        homographies - an list of homographies H0,...,Hn

        return - a list of CameraMatrix objects

        The indices lk of the cameras satisfy: 0<=l0 < ... < lk <= n.
        We compute the cameras of each frame m that does not have a camera (m,Cm) by using Hm and the two closes cameras
        l < m < l'
        '''
        num_frames = len(homographies)

        # print 'interpolating cameras for %d frames...' % num_frames
        
        #compute the camera position of each of the cameras that we want to interpolate.
        #this is a length len(homographies) list of CameraParameters objects
        interpolated_camera_parameters = self._compute_interpolated_camera_parameters(enumerated_camera_samples, num_frames)

        interpolated_cameras = [self._compute_camera(H, camera_params)
                                for H, camera_params in zip(homographies, interpolated_camera_parameters)]

        return interpolated_cameras

