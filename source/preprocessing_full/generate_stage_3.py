import os
from .preprocessing_data_controller import PreprocessingDataController

'''
This script saves a subset of the preprocessing data that is needed for tracking in stage_3/. It also removes empty intervals
'''
#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'

#INPUT_DATA_NAME = 'images-0_00-1_00'
INPUT_DATA_NAME = 'images-0_00-10_00'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_full', 'data', INPUT_DATA_NAME)

STAGE_2_IMAGE_DIRECTORY       = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'image')
STAGE_2_ANNOTATION_DIRECTORY  = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'annotation')
STAGE_2_CAMERA_DIRECTORY      = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'camera')
STAGE_2_PLAYERS_DIRECTORY     = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'players')
STAGE_2_TEAM_LABELS_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'team_labels')

STAGE_3_IMAGE_DIRECTORY       = os.path.join(PREPROCESSING_DIRECTORY, 'stage_3', 'image')
STAGE_3_ANNOTATION_DIRECTORY  = os.path.join(PREPROCESSING_DIRECTORY, 'stage_3', 'annotation')
STAGE_3_CAMERA_DIRECTORY      = os.path.join(PREPROCESSING_DIRECTORY, 'stage_3', 'camera')
STAGE_3_PLAYERS_DIRECTORY     = os.path.join(PREPROCESSING_DIRECTORY, 'stage_3', 'players')
STAGE_3_TEAM_LABELS_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'stage_3', 'team_labels')

DIRECTORY_TUPLES = [(STAGE_2_IMAGE_DIRECTORY,       STAGE_3_IMAGE_DIRECTORY),
                    (STAGE_2_ANNOTATION_DIRECTORY,  STAGE_3_ANNOTATION_DIRECTORY),
                    (STAGE_2_PLAYERS_DIRECTORY,     STAGE_3_PLAYERS_DIRECTORY),
                    (STAGE_2_TEAM_LABELS_DIRECTORY, STAGE_3_TEAM_LABELS_DIRECTORY)]
                               
NUM_PROC = 8

def generate_stage_3():
    pd_controller = PreprocessingDataController(NUM_PROC)
    
    #first remove the empty intervals from the camera directory
    print 'making directory: ', STAGE_3_CAMERA_DIRECTORY
    os.mkdir(STAGE_3_CAMERA_DIRECTORY)

    print 'removing empty intervals from %s and saving them in %s' % (STAGE_2_CAMERA_DIRECTORY, STAGE_3_CAMERA_DIRECTORY)
    pd_controller.remove_empty_intervals(STAGE_3_CAMERA_DIRECTORY, STAGE_2_CAMERA_DIRECTORY)

    for stage_2_directory, stage_3_directory in DIRECTORY_TUPLES:
        print 'making directory: ', stage_3_directory
        os.mkdir(stage_3_directory)
        
        print 'removing empty intervals from %s and saving them in %s' % (stage_2_directory, stage_3_directory)
        pd_controller.refine_intervals_from_directory(stage_3_directory, stage_2_directory,
                                                      STAGE_3_CAMERA_DIRECTORY)

if __name__ == '__main__':
    generate_stage_3()
