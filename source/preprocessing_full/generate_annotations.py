from .annotation_controller.nn_annotation_controller import NNAnnotationController
import os
import numpy as np
import cv2

#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts'
#IMAGE_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/test_data/'

# OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
# IMAGE_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/images'

OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'
IMAGE_PARENT_DIRECTORY = '/home/daniel/soccer/images'

#INPUT_DATA_NAME = 'images-8_15-9_31'
#INPUT_DATA_NAME = 'images-0_00-1_00'
INPUT_DATA_NAME = 'images-0_00-10_00'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_full', 'data', INPUT_DATA_NAME)
NN_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'nn', INPUT_DATA_NAME, 'mask_16')

ANNOTATION_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'stage_0', 'annotation')

NUM_PROC = 8

#This is a mask containing the portions of the screen dedicated to heads up display
HUD_MASK = np.zeros((352,624), np.bool)
HUD_MASK[20:39, 113:216] = True
HUD_MASK[20:40, 489:580] = True

BACKGROUND_CLOSURE_RADIUS = 20
PLAYER_CLOSURE_RADIUS = 3

def generate_data(output_parent_directory, nn_directory, hud_mask):
    '''
    output_parent_directory - the directory in which we will save the annotations
    nn_directory - a directory containing files image_0.npy,...,image_N.npy. 
       Each numpy array has shape (n,m) and type int32.
       The values at each pixel represent one of: grass, player, sideline, background
    hud_mask - a numpy array with shape (n,m) and type np.bool. It represents portions of the image that are used for
       heads up display
    '''
    if not os.path.isdir(output_parent_directory):
        print 'making directory: ', output_parent_directory
        os.mkdir(output_parent_directory)

    # print 'displaying the hud mask...'
    # cv2.imshow('hud mask', np.float32(hud_mask))
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    
    annotation_controller = NNAnnotationController(NUM_PROC, HUD_MASK, BACKGROUND_CLOSURE_RADIUS, PLAYER_CLOSURE_RADIUS)
    annotation_controller.generate_data(output_parent_directory, nn_directory)

    return

if __name__ == '__main__':
    generate_data(ANNOTATION_DIRECTORY, NN_DIRECTORY, HUD_MASK)
