import os
import cv2
import numpy as np
from .camera_controller.camera_parameter_interpolator_controller import CameraParameterInterpolatorController
from .preprocessing_data_type import PreprocessingDataType

#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts'
#IMAGE_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/test_data/'

#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'

#INPUT_DATA_NAME = 'images-8_15-9_31'
#INPUT_DATA_NAME = 'images-0_00-1_00'
INPUT_DATA_NAME = 'images-0_00-10_00'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_full', 'data', INPUT_DATA_NAME)

ABSOLUTE_HOMOGRAPHY_DIRECTORY   = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'absolute_homography')
CAMERA_SAMPLE_DIRECTORY         = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'camera_sample')

CAMERA_DIRECTORY   = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'camera')

NUM_PROC = 8

def generate_data(output_parent_directory, absolute_homography_directory, camera_sample_directory):
    '''
    output_parent_directory - the directory in which we will save the annotations
    absolute_homography_directory - a directory storing absolute homographies in all frames
    camera_sample_directory - a directory storing CameraMatrix objects in some of the frames of each interval
    '''
    if not os.path.isdir(output_parent_directory):
        print 'making directory: ', output_parent_directory
        os.mkdir(output_parent_directory)

    camera_controller = CameraParameterInterpolatorController(NUM_PROC)
    camera_controller.generate_data(output_parent_directory,
                                    absolute_homography_directory, camera_sample_directory)

    return

if __name__ == '__main__':
    generate_data(CAMERA_DIRECTORY, ABSOLUTE_HOMOGRAPHY_DIRECTORY, CAMERA_SAMPLE_DIRECTORY)
        
        
