import multiprocessing
import os
import re
import functools
import datetime
import cv2
import pickle
from ..preprocessing_full import filename_loader
from .sequential_pd_controller import SequentialPDController
from ..auxillary.iterator_len import IteratorLen
from .interval_data import IntervalData
from .frame_data.frame_data import FrameData
from .frame_data.raw_frame_data import RawFrameData

class ImageController(SequentialPDController):
    '''
    This is a SequentialPDController that deals with generating loading sequences of images.
    '''

    #this is how we store the images inside the interval/frame/ directory structure
    IMAGE_BASENAME = 'image.png'

    #this is what the input image files look like
    INPUT_IMAGE_REGEX = re.compile('image_([0-9]+)\.png')

    def __init__(self, num_processors):
        super(ImageController, self).__init__(num_processors, multiprocessing_mode=self.FRAME_MP)
        
    @classmethod
    def _get_image_filename(cls, frame_dir):
        '''
        frame_dir - the name of a directory containing an image
        
        return - the filename of the image
        '''
        image_filename = os.path.join(frame_dir, cls.IMAGE_BASENAME)
        return image_filename
    
    @classmethod
    def load_data_from_frame_dir(cls, frame_dir):
        '''
        frame_dir - a directory that contains an image
        return - a numpy array with shape (n,m,3) and type np.uint8
        '''
        #in practice we only store symlinks to images. So after loading the image file name, resolve the link
        image_filename = cls._get_image_filename(frame_dir)
        #print '   loading image from: %s' % frame_dir

        image_filename = os.readlink(image_filename)
        
        return cv2.imread(image_filename, 1)

    def _save_data_in_frame_dir(self, frame_dir, input_image_filename):
        '''
        frame_dir - a frame directory name
        input_image_filename - the filename of an image

        create a symlink to the given image in the frame directory
        '''
        output_image_filename = self._get_image_filename(frame_dir)
        #print '      creating symlink %s <- %s' % (input_image_filename, output_image_filename)
        os.symlink(input_image_filename, output_image_filename)

        return

    # def build_data(self, image_filename):
    #     '''
    #     image_filename - the name of a file storing an image
    #     return - a numpy array with shape (n,m,3) and type np.uint8. It is an array that represents an image.
    #     '''
    #     image = cv2.imread(image_filename, 1)
    #     return image

    def generate_data(self, output_parent_dir, image_directory):
        '''
        output_parent_directory - the directory in which to save the images
        image_directory - a directory containing a sequence of image files: image_0.png, image_1.png, ...

        We save the image in output_parent_directory in a single interval.
        '''
        #this is a list of tuples [(0, image_0 filename), (1, image_1 filename),...]
        indexed_image_filenames = filename_loader.load_filenames(image_directory, self.INPUT_IMAGE_REGEX)
        # print 'indexed filenames:'
        # print '\n'.join(['%d: %s' % (frame,filename) for frame,filename in indexed_image_filenames])
        
        #save the images in a single interval that contains all the images
        interval = (0, len(indexed_image_filenames) - 1)

        print 'saving the images to the interval: (%d,%d)' % interval
        
        #create an IteratorLen that stores a single IntervalData object.
        #the IntervalData object stores the filename of the image in each of the frames
        frame_datas = [RawFrameData(frame, filename) for frame,filename in indexed_image_filenames]
        interval_data = IntervalData(interval, frame_datas)
        interval_data_iter = IteratorLen(1, iter([interval_data]))

        # s = pickle.dumps(interval_data)
        # print s
        # print 'the interval data pickle has %d characters' % len(s)

        #save the interval data in the output directory
        self._save_interval_data_iter(output_parent_dir, interval_data_iter)

        return

        
