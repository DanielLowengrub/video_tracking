import numpy as np
from ..highlighted_shapes_controller.contour_hough_controller import ContourHoughController
from ..preprocessing_data_type import PreprocessingDataType
import os

#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts'
#IMAGE_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/test_data/'

#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'

#INPUT_DATA_NAME = 'images-8_15-9_31'
#INPUT_DATA_NAME = 'images-8_15-9_31-contour_hough'
#INPUT_DATA_NAME = 'images-0_00-1_00'
#INPUT_DATA_NAME = 'images-0_00-10_00'
#INPUT_DATA_NAME = os.path.join('video_2', 'images-12_45-12_50')
#INPUT_DATA_NAME = os.path.join('video_2', 'images-12_28-12_38')
#INPUT_DATA_NAME = os.path.join('Barcelona-Real_Madrid-11-29-2010-rPEd-h8DdRI', 'images-0_00-10_00')

VIDEO_NAME = 'Germany-USA-06_10_2015-FrxUfp0B3pA'
#FRAME_IMAGES_NAME = 'images-9_00-10_00'
#FRAME_IMAGES_NAME = 'images-0_00-10_00'
FRAME_IMAGES_NAME = 'images-10_00-20_00'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_pass_0', 'data',
                                       VIDEO_NAME, FRAME_IMAGES_NAME)

IMAGE_DIRECTORY              = os.path.join(PREPROCESSING_DIRECTORY, 'stage_1', 'image')
ANNOTATION_DIRECTORY         = os.path.join(PREPROCESSING_DIRECTORY, 'stage_1', 'field_mask')
HIGHLIGHTED_SHAPES_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'stage_1', 'highlighted_shapes')

PD_DIRECTORY_DICT = {PreprocessingDataType.IMAGE:      IMAGE_DIRECTORY,
                     PreprocessingDataType.ANNOTATION: ANNOTATION_DIRECTORY}

NUM_PROC = 8

CANNY_MIN = 65
CANNY_MAX = 140
LINE_CLOSING_RADIUS = 5
ELLIPSE_CLOSING_RADIUS = 5

NUM_THETA_BINS  = 800
NUM_RADIUS_BINS = 300
MASK_SHAPE = (352, 624)
PEAK_THRESHOLD = 60
PEAK_THRESHOLD_RATIO = 0.75
CLUSTERING_RADIUS = 10

MAX_THETA_STD = 1.0*np.pi/180.0
MAX_RADIUS_STD = 3.0
MIN_LONG_LINE_LENGTH = 200

MIN_POINTS_IN_ELLIPSE = 100
RANSAC_MIN_POINTS_TO_FIT = 20
RANSAC_NUM_ITERATIONS = 50
RANSAC_DISTANCE_TO_SHAPE_THRESHOLD = 3
RANSAC_MIN_PERCENTAGE_IN_SHAPE = 0.9

MAX_AVERAGE_ELLIPSE_RESIDUE = 10
THICKNESS = 3

MIN_INTERVAL_ON_LINE_LENGTH = 50
MIN_INTERVAL_ON_ELLIPSE_LENGTH = 5

MIN_TOTAL_LINE_LENGTH = 50 #40
MIN_ELLIPSE_AXIS = 15
MIN_TOTAL_ELLIPSE_LENGTH = 270

def generate_data(output_parent_directory, pd_directory_dict):
    '''
    output_parent_directory - the directory in which we will save the annotations
    pd_directory_dict - a dictionary with items (PreprocessingDataType attr, directory name
    '''
    if not os.path.isdir(output_parent_directory):
        print 'making directory: ', output_parent_directory
        os.mkdir(output_parent_directory)

    hs_controller = ContourHoughController(NUM_PROC,
                                           CANNY_MIN, CANNY_MAX,
                                           LINE_CLOSING_RADIUS, ELLIPSE_CLOSING_RADIUS,
                                           NUM_THETA_BINS, NUM_RADIUS_BINS, MASK_SHAPE, PEAK_THRESHOLD,
                                           PEAK_THRESHOLD_RATIO, CLUSTERING_RADIUS,
                                           MAX_THETA_STD, MAX_RADIUS_STD, MIN_LONG_LINE_LENGTH,
                                           MIN_POINTS_IN_ELLIPSE,
                                           RANSAC_MIN_POINTS_TO_FIT, RANSAC_NUM_ITERATIONS,
                                           RANSAC_DISTANCE_TO_SHAPE_THRESHOLD,
                                           RANSAC_MIN_PERCENTAGE_IN_SHAPE, MAX_AVERAGE_ELLIPSE_RESIDUE,
                                           THICKNESS,
                                           MIN_INTERVAL_ON_LINE_LENGTH, MIN_INTERVAL_ON_ELLIPSE_LENGTH,
                                           MIN_TOTAL_LINE_LENGTH, MIN_ELLIPSE_AXIS, MIN_TOTAL_ELLIPSE_LENGTH)

    hs_controller.generate_data(output_parent_directory, pd_directory_dict)

    return

if __name__ == '__main__':
    generate_data(HIGHLIGHTED_SHAPES_DIRECTORY, PD_DIRECTORY_DICT)
