import os
import cv2
import numpy as np
import functools
import itertools
from multiprocessing import Pool
from ..preprocessing_data_type import PreprocessingDataType
from ..preprocessing_data_loader import PreprocessingDataLoader

#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts'
#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'

NN_PARENT_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'nn_training_data', 'image_annotation', 'dataset_0')
NN_IMAGE_DIRECTORY  = os.path.join(NN_PARENT_DIRECTORY, 'image')
NN_TRUE_DIRECTORY = os.path.join(NN_PARENT_DIRECTORY, 'true')

#INPUT_DATA_NAME = 'images-9_20-9_31'
#INPUT_DATA_NAME = 'images-8_15-9_31'
#INPUT_DATA_NAME = 'images-0_00-1_00'
INPUT_DATA_NAME = 'images-0_00-10_00'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_pass_0', 'data', INPUT_DATA_NAME)
VISUALIZATION_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_pass_0', 'visualization',
                                       INPUT_DATA_NAME)

IMAGE_DIRECTORY           = os.path.join(PREPROCESSING_DIRECTORY, 'stage_3', 'image')
TRAINING_DATA_DIRECTORY   = os.path.join(PREPROCESSING_DIRECTORY, 'stage_3', 'annotation_training_data')

PD_DIRECTORY_DICT = {PreprocessingDataType.ANNOTATION_TRAINING_DATA: TRAINING_DATA_DIRECTORY,
                     PreprocessingDataType.IMAGE:  IMAGE_DIRECTORY}

NUM_PROC = 8

def copy_interval_data(input_data_name, nn_image_dir, nn_true_dir, interval_data):
    print 'copying interval (%d, %d)' % interval_data.interval
    
    interval_str = 'interval_%d_%d' % interval_data.interval
    
    for frame_data in interval_data:
        frame_str = 'frame_%d' % frame_data.frame
        
        #load the image and the training data
        pd_tuple = frame_data.build_data()

        basename_prefix = '='.join([input_data_name, interval_str, frame_str])
        image_filename         = os.path.join(nn_image_dir, basename_prefix + '.png')
        true_filename = os.path.join(nn_true_dir, basename_prefix + '.npy')

        cv2.imwrite(image_filename, pd_tuple.image)
        np.save(true_filename, pd_tuple.annotation_training_data)

    return
            
def copy_training_data(input_data_name, nn_image_dir, nn_true_dir, pd_directory_dict):
    '''
    nn_image_dir - this is where we save the images
    nn_true_dir  - this is where we save the "true" annotations
    pd_directory_dict - a dictionary with items (PreprocessingDataType attr, directory name
    '''
    #get an iterator of input data in each of the frames
    interval_data_iter = PreprocessingDataLoader.load_data(pd_directory_dict)
    worker = functools.partial(copy_interval_data, input_data_name, nn_image_dir, nn_true_dir)
    
    p = Pool(NUM_PROC)
    #for retval in p.imap_unordered(worker, interval_data_iter):
    for retval in itertools.imap(worker, interval_data_iter):
        pass

    return

if __name__ == '__main__':
    copy_training_data(INPUT_DATA_NAME, NN_IMAGE_DIRECTORY, NN_TRUE_DIRECTORY, PD_DIRECTORY_DICT)
        
        
