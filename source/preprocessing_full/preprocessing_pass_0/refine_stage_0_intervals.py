import os
from ..preprocessing_data_controller import PreprocessingDataController

'''
This script takes the image directory in stage 0, refines it using the annotation directory in stage 1, and saves the result
in the stage 1 directory
'''
#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'

#INPUT_DATA_NAME = 'images-9_20-9_31'
#INPUT_DATA_NAME = 'images-8_15-9_31'
#INPUT_DATA_NAME = 'images-0_00-1_00'
#INPUT_DATA_NAME = 'images-0_00-10_00'
#INPUT_DATA_NAME = 'images-0_00-1_38_18'
#INPUT_DATA_NAME = os.path.join('video_2', 'images-6_35-6_38')
#INPUT_DATA_NAME = os.path.join('video_2', 'images-6_51-6_55')
#INPUT_DATA_NAME = os.path.join('video_2', 'images-7_45-7_50')
#INPUT_DATA_NAME = os.path.join('video_2', 'images-9_17-9_22')
#INPUT_DATA_NAME = os.path.join('video_2', 'images-11_54-11_59')
#INPUT_DATA_NAME = os.path.join('video_2', 'images-12_28-12_38')
#INPUT_DATA_NAME = os.path.join('video_2', 'images-12_45-12_50')
#INPUT_DATA_NAME = os.path.join('video_2_HD', 'images-9_17-9_22')
#INPUT_DATA_NAME = os.path.join('Barcelona-Real_Madrid-11-29-2010-rPEd-h8DdRI', 'images-0_00-10_00')

VIDEO_NAME = 'Germany-USA-06_10_2015-FrxUfp0B3pA'
#FRAME_IMAGES_NAME = 'images-9_00-10_00'
#FRAME_IMAGES_NAME = 'images-0_00-10_00'
FRAME_IMAGES_NAME = 'images-10_00-20_00'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_pass_0', 'data',
                                       VIDEO_NAME, FRAME_IMAGES_NAME)

INPUT_IMAGE_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'stage_0', 'image')
ANNOTATION_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'stage_1', 'field_mask')
OUTPUT_IMAGE_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'stage_1', 'image')

NUM_PROC = 8
#NUM_PROC = 2

def refine_intervals(output_parent_directory, input_parent_directory, refined_parent_directory):
    '''
    output_image_directory - the directory where we should store the refined image data
    input_image_directory  - the directory containing the image data
    annotation_directory   - the directory containing the refined annotation data
    '''
    if not os.path.isdir(output_parent_directory):
        print 'making directory: ', output_parent_directory
        os.mkdir(output_parent_directory)

    pd_controller = PreprocessingDataController(NUM_PROC)
    pd_controller.refine_intervals_from_directory(output_parent_directory, input_parent_directory, refined_parent_directory)

if __name__ == '__main__':
    refine_intervals(OUTPUT_IMAGE_DIRECTORY, INPUT_IMAGE_DIRECTORY, ANNOTATION_DIRECTORY)
