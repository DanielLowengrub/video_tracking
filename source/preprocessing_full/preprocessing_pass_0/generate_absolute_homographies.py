import os
import cv2
import numpy as np
from ..absolute_homography_controller.linear_interpolator_controller import LinearInterpolatorController
from ..preprocessing_data_type import PreprocessingDataType

#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts'
#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'

#INPUT_DATA_NAME = 'images-9_20-9_31'
#INPUT_DATA_NAME = 'images-8_15-9_31'
#INPUT_DATA_NAME = 'images-8_15-9_31-contour_hough'
#INPUT_DATA_NAME = 'images-0_00-1_00'
#INPUT_DATA_NAME = 'images-0_00-10_00'
#INPUT_DATA_NAME = 'images-0_00-1_38_18'
#INPUT_DATA_NAME = os.path.join('Barcelona-Real_Madrid-11-29-2010-rPEd-h8DdRI', 'images-0_00-10_00')

VIDEO_NAME = 'Germany-USA-06_10_2015-FrxUfp0B3pA'
#FRAME_IMAGES_NAME = 'images-9_00-10_00'
#FRAME_IMAGES_NAME = 'images-0_00-10_00'
FRAME_IMAGES_NAME = 'images-10_00-20_00'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_pass_0', 'data',
                                       VIDEO_NAME, FRAME_IMAGES_NAME)

RELATIVE_HOMOGRAPHY_DIRECTORY  = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'relative_homography')
OPTIMIZED_HOMOGRAPHY_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'optimized_homography')

ABSOLUTE_HOMOGRAPHY_DIRECTORY   = os.path.join(PREPROCESSING_DIRECTORY, 'stage_3', 'absolute_homography')

NUM_PROC = 8
MIN_FRAME_DISTANCE = 10
MAX_INTERPOLATION_DISTANCE = 20

def generate_data(output_parent_directory, relative_homography_directory, absolute_homography_directory):
    '''
    output_parent_directory - the directory in which we will save the annotations
    relative_homography_directory - a directory storing relative homographies
    absolute_homography_directory - a directory storing samples of absolute homographies
    '''
    if not os.path.isdir(output_parent_directory):
        print 'making directory: ', output_parent_directory
        os.mkdir(output_parent_directory)

    absolute_homography_controller = LinearInterpolatorController(NUM_PROC, MIN_FRAME_DISTANCE,
                                                                  MAX_INTERPOLATION_DISTANCE)
    
    absolute_homography_controller.generate_data(output_parent_directory,
                                                 relative_homography_directory, absolute_homography_directory)

    return

if __name__ == '__main__':
    generate_data(ABSOLUTE_HOMOGRAPHY_DIRECTORY, RELATIVE_HOMOGRAPHY_DIRECTORY, OPTIMIZED_HOMOGRAPHY_DIRECTORY)
        
        
