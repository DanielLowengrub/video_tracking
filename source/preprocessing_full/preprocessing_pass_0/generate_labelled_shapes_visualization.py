import numpy as np
import os
from ..preprocessing_data_type import PreprocessingDataType
from ..labelled_shapes_controller.labelled_shapes_visualization_controller import LabelledShapesVisualizationController

# OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'
# IMAGE_PARENT_DIRECTORY = '/home/daniel/soccer/images'
# INPUT_DATA_NAME = 'images-8_15-9_31'

#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'

#INPUT_DATA_NAME = 'images-9_20-9_31'
#INPUT_DATA_NAME = 'images-8_15-9_31'
#INPUT_DATA_NAME = 'images-0_00-1_00'
#INPUT_DATA_NAME = 'images-0_00-10_00'

VIDEO_NAME = 'Germany-USA-06_10_2015-FrxUfp0B3pA'
FRAME_IMAGES_NAME = 'images-9_00-10_00'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_pass_0', 'data',
                                       VIDEO_NAME, FRAME_IMAGES_NAME, 'stage_3')

VISUALIZATION_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_pass_0', 'visualization',
                                       VIDEO_NAME, FRAME_IMAGES_NAME, 'stage_3')

IMAGE_DIRECTORY =              os.path.join(PREPROCESSING_DIRECTORY, 'image')
HIGHLIGHTED_SHAPES_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'highlighted_shapes')
LABELLED_SHAPES_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'labelled_shapes')

PD_DIRECTORY_DICT = {PreprocessingDataType.IMAGE:              IMAGE_DIRECTORY,
                     PreprocessingDataType.HIGHLIGHTED_SHAPES: HIGHLIGHTED_SHAPES_DIRECTORY,
                     PreprocessingDataType.LABELLED_SHAPES: LABELLED_SHAPES_DIRECTORY}

VISUALIZATION_OUTPUT_DIRECTORY = os.path.join(VISUALIZATION_DIRECTORY, 'labelled_shapes')

NUM_PROC = 8

SHAPE_COLOR = (0,255,0)

def generate_visualizations(visualization_output_directory, pd_directory_dict):
    '''
    visualization_output_directory - a directory name. this is where we store the visualization
    pd_directory_dict - a dictionary with items (PreprocessingDataType attribute, directory name)

    Draw the visualization of the highlighted shapes in the preprocessing data contained in the pd directories
    '''

    if not os.path.isdir(visualization_output_directory):
        print 'making directory: ', visualization_output_directory
        os.mkdir(visualization_output_directory)

    visualization_controller = LabelledShapesVisualizationController(NUM_PROC, SHAPE_COLOR)
    visualization_controller.generate_visualizations(visualization_output_directory, pd_directory_dict)
    return

if __name__ == '__main__':
    generate_visualizations(VISUALIZATION_OUTPUT_DIRECTORY, PD_DIRECTORY_DICT)
