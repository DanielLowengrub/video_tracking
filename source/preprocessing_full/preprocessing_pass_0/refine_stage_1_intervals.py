import os
from ..preprocessing_data_controller import PreprocessingDataController

'''
This script takes the image and annotation directories in stage 1, refines it using the 
relative homography directory in  stage 2, and saves the result in the stage 2 directory
'''
#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'

#INPUT_DATA_NAME = 'images-9_20-9_31'
#INPUT_DATA_NAME = 'images-8_15-9_31'
#INPUT_DATA_NAME = 'images-8_15-9_31-contour_hough'
#INPUT_DATA_NAME = 'images-0_00-1_00'
#INPUT_DATA_NAME = 'images-0_00-10_00'
#INPUT_DATA_NAME = 'images-0_00-1_38_18'
#INPUT_DATA_NAME = os.path.join('Barcelona-Real_Madrid-11-29-2010-rPEd-h8DdRI', 'images-0_00-10_00')

VIDEO_NAME = 'Germany-USA-06_10_2015-FrxUfp0B3pA'
#FRAME_IMAGES_NAME = 'images-9_00-10_00'
#FRAME_IMAGES_NAME = 'images-0_00-10_00'
FRAME_IMAGES_NAME = 'images-10_00-20_00'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_pass_0', 'data',
                                       VIDEO_NAME, FRAME_IMAGES_NAME)

INPUT_IMAGE_DIRECTORY =       os.path.join(PREPROCESSING_DIRECTORY, 'stage_1', 'image')
INPUT_ANNOTATION_DIRECTORY =  os.path.join(PREPROCESSING_DIRECTORY, 'stage_1', 'annotation')
INPUT_HS_DIRECTORY =  os.path.join(PREPROCESSING_DIRECTORY, 'stage_1', 'highlighted_shapes')
INPUT_DIRECTORIES = (INPUT_IMAGE_DIRECTORY, INPUT_ANNOTATION_DIRECTORY, INPUT_HS_DIRECTORY)

REFINED_DIRECTORY =           os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'relative_homography')
OUTPUT_IMAGE_DIRECTORY =      os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'image')
OUTPUT_ANNOTATION_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'annotation')
OUTPUT_HS_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'highlighted_shapes')
OUTPUT_DIRECTORIES = (OUTPUT_IMAGE_DIRECTORY, OUTPUT_ANNOTATION_DIRECTORY, OUTPUT_HS_DIRECTORY)

NUM_PROC = 8

def refine_intervals(output_parent_directories, input_parent_directories, refined_parent_directory):
    '''
    output_image_directories - a tuple of directories where we should store the refined image data
    input_image_directories  - a tuple of directories containing the image data
    refined_parent_directory   - the directory containing the refined data
    '''
    for output_parent_directory, input_parent_directory in zip(output_parent_directories, input_parent_directories):
        if not os.path.isdir(output_parent_directory):
            print 'making directory: ', output_parent_directory
            os.mkdir(output_parent_directory)

        pd_controller = PreprocessingDataController(NUM_PROC)
        pd_controller.refine_intervals_from_directory(output_parent_directory, input_parent_directory,
                                                      refined_parent_directory)

if __name__ == '__main__':
    refine_intervals(OUTPUT_DIRECTORIES, INPUT_DIRECTORIES, REFINED_DIRECTORY)
