import numpy as np
import cv2
import os
from ..preprocessing_data_type import PreprocessingDataType
from ..players_controller.players_visualization_controller import PlayersVisualizationController

#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'

#INPUT_DATA_NAME = 'images-9_20-9_31'
INPUT_DATA_NAME = 'images-8_15-9_31'
#INPUT_DATA_NAME = 'images-0_00-1_00'
#INPUT_DATA_NAME = 'images-0_00-10_00'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_pass_0', 'data',
                                       INPUT_DATA_NAME, 'stage_3')
VISUALIZATION_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_pass_0', 'visualization',
                                       INPUT_DATA_NAME, 'stage_3')

IMAGE_DIRECTORY   = os.path.join(PREPROCESSING_DIRECTORY, 'image')
CAMERA_DIRECTORY  = os.path.join(PREPROCESSING_DIRECTORY, 'camera')
PLAYERS_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'players')

PD_DIRECTORY_DICT = {PreprocessingDataType.IMAGE:   IMAGE_DIRECTORY,
                     PreprocessingDataType.CAMERA:  CAMERA_DIRECTORY,
                     PreprocessingDataType.PLAYERS: PLAYERS_DIRECTORY}

VISUALIZATION_OUTPUT_DIRECTORY = os.path.join(VISUALIZATION_DIRECTORY, 'players')

NUM_PROC = 8

CONTOUR_COLOR = (255,0,0)
ELLIPSE_COLOR = (0,255,0)
ELLIPSE_THICKNESS = 1

def generate_visualizations(visualization_output_directory, pd_directory_dict):
    '''
    visualization_output_directory - a directory name. this is where we store the visualization
    pd_directory_dict - a dictionary with items (PreprocessingDataType attribute, directory name)

    Draw the visualization of the highlighted shapes in the preprocessing data contained in the pd directories
    '''

    if not os.path.isdir(visualization_output_directory):
        print 'making directory: ', visualization_output_directory
        os.mkdir(visualization_output_directory)

    visualization_controller = PlayersVisualizationController(NUM_PROC, CONTOUR_COLOR, ELLIPSE_COLOR, ELLIPSE_THICKNESS)
    visualization_controller.generate_visualizations(visualization_output_directory, pd_directory_dict)
    return

if __name__ == '__main__':
    generate_visualizations(VISUALIZATION_OUTPUT_DIRECTORY, PD_DIRECTORY_DICT)
