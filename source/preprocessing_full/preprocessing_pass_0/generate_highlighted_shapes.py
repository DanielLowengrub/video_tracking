import numpy as np
import os
from ..preprocessing_data_type import PreprocessingDataType
from ..highlighted_shapes_controller.contour_highlighted_shapes_controller import ContourHighlightedShapesController


#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'

#INPUT_DATA_NAME = 'images-9_20-9_31'
#INPUT_DATA_NAME = 'images-8_15-9_31'
#INPUT_DATA_NAME = 'images-0_00-10_00'
#INPUT_DATA_NAME = 'images-0_00-1_38_18'
#INPUT_DATA_NAME = os.path.join('Barcelona-Real_Madrid-11-29-2010-rPEd-h8DdRI', 'images-0_00-10_00')

VIDEO_NAME = 'Germany-USA-06_10_2015-FrxUfp0B3pA'
#FRAME_IMAGES_NAME = 'images-9_00-10_00'
#FRAME_IMAGES_NAME = 'images-0_00-10_00'
FRAME_IMAGES_NAME = 'images-10_00-20_00'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_pass_0', 'data',
                                       VIDEO_NAME, FRAME_IMAGES_NAME)

IMAGE_DIRECTORY              = os.path.join(PREPROCESSING_DIRECTORY, 'stage_1', 'image')
ANNOTATION_DIRECTORY         = os.path.join(PREPROCESSING_DIRECTORY, 'stage_1', 'field_mask')
HIGHLIGHTED_SHAPES_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'stage_1', 'highlighted_shapes')

PD_DIRECTORY_DICT = {PreprocessingDataType.IMAGE:      IMAGE_DIRECTORY,
                     PreprocessingDataType.ANNOTATION: ANNOTATION_DIRECTORY}

NUM_PROC = 8
#NUM_PROC = 2

MIN_TANGENT_TO_LINE_ARC_LENGTH = 20
MIN_TANGENT_TO_ELLIPSE_ARC_LENGTH = 50
MAX_TANGENT_TO_LINE_RESIDUE = 1.5
MAX_TANGENT_TO_ELLIPSE_RESIDUE = 10
MIN_TANGENT_TO_ELLIPSE_AREA = 500

THETA_BIN_WIDTH = 2
RADIUS_BIN_WIDTH = 5
MAX_THETA_DEVIATION = 0.7
MAX_RADIUS_DEVIATION = 5
MIN_LINE_ARC_LENGTH = 300
MAX_AVERAGE_LINE_RESIDUE = 10

RANSAC_MIN_POINTS_TO_FIT = 20
RANSAC_NUM_ITERATIONS = 50
RANSAC_DISTANCE_TO_SHAPE_THRESHOLD = 3
RANSAC_MIN_PERCENTAGE_IN_SHAPE = 0.9

MAX_AVERAGE_ELLIPSE_RESIDUE = 10
THICKNESS = 3

MIN_INTERVAL_ON_LINE_LENGTH = 50
MIN_INTERVAL_ON_ELLIPSE_LENGTH = 5

MIN_ELLIPSE_AXIS = 15
MIN_TOTAL_ELLIPSE_LENGTH = 270

def generate_data(output_parent_directory, pd_directory_dict):
    '''
    output_parent_directory - the directory in which we will save the annotations
    pd_directory_dict - a dictionary with items (PreprocessingDataType attr, directory name)
    '''
    if not os.path.isdir(output_parent_directory):
        print 'making directory: ', output_parent_directory
        os.mkdir(output_parent_directory)

    hs_controller = ContourHighlightedShapesController(NUM_PROC,
                                                       MIN_TANGENT_TO_LINE_ARC_LENGTH, MAX_TANGENT_TO_LINE_RESIDUE,
                                                       THETA_BIN_WIDTH, RADIUS_BIN_WIDTH,
                                                       MAX_THETA_DEVIATION, MAX_RADIUS_DEVIATION,
                                                       MIN_LINE_ARC_LENGTH, MAX_AVERAGE_LINE_RESIDUE,
                                                       MIN_TANGENT_TO_ELLIPSE_ARC_LENGTH,
                                                       MAX_TANGENT_TO_ELLIPSE_RESIDUE, MIN_TANGENT_TO_ELLIPSE_AREA,
                                                       RANSAC_MIN_POINTS_TO_FIT, RANSAC_NUM_ITERATIONS,
                                                       RANSAC_DISTANCE_TO_SHAPE_THRESHOLD,
                                                       RANSAC_MIN_PERCENTAGE_IN_SHAPE,
                                                       MAX_AVERAGE_ELLIPSE_RESIDUE, THICKNESS,
                                                       MIN_INTERVAL_ON_LINE_LENGTH, MIN_INTERVAL_ON_ELLIPSE_LENGTH,
                                                       MIN_ELLIPSE_AXIS, MIN_TOTAL_ELLIPSE_LENGTH)
    
    hs_controller.generate_data(output_parent_directory, pd_directory_dict)

    return

if __name__ == '__main__':
    generate_data(HIGHLIGHTED_SHAPES_DIRECTORY, PD_DIRECTORY_DICT)
    
        
