from ..image_controller import ImageController
import os
import numpy as np
import cv2

# OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts'
# IMAGE_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/test_data/'

# OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
# IMAGE_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/images'

OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'
IMAGE_PARENT_DIRECTORY = '/home/daniel/soccer/images'

#INPUT_DATA_NAME = 'images-9_20-9_31'
#INPUT_DATA_NAME = 'images-8_15-9_31'
#INPUT_DATA_NAME = 'images-0_00-1_00'
#INPUT_DATA_NAME = 'images-0_00-10_00'
#INPUT_DATA_NAME = 'images-0_00-1_38_18'

# VIDEO_NAME = 'video_2'
# FRAME_IMAGES_NAME = 'images-6_35-6_38'
#INPUT_DATA_NAME = os.path.join('video_2', 'images-6_51-6_55')
#INPUT_DATA_NAME = os.path.join('video_2', 'images-7_45-7_50')
#INPUT_DATA_NAME = os.path.join('video_2', 'images-9_17-9_22')
#INPUT_DATA_NAME = os.path.join('video_2', 'images-11_54-11_59')
#INPUT_DATA_NAME = os.path.join('video_2', 'images-12_28-12_38')
#INPUT_DATA_NAME = os.path.join('video_2', 'images-12_45-12_50')
#INPUT_DATA_NAME = os.path.join('video_2_HD', 'images-9_17-9_22')
#INPUT_DATA_NAME = os.path.join('Barcelona-Real_Madrid-11-29-2010-rPEd-h8DdRI', 'images-0_00-10_00')

VIDEO_NAME = 'Germany-USA-06_10_2015-FrxUfp0B3pA'
# FRAME_IMAGES_NAME = 'images-9_00-10_00'
# FRAME_IMAGES_NAME = 'images-0_00-10_00'
FRAME_IMAGES_NAME = 'images-10_00-20_00'


PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_pass_0', 'data',
                                       VIDEO_NAME, FRAME_IMAGES_NAME)
IMAGE_DIRECTORY = os.path.join(IMAGE_PARENT_DIRECTORY, VIDEO_NAME, FRAME_IMAGES_NAME)

OUTPUT_PARENT_DIR = os.path.join(PREPROCESSING_DIRECTORY, 'stage_0', 'image')

NUM_PROC = 8

def generate_data(output_parent_dir, image_directory):
    '''
    output_parent_dir - the directory where we will store the generated data
    image_directory - a directory containing files: image_0.png, image_1.png, ...
    '''
    if not os.path.isdir(output_parent_dir):
        print 'making directory: ', output_parent_dir
        os.mkdir(output_parent_dir)

    image_controller = ImageController(NUM_PROC)
    image_controller.generate_data(output_parent_dir, image_directory)

    return

if __name__ == '__main__':
    generate_data(OUTPUT_PARENT_DIR, IMAGE_DIRECTORY)

