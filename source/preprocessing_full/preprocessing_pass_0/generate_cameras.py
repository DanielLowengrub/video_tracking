import os
import cv2
import numpy as np
from ..camera_controller.camera_parameter_interpolator_controller import CameraParameterInterpolatorController
from ..preprocessing_data_type import PreprocessingDataType

#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'

#INPUT_DATA_NAME = 'images-9_20-9_31'
#INPUT_DATA_NAME = 'images-8_15-9_31'
#INPUT_DATA_NAME = 'images-0_00-1_00'
#INPUT_DATA_NAME = 'images-0_00-10_00'
#INPUT_DATA_NAME = os.path.join('Barcelona-Real_Madrid-11-29-2010-rPEd-h8DdRI', 'images-0_00-10_00')

VIDEO_NAME = 'Germany-USA-06_10_2015-FrxUfp0B3pA'
#FRAME_IMAGES_NAME = 'images-9_00-10_00'
#FRAME_IMAGES_NAME = 'images-0_00-10_00'
FRAME_IMAGES_NAME = 'images-10_00-20_00'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_pass_0', 'data',
                                       VIDEO_NAME, FRAME_IMAGES_NAME)

ABSOLUTE_HOMOGRAPHY_DIRECTORY   = os.path.join(PREPROCESSING_DIRECTORY, 'stage_3', 'absolute_homography')
CAMERA_SAMPLE_DIRECTORY         = os.path.join(PREPROCESSING_DIRECTORY, 'stage_3', 'camera_sample')
CAMERA_DIRECTORY   = os.path.join(PREPROCESSING_DIRECTORY, 'stage_3', 'camera')

NUM_PROC = 8

def generate_data(output_parent_directory, absolute_homography_directory, camera_sample_directory):
    '''
    output_parent_directory - the directory in which we will save the annotations
    absolute_homography_directory - a directory storing absolute homographies in all frames
    camera_sample_directory - a directory storing CameraMatrix objects in some of the frames of each interval
    '''
    if not os.path.isdir(output_parent_directory):
        print 'making directory: ', output_parent_directory
        os.mkdir(output_parent_directory)

    camera_controller = CameraParameterInterpolatorController(NUM_PROC)
    camera_controller.generate_data(output_parent_directory,
                                    absolute_homography_directory, camera_sample_directory)

    return

if __name__ == '__main__':
    generate_data(CAMERA_DIRECTORY, ABSOLUTE_HOMOGRAPHY_DIRECTORY, CAMERA_SAMPLE_DIRECTORY)
        
        
