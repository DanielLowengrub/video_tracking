from ..annotation_controller.tracking_subsequence_controller import TrackingSubsequenceController
import os
import numpy as np
import cv2

#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'

#INPUT_DATA_NAME = 'images-9_20-9_31'
#INPUT_DATA_NAME = 'images-8_15-9_31'
#INPUT_DATA_NAME = 'images-0_00-1_00'
#INPUT_DATA_NAME = 'images-0_00-10_00'
#INPUT_DATA_NAME = 'images-0_00-1_38_18'
#INPUT_DATA_NAME = os.path.join('video_2', 'images-6_35-6_38')
#INPUT_DATA_NAME = os.path.join('video_2', 'images-6_51-6_55')
#INPUT_DATA_NAME = os.path.join('video_2', 'images-7_45-7_50')
#INPUT_DATA_NAME = os.path.join('video_2', 'images-9_17-9_22')
#INPUT_DATA_NAME = os.path.join('video_2', 'images-11_54-11_59')
#INPUT_DATA_NAME = os.path.join('video_2', 'images-12_28-12_38')
#INPUT_DATA_NAME = os.path.join('video_2', 'images-12_45-12_50')
#INPUT_DATA_NAME = os.path.join('video_2_HD', 'images-9_17-9_22')
#INPUT_DATA_NAME = os.path.join('Barcelona-Real_Madrid-11-29-2010-rPEd-h8DdRI', 'images-0_00-10_00')

VIDEO_NAME = 'Germany-USA-06_10_2015-FrxUfp0B3pA'
#FRAME_IMAGES_NAME = 'images-9_00-10_00'
#FRAME_IMAGES_NAME = 'images-0_00-10_00'
FRAME_IMAGES_NAME = 'images-10_00-20_00'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_pass_0', 'data',
                                       VIDEO_NAME, FRAME_IMAGES_NAME)

ANNOTATION_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'stage_0', 'field_mask')
OUTPUT_PARENT_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'stage_1', 'field_mask')

NUM_PROC = 8
#NUM_PROC = 2

GRASS_THRESHOLD = 0.4
MIN_SUBSEQUENCE_LENGTH = 50 # 3*25

def generate_data(output_parent_directory, annotation_directory):
    '''
    output_parent_directory - the directory in which we will save the new annotations
    annotation_directory - the directory containing the old annotations (i.e, even ones that can not be used for tracking
    '''
    if not os.path.isdir(output_parent_directory):
        print 'making directory: ', output_parent_directory
        os.mkdir(output_parent_directory)

    ts_controller = TrackingSubsequenceController(NUM_PROC, GRASS_THRESHOLD, MIN_SUBSEQUENCE_LENGTH)
    ts_controller.generate_data(output_parent_directory, annotation_directory)

    return

if __name__ == '__main__':
    generate_data(OUTPUT_PARENT_DIRECTORY, ANNOTATION_DIRECTORY)
