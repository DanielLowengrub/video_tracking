from ..relative_homography_controller.feature_homography_controller import FeatureHomographyController
from ..preprocessing_data_type import PreprocessingDataType
import os

#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'

# INPUT_DATA_NAME = 'images-9_20-9_31'
# INPUT_DATA_NAME = 'images-8_15-9_31'
# INPUT_DATA_NAME = 'images-8_15-9_31-contour_hough'
# INPUT_DATA_NAME = 'images-0_00-1_00'
# INPUT_DATA_NAME = 'images-0_00-10_00'
# INPUT_DATA_NAME = 'images-0_00-1_38_18'
#INPUT_DATA_NAME = os.path.join('Barcelona-Real_Madrid-11-29-2010-rPEd-h8DdRI', 'images-0_00-10_00')

VIDEO_NAME = 'Germany-USA-06_10_2015-FrxUfp0B3pA'
#FRAME_IMAGES_NAME = 'images-9_00-10_00'
#FRAME_IMAGES_NAME = 'images-0_00-10_00'
FRAME_IMAGES_NAME = 'images-10_00-20_00'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_pass_0', 'data',
                                       VIDEO_NAME, FRAME_IMAGES_NAME)

IMAGE_DIRECTORY      = os.path.join(PREPROCESSING_DIRECTORY, 'stage_1', 'image')
ANNOTATION_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'stage_1', 'annotation')

PD_DIRECTORY_DICT = {PreprocessingDataType.IMAGE: IMAGE_DIRECTORY,
                     PreprocessingDataType.ANNOTATION: ANNOTATION_DIRECTORY}

OUTPUT_PARENT_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'relative_homography')

NUM_PROC = 8
#NUM_PROC = 2
MIN_SUBSEQUENCE_LENGTH = 3*25

def generate_data(output_parent_directory, pd_directory_dict):
    '''
    output_parent_directory - the directory in which we will save the new annotations
    pd_directory_dict - a dictionary with items (PreprocessingDataType attr, directory name)
    '''
    if not os.path.isdir(output_parent_directory):
        print 'making directory: ', output_parent_directory
        os.mkdir(output_parent_directory)

    rh_controller = FeatureHomographyController(NUM_PROC, MIN_SUBSEQUENCE_LENGTH)
    rh_controller.generate_data(output_parent_directory, pd_directory_dict)

    return

if __name__ == '__main__':
    generate_data(OUTPUT_PARENT_DIRECTORY, PD_DIRECTORY_DICT)
