import numpy as np
import cv2
from ..annotation_controller.stupid_field_mask_controller import StupidFieldMaskController
from ..preprocessing_data_type import PreprocessingDataType
import os

#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts'

# OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
# HUD_MASK_DIRECTORY = '/Users/daniel/Documents/soccer/hud_masks'

OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'
HUD_MASK_DIRECTORY = '/home/daniel/soccer/hud_masks'

#INPUT_DATA_NAME = 'images-9_20-9_31'
#INPUT_DATA_NAME = 'images-8_15-9_31'
#INPUT_DATA_NAME = 'images-0_00-1_00'
#INPUT_DATA_NAME = 'images-0_00-10_00'
#INPUT_DATA_NAME = 'images-0_00-1_38_18'

#VIDEO_NAME = 'video_2'
#FRAME_IMAGES_NAME = 'images-6_35-6_38'
#INPUT_DATA_NAME = os.path.join('video_2', 'images-6_51-6_55')
#INPUT_DATA_NAME = os.path.join('video_2', 'images-7_45-7_50')
#INPUT_DATA_NAME = os.path.join('video_2', 'images-9_17-9_22')
#INPUT_DATA_NAME = os.path.join('video_2', 'images-11_54-11_59')
#INPUT_DATA_NAME = os.path.join('video_2', 'images-12_28-12_38')
#FRAME_IMAGES_NAME = 'images-12_45-12_50'
#INPUT_DATA_NAME = os.path.join('video_2_HD', 'images-9_17-9_22')
#INPUT_DATA_NAME = os.path.join('Barcelona-Real_Madrid-11-29-2010-rPEd-h8DdRI', 'images-0_00-10_00')

VIDEO_NAME = 'Germany-USA-06_10_2015-FrxUfp0B3pA'
#FRAME_IMAGES_NAME = 'images-9_00-10_00'
#FRAME_IMAGES_NAME = 'images-0_00-10_00'
FRAME_IMAGES_NAME = 'images-10_00-20_00'

HUD_MASK_FILENAME = os.path.join(HUD_MASK_DIRECTORY, VIDEO_NAME + '.png')
PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_pass_0', 'data',
                                       VIDEO_NAME, FRAME_IMAGES_NAME)

IMAGE_DIRECTORY              = os.path.join(PREPROCESSING_DIRECTORY, 'stage_0', 'image')
FIELD_MASK_DIRECTORY         = os.path.join(PREPROCESSING_DIRECTORY, 'stage_0', 'field_mask')

PD_DIRECTORY_DICT = {PreprocessingDataType.IMAGE: IMAGE_DIRECTORY}

NUM_PROC = 8

# #This is a mask containing the portions of the screen dedicated to heads up display
# HUD_MASK = np.zeros((352,624), np.bool)
# #video Barcelona-Real_Madrid-11-29-2010-rPEd-h8DdRI
# HUD_MASK[20:39, 113:216] = True
# HUD_MASK[20:40, 489:580] = True

#The range of hue values that are considered grass
HUE_MIN = 36
HUE_MAX = 54

def generate_data(output_parent_directory, hud_mask_filename, pd_directory_dict):
    '''
    output_parent_directory - the directory in which we will save the annotations
    pd_directory_dict - a dictionary with items (PreprocessingDataType attr, directory name
    '''
    if not os.path.isdir(output_parent_directory):
        print 'making directory: ', output_parent_directory
        os.mkdir(output_parent_directory)

    print 'loading HUD mask from: %s' % hud_mask_filename
    hud_mask = cv2.imread(hud_mask_filename, 0) > 0
    
    fm_controller = StupidFieldMaskController(NUM_PROC, hud_mask, HUE_MIN, HUE_MAX)
    fm_controller.generate_data(output_parent_directory, pd_directory_dict)

    return

if __name__ == '__main__':
    generate_data(FIELD_MASK_DIRECTORY, HUD_MASK_FILENAME, PD_DIRECTORY_DICT)
