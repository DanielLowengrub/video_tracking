import os
import cv2
import numpy as np
from ..annotation_training_data_controller.simple_training_data_controller import SimpleTrainingDataController
from ..preprocessing_data_type import PreprocessingDataType
from ...auxillary.soccer_field_geometry import SoccerFieldGeometry

#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts'
#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'

#INPUT_DATA_NAME = 'images-9_20-9_31'
#INPUT_DATA_NAME = 'images-8_15-9_31'
#INPUT_DATA_NAME = 'images-0_00-1_00'
#INPUT_DATA_NAME = 'images-0_00-10_00'
#INPUT_DATA_NAME = os.path.join('Barcelona-Real_Madrid-11-29-2010-rPEd-h8DdRI', 'images-0_00-10_00')

VIDEO_NAME = 'Germany-USA-06_10_2015-FrxUfp0B3pA'
FRAME_IMAGES_NAME = 'images-9_00-10_00'
#FRAME_IMAGES_NAME = 'images-0_00-10_00'
#FRAME_IMAGES_NAME = 'images-10_00-20_00'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_pass_0', 'data',
                                       VIDEO_NAME, FRAME_IMAGES_NAME)

ANNOTATION_DIRECTORY           = os.path.join(PREPROCESSING_DIRECTORY, 'stage_3', 'annotation')
HIGHLIGHTED_SHAPES_DIRECTORY   = os.path.join(PREPROCESSING_DIRECTORY, 'stage_3', 'highlighted_shapes')
ABSOLUTE_HOMOGRAPHY_DIRECTORY  = os.path.join(PREPROCESSING_DIRECTORY, 'stage_3', 'absolute_homography')
LABELLED_SHAPES_DIRECTORY      = os.path.join(PREPROCESSING_DIRECTORY, 'stage_3', 'labelled_shapes')
PLAYERS_DIRECTORY              = os.path.join(PREPROCESSING_DIRECTORY, 'stage_3', 'players')
TRAINING_DATA_DIRECTORY        = os.path.join(PREPROCESSING_DIRECTORY, 'stage_3', 'annotation_training_data')

PD_DIRECTORY_DICT = {PreprocessingDataType.ANNOTATION: ANNOTATION_DIRECTORY,
                     PreprocessingDataType.HIGHLIGHTED_SHAPES:  HIGHLIGHTED_SHAPES_DIRECTORY,
                     PreprocessingDataType.ABSOLUTE_HOMOGRAPHY: ABSOLUTE_HOMOGRAPHY_DIRECTORY,
                     PreprocessingDataType.LABELLED_SHAPES:  LABELLED_SHAPES_DIRECTORY,
                     PreprocessingDataType.PLAYERS:  PLAYERS_DIRECTORY}

NUM_PROC = 8

#the absolute image that we compute the homography to
ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
SCALE = 6
ABSOLUTE_IMAGE = cv2.imread(ABSOLUTE_FILE_NAME, 1)

MATCHED_LINE_THICKNESS = 2
MATCHED_ELLIPSE_THICKNESS = 3
UNMATCHED_THICKNESS = 25
PREDICTED_SHAPE_DILATION = 10

def build_soccer_field():
    field_width  = ABSOLUTE_IMAGE.shape[1] - 1
    field_length = ABSOLUTE_IMAGE.shape[0] - 1
    return SoccerFieldGeometry(SCALE, field_width, field_length)

def generate_data(output_parent_directory,  pd_directory_dict):
    '''
    output_parent_directory - the directory in which we will save the annotations
    pd_directory_dict - a dictionary with items (PreprocessingDataType attr, directory name
    '''
    if not os.path.isdir(output_parent_directory):
        print 'making directory: ', output_parent_directory
        os.mkdir(output_parent_directory)

    soccer_field = build_soccer_field()
    td_controller = SimpleTrainingDataController(NUM_PROC, soccer_field, MATCHED_LINE_THICKNESS,
                                                 MATCHED_ELLIPSE_THICKNESS, UNMATCHED_THICKNESS, PREDICTED_SHAPE_DILATION)
    
    td_controller.generate_data(output_parent_directory, pd_directory_dict)

    return

if __name__ == '__main__':
    generate_data(TRAINING_DATA_DIRECTORY, PD_DIRECTORY_DICT)
        
        
