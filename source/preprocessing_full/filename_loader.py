import os

def load_filenames(directory, basename_regex, sort_group=0):
    '''
    directory - the name of a directory
    basename_regex - a regex that matches a tuple of integers
    sort_group - the index of the matching group that will be used to sort the filenames

    Return a list of tuples (index, full path to file) 
    index is a tuple of integers index=(i0,..,ik) that was matched to the file basename by the basename regex.
       if k=0, then index is just the integer i0
    The list is sorted by index[sort_item]
    files in the directory that could not be matched to the regex are omitted.

    Example: 
    suppose the directory contains files: interval_0_10, interval_12_15, interval_20_22, foo_bar
    the regex is: interval_([0-9]+)_([0-9]+)
    and sort_group = 0

    The output would be: [((0,10), directory/interval_0_10), ((12,15),interval_12_15), ((20,22), interval_20_22)]
    '''

    #we store tuples (index, filename)
    indexed_filenames = []
    num_groups = basename_regex.groups
    
    for basename in os.listdir(directory):
        #check if the filename has the correct form
        m = basename_regex.match(basename)
        
        if m is None or (not len(m.groups()) > 0):
            continue
        
        file_index = tuple(map(int,m.groups())) if num_groups > 1 else int(m.groups()[0])
        filename = os.path.join(directory, basename)
        indexed_filenames.append((file_index, filename))

    #now sort the file names by the index
    group_key = (lambda x: x[0][sort_group]) if num_groups > 1 else (lambda x: x[0])
    indexed_filenames.sort(key=group_key)

    return indexed_filenames
