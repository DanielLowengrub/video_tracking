import numpy as np
from .highlighted_shapes_controller.hough_highlighted_shapes_controller import HoughHighlightedShapesController
from .preprocessing_data_type import PreprocessingDataType
import os

#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts'
#IMAGE_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/test_data/'

# OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
# IMAGE_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/images'

OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'
# IMAGE_PARENT_DIRECTORY = '/home/daniel/soccer/images'

#INPUT_DATA_NAME = 'images-8_15-9_31'
#INPUT_DATA_NAME = 'images-0_00-1_00'
INPUT_DATA_NAME = 'images-0_00-10_00'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_full', 'data', INPUT_DATA_NAME)

IMAGE_DIRECTORY              = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'image')
ANNOTATION_DIRECTORY         = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'annotation')
HIGHLIGHTED_SHAPES_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'highlighted_shapes')

PD_DIRECTORY_DICT = {PreprocessingDataType.IMAGE:      IMAGE_DIRECTORY,
                     PreprocessingDataType.ANNOTATION: ANNOTATION_DIRECTORY}

NUM_PROC = 8

NUM_THETA_BINS  = 800
NUM_RADIUS_BINS = 300
MASK_SHAPE = (352, 624)
PEAK_THRESHOLD = 60
PEAK_THRESHOLD_RATIO = 0.75
CLUSTERING_RADIUS = 10

MAX_THETA_STD = 1.0*np.pi/180.0
MAX_RADIUS_STD = 3.0
MIN_LONG_LINE_LENGTH = 200

MIN_POINTS_IN_ELLIPSE = 100
RANSAC_MIN_POINTS_TO_FIT = 20
RANSAC_NUM_ITERATIONS = 50
RANSAC_DISTANCE_TO_SHAPE_THRESHOLD = 3
RANSAC_MIN_PERCENTAGE_IN_SHAPE = 0.9

MAX_AVERAGE_ELLIPSE_RESIDUE = 10
THICKNESS = 3

MIN_INTERVAL_ON_LINE_LENGTH = 50
MIN_INTERVAL_ON_ELLIPSE_LENGTH = 5

MIN_TOTAL_LINE_LENGTH = 50 #40
MIN_ELLIPSE_AXIS = 15
MIN_TOTAL_ELLIPSE_LENGTH = 270

def generate_data(output_parent_directory, pd_directory_dict):
    '''
    output_parent_directory - the directory in which we will save the annotations
    pd_directory_dict - a dictionary with items (PreprocessingDataType attr, directory name
    '''
    if not os.path.isdir(output_parent_directory):
        print 'making directory: ', output_parent_directory
        os.mkdir(output_parent_directory)

    hs_controller = HoughHighlightedShapesController(NUM_PROC,
                                                     NUM_THETA_BINS, NUM_RADIUS_BINS, MASK_SHAPE, PEAK_THRESHOLD,
                                                     PEAK_THRESHOLD_RATIO, CLUSTERING_RADIUS,
                                                     MAX_THETA_STD, MAX_RADIUS_STD, MIN_LONG_LINE_LENGTH,
                                                     MIN_POINTS_IN_ELLIPSE,
                                                     RANSAC_MIN_POINTS_TO_FIT, RANSAC_NUM_ITERATIONS,
                                                     RANSAC_DISTANCE_TO_SHAPE_THRESHOLD,
                                                     RANSAC_MIN_PERCENTAGE_IN_SHAPE, MAX_AVERAGE_ELLIPSE_RESIDUE,
                                                     THICKNESS,
                                                     MIN_INTERVAL_ON_LINE_LENGTH, MIN_INTERVAL_ON_ELLIPSE_LENGTH,
                                                     MIN_TOTAL_LINE_LENGTH, MIN_ELLIPSE_AXIS, MIN_TOTAL_ELLIPSE_LENGTH)

    hs_controller.generate_data(output_parent_directory, pd_directory_dict)

    return

if __name__ == '__main__':
    generate_data(HIGHLIGHTED_SHAPES_DIRECTORY, PD_DIRECTORY_DICT)
