import itertools
from ..auxillary import iterator_operations
from .preprocessing_frame_data import PreprocessingFrameData
from .preprocessing_data_controller import PreprocessingDataController

class PreprocessingDataLoader(object):
    '''
    This class is in charge of loading PreprocessingFrameData from a dictionary of directories.

    Specifically, it is given a dictionary with items (preprocessing data type, directory name). It loads the different
    data type and packages them into an iterator of PreprocessingFrameData objects - one for each frame.
    '''

    @classmethod
    def _strip_intervals(cls, indexed_data_iterators):
        '''
        indexed_data_iterators - a list of iterators [L0,...,LN]. Ll is an iterator of tuples of the form
                                   ((i0,j0), I0), ((i1,j1), I1), ...
                                   All of the Lls must share the same sequence of intervals: (i0,j0),(i1,j1),...
 
        return a tuple: intervals, [I0, I1, ...]
        intervals is an iterator of the form (i0,j0),(i1,j1),...
        '''
        #strip the intervals off of the first iterator L0 and save it
        intervals, data_iterator = iterator_operations.iunzip(indexed_data_iterators[0])

        #strip the intervals off of the rest of the data iterators
        #to do this, unzip each of the data iterators and keep only the second part
        data_iterators = [data_iterator] + map(lambda x: iterator_operations.iunzip(x)[1], indexed_data_iterators[1:])

        return intervals, data_iterators

    @classmethod
    def load_data(cls, directory_dictionary):
        '''
        directory_dictionary - a dictionary with items (data_type_key, data_directory)
                               data_type_key is a PreprocessingDataType attribute.
                               data_directory is a directory name

        return a tuple (intervals,  data_iterator)
        intervals is an iterator of tuples (i0,j0),(i1,j1),.... ik and jk are integers, and represent the start and end of 
            an interval.
        data_iterator is an iterator of iterators (I0,I1,...).
           Ik is an iterator of PreprocessingFrameData objects: (x0,x1,...,x{jk-ik}) which represents the data contained
           in each frame of the k-th interval: (ik,jk).
        '''
        print 'loading data from:'
        print directory_dictionary
        
        data_types = directory_dictionary.keys()
        data_controllers = map(get_data_controller, data_types)
        
        #each data iterator is of the form:
        #((i0,j0), L0), ((i1,j1),L1),...
        indexed_data_iterators = [data_controller.load_data(directory_dictionary[data_type])
                                  for data_controller,data_type in zip(data_controllers, data_types)]
        

        #after stripping the intervals, each of the data iterators is now of the form
        #(L0,L1,...) where Ik is an iterator over the frames in the k-th interval
        #intervals is an iterator of the form: (i0,j0),(i1,j1),...
        intervals, data_iterators = cls._strip_intervals(indexed_data_iterators)

        #combine the data iterators into an iterator of the form: (I0, I1, ...) where Ik is an iterator of
        #dictionaries: (d0,...,d{jk-ik}). dl stores the segmented data types in the l-th frame of the k-th interval.
        #Suppose there are three data types a,b,c. in the following code,
        #objects_in_interval is a tuple of iterators: (a0,a1,...), (b0,b1,...),(c0,c1,...)
        #objects_in_frame is a tuple (ai,bi,ci)
        data_iterator = ((PreprocessingFrameData(dict(zip(data_types, objects_in_frame)))
                          for objects_in_frame in itertools.izip(*objects_in_interval))
                         for objects_in_interval in itertools.izip(*data_iterators))

        return intervals, data_iterator
