import os
import cv2
import numpy as np
from .optimized_homography_controller.shape_distance_minimization_controller import ShapeDistanceMinimizationController
from ..auxillary.soccer_field_geometry import SoccerFieldGeometry
from .preprocessing_data_type import PreprocessingDataType

#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts'
#IMAGE_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/test_data/'

# OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
# IMAGE_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/images'

OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'
# IMAGE_PARENT_DIRECTORY = '/home/daniel/soccer/images'

#INPUT_DATA_NAME = 'images-8_15-9_31'
#INPUT_DATA_NAME = 'images-0_00-1_00'
INPUT_DATA_NAME = 'images-0_00-10_00'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_full', 'data', INPUT_DATA_NAME)

ANNOTATION_DIRECTORY           = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'annotation')
HIGHLIGHTED_SHAPES_DIRECTORY   = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'highlighted_shapes')
EMBEDDING_DIRECTORY            = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'soccer_field_embedding')
OPTIMIZED_HOMOGRAPHY_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'stage_2', 'optimized_homography')

PD_DIRECTORY_DICT = {PreprocessingDataType.HIGHLIGHTED_SHAPES:     HIGHLIGHTED_SHAPES_DIRECTORY,
                     PreprocessingDataType.SOCCER_FIELD_EMBEDDING: EMBEDDING_DIRECTORY}

NUM_PROC = 8
IMAGE_SHAPE = (352, 624)
LEARNING_RATE = 0.005
MAX_EPOCHS = 10000
LOCAL_MINIMUM_THRESHOLD = 10**(-12)
NUM_SHAPES_BANDWIDTH = 40

def load_absolute_soccer_field():
    '''
    Return a tuple (absolute shapes, absolute grid points)
    '''
    ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)
    SCALE = 6
    FIELD_WIDTH = absolute_image.shape[1]-1
    FIELD_LENGTH = absolute_image.shape[0]-1
    GRID_WIDTH = 20
    GRID_LENGTH = 14
    soccer_field = SoccerFieldGeometry(SCALE, FIELD_WIDTH, FIELD_LENGTH, GRID_WIDTH, GRID_LENGTH)
    
    return soccer_field

def generate_data(output_parent_directory, pd_directory_dict):
    '''
    output_parent_directory - the directory in which we will save the annotations
    pd_directory_dict - a dictionary with items (PreprocessingDataType attr, directory name
    '''
    if not os.path.isdir(output_parent_directory):
        print 'making directory: ', output_parent_directory
        os.mkdir(output_parent_directory)

    soccer_field = load_absolute_soccer_field()
    optimized_homography_controller = ShapeDistanceMinimizationController(NUM_PROC, soccer_field, IMAGE_SHAPE,
                                                                          NUM_SHAPES_BANDWIDTH,
                                                                          LEARNING_RATE, MAX_EPOCHS, LOCAL_MINIMUM_THRESHOLD)
        
    optimized_homography_controller.generate_data(output_parent_directory, pd_directory_dict)

    return

if __name__ == '__main__':
    generate_data(OPTIMIZED_HOMOGRAPHY_DIRECTORY, PD_DIRECTORY_DICT)
        
        
