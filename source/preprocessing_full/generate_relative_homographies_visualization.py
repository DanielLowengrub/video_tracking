import numpy as np
import os
from .preprocessing_data_type import PreprocessingDataType
from .relative_homography_controller.relative_homography_visualization_controller import RelativeHomographyVisualizationController

# OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'
# IMAGE_PARENT_DIRECTORY = '/home/daniel/soccer/images'
# INPUT_DATA_NAME = 'images-8_15-9_31'

OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
INPUT_DATA_NAME = 'images-0_00-1_00'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_full', 'data', INPUT_DATA_NAME, 'stage_2')
VISUALIZATION_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_full', 'visualization', INPUT_DATA_NAME,
                                       'stage_2')

IMAGE_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'image')
ANNOTATION_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'annotation')
RELATIVE_HOMOGRAPHY_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'relative_homography')

VISUALIZATION_OUTPUT_DIRECTORY = os.path.join(VISUALIZATION_DIRECTORY, 'relative_homography')

NUM_PROC = 2

CONTOUR_COLOR = (255,0,0)
CONTOUR_THICKNESS = 2

def generate_visualizations(visualization_parent_directory, image_directory, annotation_directory,
                            relative_homography_directory):
    '''
    visualization_parent_directory - a directory name. this is where we store the visualization
    image_directory - the directory containing the images
    annotation_directory - directory containing the annotations
    relative_homography_directory - directory containing the relative homographies

    Draw visualizations of the relative homographies and save them in the visualization directory
    '''

    if not os.path.isdir(visualization_parent_directory):
        print 'making directory: ', visualization_parent_directory
        os.mkdir(visualization_parent_directory)

    visualization_controller = RelativeHomographyVisualizationController(NUM_PROC, CONTOUR_COLOR, CONTOUR_THICKNESS)
    visualization_controller.generate_visualizations(visualization_parent_directory, image_directory, annotation_directory,
                                                     relative_homography_directory)
    return

if __name__ == '__main__':
    generate_visualizations(VISUALIZATION_OUTPUT_DIRECTORY, IMAGE_DIRECTORY, ANNOTATION_DIRECTORY,
                            RELATIVE_HOMOGRAPHY_DIRECTORY)
