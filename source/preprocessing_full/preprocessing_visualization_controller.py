import os
import re
import sys
import functools
from multiprocessing import Pool
from datetime import datetime
import itertools
import cv2
import traceback

class PreprocessingVisualizationController(object):
    '''
    A PreprocessingDataController is in charge of creating visualizations of preprocessing data that was generated with a
    PreprocessingDataController

    All of the visualizations are saved in the format
    interval_i0_j0/
       frame_l0.png
       frame_l1.png
       ...

    interval_i1_j1/
       frame_l0.png
       frame_l1.png
       ...

    ...

    This base class provides methods:
    * generate_visualizations: subclasses can use this to generate visualizations of a specific type of preprocessing data

    In order for "generate_visualizations" to work, the subclass must implement:
    _build_data_visualization: this returns an image which is a visualization of the given data
    '''
    
    INTERVAL_BASENAME = 'interval_%d_%d'
    FRAME_BASENAME = 'frame_%d.png'
    FRAME_REGEX = re.compile('frame_([0-9]+)')

    def __init__(self, num_processors):
        '''
        num_proccessors - the number of available processors.

        While generating data, we typically send out the intervals to difference threads to processed concurrently.
        Knowing how many processors there are lets us know how many intervals to send to each processor.
        '''

        self._num_processors = num_processors
        
    @classmethod
    def __get_frame_filename(cls, interval_dir, frame):
        '''
        interval_dir - the directory of an interval
        frame - an integer l representing the index of a frame in the interval

        return the filename of the file in which the visualization should be saved
        '''
        frame_filename = os.path.join(interval_dir, cls.FRAME_BASENAME % frame)
        return frame_filename

    @classmethod
    def __get_interval_directory_name(cls, base_dir, interval):
        '''
        base_dir - a directory
        interval - an IntervalData objec
        
        return the name of the directory in which the visualizations of the interval should be saved
        '''
        interval_dir = os.path.join(base_dir, cls.INTERVAL_BASENAME % interval)
        return interval_dir
    
    @classmethod
    def __make_interval_directory(cls, base_dir, interval):
        '''
        base_dir - a directory_name
        interval - a tuple (ik,jk)
        
        make the directory in which the interval should be saved
        '''
        interval_dir = cls.__get_interval_directory_name(base_dir, interval)
        if os.path.isdir(interval_dir):
            return interval_dir

        os.mkdir(interval_dir)
        return interval_dir    

    def __generate_frame_data_visualization(self, interval_dir, frame_data):
        '''
        interval_dir - a directory name. this is where we store the visualization of the frame data
        frame_data - an FrameData object
        
        generate a visualization of the frame data and save it
        '''
        frame_filename = self.__get_frame_filename(interval_dir, frame_data.frame)

        #if the frame already has a visualization, skip it
        if os.path.isfile(frame_filename):
            return
        
        #this is an image
        try:
            visualization = self._build_frame_data_visualization(frame_data)
        except:
            print 'could not save the data in %s' % frame_filename
            sys.stdout.flush()
            raise

        #print '      saving visualization for frame %d in file: %s' % (frame_data.frame, frame_filename)
        cv2.imwrite(frame_filename, visualization)
        return
    
    def __generate_interval_data_visualization(self, visualization_parent_dir, interval_data):
        '''
        visualization_parent_dir - a directory name. this is where we store the interval data
        interval_data - an IntervalData object

        save the visualization of the frame in the interval data in the given directory
        '''
        print 'interval data: ', interval_data
        interval_dir = self.__make_interval_directory(visualization_parent_dir, interval_data.interval)
        print '   saving visualization of interval (%d,%d) in directory: %s' % (interval_data.interval + (interval_dir,))
        
        #for the moment, we process all frames in the interval with the same processor
        pool_worker = functools.partial(self.__generate_frame_data_visualization, interval_dir)
        for retval in itertools.imap(pool_worker, interval_data):
            continue
        
        return
    
    def _build_frame_data_visualization(self, frame_data):
        '''
        frame_data - a FrameData object

        This method should be implemented by all subclasses.
        '''
        raise NotImplementedError('_build_frame_data_visualization must be implemented by the subclass')

    def _generate_interval_data_iter_visualization(self, visualization_parent_dir, interval_data_iter):
        '''
        visualization_parent_dir - a directory name. this is where we store the interval data
        interval_data_iter - an IteratorLen of IntervalData objects

        generate visualizations of the  the interval data objects and save them in the output directory
        '''
        print 'visualizing interval data iterator...'
        
        #if there is more than one interval, send them out to multiple processors
        if len(interval_data_iter) > 1:
            chunksize = max(1, len(interval_data_iter) / self._num_processors)
            print '   using chunksize: ', chunksize
            
            pool_worker = functools.partial(generate_interval_data_visualization_worker, self, visualization_parent_dir)
            p = Pool(self._num_processors)
            for retval in p.imap_unordered(pool_worker, interval_data_iter):
            #for retval in itertools.imap(pool_worker, interval_data_iter):
                continue

        else:
            print '   there is only one interval...'
            interval_data = next(interval_data_iter)
            self.__generate_interval_data_visualization(visualization_parent_dir, interval_data)
            
        return

    def generate_visualizations(self, visualization_parent_dir, *input_data):
        '''
        visualization_parent_dir - the directory where we should save the generated data
        input_data - the data we want to visualize
        '''
        raise NotImplementedError('generate_data must be implemented by subclasses')
    
def generate_interval_data_visualization_worker(pv_controller, visualization_parent_dir, interval_data):
    '''
    pv_controller - a PreprocessingVisualizationController object
    visualization_parent_dir - the directory in which we store the visualizations generated for each of the intervals
    interval_data - an IntervalData object

    generate visualizations of the frames in the interval data object
    '''
    time_str = datetime.now().isoformat()
    print '[%s] visualizing data in interval (%d,%d) ...' % ((time_str,) + interval_data.interval)

    try:
        #name mangling
        pv_controller._PreprocessingVisualizationController__generate_interval_data_visualization(visualization_parent_dir,
                                                                                                  interval_data)
    except:
        print 'could not visualize data in interval (%d,%d)' % interval_data.interval
        print traceback.print_exc()
        sys.stdout.flush()
        raise


    return
        
    
