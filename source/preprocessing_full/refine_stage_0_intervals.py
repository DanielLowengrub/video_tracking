import os
from .preprocessing_data_controller import PreprocessingDataController

'''
This script takes the image directory in stage 0, refines it using the annotation directory in stage 1, and saves the result
in the stage 1 directory
'''
#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'

INPUT_DATA_NAME = 'images-8_15-9_31'
#INPUT_DATA_NAME = 'images-0_00-1_00'
#INPUT_DATA_NAME = 'images-0_00-10_00'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_full_0', 'data', INPUT_DATA_NAME)

INPUT_IMAGE_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'stage_0', 'image')
#ANNOTATION_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'stage_1', 'annotation')
ANNOTATION_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'stage_1', 'field_mask')
OUTPUT_IMAGE_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'stage_1', 'image')

NUM_PROC = 8

def refine_intervals(output_parent_directory, input_parent_directory, refined_parent_directory):
    '''
    output_image_directory - the directory where we should store the refined image data
    input_image_directory  - the directory containing the image data
    annotation_directory   - the directory containing the refined annotation data
    '''
    if not os.path.isdir(output_parent_directory):
        print 'making directory: ', output_parent_directory
        os.mkdir(output_parent_directory)

    pd_controller = PreprocessingDataController(NUM_PROC)
    pd_controller.refine_intervals_from_directory(output_parent_directory, input_parent_directory, refined_parent_directory)

if __name__ == '__main__':
    refine_intervals(OUTPUT_IMAGE_DIRECTORY, INPUT_IMAGE_DIRECTORY, ANNOTATION_DIRECTORY)
