import itertools
import numpy as np
import cv2
from ..auxillary.rectangle import Rectangle

class Contour(object):
    '''
    This object stores an opencv type contour, and implements various often used
    contour related methods. For example:
    * create a mask corresponding to the contour interior / the interior minus the interior of the children
    * get characteristics such as the area and aspect ratio
    * get the convex hull and (axis aligned) bounding box
    '''

    EPSILON = 0.01
    
    def __init__(self, cv_contour, image):
        '''
        cv_contour - an opencv style contour
        image - a numpy array of shape (n,m) and type np.uint8
        '''

        self._cv_contour = cv_contour
        self._image = image

        #this is a list of the contours contained in this contour.
        #is is set by the user later
        self._children_contours = []

    def get_image(self):
        return self._image

    def set_image(self, image):
        self._image = image
        
    def get_cv_contour(self):
        '''
        Return the opencv version of the contour
        '''
        return self._cv_contour

    def set_child_contours(self, contours):
        '''
        contours - a list of Contour objects
        '''
        self._children_contours = contours

    def get_child_contours(self):
        '''
        return - a list of Contour objects. They are the children of this contour.
        '''
        return self._children_contours
    
    def get_mask(self):
        '''
        Return a numpy array of shape (n,m) and type np.float64
        The mask we return corresponds to the interior of the contour
        '''
        mask = np.zeros(self._image.shape[:2], np.float64)            
        cv2.drawContours(mask, [self._cv_contour], -1, 1, -1)

        return mask

    def add_mask(self, mask):
        '''
        mask - a numpy array with shape (k,l) and type np.float32
        
        add the interior of the contour to the given mask.
        '''
        cv2.drawContours(mask, [self._cv_contour], -1, 1, -1)
        return

    def get_bounding_rectangle(self):
        '''
        return a Rectangle object which represents the AABB surrounding the contour.
        '''
        x,y,w,h = cv2.boundingRect(self.get_cv_contour())
        top_left = np.array([x,y])
        shape = (h,w)

        rectangle = Rectangle.from_top_left_and_shape(top_left, shape)
        
        return rectangle
    
    def get_mask_in_rectangle(self, rectangle):
        '''
        rectangle - a Rectangle object

        Return a numpy array with the same shape as the rectangle and type bool. It represents the part of the
        retangle that is contained in the contour.
        '''
        from ..auxillary import planar_geometry

        # print 'drawing contour mask in the rectangle:'
        # print rectangle

        # print 'contour points:'
        # print self._cv_contour
        
        #first we transform the contour to the coordinate system of the rectangle
        H = rectangle.get_homography_to_rectangle_coords()
        new_cv_contour = planar_geometry.apply_homography_to_points(H, self._cv_contour.reshape((-1,2))).reshape((-1,1,2)).astype(np.int32)

        # print 'contour points in rectange coords:'
        # print new_cv_contour
        
        #draw the transformed contour on an image the size of the rectangle
        mask = np.zeros(rectangle.get_shape(), np.float32)            
        cv2.drawContours(mask, [new_cv_contour], -1, 1, -1)
        
        return mask > 0

    def get_points_in_contour(self):
        '''
        return a numpy array with shape (num points in contour, 2). and type np.int32.
        It is a list of the (x,y) values of all pixels in the contour.
        '''
        from ..auxillary import planar_geometry
        
        #for efficiency reasons, first compute the coordinates in the bounding rectangle of the contour
        rectangle = self.get_bounding_rectangle()
        mask = self.get_mask_in_rectangle(rectangle)

        #find the points in the rectangle coordinates
        points = np.vstack(np.where(mask)).transpose()
        points = points[:,(1,0)]
        
        #transform them to image coordinates
        H = rectangle.get_homography_from_rectangle_coords()
        points = planar_geometry.apply_homography_to_points(H, points).astype(np.int32)

        return points
    
    def get_outer_mask(self):
        '''
        Return a numpy array of shape (n,m) and type np.float64

        The mask we return corresponds to the part inside the contour that is not contained in any
        of the child contours.
        '''

        outer_mask = np.zeros(self._image.shape[:2], np.float64)

        #set all points in the contour to 1
        cv2.drawContours(outer_mask, [self._cv_contour], -1, 1, -1)

        #set all points in the children to 0
        cv2.drawContours(outer_mask, [contour.get_cv_contour() for contour in self._children_contours], -1, 0, -1)

        return outer_mask

    def get_area(self):
        '''
        Return the number of pixels contained in this contour.
        '''

        return self.get_mask().sum()

    def get_outer_area(self):
        '''
        Return the number of pixels contained in the contour, but not in any child contours.
        '''

        return self.get_outer_mask().sum()

    def get_area_of_convex_hull(self):
        '''
        Return the number of pixels contained in the convex hull.
        '''

        hull = cv2.convexHull(self.get_cv_contour())
        return cv2.contourArea(hull)

    def get_aspect_ratio(self):
        '''
        Return W / H where W is the width of the bounding rectangle, and H is the height.
        '''

        x,y,w,h = cv2.boundingRect(self.get_cv_contour())
        return float(w)/float(h)

    def get_centroid(self):
        '''
        return the center of mass of the contour.
        '''
        M = cv2.moments(self._cv_contour)

        # print self._cv_contour
        # print M

        if np.abs(M['m00']) < self.EPSILON:
            return None
        
        cx = int(M['m10']/M['m00'])
        cy = int(M['m01']/M['m00'])

        return np.array([cx,cy])
    
    def get_transformed_contour(self, H):
        '''
        H - a 3x3 numpy array with type np.float32
        contour - a Contour object

        return a Contour object which is obtained by applying the homography H to this contour.
        this contour is not changed.
        '''
        from ..auxillary import planar_geometry

        child_cv_contours = [c.get_cv_contour() for c in self.get_child_contours()]
        
        #transform the points in the cv contours by H
        c = self._cv_contour
        cv_contour = planar_geometry.apply_homography_to_points(H,c.reshape((-1,2))).reshape((-1,1,2)).astype(np.int32)
        child_cv_contours = [planar_geometry.apply_homography_to_points(H,c.reshape((-1,2))).reshape((-1,1,2)).astype(np.int32)
                             for c in child_cv_contours]
        
        #turn the cv contours into contours
        contour = Contour(cv_contour, self._image)
        child_contours = [Contour(c, self._image) for c in child_cv_contours]
        
        contour.set_child_contours(child_contours)
        
        return contour

    @classmethod
    def get_outer_mask_of_contours(cls, image, contours):
        '''
        image - a numpy array of shape (n,m,3) and type np.uint8
        contours - a list of contour objects on the image.

        Return a mask which has value 1 at a pixel that is contained in one of the contours, and not in any of their
        children.

        This is the same as taking the union of all of the masks contour.get_outer_mask(), where contour
        varies over all of the given contours.
        '''
        outer_mask = np.zeros(image.shape[:2], np.float32)
        
        #set all points in the contour to 1
        cv2.drawContours(outer_mask, [contour.get_cv_contour() for contour in contours], -1, 1, -1)

        #set all points in the children to 0
        child_cv_contours = [c.get_cv_contour()
                             for c in itertools.chain(*(contour.get_child_contours() for contour in contours))]

        cv2.drawContours(outer_mask, child_cv_contours, -1, 0, -1)

        return outer_mask
