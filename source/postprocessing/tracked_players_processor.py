from ..dynamics.particle_filter_fast.player_tracker_controller import PlayerTrackerController

class TrackedPlayersProcessor(object):
    '''
    This is a base class for classes that operate on TrackedPlayers objects.
    '''

    def _process_tracked_players(self, tracked_players):
        '''
        tracked_players - a TrackedPlayers object
        return - a TrackedPlayers object
        '''
        raise NotImplementedError('process tracked players has not been implemented')
    
    def process_tracked_players(self, tracked_players_directory, output_directory):
        '''
        tracked_players_directory - a directory containing the output of PlayerTrackerController.track_players
        output_directoy - the direcory where we should store the improved TrackedPlayers object.
        '''
        print 'generating team labels for the tracked players in directory:'
        print tracked_players_directory
        
        #load the iterator of TrackedPlayers
        indexed_tps = PlayerTrackerController.load_tracked_players(tracked_players_directory)

        #in each of the intervals, process the TrackedPlayers object
        indexed_tps = ((interval, self._process_tracked_players(tracked_players)) for interval,tracked_players in indexed_tps)

        #save the new tracked players to the output directoy
        PlayerTrackerController.save_tracked_players(indexed_tps, output_directory)

        return
