from ..team_label_interpolator.team_label_interpolator import TeamLabelInterpolator
from ..tracked_players_processor import TrackedPlayersProcessor

class TeamLabelsGenerator(TrackedPlayersProcessor):
    '''
    The job of this class is to take a TrackedPlayers object and create a new TrackedPlayers object in which the team labels
    have smoother transitions.

    It is used as part of the postprocessing process that cleans up the output of the player tracker.
    '''

    def __init__(self):
        self._team_label_interpolator = TeamLabelInterpolator()


    def _interpolate_labels_in_intervals(self, labels, intervals):
        '''
        labels - a numpy array with TeamLabel properties
        intervals - a list of intervals of the form (i,j) where 0 <= i < j <= len(labels)
        
        interpolate the labels in each of the intervals and update the labels array accordingly.
        '''
        for interval in intervals:
            labels_in_interval = interval.get_slice(labels)
            labels_in_interval[...] = self._team_label_interpolator.interpolate_labels(labels_in_interval)
            #print 'labels in interval after substitution:'
            #print labels_in_interval
            
        return
    
    def _process_tracked_players(self, tracked_players):
        '''
        tracked_players - a TrackedPlayers object

        return a new TrackedPlayers object whose team labels have been interpolated to make them less "jumpy".
        '''
        new_tracked_players = tracked_players.get_copy()

        #interpolate the labels of each of the players in the TrackedPlayers object
        new_team_labels = dict()
        for player_key in new_tracked_players.get_player_keys():
            labels = new_tracked_players.get_team_labels(player_key)
            print '** interpolating the labels:'
            print [x for x in labels]

            intervals = new_tracked_players.get_position_intervals(player_key)
            print '   in the intervals:'
            print intervals
            
            self._interpolate_labels_in_intervals(labels, intervals)

            print '   the interpolated labels:'
            print [x for x in new_tracked_players.get_team_labels(player_key)]
            
        return new_tracked_players
