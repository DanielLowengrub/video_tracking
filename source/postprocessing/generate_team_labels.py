from .team_labels_generator.team_labels_generator import TeamLabelsGenerator
import os

# OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts'
# IMAGE_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/test_data/'
# INPUT_DATA_NAME = 'images_50_55'

OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts'
IMAGE_PARENT_DIRECTORY =  '/Users/daniel/Dropbox/soccer/daniel_refactored/test_data'
INPUT_DATA_NAME = 'video7'

# OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
# OUTPUT_TESTING_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output_testing'
# IMAGE_PARENT_DIRECTORY =  '/Users/daniel/Documents/soccer/images'
# INPUT_DATA_NAME = 'images-8_15-9_31'

# OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'
# IMAGE_PARENT_DIRECTORY = '/home/daniel/soccer/images'
# INPUT_DATA_NAME = 'images-8_15-9_31'

POSTPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'postprocessing', 'data', INPUT_DATA_NAME)
TRACKED_PLAYERS_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'player_tracker', 'data', INPUT_DATA_NAME)
#POSTPROCESSING_DIRECTORY = os.path.join(OUTPUT_TESTING_PARENT_DIRECTORY, 'postprocessing', 'data', INPUT_DATA_NAME)
#TRACKED_PLAYERS_DIRECTORY = os.path.join(OUTPUT_TESTING_PARENT_DIRECTORY, 'player_tracker', 'data', INPUT_DATA_NAME)

TEAM_LABELS_DIRECTORY = os.path.join(POSTPROCESSING_DIRECTORY, 'team_labels')

def generate_team_labels(tracked_players, team_labels_directory):
    '''
    tracked_players_directory - a directory containing the output PlayerTrackerController.track_players
    output_directoy - the direcory where we should store the improved TrackedPlayers object.
    '''    
    if not os.path.isdir(team_labels_directory):
        print 'making directory: ', team_labels_directory
        os.mkdir(team_labels_directory)

    team_labels_generator = TeamLabelsGenerator()
    team_labels_generator.process_tracked_players(tracked_players, team_labels_directory)
    
if __name__ == '__main__':
    generate_team_labels(TRACKED_PLAYERS_DIRECTORY, TEAM_LABELS_DIRECTORY)
