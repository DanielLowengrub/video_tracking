import numpy as np
from ...dynamics.particle_filter_fast.tracked_balls import TrackedBalls
from ..tracked_balls_processor import TrackedBallsProcessor
from ...auxillary.interval import Interval

class BallTrajectoryOverlapResolver(TrackedBallsProcessor):
    '''
    The goal of this class if to remove overlapping ball trajectories. This is necessary because sometimes there are two
    possible ball trajectories in the same frame, and we must choose which one is the ball.

    Different implementations of this class can decide how to make this choice.
    '''

    def _compare_trajectories(self, ball_trajectory_0, ball_trajectory_1):
        '''
        ball_trajectory_i - a BallTrajectory object
        return -1 if ball_trajectory_0 is worse than ball_trajectory_1, and 1 otherwise
        '''

        if len(ball_trajectory_0.positions) < len(ball_trajectory_1.positions):
            return -1

        return 1
    
    def _process_tracked_balls(self, tracked_balls):
        '''
        tracked_balls - a TrackedBalls object
        return - a TrackedBalls object which is the same as the input one, but where the overlaps have been resolved
        '''

        #get the ball trajectories and sort them by the starting frame
        bt_items = tracked_balls.get_ball_trajectory_dict().items()
        bt_items.sort(key=lambda x: x[1].start_frame)
        bt_keys = [k for k,v in bt_items]

        ball_trajectory_dict = tracked_balls.get_ball_trajectory_dict().copy()
        
        #iterate over pairs of trajectories, and crop pairs that overlap
        for i, key_0 in enumerate(bt_keys):
            if key_0 not in ball_trajectory_dict:
                continue
            
            bt_0 = ball_trajectory_dict[key_0]
            interval_0 = Interval(bt_0.start_frame, bt_0.start_frame + len(bt_0.positions))

            print '* cropping ball trajectory %d' % key_0
            print '  it has interval: ', interval_0
            
            for key_1 in bt_keys[i+1:]:
                if key_1 not in ball_trajectory_dict:
                    continue

                bt_1 = ball_trajectory_dict[key_1]
                interval_1 = Interval(bt_1.start_frame, bt_1.start_frame + len(bt_1.positions))

                print '   - comparing it with ball trajectory %d' % key_1
                print '     it has interval: ', interval_1

                #if the intervals do not overlap, stop processing bt_0
                if not interval_0.has_overlap(interval_1):
                    print '      there is no overlap.'
                    break

                #if they do, remove one of the trajectories
                trajectory_cmp = self._compare_trajectories(bt_0, bt_1)

                #if bt_0 is better
                if trajectory_cmp == 1:
                    print '      trajectory %d is better' % key_0
                    del ball_trajectory_dict[key_1]

                #if bt_1 is better, delete bt_0 and stop processing it
                else:
                    print '      trajectory %d is better' % key_1
                    del ball_trajectory_dict[key_0]
                    break
                    

        tracked_balls = TrackedBalls(ball_trajectory_dict)
        return tracked_balls

