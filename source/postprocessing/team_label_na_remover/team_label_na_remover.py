import numpy as np
import itertools
from ..tracked_players_processor import TrackedPlayersProcessor
from ...preprocessing.team_labels_generator.team_label import TeamLabel
from ...auxillary.interval import IntervalList
from ...dynamics.particle_filter_fast.tracked_players import TrackedPlayers

class NASequence(object):
    '''
    This class represents a sequence of NA labels. 
    Specifically, it represents a subsequence labels[i:j+1] \subset labels such that:
       * The players is active in the interval (i,j)
       * The sequence is maximal in the sense that labels[i-1] and labels[j+1] have we defined teams, which are different.
    '''
    
    def __init__(self, player_key, interval, start_team, end_team):
        '''
        player_key - the key of the player that this sequence represents
        interval - an Interval object
        start_team - a TeamLabel property
        end_label - a TeamLabel property
        '''
        
        self._player_key = player_key
        self._interval = interval
        self._start_team = start_team
        self._end_team = end_team

        self._resolved = False

    def __repr__(self):
        return 'NASequence: [player_key=%d, interval=%s]' % (self._player_key, self._interval)
    
    @property
    def player_key(self):
        return self._player_key

    @property
    def interval(self):
        return self._interval

    @property
    def start_team(self):
        return self._start_team

    @property
    def end_team(self):
        return self._end_team

    @property
    def resolved(self):
        return self._resolved

    @resolved.setter
    def resolved(self, value):
        self._resolved = value
        
class TeamLabelNARemover(TrackedPlayersProcessor):
    '''
    The job of this class os to remove the NA entries from the team labels in a TrackedPlayer object.

    We do this in two passes:
    (a NA sequence is a subsequence of labels s=labels[i:j](NA, ..., NA) such that labels[i-1] and labels[j] are 
    non-degenerate team labels. The point of the following two steps is to fill these subsequences with non-degenerate team
    values as well.)

    1) Locate all pairs of NA sequences from two different players that have a common position (up to a threshold).
       Let (p1,p2) be such a pair of players. Suppose that the NA sequences are pi.labels[na_start_i : na_end_i]
       that they meet at time na_start_i<t<na_end_i, and that pi.labels[na_start_i-1] = bi pi.labels[na_end_i] =  ai 

       We swap the positions p1[t:] with p2[t:], and set: p1.labels[na_start_1 : t] = a1 and p1.labels[t:na_end_1] = b2.
       Similarly, p2.labels[na_start_2 : t] = a2 and p2.labels[t:na_end_2] = b1.

       This process is carried out with the intervals (na_start, na_end) ordered by na_end in decsending order.

    2) The remaining NA sequences labels[na_start : na_end] are dealt with by simply setting:
       labels[na_start : m] = labels[na_start - 1]
       labels[m : na_end] = labels[na_end]
       where m = (na_start + na_end) / 2

       This is a very crude way to fill in the values labels[na_start : na_end], but for the moment we just want to get 
       something up and running.
    '''
    def __init__(self, position_distance_threshold):
        '''
        position_distance_threshold - a positive float. this thresh is used to determine if two players cross paths.
        '''
        self._position_distance_threshold = position_distance_threshold
        
    def _find_na_sequences_in_interval(self, player_key, labels, interval):
        '''
        player_key - the key of a player
        labels - a numpy array whose values are TeamLabel properties
        interval - an Interval object
        
        return a list of NASequence objects.
        '''
        print '   finding na sequences in the interval: ', interval
        labels_in_interval = interval.get_slice(labels)
        print '   labels in interval: '
        print labels_in_interval

        #find the subsequences of labels[interval] whose values are NA or UNDETERMINED
        is_na = np.logical_or(labels_in_interval == TeamLabel.NA, labels_in_interval == TeamLabel.UNDETERMINED)

        #if all of the labels are NA, then do not return any na sequences since there is nothing to do about them.
        if np.all(is_na):
            return []

        relative_na_intervals = IntervalList.from_boolean_array(is_na).get_intervals()

        #the relative_na_intervals are intervals whose coordinates are relative to interval. we convert these to abs coords
        absolute_na_intervals = [interval.relative_to_absolute_interval(i) for i in relative_na_intervals]

        print '   absolute na intervals:'
        print absolute_na_intervals
        
        #use the intervals to build NASequence objects
        return [NASequence(player_key, x, labels[x.first_element()-1], labels[x.last_element()])
                for x in absolute_na_intervals]
        

    def _find_na_sequences(self, tracked_players):
        '''
        tracked_players - a TrackedPlayers object
        
        return a list of NASequence objects
        '''
        print 'finding na sequences...'
        na_sequences = []

        for player_key in tracked_players.get_player_keys():
            labels = tracked_players.get_team_labels(player_key)
            position_intervals = tracked_players.get_position_intervals(player_key)

            #find the na sequences in each of the intervals
            na_sequences_in_intervals = [self._find_na_sequences_in_interval(player_key, labels, interval)
                                         for interval in position_intervals]

            #concatenate them and add them to the list of (lists of) na sequences
            na_sequences.append(itertools.chain.from_iterable(na_sequences_in_intervals))

        #concatenate the iterators in na_sequences
        na_sequences = list(itertools.chain.from_iterable(na_sequences))

        print na_sequences
        
        return na_sequences

    def _find_na_sequence_intersection(self, tracked_players, na_sequence_0, na_sequence_1):
        '''
        tracked_players - a TrackedPlayers object
        na_sequence_i - an NASequence object. We assume that their intervals overlap

        return - an integer i. It represents the index in the overlap of the two sequences for which the respective players
           are closest. If the distance at this points is larger than the distance threshold, return None
        '''        
        overlap = na_sequence_0.interval.get_overlap(na_sequence_1.interval)
        
        positions_0 = overlap.get_slice(tracked_players.get_positions(na_sequence_0.player_key))
        positions_1 = overlap.get_slice(tracked_players.get_positions(na_sequence_1.player_key))

        print '      positions_0:\n', positions_0.T
        print '      positions_1:\n', positions_1.T
        
        #find the minimum distance between the positions in the overlapping region
        distances = np.linalg.norm(positions_0 - positions_1, axis=1)
        closest_index = np.argmin(distances)

        print '      closest_index: ', closest_index
        
        #if it is not within the distance thresh, then we can not untangle these sequences
        if distances[closest_index] > self._position_distance_threshold:
            return None

        return overlap.relative_to_absolute_coords(closest_index)

    def _untangle_na_sequence_pair(self, tracked_players, intersection_index, na_sequence_0, na_sequence_1):
        '''
        tracked_players - a TrackedPlayers object
        intersection_index - an integer
        na_sequence_i - an NASequence object. We assume that their intervals overlap

        swap the players at the intersection index and update the labels in the na_sequence regions accordingly.
        This is best explained by example. Suppose that the labels of the players are: (aligned by time)
         5  6  7  8   9   10 11 12
        [A, A, A, NA, NA, NA, B, B]
           [B, B, NA, NA, A,  A, A, A, A]

        Suppose that the positions of these players intersect at time i=9. After swapping the players, their labels are now:
        [A, A, A, NA, NA, A,  A, A, A, A]
           [B, B, NA, NA, NA, B, B]

        We then fill the labels of each player so that there are no more NA values
        [A, A,..., A, A, A]
           [B,..., B]
        '''
        print '      untangling the pair of na sequences at index %d: ' % intersection_index
        print '      0: ', na_sequence_0
        print '      1: ', na_sequence_1

        print '      the initial labels: '
        print '      0: ', tracked_players.get_team_labels(na_sequence_0.player_key)
        print '      1: ', tracked_players.get_team_labels(na_sequence_1.player_key)

        #first swap the players at the specified index
        TrackedPlayers.swap_players(tracked_players, na_sequence_0.player_key, na_sequence_1.player_key, intersection_index)

        #then modify the labels
        labels_0 = tracked_players.get_team_labels(na_sequence_0.player_key)
        labels_1 = tracked_players.get_team_labels(na_sequence_1.player_key)
        
        labels_0[na_sequence_0.interval.first_element() : intersection_index] = na_sequence_0.start_team
        labels_0[intersection_index : na_sequence_1.interval.last_element()] = na_sequence_1.end_team
        labels_1[na_sequence_1.interval.first_element() : intersection_index] = na_sequence_1.start_team
        labels_1[intersection_index : na_sequence_0.interval.last_element()] = na_sequence_0.end_team

        print '      the final labels: '
        print '      0: ', labels_0
        print '      1: ', labels_1

        return
    
    def _untangle_na_sequences(self, tracked_players, na_sequences):
        '''
        tracked_players - a TrackedPlayers object
        na_sequences - a list of NASequence objects
        
        Find pairs of na_sequences that have an overlapping position. For each of those, swap the positions of the players
        after the intersection points and set the labels accordingly. The "resolved" attributes of these na sequences are
        set to True.
        '''
        print 'untangleing na sequences...'
        
        #sort the na sequences from big to small according to the last index of their intervals
        na_sequences.sort(key=lambda x: x.interval.last_element(), reverse=True)

        for i, na_sequence_0 in enumerate(na_sequences):
            print '* na_sequence_0: ', na_sequence_0
            #if this interval has already been resolved, skip it
            if na_sequence_0.resolved:
                continue

            for na_sequence_1 in na_sequences[i+1:]:
                print '   - na_sequence_1: ', na_sequence_1
                
                if na_sequence_1.resolved:
                    continue
                
                #if the intervals do not overlap, stop searching for sequences to pair with na_sequence_0
                if not na_sequence_0.interval.has_overlap(na_sequence_1.interval):
                    print '      the sequences do not overlap' 
                    break

                #try to untangle the pair of na sequences.
                #in order to untangle these sequences, they must have opposite teams on each end of the sequence
                if na_sequence_0.start_team == na_sequence_1.start_team:
                    print '      the sequences have the same teams'
                    continue

                intersection_index = self._find_na_sequence_intersection(tracked_players, na_sequence_0, na_sequence_1)

                #if the sequences do not get closest to one another, try the next sequence
                if intersection_index is None:
                    print '      the sequences do not intersect'
                    continue

                #if we can untangle these sequences, do so and then stop processing na_sequence_0
                else:
                    print '      untangling the sequences...'
                    self._untangle_na_sequence_pair(tracked_players, intersection_index, na_sequence_0, na_sequence_1)
                    na_sequence_0.resolved = True
                    na_sequence_1.resolved = True
                    break
                
            #if we did not manage to resolve this sequence, then do so by force
            if not na_sequence_0.resolved:
                self._split_na_sequence(tracked_players, na_sequence_0)
            
        return

    def _split_na_sequence(self, tracked_players, na_sequence):
        '''
        tracked_players - a TrackedPlayers object
        na_sequence - an NASequence object

        set the first half of the labels in the sequence to start_team and second half to end_team.
        '''
        print 'splitting the sequence: ', na_sequence
        
        labels = tracked_players.get_team_labels(na_sequence.player_key)
        start_index, end_index = na_sequence.interval.first_element(), na_sequence.interval.last_element()
        center_index = (start_index + end_index) / 2

        print '   center index: ', center_index
        
        labels[start_index : center_index] = na_sequence.start_team
        labels[center_index : end_index] = na_sequence.end_team

        return
    
    def _process_tracked_players(self, tracked_players):
        '''
        tracked_players - a TrackedPlayers object
        
        return - a TrackedPlayers object which does not have any NA team labels.
        '''

        #find all of the sequences of NA labels
        na_sequences = self._find_na_sequences(tracked_players)

        print 'the na sequences are: '
        print '* ' + '\n\n* '.join(map(str, na_sequences))
        
        #first untangle sequences of NA labels that cross over one another
        self._untangle_na_sequences(tracked_players, na_sequences)

        #then resolve the rest of the the sequences by brute force
        # for na_sequence in na_sequences:
        #     if not na_sequence.resolved:
        #         self._split_na_sequence(tracked_players, na_sequence)

        return tracked_players
