from .team_label_consolidator.team_label_consolidator import TeamLabelConsolidator
import os

OUTPUT_PARENT_DIRECTORY =        '/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts'
IMAGE_PARENT_DIRECTORY =         '/Users/daniel/Dropbox/soccer/daniel_refactored/test_data'
INPUT_DATA_NAME = 'video7'

# OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
# OUTPUT_TESTING_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output_testing'
# IMAGE_PARENT_DIRECTORY =  '/Users/daniel/Documents/soccer/images'
# INPUT_DATA_NAME = 'images-8_15-9_31'

# OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'
# IMAGE_PARENT_DIRECTORY = '/home/daniel/soccer/images'
# INPUT_DATA_NAME = 'images-8_15-9_31'

POSTPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'postprocessing', 'data', INPUT_DATA_NAME)
TRACKED_PLAYERS_DIRECTORY = os.path.join(POSTPROCESSING_DIRECTORY, 'no_team_label_na')

CONSOLIDATED_LABEL_DIRECTORY = os.path.join(POSTPROCESSING_DIRECTORY, 'consolidated_team_labels')

DISTANCE_THRESHOLD = 10.0

def remove_team_label_na(tracked_players, consolidated_label_directory):
    '''
    tracked_players_directory - a directory containing the output PlayerTrackerController.track_players
    output_directoy - the direcory where we should store the improved TrackedPlayers object.
    '''    
    if not os.path.isdir(consolidated_label_directory):
        print 'making directory: ', consolidated_label_directory
        os.mkdir(consolidated_label_directory)

    tl_consolidator = TeamLabelConsolidator()
    tl_consolidator.process_tracked_players(tracked_players, consolidated_label_directory)
    
if __name__ == '__main__':
    remove_team_label_na(TRACKED_PLAYERS_DIRECTORY, CONSOLIDATED_LABEL_DIRECTORY)
