import numpy as np
import collections
import itertools
from ...auxillary.interval import IntervalList
from ..tracked_players_processor import TrackedPlayersProcessor
from ...preprocessing.team_labels_generator.team_label import TeamLabel

PlayerSequence = collections.namedtuple('PlayerSequence', ['player_key', 'interval'])

class TeamLabelConsolidator(TrackedPlayersProcessor):
    '''
    This class updates a TrackedPlayers object such that the team labels each of the player are either all TEAM_A or all
    TEAM_B.
    '''

    def _find_player_sequences(self, tracked_players):
        '''
        tracked_players - a TrackedPlayers object
        return a tuple (list of PlayerSequence objects with team A, list of PlayerSequence objects with team B)
        '''
        team_a_sequences = []
        team_b_sequences = []
        for player_key in tracked_players.get_player_keys():
            labels = tracked_players.get_team_labels(player_key)
            team_a_intervals = IntervalList.from_boolean_array(labels == TeamLabel.TEAM_A).get_intervals()
            team_b_intervals = IntervalList.from_boolean_array(labels == TeamLabel.TEAM_B).get_intervals()

            team_a_sequences.append([PlayerSequence(player_key, interval) for interval in team_a_intervals])
            team_b_sequences.append([PlayerSequence(player_key, interval) for interval in team_b_intervals])

        team_a_sequences = list(itertools.chain.from_iterable(team_a_sequences))
        team_b_sequences = list(itertools.chain.from_iterable(team_b_sequences))
        
        return team_a_sequences, team_b_sequences

    def _build_disjoint_groups(self, player_sequences):
        '''
        player_sequences - a list of PlayerSequences objects

        return - a list of lists of PlayerSequence objects. The interval of the player sequences in each of the lists are
           disjoint.

        For example, suppose that the intervals of the player sequences are: (0,5), (3,10), (7,15).
        Then we would output two groups of player sequences, which have the intervals:
        [(0,5), (7,15)] and [(3,10)]

        We use the following algorithm:
        * sort the player sequences by player_sequence.interval.first_element()
        * try to add each player sequence to one of the existing groups. If this is not possible, form a new group.
        '''
        print 'building disjoint groups from the player sequences:'
        print '* ' + '\n* '.join(map(str, player_sequences))
        
        player_sequence_groups = []
        player_sequences.sort(key=lambda x: x.interval.first_element())

        for ps in player_sequences:
            print '   processing sequence: ', ps
            
            #try to add the player sequence to one of the existing groups
            added_player_sequence_to_group = False
            for ps_group in player_sequence_groups:
                #check if this player sequences interval overlaps with the interval of the last player sequence in the group
                if not ps.interval.has_overlap(ps_group[-1].interval):
                    print '      adding the sequence to group: ', ps_group
                    ps_group.append(ps)
                    added_player_sequence_to_group = True
                    break

            #if this player sequence was not added to any group, make a new group for it.
            if not added_player_sequence_to_group:
                print '      building a new group'
                player_sequence_groups.append([ps])

        print 'returning groups:'
        print '\n'.join(map(str, player_sequence_groups))
        
        return player_sequence_groups
    
    def _process_tracked_players(self, tracked_players):
        '''
        tracked_players - a TrackedPlayers object
        return a TrackedPlayers object in which each of the players is labelled only with TEAM_A or TEAM_B
        '''

        #get a list of all sequences of continuous player positions for each team        
        team_a_sequences, team_b_sequences = self._find_player_sequences(tracked_players)

        print 'team a sequences:'
        print '* ' + '\n* '.join(map(str, team_a_sequences))

        print 'team b sequences:'
        print '* ' + '\n* '.join(map(str, team_b_sequences))

        #group each of the team sequences into a lists of lists of non-intersecting sequences.
        #each such list will represent a player
        team_a_groups = self._build_disjoint_groups(team_a_sequences)
        team_b_groups = self._build_disjoint_groups(team_b_sequences)

        #create a TrackedPlayers object out of the union of these lists
        player_sequence_groups = team_a_groups + team_b_groups
        player_interval_lists = [[(ps.player_key, ps.interval) for ps in ps_group]
                                 for ps_group in player_sequence_groups]

        consolidated_tracked_players = tracked_players.redistribute(player_interval_lists)
        return consolidated_tracked_players
                        
        
