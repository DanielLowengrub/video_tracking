from .ball_trajectory_cropper.ball_trajectory_cropper import BallTrajectoryCropper
import os
import cv2

OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts'
IMAGE_PARENT_DIRECTORY =  '/Users/daniel/Dropbox/soccer/daniel_refactored/test_data'
INPUT_DATA_NAME = 'video7'

# OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
# OUTPUT_TESTING_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output_testing'
# IMAGE_PARENT_DIRECTORY =  '/Users/daniel/Documents/soccer/images'
# INPUT_DATA_NAME = 'images-8_15-9_31'

# OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'
# IMAGE_PARENT_DIRECTORY = '/home/daniel/soccer/images'
# INPUT_DATA_NAME = 'images-8_15-9_31'

POSTPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'postprocessing', 'data', INPUT_DATA_NAME)
TRACKED_BALLS_DIRECTORY = os.path.join(POSTPROCESSING_DIRECTORY, 'no_penalty_spots')

CROPPED_TRAJECTORIES_DIRECTORY = os.path.join(POSTPROCESSING_DIRECTORY, 'cropped_ball_trajectories')

DISTANCE_THRESHOLD = 10.0
OVERLAP_THRESHOLD = 5

def crop_ball_trajectories(tracked_balls_directory, cropped_trajectories_directory):
    '''
    tracked_balls_directory - a directory containing the output BallTrackerController.track_balls
    cropped_trajectories_directory - the direcory where we should store the improved TrackedBalls object.
    '''    
    if not os.path.isdir(cropped_trajectories_directory):
        print 'making directory: ', cropped_trajectories_directory
        os.mkdir(cropped_trajectories_directory)

    bt_cropper = BallTrajectoryCropper(OVERLAP_THRESHOLD, DISTANCE_THRESHOLD)
    bt_cropper.process_tracked_balls(tracked_balls_directory, cropped_trajectories_directory)
    
if __name__ == '__main__':
    crop_ball_trajectories(TRACKED_BALLS_DIRECTORY, CROPPED_TRAJECTORIES_DIRECTORY)
