import os
import pickle
import cv2
import re
import numpy as np
import itertools
from ..dynamics.particle_filter_fast.ball_tracker_controller import BallTrackerController
from ..dynamics.ball_tracking.airborne_plane_trajectory.airborne_plane_trajectory_fitter import AirbornePlaneTrajectoryFitter
from ..preprocessing import file_sequence_loader
from ..preprocessing.camera_samples_interpolator.camera_samples_interpolator import CameraSamplesInterpolator
from ..auxillary import iterator_operations

class AirbornePlaneTrajectoryExtractor(object):
    '''
    This class is in charge of extracting the airborne plane trajectories from a collection of "on ground" trajectories.

    Specifically, we first track balls with the assumption that it is rolling on the ground.
    Naturally, if a sequence of images actually corresponds to a ball that is airborne, the trajectory will be recorded as
    a sequence of "on ground" trajectories. This is because the projection of an airborne trajectory to the ground does not
    move in a straight line, but rather a parabola like one.

    This class takes a collection of "on ground" trajectories, and identifies those that come from an airborne plane 
    trajectory. It removes those trajectories from the list, and saves the corresponding  AirbornePlaneTrajectory 
    object in a seperate directory.
    '''

    AIRBORNE_PLANE_TRAJECTORY_BASENAME = 'airborne_plane_trajectory_%d_%d.pckl'
    AIRBORNE_PLANE_TRAJECTORY_REGEX = re.compile('airborne_plane_trajectory_([0-9]+)_([0-9]+)\.pckl')
    
    RES = 0
    AUTOCORR = 1

    def __init__(self, fps, max_frame_gap,
                 intermediate_line_thresholds, final_line_thresholds,
                 intermediate_parabola_thresholds, final_parabola_thresholds,
                 height_acceleration_bounds):
        '''
        fps - an integer. The number of frame per second.
        max_frame_gap - the maximum number of missed observations in an airborne trajectory
        intermediate_line_thresholds - a tuple (res thresh, autocorr thresh). 
           This determines how close the horizontal component of the plane positions must be to being linear wrt time t for
           any segment of an airborne plane trajectory
        final_line_thresholds - same as intermediate_line_thresholds, but for an entire trajectory. 
           This should be a smaller threshold since we expect the fit to be better when applied to a complete trajectory
        intermediate_parabola_thresholds - a tuple (res thresh, autocorr thresh). 
           This determines how close the vertical component of the plane positions are to being  quadratic wrt time t for
           any segment of an airborne plane trajectory
        final_parabola_thresholds - same as intermediate_parabola_thresholds, but for an entire trajectory. 
           This should be a smaller threshold since we expect the fit to be better when applied to a complete trajectory
        
        height_acceleration_bounds - a tuple of floats (min_acceleration, max_acceleration). These determine the allowed
                                     acceleration of the vertical component of the plane positions.
        '''
        self._fps = fps
        self._max_frame_gap = max_frame_gap
        self._intermediate_line_thresholds = intermediate_line_thresholds
        self._final_line_thresholds = final_line_thresholds
        self._intermediate_parabola_thresholds = intermediate_parabola_thresholds
        self._final_parabola_thresholds = final_parabola_thresholds
        self._height_acceleration_bounds = height_acceleration_bounds

        self._apt_fitter = AirbornePlaneTrajectoryFitter(fps)

    @classmethod
    def _load_airborne_plane_trajectories_file(cls, airborne_plane_trajectories_file):
        '''
        airborne_plane_trajectories_file - a file containing a list of AirbornePlaneTrajectory objects
        '''
        f = open(airborne_plane_trajectories_file, 'rb')
        apts = pickle.load(f)
        f.close()

        return apts

    @classmethod
    def load_airborne_plane_trajectories(self, airborne_plane_trajectory_directory):
        '''
        airborne_plane_trajectory_directory - a directory containing files airborne_plane_trajectory_ik_jk.pckl
        return an iterator of tuples ((ik,jk), list of AirbornePlaneTrajectory objects)
        '''
                
        interval_files = file_sequence_loader.get_sorted_file_names(airborne_plane_trajectory_directory,
                                                                    cls.AIRBORNE_PLANE_TRAJECTORY_REGEX)

        for interval_file in interval_files:
            interval_basename = os.path.basename(interval_file)
            interval = tuple(map(int,cls.AIRBORNE_PLANE_TRAJECTORY_REGEX.match(interval_basename).groups()))
            print 'loading airborne plane trajectories from: ', interval_file
            
            apts = cls._load_airborne_plane_trajectories_file(interval_file)
            
            yield (interval, apts)

    def _save_trajectories(self, interval, tracked_balls, airborne_plane_trajectories,
                           tracked_balls_directory, airborne_plane_directory):
        '''
        interval - a tuple (start_frame, end_frame)
        tracked_balls - a TrackedBalls object
        airborne_plane_trajectories - a list of AirbornePlaneTrajectories objects
        tracked_balls_directory - a directory name
        airborne_plane_trajectory_directory - a directory name

        save the tracked_balls in tracked_balls_directory and save the airborne_plane_trajectories in 
        airborne_plane_trajectory_directory.
        '''
        BallTrackerController.save_tracked_balls_in_interval(interval, tracked_balls, tracked_balls_directory)

        apt_filename = os.path.join(airborne_plane_directory, self.AIRBORNE_PLANE_TRAJECTORY_BASENAME % interval)
        f = open(apt_filename, 'wb')
        pickle.dump(airborne_plane_trajectories, f)
        f.close()

        return
        
    def _is_successfull_fitting(self, airborne_plane_trajectory, errors, line_thresholds, parabola_thresholds):
        '''
        airborne_plane_trajectory - an AirbornePlaneTrajectory object
        airborne_plane_trajectory_errors - a FittingErrors object

        return True iff this trajectory should be considered to be valid
        '''
        if ((errors.line_res > line_thresholds[self.RES]) or
            (errors.line_autocorr > line_thresholds[self.AUTOCORR])):
            return False
        
        if ((errors.parabola_res > parabola_thresholds[self.RES]) or
            (errors.parabola_autocorr > parabola_thresholds[self.AUTOCORR])):
            return False

        #make sure the the acceleration of the parabola is within the bounds
        height_acceleration = airborne_plane_trajectory.get_vertical_acceleration()
        if not (self._height_acceleration_bounds[0] < height_acceleration < self._height_acceleration_bounds[1]):
            return False

        return True

    def _is_successfull_intermediate_fitting(self, airborne_plane_trajectory, errors):
        return self._is_successfull_fitting(airborne_plane_trajectory, errors, self._intermediate_line_thresholds,
                                            self._intermediate_parabola_thresholds)

    def _is_successfull_final_fitting(self, airborne_plane_trajectory, errors):
        return self._is_successfull_fitting(airborne_plane_trajectory, errors, self._final_line_thresholds,
                                            self._final_parabola_thresholds)

    def _extract_trajectories_in_interval(self, interval, cameras, tracked_balls,
                                          tracked_balls_directory, airborne_plane_directory):
        '''
        interval - a tuple (start frame, last frame)
        cameras - an iterator of CameraMatrix objects
        tracked_balls - a TrackedBalls object
        tracked_balls_directory - the directory where we should save the new TrackedBalls object
        airborne_plane_trajectory_directory - the directory where we should store the AirbornePlaneTrajectory objects
        '''
        print 'extracting airborne trajectories from interval: (%d,%d)' % interval
        cameras = list(cameras)
        airborne_plane_trajectories = []
        
        #build a list of ball trajectories, sorted by start_frame
        ball_trajectories = tracked_balls.get_sorted_ball_trajectories()

        for i,start_bt in enumerate(ball_trajectories):
            #make sure that this id is still in the dictionary. I.e, that it has not been added to an airborne trajectory
            if not tracked_balls.has_ball_trajectory(start_bt.ball_trajectory_id):
                continue
            
            start_bt_interval = (start_bt.start_frame, start_bt.start_frame + len(start_bt.positions)-1)
            print 'trying to start a trajectory with frame interval: (%d, %d)' % start_bt_interval

            airborne_plane_trajectory = None
            airborne_plane_trajectory_errors = None
            ball_trajectory_ids_in_airborne_trajectory = [start_bt.ball_trajectory_id]
            #this is a numpy array with shape (n,3). Each row is of the form (frame index, x, y)
            #these are the image positions that will be fitted to an airborne plane trajectory
            enumerated_image_positions = tracked_balls.get_enumerated_image_positions(start_bt.ball_trajectory_id)
            enumerated_image_positions[:,0] -= start_bt.start_frame
            #do not try to create an airborne trajectory with a trajectory that starts after this frame
            max_start_frame = start_bt.start_frame + len(start_bt.image_positions) - 1 + self._max_frame_gap

            # print 'enumerated image positions:'
            # print enumerated_image_positions
            
            #try to merge start_bt with the trajectories that come after it in order to create an airborne trajectory
            for next_bt in ball_trajectories[i+1:]:
                #make sure that this ball trajectory has not already been added to an airborne trajectory
                if not tracked_balls.has_ball_trajectory(next_bt.ball_trajectory_id):
                    continue

                #if this trajectory starts too far after then current airborne trajectory, then stop trying to add trajectories
                if next_bt.start_frame > max_start_frame:
                    break

                next_bt_interval = (next_bt.start_frame, next_bt.start_frame + len(next_bt.positions)-1)
                print '   trying to add a trajectory with frame interval: (%d, %d)' %  next_bt_interval
                
                next_enumerated_image_positions = tracked_balls.get_enumerated_image_positions(next_bt.ball_trajectory_id)
                next_enumerated_image_positions[:,0] -= start_bt.start_frame
                next_enumerated_image_positions = np.vstack([enumerated_image_positions, next_enumerated_image_positions])
                end_frame = start_bt.start_frame + next_enumerated_image_positions[:,0].max()
                next_cameras = cameras[start_bt.start_frame : end_frame+1]

                print '   start_frame: %d, end_frame: %d' % (start_bt.start_frame, end_frame)
                # print '   next enumerated image positions:'
                # print next_enumerated_image_positions
                
                apt, apt_errors = self._apt_fitter.fit_to_trajectory(start_bt.ball_trajectory_id, start_bt.start_frame,
                                                                     next_enumerated_image_positions, next_cameras)

                print '   fitting errors: ', apt_errors
                
                #check if next_bt can be added on to an airborne_plane_trajectory
                if self._is_successfull_intermediate_fitting(apt, apt_errors):
                    print '   the match was successfull'
                    airborne_plane_trajectory = apt
                    airborne_plane_trajectory_errors = apt_errors
                    ball_trajectory_ids_in_airborne_trajectory.append(next_bt.ball_trajectory_id)
                    enumerated_image_positions = next_enumerated_image_positions
                    max_start_frame = end_frame + self._max_frame_gap
                    #print '   airborne trajectory: ', airborne_plane_trajectory
                    print '   new max_start_frame: %d' % max_start_frame
                    print '   airborne ids: ', ball_trajectory_ids_in_airborne_trajectory
                    
            #if we managed create an airborne trajectory starting with start_bt, then remove the ball trajectories that were
            #involved from tracked_balls since we will replace them with the airborne one.
            #Also add the airborn trajectory to the list if airborne trajectories, and add a new trajectory to tracked_balls
            #of ball trajectories
            if airborne_plane_trajectory is not None and self._is_successfull_final_fitting(airborne_plane_trajectory,
                                                                                            airborne_plane_trajectory_errors):
                print '   * creating an airborne trajectory with id: ', start_bt.ball_trajectory_id
                #print '   final trajectory: ', airborne_plane_trajectory
                print '   removing ids: ', ball_trajectory_ids_in_airborne_trajectory
                tracked_balls.remove_ball_trajectories(ball_trajectory_ids_in_airborne_trajectory)
                airborne_plane_trajectories.append(airborne_plane_trajectory)
                tracked_balls.add_trajectory(start_bt.ball_trajectory_id, start_bt.start_frame,
                                             airborne_plane_trajectory.get_world_positions(),
                                             airborne_plane_trajectory.get_image_positions(), on_ground=False)

        #save the new ball trajectory dict and the airborne trajectories
        self._save_trajectories(interval, tracked_balls, airborne_plane_trajectories,
                                tracked_balls_directory, airborne_plane_directory)

        return
                
    def extract_airborne_plane_trajectories(self, camera_directory, tracked_balls_directory,
                                            output_tracked_balls_directory, output_airborne_plane_directory):
        '''
        camera_directory - a directory with subdiretories camera_ik_jk/. Each subdirectory contains files
                           camera_0.npy,...,camera_{jk-ik}.npy
        tracked_balls_directory - a directory containing the output of BallTrackerController.track_players
        output_tracked_balls_directoy - the direcory where we should store the new TrackedBalls objects.
        output_airborne_plane_directory - the directory where we should store the AirbornePlaneTrajectory objects

        Note that the TrackedBalls objects will contain BallTrajectory objects that represent the airborne trajectories as well
        as the ground trajectories. The airborne_plane_trajectory objects are used mainly for debugging.
        If a BallTrajectory is modelled by and AirbornePlaneTrajectory object, then they will both share the same 
        ball_trajectory_id.
        '''        
        #this is an iterator of tuples ((ik,jk), Ik) where Ik is an iterator of CameraMatrix objects
        indexed_cameras = CameraSamplesInterpolator.load_cameras(camera_directory)

        #this is an iterator of tuples ((ik,jk), TrackedBalls object)
        indexed_tracked_balls = BallTrackerController.load_tracked_balls(tracked_balls_directory)

        intervals, cameras = iterator_operations.iunzip(indexed_cameras)
        tracked_balls = (tbs for interval,tbs in indexed_tracked_balls)

        for interval, cameras_in_interval, tracked_balls_in_interval in itertools.izip(intervals, cameras, tracked_balls):
            print 'processing balls in interval (%d,%d):' % interval
            self._extract_trajectories_in_interval(interval, cameras_in_interval, tracked_balls_in_interval,
                                                   output_tracked_balls_directory, output_airborne_plane_directory)
