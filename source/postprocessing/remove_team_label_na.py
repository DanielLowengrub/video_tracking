from .team_label_na_remover.team_label_na_remover import TeamLabelNARemover
import os

OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts'
IMAGE_PARENT_DIRECTORY =  '/Users/daniel/Dropbox/soccer/daniel_refactored/test_data'
INPUT_DATA_NAME = 'video7'

# OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
# OUTPUT_TESTING_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output_testing'
# IMAGE_PARENT_DIRECTORY =  '/Users/daniel/Documents/soccer/images'
# INPUT_DATA_NAME = 'images-8_15-9_31'

# OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'
# IMAGE_PARENT_DIRECTORY = '/home/daniel/soccer/images'
# INPUT_DATA_NAME = 'images-8_15-9_31'

POSTPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'postprocessing', 'data', INPUT_DATA_NAME)
TRACKED_PLAYERS_DIRECTORY = os.path.join(POSTPROCESSING_DIRECTORY, 'team_labels')

NO_NA_LABEL_DIRECTORY = os.path.join(POSTPROCESSING_DIRECTORY, 'no_team_label_na')

DISTANCE_THRESHOLD = 10.0

def remove_team_label_na(tracked_players, no_na_label_directory):
    '''
    tracked_players_directory - a directory containing the output PlayerTrackerController.track_players
    output_directoy - the direcory where we should store the improved TrackedPlayers object.
    '''    
    if not os.path.isdir(no_na_label_directory):
        print 'making directory: ', no_na_label_directory
        os.mkdir(no_na_label_directory)

    na_remover = TeamLabelNARemover(DISTANCE_THRESHOLD)
    na_remover.process_tracked_players(tracked_players, no_na_label_directory)
    
if __name__ == '__main__':
    remove_team_label_na(TRACKED_PLAYERS_DIRECTORY, NO_NA_LABEL_DIRECTORY)
