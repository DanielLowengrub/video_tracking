from .airborne_plane_trajectory_extractor import AirbornePlaneTrajectoryExtractor
import os
import cv2

# OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts'
# IMAGE_PARENT_DIRECTORY =  '/Users/daniel/Dropbox/soccer/daniel_refactored/test_data'
# INPUT_DATA_NAME = 'video7'

OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
OUTPUT_TESTING_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output_testing'
IMAGE_PARENT_DIRECTORY =  '/Users/daniel/Documents/soccer/images'
INPUT_DATA_NAME = 'images-8_15-9_31'

# OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'
# IMAGE_PARENT_DIRECTORY = '/home/daniel/soccer/images'
# INPUT_DATA_NAME = 'images-8_15-9_31'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing', 'data', INPUT_DATA_NAME)
POSTPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'postprocessing', 'data', INPUT_DATA_NAME)
TRACKED_BALLS_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'ball_tracker', 'data', INPUT_DATA_NAME)

CAMERA_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'camera')

OUTPUT_AIRBORNE_PLANE_DIRECTORY = os.path.join(POSTPROCESSING_DIRECTORY, 'extracted_airborne_trajectories',
                                               'airborne_plane_trajectories')
OUTPUT_TRACKED_BALLS_DIRECTORY = os.path.join(POSTPROCESSING_DIRECTORY, 'extracted_airborne_trajectories',
                                               'tracked_balls')


FPS = 25
MAX_FRAME_GAP = 5
INTERMEDIATE_LINE_THRESHOLDS = (0.05, 0.05)
FINAL_LINE_THRESHOLDS = (0.01, 0.01)
INTERMEDIATE_PARABOLA_THRESHOLDS = (0.5, 0.5)
FINAL_PARABOLA_THRESHOLDS = (0.1, 0.1)
HEIGHT_ACCELERATION_BOUNDS = (-150, -40)

def extract_airborne_plane_trajectories(camera_directory, tracked_balls_directory,
                                        output_tracked_balls_directory, output_airborne_plane_directory):
    '''
    camera_directory - a directory containing the output of CameraSamplesInterpolator.interpolate_cameras
    tracked_balls_directory - a directory containing the output BallTrackerController.track_balls
    airborne_plane_directory - the direcory where we should store the improved TrackedBalls objects, and the 
       AirbornePlaneTrajectory objects
    '''    
    os.mkdir(output_tracked_balls_directory)
    os.mkdir(output_airborne_plane_directory)
    
    apt_extractor = AirbornePlaneTrajectoryExtractor(FPS, MAX_FRAME_GAP,
                                                     INTERMEDIATE_LINE_THRESHOLDS, FINAL_LINE_THRESHOLDS,
                                                     INTERMEDIATE_PARABOLA_THRESHOLDS, FINAL_PARABOLA_THRESHOLDS,
                                                     HEIGHT_ACCELERATION_BOUNDS)
    apt_extractor.extract_airborne_plane_trajectories(camera_directory, tracked_balls_directory,
                                                      output_tracked_balls_directory, output_airborne_plane_directory)
    
if __name__ == '__main__':
    extract_airborne_plane_trajectories(CAMERA_DIRECTORY, TRACKED_BALLS_DIRECTORY,
                                        OUTPUT_TRACKED_BALLS_DIRECTORY, OUTPUT_AIRBORNE_PLANE_DIRECTORY)

