from ..dynamics.particle_filter_fast.ball_tracker_controller import BallTrackerController

class TrackedBallsProcessor(object):
    '''
    This is a base class for classes that operate on TrackedBalls objects.
    '''

    def _process_tracked_balls(self, tracked_balls):
        '''
        tracked_balls - a TrackedBalls object
        return - a TrackedPlayers object
        '''
        raise NotImplementedError('process tracked balls has not been implemented')
    
    def process_tracked_balls(self, tracked_balls_directory, output_directory):
        '''
        tracked_balls_directory - a directory containing the output of BallTrackerController.track_players
        output_directoy - the direcory where we should store the improved TrackedPlayers object.
        '''
        print 'processing the tracked balls in directory:'
        print tracked_balls_directory
        
        #load the iterator of TrackedBalls
        indexed_tbs = BallTrackerController.load_tracked_balls(tracked_balls_directory)

        #in each of the intervals, process the TrackedBalls object
        indexed_tbs = ((interval, self._process_tracked_balls(tracked_balls)) for interval,tracked_balls in indexed_tbs)

        #save the new tracked balls to the output directoy
        BallTrackerController.save_tracked_balls(indexed_tbs, output_directory)

        return
