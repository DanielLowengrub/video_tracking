import numpy as np
from ...preprocessing.team_labels_generator.team_label import TeamLabel

class TeamLabelInterpolator(object):
    '''
    The task of a TeamLabelInterpolator is to convert team label measurements from a subset of the frames, to a list
    of team labels in all frames.

    The reason we need this is that the team label detector does not manage to detect the labels in all frames, so we have
    to deduce the rest of the labels after the fact.
    '''
                              
    def _find_first_team_label(self, labels):
        '''
        labels - a numpy array with values in (NA, UNDETERMINED, TEAM_A, TEAM_B)

        return - an integer
        
        It is the first integer i which satisfies: labels[i] in (TEAM_A, TEAM_B).
        If there is no such integer, return len(labels)
        '''
        first_index = np.argmax(np.logical_or(labels == TeamLabel.TEAM_A, labels == TeamLabel.TEAM_B))

        #if first_index=0, then it is possible that *none* of the values where TEAM_A or TEAM_B
        if (first_index == 0) and (labels[0] not in (TeamLabel.TEAM_A, TeamLabel.TEAM_B)):
            first_index = len(labels)

        return first_index

    def interpolate_labels(self, labels):
        '''
        labels - a numpy array with shape (num frames) and type np.int32.
        return - a numpy array with shape (num frames) and type np.int32.
        
        The elements of labels should be one the the team label types defined in TeamLabels.
        In this version, we fill every interval of the form TEAM_X,NA,...,NA,TEAM_X with TEAM_X.
        '''
        interpolated_labels = np.ones(labels.shape, np.int32) * TeamLabel.NA
        
        print '* interpolating the labels: '
        print [x for x in labels]

        interval_start = self._find_first_team_label(labels)
        #if initial_start is the end of the array, then there are no values to interpolate
        if interval_start == len(labels):
            return interpolated_labels

        interval_team = labels[interval_start]
        interval_end = interval_start

        # print '   interval_start: %d, interval_end: %d, interval_team: %d' % (interval_start, interval_end, interval_team)
        
        #by the definition of interval start, the initial segment of labels looks like: (NA,...,NA,interval_team)
        #we interpolate interval_team backwards to fill all previous NA values
        interpolated_labels[:interval_start] = interval_team

        current_index = interval_start + 1
        
        while current_index < len(labels):
            current_team = labels[current_index]
            # print '   current_index: %d, current_team: %d' % (current_index, current_team)
            # print '   interval_start: %d, interval_end: %d, interval_team: %d' % (interval_start, interval_end,
            #                                                                       interval_team)
            
            #if the current value is the team of the interval, then update the end of the interval to be the current position
            if current_team == interval_team:
                # print '   updating the end of the interval.'
                interval_end = current_index

            #if the current value is equal to the other team, then stop processing this interval and open a new one
            elif current_team not in (TeamLabel.NA, TeamLabel.UNDETERMINED):
                # print '   resetting interval.'
                #interpolate the interval with the interval's team
                interpolated_labels[interval_start:interval_end + 1] = interval_team

                #start a new interval
                interval_start = interval_end = current_index
                interval_team = current_team

            current_index += 1

        #interpolate the values in the final interval
        interpolated_labels[interval_start:] = interval_team

        print 'interpolated labels:'
        print [x for x in interpolated_labels]
        
        return interpolated_labels


        
