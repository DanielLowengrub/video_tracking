from .ball_trajectory_overlap_resolver.ball_trajectory_overlap_resolver import BallTrajectoryOverlapResolver
import os
import cv2

OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts'
IMAGE_PARENT_DIRECTORY =  '/Users/daniel/Dropbox/soccer/daniel_refactored/test_data'
INPUT_DATA_NAME = 'video7'

# OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
# OUTPUT_TESTING_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output_testing'
# IMAGE_PARENT_DIRECTORY =  '/Users/daniel/Documents/soccer/images'
# INPUT_DATA_NAME = 'images-8_15-9_31'

# OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'
# IMAGE_PARENT_DIRECTORY = '/home/daniel/soccer/images'
# INPUT_DATA_NAME = 'images-8_15-9_31'

POSTPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'postprocessing', 'data', INPUT_DATA_NAME)
TRACKED_BALLS_DIRECTORY = os.path.join(POSTPROCESSING_DIRECTORY, 'cropped_ball_trajectories')

RESOLVED_TRAJECTORIES_DIRECTORY = os.path.join(POSTPROCESSING_DIRECTORY, 'no_ball_trajectory_overlaps')

def resolve_ball_trajectory_overlaps(tracked_balls_directory, resolved_trajectories_directory):
    '''
    tracked_balls_directory - a directory containing the output BallTrackerController.track_balls
    resolved_trajectories_directory - the direcory where we should store the improved TrackedBalls object.
    '''    
    if not os.path.isdir(resolved_trajectories_directory):
        print 'making directory: ', resolved_trajectories_directory
        os.mkdir(resolved_trajectories_directory)

    bt_resolver = BallTrajectoryOverlapResolver()
    bt_resolver.process_tracked_balls(tracked_balls_directory, resolved_trajectories_directory)
    
if __name__ == '__main__':
    resolve_ball_trajectory_overlaps(TRACKED_BALLS_DIRECTORY, RESOLVED_TRAJECTORIES_DIRECTORY)
