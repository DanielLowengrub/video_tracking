from ..tracked_balls_processor import TrackedBallsProcessor
from ...dynamics.particle_filter_fast.tracked_balls import TrackedBalls
import numpy as np

class PenaltySpotRemover(TrackedBallsProcessor):
    '''
    This class is in charge of removing the "penalty spot" from the collection of tracked balls.
    Since the penalty spot looks very much like a ball, it gets tracked by the ball tracker so we remove it afterwards.
    '''
    def __init__(self, soccer_field_geometry, distance_threshold_in_yards):
        '''
        soccer_field_geometry - a SoccerFieldGeometry object
        distance_threshold_in_yards - the distance from a penalty point (in yards) that is considered to be near the point.
        '''

        self._penalty_spots = soccer_field_geometry.get_penalty_spots()
        self._distance_threshold = soccer_field_geometry.convert_yards_to_world_coords(distance_threshold_in_yards)

    def _is_penalty_spot(self, ball_trajectory):
        '''
        ball_trajectory - a BallTrajectory object
        
        return True iff this trajectory is actually a penalty spot.
        '''
        #get the positions on the field
        positions = ball_trajectory.positions[:,:2]
        
        for ps in self._penalty_spots:
            if np.all(np.linalg.norm(positions - ps.reshape((1,2)), axis=1) < self._distance_threshold):
                print 'ball trajectory %d is a penalty spot.' % ball_trajectory.ball_trajectory_id
                return True
            
        return False
    
    def _process_tracked_balls(self, tracked_balls):
        '''
        tracked_balls - a TrackedBalls object
        return - a TrackedBalls object which is the same as the input one, but without the "balls" that represent PenaltyDots
        '''

        new_ball_trajectory_dict = dict((k,bt) for k,bt in tracked_balls.get_ball_trajectory_dict().iteritems()
                                        if not self._is_penalty_spot(bt))

        return TrackedBalls(new_ball_trajectory_dict)
            
