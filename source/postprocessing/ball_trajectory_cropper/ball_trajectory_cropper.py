import numpy as np
from ...dynamics.particle_filter_fast.tracked_balls import TrackedBalls
from ..tracked_balls_processor import TrackedBallsProcessor
from ...auxillary.interval import Interval

class BallTrajectoryCropper(TrackedBallsProcessor):
    '''
    This class is in charge of cropping ball trajectories to prevent their end points from intersection.
    If a ball changes quickly from one trajectory to another, then the end point of the first one can overlap with the start
    point of the second. This class shortens both trajectories so that there is only one ball position at any given frame.
    '''
    def __init__(self, overlap_threshold, distance_threshold):
        '''
        overlap_threshold - an integer. We only crop a pair of trajectories if have at most this many frames in common.
        distance_threshold - a float. This is used to determine if two ball image positions overlap
        '''
        self._overlap_threshold = overlap_threshold
        self._distance_threshold = distance_threshold
        
    def _crop_ball_trajectories(self, ball_trajectory_0, ball_trajectory_1, interval_0, interval_1):
        '''
        ball_trajectory_i - a BallTrajectory object
        interval_i - an Interval object which represents the interval in which the ball trajectories are defined

        return a pair of BallTrajectory objects. The are cropped versions of the original ball trajectories which do not
        overlap in time.

        If the trajectories can not be cropped (since their ball positions are not close enough to one another),
        return the original trajectories.
        '''
        overlap_interval = interval_0.get_overlap(interval_1)
        print '      overlap interval: ', overlap_interval        

        if overlap_interval.length() > self._overlap_threshold:
            print '      the overlap is too large'
            return ball_trajectory_0, ball_trajectory_1

        if interval_1.last_element() < interval_0.first_element():
            print '      the second trajectory does not come after the first one'
            return ball_trajectory_0, ball_trajectory_1
        
        image_positions_0 = interval_0.get_relative_slice(ball_trajectory_0.image_positions, overlap_interval)
        image_positions_1 = interval_1.get_relative_slice(ball_trajectory_1.image_positions, overlap_interval)
        
        #find the point on in the overlap interval in which the ball image positions are closest
        distances = np.linalg.norm(image_positions_0 - image_positions_1, axis=1)
        closest_index = np.argmin(distances)
        
        #if the balls are too far apart to align, then set the closest index to be the middle of the overlap interval
        if distances[closest_index] > self._distance_threshold:
            print '      the closest distance was %f so the balls are too far apart to align.' % distances[closest_index]
            closest_index = overlap_interval.length() / 2
            #return ball_trajectory_0, ball_trajectory_1

        closest_index = overlap_interval.relative_to_absolute_coords(closest_index)
        print '      cropping at index: ', closest_index
        
        #crop at the closest index
        cropped_bt_0, cropped_bt_1 = TrackedBalls.crop_trajectories(ball_trajectory_0, ball_trajectory_1, closest_index,
                                                                    overlap_at_end_points=False)
        return cropped_bt_0, cropped_bt_1
    
    def _process_tracked_balls(self, tracked_balls):
        '''
        tracked_balls - a TrackedBalls object
        return - a TrackedBalls object which is the same as the input one, but where some of the trajectories have been cropped
        '''

        #get the ball trajectories and sort them by the starting frame
        bt_items = tracked_balls.get_ball_trajectory_dict().items()
        bt_items.sort(key=lambda x: x[1].start_frame)
        bt_keys = [k for k,v in bt_items]

        cropped_ball_trajectories = tracked_balls.get_ball_trajectory_dict().copy()
        
        #iterate over pairs of trajectories, and crop pairs that overlap
        for i, key_0 in enumerate(bt_keys):
            bt_0 = cropped_ball_trajectories[key_0]
            interval_0 = Interval(bt_0.start_frame, bt_0.start_frame + len(bt_0.positions))

            print '* cropping ball trajectory %d' % key_0
            print '  it has interval: ', interval_0
            
            for key_1 in bt_keys[i+1:]:                
                bt_1 = cropped_ball_trajectories[key_1]
                interval_1 = Interval(bt_1.start_frame, bt_1.start_frame + len(bt_1.positions))

                print '   - comparing it with ball trajectory %d' % key_1
                print '     it has interval: ', interval_1

                #if the intervals do not overlap, stop processing bt_0
                if not interval_0.has_overlap(interval_1):
                    print '      there is no overlap.'
                    break

                #if they do, crop the trajectories so that they do not overlap in time
                cropped_bt_0, cropped_bt_1 = self._crop_ball_trajectories(bt_0, bt_1, interval_0, interval_1)

                #update the dict of cropped trajectories
                cropped_ball_trajectories.update([(key_0, cropped_bt_0), (key_1, cropped_bt_1)])

        cropped_tracked_balls = TrackedBalls(cropped_ball_trajectories)
        return cropped_tracked_balls
