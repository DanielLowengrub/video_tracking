from .penalty_spot_remover.penalty_spot_remover import PenaltySpotRemover
from ..auxillary.soccer_field_geometry import SoccerFieldGeometry
import os
import cv2

OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts'
IMAGE_PARENT_DIRECTORY =  '/Users/daniel/Dropbox/soccer/daniel_refactored/test_data'
INPUT_DATA_NAME = 'video7'

# OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
# OUTPUT_TESTING_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output_testing'
# IMAGE_PARENT_DIRECTORY =  '/Users/daniel/Documents/soccer/images'
# INPUT_DATA_NAME = 'images-8_15-9_31'

# OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'
# IMAGE_PARENT_DIRECTORY = '/home/daniel/soccer/images'
# INPUT_DATA_NAME = 'images-8_15-9_31'

POSTPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'postprocessing', 'data', INPUT_DATA_NAME)
TRACKED_BALLS_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'ball_tracker', 'data', INPUT_DATA_NAME)

NO_PENALTY_SPOTS_DIRECTORY = os.path.join(POSTPROCESSING_DIRECTORY, 'no_penalty_spots')

DISTANCE_THRESHOLD_IN_YARDS = 2.5

def load_absolute_soccer_field():
    '''
    Return a tuple (soccer field, absolute image)
    '''
    ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)
    SCALE = 6
    FIELD_WIDTH = absolute_image.shape[1]-1
    FIELD_LENGTH = absolute_image.shape[0]-1
    GRID_WIDTH = 20
    GRID_LENGTH = 14
    soccer_field = SoccerFieldGeometry(SCALE, FIELD_WIDTH, FIELD_LENGTH, GRID_WIDTH, GRID_LENGTH)

    return soccer_field

def remove_penalty_spots(tracked_balls_directory, no_penalty_spots_directory):
    '''
    tracked_balls_directory - a directory containing the output BallTrackerController.track_balls
    output_directoy - the direcory where we should store the improved TrackedBalls object.
    '''    
    if not os.path.isdir(no_penalty_spots_directory):
        print 'making directory: ', no_penalty_spots_directory
        os.mkdir(no_penalty_spots_directory)

    soccer_field = load_absolute_soccer_field()
    penalty_spot_remover = PenaltySpotRemover(soccer_field, DISTANCE_THRESHOLD_IN_YARDS)
    penalty_spot_remover.process_tracked_balls(tracked_balls_directory, no_penalty_spots_directory)
    
if __name__ == '__main__':
    remove_penalty_spots(TRACKED_BALLS_DIRECTORY, NO_PENALTY_SPOTS_DIRECTORY)
