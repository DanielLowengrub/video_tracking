import numpy as np
import cv2
from .contour_generator import ContourGenerator
from ...data_structures.contour import Contour

#some constants related to the contour hierarchy data
NEXT = 0
PREV = 1
CHILD = 2
PARENT = 3
        
        
class ContourFromMaskGenerator(ContourGenerator):
    '''
    This contour generator generates a list of contours from a black and white mask.
    '''

    def _get_child_contours(self, parent_index, contours, hierarchy):
        '''
        Get all children contours of the parent contour.
        Note the the hierarchy variable contains one 4-tuple of the form
        [next, prev, child, parent]
        
        So to get all of the children, we first use the "child" index of the parent to get the first child,
        Then keep using the "next" index to get the rest of the children
        '''
        child_contours = []
        child_index = hierarchy[parent_index][CHILD]
        
        #if there are no children, return an empty list
        if child_index == -1:
            return child_contours

        #otherwise, start adding children
        while child_index != -1:
            child_contours.append(contours[child_index])
            child_index = hierarchy[child_index][NEXT]

        return child_contours

    def generate_contours(self, image, mask):
        '''
        image - a numpy array of shape (n,m,3) and type np.uint8
        mask - and numpy array wih shape (n,m) and type np.float32 with values between 0 and 1.

        We return a list of contours tracing the boundaries of the mask
        '''
        contours = []

        #mask = np.float32(mask > 0)
        #mask = np.uint8(mask * 255)
        mask = np.uint8(mask)
        
        #extract the contours and perserve hierarchy data
        cv_contours, hierarchy = cv2.findContours(mask, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)

        if len(cv_contours) == 0:
            return []
        
        hierarchy = hierarchy[0]

        #save all the parent contours, together with their children
        for contour_index, cv_contour in enumerate(cv_contours):

            #skip the contours that are not external contours, i.e, if they have a parent contour
            if hierarchy[contour_index][PARENT] != -1:
                continue
        
            #now we know that it is a parent contour. So we get a list of it's children, and then
            #use this data to create a contour object
            child_cv_contours = self._get_child_contours(contour_index, cv_contours, hierarchy)

            #convert the cv_contours to Contour objects.
            child_contours = [Contour(child_cv_contour, image) for child_cv_contour in child_cv_contours]
            contour = Contour(cv_contour, image)
            contour.set_child_contours(child_contours)

            contours.append(contour)

        return contours

