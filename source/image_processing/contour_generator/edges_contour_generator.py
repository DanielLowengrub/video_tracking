import numpy as np
import mahotas
import cv2
from .contour_generator import ContourGenerator
from ...data_structures.contour import Contour
from ...auxillary import mask_operations

#some constants related to the contour hierarchy data
NEXT = 0
PREV = 1
CHILD = 2
PARENT = 3
        
DEBUG = False

class EdgesContourGenerator(ContourGenerator):
    '''
    This type of contour generator uses the edges in the image to generate contours.
    Specifically, it first uses a Canny edge generator to generate edges, then closes them up with a kernel.
    Finally, it applies a contour detector to the result.

    Also, this class returns only parent contours, and each parent contour contains a list of it's top level children.

    ASSUPTIONS: This contour detector assumes that there are a bunch of noisy objects on a fairly homogeneous background.
                In this case, the objects will be filled with a whole bunch of edges, and after applying the 
                closing kernel, we will get a solid white shape for each image.
                However, if the objects themselves are homogeneous, 
                only their outlines will show up as edges. Then, each such edge will contribute
                2 contours (inside and outside) and there will be a mess.
    '''

    def __init__(self, canny_min, canny_max, kernel_close = np.ones((9,9), np.uint8), padding=10):
        '''
        canny_min, canny_max - integers. parameters for the Canny algorithm
        kernel_close is the kernel we use to close up the edges.
        padding - an integer. throw out edges that are within padding of the side of the image
        '''
        self._canny_min = canny_min
        self._canny_max = canny_max
        self._kernel_close = kernel_close
        self._padding = padding
        
        return
    
    def _get_child_contours(self, parent_index, contours, hierarchy):
        '''
        Get all children contours of the parent contour.
        Note the the hierarchy variable contains one 4-tuple of the form
        [next, prev, child, parent]
        
        So to get all of the children, we first use the "child" index of the parent to get the first child,
        Then keep using the "next" index to get the rest of the children
        '''
        child_contours = []
        child_index = hierarchy[parent_index][CHILD]
        
        #if there are no children, return an empty list
        if child_index == -1:
            return child_contours

        #otherwise, start adding children
        while child_index != -1:
            child_contours.append(contours[child_index])
            child_index = hierarchy[child_index][NEXT]

        return child_contours

    def _get_padded_mask(self, mask):
        '''
        mask - a np array with shape (n,m) and type np.bool
        return a copy of mask in which the pixels that are within self._padding of the boundary are set to False
        '''
        padded_mask = mask.copy()
        padded_mask[:self._padding,:]  = False
        padded_mask[-self._padding:,:] = False
        padded_mask[:,:self._padding]  = False
        padded_mask[:,-self._padding:] = False

        return padded_mask
    
    def generate_contours(self, image, mask=None, approx=cv2.CHAIN_APPROX_SIMPLE):
        '''
        image - a numpy array of shape (n,m,3) and type np.uint8
        mask - a numpy array of shape (n,m) and type np.bool

        return a tuple (list of Contour objects, contour mask)
        '''
        contours = []
        
        #first use the Canny edge detector to find edges.
        edges = cv2.Canny(image, self._canny_min, self._canny_max,  L2gradient=True) #80, 150 (55,90)

        if mask is None:
            mask = np.ones(image.shape[:2], np.bool)

        #get the padded the mask
        padded_mask = self._get_padded_mask(mask)

        #remove the edges that are in the padded mask
        edges[np.logical_not(padded_mask)] = 0

        if DEBUG:
            cv2.imshow('mask', np.float32(padded_mask))
            cv2.waitKey(0)
            cv2.destroyAllWindows()

            cv2.imshow('edges', edges)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

        #now fill in the edges
        closed_edges = cv2.morphologyEx(edges, cv2.MORPH_CLOSE, self._kernel_close)

        if DEBUG:
            cv2.imshow('closed edges', 255*closed_edges)
            cv2.waitKey(0)
            cv2.destroyAllWindows()


        #extract the contours and perserve hierarchy data
        cv_contours, hierarchy = cv2.findContours(closed_edges, cv2.RETR_CCOMP, approx)

        hierarchy = hierarchy[0]

        #save all the parent contours, together with their children
        for contour_index, cv_contour in enumerate(cv_contours):

            #skip the contours that are not external contours, i.e, if they have a parent contour
            if hierarchy[contour_index][PARENT] != -1:
                continue
        
            #now we know that it is a parent contour. So we get a list of it's children, and then
            #use this data to create a contour object
            child_cv_contours = self._get_child_contours(contour_index, cv_contours, hierarchy)

            #convert the cv_contours to Contour objects.
            child_contours = [Contour(child_cv_contour, image) for child_cv_contour in child_cv_contours]
            contour = Contour(cv_contour, image)
            contour.set_child_contours(child_contours)

            contours.append(contour)

        return contours, (closed_edges > 0)

