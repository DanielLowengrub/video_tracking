class ContourGenerator(object):
    '''
    The job of the contour generator is to find all contours on a certain region of an image.

    Different implementations use different methods to convert the image to black and white, and may
    put different hierarchy data in the contours.
    '''

    def generate_contours(self, image, mask):
        '''
        image - a numpy array of size (n,m,3) and type np.uint8.
        mask - a numpy array of size (n,m) and type np.float64

        Return a list of contour objects.
        Depending on the implementation, these contour objects may consist only of the parent contours, 
        and may contain hierarchy data.
        '''

        pass

        
