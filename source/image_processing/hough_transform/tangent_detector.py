import cv2
import numpy as np
import collections, itertools
from ...auxillary.fit_points_to_shape.line import Line
from ...auxillary.interval import Interval, IntervalList
from ...auxillary.contour_shape_overlap.interval_on_contour import IntervalOnContour
import matplotlib.pyplot as plt

'''
TangentCandidate corresponds to a range of points on a contour that could possibly fit a tangent vector.
It stores an Interval object corresponding to the range, and the index of the center of the candidate.
'''
_TangentCandidate = collections.namedtuple('_TangentCandidate', ['center_index', 'interval'])

class TangentLine(object):
    '''
    TangentLine corresponds to a line that is tangent to a point on a contour. It has two main attributes:
    center - a numpy array of length 2. This is the point on the contour that meets the tangent line.
    line - a Line object. This is the tangent line.
    
    We also define a hash function which just hashes the union of the center and line paramenters.
    '''

    def __init__(self, center, line):
        self.center = center
        self.line = line

        #there is also an object to assign an index to this tangent line
        self._id = None

    def set_id(self, tangent_line_id):
        self._id = tangent_line_id
        return

    def get_id(self):
        return self._id

    
'''
TangentLines - This stores a list of TangentLine objects, together with the arc length of their union.
'''
WeightedTangentLines = collections.namedtuple('WeightedTangentLines', ['tangent_lines', 'weight'])

class TangentDetector(object):
    '''
    The goal of a TangentDetector is to extract a list of tangent vectors from a contour.
    Furthermore, it groups these vectors according to the (theta, r) parameters of the lines they span.

    A tangent vector is defined to be a point on a contour, toether with a line that passes through the contour, and approximates the neighborhood of the point.

    The behavior of the tangent detector depends on a FitPointsToShape object. Specifically, at a given point p, we compute the tangent vactor to p by fitting the neighborhood
    of p to the given type of shape, and using this type of shape to compute the tangent vector.
    '''

    def __init__(self, shape_fitter, min_line_length, max_geometric_residue, theta_bins, radius_bins):
        '''
        min_line_length - the minimum length that a tangent line is allowed to have.
        max_geometric_residue - the maximum distance that a tangent line is allowed to have to the contour points.
        '''
        self._shape_fitter = shape_fitter
        self._min_line_length = min_line_length
        self._half_min_line_length = 0.5 * min_line_length
        self._max_geometric_residue = max_geometric_residue

        self._theta_bins = theta_bins
        self._radius_bins = radius_bins
        
        self._arc_length_from_start = None

        #this will hold a numpy array with shape (number of points in contour, 2)
        self._points = None

        #FOR DEBUGGING ONLY
        #self._debug_ellipse = False
        self._debug_line = False
        
    def _compute_arc_length_from_start(self):
        '''
        We populate the arc_distance_from_start array. At the i-th index it contains the arc distance from the first point in the
        contour to the i-th point.
        '''

        vector_to_next = self._points[1:] - self._points[:-1]
        distance_to_next = np.linalg.norm(vector_to_next, axis=1)
        arc_length_from_start = np.cumsum(distance_to_next)

        #add a zero to the beginning because the distance to the first point is 0
        self._arc_length_from_start = np.hstack([np.zeros(1), arc_length_from_start])

        return

    def _get_index_of_first_center_point(self):
        '''
        Return the index i of the first point pi in _points such that the sum d(p0,p1)+...+d(p_{i-1},pi) > min_line_length / 2
        '''
        #we search the array of arc lengths and find the index of the first one that is larger than half-line_length
        return np.searchsorted(self._arc_length_from_start, self._half_min_line_length)
        
    def _get_tangent_candidates(self):
        '''
        We return a list of _TangentCandidate objects
        Each TC corresponds to a possible tangent vector whose center is the point at TC.center_index and where TC.first_index and TC.last_index
        define shortest range of points on the contour p_{first index},...,p_{last index} such that:
        * arc_length(p_{first_index}, p_{center index}) = d(p1,p_{first index}) + ... + d(p_{center_index},p_{center_index-1}) > min_line_length / 2
        * arc_length(p_{center_index}, p_{last_index})  = d(p_{center_index},p_{center_index+1}) + ... + d(p_{last index -1}, p_{last index}) > min_line_length / 2
        '''
        tangent_candidates = []

        #Get the first possible center point
        #this is the first point pi in the cv_contour such that the sum d(p0,p1)+...+d(p_{i-1},pi) > min_line_length / 2
        center_index = self._get_index_of_first_center_point()

        while center_index + 1 < len(self._points):
            #find the largest index i that is smaller than center_index and such that the arc distance from
            #i to center index is larger than half_min_line_length

            #we construct a list [-arc_length(p0,center point), -arc_length(p1,center point), ..., -arc_length(p_{center_index-1}, center point)]
            arc_length_from_center = self._arc_length_from_start[:center_index] - self._arc_length_from_start[center_index]

            #and find the largest index such that -arc_length_from_center[i] <= -self._half_min_line_length
            first_index = np.searchsorted(arc_length_from_center, -self._half_min_line_length) - 1

            #now construct a list [arc_length(center point, p_{center_point+1}),...]
            arc_length_from_center = self._arc_length_from_start[center_index+1:] - self._arc_length_from_start[center_index]

            #and find the smallest index such that arc_length_from_center[i] >= self._half_min_line_length
            last_index = np.searchsorted(arc_length_from_center, self._half_min_line_length)
            last_index += center_index + 1

            #if we could not find a last point that was far away enough from the center point, stop searching
            if last_index == len(self._points):
                break
            
            #if we could not find a starting point that was far away enough from the center, proceed to the next possible center point
            if first_index == -1:
                center_index += 1
                continue

            # print 'new candidate:'
            # print '   arc length from start to center: ', self._arc_length_from_start[center_index] - self._arc_length_from_start[first_index]
            # print '   arc length from center to end: ', self._arc_length_from_start[last_index] - self._arc_length_from_start[center_index]

            tangent_candidates.append(_TangentCandidate(center_index, Interval(first_index,last_index+1)))
            center_index += 1

        return tangent_candidates

    def _get_theta_bins(self):
        '''
        theta_bins is a list [0,...,180] corresponding the the boundaries of the bins that are obtained by dividing up the range [0,180]
        into bins with size theta_bin_size.
        '''
        return range(0,181,self._theta_bin_size)

    def _get_radius_bins(self):
        '''
        radius_bins is a list [-L, ..., L] corresponding to the boundaries of bins that are obtained by dividing the range [-L,L] into bins with size radius_bin_size.
        here, L = norm(image_shape).
        '''
        L = np.linalg.norm(np.array(self._image_shape))
        return range(-L, L+1, self._radius_bin_size)

    def _compute_arc_length_of_interval(self, interval):
        return self._arc_length_from_start[interval.last_element()-1] - self._arc_length_from_start[interval.first_element()]

    def _pad_points_and_arc_length_from_start(self):
        '''
        Let I = (points[0],...,points[k]) be the first sequence of points in _points with length >= min_line_length.
        We add the points in I to the end of _points, and update _arc_length_from_start to reflect this.
        '''

        # print 'padding the points...'
        # print 'points: ', self._points
        # print 'arc lengths: ', self._arc_length_from_start

        first_to_last_arc_length = np.linalg.norm(self._points[-1] - self._points[0])
        #print 'first to last arc length: ', first_to_last_arc_length
        
        #the last point in the padding is the first point whose distance from _points[0] is >= min_line_length.
        last_point_in_padding_index = np.searchsorted(self._arc_length_from_start, self._min_line_length)
        #print 'index of last point in padding: ', last_point_in_padding_index
        
        points_padding = self._points[:last_point_in_padding_index]

        #add the padding to the end of the list of points
        self._points = np.vstack([self._points, points_padding])
        
        #and update the _arc_length_from_start array.
        #We add the arc length to the last point plus the arc length from the last point to the first point to the arc lengths of each of the padding points
        arc_length_padding = (self._arc_length_from_start[-1] + first_to_last_arc_length) + self._arc_length_from_start[:last_point_in_padding_index]
        
        self._arc_length_from_start = np.hstack([self._arc_length_from_start, arc_length_padding])

        # print 'the padded points: ', self._points
        # print 'the padded arc lengths: ', self._arc_length_from_start
        
        return

    def _display_fitted_line(self, points_in_interval, line, geometric_residue):
        print 'drawing the line:'
        print line
        
        fig = plt.figure()
        ax = fig.add_subplot('111')
        ax.imshow(self._image)
        ax.plot(points_in_interval[:,0], points_in_interval[:,1], 'bo')

        if np.abs(line._b) > 10**(-2):
            x = np.array([0, self._image.shape[1]])
            y = (-line._a / line._b)*x - (line._c / line._b)
            
        else:
            y = np.array([0, self._image.shape[0]])
            x = -(line._b / line._a)*y - (line._c / line._a)

        points = np.int32(np.vstack([x,y]).transpose().reshape((-1, 2)))
        print 'points = ', points
        ax.plot(points[:,0], points[:,1])
        
        ax.set_xlim(-10, self._image.shape[1] + 10)
        ax.set_ylim(self._image.shape[0] + 10, -10)
    
        plt.show()
    
        
    def _fit_points_to_shape(self, points_in_interval):
        '''
        points_in_interval - a numpy array with shape (num points, 2)

        Return a PlanarShape object that fits these points. 
        If the geometric residue of the fit was too large, we return None.
        '''

        shape, algebraic_residue = self._shape_fitter.fit_points(points_in_interval)

        if shape is None:
            return None

        geometric_residue = shape.get_geometric_residue(points_in_interval)
        #if self._debug_ellipse:
        #    self._display_fitted_ellipse(points_in_interval, shape, geometric_residue)
        if self._debug_line:
            self._display_fitted_line(points_in_interval, shape, geometric_residue)
            
        if geometric_residue > self._max_geometric_residue:
                #print 'the geometric residue was too large: ', geometric_residue
                return None
            
        #print 'got a match with geometric residue: ', geometric_residue
        return shape
    
    def _get_weighted_tangent_lines_dict_on_subsequence(self, points, closed_loop=False):
        '''
        contour - a Contour object
        contour_mask - a numpy array of length (num pts in contour) and type np.bool. We only consider points in the mask.
        closed_loop - boolean. If this is true, we treat this sequence as a closed loop. This means that when we compute the tangent line at a point
                      near the end of the point sequence, we use points from the beginning of the sequence.

        Return tuple: weighted_tangent_lines_dict, tangent_point_mask
        where:
        weighted_tangent_lines_dict is a dictionary with key/values ((angle bin, radius bin), WeightedTangentLines object)
        tangent_point_mask is a numpy array of length len(points) and type np.bool. It has value True at index i iff points[i] lies on a tangent line.

        angle_bins is a list [0,...,180] corresponding the the boundaries of the bins that are obtained by dividing up the range [0,180] into bins with size theta_bin_size
        radius_bins is a list [-L, ..., L] corresponding to the boundaries of bins that are obtained by dividing the range [-L,L] into bins with size radius_bin_size.
        here, L = norm(image_shape).
        '''

        self._points = points
        
        #this mask records which of the given points are the center of a tangent line
        tangent_point_mask = np.zeros(len(points), np.bool)
        
        #precompute the arc lengths
        self._compute_arc_length_from_start()

        if closed_loop:
            self._pad_points_and_arc_length_from_start()
            
        weighted_tangent_lines_dict = collections.defaultdict(list)
        intervals_dict = collections.defaultdict(IntervalList)
        
        for tangent_candidate in self._get_tangent_candidates():
            #try to fit the points in the tangent candidate to a shape
            points_in_interval = tangent_candidate.interval.get_slice(self._points)
            shape = self._fit_points_to_shape(points_in_interval)
                
            #if there is a match
            if shape is not None:
                #generate a line that is tangent to the shape at the center point of the tangent candidate
                center_point = self._points[tangent_candidate.center_index]
                tangent_vector = shape.get_tangent_vector(center_point)
                line = Line.from_point_and_direction(center_point, tangent_vector)
                
                #determine which bucket to put this tangent line in
                theta, radius = line.get_angle_radius(degrees=True)
                angle_bin = np.digitize([theta], self._theta_bins)[0]
                radius_bin = np.digitize([radius], self._radius_bins)[0]
                tangent_line = TangentLine(center_point, line)

                # if  center_point[0] <= 70 and center_point[1] < 200:
                #     print 'center: %s, direction: %s' % (center_point, tangent_vector)
                #     print '   %s' % line
                #     print '   theta: %s, radius: %s, bin: %s' % (theta, radius, (angle_bin,radius_bin))

                weighted_tangent_lines_dict[(angle_bin, radius_bin)].append(tangent_line)
                intervals_dict[(angle_bin, radius_bin)].add_interval(tangent_candidate.interval, in_order=True)

                #we also record the match in the tangent_point_mask
                #note that the center_index could be larger than len(tangent_point_mask) since we may have padded the points by adding
                #some of the first points to the end.
                #therefore, we take index mod the length of the tangent_point_mask
                center_index_in_point_mask = tangent_candidate.center_index % len(tangent_point_mask)
                tangent_point_mask[center_index_in_point_mask] = True

        #now use the intervals in each bucket to give the bucket a weight
        for key, interval_list in intervals_dict.iteritems():
            weight = sum(self._compute_arc_length_of_interval(interval) for interval in interval_list.get_intervals())
            tangent_lines = weighted_tangent_lines_dict[key]
            weighted_tangent_lines_dict[key] = WeightedTangentLines(tangent_lines, weight)

        return weighted_tangent_lines_dict, tangent_point_mask


        
    def _find_continuous_subsequences(self, contour, contour_mask):
        '''
        contour - a Contour object
        contour_mask - a numpy array of shape (num points) and type np.bool

        Return a list of numpy arrays with shape (num points in sequence, 2), with one list for each contiuous sequence of points in the mask.
        '''

        #first generate a list of contour intervals
        intervals_on_contour = IntervalOnContour.from_point_mask(contour, contour_mask)

        #return the points on each of the intervals
        return dict([(interval, interval.get_points()) for interval in intervals_on_contour])

    def _disjoint_union_of_weighted_tangent_lines(self, weighted_tangent_lines_list):
        '''
        weighted_tangent_lines_list - a list of WeightedTangentLine objects

        Return a WeightedTangentLine object, whose list of tangent lines is the union of the lists of each of the given wtls, and whose
        weight is the sum of the weights.
        '''

        #print 'taking the disjoint union over the weighted tangent lines'
        #print '\n'.join(str(wtl) for wtl in weighted_tangent_lines_list)
        
        total_weighted_tangent_lines = reduce(lambda x,y: WeightedTangentLines(x.tangent_lines + y.tangent_lines, x.weight + y.weight),
                                              weighted_tangent_lines_list, WeightedTangentLines([],0))
        
        return total_weighted_tangent_lines
    
    def get_weighted_tangent_lines_dict(self, contour, contour_mask=None):
        '''
        contour - a Contour object
        contour_mask - a numpy array of length (num pts in contour) and type np.bool. We only consider points in the mask.
        Return tuple: tangent_lines_dict, angle_bins, radius_bins

        where:
        tangent_lines_dict is a dictionary with key/values ( (angle bin, radius bin), [list of tangent lines whose angle and radius is in this bin])
        angle_bins is a list [0,...,180] corresponding the the boundaries of the bins that are obtained by dividing up the range [0,180] into bins with size theta_bin_size
        radius_bins is a list [-L, ..., L] corresponding to the boundaries of bins that are obtained by dividing the range [-L,L] into bins with size radius_bin_size.
        here, L = norm(image_shape).
        '''
        #THIS IS FOR DEBUGGING ONLY
        self._image = contour.get_image()

        #we use this mask to record which points are on a tangent line
        self._tangent_point_mask = np.zeros(len(contour.get_cv_contour()), np.bool)

        if contour_mask is None:
            contour_mask = np.ones(len(contour.get_cv_contour()), np.bool)

        #find continuous sequences of points in the mask
        #continuous_point_subsequences_dict = self._find_continuous_subsequences(contour, contour_mask)

        #find contiuous intervals in the contour
        intervals_on_contour = IntervalOnContour.from_point_mask(contour, contour_mask)

        if len(intervals_on_contour) == 0:
            return dict()
        #print 'the point sequences are: '
        #print '\n'.join(str(p) for p in continuous_point_subsequences.values())

        #if all of the points are in the mask, then there is only one point sequence, and it is a loop
        closed_loop =  np.all(contour_mask)
        
        #compute a tangent_lines dictionary for each subsequence
        continuous_point_sequences = (interval.get_points() for interval in intervals_on_contour)
        weighted_tangent_lines_dicts, tangent_point_masks = zip(*(self._get_weighted_tangent_lines_dict_on_subsequence(s, closed_loop)
                                                                  for s in continuous_point_sequences))

        #combine the tangent_lines dictionaries into one dictionary
        angle_radius_bins = set(itertools.chain(*(d.iterkeys() for d in weighted_tangent_lines_dicts)))
        weighted_tangent_lines_dict = dict((k, self._disjoint_union_of_weighted_tangent_lines([d[k] for d in weighted_tangent_lines_dicts if k in d]))
                                           for k in angle_radius_bins)

        #combine the tangent_point_masks into one mask on all of the contour points
        for interval_on_contour, tangent_point_mask in zip(intervals_on_contour, tangent_point_masks):
            relative_tangent_point_indices = np.where(tangent_point_mask)[0]
            if len(relative_tangent_point_indices) == 0:
                continue
            
            absolute_tangent_point_indices = np.array(map(interval_on_contour.get_absolute_index, relative_tangent_point_indices))
            self._tangent_point_mask[absolute_tangent_point_indices] = True
            
        return weighted_tangent_lines_dict

    def get_point_mask(self):
        '''
        Return a numpy array of length (number of points in the contour from the previous call of get_weighted_tangent_lines_dict) and type np.bool.
        This boolean array records which of the points on the contour were used to generate a tangent line.
        '''
        return self._tangent_point_mask
