import itertools
import numpy as np
import cv2
from .tangent_detector import TangentDetector, WeightedTangentLines
from ...auxillary.fit_points_to_shape.fit_points_to_line import FitPointsToLine
from scipy.ndimage.measurements import label
import matplotlib.pyplot as plt
from ....testers.auxillary import primitive_drawing

#WARNING:
#There are at least two weaknesses in the current algo:
#1) Does not detect very straight lines: If a line is so straight that the edge detector gives it only 2 or 3 contour points,
#   then no tanget vector will be generated for this line, and the line will not be detected.
#   Possible solution: Add each pair of contour points pi,p{i+1} that are far enough apart to the set of tangent candidates.
#
#2) Detects Ellipses: Sometimes this algo detects the side of an ellipse as a line. This happens because the tangent vectors
#   of the ellipse are very close to one another but sometimes there is a gap between bins. This creates a cluster of bins
#   which is fitted to a line.
DEBUG = True

class LineDetector(object):
    '''
    We extract a list of lines from a given set of contours on the image.

    The algorithm works as follows:
    1) Extract tangent lines from each contour using the TangentDetector.
       For each contour, this gives us a dictionary with key,value pairs ((angle bin, radius bin), WeightedTangentLines object)
    2) For each bin, combine the WeightedTangentLine objects into one object.
    3) Mark the non empty bins on an array, and find connected components.
    4) Find all components whose total weight is above a given threshold, and whose standard deviation is not too large.
    5) For each such component, try to fit all of the points that it contains to a line. If this fails, throw out the component.
    6) For each of the lines obtained in the previous step, try to fit it to all of the points in each of the given contours. For each contour, record which points
       lie on the line.
    7) Return the list of lines. In addition, for each line return a list [ [points on the line from contour 0], [points on the line from contour 1],...] 
    '''

    def __init__(self, min_tangent_arc_length, max_tangent_algebraic_residue,
                 theta_bin_width, radius_bin_width, max_theta_deviation, max_radius_deviation, min_arc_length,
                 max_average_algebraic_residue):
        '''
        min_tangent_length - the minimum arc length that is needed to determine a tangent vector
        min_tangent_algebraic_residue - the minimum algebraic residue that is allowed when fitting line to tanent vectors
        theta_bin_width - the width of the bins we use to discritize the angles of the lines
        radius_bin_width - the width of the bins we use to discritize the radii of the lines
        max_theta_deviation - the maximum std that we allow among the angles of tangent vectors that are on the same line. This should be given in the units of "bins"
        max_radius_deviation - " " radii " "
        minimum_arc_length - the minimum arc length required for a sequence of points to be considered on a line
        minimum_average_algebraic_residue - we use this to test if a list of points lie on a line.
        '''
        self._min_tangent_arc_length = min_tangent_arc_length
        self._max_tangent_algebraic_residue = max_tangent_algebraic_residue
        
        self._theta_bin_width = theta_bin_width
        self._radius_bin_width = radius_bin_width
        
        self._max_theta_radius_deviation = np.array([max_theta_deviation, max_radius_deviation])

        self._min_arc_length = min_arc_length
        self._max_average_algebraic_residue = max_average_algebraic_residue

        self._line_fitter = FitPointsToLine()
        
        self._image = None
        self._contours = None
        self._contour_masks = None
        self._theta_bins = None
        self._radius_bins = None
        self._tangent_detector = None
        self._disgarded_weighted_tangent_lines_dict = None
        
    def _compute_histogram_bins(self):
        self._theta_bins = np.arange(-180, 181, self._theta_bin_width)

        L = np.linalg.norm(np.array(self._image.shape))
        self._radius_bins = np.arange(0, L+1, self._radius_bin_width)

        return

    def _build_tangent_detector(self):
        self._tangent_detector = TangentDetector(self._line_fitter, self._min_tangent_arc_length,
                                                 self._max_tangent_algebraic_residue, self._theta_bins, self._radius_bins)
        return
    
    def _compute_weighted_tangent_lines_dict(self):
        '''
        Return a dictionary with key/value pairs 
        ((theta bin,radius bin), WeightedTangentLines object corresponding to tangent lines whose theta/radius parameters are in the given bin)
        '''
        weighted_tangent_lines_dict = dict()

        #first get the dictionaries corresponding to each contour seperately
        weighted_tangent_lines_dicts = [self._tangent_detector.get_weighted_tangent_lines_dict(contour, contour_mask)
                                        for contour,contour_mask in zip(self._contours, self._contour_masks)]
        
        keys = set(itertools.chain(*(d.iterkeys() for d in weighted_tangent_lines_dicts)))

        #now merge these dictionaries into one
        for key in keys:
            tangent_lines = list(itertools.chain(*(d[key].tangent_lines for d in weighted_tangent_lines_dicts if key in d)))
            weight = sum(d[key].weight for d in weighted_tangent_lines_dicts if key in d)
            weighted_tangent_lines_dict[key] = WeightedTangentLines(tangent_lines, weight)

        return weighted_tangent_lines_dict

    def _compute_tangent_line_histogram(self, weighted_tangent_lines_dict):
        '''
        weighted_tangent_lines_dict - dictionary with key/value pairs ((theta,radius), WeightedTangentLines object corresponding to tangent lines with the given parameters)

        We return a histogram which in position (theta bin, radius bin) has the weight of the WeightedTangentLines object weighted_tangent_lines_dict[(theta bin, radius bin)]
        '''

        hist = np.zeros((len(self._theta_bins)+1, len(self._radius_bins)+1), np.float32)

        for key, weighted_tangent_lines in weighted_tangent_lines_dict.iteritems():
            #print 'weight(%d, %d) = %f' % (key[0], key[1], weighted_tangent_lines.weight)
            hist[key[0], key[1]] = weighted_tangent_lines.weight

        return hist

    def _compute_connected_components_dict(self, tangent_line_histogram, weighted_tangent_line_dict):
        '''
        We first find the connected components among the non zero elements of the histogram, and label them by integers.
        We return a dictionary whose keys are CC labels, and whose values are lists of (theta bin, radius bin) pairs.
        '''

        mask = np.int32((tangent_line_histogram > 0))
        #num features is the number of connected components.
        #labelled_array is an array with the same shape as mask, and such that the non zero elements have values in 1,...,num_labels depending on which CC they are in.
        labelled_array, num_features = label(mask)

        # print 'showing the connected components of the histogram...'
        # p = plt.imshow(labelled_array, interpolation="nearest")
        # plt.colorbar(p)
        # plt.show()
    
        connected_components_dict = dict()
        for cc_label in range(1,num_features+1):
            connected_components_dict[cc_label] = [tuple(theta_radius) for theta_radius in np.argwhere(labelled_array == cc_label)]

        return connected_components_dict

    def _remove_noisy_components(self, connected_components_dict, weighted_tangent_lines_dict):
        '''
        connected_components_dict - a dictionary with key/value pairs (CC label, [list of (theta bin, radius bin) pairs])
        weighted_tangent_lines_dict - a dictionary with key/value pairs ((theta bin, radius bin), WeightedTangentLines object)

        We remove the entries of the connected_components_dict that have too much noise. This means that either:
        1) the variance in theta_bins or radius_bins is too large
        2) the total weight in the component is too small
        '''

        #print 'removing noise components...'
        self._disgarded_weighted_tangent_lines_dict = dict()
        
        for cc_label in connected_components_dict.keys():
            if DEBUG:
                print '   checking component %d with bins: ' % cc_label, connected_components_dict[cc_label]
            theta_radius_array = np.array(connected_components_dict[cc_label])
            weights = [weighted_tangent_lines_dict[theta_radius].weight for theta_radius in connected_components_dict[cc_label]]
            arc_length = sum(weights)
            theta_radius_mean = np.average(theta_radius_array, axis=0, weights=weights)
            theta_radius_std = np.average((theta_radius_array - theta_radius_mean)**2, axis=0, weights=weights)

            if DEBUG:
                print '      theta radius std: ', theta_radius_std
                print_args = (cc_label, arc_length, theta_radius_std[0], theta_radius_std[1])

            #if the total arc length of this component is too small, discard it and move to the next component
            if arc_length < self._min_arc_length or np.any(theta_radius_std > self._max_theta_radius_deviation):
                if DEBUG:
                    print '      removing component %d with arc length=%f, theta std=%f, radius_std=%f' % print_args

                #add all of the bins in this connected component to the disgarded ones
                for theta_radius in connected_components_dict[cc_label]:
                    self._disgarded_weighted_tangent_lines_dict[theta_radius] = weighted_tangent_lines_dict[theta_radius]
                    
                del connected_components_dict[cc_label]
                continue
            
            
            # #check if this component has too large of a std
            # if np.any(theta_radius_std > self._max_theta_radius_deviation):
            #     print ' '*3 + 'removing component %s because its std (theta std=%f, radius std=%f) was too large.' % (str(cc_label), theta_radius_std[0], theta_radius_std[1])
            #     del connected_components_dict[cc_label]
            #     continue

            if DEBUG:
                print ' '*3 + '* keeping component %d with arc length=%f, theta std=%f, radius_std=%f' % print_args
            
        return

    def _fit_components_to_lines(self, connected_components_dict, weighted_tangent_lines_dict):
        '''
        connected_components_dict - a dictionary with key/value pairs (CC label, [list of (theta bin, radius bin) pairs])
        weighted_tangent_lines_dict - a dictionary with key/value pairs ((theta bin, radius bin), WeightedTangentLines object)

        We try to fit the union of the points in each CC to a line. We return a dictionary with key value pairs (cc_label, line)
        '''
        #print 'fitting components to line...'
        component_line_dict = dict()
        
        for cc_label in connected_components_dict.keys():
            #get a list of the centers of all of the tangent lines in the bins of this component
            centers = list(itertools.chain(*([tl.center for tl in weighted_tangent_lines_dict[theta_radius_bin].tangent_lines]
                                             for theta_radius_bin in connected_components_dict[cc_label])))
            #print '      mean position: ', np.vstack(centers).mean(axis=0)

            if DEBUG:
                print 'fitting a line to the following points:'
                image_copy = self._image.copy()
                primitive_drawing.draw_points(image_copy, centers, (255,0,0))
                cv2.imshow('image with centers', image_copy)
                cv2.waitKey(0)
                cv2.destroyAllWindows()

                # fig = plt.figure()
                # ax = fig.add_subplot('111')
                # ax.plot([center[0] for center in centers], [center[1] for center in centers], 'bo')
                # ax.set_xlim(-10, self._image.shape[1] + 10)
                # ax.set_ylim(-10, self._image.shape[0] + 10)
                # plt.show()
            
            fitted_line, total_algebraic_residue = self._line_fitter.fit_points(centers)
            average_algebraic_residue = total_algebraic_residue / len(centers) if fitted_line is not None else 0

            #if we could  match the points to a line, record it in thedictionary
            print_args = (str(cc_label), average_algebraic_residue)
            if fitted_line is not None and average_algebraic_residue < self._max_average_algebraic_residue:
                if DEBUG:
                    print '      managed to match component %s to a line. It had an algebraic residue of: %f' % print_args
                    #print '      line: ', fitted_line
                component_line_dict[cc_label] = fitted_line

            else:
                #add all of the bins in this connected component to the disgarded ones
                for theta_radius in connected_components_dict[cc_label]:
                    self._disgarded_weighted_tangent_lines_dict[theta_radius] = weighted_tangent_lines_dict[theta_radius]

                if DEBUG:
                    print ' '*3 + 'could not match component %s to a line. It had an algebraic residue of: %f' % (str(cc_label), average_algebraic_residue)
                
        return component_line_dict
    
    def get_lines(self, image, contours, contour_masks=None):
        '''
        image - a numpy array with shape (n,m,3) and type np.uint8
        contours - a list of contour objects
        contour_masks - a list of contour masks, one per contour object. A contour mask is a np array with length (num pts in contour) and type np.bool.
                        By default the masks contain all points.
        Return a list of Line objects corresponding to lines that appear in (masked parts of) the contours.
        '''

        self._image = image
        self._contours = contours
        self._contour_masks = contour_masks

        if DEBUG:
            print 'finding lines with the following contours:'
            image_copy = image.copy()
            cv2.drawContours(image_copy, [c.get_cv_contour() for c in contours], -1, (255,0,0), 1)
            cv2.imshow('contours', image_copy)
            cv2.waitKey(0)
            cv2.destroyAllWindows()
        
        if contour_masks is None:
            self._contour_masks = [np.ones(len(c.get_cv_contour()), np.bool) for c in self._contours]
            
        self._compute_histogram_bins()
        self._build_tangent_detector()
        
        #get a dictionary whose key/value pairs are ((theta, radius), WeightedTangentLines)
        weighted_tangent_lines_dict = self._compute_weighted_tangent_lines_dict()

        #this is a histogram corresponding to the theta bins and radius bins.
        #The value of a bin corresponds to the weight of the WeightedTangentLines object in that bin.
        tangent_line_histogram = self._compute_tangent_line_histogram(weighted_tangent_lines_dict)

        # print 'showing the histogram...'
        # p = plt.imshow(tangent_line_histogram, interpolation="nearest")
        # plt.colorbar(p)
        # plt.show()
    
        #connected_components_dict is a dictionary with key/value pairs
        #(component label (an integer), [list of (theta,radius) pairs] that are in this  component)
        connected_components_dict = self._compute_connected_components_dict(tangent_line_histogram,
                                                                            weighted_tangent_lines_dict)

        #remove the components that can not lie on a line
        self._remove_noisy_components(connected_components_dict, weighted_tangent_lines_dict)

        #compute a dictionary whose key value pairs are
        #(component label, Line object passing through all points in the component)
        #If we could not find a line, there is no entry for that component
        component_line_dict = self._fit_components_to_lines(connected_components_dict, weighted_tangent_lines_dict)

        #this is only used for debugging
        self._connected_components_dict = connected_components_dict
        #also update the weighted_tangent_lines dictionary so that it only contains weighted tangent lines that are on actual lines
        components_on_lines = component_line_dict.keys()
        theta_radius_pairs_on_lines = itertools.chain(*connected_components_dict.values())
        self._weighted_tangent_lines_dict = dict((k, weighted_tangent_lines_dict[k]) for k in theta_radius_pairs_on_lines)
        
        return component_line_dict.values()

    def get_disgarded_tangent_lines(self):
        '''
        Return a dictionary with key/values ((theta, radius), WeightedTangentLines object) that contains all tangent vectors that we did not manage to fit to lines.
        '''
        return self._disgarded_weighted_tangent_lines_dict

    def get_connected_components_dict(self):
        return self._connected_components_dict

    def get_tangent_line_centers(self):
        '''
        Return a numpy array with length (number of centers, 2). 
        This represents a list of centers the TangentLines that are part of a Line
        '''

        weighted_tangent_lines_list = itertools.chain(*self._weighted_tangent_lines_dict.itervalues())
        return np.vstack(list(itertools.chain(*([tangent_line.center for tangent_line in wtls.tangent_lines] for wtls in weighted_tangent_lines_list))))
    
if __name__ == '__main__':
    ld = LineDetector(0,0,0,0,0,0,0,0)
