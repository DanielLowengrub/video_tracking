import cv2
import numpy as np
from .tangent_detector import TangentDetector

class TangentToEllipseDetector(TangentDetector):
    '''
    This is an implementation of TangentDetector which is specialized for detecting tangents to ellipses.
    '''

    def __init__(self, ellipse_fitter, min_line_length, max_geometric_residue, min_ellipse_area, theta_bins, radius_bins):
        '''
        min_line_length - the minimum length that a tangent line is allowed to have.
        max_geometric_distance - the maximum distance that a tangent line is allowed to have to the contour points.
        min_ellipse_area - the minimum area of a fitted ellipse
        '''

        super(TangentToEllipseDetector, self).__init__(ellipse_fitter, min_line_length, max_geometric_residue, theta_bins, radius_bins)

        self._min_ellipse_area = min_ellipse_area
        self._debug_ellipse = False
        
    #FOR DEBUGGING ONLY!!!
    def _display_fitted_ellipse(self, points, ellipse, residue):
        ellipse.compute_geometric_parameters()
        print 'fit %d points to an ellipse with area %f. residue: %f.' % (len(points), ellipse.get_area(), residue)

        image = self._image.copy()
        radius = 2
        color = (255, 0, 0)
        thickness = -1

        for point in points:
            cv2.circle(image, tuple(np.int32(point).tolist()), radius, color, thickness=thickness)


        center_fitted, axes_fitted, angle_fitted = ellipse.get_geometric_parameters(in_degrees=True)
        color_fitted = (0,255,0)
        cv2.ellipse(image, tuple(np.int32(center_fitted).tolist()), tuple(np.int32(axes_fitted).tolist()), angle_fitted, 0, 360, color_fitted, 1, cv2.CV_AA)
        
        cv2.imshow('points and ellipse', image)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    def _fit_points_to_shape(self, points_in_interval):
        '''
        points_in_interval - a numpy array with shape (num points, 2)

        Return a PlanarShape object that fits these points. 
        If the geometric residue of the fit was too large, or if the area of the ellipse was too small, we return None.
        '''

        ellipse, algebraic_residue = self._shape_fitter.fit_points(points_in_interval)

        if (ellipse is None) or ellipse.is_degenerate():
            return None

        geometric_residue = ellipse.get_geometric_residue(points_in_interval)
        ellipse_area = ellipse.get_area()

        if self._debug_ellipse:
            self._display_fitted_ellipse(points_in_interval, ellipse, geometric_residue)
                
        if (geometric_residue > self._max_geometric_residue) or (ellipse_area < self._min_ellipse_area):
                #print 'no match. residue: %f, area: %f' % (geometric_residue, ellipse_area)
                return None
            
        #print 'got a match. residue: %f, area: %f' % (geometric_residue, ellipse_area)
        return ellipse
    
