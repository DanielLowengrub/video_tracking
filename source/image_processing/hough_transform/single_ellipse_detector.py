import numpy as np
import cv2
from ..point_on_contour_classifier.ellipse_classifier import EllipseClassifier
from ...auxillary.fit_points_to_shape.fit_points_to_ellipse_stable import FitPointsToEllipseStable
from ...auxillary.fit_points_to_shape.ransac_fit_points import RansacFitPoints
from ....testers.auxillary import primitive_drawing

DEBUG = True

class SingleEllipseDetector(object):
    '''
    This class is used to detect a single ellipse on a contour.
    If there is more than one ellipse, this does not return a match.

    This should be used when we only expect there to be one ellipse since it is much faster than the general ellipse detector.
    '''

    THETA_BIN_WIDTH = 2
    RADIUS_BIN_WIDTH = 5
    
    def __init__(self, min_tangent_to_line_arc_length, max_tangent_to_line_geometric_residue,
                 min_tangent_to_ellipse_arc_length, max_tangent_to_ellipse_geometric_residue, min_tangent_to_ellipse_area,
                 ransac_min_points_to_fit, ransac_num_iterations, ransac_distance_to_shape_threshold,
                 ransac_min_percentage_in_shape, max_average_ellipse_residue):
        '''
        min_tangent_to_line_arc_length - the minimum arc length that is needed to determine a tangent vector to a line
        max_tangent_to_line_geometric_residue - the minimum geometric residue that is allowed when fitting tangent vectors to lines

        min_tangent_to_ellipse_arc_length - the minimum arc length that is needed to determine a tangent vector to an ellipse
        max_tangent_to_ellipse_geometric_residue - the minimum geometric residue that is allowed when fitting tangent vectors to ellipses
        min_tangent_to_ellipse_area - we only use ellipses with at least this area to compute tangent lines to an ellipse.
        
        max_ellipse_residue - the maximum allowable residue for a set of points to fit an ellipse
        '''
        
        self._max_average_ellipse_residue = max_average_ellipse_residue

        ellipse_fitter = FitPointsToEllipseStable()
        self._ransac_ellipse_fitter = RansacFitPoints(ellipse_fitter, ransac_min_points_to_fit, ransac_num_iterations,
                                                      ransac_distance_to_shape_threshold, ransac_min_percentage_in_shape)
    
        self._point_on_ellipse_classifier = EllipseClassifier(min_tangent_to_line_arc_length,
                                                              max_tangent_to_line_geometric_residue,
                                                              min_tangent_to_ellipse_arc_length,
                                                              max_tangent_to_ellipse_geometric_residue,
                                                              min_tangent_to_ellipse_area,
                                                              self.THETA_BIN_WIDTH, self.RADIUS_BIN_WIDTH)

    def find_ellipse(self, contours, contour_masks=None):
        '''
        contours - a list of contours
        contour_masks - a list of numpy arrays with length (number of points in i-th contour) and type np.bool

        Return an Ellipse object
        '''
        if contour_masks == None:
            contour_masks = [None for contour in contours]
            
        #construct a numpy array containing all points that are on an ellipse
        point_on_ellipse_masks = (self._point_on_ellipse_classifier.classify_points(contour, contour_mask)
                                  for contour, contour_mask in zip(contours, contour_masks))

        #TEST: We try fitting points before deciding which points are on an ellipse
        #the point is that the current "point on ellipse" detector disgards points near the ends of the shape.
        #a better solution would be to just wait until the NN classifier is finished
        #point_on_ellipse_masks = (np.ones(len(contour.get_cv_contour()), np.bool) for contour in contours)
        points_on_ellipse = np.vstack([contour.get_cv_contour().reshape((-1,2))[point_mask] for contour,point_mask in zip(contours, point_on_ellipse_masks)])

        if DEBUG and len(points_on_ellipse) > 20:
            print 'fitting an ellipse to the following points:'
            image = np.zeros((352,624,3), np.uint8)
            primitive_drawing.draw_points(image, points_on_ellipse, (255,0,0))
            cv2.imshow('image with points', image)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

        fitted_ellipse, total_geometric_residue, inlier_mask = self._ransac_ellipse_fitter.fit_points(points_on_ellipse)

        if fitted_ellipse is None:
            return None

        #the average ellipse residue in the total residue, divided by the number of points that were determined to be part of the ellipse
        average_ellipse_residue = total_geometric_residue / sum(inlier_mask)
        if average_ellipse_residue > self._max_average_ellipse_residue:
            return None

        return fitted_ellipse
