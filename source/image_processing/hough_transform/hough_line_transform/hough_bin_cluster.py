import collections
import numpy as np
from ....auxillary import planar_geometry
from ....auxillary.fit_points_to_shape.line import Line

class HoughBin(object):
    def __init__(self, theta_bin, radius_bin, theta, radius, count):
        self._theta_bin = theta_bin
        self._radius_bin = radius_bin
        self._theta = theta
        self._radius = radius
        self._count = count

    def __hash__(self):
        return hash((int(self._theta_bin), int(self._radius_bin)))
    
    @property
    def theta_bin(self):
        return self._theta_bin

    @property
    def radius_bin(self):
        return self._radius_bin

    @property
    def theta(self):
        return self._theta

    @property
    def radius(self):
        return self._radius

    @property
    def count(self):
        return self._count
        

class HoughBinCluster(object):
    '''
    This represents a cluster of bins in a hough line transform.
    '''

    def __init__(self, center, hough_bins):
        '''
        center - a numpy array with shape (2,) and type np.float64. It represents the origin of the coordinate system that
           was used to calculate the transform
        hough_bins - a list of HoughBin objects
        '''
        self._center = center
        self._hough_bins = hough_bins
        self._total_count = sum(hb.count for hb in hough_bins)

        theta_bins = np.array([hb.theta_bin for hb in hough_bins])
        radius_bins = np.array([hb.radius_bin for hb in hough_bins])
        self._theta_radius_bins = np.vstack([theta_bins, radius_bins]).transpose()
        
        #when calculating averages, weigh each bin by its count
        weights = np.array([hb.count for hb in hough_bins])
        theta_values  = np.array([hb.theta for hb in hough_bins])
        radius_values = np.array([hb.radius for hb in hough_bins])

        self._theta_value_mean  = np.average(theta_values, weights=weights)
        self._radius_value_mean = np.average(radius_values,  weights=weights)
        self._theta_value_std  = np.sqrt(np.average((theta_values - self._theta_value_mean)**2, weights=weights))
        self._radius_value_std = np.sqrt(np.average((radius_values - self._radius_value_mean)**2, weights=weights))


        #this is a translation matrix from the coordinate system whose origin is "center", to the coordinate system whose
        #origin is (0,0)
        H = planar_geometry.translation_matrix(center)
        self._lines = [planar_geometry.apply_homography_to_line(H, Line(np.cos(theta), np.sin(theta), -radius))
                       for theta,radius in zip(theta_values, radius_values)]

        self._mean_line = Line(np.cos(self._theta_value_mean), np.sin(self._theta_value_mean), -self._radius_value_mean)
        self._mean_line = planar_geometry.apply_homography_to_line(H, self._mean_line)

    def __repr__(self):
        params = (len(self._hough_bins), self._total_count, self._theta_value_mean * 180/np.pi, self._radius_value_mean)
        return '[HoughLineCluster] num_bins=%d, total_count=%d, theta=%f, r=%f)' % params

    @classmethod
    def merge_clusters(cls, hough_bin_clusters):
        '''
        hough_bin_clusters - a list of HoughBinClusters
        return - a HoughBinCluster
        '''
        center = hough_bin_clusters[0]._center
        hough_bins_union = list(set.union(*[set(hbc._hough_bins) for hbc in hough_bin_clusters]))
        return cls(center, hough_bins_union)
    
    def get_total_count(self):
        '''
        return the sum over the counts of all bins in the cluster
        '''
        return self._total_count

    def get_theta_bin_mean(self):
        '''
        return the mean theta bin index in the cluster
        '''
        return self._theta_bin_mean

    def get_radius_bin_mean(self):
        '''
        return the mean radius bin index in the cluster
        '''
        return self._radius_bin_mean

    def get_theta_value_std(self):
        '''
        return the std of the theta values in the cluster
        '''
        return self._theta_value_std

    def get_radius_value_std(self):
        '''
        return the std of the radius values in the cluster
        '''
        return self._radius_value_std

    def get_theta_value_mean(self):
        '''
        return the mean theta value in the cluster
        '''
        return self._theta_value_mean

    def get_radius_value_mean(self):
        '''
        return the mean of the radius values indices in the cluster
        '''
        return self._radius_value_mean

    def get_lines(self):
        '''
        return - a list of Line object. There is one line per bin in the cluster.
        '''
        return self._lines

    def get_mean_line(self):
        '''
        return - a Line object. It is average line. The average is taken over the bins in the cluster.
        '''
        return self._mean_line

    def get_theta_radius_bins(self):
        '''
        return - a numpy array with shape (num bins, 2)
        '''
        return self._theta_radius_bins
