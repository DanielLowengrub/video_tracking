import itertools
import numpy as np
import cv2
from scipy import ndimage, sparse, spatial
from .hough_bin_cluster import HoughBin, HoughBinCluster
from ....auxillary.rectangle import Rectangle

class HoughLineTransform(object):
    '''
    This class is in charge of generating hough line transforms of binary 2D arrays.
    It also contains methods for using a hough transform to create clusters of lines.

    The hough transform works as follows. 
    First set up a number of "theta bins", which correspond to angles -pi < theta <= pi. Also set up a number of "radius bins"
    which represent radii: 0 <= r < max_radius.

    Now, let mask be a binary numpy array with shape (n,m). We create a 2D histogram whose bins represent pairs of theta bins
    and radius bins. To each point (x0,y0) for which mask[y,x]=1, we add a count to each bin (theta,r) for which:
    r = x0*cos(theta) + y0*sin(theta)
    Each such pair (theta,r) represents a line L passing through (x0,y0), which is defined by the equation:
    cos(theta)*x + sin(theta)*y = r

    The points in the histogram with high values ("hough peaks") correspond to lines that pass through many points.
    To find the lines, we pick out the peaks, and cluster them into groups of nearby peaks.
    '''

    def __init__(self, num_theta_bins, num_radius_bins, mask_shape, peak_threshold, peak_threshold_ratio, clustering_radius):

        '''
        num_theta_bins - the number of bins that will be used to digitize the interval [-pi,pi)
        num_radius_bins - the number of bins that will be used to digitize the interval [0, max_radius].
           max_radius is determined by the mask shape
        mask_shape     - the shape of the binary arrays that we will be computing hough transforms of.
        peak_threshold - if a bin in the hough transform has at least this many counts, then use it to create a line.
        peak_threshold_ratio - if a bin B passes the threshold and is a peak, then also include bins whose count is larger than
           this fraction B's count
        clustering_radius - two hough peaks are considered to be neighbors if their distance is less than or equal to this
        '''

        #this is a numpy array with length num_theta_bins.
        #the i-th bin is [theta_bin_endpoints[i], theta_bin_endpoints[i+1]).
        #the last bin is [theta_bin_endpoints[-1], pi)
        self._theta_bin_endpoints, self._padded_theta_bin_endpoints = self._build_theta_bin_endpoints(num_theta_bins,
                                                                                                      clustering_radius)
        #self._padded_theta_bin_endpoints = self._build_padded_theta_bin_endpoints(num_theta
        #this is a numpy array with length num_theta_bins and type np.int32. The i-th entry is equal to i.
        #this is the index of the bin that stores the i-th theta value
        self._theta_bin_indices  = np.arange(num_theta_bins)
        
        #this is the center of the coordinate system we will use when computing (theta,r) coordinates
        self._center = (np.array([mask_shape[1], mask_shape[0]], np.float32) / 2.0)
        max_radius = np.linalg.norm(self._center)

        #this is a numpy array with length num_radius_bins
        #the i-th bin is [radius_bin_endpoints[i], radius_bin_endpoints[i+1]).
        #the last bin is [radius_bin_endpoints[-1], max_radius)
        self._radius_bin_endpoints, self._padded_radius_bin_endpoints = self._build_radius_bin_endpoints(num_radius_bins,
                                                                                                         max_radius,
                                                                                                         clustering_radius)
        # print 'padded radius bin endpoints: ', self._padded_radius_bin_endpoints
        
        self._histogram_shape = (num_theta_bins, num_radius_bins)
        self._peak_threshold = peak_threshold
        self._peak_threshold_ratio = peak_threshold_ratio
        
        #these are precomputed values that will be used to compute hough transforms
        self._cos_theta = np.cos(self._theta_bin_endpoints) #The i-th entry is equal to cos(the left endpoint of bin i)
        self._sin_theta = np.sin(self._theta_bin_endpoints) #The i-th entry is equal to sin(the left endpoint of bin i)

        #these will be used to cluster the hough bins
        #self._peak_dilation_kernel = np.ones((2*clustering_radius + 1, 2*clustering_radius + 1), np.bool)
        #self._peak_structure_element = np.ones((3,3), np.bool)
        self._clustering_radius = clustering_radius
        self._peak_footprint = self._build_peak_footprint(clustering_radius)
        self._relative_peak_center = np.array([clustering_radius, clustering_radius])

        #this mask indicates a portion of a padded hough transform which represents each theta,radius value exactly once.
        self._unique_value_mask = self._build_unique_value_mask(num_theta_bins, num_radius_bins, clustering_radius)

            
    @classmethod
    def _build_theta_bin_endpoints(cls, num_theta_bins, clustering_radius):
        '''
        num_theta_bins - an integer
        return - a numpy array theta_bin_endpoints
        
        Recall the the theta bins divide the interval [-pi,pi) into num_theta_bins equal intervals.
        The i-th element of the output array theta_bin_endpoints is equal to the left endpoint of the i-th theta bin. 
        In particular, the 0-th element is always -pi.

        For example, if num_theta_bins=3, we return an array [t0=-pi,t1,t2]. The 3 bins are:
        [t0=-pi,t1), [t1,t2), [t2, pi)
        '''
        #create num_theta_bins+1 values *including* pi, and then remove the last value (which is equal to pi)
        endpoints = np.linspace(-np.pi, np.pi, num_theta_bins+1)[:-1]

        bin_width = endpoints[1] - endpoints[0]
        left_padding = np.array([(endpoints[0] - i*bin_width) for i in range(clustering_radius,0,-1)])
        right_padding = np.array([(endpoints[-1] + i*bin_width) for i in range(1,clustering_radius+1)])
        padded_endpoints = np.hstack([left_padding, endpoints, right_padding])
        
        return endpoints, padded_endpoints

    @classmethod
    def _build_radius_bin_endpoints(cls, num_radius_bins, max_radius, clustering_radius):
        '''
        num_radius_bins - an integer
        max_radius - a float
        return - a numpy array with length num_radius_bins
        
        Recall the the radius bins divide the interval [0,max_radius) into num_radius_bins equal intervals.
        The i-th element of the output array is equal to the left endpoint of the i-th radius bin. In particular,
        the 0-th element is always 0.

        For example, if num_theta_bins=3, we return an array [t0=-pi,t1,t2]. The 3 bins are:
        [t0=-pi,t1), [t1,t2), [t2, max_radius)
        '''
        #create num_radius_bins+1 values *including* max_radius, and then remove the last value (which is equal to max_radius)
        endpoints = np.linspace(0, max_radius, num_radius_bins+1)[:-1]

        bin_width = endpoints[1] - endpoints[0]
        left_padding = np.array([(endpoints[0] - i*bin_width) for i in range(clustering_radius,0,-1)])
        padded_endpoints = np.hstack([left_padding, endpoints])
        
        return endpoints, padded_endpoints

    @classmethod
    def _build_peak_footprint(cls, clustering_radius):
        '''
        clustering_radius - a float. 
        return - a numpy array with shape (2*clustering_radius + 1, 2*clustering_radius + 1)
           containing a circle with radius clustering_radius
        '''
        x_coords = np.arange(-clustering_radius,clustering_radius+1,1)
        y_coords = np.arange(-clustering_radius,clustering_radius+1,1)
        XX,YY = np.meshgrid(x_coords, y_coords)
        footprint = (XX**2 + YY**2) < clustering_radius**2
        return footprint

    @classmethod
    def _build_unique_value_mask(cls, num_theta_bins, num_radius_bins, clustering_radius):
        '''
        num_theta_bins - an integer
        num_radius_bins - an integer
        clustering_radius - an integer

        return a numpy array with shape (2*clustering_radius + num_theta_bins, num_radius_bins) and type np.bool

        The shape of the hough transform is (num_theta_bins, num_radius_bins). Since we want the theta values to wrap around,
        in practice we pad the transform along the theta axis with "clustering_radius" indices on each side.

        The output array is a mask, with the property that every theta/radius *value* corresponds to a unique bin in the mask
        '''
        unique_value_mask = np.ones((num_theta_bins + 2*clustering_radius, num_radius_bins + clustering_radius), np.bool)

        #all of the bins with: theta_bin < clustering_radius or theta_bin > cluster_radius+num_theta_bins represent values that
        #are contained in other bins
        unique_value_mask[:clustering_radius,:] = False
        unique_value_mask[num_theta_bins + clustering_radius:,:] = False

        #all of the bins with radius_bin < 2*clustering_radius and theta_bin >= clustering_radius + num_theta_bins/2 represent
        #values that are contained in other bins
        unique_value_mask[:,:clustering_radius] = False
        
        return unique_value_mask

    def _in_unique_bin_region(self, theta_radius_bin):
        '''
        theta_radius_bin - a tuple with length 2

        return - True iff the  bin is in the "unique bin region". This is a subset S of the padded hough transform
           such that each theta/radius value corresponds to exactly one bin in S
        '''
        theta_start = self._clustering_radius
        theta_stop  = len(self._theta_bin_indices) + self._clustering_radius
        radius_start = self._clustering_radius
        
        return (theta_start <= theta_radius_bin[0] < theta_stop) and (radius_start <= theta_radius_bin[1])

    def _in_unique_theta_mod_shift_region(self, theta_radius_bin):
        '''
        theta_radius_bin - a tuple with length 2

        return - True iff the hough bin is in the "unique bin region". This is a subset S of the padded hough transform
           such that each theta/radius value, where theta is taken mod pi, corresponds to exactly one bin in S
        '''
        #theta_start = self._clustering_radius
        theta_zero_bin  = len(self._theta_bin_indices)/2 + self._clustering_radius
        
        return (theta_radius_bin[0] < theta_zero_bin)

    def _padded_bins_to_values(self, padded_bins):
        '''
        padded_bins - a numpy array with shape (n,2) and type np.int32. It represents a list of tuples (theta_bin,radius_bin)
        return - a numpy array with shape (n,2) and type np.float64. The i-th entry is equal to theta/radius value
           corresponding to the i-th bin in the padded transfrom

        note that it is possible for the radius values to be negative.
        '''
        theta_values = self._padded_theta_bin_endpoints[padded_bins[:,0]]
        radius_values = self._padded_radius_bin_endpoints[padded_bins[:,1]]
        values = np.vstack([theta_values, radius_values]).transpose()
        return values

    def _radius_values_to_bins(self, radius_values):
        '''
        radius_values - a numpy array with shape (n,) and type np.float64. It represents a list of radii.
           In particular, all values must be greater or equal to 0.
        return - a numpy array with shape (n,) and type np.int32. The i-th entry is equal to the index of the bin which
           contains radius_values[i].
        '''
        #we subtract 1 because the digitize function consider bin zero to be the bin (-infty, 0]. But our first bin
        #is [0,max_radius/num_radius_bins]
        radius_bins = np.digitize(radius_values, self._radius_bin_endpoints) - 1
        return radius_bins
    
    def get_histogram_shape(self):
        return self._histogram_shape

    def build_hough_line_transform(self, mask):
        '''
        mask - a numpy array with shape (n,m) and type np.bool
        
        return - a numpy array with shape get_histogram_shape() and type np.int32
        
        to each point (x,y) in the mask, we add one to the bins of the tuples (theta,r) which satisfy:
        r = x*cos(theta) + y*sin(theta)
        '''
        #initialize an 2D accumulator
        theta_radius_hist = np.zeros(self.get_histogram_shape(), np.int32)

        #get the (x,y) coordinates of the points in the mask, in the coordinate system with origin _center
        y_coords, x_coords = np.where(mask)
        points = np.vstack([x_coords, y_coords]).transpose()
        points = points - self._center
    
        for x,y in points:
            #for each endpoint of a theta bin "theta", compute the radius r which satisfies: r = x*cos(theta) + y*sin(theta)
            radius = x*self._cos_theta + y*self._sin_theta
            #compute the indices of the bins containing each of these radii
            radius_bins = self._radius_values_to_bins(radius)
        
            theta_radius_bins = np.vstack([self._theta_bin_indices, radius_bins]).transpose()
            #remove the bins for which the radius is negative
            theta_radius_bins = theta_radius_bins[radius >= 0]

            #add one to the accumulator
            theta_radius_hist[theta_radius_bins[:,0], theta_radius_bins[:,1]] += 1

        return theta_radius_hist

    def _build_padded_hough_transform(self, hough_transform):
        '''
        hough_transform - a numpy array with shape (n,m) and type np.int32
        
        The point of the padding is that we want to put bins (-pi, r) near bins (pi,r). So we add bins corresponding to
        (-pi - epsilon, r), with the same value as the bin (pi-epsilon, r). Similarly, we add bins corresponding to
        (pi+epsilon,r) with the same value as the bin corresponding to (-pi+epsilon, r).

        Also, for small r, we would like the bins (theta,r) to be near the bins (theta +- pi, r). This is because a vector can
        move on a line through the origin, and have the angle flip by +- pi radians.
        So we add bins corresponding to (theta+pi,-epsilon), with the same value as the bin corresponding to (theta,epsilon).

        return - a numpy array with shape (n + 2*_cluster_radius, m + _cluster_radius) and type np.int32
              we add _cluster_radius columns on the right and left of the input array, and _cluster_radius rows to the top. 
              The values in the theta axis direction are determined by wrapping.
              In the radius direction, we reflect the first _cluster radius rows, and shift by num_theta_values/2 (i.e, a bin
              size corresponding to 180 degrees).
        '''
        #pad the beginning of the r axis in reflection mode
        padded_transform = np.pad(hough_transform, ((0,0),(self._clustering_radius,0)), mode='symmetric')

        #break the padding into two equal pieces and swap them. This corresponds by a shift of 180 degrees
        theta_zero_index = hough_transform.shape[0] / 2
        reflected_values_to_zero = padded_transform[:theta_zero_index,:self._clustering_radius]
        reflected_values_from_zero = padded_transform[theta_zero_index:,:self._clustering_radius]
        temp = reflected_values_to_zero.copy()
        reflected_values_to_zero[...] = reflected_values_from_zero
        reflected_values_from_zero[...] = temp
        
        #pad both sides of the theta axis in wrap mode
        padded_transform = np.pad(padded_transform, ((self._clustering_radius, self._clustering_radius), (0,0)), mode='wrap')
        return padded_transform

    def _find_padded_bins_in_cluster(self, padded_peak, padded_transform):
        '''
        padded_peak - a numpy array with length 2. It represents a bin (theta_bin, radius_bin)
        padded_transform - a padded hough line transform

        return a numpy array with shape (n,2). 
        It represents the bins in the padded transform that are in the cluster whose center is the padded_peak.
        '''
        #print 'finding padded bins in the cluster with peak: ', padded_peak

        peak_value = padded_transform[padded_peak[0], padded_peak[1]]
        peak_threshold = self._peak_threshold_ratio * peak_value

        #build a Rectangle whose center is the peak.
        #Note that the Rectangle object expects a point in a 2D array (x,y) to correspond to the entry array[y,x]. I.e,
        #the x axis is the horizontal axis and the y axis is the vertical one. So we have to exchange the entries in the peak
        peak_center = np.array([padded_peak[1], padded_peak[0]])
        top_left = peak_center - self._relative_peak_center
        peak_nbd_rect = Rectangle.from_top_left_and_shape(top_left, self._peak_footprint.shape)

        #crop the peak rectangle so that is is inside the padded transform array
        padded_transform_rect = Rectangle.from_array(padded_transform)
        cropped_peak_nbd_rect = padded_transform_rect.intersection(peak_nbd_rect)

        #crop the footprint and use it to find the points in the cropped rectangle that should be included in the cluster
        cropped_footprint = peak_nbd_rect.get_relative_slice(self._peak_footprint, cropped_peak_nbd_rect)
        cropped_peak_nbd_mask = np.logical_and(cropped_peak_nbd_rect.get_slice(padded_transform) > peak_threshold,
                                               cropped_footprint)
        
        #the points in the cluster in the cropped rect coordinates
        rel_theta_bins, rel_radius_bins = np.where(cropped_peak_nbd_mask)

        #the points in the cluster in the coordinates of the padded hough transform
        padded_theta_bins  = rel_theta_bins  + cropped_peak_nbd_rect.get_top_left()[1]
        padded_radius_bins = rel_radius_bins + cropped_peak_nbd_rect.get_top_left()[0]
        padded_bins = np.vstack([padded_theta_bins, padded_radius_bins]).transpose()

        return padded_bins

    def _build_cluster(self, padded_peak, padded_transform):
        '''
        padded_peak - a numpy array with lengh 2
        padded_transform - a numpy array with shape (n,m) and type np.int32.

        return - a HoughBinCluster object.

        The padded_peak represents a bin (theta_bin, radius_bin) in the padded transform. 
        The padded transform is a padded version of the hough transform
        '''
        # print 'building cluster with padded peak: ', padded_peak
        
        #find the padded bins in this cluster
        padded_bins = self._find_padded_bins_in_cluster(padded_peak, padded_transform)
        padded_values = self._padded_bins_to_values(padded_bins)

        # print '   padded bins and values:\n', np.hstack([padded_bins, padded_values * np.array([[180/np.pi, 1]])])
        counts = padded_transform[padded_bins[:,0], padded_bins[:,1]]
        # print '   bin counts: (min=%d, max=%d):\n' % (counts.min(), counts.max()), counts
        
        hough_bins = [HoughBin(*args) for args in zip(padded_bins[:,0], padded_bins[:,1],
                                                      padded_values[:,0], padded_values[:,1], counts)]
        hough_bin_cluster = HoughBinCluster(self._center, hough_bins)

        return hough_bin_cluster

    def _bin_clusters_in_same_component(self, bin_cluster_0, bin_cluster_1):
        '''
        bin_cluster_0, bin_cluster_1 - HoughBinCluster objects
        return True if the bin clusters should be in the same "connected component". The connectivity is determined by how
        close the bins in the clusters are to one another.
        '''
        #get the min pairwise distance between a point in cluster 0 and a point in cluster 1
        pairwise_distances = spatial.distance.cdist(bin_cluster_0.get_theta_radius_bins(),
                                                    bin_cluster_1.get_theta_radius_bins())
        cluster_distance = np.min(pairwise_distances)
        # print '      cluster 0 bin values:'
        # print bin_cluster_0.get_theta_radius_bins()
        # print '      cluster 1 bin values:'
        # print bin_cluster_1.get_theta_radius_bins()

        # print '      pairwise distances:\n', pairwise_distances
        # print '      cluster distance: ', cluster_distance
        
        #put the bins in the same component if the min distance is less than half the clustering radius
        return cluster_distance < self._clustering_radius / 2.0
    
    def _find_bin_cluster_components(self, bin_cluster_dict):
        '''
        bin_clusters - a dictionary of HoughBinCluster objects
        return - a list of dictionaries of HoughBinCluster objects
        Each of the dictionaries is a sub dictionary of bin_cluster_dict, and represents a connected component of bin clusters.
        Two clusters are in the same component if their bins are close to one another.
        '''
        #print 'finding bin cluster components...'

        #this is a graph whose vertices are bin clusters.
        #There is an edge between two vertices if the elements of the cluster
        #are close to one another
        bin_cluster_keys = bin_cluster_dict.keys()
        bin_clusters = [bin_cluster_dict[k] for k in bin_cluster_keys]
        
        G_dense = np.zeros((len(bin_clusters), len(bin_clusters)), np.bool)
        for (i, bin_cluster_i), (j, bin_cluster_j) in itertools.combinations(enumerate(bin_clusters), 2):
            #print '   comparing clusters %s and %s' % (bin_cluster_keys[i], bin_cluster_keys[j])
            if self._bin_clusters_in_same_component(bin_cluster_i, bin_cluster_j):
                #print '      the clusters are in the same component'
                G_dense[i,j] = G_dense[j,i] = True

        #find the connected components of the graph
        n_components, labels = sparse.csgraph.connected_components(G_dense)

        #store a list of bin clusters for each of the components
        bin_cluster_components = [dict() for i in range(n_components)]
        for label,k,bin_cluster in zip(labels, bin_cluster_keys, bin_clusters):
            #print '   adding the cluster %s to component %d' % (bin_cluster, label)
            bin_cluster_components[label][k] = bin_cluster

        return bin_cluster_components

    def _merge_bin_cluster_components(self, bin_cluster_components):
        '''
        bin_cluster_components - a list of dictionaries of HoughBinCluster objects
        return - a list of HoughBinCluster objects. Each of the output clusters is obtained by merging one of the input 
           components.
        '''
        #print 'merging bin cluster components'
        
        bin_clusters = []
        for bin_cluster_component in bin_cluster_components:
            peaks = bin_cluster_component.keys()
            # print '   - processing component with peaks: ', peaks
            peak_in_unique_bin_region = map(self._in_unique_bin_region, peaks)
            peak_in_unique_theta_mod_shift_region = map(self._in_unique_theta_mod_shift_region, peaks)

            # print '      peak_in_unique_bin_region:             ', peak_in_unique_bin_region
            # print '      peak_in_unique_theta_mod_shift_region: ', peak_in_unique_theta_mod_shift_region
            #if none of the cluster peaks are in the unique portion, then throw out this component.
            if not any(peak_in_unique_bin_region):
                # print '      none of the peaks are in the unique bin region. disgarding component'
                pass
            
            #if all of the clusters peaks are in the unique portion of the padded transform, merge all of the clusters
            #and add the result to the output.
            #alternatively, if some of the peaks are in the unique portion and some are not,
            #then as a tie breaker, check if all of the bins are in the unique part of the "theta mod shift" region.
            #In practice this means that the theta bins correspond to values in the range [-pi,0)
            elif all(peak_in_unique_bin_region) or all(peak_in_unique_theta_mod_shift_region):
                # print '      merging component'   
                bin_clusters.append(HoughBinCluster.merge_clusters(bin_cluster_component.values()))

            else:
                # print '      disgarding component'
                pass
            
        return bin_clusters
                
    def find_hough_bin_clusters(self, hough_line_transform):
        '''
        hough_line_transform - a numpy array with shape get_histogram_shape() and type np.int32
           It should be the output of build_hough_line_transform
        return - a list of HoughBinCluster objects. Each one represents a cluster of bins in the hough line transform.
           Each cluster should represent a single line in the image.
        '''
        #pad the transform so that bins that correspond to nearby lines are close to one another. For example, (-pi,r) and
        #(pi,r) should be close, so we pad the angle axis on both side.
        padded_transform = self._build_padded_hough_transform(hough_line_transform)
    
        # pt_copy = np.float32(padded_transform) / padded_transform.max()
        # pt_copy[:,self._clustering_radius] = 1.0
        # pt_copy[self._clustering_radius,:] = 1.0
        # pt_copy[hough_line_transform.shape[0]+self._clustering_radius,:] = 1.0
        # cv2.imshow('padded hough transform', pt_copy.transpose())
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        #the peaks are defined to be the points whose value is equal to the maximum value in their nbd.
        #in order to prevent duplicates, only take points that are not in the padding
        padded_transform_max = ndimage.filters.maximum_filter(padded_transform, footprint=self._peak_footprint)

        # cv2.imshow('padded transform max', np.float32(padded_transform_max.transpose()) / padded_transform_max.max())
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        # cv2.imshow('unique value mask', np.float32(self._unique_value_mask.transpose()))
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        padded_peak_mask = np.logical_and.reduce([padded_transform_max == padded_transform,
                                                  padded_transform > self._peak_threshold])
                                                  #self._unique_value_mask])
        
        # cv2.imshow('padded hough peaks', np.float32(padded_peak_mask.transpose()))
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        #get the coordinates of the peaks in the padded transform coordinate system
        padded_peak_theta, padded_peak_radius = np.where(padded_peak_mask)
        padded_peaks = np.vstack([padded_peak_theta, padded_peak_radius]).transpose()

        #use each peak to create a cluster of bins concentrated at the peak
        bin_clusters = [self._build_cluster(padded_peak, padded_transform) for padded_peak in padded_peaks]
        bin_cluster_dict = dict((tuple(padded_peak.tolist()), bin_cluster)
                                for padded_peak,bin_cluster in zip(padded_peaks, bin_clusters))

        # print 'bin cluster dictionary:'
        # print '\n'.join(['%s: %s' % (str(k),str(v)) for k,v in bin_cluster_dict.iteritems()])

        if len(bin_cluster_dict) == 0:
            return []
        
        #put two bin clusters in the same component if they are close to one another
        bin_cluster_components = self._find_bin_cluster_components(bin_cluster_dict)

        # print 'cluster components:'
        # print '\n'.join([str(component.keys()) for component in bin_cluster_components])
        
        #merge the bin clusters in each component
        bin_clusters = self._merge_bin_cluster_components(bin_cluster_components)
        
        return bin_clusters
        
    #OLD VERSION - This method finds peaks with a global threshold
    # def find_hough_bin_clusters(self, hough_line_transform):
    #     '''
    #     hough_line_transform - a numpy array with shape get_histogram_shape() and type np.int32
    #        It should be the output of build_hough_line_transform
    #     return - a list of HoughBinCluster objects. Each one represents a cluster of bins in the hough line transform.
    #        Each cluster should represent a single line in the image.
    #     '''

    #     #this is a mask indicating which of the bins are above the threshold
    #     hough_peak_mask = hough_line_transform > self._peak_threshold

    #     #dilate the hough_peak_mask so that bins that are within neighbor_radius are in the same cluster
    #     dilated_mask = ndimage.morphology.binary_dilation(hough_peak_mask, self._peak_dilation_kernel)

    #     #find connected components in the dilated hough mask
    #     #labels is a numpy array with the same shape as hough_mask. It has value 0 where the mask is zero, and values:
    #     #[1,...,num_labels] at each of the different labels
    #     labels, num_labels = ndimage.measurements.label(dilated_mask, self._peak_structure_element)
    #     labels *= np.uint64(hough_peak_mask)
    
    #     hough_bin_clusters = []
    #     for bin_label in range(1,num_labels+1):
    #         theta_bins, radius_bins = np.where(labels == bin_label)
    #         theta_values  = self._theta_bins_to_values(theta_bins)
    #         radius_values = self._radius_bins_to_values(radius_bins)
    #         counts = hough_line_transform[theta_bins, radius_bins]

    #         hough_bins = [HoughBin(*args) for args in zip(theta_bins, radius_bins, theta_values, radius_values, counts)]
    #         hough_bin_cluster = HoughBinCluster(self._center, hough_bins)
    #         hough_bin_clusters.append(hough_bin_cluster)

    #     return hough_bin_clusters





