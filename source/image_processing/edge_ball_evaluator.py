import numpy as np
from ..auxillary import planar_geometry, world_to_image_projections
from ..auxillary.rectangle import Rectangle
from ..auxillary.rectangles import Rectangles

class EdgeBallEvaluator(object):
    '''
    This class is used to evaluate how likely it is that a given point in world coordinates is the center of a ball.
    To evaluate the position, it is given an image and the camera that produced it.

    The evaluation is determined as follows:
    1) Extract a patch around the center of the image of the ball.
    2) Divide the patch into the ball region and the grass region
    3) Use a GMM for compute the log prob of the ball region. Same for the grass.
    4) return the total log prob
    '''

    PATCH_SIZE = 6    #To evaluate a ball position, we consider a patch whose center is the ball,
                      #and which extends by PATCH_SIZE in each direction.

    INSIDE_MAX = 1.5
    OUTSIDE_MIN = 4
    OUTSIDE_MAX = 10
    OUTSIDE_WEIGHT = 3
    
    def _generate_radius_sq_map(self, ellipse):
        '''
        ellipse - a numpy array with shape (3,3) representing an ellipse
        
        return a tupe (rectangle, radius_sq_map) where
        radius_sq_map - numpy array with shape (2*PATCH_SIZE + 1, 2*PATCH_SIZE + 1) and type int32.
                        the value of the entry (y,x) represents the value of the normalized ellipse on the point 
                        center_of_ellipse+(x,y).
        rectangle - a Rectangle object defining where the radius_sq_map is located on the image. I.e, the center of the
                    rectangle is the center of the ellipse, and it's size is the same size as the radius_sq_map
        '''
        ellipse_center, M, b = planar_geometry.compute_normalized_ellipse(ellipse)
        rectangle_center = np.rint(ellipse_center).astype(np.int32)
        center_offset = rectangle_center - ellipse_center

        # print 'ellipse center: ', ellipse_center
        print 'integer ellipse center: ', rectangle_center
        # print 'center offset: ', center_offset
        
        x = np.arange(-self.PATCH_SIZE, self.PATCH_SIZE+1,1) + center_offset[0]
        y = np.arange(-self.PATCH_SIZE, self.PATCH_SIZE+1,1) + center_offset[1]

        XX, YY = np.meshgrid(x,y)
        ellipse_values = XX*XX*M[0,0] + 2*XX*YY*M[0,1] + YY*YY*M[1,1]

        top_left = rectangle_center - np.array([self.PATCH_SIZE, self.PATCH_SIZE])
        rectangle = Rectangle.from_top_left_and_shape(top_left, ellipse_values.shape)

        return rectangle, ellipse_values
    
    def compute_radius_sq_masks(self, camera_matrix, ball_radius, ball_position):
        '''
        camera_matrix - a CameraMatrix object
        ball_radius - a float
        ball_position - a numpy array with length 3.

        return - a tuple (rectangle, radius_sq_masks)
        rectangle - a Rectangle object
        radius_sq_masks - a pair of masks (inside mask, outside mask). Both masks have the same shape as the rectangles.

        If one of the masks is empty, return None
        '''
        ball_position = ball_position.reshape((1,3))
        ball_ellipse = world_to_image_projections.project_spheres_to_image(camera_matrix, ball_radius, ball_position)[0]
        rectangle, radius_sq_map = self._generate_radius_sq_map(ball_ellipse)

        # print 'ball ellipse:'
        # print ball_ellipse
        
        # print 'radius sq map:'
        # print radius_sq_map.astype(np.int32)
        
        inside_mask = radius_sq_map <= self.INSIDE_MAX
        outside_mask = np.logical_and(self.OUTSIDE_MIN <= radius_sq_map, radius_sq_map <= self.OUTSIDE_MAX)

        radius_sq_masks = (inside_mask, outside_mask)
        
        #make sure that the masks are not empty
        if all(mask.any() for mask in radius_sq_masks):
            return rectangle, radius_sq_masks

        #if one of them is empty, then the radius masks are declared to be degenerate
        else:
            return None, None
    
    def evaluate_radius_sq_masks(self, rectangles, radius_sq_masks, edge_mask):
        '''
        rectangles - a Rectangles object.
        radius_sq_masks - a tuple (ball mask, grass mask)
        edge_mask - a numpy array with shape (n, m) and type np.bool. It represents the edges in an image

        return - a numpy array with length (number of rectangles). The i-th number evaluates the negation of the
        log probability that the region of the image inside the i-th rectangle is the image of a ball.

        The SMALLER the number the better the fit

        If one of the masks is empty, then we raise a ValueError exception.
        '''
        print 'evaluating radius masks on %d rectangles...' % rectangles.get_number_of_rectangles()

        if not all(mask.any() for mask in radius_sq_masks):
            raise ValueError('One of the radius masks was empty!')
        
        #first get the slices corresponding to each rectangle.
        edge_slices = rectangles.get_slices(edge_mask)

        inside_mask, outside_mask = radius_sq_masks
        
        #compute the log probability for the inside and outside regions
        inside_area = float(inside_mask.sum())
        outside_area = float(outside_mask.sum())

        inside_ratio = (edge_slices * inside_mask).sum(axis=(1,2)) / inside_area
        outside_ratio = (edge_slices * outside_mask).sum(axis=(1,2)) / outside_area

        edge_evaluation = inside_ratio - (self.OUTSIDE_WEIGHT * outside_ratio)
        
        return -edge_evaluation

    def evaluate_ball_position(self, edge_mask, camera_matrix, ball_radius, ball_position):
        '''
        image_sat - a numpy array with shape (n, m) and type np.uint8. It represents the saturation channel of an image.
        camera_matrix - a CameraMatrix object
        ball_radius - a float
        ball_position - a numpy array with length 3.

        return - a float specifying how far the log prob of being a ball position. The smaller the number
        the better the fit.

        If there is not enough information to evaluate the position, return np.nan.
        '''
        print 'evaluating ball at position: ', ball_position
        
        rectangle, radius_sq_masks = self.compute_radius_sq_masks(camera_matrix, ball_radius, ball_position)

        if rectangle is None:
            return np.nan
        
        rectangles = Rectangles.from_rectangle(rectangle)
        evaluation = self.evaluate_radius_sq_masks(rectangles, radius_sq_masks, edge_mask)[0]

        print 'evaluation: ', evaluation
        
        return evaluation
