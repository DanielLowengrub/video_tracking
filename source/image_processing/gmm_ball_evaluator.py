import numpy as np
from ..auxillary import planar_geometry, world_to_image_projections
from ..auxillary.rectangle import Rectangle
from ..auxillary.rectangles import Rectangles

class GMMBallEvaluator(object):
    '''
    This class is used to evaluate how likely it is that a given point in world coordinates is the center of a ball.
    To evaluate the position, it is given an image and the camera that produced it.

    The evaluation is determined as follows:
    1) Extract a patch around the center of the image of the ball.
    2) Divide the patch into the ball region and the grass region
    3) Use a GMM for compute the log prob of the ball region. Same for the grass.
    4) return the total log prob
    '''

    PATCH_SIZE = 6    #To evaluate a ball position, we consider a patch whose center is the ball,
                      #and which extends by PATCH_SIZE in each direction.

    BALL_MEAN = 70.0
    BALL_STDEV = 15.0
    HUE_MEAN = 44.0
    HUE_STDEV = 2.0
    
    def _generate_radius_sq_map(self, ellipse):
        '''
        ellipse - a numpy array with shape (3,3) representing an ellipse
        
        return a tupe (rectangle, radius_sq_map) where
        radius_sq_map - numpy array with shape (2*PATCH_SIZE + 1, 2*PATCH_SIZE + 1) and type int32.
                        the value of the entry (y,x) represents the value of the normalized ellipse on the point 
                        center_of_ellipse+(x,y).
        rectangle - a Rectangle object defining where the radius_sq_map is located on the image. I.e, the center of the
                    rectangle is the center of the ellipse, and it's size is the same size as the radius_sq_map
        '''
        ellipse_center, M, b = planar_geometry.compute_normalized_ellipse(ellipse)
        rectangle_center = np.rint(ellipse_center).astype(np.int32)
        center_offset = rectangle_center - ellipse_center

        # print 'ellipse center: ', ellipse_center
        print 'integer ellipse center: ', rectangle_center
        # print 'center offset: ', center_offset
        
        x = np.arange(-self.PATCH_SIZE, self.PATCH_SIZE+1,1) + center_offset[0]
        y = np.arange(-self.PATCH_SIZE, self.PATCH_SIZE+1,1) + center_offset[1]

        XX, YY = np.meshgrid(x,y)
        ellipse_values = XX*XX*M[0,0] + 2*XX*YY*M[0,1] + YY*YY*M[1,1]

        top_left = rectangle_center - np.array([self.PATCH_SIZE, self.PATCH_SIZE])
        rectangle = Rectangle.from_top_left_and_shape(top_left, ellipse_values.shape)

        return rectangle, ellipse_values

    def _compute_log_prob(self, image_sats, mask, mean, stdev):
        '''
        images - a numpy array with shape (num images, n, m) and type np.uint8.
        mask - a numpy array with shape (n,m)
        mean - a float
        stdev - a float
        
        return - a numpy array with length (num images). The ith value is the log probability of the masked region of the
        i-th image w.r.t N(mean,stdev)
        '''

        #this is a numpy array with shape (num images, num pixels in mask)
        image_values = image_sats[:,mask].astype(np.float64)

        #this is a numpy array with length (num images)
        log_probs = -((image_values - mean)**2 / stdev**2).mean(axis=1)

        print 'means and stdevs:'
        print np.vstack([image_values.mean(axis=1), image_values.std(axis=1)])
        
        return log_probs
    
    def compute_radius_sq_masks(self, camera_matrix, ball_radius, ball_position):
        '''
        camera_matrix - a CameraMatrix object
        ball_radius - a float
        ball_position - a numpy array with length 3.

        return - a tuple (rectangle, radius_sq_masks)
        rectangle - a Rectangle object
        radius_sq_masks - a pair of masks (ball mask, grass mask). Both masks have the same shape as the rectangles.

        If one of the masks is empty, return None
        '''
        ball_position = ball_position.reshape((1,3))
        ball_ellipse = world_to_image_projections.project_spheres_to_image(camera_matrix, ball_radius, ball_position)[0]
        rectangle, radius_sq_map = self._generate_radius_sq_map(ball_ellipse)

        # print 'ball ellipse:'
        # print ball_ellipse
        
        # print 'radius sq map:'
        # print radius_sq_map.astype(np.int32)
        
        ball_mask = np.logical_and(0 <= radius_sq_map, radius_sq_map <= 1)
        grass_mask = 1 < radius_sq_map

        radius_sq_masks = (ball_mask, grass_mask)
        
        #make sure that the masks are not empty
        if all(mask.any() for mask in radius_sq_masks):
            return rectangle, radius_sq_masks

        #if one of them is empty, then the radius masks are declared to be degenerate
        else:
            return None, None
    
    def evaluate_radius_sq_masks(self, rectangles, radius_sq_masks, image_hs):
        '''
        rectangles - a Rectangles object.
        radius_sq_masks - a tuple (ball mask, grass mask)
        image_hs - a numpy array with shape (n, m, 2) and type np.uint8. It represents the hue/saturation channels of an image.

        return - a numpy array with length (number of rectangles). The i-th number evaluates the negation of the
        log probability that the region of the image inside the i-th rectangle is the image of a ball.

        The SMALLER the number the better the fit

        If one of the masks is empty, then we raise a ValueError exception.
        '''
        print 'evaluating radius masks on %d rectangles...' % rectangles.get_number_of_rectangles()

        if not all(mask.any() for mask in radius_sq_masks):
            raise ValueError('One of the radius masks was empty!')
        
        #first get the slices corresponding to each rectangle.
        image_hue_slices = rectangles.get_slices(image_hs[:,:,0])
        image_sat_slices = rectangles.get_slices(image_hs[:,:,1])

        ball_mask = radius_sq_masks[0]
        hue_mask = np.ones(ball_mask.shape, np.bool)
        
        #compute the log probability for the ball and grass regions
        ball_log_prob = self._compute_log_prob(image_sat_slices, ball_mask, self.BALL_MEAN, self.BALL_STDEV)
        hue_log_prob = self._compute_log_prob(image_hue_slices, hue_mask, self.HUE_MEAN, self.HUE_STDEV)

        print 'ball and hue log probs: '
        print -np.vstack([ball_log_prob, hue_log_prob])

        log_prob = ball_log_prob + hue_log_prob
        
        return -log_prob

    def evaluate_ball_position(self, image_hs, camera_matrix, ball_radius, ball_position):
        '''
        image_sat - a numpy array with shape (n, m) and type np.uint8. It represents the saturation channel of an image.
        camera_matrix - a CameraMatrix object
        ball_radius - a float
        ball_position - a numpy array with length 3.

        return - a float specifying how far the log prob of being a ball position. The smaller the number
        the better the fit.

        If there is not enough information to evaluate the position, return np.nan.
        '''
        print 'evaluating ball at position: ', ball_position
        
        rectangle, radius_sq_masks = self.compute_radius_sq_masks(camera_matrix, ball_radius, ball_position)

        if rectangle is None:
            return np.nan
        
        rectangles = Rectangles.from_rectangle(rectangle)
        evaluation = self.evaluate_radius_sq_masks(rectangles, radius_sq_masks, image_hs)[0]

        print 'evaluation: ', evaluation
        
        return evaluation
