class HeadExtractor(object):
    '''
    This is the base class for head extractor.

    The job of a head extractor is to determine which points on an image are heads of people.
    The input can be either a mask, or a color image, depending on the implementation.
    '''

    def get_head_positions(self, image):
        '''
        Return a list of positions in image coordinates which are centered on human heads.

        This is sloppy, but the type of image is determined by the implementation.
        '''

        pass
