import numpy as np
import cv2
from .head_extractor import HeadExtractor

class BWHeadExtractor(HeadExtractor):
    '''
    Black and White head extractor. This uses only a mask of the player to detect a head.
    It also assumes that there is only one player in the image, and that the head is on the top of the image.
    '''

    def __init__(self):

        self._head_to_height_ratio = 0.1 #height of head = 0.1 * height of body. This is a very conservative number, most heads are bigger.
        self._padding = 5 #we pad the image before looking for contours
    def get_head_positions(self, image):
        '''
        image - a numpy array of shape (n,m) and type np.uint8

        Since this is a very simple implementation, we check if the top _head_to_height_ratio of the image is connected. If it is, this is the head.
        '''

        #print 'image shape: ', image.shape
        
        #slice off the top of the image
        head_height = image.shape[0] * self._head_to_height_ratio
        image_top = image[:head_height, :]

        padding = np.zeros((self._padding, image_top.shape[1]), np.uint8)
        image_top = np.vstack([padding, image_top, padding])
        
        #cv2.imshow('head', image_top)
        #cv2.waitKey(0)
        #cv2.destroyAllWindows()

        #print 'image top shape: ', image_top.shape
        
        #get all connected components
        cv_contours, hierarchy = cv2.findContours(image_top, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)

        #print 'we found %d contours.' % len(cv_contours)
        #if there is not exactly one connected component, return an empty list.
        if not len(cv_contours) == 1:
            return []

        #otherwise, take the center of this component
        x, y, w, h = cv2.boundingRect(cv_contours[0])
        head_position = np.array([x + (w/2),(y + (h/2)) - self._padding])

        return [head_position]

        
