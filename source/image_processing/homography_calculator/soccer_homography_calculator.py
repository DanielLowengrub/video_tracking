import numpy as np
from operator import itemgetter
from ...data_structures.contour import Contour
from ...auxillary.soccer_field_geometry import SoccerFieldGeometry
from ...auxillary.shape_matching.directed_shape_matching_graph_builder import DirectedShapeMatchingGraphBuilder
from ...auxillary.fit_points_to_shape.line import Line
from ...auxillary import planar_geometry
from iwasawa_homography_calculator import IwasawaHomographyCalculator
from correspondence_homography_calculator import CorrespondenceHomographyCalculator

DEBUG = False

class SoccerHomographyCalculator(object):
    '''
    This class is in charge of computing a homography matrix using a collection of lines and ellipses on an image.

    We assume that the shapes come from a homography transformation of a soccer field, 
    and we find both the homography matrix, and the which part of the soccer field each shape corresponds to.
    '''

    INF_LINE_DISTANCE_RATIO = 0.75 #a homography is degenerate if the distance of the projection of the line at infinity is
                                   #closer to the image center than (INF_LINE_DISTANCE_RATIO * norm(image center))

    MIN_FIELD_MASK_OVERLAP_PERCENTAGE = 0.75 #Let H be a homography and let M be a mask denoting the points on the image that
                                             #are transformations of points in the absolute field. Let FM be the field mask
                                             #on the image. H is declared to be degenerate if
                                             #(overlap area of M and FM) / (total area of M and FM) < min_fm_overlap_percentage
                                             
    MIN_LINES_IN_PARALLEL_COMPONENT = 2 #each collection of parallel lines must have at least this many elements

    MAX_LINE_LINE_EMBEDDINGS = 100
    
    #the grid of points we overlay on the soccer field to visualize the homography
    GRID_WIDTH = 20
    GRID_LENGTH = 14

    CENTER_CIRCLE_KEY = SoccerFieldGeometry.format_key(SoccerFieldGeometry.CENTER_CIRCLE)
    CENTER_LINE_KEY = SoccerFieldGeometry.format_key(SoccerFieldGeometry.CENTER_LINE)

    def __init__(self, absolute_image, scale,
                 min_lines_in_correspondence,
                 highlighted_line_threshold, highlighted_ellipse_threshold, parallel_lines_threshold,
                 correspondence_threshold, iwasawa_threshold):
        '''
        absolute_image - an image of the absolute soccer field
        scale - each yard in the soccer field corresponds to scale pixels in the absolute image
        min_lines_in_correspondence - the smallest number of lines that can be used to compute a line correspondence homography
        highighted_line_threshold - we ignore line segments that are shorter than this number of pixels
        highlighted_ellipse_threshold - we ignore ellipse segements that are shorter than this angle in degrees
        parallel_lines_threshold - we consider two lines to be parallel if 1 - cos(angle between the lines) < threshold
        correspondence_threshold - the maximum error allowed when calculating homographies based on a correspondence of lines
        iwasawa_threshold - the maximum error allowed when using the iwasawa algorithm to compute a homography based on an ellipse and a center line
        '''
        self._min_lines_in_correspondence = min_lines_in_correspondence
        self._correspondence_threshold = correspondence_threshold
        self._iwasawa_threshold = iwasawa_threshold

        #this is the object we use to construct shape matching graphs out of collections of highlighted lines
        self._shape_matching_graph_builder = DirectedShapeMatchingGraphBuilder(highlighted_line_threshold,
                                                                               highlighted_ellipse_threshold,
                                                                               parallel_lines_threshold)

        #record data about the absolute soccer field geometry
        self._absolute_image = absolute_image
        self._absolute_mask = np.zeros(absolute_image.shape[:2], np.float32)
        
        field_width = absolute_image.shape[1] - 1
        field_length = absolute_image.shape[0] - 1
        self._soccer_field = SoccerFieldGeometry(scale, field_width, field_length, self.GRID_WIDTH, self.GRID_LENGTH)

        #this is a ShapeMatchingGraph object which can be used to align highlighted shapes in an image with the soccer field
        smgb = self._shape_matching_graph_builder
        absolute_highlighted_shape_dict = self._soccer_field.get_highlighted_shape_dict(format_keys=False)
        self._absolute_shape_matching_graph = smgb.build_shape_matching_graph(self._absolute_image, self._absolute_mask,
                                                                              absolute_highlighted_shape_dict)
        
        #build the homography calculators
        self._iwasawa_homography_calculator = IwasawaHomographyCalculator()
        self._correspondence_homography_calculator = CorrespondenceHomographyCalculator()


    def _compute_homography_jacobian(self, H):
        D = H[2,2]
        return (1 / D**2) * np.array([[H[0,0] - H[2,0]*H[0,2], H[0,1] - H[2,1]*H[0,2]],
                                      [H[1,0] - H[2,0]*H[1,2], H[1,1] - H[2,1]*H[1,2]]])

    def _preserves_orientation(self, H):
        '''
        H - a homography matrix
        Return true iff H preserve orientation and does not flip any of the axes
        '''

        J = self._compute_homography_jacobian(H)
        detJ = np.linalg.det(J)
        # print '      H:'
        # print H
        # print '      J:'
        # print J
        # print '      |J| = ', detJ
        # print '      J[0,0] = ', J[0,0], ' J[1,1] = ', J[1,1]
        return (J[0,0] > 0) and (J[1,1] > 0) and (detJ > 0)# and (np.abs(detJ - 1.0) < 0.5)

    def _line_at_infinity_in_image(self, image, H):
        '''
        image - a numpy array with shape (n,m,3) and type np.uint8
        H - a numpy array with shape (3,3) and type np.float64 which represents a homography to the image.

        Return True if the transformation of the line at infinity in the source is inside the image.
        '''
        source_inf = np.array([0,0,1], np.float32)
        transformation_of_inf = Line.from_parametric_representation(np.dot(np.linalg.inv(H).transpose(), source_inf))
        
        image_center = np.array([image.shape[1]-1,image.shape[0]-1], np.float32) / 2.0

        #check if the image of the line at infinity enters the image
        distance_to_center = transformation_of_inf.get_geometric_distance_from_point(image_center)

        in_image = distance_to_center < self.INF_LINE_DISTANCE_RATIO*np.linalg.norm(image_center)

        #print '      distance to center: ', distance_to_center
        #print '      in image: ', in_image

        return in_image

    def _homography_matches_field_mask(self, image, field_mask, H, debugging_mode=False):
        '''
        image - a numpy array with shape (n,m,3) and type np.uint8
        field_mask - a numpy array with shape (n,m) and type np.bool. It represents the part of the image that is covered by
                     grass.
        H - a (3,3) numpy array with type np.float32. It represents a homography matrix mapping the absolute field 
            to the image.

        return True if the transformation of the absolute soccer field matches with the field mask of the image.
        '''
        if DEBUG:
            print '      checking if homography matches field mask...'
        absolute_field_mask_shape = (self._soccer_field.field_length(), self._soccer_field.field_width())
        absolute_field_mask = np.ones(absolute_field_mask_shape, dtype=np.bool)
        transformed_field_mask = planar_geometry.apply_homography_to_mask(H, absolute_field_mask, image.shape[:2])

        mask_union = np.logical_or(field_mask, transformed_field_mask)
        mask_intersection = np.logical_and(field_mask, transformed_field_mask)

        total_area = mask_union.sum()
        overlap_area = mask_intersection.sum()

        #if the total area of the two masks is zero, then there are no masks to compare
        if total_area == 0:
            return False

        overlap_percentage = float(overlap_area) / total_area

        if DEBUG:
            print '         overlap percentage: ', overlap_percentage
            print '   H:'
            print H
            print '   absolute cv contour:'
            print absolute_cv_contour
            print '   image cv contour: '
            print image_cv_contour
            _display_mask(image, transformed_field_mask)

        #we say that the transformed field mask matches the field mask if the overlap is large
        return overlap_percentage > self.MIN_FIELD_MASK_OVERLAP_PERCENTAGE

    def _homography_matches_highlighted_regions(self, H, abs_highlighted_shape, img_highlighted_shape,
                                                image_region, anchor_sign):
        '''
        H - a homography matrix
        source_highlighted_shape - a HighlightedShape object
        target_highlighted_shape - a HighlightedShape object.
        anchor_sign - an integer. -1, 0 or 1. It represents the side of the line at infty that we apply H to

        Assume that H*(underlying shape of source hs) ~ (underlying shape of target hs)

        return True if the highlighted region of (target hs) is contained in H*(highlighted region of target hs)
        '''
        #transform the abs shape by the homography
        transformed_abs_hs = abs_highlighted_shape.apply_homography(H, image_region, anchor_sign)
        if transformed_abs_hs is None:
            return False
        
        # print 'intervals on img shape:'
        # print img_highlighted_shape._intervals_on_shape
        # print 'intervals on abs shape:'
        # print abs_highlighted_shape._intervals_on_shape
        # print 'intervals on transformed abs shape:'
        # print transformed_abs_hs._intervals_on_shape
        
        #snap the highlighted region of the transformed shape to the image shape
        transformed_abs_hs = transformed_abs_hs.snap_to_shape(img_highlighted_shape.get_shape_with_position())
        # print 'intervals on transformed abs shape, snapped to image shape:'
        # print transformed_abs_hs._intervals_on_shape
        transformed_abs_hs = transformed_abs_hs.interval_dilation(50)
        # print 'intervals on dilates transformed abs shape'
        # print transformed_abs_hs._intervals_on_shape

        #check if the highlighted region of transformed_abs_shape contains the highilghted region of the image shape
        return transformed_abs_hs.contains_highlighted_shape(img_highlighted_shape)
    
    def _embedding_matches_highlighted_regions(self, H, embedding, shape_matching_graph, image_region, anchor_sign):
        '''
        H - a homography matrix
        embedding - a dictionary with items 
           (key of absolute soccer field shape, key of image shape)
        shape_matching_graph - a ShapeMatchingGraph representing a configuration of shapes in the image
        anchor_sign - an integer. -1, 0 or 1. It represents the side of the line at infty that we apply H to

        return True if every pair (abs highlighted shape, image highlighted shape) satisfies:
           the highliighted region of H*(abs hs) contains the highlighted region of (image hs)
        '''
        for abs_shape_key, img_shape_key in embedding.iteritems():
            if DEBUG:
                print '      checking the the highighted region of shape: ', abs_shape_key
            abs_highlighted_shape = self._absolute_shape_matching_graph[abs_shape_key]
            img_highlighted_shape = shape_matching_graph.get_highlighted_shape(img_shape_key)
            if not self._homography_matches_highlighted_regions(H, abs_highlighted_shape, img_highlighted_shape,
                                                                image_region, anchor_sign):
                # print '      no match'
                return False

            else:
                # print '      match'
                pass
            
        return True
    
    def _homography_is_degenerate(self, image, annotation, H, debugging_mode=False):
        '''
        image - a numpy array with shape (n,m,3) and type np.uint8
        annotation - a SoccerAnnotation object
        field_mask - a numpy array with shape (n,m) and type np.bool. It represents the part of the image that is covered by
                     grass.
        H - a (3,3) numpy array with type np.float32. It represents a homography matrix mapping the absolute field 
            to the image.

        Return True if the homography is degenerate.
        '''
        if DEBUG:
            print '   - checking if homography is degenerate...'
        #check if the homography preserves orientation
        preserves_orientation = self._preserves_orientation(H)
        
        #check if the image of the line at infinity enters the image
        line_at_infinity_in_image = self._line_at_infinity_in_image(image, H)

        #check if the transformation of the soccer field agrees with the field mask
        field_mask = annotation.get_soccer_field_mask()
        matches_field_mask = self._homography_matches_field_mask(image, field_mask, H, debugging_mode)

        if DEBUG:
            print '      preserves orientation:  ', preserves_orientation
            print '      line at infty in image: ', line_at_infinity_in_image
            print '      matches field mask:     ', matches_field_mask
        
        is_degenerate = (not preserves_orientation) or line_at_infinity_in_image or (not matches_field_mask)
        
        return is_degenerate

    def _is_valid_graph_for_ellipse(self, shape_matching_graph):
        '''
        shape_matching_graph - a ShapeMatchingGraph object
        Return true if this graph can be used to compute a homography using an ellipse
        '''
        #we need exactly one ellipse and at least three shapes to compute an embedding wth an ellipse
        num_ellipses = sum(hs.is_ellipse() for hs in shape_matching_graph.get_highlighted_shapes_iter())
        num_lines = sum(hs.is_line() for hs in shape_matching_graph.get_highlighted_shapes_iter())
        
        return (num_ellipses == 1) and (num_lines >= 2)

    def _is_valid_embedding_for_ellipse(self, shape_matching_graph, embedding):
        '''
        shape_matching_graph - a ShapeMatchingGraph object
        embedding - a dictionary with items (absolute shape key, target shape key)

        return - True if this embedding of the shape matching graph can be used to compute a homography with an ellipse.
        '''
        #make sure that the embedding contains both the center circle and the center line
        return (self.CENTER_CIRCLE_KEY in embedding) and (self.CENTER_LINE_KEY in embedding)

    def _compute_homography_from_ellipse_embedding(self, image, annotation, shape_matching_graph, embedding):
        '''
        image - a numpy array with shape (n,m,3) and type np.uint8
        annotation - a SoccerAnnotation object
        shape_matching_graph - a ShapeMatchingGraph object
        embedding - a dictionary which represents an embedding of the ShapeMatchingGraph in the absolute graph.

        return a tuple (H, error)
        H - a numpy array with shape (3,3) and type np.float64
        error - a float

        If we did not manage to find a homography, return (None, None)
        '''
        image_cc_key = embedding[self.CENTER_CIRCLE_KEY]
        image_cl_key = embedding[self.CENTER_LINE_KEY]

        abs_graph = self._absolute_shape_matching_graph
        sm_graph = shape_matching_graph

        #build three correspondences. One for the center circle, one for the center line, and one for the rest of the lines
        cc_corr = (abs_graph[self.CENTER_CIRCLE_KEY].get_shape(), sm_graph[image_cc_key].get_shape())
        cl_corr = (abs_graph[self.CENTER_LINE_KEY].get_shape(), sm_graph[image_cl_key].get_shape())
        line_corr = [(abs_graph[abs_key].get_shape(), sm_graph[image_key].get_shape())
                     for abs_key,image_key in embedding.iteritems()
                     if abs_key not in (self.CENTER_CIRCLE_KEY, self.CENTER_LINE_KEY)]

        #if there are no line correspondences we can not compute the homography
        if len(line_corr) == 0:
            return (None, None)

        #use the center circle, center line and line correspondences to compute 4 possible homography matrices and their errors
        #Hs is a list of numpy arrays, each with shape (3,3) and type np.float64
        #errors is a numpy array with shape (number of homographies, number of shapes)
        Hs, errors = self._iwasawa_homography_calculator.compute_homography(self._absolute_image, image, cc_corr, cl_corr,
                                                                            line_corr)
        
        #find the unique homography matrix that is not degenerate, and has a small enough mean error
        filtered_H_errors = self._filter_ellipse_homographies(image, annotation, Hs, errors)
        
        #if this homography is not unique, we can not determine the homography
        if not len(filtered_H_errors) == 1:
            if DEBUG:
                print '   there is not a unique homography which is not degenerate.'
            return (None, None)
        
        #if we found a unique such homography matrix, return it, together with the mean error
        H, errors = filtered_H_errors[0]
        return H, errors.mean()

    def _filter_ellipse_homographies(self, image, annotation, Hs, errors):
        '''
        image - a numpy array with shape (n,m,3) and type np.uint8
        annotation - a SoccerAnnotation object
        Hs - a list of homography matrices. Each ones is a numpy array with shape (3,3) and type np.float64
        errors - a numpy array with shape (number of homographies, number of shapes)

        return a list of tuples (H, errors) consisting of the homographies which are non degenerate, and have a small error.
        '''
        return [(H,e) for H,e in zip(Hs, errors)
                if (e.mean() < self._iwasawa_threshold) and not self._homography_is_degenerate(image, annotation, H)]
        
    def _find_best_embedding_with_ellipse(self, image, annotation, shape_matching_graph):
        '''
        image - a numpy array with shape (n,m,3) and type np.uint8
        annotation - a SoccerAnnotation object
        shape_matching_graph - a ShapeMatchingGraph object

        return - a pair (homography, embedding)
        homography is a homography from the absolute image to the image which induces the embedding.
        embedding is a dictionary with items (absolute shape key, image shape key)

        We choose the pair that minimizes the average difference of H * source_shape and target_shape for all pairs 
        (source shape, target shape) in the embedding.

        Since there is exactly one ellipse, we use the iwasawa homography calculator. To use this, the embedding must contain 
        the center circle and the center line.
        Also, since the iwasawa algorithm currently does not guarentee a close match for lines other than the center line, 
        we do not expect there to be a unique homography that satisfies the _iwasawa_threshold. 
        So we take the homography which induces the smallest error.
        
        Specifically, we do the following:
        * For each embedding, find the unique homography that preserves the orientation and induces the embedding up to the 
          iwasawa_threshold.
          If a unique such homography does not exist, skip this embedding.
        * Return the homography from the previous step with the smallest error.
        '''
        if DEBUG:
            print 'finding best embedding with ellipse...'

        #if the shape matching graph can not be used to compute the homography
        if not self._is_valid_graph_for_ellipse(shape_matching_graph):
            return (None, None)

        #find all of the embeddings of the given shape matching graph in the absolute graph
        embeddings = self._absolute_shape_matching_graph.compute_graph_embeddings(shape_matching_graph)

        #build a tuple (homography, embedding, error) which stores the current best (homography,embedding) pair, together with
        #their error
        best_H_embedding_error = (None, None, np.inf)
        
        for embedding in embeddings:            
            #make sure that the embedding can be used to compute a homography
            if not self._is_valid_embedding_for_ellipse(shape_matching_graph, embedding):
                continue

            if DEBUG:
                print '* considering the embedding: '
                print '   ', _embedding_repr(embedding)

            #use the embedding to find a homography. 
            H, mean_error = self._compute_homography_from_ellipse_embedding(image, annotation, shape_matching_graph,
                                                                            embedding)

            #If there were no homographies consistent with the embedding.
            if H is None:
                if DEBUG:
                    print '   could not find any homographies.'
                continue

            if DEBUG:
                print '   found a homography with mean error: ', mean_error

            if DEBUG:
                print '   checking the highlighted regions...'

            anchor_sign = planar_geometry.compute_anchor_sign_from_target_shape(H, image.shape[:2])
            image_region = planar_geometry.get_image_region(image)
            if not self._embedding_matches_highlighted_regions(H, embedding, shape_matching_graph,
                                                               image_region, anchor_sign):
                if DEBUG:
                    print '      there is no match.'
                continue

            if DEBUG:
                print '   current best error: ', best_H_embedding_error[2]
            if  mean_error < best_H_embedding_error[2]:
                if DEBUG:
                    print '   updating to homography with mean error: ', mean_error
                best_H_embedding_error = (H, embedding, mean_error)

        if DEBUG:
            print 'the best ellipse embedding has error: ', best_H_embedding_error[2]
        
        return best_H_embedding_error[:2]

    def _is_valid_graph_for_lines(self, shape_matching_graph):
        '''
        shape_matching_graph - a ShapeMatchingGraph object

        return True iff this shape matching graph can be used to compute a line based shape embedding. 
        '''
        if DEBUG:
            print 'checking if this is a valid graph for a line based homography calculation...'
        
        #if there are not enough lines to compute an embedding
        if shape_matching_graph.number_of_nodes() < self._min_lines_in_correspondence:
            return False

        #make sure that there are two classes of parallel lines, and that there are at least 2 lines in each class
        parallel_line_components = shape_matching_graph.find_parallel_line_components()
        if DEBUG:
            print '   parallel line components:'
            print '\n'.join(map(str,parallel_line_components))
        
        if not (len(parallel_line_components) == 2):
            if DEBUG:
                print '   there are not enough parallel line components.'
            return False

        #make sure that both classes of parallel lines have enough elemenets
        if any(len(plc)<self.MIN_LINES_IN_PARALLEL_COMPONENT for plc in parallel_line_components):
            if DEBUG:
                print '   one of the parallel line components is too small.'
            return False

        if DEBUG:
            print '   the graph is valid.'
        return True

    def _compute_homography_from_lines_embedding(self, image, annotation, shape_matching_graph, embedding,
                                                 debugging_mode=False):
        '''
        image - a numpy array with shape (n,m,3) and type np.uint8
        annotation - a SoccerAnnotation object
        shape_matching_graph - a ShapeMatchingGraph object
        embedding - a dictionary which represents an embedding of the ShapeMatchingGraph in the absolute graph.

        return - a pair (homography, error)
        homography is a numpy array with shape (3,3) which represents a homography that induces the given embedding.
        error is a float which represents the maximum difference ||H*absolute_shape - image_shape|| where 
        (absolute_shape, image_shape) are matched by the embedding.

        If we could not find a homography, return (None, None).
        '''
        if DEBUG:
            print 'computing homography from lines embedding...'
        
        abs_graph = self._absolute_shape_matching_graph
        sm_graph = shape_matching_graph

        #use the embedding to construct a correspondence of lines
        line_correspondences = [(abs_graph[abs_key].get_shape(), sm_graph[image_key].get_shape())
                                for abs_key,image_key in embedding.iteritems()]

        #use the correspondences to compute an embedding
        H, errors = self._correspondence_homography_calculator.compute_homography(self._absolute_image, image,
                                                                                  line_correspondences)
        #if the homography is degenerate, do not return anything
        if self._homography_is_degenerate(image, annotation, H, debugging_mode):
            if DEBUG:
                print '   the homography is degenerate.'
            return (None, None)
        
        max_error = errors.max()
        if DEBUG:
            print '   max error: ', max_error
        return H, max_error
    
    def _find_best_embedding_with_lines(self, image, annotation, shape_matching_graph):
        '''
        image - a numpy array with shape (n,m,3) and type np.uint8
        annotation - a SoccerAnnotation object
        shape_matching_graph - a ShapeMatchingGraph object

        return - a pair (homography, embedding) 
        homography is a homography from the absolute image to the image which induces the embedding.
        embedding is a dictionary with items (absolute shape key, image shape key)

        We choose the pair that minimizes the average difference of H * source_shape and target_shape for all pairs 
        (source shape, taget shape) in the embedding.

        Since all of the shapes are lines, we use the CorrespondenceHomographyCalculator 
        to compute the homography for each embedding.

        Since the correct embedding should give a good match, we do the following:
        * For each embedding, compute the unique homography matrix that induces the embedding
        * throw out homographies that do not preserve orientation, or which have a large error
        * if we are left with one homography, return it and the corresponding embedding, otherwise, return None
        '''
        if DEBUG:
            print 'finding best embedding with line correspondences...'

        #check if the given shape matching graph is suitable for computing a line base homography
        if not self._is_valid_graph_for_lines(shape_matching_graph):
            if DEBUG:
                print '   the graph is not valid for line based homography.'
            return (None, None)

        #find all of the embeddings of the given shape matching graph in the absolute graph
        embeddings = self._absolute_shape_matching_graph.compute_graph_embeddings(shape_matching_graph)

        #if there are too many embeddings, then do not try to search for the best one
        if len(embeddings) > self.MAX_LINE_LINE_EMBEDDINGS:
            if DEBUG:
                print '   there were too many possible embeddings (%d).' % len(embeddings)
            return (None, None)

        #this stores tuples of the form (homography, embedding)
        H_embeddings = []
        #this is for debugging purposes
        error_embeddings = []
        
        for i,embedding in enumerate(embeddings):
            if DEBUG:
                print '*  %d: evaluating the embedding: ' % i
                print '   ', _embedding_repr(embedding)

            #debugging_mode = (i == 31)
            debugging_mode = False
            
            #use the embedding of the shape matching graph in the absolute graph to compute a homography
            H, max_error = self._compute_homography_from_lines_embedding(image, annotation, shape_matching_graph,
                                                                         embedding, debugging_mode)
                                                                         

            #for debugging purposes, record the error of non degenerate embeddings
            if H is not None:
                error_embeddings.append((max_error, embedding))

            #if the error was too large, or if H is degenerate
            if H is None or (max_error > self._correspondence_threshold):
                continue
                                        
            H_embeddings.append((H, embedding))

        if DEBUG:
            _print_embedding_with_lines_summary(embeddings, H_embeddings, error_embeddings)
        
        #if there is a unique homography, return it
        if len(H_embeddings) == 1:
            return H_embeddings[0]

        else:
            return None, None
        
    def _find_best_embedding(self, image, annotation, shape_matching_graph):
        '''
        image - a numpy array with shape (n,m,3) and type np.uint8
        annotation - a SoccerAnnotation object
        shape_matching_graph - a ShapeMatchingGraph object

        Return a pair (homography, embedding)
        - homography is a numpy array with shape (3,3) and type np.float64, which represents a homography from the absolute
          field to the image
        - embedding is a dictionary with items (absolute shape key, image shape key).

        We choose the pair that minimizes the average of ||H * source_shape - target_shape|| for all pairs 
        (source shape, taget shape) in the embedding.
        '''
        number_of_ellipses = sum(hs.is_ellipse() for hs in shape_matching_graph.get_highlighted_shapes_iter())

        #if there is a single ellipse, then we can use a specialized homography calculator
        if number_of_ellipses == 1:
            return self._find_best_embedding_with_ellipse(image, annotation, shape_matching_graph)

        #if there is no ellipse, then compute the homography by using a least squares method to match up the lines
        elif number_of_ellipses == 0:
            return self._find_best_embedding_with_lines(image, annotation, shape_matching_graph)

        else:
            return None, None
        
    def compute_homography(self, image, annotation, highlighted_shape_dict):
        '''
        image - a numpy array with shape (n,m,3) and type np.uint8
        annotation - a SoccerAnnotation object
        highlighted_shape_dict - a dictionary with items (name of shape, HighlightedShape object)

        Return a pair (homography, shape_matching)
        homography - a numpy array with shape (3,3) and type np.float64 which represents a homography that maps the 
                     soccer field to the image
        shape_matching - a dictionary with items
        (name of a shape on the absolute soccer field, 
        name of the shape in the highlighted_shape_dict that the absolute shape is mapped to under the homography)
        '''
        #this mask represents the regions of the image that we are not sure how to classify
        noise_mask = np.logical_or(annotation.get_sidelines_mask()>0, annotation.get_players_mask()>0)

        #_display_mask(image, noise_mask)
        
        #build a graph whose nodes are the shapes in the highlighted shape dictionary, and whose edges represent the
        #intersection types of the shapes.
        shape_matching_graph = self._shape_matching_graph_builder.build_shape_matching_graph(image, noise_mask,
                                                                                             highlighted_shape_dict)

        if DEBUG:
            print 'shape matching graph: '
            print str(shape_matching_graph)
        
        
        #find the best embedding of the shape matching graph into the absolute shape matching graph.
        #the homography is from the absolute field to the image. If absolute shape A is paired with image shape B by the
        #embedding, then the homography maps A to B.
        homography, embedding = self._find_best_embedding(image, annotation, shape_matching_graph)

        return homography, embedding

#THESE METHODS ARE USED FOR DEBUGGING
def _display_mask(image, mask, color=(0,0,255), alpha=0.5):
    import cv2
    image_with_mask = image.copy()
    image_with_mask[mask] = np.array(color)
    image_with_mask = (alpha*image_with_mask + (1-alpha)*image).astype(np.uint8)

    cv2.imshow('image with mask', image_with_mask)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    return

def _embedding_repr(embedding):
    return str(sorted([(v,k) for k,v in embedding.items()], key=lambda x: x[0]))
                      
def _print_embedding_with_lines_summary(embeddings, H_embeddings, error_embeddings):
    '''
    This is used for debugging purposes only
    embeddings - a list of embedding dictionaries
    H_embeddings - a list of pairs (homography, embedding). There is one for each nondegenerate homography with small error
    error_embeddings - a list of pairs (error, embedding). There is one for every embedding which induces a nondegenerate 
    homography, but possibly with a large error
    
    print a summary of these data
    '''
    head = 10 #only show the first "head" embeddings, where the embeddings are sorted by their error from small to large
    
    #sort the pairs (error, embedding) by the error, from small to large
    error_embeddings.sort(key=itemgetter(0))
    print '   embedding with lines summary:'
    print '   * total number of embeddings considered: ', len(embeddings)
    print '   * we found %d homographies that preserve orientation and are within the error threshold' % len(H_embeddings)
    print '   * the %d nondegenerate embeddings with smallest error are:' % head
    print '\n'.join('%f: %s' % (error, _embedding_repr(embedding)) for error,embedding in error_embeddings[:head])
    return
