import tensorflow as tf
import numpy as np
import functools
from .shape_distance_minimizer import ShapeDistanceMinimizer
from ...auxillary import planar_geometry, tf_operations

class SimpleCameraShapeDistanceMinimizer(ShapeDistanceMinimizer):
    '''
    This class optimizes for a homography that comes from a camera matrix, under the assumption that the
    y axis of the camera is equal to the world Z axis.
    This means that:
    P = C * [R | -RT] and P = H[:,(0,1,3)]
    
    where
    C = [f 0       u]
        [0 alpha*f v]
        [0 0       1]

    R = [r0  r1 0]
        [0   0  1]
        [-r1 r0 0]

    Note that this is equivalent to assuming that there exists a number v such that
    H = [h00   h01   h02]
        [h10   h11   h12]
        [v*h10 v*h11 1  ]

    So in partice, we try to find (h00, h01, h02, h10, h11, h12, v) that minimize the distance between the shapes.
    '''

    def __init__(self, learning_rate, max_epochs, local_minimum_threshold):
        '''
        learning_rate - the step sizes in the gradient direction that we take in each iteration
        max_epochs - the maximum number of iterations
        local_minimum_threshold - we decide that we are in a local minimum if the cost function changes by less than this.
        '''
        super(SimpleCameraShapeDistanceMinimizer, self).__init__(learning_rate, max_epochs, local_minimum_threshold)
        
    def initialize_session(self):
        '''
        Set up a TensorFlow session and build the cost function graph.
        '''
        self._sess = tf.Session()

        self._H_ph = tf.placeholder(tf.float32)
        self._source_lines_ph = tf.placeholder(tf.float32)
        self._target_lines_ph = tf.placeholder(tf.float32)
        self._source_ellipses_ph = tf.placeholder(tf.float32)
        self._target_ellipses_ph = tf.placeholder(tf.float32)

        self._lines_cost_tensor    = self._lines_cost(self._source_lines_ph, self._target_lines_ph, self._H_ph)
        self._ellipses_cost_tensor = self._ellipses_cost(self._source_ellipses_ph, self._target_ellipses_ph, self._H_ph)

        #this is the cost function we try to minimize
        cost_function = functools.partial(self._homography_cost, self._source_lines_ph, self._target_lines_ph,
                                          self._source_ellipses_ph, self._target_ellipses_ph)
        initial_H_params = np.zeros((7,), np.float32)

        self._tf_optimizer.initialize_session(self._sess, initial_H_params, cost_function)

        return

    @classmethod
    def _homography_cost(cls, source_lines, target_lines, source_ellipses, target_ellipses, H_params):
        '''
        H_params - a tf array with shape (7,) and type np.float32
        source_lines, target_lines - a tf array with shape (num lines, 3) and type np.float32
        source_ellipses, target_ellipses - a tf array with shape (num ellipses, 3, 3) and type np.float32.

        return a scalar with type np.float32
        '''

        #first build a 3x3 homography matrix out of the homography paramters
        one = np.ones((1,), np.float32)
        H = tf.reshape(tf.concat(axis=0, values=[H_params[:6], H_params[6]*H_params[3:5], one]), [3,3])
        
        lines_cost = cls._lines_cost(source_lines, target_lines, H)
        ellipses_cost = cls._ellipses_cost(source_ellipses, target_ellipses, H)

        return lines_cost + ellipses_cost
    

    @classmethod
    def _build_homography_parameters(cls, H):
        '''
        H - a numpy array with shape (3,3)
        return - a numpy array with shape (7,)
        
        we assume that H is of the form:
        H = [h00   h01   h02]
            [h10   h11   h12]
            [v*h10 v*h11 1  ]

        and return (h00, h01, h02, h10, h11, h12, v)
        '''
        v = H[2,0] / H[1,0]
        H_flat = H.reshape((-1,))
        H_params = np.append(H_flat[:6], v)
        return H_params

    @classmethod
    def _build_homography(cls, H_params):
        '''
        H_params - a numpy array with shape (7,)
        return - a numpy array with shape (3,3)

        This is the inverse of _build_homography_params
        '''
        H_flat = np.hstack([H_params[:6], H_params[6]*H_params[3:5], np.ones(1)])
        H = H_flat.reshape((3,3))
        return H
    
    def optimize_homography(self, initial_H, source_image_shape, source_shapes, target_image_shape, target_shapes):
        '''
        initial_H - a numpy array with shape (3,3) and type np.float32
        source_image_shape - a numpy array with length 3 and type np.int32 specifying the shape if the source image
        source_shapes - a list of PlanarShape objects corresponding to shapes on the source image
        target_image_shape - a numpy array with length 3 and type np.int32 specifying the shape of the target image
        target_shapes - a list of PlanarShape objects corresponding to shapes on the target image

        WARNING: We currently assume that there is at most one source/target ellipse
        return a numpy array with shape (3,3) and type np.float32.
        This represents a new homography matrix H which minimizes the distances between H*S_i and T_i for each pair of
        source and target shapes (S_i,T_i).
        '''

        # print 'source_image_shape: ', source_image_shape
        # print 'optimizing homography with source shapes:'
        # print '\n'.join(map(str, source_shapes))
        # print 'and target shapes:'
        # print '\n'.join(map(str, target_shapes))
        # print 'initial H:'
        # print initial_H
        # print initial_H[1,:2] / initial_H[2,:2]
        
        #First normalize the source and target shapes
        #N_source is a (3,3) numpy array which represents the homography matrix taking the source shapes to the
        #normalized source shapes. Similarly for N_target
        N_source = self._compute_normalization_matrix(source_image_shape, source_shapes)
        N_target = self._compute_normalization_matrix(target_image_shape, target_shapes)

        normalized_source_shapes = [planar_geometry.apply_homography_to_shape(N_source, s) for s in source_shapes]
        normalized_target_shapes = [planar_geometry.apply_homography_to_shape(N_target, t) for t in target_shapes]

        # print 'N source:'
        # print N_source
        # print 'N target:'
        # print N_target

        #we also update the initial homography to the new coordinates
        initial_H = np.float32(np.dot(N_target, np.dot(initial_H, np.linalg.inv(N_source))))
        initial_H /= initial_H[2,2]

        # print 'normalized initial H:'
        # print initial_H
        # print initial_H[1,:2] / initial_H[2,:2]
        
        # initial_shape_matching_errors = self._compute_shape_matching_errors(initial_H,
        #                                                                     normalized_source_shapes,
        #                                                                     normalized_target_shapes)
                
        initial_H_params = self._build_homography_parameters(initial_H)
        feed_dict = self._build_feed_dict(normalized_source_shapes, normalized_target_shapes, initial_H)

        best_H_params = self._tf_optimizer.minimize(initial_H_params, feed_dict, print_progress_interval=-1)
        best_H = self._build_homography(best_H_params)
        
        #since best_H represents the best homography between normalized coordinates,
        #we now compute the homography between the original coordinates.
        best_H = np.dot(np.linalg.inv(N_target), np.dot(best_H, N_source))
        best_H /= best_H[2,2]

        return best_H
    
