import tensorflow as tf
import numpy as np
import functools
import sys
from ...auxillary import planar_geometry, tf_operations
from ...auxillary.tf_optimizer import TFOptimizer

DEBUG = False

class ShapeDistanceMinimizer(object):
    '''
    The goal of this class is to correct a homography matrix in a way that minimizes the distances between
    a list of source shapes and a list of target shapes.
    '''

    def __init__(self, learning_rate, max_epochs, local_minimum_threshold):
        '''
        learning_rate - the step sizes in the gradient direction that we take in each iteration
        max_epochs - the maximum number of iterations
        local_minimum_threshold - we decide that we are in a local minimum if the cost function changes by less than this.
        '''
        self._learning_rate = learning_rate
        self._max_epochs = max_epochs
        self._local_minimum_threshold = local_minimum_threshold
        self._tf_optimizer = TFOptimizer(learning_rate, max_epochs, local_minimum_threshold)

    def initialize_session(self):
        '''
        Set up a TensorFlow session and build the cost function graph.
        '''
        self._sess = tf.Session()

        self._H_ph = tf.placeholder(tf.float32)
        self._source_lines_ph = tf.placeholder(tf.float32)
        self._target_lines_ph = tf.placeholder(tf.float32)
        self._source_ellipses_ph = tf.placeholder(tf.float32)
        self._target_ellipses_ph = tf.placeholder(tf.float32)

        self._lines_cost_tensor = self._lines_cost(self._source_lines_ph, self._target_lines_ph, self._H_ph)
        self._ellipses_cost_tensor = self._ellipses_cost(self._source_ellipses_ph, self._target_ellipses_ph, self._H_ph)

        #this is the cost function we try to minimize
        cost_function = functools.partial(self._homography_cost, self._source_lines_ph, self._target_lines_ph,
                                          self._source_ellipses_ph, self._target_ellipses_ph)
        initial_H_flat = np.zeros((8,), np.float32)

        self._tf_optimizer.initialize_session(self._sess, initial_H_flat, cost_function)

        #print 'source_lines_ph: ', self._source_lines_ph
        sys.stdout.flush()
        return

    def close_session(self):
        '''
        Close the TensorFlow session
        '''
        self._sess.close()

    @classmethod
    def _compute_normalization_matrix(cls, image_shape, shapes):
        '''
        image_shape - a numpy array with length 3 and type np.int32
        shapes - a list of PlanarShape objects

        Return a numpy array with shape (3,3) and type np.float32.
        N is a homgoraphy matrix mapping the original shapes to the normalized shapes.
        '''
        ellipses = [s for s in shapes if s.is_ellipse()]
        
        #if there is a single ellipse, then we normalize the ellipse to be the unit circle.
        if len(ellipses) == 1:
            ellipse = ellipses[0]
            N = planar_geometry.compute_homography_to_unit_circle(ellipse)
            # ellipse.compute_geometric_parameters()
            # center, axes, angle = ellipse.get_geometric_parameters()
            # radius = axes[0]
            # T = planar_geometry.translation_matrix(-center)
            # S = planar_geometry.scaling_matrix(np.ones(2)/radius)
            # N = np.dot(S,T)

        #otherwise, we scale the image to a unit square
        else:
            image_dimensions = np.array([image_shape[1]-1, image_shape[0]-1], np.float32)
            
            #first get a matrix that translates the image center to the origin
            image_center = image_dimensions / 2.0
            T = planar_geometry.translation_matrix(-image_center)

            #then scale by the inverse of the image shape
            scale = 2.0 / image_dimensions
            #print 'scaling by: ', scale
            S = planar_geometry.scaling_matrix(scale)

            N = np.dot(S,T)

        return N

    @classmethod
    def _lines_cost(cls, source_lines, target_lines, H):
        '''
        H - a (3,3) tensor of type np.float32 representing a homography matrix
        lines - a tensor with shape (number of lines, 3) and type np.float32

        Each row of lines represents the parameters (a,b,c) of the line a*x + b*y + c = 0

        Return a scalar with type float32.

        Let G=H^{-1}. 
        For each line, we compute the sum of squares of the difference 
        between G(target_line) and source_line. We return the sum over all lines.
        '''
        source_lines_pred = tf_operations.apply_inverse_homography_to_lines(H, target_lines)

        #normalize the prediction
        source_lines_pred = tf_operations.normalized_vectors(source_lines_pred)
    
        return tf.reduce_sum(tf.pow(source_lines - source_lines_pred, 2))

    @classmethod
    def _ellipse_cost(cls, source_ellipse, target_ellipse, H):
        '''
        H - a (3,3) tensor of type np.float32 representing a homography matrix
        source_ellipse, target_ellipse - a tensor with shape (3, 3) and type np.float32
        
        return a scalar with type float32
        
        Let G=H^{-1}. If E is the source ellipse and E' is the target we should have:
        E = G(E') = (H^T)*E'*H
        
        To measure difference between G(E') and E, we compute:
        (tr(G(E') * (E^{-1})) - 3)^2
        '''    
        source_ellipse_pred = tf_operations.apply_inverse_homography_to_ellipse(H,target_ellipse)

        source_ellipse_pred_norm = tf.sqrt(tf.reduce_sum(tf.pow(source_ellipse_pred, 2)))
        source_ellipse_pred = source_ellipse_pred / source_ellipse_pred_norm

        trace_1 = tf.trace(tf.matmul(source_ellipse_pred, tf.matrix_inverse(source_ellipse)))
        trace_2 = tf.trace(tf.matmul(tf.matrix_inverse(source_ellipse_pred), source_ellipse))

        return tf.pow(trace_1 - 3, 2) + tf.pow(trace_2 - 3, 2)

    @classmethod
    def _ellipses_cost(cls, source_ellipses, target_ellipses, H):
        '''
        H - a (3,3) tensor of type np.float32 representing a homography matrix
        source_ellipse, target_ellipse - a tensor with shape (num_ellipses, 3, 3) and type np.float32
        
        return a scalar with type float32
        '''
        zero_ellipse_cost = lambda: tf.zeros([])
        one_ellipse_cost  = lambda: cls._ellipse_cost(tf.reshape(source_ellipses, [3,3]),
                                                     tf.reshape(target_ellipses, [3,3]), H)
        
        zero_ellipse_pred = tf.equal(tf.shape(source_ellipses)[0], 0)

        ellipse_cost = tf.cond(zero_ellipse_pred, zero_ellipse_cost, one_ellipse_cost)

        return ellipse_cost
    
    @classmethod
    def _homography_cost(cls, source_lines, target_lines, source_ellipses, target_ellipses, H_flat):
        '''
        H_flat - a tf array with shape (8,) and type np.float32
        source_lines, target_lines - a tf array with shape (num lines, 3) and type np.float32
        source_ellipses, target_ellipses - a tf array with shape (num ellipses, 3, 3) and type np.float32.

        return a scalar with type np.float32
        '''

        #first reshape H_flat into a 3x3 matrix with 1 in position (2,2)
        one = np.ones((1,), np.float32)
        H = tf.reshape(tf.concat(axis=0, values=[H_flat, one]), [3,3])

        lines_cost = cls._lines_cost(source_lines, target_lines, H)
        ellipses_cost = cls._ellipses_cost(source_ellipses, target_ellipses, H)

        return lines_cost + ellipses_cost

    @classmethod
    def _normalize(cls, array):
        return np.float32(array / np.linalg.norm(array))

    def _calibrate_pair(self, source_shape_array, target_shape_array, H):
        '''
        source_shape_array, target_shape_array - a list of numpy arrays. Their shapes are either both (3,) or both (3,3).
        H - a numpy array with shape (3,3)

        We check whether H*source_shape or H*(-source_shape) is closer to target_shape (where "close" is defined to
        be the cost associated to (source_shape, target_shape, H).
        If the -source_shape is closer, we multiply source_shape by -1.

        If the shape of the arrays is (3,), we assume they are lines. If it is (3,3), we assume they are ellipses.
        '''
        if DEBUG:
            print '   calibrating pair...'
            
        is_line = source_shape_array.shape == target_shape_array.shape == (3,)
        is_ellipse = source_shape_array.shape == target_shape_array.shape == (3,3)

        if is_line:
            #print '   running line cost tensor...'
            feed_dict={self._source_lines_ph: source_shape_array.reshape((1,3)),
                       self._target_lines_ph: target_shape_array.reshape((1,3)),
                       self._H_ph        : H}
            cost_pos = self._sess.run(self._lines_cost_tensor, feed_dict=feed_dict)

            feed_dict={self._source_lines_ph: -source_shape_array.reshape((1,3)),
                       self._target_lines_ph:  target_shape_array.reshape((1,3)),
                       self._H_ph        : H}
            cost_neg = self._sess.run(self._lines_cost_tensor, feed_dict=feed_dict)
            #print '   done'
            
        elif is_ellipse:
            if DEBUG:
                print '   running ellipse cost tensor...'
            feed_dict={self._source_ellipses_ph: source_shape_array.reshape((1,3,3)),
                       self._target_ellipses_ph: target_shape_array.reshape((1,3,3)),
                       self._H_ph        : H}
            cost_pos = self._sess.run(self._ellipses_cost_tensor, feed_dict=feed_dict)

            feed_dict={self._source_ellipses_ph: -source_shape_array.reshape((1,3,3)),
                       self._target_ellipses_ph: target_shape_array.reshape((1,3,3)),
                       self._H_ph        : H}
            cost_neg = self._sess.run(self._ellipses_cost_tensor, feed_dict=feed_dict)
            #print '   done'

            source_ellipse = source_shape_array.reshape((3,3))
            target_ellipse = target_shape_array.reshape((3,3))
            source_ellipse_pred = np.matmul(H.transpose(), np.matmul(target_ellipse, H))
            source_ellipse_pred = source_ellipse_pred / np.linalg.norm(source_ellipse_pred)    
            #print '   done'
        
        else:
            raise ValueError('the input arrays must be either lines or ellipses')

        if DEBUG:
            print 'pos cost: ', cost_pos, ' neg cost: ', cost_neg
        
        if cost_neg < cost_pos:
            #print 'multiplying by -1'
            source_shape_array *= -1

        return

    def _build_feed_dict(self, source_shapes, target_shapes, H):
        '''
        H - a numpy array with shape (3,3) and type np.float32
        source_shapes, target_shapes - a list of PlanarShape objects
        
        return a dictionary with the following items:
        self._source_lines_ph: a np array with shape (num lines, 3),
        self._target_lines_ph: a np array with shape (num lines, 3),
        self._source_ellipses_ph: a np array with shape (ellipses, 3, 3)
        self._target_ellipses_ph: a np array with shape (ellipses, 3, 3)

        '''

        if DEBUG:
            print 'building cost function...'
        
        #first convert the shapes to numpy arrays
        source_line_arrays    = map(self._normalize,(l.get_parametric_representation()
                                                    for l in source_shapes if l.is_line()))
        source_ellipse_arrays = map(self._normalize,(e.get_parametric_representation()
                                                    for e in source_shapes if e.is_ellipse()))
        target_line_arrays    = map(self._normalize,(l.get_parametric_representation()
                                                    for l in target_shapes if l.is_line()))
        target_ellipse_arrays = map(self._normalize,(e.get_parametric_representation()
                                                    for e in target_shapes if e.is_ellipse()))

        #we multiply each source shape by -1 if this decreases the distance between (H^-1)*source_shape and target_shape
        for source, target in zip(source_line_arrays + source_ellipse_arrays, target_line_arrays + target_ellipse_arrays):
            self._calibrate_pair(source, target, H)

        source_lines = np.vstack(source_line_arrays)
        target_lines = np.vstack(target_line_arrays)

        if len(source_ellipse_arrays) > 0:
            source_ellipses = source_ellipse_arrays[:1]
            target_ellipses = target_ellipse_arrays[:1]

        else:
            source_ellipses = np.zeros((0,3,3))
            target_ellipses = np.zeros((0,3,3))
            
        feed_dict = {self._source_lines_ph: source_lines,
                     self._target_lines_ph: target_lines,
                     self._source_ellipses_ph: source_ellipses,
                     self._target_ellipses_ph: target_ellipses}

        return feed_dict

    @classmethod
    def _compute_shape_matching_errors(cls, H, source_shapes, target_shapes):
        '''
        H - a numpy array with shape (3,3) and type np.float32
        source_shapes, target_shapes - a list of PlanarShape objects

        Return numpy array with shape (num shapes,). The i-th entry is the distance between 
        H*source_shapes[i] and target_shapes[i]
        '''

        return np.array([planar_geometry.apply_homography_to_shape(H, S).compute_shape_similarity(T)
                         for S,T in zip(source_shapes, target_shapes)])
    
    def optimize_homography(self, initial_H, source_image_shape, source_shapes, target_image_shape, target_shapes):
        '''
        initial_H - a numpy array with shape (3,3) and type np.float32
        source_image_shape - a numpy array with length 3 and type np.int32 specifying the shape if the source image
        source_shapes - a list of PlanarShape objects corresponding to shapes on the source image
        target_image_shape - a numpy array with length 3 and type np.int32 specifying the shape of the target image
        target_shapes - a list of PlanarShape objects corresponding to shapes on the target image

        WARNING: We currently assume that there is at most one source/target ellipse
        return a numpy array with shape (3,3) and type np.float32.
        This represents a new homography matrix H which minimizes the distances between H*S_i and T_i for each pair of
        source and target shapes (S_i,T_i).
        '''

        # print 'source_image_shape: ', source_image_shape
        # print 'optimizing homography with source shapes:'
        # print '\n'.join(map(str, source_shapes))
        # print 'and target shapes:'
        # print '\n'.join(map(str, target_shapes))
        # print 'initial H:'
        # print initial_H

        #First normalize the source and target shapes
        #N_source is a (3,3) numpy array which represents the homography matrix taking the source shapes to the
        #normalized source shapes. Similarly for N_target
        N_source = self._compute_normalization_matrix(source_image_shape, source_shapes)
        N_target = self._compute_normalization_matrix(target_image_shape, target_shapes)

        normalized_source_shapes = [planar_geometry.apply_homography_to_shape(N_source, s) for s in source_shapes]
        normalized_target_shapes = [planar_geometry.apply_homography_to_shape(N_target, t) for t in target_shapes]

        if DEBUG:
            print 'N source:'
            print N_source
            print 'N target:'
            print N_target

            print 'normalized source shapes:'
            print '\n'.join(map(str,normalized_source_shapes))
            print 'normalized target shapes:'
            print '\n'.join(map(str,normalized_target_shapes))

        #we also update the initial homography to the new coordinates
        initial_H = np.float32(np.dot(N_target, np.dot(initial_H, np.linalg.inv(N_source))))
        initial_H /= initial_H[2,2]

        if DEBUG:
            print 'intital normalized H:'
            print initial_H
        
        # initial_shape_matching_errors = self._compute_shape_matching_errors(initial_H,
        #                                                                     normalized_source_shapes,
        #                                                                     normalized_target_shapes)
                
        #since the input to the cost function is the length 8 flattened homography matrix,
        #we flatten the initial homography
        #and remove the last element before passing it to the optimizer.
        initial_H_flat = np.float32(initial_H.flatten()[:-1])

        #fill the feed dict with the normalized shapes
        feed_dict = self._build_feed_dict(normalized_source_shapes, normalized_target_shapes, initial_H)

        #feed_dict[self._H_ph] = initial_H_flat
        #print 'ellipses cost:'
        feed_dict[self._H_ph] = initial_H
        #print self._sess.run(self._ellipses_cost_tensor, feed_dict=feed_dict)

        best_H_flat = self._tf_optimizer.minimize(initial_H_flat, feed_dict, print_progress_interval=-1)
        best_H = np.append(best_H_flat, 1).reshape((3,3))

        # print 'initial shape matching errors:'
        # print initial_shape_matching_errors
        # print 'optimized shape matching errors:'
        # print self._compute_shape_matching_errors(best_H, normalized_source_shapes, normalized_target_shapes)
        
        #since best_H represents the best homography between normalized coordinates,
        #we now compute the homography between the original coordinates.
        best_H = np.dot(np.linalg.inv(N_target), np.dot(best_H, N_source))
        best_H /= best_H[2,2]

        return best_H
    
