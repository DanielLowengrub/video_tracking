import tensorflow as tf
import numpy as np
import functools
import collections
from .shape_distance_minimizer import ShapeDistanceMinimizer
from ...auxillary import planar_geometry, tf_operations, world_rotation, camera_operations
from ...auxillary.camera_matrix import CameraMatrix
from ...auxillary.fit_points_to_shape.ellipse import Ellipse

class CameraShapeDistanceMinimizer(ShapeDistanceMinimizer):
    '''
    This class optimizes for a homography that comes from a camera matrix. This means that
    P = C * [R | -RT] and P = H[:,(0,1,3)]
    
    where
    C = [f 0        u]
        [0 aspect*f v]
        [0 0        1]

    R = (Rx * Rz)^-1

    Rz is a rotation in the clockwise direction around the z axis, and Rx is a rotation in the clockwise direction around
    the x axis.

    This means that the camera is represented by 9 variables:
    theta_x, theta_z, focus, aspect, camera_center=(u,v), camera_position=T

    Note that the camera is only uniquly determined by the shapes if some of these parameters are fixed.
    For example, you should fix (camera position and aspect) or (theta_x and aspect).
    '''
    #this records sizes of the various camera parameters.
    #This is used to convert a CameraParameters object to a tensor and vice versa
    CAMERA_PARAMETER_SIZE = camera_operations.CameraParameters(theta_x=1, theta_z=1, focus=1, aspect=1,
                                                               camera_center=2, camera_position=3)
    
    def __init__(self, fixed_parameter_dict, learning_rate, max_epochs, local_minimum_threshold):
        '''
        fixed_parameters - a dictionary with items (camera parameter, np array). The keys of this dict should be a subset of
           CameraParameters._fields
        learning_rate - the step sizes in the gradient direction that we take in each iteration
        max_epochs - the maximum number of iterations
        local_minimum_threshold - we decide that we are in a local minimum if the cost function changes by less than this.
        '''
        super(CameraShapeDistanceMinimizer, self).__init__(learning_rate, max_epochs, local_minimum_threshold)
        
        self._fixed_parameter_dict = fixed_parameter_dict

    def initialize_session(self):
        '''
        Set up a TensorFlow session and build the cost function graph.
        '''
        self._sess = tf.Session()

        self._H_ph = tf.placeholder(tf.float32)
        self._source_lines_ph = tf.placeholder(tf.float32)
        self._target_lines_ph = tf.placeholder(tf.float32)
        self._source_ellipses_ph = tf.placeholder(tf.float32)
        self._target_ellipses_ph = tf.placeholder(tf.float32)
        
        variable_dict = dict()
        for param,size in self.CAMERA_PARAMETER_SIZE._asdict().iteritems():
            trainable = param not in self._fixed_parameter_dict
            if size == 1:
                variable_dict[param] = tf.Variable(tf.zeros([]), trainable=trainable, name=param)
            else:
                variable_dict[param] = tf.Variable(tf.zeros([size]), trainable=trainable, name=param)
                
        self._camera_parameter_variables = camera_operations.CameraParameters(**variable_dict)
        
        #this is the cost function we try to minimize
        self._loss_op = self._camera_parameters_loss(self._camera_parameter_variables,
                                                     self._source_lines_ph, self._target_lines_ph,
                                                     self._source_ellipses_ph, self._target_ellipses_ph)
        self._loss_op = self._loss_op - 100*tf.minimum(self._camera_parameter_variables.aspect, 0)
        
        self._optimizer = tf.train.GradientDescentOptimizer(self._learning_rate).minimize(self._loss_op)

        self._lines_cost_tensor    = self._lines_cost(self._source_lines_ph, self._target_lines_ph, self._H_ph)
        self._ellipses_cost_tensor = self._ellipses_cost(self._source_ellipses_ph, self._target_ellipses_ph, self._H_ph)

        return

    @classmethod
    def _build_C(cls, camera_parameters):
        '''
        camera_parameters - a CameraParameters object that stores tf tensors
        return - a tf array with shape (3,3)
        '''
        f  = camera_parameters.focus
        a  = camera_parameters.aspect
        uo = camera_parameters.camera_center[0]
        vo = camera_parameters.camera_center[1]

        C_flat = tf.stack([f,  0,   uo,
                          0, -a*f, vo,
                          0,  0,   1])
        
        return tf.reshape(C_flat, [3,3])

    @classmethod
    def _build_R(cls, camera_parameters):
        '''
        camera_parameters - a CameraParameters object that stores tf tensors
        return - a tf array with shape (3,3)
        '''
        theta_x = camera_parameters.theta_x
        theta_z = camera_parameters.theta_z

        Rx_inv = tf_operations.rot_x(-theta_x)
        Rz_inv = tf_operations.rot_z(-theta_z)

        return tf.matmul(Rx_inv, Rz_inv)

    @classmethod
    def _normalize_camera_parameters(cls, camera_parameters, N_source, N_target):
        '''
        camera_parameters - a CameraParameters object whose values are np arrays
        N_source - a numpy array with shape (3,3). It represents a transformation of the XY plane
        N_target - a numpy array with shape (3,3). It represents a transformation of the uv plane
        
        return - a CameraParameters object whose values are np arrays
        Let P be the original camera matrix, and let N_source be the (4,4) matrix which fixes the Z axis.

        The output contains the camera parameters for the camera matrix:
        P' = N_target * P * (N_source)^-1
        '''        
        #turn the camera parameters into a camera
        camera = camera_operations.build_camera(camera_parameters)
        P = camera.get_numpy_matrix()

        print 'P:'
        print P
        
        #normalize the camera matrix, and build a camera from it
        normalized_P = np.matmul(N_target, np.matmul(P, np.linalg.inv(N_source)))
        print 'normalized P:'
        print normalized_P
        
        normalized_camera = CameraMatrix(normalized_P)

        #return the parameters of the normalized camera
        return camera_operations.compute_camera_parameters(normalized_camera)

    def _build_homography_matrix(self, camera_parameters):
        '''
        parameter_tensor - a tf tensor with shape _variable_size which contains the camera parameter variables.
        return - a tf array with shape (3,3) representing a homography matrix
        '''
        #build the components of the camera matrix. these are tf tensors
        C = self._build_C(camera_parameters)
        R = self._build_R(camera_parameters)
        T = camera_parameters.camera_position

        RT = tf.matmul(R, tf.reshape(T, [3,1]))
        CR = tf.matmul(C, R)
        CRT = tf.matmul(C,RT)

        #the first 2 columns of the homography matrix
        H01 = CR[:,:2]
        #the last column
        H2 = tf.reshape(-CRT, [3,1])

        H = tf.concat(axis=1, values=[H01, H2])
        H = H / H[2,2]

        self._aspect = camera_parameters.aspect
        self._H = H
        self._C = C
        self._R = R
        self._T = T
        self._RT = RT
        self._CRT = CRT
        return H
    
    def _camera_parameters_loss(self, camera_parameters, source_lines, target_lines, source_ellipses, target_ellipses):
        '''
        camera_parameters
        source_lines, target_lines - a tf array with shape (num lines, 3) and type np.float32
        source_ellipses, target_ellipses - a tf array with shape (num ellipses, 3, 3) and type np.float32.
        parameter_tensor - a tf tensor with size _variable_size

        return a scalar with type np.float32
        '''

        #first build a 3x3 homography matrix out of the camera paramters
        H = self._build_homography_matrix(camera_parameters)
        
        lines_cost    = self._lines_cost(source_lines, target_lines, H)
        ellipses_cost = self._ellipses_cost(source_ellipses, target_ellipses, H)

        return lines_cost + ellipses_cost

    @classmethod
    def _compute_world_normalization_matrix(cls, N_plane, camera_parameters):
        N_world = np.eye(4)
        N_world[:2,:2] = N_plane[:2,:2]
        N_world[3,:2]  = N_plane[2,:2]
        N_world[:2,3]  = N_plane[:2,2]

        T_world = np.eye(4)
#        T_world[2,3] = -camera_parameters.camera_position[2]/2.0
        S_world = np.eye(4)
        S_world[2,2] = 1.0 / camera_parameters.camera_position[2]

        N_world = np.matmul(S_world, np.matmul(T_world, N_world))

        return N_world

    @classmethod
    def _normalize_ellipse_array(cls, E):
        print 'normalizing:'
        ellipse = Ellipse.from_parametric_representation(E)
        center, axes, angle = ellipse.get_geometric_parameters()
        ellipse = Ellipse.from_geometric_parameters(center, axes, angle)
        return ellipse.get_parametric_representation()
    
    def optimize_homography(self, initial_varying_parameter_dict,
                            source_image_shape, source_shapes, target_image_shape, target_shapes):
        '''
        initial_varying_parameter_dict - a dictionary with items (camera parameter, np array). The keys of this dict should
           be the camera parameters that were not keys in the dict fixed_parameter_dict that was passed to the constructor.
        source_image_shape - a numpy array with length 3 and type np.int32 specifying the shape if the source image
        source_shapes - a list of PlanarShape objects corresponding to shapes on the source image
        target_image_shape - a numpy array with length 3 and type np.int32 specifying the shape of the target image
        target_shapes - a list of PlanarShape objects corresponding to shapes on the target image

        return a CameraParameters object

        This represents the parameters whose homography matrix minimizes the distances between H*S_i and T_i for each pair of
        source and target shapes (S_i,T_i).
        '''        
        #First normalize the source and target shapes
        #N_source is a (3,3) numpy array which represents the homography matrix taking the source shapes to the
        #normalized source shapes. Similarly for N_target
        N_source = self._compute_normalization_matrix(source_image_shape, source_shapes)
        N_target = self._compute_normalization_matrix(target_image_shape, target_shapes)

        normalized_source_shapes = [planar_geometry.apply_homography_to_shape(N_source, s) for s in source_shapes]
        normalized_target_shapes = [planar_geometry.apply_homography_to_shape(N_target, t) for t in target_shapes]

        print 'N source:'
        print N_source
        print 'N target:'
        print N_target

        #merge the varying parameter dict and the fixed parameter dict
        pd = self._fixed_parameter_dict.copy()
        pd.update(initial_varying_parameter_dict)
        initial_camera_parameters = camera_operations.CameraParameters(**pd)

        print 'initial camera parameters:'
        print initial_camera_parameters

        # source_ellipse = [s for s in source_shapes if s.is_ellipse()][0]
        # target_ellipse = [s for s in target_shapes if s.is_ellipse()][0]
        # print 'source ellipse: ', source_ellipse
        # print 'target ellipse: ', target_ellipse
        # center, axes, angle = source_ellipse.get_geometric_parameters()
        # source_ellipse = Ellipse.from_geometric_parameters(center, axes, angle)
        # source_ellipse = source_ellipse.get_parametric_representation()
        # target_ellipse = target_ellipse.get_parametric_representation()
        # initial_camera = camera_operations.build_camera(initial_camera_parameters)
        # initial_H = initial_camera.get_homography_matrix()
        # #initial_H = initial_H / initial_H[2,2]
        # source_ellipse_pred = np.matmul(initial_H.transpose(), np.matmul(target_ellipse, initial_H))
        # print 'source ellipse pred (before normalizing): ', source_ellipse_pred
        # source_ellipse_pred = Ellipse.from_parametric_representation(source_ellipse_pred)
        # print 'source ellipse pred: ', source_ellipse_pred
        # center, axes, angle = source_ellipse_pred.get_geometric_parameters()
        # source_ellipse_pred = Ellipse.from_geometric_parameters(center, axes, angle)
        # source_ellipse_pred = source_ellipse_pred.get_parametric_representation()
        
        # #source_ellipse_pred = source_ellipse_pred / np.linalg.norm(source_ellipse_pred)
        # #source_ellipse = source_ellipse / np.linalg.norm(source_ellipse)

        # print 'before normalizing:'
        # print 'initial_H:'
        # print initial_H
        # print 'source_ellipse'
        # print source_ellipse
        # print 'source_ellipse_pred:'
        # print source_ellipse_pred
        # # norm_prod = np.linalg.norm(source_ellipse) * np.linalg.norm(source_ellipse_pred)
        # # tr_s = np.sqrt(np.trace(np.matmul(source_ellipse,source_ellipse))) 
        # # tr_p = np.sqrt(np.trace(np.matmul(source_ellipse_pred,source_ellipse_pred)))
        # # print 'trace: ', np.trace(np.matmul(source_ellipse, source_ellipse_pred)) / norm_prod
        # print 'trace_1: ', np.trace(np.matmul(np.linalg.inv(source_ellipse), source_ellipse_pred))
        # print 'trace_2: ', np.trace(np.matmul(np.linalg.inv(source_ellipse_pred), source_ellipse))

        print 'after normalizing:'
        N_world = self._compute_world_normalization_matrix(N_source, initial_camera_parameters)
        initial_camera_parameters = self._normalize_camera_parameters(initial_camera_parameters, N_world, N_target)
        initial_camera = camera_operations.build_camera(initial_camera_parameters)
        initial_H = initial_camera.get_homography_matrix()

        source_ellipse = [s for s in normalized_source_shapes if s.is_ellipse()][0]
        target_ellipse = [s for s in normalized_target_shapes if s.is_ellipse()][0]
        print 'source ellipse: ', source_ellipse
        print 'target ellipse: ', target_ellipse

        center, axes, angle = source_ellipse.get_geometric_parameters()
        source_ellipse = Ellipse.from_geometric_parameters(center, axes, angle)
        source_ellipse = source_ellipse.get_parametric_representation()

        center, axes, angle = target_ellipse.get_geometric_parameters()
        target_ellipse = Ellipse.from_geometric_parameters(center, axes, angle)
        target_ellipse = target_ellipse.get_parametric_representation()        

        source_ellipse_pred = np.matmul(initial_H.transpose(), np.matmul(target_ellipse, initial_H))
        #print 'source ellipse pred (before scaling):'
        #print source_ellipse_pred
        scaled_source_ellipse_pred = Ellipse.from_parametric_representation(source_ellipse_pred)
        print 'source ellipse pred: ', scaled_source_ellipse_pred
        center, axes, angle = scaled_source_ellipse_pred.get_geometric_parameters()
        scaled_source_ellipse_pred = Ellipse.from_geometric_parameters(center, axes, angle)
        source_ellipse_pred = scaled_source_ellipse_pred.get_parametric_representation()
        # print 'scaled source ellipse pred:'
        # print scaled_source_ellipse_pred
        # i = np.argmax(source_ellipse_pred)
        # alpha = scaled_source_ellipse_pred.flatten()[i] / source_ellipse_pred.flatten()[i]
        # print 'alpha: ', alpha

        # target_ellipse = alpha * target_ellipse
        # source_ellipse_pred = np.matmul(initial_H.transpose(), np.matmul(target_ellipse, initial_H))
        # print 'new source ellipse pred:'
        # print source_ellipse_pred
        
        print 'initial_H:'
        print initial_H
        print 'source_ellipse'
        print source_ellipse
        print 'target_ellipse:'
        print target_ellipse
        print 'source_ellipse_pred:'
        print source_ellipse_pred
        # norm_prod = np.linalg.norm(source_ellipse) * np.linalg.norm(source_ellipse_pred)
        # tr_s = np.sqrt(np.trace(np.matmul(source_ellipse,source_ellipse))) 
        # tr_p = np.sqrt(np.trace(np.matmul(source_ellipse_pred,source_ellipse_pred)))
        # print 'trace: ', np.trace(np.matmul(source_ellipse, source_ellipse_pred)) / norm_prod
        norm_s = np.linalg.norm(source_ellipse)
        norm_p = np.linalg.norm(source_ellipse_pred)
        print '||source_ellipse|| = ', norm_s
        print '||source_ellipse_pred|| = ', norm_p
        print 'trace_1: ', np.trace(np.matmul(np.linalg.inv(source_ellipse), source_ellipse_pred))*norm_s/norm_p
        print 'trace_2: ', np.trace(np.matmul(np.linalg.inv(source_ellipse_pred), source_ellipse))*norm_p/norm_s


        #N_world = self._compute_world_normalization_matrix(N_source, initial_camera_parameters)

        #print 'N_world:'
        #print N_world
        
        #normalize the initial camera parameters
        #initial_camera_parameters = self._normalize_camera_parameters(initial_camera_parameters, N_world, N_target)

        print 'normalized parameters:'
        print initial_camera_parameters
        
        #initial_camera = camera_operations.build_camera(initial_camera_parameters)
        #initial_H = initial_camera.get_homography_matrix()
        #initial_H = initial_H# / initial_H[2,2]
        print 'normalized H:'
        print initial_H

        feed_dict = self._build_feed_dict(normalized_source_shapes, normalized_target_shapes, initial_H)

        
        best_camera_parameters = self._minimize_camera_parameters(initial_camera_parameters,
                                                                  feed_dict, print_progress_interval=1)

        print 'best camera parameters:'
        print best_camera_parameters
        
        #since best_camera_parameters represents a camera in normalized coordinates,
        #we now compute the parameters representing a camera in original coords
        best_camera_parameters = self._normalize_camera_parameters(best_camera_parameters, np.linalg.inv(N_world),
                                                                   np.linalg.inv(N_target))

        return best_camera_parameters
    
    def _minimize_camera_parameters(self, initial_camera_parameters, feed_dict, print_progress_interval=-1):
        '''
        initial_camera_parameters - a CameraParameters object
        feed_dict - a dictionary with items (placeholder, np array)
        '''

        #assign the initial values to the camera paramter variables
        for param,param_variable in self._camera_parameter_variables._asdict().iteritems():
            initial_value = getattr(initial_camera_parameters, param)
            assign_op = tf.assign(param_variable, initial_value)
            self._sess.run(assign_op)

        print 'initial cost: ', self._sess.run(self._loss_op, feed_dict=feed_dict)
        print 'camera parameters:'
        for param,param_variable in self._camera_parameter_variables._asdict().iteritems():
            print '%s: %s' % (param, self._sess.run(param_variable))

        print 'H:'
        print self._sess.run(self._H, feed_dict=feed_dict)
        print 'C:'
        print self._sess.run(self._C, feed_dict=feed_dict)
        print 'R:'
        print self._sess.run(self._R, feed_dict=feed_dict)
        print 'T:'
        print self._sess.run(self._T, feed_dict=feed_dict)
        print 'RT:'
        print self._sess.run(self._RT, feed_dict=feed_dict)
        print 'CRT:'
        print self._sess.run(self._CRT, feed_dict=feed_dict)

        prev_training_cost = 0.0
        for epoch_i in range(self._max_epochs):
            self._sess.run(self._optimizer, feed_dict=feed_dict)
            training_cost = self._sess.run(self._loss_op, feed_dict=feed_dict)

            if (print_progress_interval > 0) and (epoch_i % print_progress_interval == 0):
                #print the cost after this step
                print '\n-----------------------'
                print 'summary of epoch %d:' % epoch_i
                print '   training cost: %f' % training_cost
                print 'camera parameters:'
                for param,param_variable in self._camera_parameter_variables._asdict().iteritems():
                    print '%s: %s' % (param, self._sess.run(param_variable))

                print 'H:'
                print self._sess.run(self._H, feed_dict=feed_dict)
                print 'C:'
                print self._sess.run(self._C, feed_dict=feed_dict)
                print 'R:'
                print self._sess.run(self._R, feed_dict=feed_dict)
                print 'T:'
                print self._sess.run(self._T, feed_dict=feed_dict)
                print 'RT:'
                print self._sess.run(self._RT, feed_dict=feed_dict)
                print 'CRT:'
                print self._sess.run(self._CRT, feed_dict=feed_dict)

                # current_value = self._sess.run(self._optimized_var, feed_dict=feed_dict)
                # print '   current value: '
                # print current_value

            # Allow the training to quit if we've reached a local minimum
            if np.abs(prev_training_cost - training_cost) < self._local_minimum_threshold:
                break
            
            prev_training_cost = training_cost

        #get the final optimized parameters
        optimized_camera_parameter_dict = dict()
        for param,param_variable in self._camera_parameter_variables._asdict().iteritems():
            optimized_camera_parameter_dict[param] = self._sess.run(param_variable)

        optimized_camera_parameters = camera_operations.CameraParameters(**optimized_camera_parameter_dict)

        return optimized_camera_parameters
    
