import numpy as np
from ....source.auxillary.planar_geometry import rotation_matrix, apply_homography_to_line, apply_homography_to_ellipse

class EllipseHomographyCalculator(object):
    '''
    Given a triple of Ellipse, Line, Point, this class computes a homography matrix that sends the ellipse to the unit circle at the origin, the line to x=0, and the point
    to (0,0)
    '''
    PERMUTATION = np.array([[0, 0, 1],
                            [0, 1, 0],
                            [1, 0, 0]])
    
    @classmethod
    def _compute_H(cls, ellipse):
        '''
        Return a 3x3 matrix H such that H = O^Tdiag(1/sqrt(l1),..,1/sqrt(l3)) where l1 >= l2 >= l3 are the eigen values of the parametric representation of A.
        '''

        C = np.float64(ellipse.get_parametric_representation())
        vals, O = np.linalg.eigh(C)

        #fix O so that O^T*diag(vals)*O = C
        O = O.transpose()

        #swap the first and last eigenvalus
        vals = vals[::-1]
        O = np.dot(cls.PERMUTATION, O)

        H = np.dot(O.transpose(), np.diag(1 / np.sqrt(np.abs(vals))))

        return H

    @classmethod
    def _compute_R(cls, alpha):
        return np.array([[np.cos(alpha), np.sin(alpha), 0],
                         [-np.sin(alpha), np.cos(alpha),0],
                         [0,              0,            1]])
    
    classmethod
    def _compute_B(cls, a):
        return np.array([[np.sqrt(1 + (a**2)), 0, a],
                         [0, 1                  , 0],
                         [a, 0, np.sqrt(1 + (a**2))]])

    
    def compute_homography(self, ellipse, line, point):
        '''
        ellipse - an Ellipse object
        line - a Line object
        point - a point

        Return the 2 unique homography matricies H1,H2 that map the unit circle to the ellipse, the line x=0 to the line, and (0,0) to the point.
        
        '''

        H = self._compute_H(ellipse)
        x = np.dot(np.linalg.inv(H), np.append(point, 1))

        print 'H: '
        print H
        print 'x: ', x

        inverse_ellipse = apply_homography_to_ellipse(np.linalg.inv(H), ellipse)
        inverse_ellipse.compute_geometric_parameters()
        center, axes, angle = inverse_ellipse.get_geometric_parameters(in_degrees=True)
        print 'inverse_ellipse: center = %s, axes = %s, angle = %s' % (center, axes, angle)

        #compute alpha and a
        alpha = np.arctan2(-x[1], x[0])
        a = np.sqrt( (x[0]**2 + x[1]**2) / (x[2]**2 - x[0]**2 - x[1]**2) )

        #compute the rotation and scaling matrices
        R_alpha = self._compute_R(alpha)
        B = self._compute_B(a)

        l = np.dot(B.transpose(), np.dot(R_alpha.transpose(), np.dot(H.transpose(), line.get_parametric_representation())))

        print 'l: ', l

        #since we only know l up to +-, there are two possibilities for beta
        beta1 = np.arctan2(-l[1], l[0])
        beta2 = beta1 - np.pi
        
        R_beta1 = self._compute_R(beta1)
        R_beta2 = self._compute_R(beta2)

        print 'alpha: ', alpha
        print 'beta1: ', beta1
        print 'beta2: ', beta2
        print 'a: ', a
        return np.dot(H, np.dot(R_alpha, np.dot(B, R_beta1))), np.dot(H, np.dot(R_alpha, np.dot(B, R_beta2)))

        
