import numpy as np
import cv2
import itertools
from ...auxillary import planar_geometry
from ...auxillary.fit_points_to_shape.line import Line
from ...auxillary import quadratic_linear_solver
from ....testers.auxillary import primitive_drawing

#EFFICIENCY NOTES:
#1) Instead of transforming lines one by one which involves a matrix inversion, do the inversion once, and transform them in one call.
# Do the same thing for all "apply homography _ " calls.
class IwasawaHomographyCalculator(object):
    '''
    This class uses the Iwasawa decomposition to compute a homography matrix given:
    1) An ellipse circle correspondnence
    2) A line that maps to a line passing through the center of the circle
    3) A point on the line that maps to the center of the circle
    '''

    EPSILON = 10**(-5)
    W_THRESH = 1
    PERMUTATION = np.array([[0, 1, 0],
                            [0, 0, 1],
                            [1, 0, 0]])

    CENTER_POINT_DEGENERACY_THRESHOLD = 10**(-1)
    ABS_R_RANGE = (0.1,10)
    
    @classmethod
    def _compute_K(cls, theta):
        return np.array([[np.cos(theta), -np.sin(theta), 0],
                         [np.sin(theta), np.cos(theta),  0],
                         [0,             0,              1]])

    @classmethod
    def _compute_A(cls, r):
        a = 0.5 * r
        b = 0.5 * 1/r
        return np.array([[1,   0,    0],
                         [0,  a+b, -a+b],
                         [0, -a+b,  a+b]])

    @classmethod
    def _compute_N(cls, x):
        a = 0.5*(x**2)
        return np.array([[ 1, x,    x],
                         [-x, 1-a, -a],
                         [ x, a,  1+a]])

    
    @classmethod
    def _compute_H(cls, ellipse):
        '''
        Return the 3x3 matrix H = O^Tdiag(1/sqrt(l1),..,1/sqrt(l3)) where l1,l2 >= l3 are the eigen values of the parametric representation of the ellipse and
        O the the orthogonal basis diagonalizing A.
        '''

        C = ellipse.get_parametric_representation()

        vals, O = np.linalg.eigh(C)

        #if only one eigen value is positive, replace C with -C
        if sum(vals > 0) == 1:
            C = -C
            vals, O = np.linalg.eigh(C)
        
        #fix O so that O^T*diag(vals)*O = C
        O = O.transpose()

        #swap the first and last eigenvalues
        vals = vals[np.array([1,2,0])]
        O = np.dot(cls.PERMUTATION, O)

        H = np.dot(O.transpose(), np.diag(1 / np.sqrt(np.abs(vals))))

        # print 'H^T*C*H = '
        # print np.dot(H.transpose(), np.dot(C, H))

        # print 'vals = ', vals
        return H

    def _r_is_degenerate(self, r):
        '''
        return True iff the value if r is degenerate
        '''
        return (np.abs(r) < self.ABS_R_RANGE[0]) or (np.abs(r) > self.ABS_R_RANGE[1])
    
    def _compute_theta_alpha_pairs(self, H, center_line):
        '''
        H - a homography matrix that takes the center circle to the target ellipse
        center_line - a Line object which is the image of the line x=0.

        We return a list of tuples (theta, alpha).
        For each tuple, there exist r and x such that:
        1) alpha=r*x and 
        2) H * K(theta) * A(r) * N(x)is a homography that takes the center circle to the given ellipse, and also takes the line (x=0) to the given line.

        So using the center line, we reduce the problem to only one parameter. Namely, we only need to find r and x such that alpha=rx.
        '''
        L = center_line.get_parametric_representation()

        # print 'computing theta,alpha pairs with:'
        # print 'L = ', L
        
        #v is the preimage of the given line.
        v = np.dot(H.transpose(), L)

        # print ' '*3 + 'H^T*L = v = ', v
        #We now try to find K(theta), A(r), N(x) such that HKAN maps the center line to (x=0)
        #First consider the equation mu*(HKAN)^T*L = (1,0,0) => mu*v = (KAN)^{-T}*(1,0,0)
        #Since the top left 2x2 block of K is a rotation matrix, the norm of (AN)^{-T}(1,0) is equal to the norm of mu*v[0:2]
        #this implies the following constraint on mu:
        denom = v[0]**2 + v[1]**2 - v[2]**2
        mu_squared = 1 / denom

        #so the two possible values of mu
        mu_1 = np.sqrt(1/np.abs(denom))
        mu_2 = -mu_1

        #for each value of mu, we can compute the rotation matrix in the upper left 2x2 block of K.
        #In other words, we compute theta such that K=K(theta)
        #We also compute the product alpha = r*x
        theta_alpha_pairs = []
        
        for mu in (mu_1, mu_2):
            # print 'computing theta alpha with mu= ', mu
            
            #by the last row of the equality K^{-T}*(AN)^{-T}(1,0,0) = mu*v we see that -rx = mu*vz
            alpha = -mu * v[2]
            
            #theta is the angle between (mu*vz, 1) and (mu*vy, mu*vx)
            cos_theta = (mu_squared*v[1]*v[2] + mu*v[0]) / (1 + mu_squared*(v[2]**2))
            theta = np.arccos(cos_theta)

            #to compute the sign, we use the fact that it rotates (mu*vz,1) to (mu*vy, mu*vx)
            #so we check the projection of (mu*vy, mu*vx) onto perp(mu*vz, 1)
            projection = mu*v[1] - mu_squared*v[0]*v[2]
            if projection > self.EPSILON:
                #print 'the angle is convex, adding theta'
                theta_alpha_pairs.append((theta,alpha))

            elif projection < -self.EPSILON:
                #print 'the angle is concave, adding -theta'
                theta_alpha_pairs.append((-theta,alpha))

            else:
                #print 'adding both theta and -theta'
                theta_alpha_pairs += [(theta,alpha), (-theta,alpha)]

            
        return theta_alpha_pairs

    def _build_system_of_equations(self, H, theta, alpha, source_line, target_line):
        '''
        H - a homography matrix
        theta, alpha - floats
        source_line, target_line - Line objects

        H takes the center unit circle to an ellipse, and there exist r,x such that r*x = alpha and
        H*K(theta)*A(r)*N(x) maps the line (x=0) to a given line.

        The requirement that H*K(theta)*A(r)*N(x) maps the source line to the target line imposes constraints on r and x.
        Specifically, it imposes three equations of the form:
        Q_i(r) + a_i*lambda = 0,  0 <= i <= 2

        where Q(r) is a degree 2 polynomial in r and a is a float.

        We return a tuple (Q, a) where Q is a 3x3 numpy array and a is a length 3 numpy array such that
        Q_i(r) +a_i*lambda = Q[i,0] + Q[i,1]*r + Q[i,2]*r^2 + a[i]*lambda
        '''
        K = self._compute_K(theta)
        s = source_line.get_parametric_representation()
        t = target_line.get_parametric_representation()

        # print 'building systems of equations with:'
        # print 'H = '
        # print H
        # print 'theta = ', theta
        # print 'alpha = ', alpha
        # print 't = ', t
        # print 's = ', s
        
        #Since HKAN is supposed to map the source line to the target line, there exists a lambda such that
        #(HKAN)^{-T} * s = lambda*t => (HK)^{-T} * (AN)^{-T} * s = lambda*t =>
        #(AN)^{-T} * s= lambda * K^T * H^T * t
        #
        #So we replace t with K^T * H^T * t, and try to solve (A(r)*N(x))^{-T} * s = lambda * t
        t = np.dot(K.transpose(), np.dot(H.transpose(), t))
        #t = t / np.linalg.norm(t[:2])
        
        # print 'K^T * H^T * t = ', t
        
        #Writing out (A(r) * N(x))^{-T} explicitly and multiplying both sides by r shows that we have the following equalities:
        #s0*r + alpha*(s1-s2) - t0*lambda = 0
        #0.5*(s1 + s2)*r^2 - s0*alpha*r - 0.5*(alpha^2 - 1)(s1-s2) - t1*lambda = 0
        #0.5*(s1 + s2)*r^2 - s0*alpha*r - 0.5*(alpha^2 + 1)(s1-s2) - t2*lambda = 0

        Q = np.array([[alpha*(s[1]-s[2]), s[0], 0],
                      [-0.5*(alpha**2 - 1)*(s[1]-s[2]), -s[0]*alpha, 0.5*(s[1] + s[2])],
                      [-0.5*(alpha**2 + 1)*(s[1]-s[2]), -s[0]*alpha, 0.5*(s[1] + s[2])]])

        a = -t
        #Q[2,:] *= 10
        #a[2] *= 10
        return (Q, a)
        
    def _compute_theta_r_x(self, H, theta, alpha, line_correspondences):
        '''
        H - a homography matrix
        theta, alpha - floats
        line_correspondences - a list of tuples (source Line, target Line)

        H takes the center unit circle to an ellipse, and there exist r,x such that r*x = alpha and
        H*K(theta)*A(r)*N(x) maps the line (x=0) to a given line.

        In this method we use the line correspondences to determine the values of r and x.

        We return a tuple (theta, r, x)
        '''

        #Each line correspondence gives us a system of equations which is quadratic in r, and linear in an auxillary variable lamda.
        #We construct this system for each correspondence, and then solve it using the quadratic_linear solver
        # print 'building systems of equations...'
        systems_of_equations = [self._build_system_of_equations(H, theta, alpha, source_line, target_line) for source_line, target_line in line_correspondences]

        # print 'we got:'
        # for i, (Q,a) in enumerate(systems_of_equations):
            # print '   system %d:' % i
            # print '   Q:'
            # print Q
            # print '   a: ', a
            
        #solve the system to get the variable r, and a list of the values of the auxillary variables lambda_1,...,lambda_m
        #since there is more than one possible solution, we record a list of possible solutions
        r_and_lambdas_list = quadratic_linear_solver.solve_least_squares(systems_of_equations)

        theta_r_x_tuples = []
        for r_and_lambdas in r_and_lambdas_list:
            r = r_and_lambdas[0]

            #check if r is degenerate
            if self._r_is_degenerate(r):
                continue
            
            lambdas = r_and_lambdas[1:]
            x = alpha / r

            K = self._compute_K(theta)
            A = self._compute_A(r)
            N = self._compute_N(x)
            AN_inverse_T = np.linalg.inv(np.dot(A,N)).transpose()
            HK = np.dot(H,K)
            HK_T = np.dot(H,K).transpose()
            HKAN_T = np.dot(H, np.dot(K, np.dot(A,N))).transpose()
            HKAN_inverse_T = np.linalg.inv(HKAN_T)
            
            # print 'r * (AN^{-T})*s'
            # print 'validating least squares solutions with theta=%f, r=%f, lambda=%s' % (theta, r, str(lambdas))
            # for i, (Q,a) in enumerate(systems_of_equations):
            #     print ' '*3 + 'checking system %d' % i
            #     print ' '*3 + 'Q * (1,r,r^2)^T + a * lambda = ', np.dot(Q, np.array([1,r,r**2])) + (a * lambdas[i])

            # print '||HK|| = ', np.linalg.norm(HK, ord=2)
            # print '||HK^-1|| = ', np.linalg.norm(np.linalg.inv(HK), ord=2)
            # print '||H^-1|| = ', np.linalg.norm(np.linalg.inv(H), ord=2)
            # print '||K^-1|| = ', np.linalg.norm(np.linalg.inv(K), ord=2)
            # print '||AN|| = ', np.linalg.norm(np.dot(A,N), ord=2)            

            # print 'validating with the line correspondences.'
            # print 'The i-th line correspondence should give the same error as the i-th system of equations.'
            # for i, (source_line, target_line) in enumerate(line_correspondences):
            #     print ' '*3 + 'checking line correspondence %d' % i
            #     s = source_line.get_parametric_representation()
            #     t = target_line.get_parametric_representation()
            #     print 'r*((AN)^{-T}*s) = ', r * (np.dot(AN_inverse_T,s))
            #     print '(HK)^T*t = ', np.dot(HK_T, t)
            #     print 'r*((AN)^{-T}*s) - lambda_i*(HK)^T*t = ', r * (np.dot(AN_inverse_T,s)) - lambdas[i]*np.dot(HK_T, t)
            #     print 'r*s = ', r*s
            #     print 'lambda_i*(HKAN)^T*t   = ', lambdas[i]*np.dot(HKAN_T, t)
            #     print 'r*(HKAN^{-T})*s  = ', r*np.dot(HKAN_inverse_T, s)
            #     print 'lambda_i*t = ', lambdas[i]*t
            #     print '||r*s - lambda_i*(HKAN)^T*t||         = ', np.linalg.norm(r*s - lambdas[i]*np.dot(HKAN_T, t))
            #     print '||r*((HKAN)^{-T}*s) - lambda_i*t||    = ', np.linalg.norm(r*np.dot(HKAN_inverse_T, s) - lambdas[i]*t)
            #     print '||r*((AN)^{-T}*s) - lambda_i*(HK)^T*t|| = ', np.linalg.norm(r * (np.dot(AN_inverse_T,s)) - lambdas[i]*np.dot(HK_T, t))
            #if r is too small then the view is degenerate so return None
            if abs(r) < self.EPSILON:
                return []
        
            x = alpha / r

            theta_r_x_tuples.append((theta, r, x))
            
        return theta_r_x_tuples
        
    def _compute_homography_matrices(self, ellipse, center_line, line_correspondences):
        '''
        ellipse - an Ellipse that should be the image of the unit circle
        center_line - a Line that should be the image of (x=0).
        line_correspondences - a list of tuples (source line, target line)

        Return a the 4 homography matrices that map the unit circle to the ellipse, (x=0) to the line, induces the line correspondences.
        '''
        
        #get a matrix H such that H^T * C * H = [[1,0,0],[0,1,0],[0,0,-1]]
        #this matrix maps the circle to the ellipse
        H = self._compute_H(ellipse)
        #H = H / H[2,2]
        
        #Get a list of tuples (theta, alpha) such that there exist r,x with r*x = alpha such that the homography H*K(theta)*A(r)*N(x)
        #takes the center circle to the ellipse, AND the line (x=0) to the given center line.
        # print 'computing theta alpha pairs...'
        theta_alpha_pairs = self._compute_theta_alpha_pairs(H, center_line)

        # print 'got %d pairs. testing...' % len(theta_alpha_pairs)
        # print '   the target center line is: ', str(center_line)
        # source_center_line = Line(1,0,0)
        # for theta, alpha in theta_alpha_pairs:
        #     print '   testing theta=%f, alpha=%f with r=1' % (theta, alpha)
        #     K = self._compute_K(theta)
        #     A = self._compute_A(1)
        #     N = self._compute_N(alpha)
        #     HKAN = np.dot(H, np.dot(K, np.dot(A,N)))

        #     transformed_line = planar_geometry.apply_homography_to_line(HKAN, source_center_line)
        #     print '   the transformed y axis is: ', transformed_line

            
        #For each pair (alpha, theta), we use the line correspondences to compute r and x.
        # print 'computing theta, r, x tuples...'
        theta_r_x_tuples = list(itertools.chain(*(self._compute_theta_r_x(H, theta, alpha, line_correspondences) for theta,alpha in theta_alpha_pairs)))

        # print 'got %d tuples. testing...' % len(theta_r_x_tuples)
        # for theta, r, x in theta_r_x_tuples:
        #     print ' '*3 + 'testing theta=%f, r=%f, x=%f' % (theta, r, x)
        #     K = self._compute_K(theta)
        #     A = self._compute_A(r)
        #     N = self._compute_N(x)
        #     HKAN = np.dot(H, np.dot(K, np.dot(A,N)))

        #     for i, (source_line, target_line) in enumerate(line_correspondences):
        #         print ' '*6 + 'checking line correpondence number %d' % i
        #         transformed_source_line = planar_geometry.apply_homography_to_line(HKAN, source_line)
        #         print ' '*6 + 'transformed source: %s' % str(transformed_source_line)
        #         print ' '*6 + 'target:             %s' % str(target_line)
                
        #finally, for each triple (theta, r, x), compute a homography matrix
        homography_matrices = []
        for theta, r, x in theta_r_x_tuples:
            K = self._compute_K(theta)
            A = self._compute_A(r)
            N = self._compute_N(x)
            KAN = np.dot(K, np.dot(A, N))

            HKAN = np.dot(H, KAN)
            homography_matrices.append(HKAN)

        return homography_matrices

    def _compute_shape_matching_errors(self, target_image, homography_matrices, line_correspondences):
        '''
        target_image - numpy array with shape (n,m,3) and type np.uint8
        homography_matrices - a list of homography matrices from source to target
        line_correspondences - a list of tuples (source line, target line)

        For each homography matrix, compute the similarity of the target lines and the transformed source lines.
        Return the errors as a numpy array with shape (num homography matrices, num line correpondences)
        '''

        #We first scale the target lines so that they lie in a unit square, and are centered around the origin
        #This is done so that the radii of the lines is of the same order of magnitude as the angles of the lines.
        S = np.diag(np.array([2.0/target_image.shape[1], 2.0/target_image.shape[0], 1]))
        T = np.eye(3)
        T[:2,2] = -np.array([target_image.shape[1], target_image.shape[0]])/2.0
        ST = np.dot(S,T)
        # print 'T:'
        # print T
        # print 'ST:'
        # print ST
        
        scaled_homography_matrices = [np.dot(ST,H) for H in homography_matrices]
        
        scaled_transformed_source_shapes_list = [[planar_geometry.apply_homography_to_line(scaled_H, s)
                                                  for s,t in line_correspondences]
                                                 for scaled_H in scaled_homography_matrices]
        
        scaled_target_shapes = [planar_geometry.apply_homography_to_line(ST, t) for s,t in line_correspondences]

        #for each homography matrix, compute the errors between the transformed source lines, and the target lines
        errors = np.array([[target_line.compute_shape_similarity(source_line)
                            for source_line, target_line in zip(scaled_transformed_source_shapes, scaled_target_shapes)]
                          for scaled_transformed_source_shapes in scaled_transformed_source_shapes_list])

        return errors

    def _compute_scaling_matrix(self, ellipse, line_through_center):
        '''
        ellipse - an ellipse object
        line_through_center - a line object that passes through the center of the ellipse

        Return a homography M that transforms the ellipse to the unit circle and the line to the y axis
        '''
        # print 'computing scaling matrix...'
        
        #first get a homography matrix transforming the ellipse to the unit circle
        M = planar_geometry.compute_homography_to_unit_circle(ellipse)

        # print 'M:'
        # print M
        
        #now get the image of the line
        transformed_line = planar_geometry.apply_homography_to_line(M, line_through_center)

        #get a rotation matrix rotating the line to the y axis
        L = line_through_center.get_parametric_representation()
        H = np.diag(np.ones(3))
    
        #Rotate the normal vector L[:2] to the vector (1,0).
        v = L[:2]

        #if the normal vector is pointing in the -x direction, replace it with -v so that we don't change the orientation
        if v[0] < 0:
            v *= -1
            
        v_perp = planar_geometry.perp(v)
        R = np.vstack([v, v_perp]).transpose()
        H[:2,:2] = np.linalg.inv(R)

        #finally, compose M with H
        return np.dot(H, M)
        return M
    
    def compute_homography(self, source_image, target_image, ellipse_correspondence, line_through_center_correspondence, line_correspondences):
        '''
        ellipse_correspondence - a tuple (source ellipse, target ellipse).
        line_through_center_correspondence - a tuple (source line, target line). Both the source line and the target line pass through the center of the ellipses
        line_correspondences - a list of tuples (source line, target line)

        Return a tuple of tuples (H, errors) where H is a homography matrix, and errors is a list of the errors for each of the line correspondences. I.e,
        For each line correspondence (source line, target line) it stores the difference between H*source_line and target_line.
        '''

        #We first find a homography M_source that transforms the source ellipse to the unit circle
        #The final homography will by the composition of M, with a homography H
        M = self._compute_scaling_matrix(ellipse_correspondence[0], line_through_center_correspondence[0])
        
        # print 'homography to unit circle:'
        # print M
        
        #transform the source ellipse and center line by M
        scaled_ellipse_correspondence = (planar_geometry.apply_homography_to_ellipse(M, ellipse_correspondence[0]), ellipse_correspondence[1])
        scaled_line_through_center_correspondence = (planar_geometry.apply_homography_to_line(M, line_through_center_correspondence[0]), line_through_center_correspondence[1])
        scaled_line_correspondences = [(planar_geometry.apply_homography_to_line(M, source_line), target_line) for source_line, target_line in line_correspondences]

        # print 'scaled line though center correspondence:'
        # print map(str, scaled_line_through_center_correspondence)

        # print 'original line correspondences: '
        # print ', '.join([('(%s, %s)' % tuple(map(str, lc))) for lc in line_correspondences])
        # print 'scaled line correspondences: '
        # print ', '.join([('(%s, %s)' % tuple(map(str, lc))) for lc in scaled_line_correspondences])
        
        homography_matrices = self._compute_homography_matrices(scaled_ellipse_correspondence[1], scaled_line_through_center_correspondence[1], scaled_line_correspondences)

        #the actual homography matrices are obtained by composing with M
        homography_matrices = [np.dot(H,M) for H in homography_matrices]
        homography_matrices = [H / H[2,2] for H in homography_matrices]
        
        # print 'we got the homography matrices:'
        # print '\n'.join(map(str, homography_matrices))

        #we now compute the errors for each homography matrix
        errors = self._compute_shape_matching_errors(target_image, homography_matrices, line_correspondences)
    
        return homography_matrices, errors
        
    
