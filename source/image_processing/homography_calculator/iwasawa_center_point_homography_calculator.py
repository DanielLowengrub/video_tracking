import numpy as np
import cv2
from ...auxillary import planar_geometry
from ...auxillary.fit_points_to_shape.line import Line
from ....testers.auxillary import primitive_drawing

#EFFICIENCY NOTES:
#1) Instead of transforming lines one by one which involves a matrix inversion, do the inversion once, and transform them in one call.
# Do the same thing for all "apply homography _ " calls.
class IwasawaCenterPointHomographyCalculator(object):
    '''
    This class uses the Iwasawa decomposition to compute a homography matrix given:
    1) An ellipse circle correspondnence
    2) A line that maps to a line passing through the center of the circle
    3) A point on the line that maps to the center of the circle
    '''

    EPSILON = 10**(-5)
    W_THRESH = 1
    PERMUTATION = np.array([[0, 1, 0],
                            [0, 0, 1],
                            [1, 0, 0]])

    CENTER_POINT_DEGENERACY_THRESHOLD = 10**(-1)
    
    @classmethod
    def _compute_K(cls, theta):
        return np.array([[np.cos(theta), -np.sin(theta), 0],
                         [np.sin(theta), np.cos(theta),  0],
                         [0,             0,              1]])

    @classmethod
    def _compute_A(cls, r):
        a = 0.5 * r
        b = 0.5 * 1/r
        return np.array([[1,   0,    0],
                         [0,  a+b, -a+b],
                         [0, -a+b,  a+b]])

    @classmethod
    def _compute_N(cls, x):
        a = 0.5*(x**2)
        return np.array([[ 1, x,    x],
                         [-x, 1-a, -a],
                         [ x, a,  1+a]])

    
    @classmethod
    def _compute_H(cls, ellipse):
        '''
        Return the 3x3 matrix H = O^Tdiag(1/sqrt(l1),..,1/sqrt(l3)) where l1,l2 >= l3 are the eigen values of the parametric representation of the ellipse and
        O the the orthogonal basis diagonalizing A.
        '''

        C = ellipse.get_parametric_representation()

        vals, O = np.linalg.eigh(C)

        #if only one eigen value is positive, replace C with -C
        if sum(vals > 0) == 1:
            C = -C
            vals, O = np.linalg.eigh(C)
        
        #fix O so that O^T*diag(vals)*O = C
        O = O.transpose()

        #swap the first and last eigenvalues
        vals = vals[np.array([1,2,0])]
        O = np.dot(cls.PERMUTATION, O)

        H = np.dot(O.transpose(), np.diag(1 / np.sqrt(np.abs(vals))))

        print 'H^T*C*H = '
        print np.dot(H.transpose(), np.dot(C, H))

        return H
    
    def _compute_homography_matrices(self, ellipse, line, point):
        '''
        ellipse - an Ellipse that should be the image of the unit circle
        line - a Line that should be the image of (x=0).
        point - a point that should be the image of the center of the circle

        Return a the 4 homography matrices that map the unit circle to the ellipse, (x=0) to the line, and (0,0) to the point.
        '''
        L = line.get_parametric_representation()
        P = np.append(point, 1)

        print 'computing homography with line: ', L
        print 'and with point: ', P
        
        #get a matrix H such that H^T * C * H = [[1,0,0],[0,1,0],[0,0,-1]]
        #this matrix maps the circle to the ellipse
        H = self._compute_H(ellipse)

        #u is the preimage of (0,0) under H
        #v is the preimage of the given line.
        H_inv = np.linalg.inv(H)
        u = np.dot(H_inv, P)
        v = np.dot(H.transpose(), L)

        #We now try to find K(theta), A(r), N(x) such that HKAN maps the line to (x=0) and the point to (0,0)
        #First consider the equation mu*(HKAN)^T*L = (1,0,0) => mu*v = (KAN)^{-T}*(1,0,0)
        denom = v[0]**2 + v[1]**2 - v[2]**2
        mu_squared = 1 / denom

        #the two possible values of mu
        mu_1 = np.sqrt(1/np.abs(denom))
        mu_2 = -mu_1

        #for each mu, find the corresponding values theta
        mu_theta_pairs = []
        for mu in (mu_1, mu_2):
            #theta is the angle between (mu*vz, 1) and (mu*vy, mu*vx)
            cos_theta = (mu_squared*v[1]*v[2] + mu*v[0]) / (1 + mu_squared*(v[2]**2))
            theta = np.arccos(cos_theta)

            #check the projection of (mu*vy, mu*vx) onto perp(mu*vz, 1)
            projection = mu*v[1] - mu_squared*v[0]*v[2]
            if projection > self.EPSILON:
                print 'the angle is convex, adding theta'
                mu_theta_pairs.append((mu,theta))

            elif projection < -self.EPSILON:
                print 'the angle is concave, adding -theta'
                mu_theta_pairs.append((mu, -theta))

            else:
                print 'adding both theta and -theta'
                mu_theta_pairs += [(mu, theta), (mu, -theta)]

        #for each pair (mu,theta), determine the values of r using the equation HKAN*(1,0,0) = lambda*P => AN*(1,0,0) = lambda*K(theta)^-1 * u
        r_mu_theta_pairs = []
        for mu,theta in mu_theta_pairs:
            R = np.array([[np.cos(theta), np.sin(theta)], [-np.sin(theta), np.cos(theta)]])
            w = np.append(np.dot(R, u[:2]), u[2])

            #this condition is necessary in order to have a solution
            if np.abs(w[0] - (-mu*v[2]*(w[1]+w[2]))) > self.W_THRESH:
                print 'the difference between %f and %f was %f.' % (w[0], (-mu*v[2]*(w[1]+w[2])), np.abs(w[0] - (-mu*v[2]*(w[1]+w[2]))))
                continue
        
            numerator = (w[1]+w[2])*(1 - mu_squared*(v[2]**2)) - 2*w[1]
            denominator = w[1] + w[2]
            r_squared = numerator / denominator

            if r_squared < 0:
                continue
        
            r_mu_theta_pairs.append((np.sqrt(r_squared), mu, theta))
            r_mu_theta_pairs.append((-np.sqrt(r_squared), mu, theta))


        #finally, for each triple (r, mu, theta), compute x, and use it to compute a homography matrix
        homography_matrices = []
        for r,mu,theta in r_mu_theta_pairs:
            x = -mu*v[2] / r

            K = self._compute_K(theta)
            A = self._compute_A(r)
            N = self._compute_N(x)
            KAN = np.dot(K, np.dot(A, N))

            HKAN = np.dot(H, KAN)
            homography_matrices.append(HKAN)

            #DEBUGGING:
            #For each line correspondence (s,t), compute lambda that minimizes the square of r*(AN)^{-T}*s - lambda*(HK)^T*t
            #We do this to compare the outcome to the alternative algorithm that computes the least squares directly
            AN_inverse_T = np.linalg.inv(np.dot(A,N)).transpose()
            HK_T = np.dot(H,K).transpose()
            
            print 'checking least squares with r=%f and x=%f.' % (r,x)
            
            for i, (source_line, target_line) in enumerate(self._line_correspondences):
                print ' '*3 + 'computing lambda for correspondence %d' % i
                s = source_line.get_parametric_representation()
                t = target_line.get_parametric_representation()
                a = r * np.dot(AN_inverse_T, s)
                b = np.dot(HK_T,t)

                #we want lam to minimize ||a - lam*b||
                b_normed = b / np.linalg.norm(b)
                lam = np.dot(b_normed, a)
                lam /= np.linalg.norm(b)

                print ' '*3 + 'r*(AN)^{-T}*s = ', a
                print ' '*3 + '(HK)^T*t = ', b
                print ' '*3 + 'lambda = ', lam
                print ' '*3 + 'lambda*(HK)^T*t = ', lam*b
                print ' '*3 + '||r*(AN)^{-T}*s - lambda*(HK)^T*t|| = ', np.linalg.norm(a - lam*b)
           
        return homography_matrices

    def _compute_shape_matching_errors(self, target_image, homography_matrices, line_correspondences):
        '''
        target_image - numpy array with shape (n,m,3) and type np.uint8
        homography_matrices - a list of homography matrices from source to target
        line_correspondences - a list of tuples (source line, target line)

        For each homography matrix, compute the similarity of the target lines and the transformed source lines.
        Return the errors as a numpy array with shape (num homography matrices, num line correpondences)
        '''

        #We first scale the target lines so that they lie in a unit square.
        #This is done so that the radii of the lines is of the same order of magnitude as the angles of the lines.
        S = np.diag(np.array([1.0/target_image.shape[1], 1.0/target_image.shape[0], 1]))
        scaled_homography_matrices = [np.dot(S,H) for H in homography_matrices]
        
        scaled_transformed_source_shapes_list = [[planar_geometry.apply_homography_to_line(SH, s) for s,t in line_correspondences]
                                            for SH in scaled_homography_matrices]
        
        scaled_target_shapes = [planar_geometry.apply_homography_to_line(S, t) for s,t in line_correspondences]

        #for each homography matrix, compute the errors between the transformed source lines, and the target lines
        errors = np.array([[target_line.compute_shape_similarity(source_line) for source_line, target_line in zip(scaled_transformed_source_shapes, scaled_target_shapes)]
                          for scaled_transformed_source_shapes in scaled_transformed_source_shapes_list])

        return errors

    def _compute_center_point(self, transformed_center_circle, transformed_center_line, line_correspondences):
        '''
        transformed_center_circle, transformed_center_line - the transformations of the center unit circle and the line (x=0)
        line_correspondences - a list of pairs (line, transformed line)

        Return the transformation of the center point of the circle.
        We do this by using the line correspondence to find the transformation of one of the points on (x=0). The intersection with the ellipse gives 2 more.
        We then use the cross ratio to find the center point
        '''

        print 'computing center point...'
        print 'transformed center line: ', str(transformed_center_line)
        print line_correspondences
        print 'line correspondences: ', ', '.join([('(%s, %s)' % tuple(map(str, lc))) for lc in line_correspondences])
                                                  
        center_line = Line(1,0,0)

        #first get the intersection points of the transformed center line with the transformed center circle
        transformed_ellipse_line_intersection_points = planar_geometry.line_ellipse_intersection(transformed_center_line, transformed_center_circle)
        #sort the intersection points from top to bottom
        a = transformed_ellipse_line_intersection_points[:,1].argsort()
        transformed_ellipse_line_intersection_points = transformed_ellipse_line_intersection_points[a]
        
        print '   transformed ellipse line intersection points:'
        print transformed_ellipse_line_intersection_points

        #if there were no intersection points, return none
        if len(transformed_ellipse_line_intersection_points) == 0:
            return None
        
        #add the ellipse line intersection points to the projective point correspondences
        ellipse_line_intersection_points = np.array([[0,-1],[0,1]])
        
        projective_point_correspondences = [(np.append(p1,1), np.append(p2,1)) for p1,p2 in zip(ellipse_line_intersection_points, transformed_ellipse_line_intersection_points)]
    
        #now create point correspondences from the line correspondences
        for source_line, transformed_line in line_correspondences:
            print '   getting intersection point from source line: ', source_line
            print '   and transformed line: ', transformed_line
            
            #the source point is the intersection of the source line and the x=0 axis
            source_point = planar_geometry.projective_line_line_intersection(center_line, source_line)
            transformed_point = planar_geometry.projective_line_line_intersection(transformed_center_line, transformed_line)
            
            print '   source point: ', source_point
            print '   transformed point: ', transformed_point
            projective_point_correspondences.append((source_point, transformed_point))

        #now use the point correspondences to get a homography
        H = planar_geometry.compute_homography_on_line(center_line, projective_point_correspondences)

        #use the homography to compute the image of the origin
        projective_transformed_center = np.dot(H, np.array([0,0,1]))

        transformed_center = projective_transformed_center[:2] / projective_transformed_center[2]
        
        #make sure that the transformed center is not too close to the ellipse line intersection points
        distance = np.linalg.norm(transformed_ellipse_line_intersection_points[0] - transformed_ellipse_line_intersection_points[1])
        min_distance_to_intersection_points = self.CENTER_POINT_DEGENERACY_THRESHOLD * distance

        if np.min(np.linalg.norm(transformed_ellipse_line_intersection_points - transformed_center, axis=1)) < min_distance_to_intersection_points:
            return None

        return transformed_center

    def _compute_scaling_matrix(self, ellipse, line_through_center):
        '''
        ellipse - an ellipse object
        line_through_center - a line object that passes through the center of the ellipse

        Return a homography M that transforms the ellipse to the unit circle and the line to the y axis
        '''
        print 'computing scaling matrix...'
        
        #first get a homography matrix transforming the ellipse to the unit circle
        M = planar_geometry.compute_homography_to_unit_circle(ellipse)

        print 'M:'
        print M
        
        #now get the image of the line
        transformed_line = planar_geometry.apply_homography_to_line(M, line_through_center)

        #get a rotation matrix rotating the line to the y axis
        L = line_through_center.get_parametric_representation()
        H = np.diag(np.ones(3))
    
        #Rotate the normal vector L[:2] to the vector (1,0).
        v = L[:2]

        #if the normal vector is pointing in the -x direction, replace it with -v so that we don't change the orientation
        if v[0] < 0:
            v *= -1
            
        v_perp = planar_geometry.perp(v)
        R = np.vstack([v, v_perp]).transpose()
        H[:2,:2] = np.linalg.inv(R)

        #finally, compose M with H
        return np.dot(H, M)
        return M
    
    def _compute_homography_jacobian(self, H):
        D = H[2,2]
        return (1 / D**2) * np.array([[H[0,0] - H[2,0]*H[0,2], H[0,1] - H[2,1]*H[0,2]],
                                      [H[1,0] - H[2,0]*H[1,2], H[1,1] - H[2,1]*H[1,2]]])

    def _preserves_orientation(self, H):
        '''
        H - a homography matrix
        Return true iff H preserve orientation and does not flip any of the axes
        '''

        J = self._compute_homography_jacobian(H)
        return (J[0,0] > 0) and (J[1,1] > 0) and (np.linalg.det(J) > 0)
    
    def compute_homography(self, source_image, target_image, ellipse_correspondence, line_through_center_correspondence, line_correspondences):
        '''
        ellipse_correspondence - a tuple (source ellipse, target ellipse).
        line_through_center_correspondence - a tuple (source line, target line). Both the source line and the target line pass through the center of the ellipses
        line_correspondences - a list of tuples (source line, target line)
        '''

        #We first find a homography M_source that transforms the source ellipse to the unit circle
        #The final homography will by the composition of M, with a homography H
        M = self._compute_scaling_matrix(ellipse_correspondence[0], line_through_center_correspondence[0])

        print 'homography to unit circle:'
        print M
        
        #transform the source ellipse and center line by M
        scaled_ellipse_correspondence = (planar_geometry.apply_homography_to_ellipse(M, ellipse_correspondence[0]), ellipse_correspondence[1])
        scaled_line_through_center_correspondence = (planar_geometry.apply_homography_to_line(M, line_through_center_correspondence[0]), line_through_center_correspondence[1])
        scaled_line_correspondences = [(planar_geometry.apply_homography_to_line(M, source_line), target_line) for source_line, target_line in line_correspondences]

        #for debugging
        self._line_correspondences = scaled_line_correspondences
        
        print 'scaled line though center correspondence:'
        print map(str, scaled_line_through_center_correspondence)

        print 'original line correspondences: '
        print ', '.join([('(%s, %s)' % tuple(map(str, lc))) for lc in line_correspondences])
        print 'scaled line correspondences: '
        print ', '.join([('(%s, %s)' % tuple(map(str, lc))) for lc in scaled_line_correspondences])
        
        #now use scaled line correspondences to compute the image of the center of the ellipse
        transformed_center_point = self._compute_center_point(scaled_ellipse_correspondence[1], scaled_line_through_center_correspondence[1], scaled_line_correspondences)

        if transformed_center_point is None:
            return [], []
        
        image_with_center_point = target_image.copy()
        primitive_drawing.draw_line(image_with_center_point, line_through_center_correspondence[1], (0,255,0))
        primitive_drawing.draw_point(image_with_center_point, transformed_center_point, (255,0,0))
        cv2.imshow('the center of the ellipse', image_with_center_point)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

        #using the transformed center point, we can compute the possible homography matrices
        homography_matrices = self._compute_homography_matrices(scaled_ellipse_correspondence[1], scaled_line_through_center_correspondence[1], transformed_center_point)

        #the actual homography matrices are obtained by composing with M
        homography_matrices = [np.dot(H,M) for H in homography_matrices]
        homography_matrices = [H / H[2,2] for H in homography_matrices]
        print 'we got the homography matrices:'
        print '\n'.join(map(str, homography_matrices))

        # for i,H in enumerate(homography_matrices):
        #     J = self._compute_homography_jacobian(H)
        #     print 'Jac(H_%d):' % i
        #     print J
        #     print '    the determinant: ', np.linalg.det(J)
            
        #we now compute the errors for each homography matrix, and choose the one with the average error
        errors = self._compute_shape_matching_errors(target_image, homography_matrices, line_correspondences)

        #print 'the errors:'
        #print errors
        
        #mean_errors = errors.mean(axis=1)
        #smallest_error_index = 2#np.argmin(mean_errors)

        #best_homography = homography_matrices[smallest_error_index]
        #best_errors = errors[smallest_error_index]

        ##find the homography that preserves orientation
        #orientation_preserving_homographies = [H for H in homography_matrices if self._preserves_orientation(H)]

        #if len(orientation_preserving_homographies) == 1:
        #    H = orientation_preserving_homographies[0]
        #    errors = self._compute_shape_matching_errors(target_image, H, line_correspondences)
        #    return H, errors
    
        return homography_matrices, errors
        
    
