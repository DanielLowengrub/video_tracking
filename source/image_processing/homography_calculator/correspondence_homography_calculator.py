import itertools, operator, functools
import numpy as np
import cv2
from ....testers.auxillary.contour_shape_overlap.highlighted_shape_generator import draw_line
from ...auxillary import planar_geometry

DEBUG = False

class CorrespondenceHomographyCalculator(object):
    '''
    This class is used to calculate a homography matrix based on a pairing of planar shapes.
    For the moment we are only implementing this for lines.
    '''

    EPSILON = 10**(-5)
    
    def _get_projective_line_intersection(self, lines):
        '''
        lines is a tuple of Line objects
        If the lines are not equal (up to a threshold), return their intersection in projective space. Else, return None.
        '''
        
        #get the parametric representation of the intersection point
        parametric_lines = [line.get_parametric_representation() for line in lines]
        #parametric_lines = [parametric_line / np.linalg.norm(parametric_line) for parametric_line in parametric_lines]

        #print '   parametric lines: %s, %s' % tuple(map(str, parametric_lines))
        intersection_point = np.cross(*parametric_lines)
        #print '   intersection point: ', intersection_point

        #affine_point = intersection_point
        #print '   intersection norm: ', np.linalg.norm(intersection_point)
        #if the intersection point is degenerate, return None
        if np.linalg.norm(intersection_point) < self.EPSILON:
            return None

        return intersection_point

    def set_source_and_target_images(self, source_image, target_image):
        '''
        These images are used for debugging
        '''
        self._source_image = source_image
        self._target_image = target_image

    def _display_line_correspondences(self, line_correspondences):
        for source_line, target_line in line_correspondences:
            source_image = self._source_image.copy()
            target_image = self._target_image.copy()

            draw_line(source_image, source_line)
            draw_line(target_image, target_line)

            cv2.imshow('the source line', source_image)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

            cv2.imshow('the target line', target_image)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

    def _display_point_correspondences(self, point_correspondences):
        for source_point, target_point in point_correspondences:
            source_image = self._source_image.copy()
            target_image = self._target_image.copy()

            if (abs(source_point[2]) <= self.EPSILON) or (abs(target_point[2]) <= self.EPSILON):
                continue

            source_point = source_point[:2] / source_point[2]
            target_point = target_point[:2] / target_point[2]
            cv2.circle(source_image, tuple(np.int32(source_point).tolist()), 3, (0,0,255), thickness=-1)
            cv2.circle(target_image, tuple(np.int32(target_point).tolist()), 3, (0,0,255), thickness=-1)
            
            cv2.imshow('the source point', source_image)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

            cv2.imshow('the target point', target_image)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

            
    def _build_point_correspondences(self, shape_correspondences):
        '''
        Use the given shape correspondence to build a list of point correspondences.
        Return: a list of pairs of points.
        '''

        point_correspondences = []

        #print '   building point correspondences...'
        #iterate over all pairs of correspondences
        for shape_correspondences in itertools.combinations(shape_correspondences, 2):
            #get the intersection points of the lines in each of the correspondences
            #print '   getting point correspondence from: '
            #print '   pair 1: (%s, %s)' % tuple(map(str, shape_correspondences[0]))
            #print '   pair 2: (%s, %s)' % tuple(map(str, shape_correspondences[1]))

            intersection_points = [self._get_projective_line_intersection(lines) for lines in zip(*shape_correspondences)]

            #print 'intersection_points: ', intersection_points
                                                
            if all(point is not None for point in intersection_points) and all(abs(point[2]) >= self.EPSILON for point in intersection_points):
                point_correspondences.append(intersection_points)

        return point_correspondences

    def _compute_point_correspondence_equations(self, point_correspondence):
        '''
        point_correspondence - a tuple of projective points
        Return a numpy array of shape (2,8) corresponding to the two equations on the homography matrix imposed by the point.

        The equations:
        Since we need to divide by one of the elements of point_correspondence[1], we give different equations depending on which element is largest.
        '''

        #print 'computing equations for: (%s, %s)' % tuple(map(str, point_correspondence))
        
        max_index = np.argmax(point_correspondence[1])
        x, y, z = point_correspondence[0].tolist()
        a, b, c = point_correspondence[1].tolist()
        
        # if max_index == 0:
        #     #In this case:
        #     #lambda = (h11x + h12y + h13z)/a so:
        #     #a* (h21x + h22y + h23z) = (h11x + h12y + h13z)*b => (-xb, -yb, -zb, xa, ya, za, 0, 0, 0) * H = 0
        #     #a* (h31x + h32y + z) = (h11x + h12y + h13z)*c   => (-xc, -yc, -zc, 0,  0, 0, xa, ya, 0) * H = -za
        eq_a1 = np.array([-x*b, -y*b, -z*b, x*a, y*a, z*a, 0,   0,  0])
        eq_a2 = np.array([-x*c, -y*c, -z*c, 0,   0,   0, x*a, y*a, -z*a])

        #print '   using i=0:'
        #print '   eq1: ', eq_a1# / np.linalg.norm(eq_a1)
        #print '   eq2: ', eq_a2# / np.linalg.norm(eq_a2)
        # elif max_index == 1:
        #     #In this case:
        #     #lambda = (h21x + h22y + h23z)/b so:
        #     #b*(h11x + h12y + h13z) = (h21x + h22y + h23z)*a => (xb, yb, zb, -xa, -ya, -za, 0, 0, 0) * H = 0
        #     #b*(h31x + h31y + z) = (h21x + h22y + h23z)*c   => (0,  0,  0, -xc, -yc, -zc, xb, yb, 0) * H = -zb
        #     eq1 = np.array([x*b, y*b, z*b, -x*a, -y*a, -z*a, 0,   0,   0])
        #     eq2 = np.array([0,   0,   0,   -x*c, -y*c, -z*c, x*b, y*b, -z*b])

        # elif max_index == 2:
            #In this case:
            #lambda = (h31x + h32y + z)/c so:
            #c*(h11x + h12y + h13z) = (h31x + h32y + z)*a =>   (xc, yc, zc, 0,  0,  0,  -xa, -ya, 0) * H = za
            #c*(h21x + h22y + h32z) = (h31x + h32y + z)*b   => (0,  0,  0,  xc, yc, zc, -xb, -yb, 0) * H = zb
        eq_c1 = np.array([x*c, y*c, z*c, 0,   0,   0,   -x*a, -y*a, z*a])
        eq_c2 = np.array([0,   0,   0,   x*c, y*c, z*c, -x*b, -y*b, z*b])

        #print '   eq_c1: ', eq_c1# / np.linalg.norm(eq_c1)
        #print '   eq_c2: ', eq_c2# / np.linalg.norm(eq_c2)
        
        #return np.vstack([eq_a1, eq_a2, eq_c1, eq_c2])
        return np.vstack([eq_c1, eq_c2])

    def _compute_line_correspondence_equations(self, line_correspondence):
        '''
        line_correspondence - a pair of lines
        Return a numpy array of shape (2,8) corresponding to the two equations on the homography matrix imposed by the point

        Since L = H^T*L', we get the point correspondence equation for (L', L) and take the transpose
        '''

        #print 'computing equations for: (%s, %s)' % tuple(map(str,line_correspondence))
        
        point_correspondence = tuple(l.get_parametric_representation() for l in reversed(line_correspondence))
        point_equations = self._compute_point_correspondence_equations(point_correspondence)

        #print 'the point equations:'
        #print point_equations
        
        #these equations are for the matrix H^T, so we have to transpose the equations for the matrix coefficients
        #to do this, we first turn them into 3x3 matrices, transpose them, and then flatten them again
        matrix_coefficients_equations = point_equations[:,:-1]
        #print 'matrix coefficients equations: ', matrix_coefficients_equations
        matrix_coefficients_equations = [np.append(eq,1).reshape((3,3)).transpose().reshape((-1,))[:-1] for eq in matrix_coefficients_equations]

        #print 'matrix coefficients equations:'
        #print matrix_coefficients_equations
        
        #stack up the matrix coefficients, and add on the last column of the equations
        equations = np.hstack([np.vstack(matrix_coefficients_equations), point_equations[:,-1].reshape((-1,1))])
        
        return equations
                                         

    def _solve_homography_constraints(self, homography_constraints):
        '''
        homography_constraints - a (num constraints, 9) numpy array. each row represents and equation of the form: row[:,-1]^T * H = row[-1]
        '''

        H_coefficients = np.linalg.lstsq(homography_constraints[:,:-1], homography_constraints[:,-1])[0]
        return np.append(H_coefficients, 1).reshape((3,3))


    def _compute_shape_center(self, image, shape):
        '''
        image - numpy array with shape (n,m,3) and type np.uint8
        shape - a PlanarShape

        Return the center of the shape w.r.t the image.
        E.g, for a line, we return the center of the part of the line on the image. 
        If the line does not intersect the image return None.
        '''

        #print 'computing center of: ', str(shape)
        
        image_region = np.array([[0,0],[image.shape[1],image.shape[0]]])
        boundary_lines = planar_geometry.get_region_boundary_lines(image_region)
        intersection_points = filter(lambda x: x is not None and planar_geometry.point_in_region(x, image_region),
                                     (planar_geometry.affine_line_line_intersection(shape, l) for l in boundary_lines))

        #print '   intersection points: ', intersection_points
        #if there are 2 intersection points with the boundary of the image, return the center
        if len(intersection_points) == 2:
            return np.vstack(intersection_points).mean(axis=0)

        else:
            return None
        
    def _compute_normalization_matrix(self, points):
        '''
        points - a numpy array of size (num points, 2)
        Return a homography matrix M such that M*points has mean 0 and std 1.
        '''
        #print 'computing normalization matrix...'
        #translate to the center
        center = points.mean(axis=0)
        T = np.identity(3)
        T[:2,2] = -center.transpose()
        
        #scale by 1/std
        scale = (points - center).std(axis=0)
        S = np.diag(np.append(1/scale, 1))

        #first translate then scale
        M =  np.dot(S, T)

        #print 'center: ', center
        #print 'scale: ', scale
        #print 'M = '
        #print M

        return M
    
    def _compute_normalization_matrices(self, source_image, target_image, shape_correspondences):
        '''
        source_image, target_image - np array of shape (n,m,3) and type np.uint8
        shape_correspondences - a list of tuples of the form (PlanarShape, PlanarShape).

        Return a pair of matrices (source matrix, target matrix) which normalize the source and target shapes.
        '''

        #print 'computing normalization matrices...'
        
        #first compute the pairwise intersection points on both the source and the target
        point_correspondences = self._build_point_correspondences(shape_correspondences)
        source_intersection_points = map(lambda p: p[0][:2]/p[0][2], point_correspondences)
        target_intersection_points = map(lambda p: p[1][:2]/p[1][2], point_correspondences)

        #now compute the centers of the shapes
        source_shapes = map(operator.itemgetter(0), shape_correspondences)
        target_shapes = map(operator.itemgetter(1), shape_correspondences)

        source_shape_centers = filter(lambda x: x is not None, map(functools.partial(self._compute_shape_center, source_image), source_shapes))
        target_shape_centers = filter(lambda x: x is not None, map(functools.partial(self._compute_shape_center, target_image), target_shapes))

        #print 'source interection points:'
        #print source_intersection_points
        #print 'source shape centers:'
        #print source_shape_centers
        source_points = np.vstack(source_intersection_points + source_shape_centers)
        target_points = np.vstack(target_intersection_points + target_shape_centers)

        source_matrix = self._compute_normalization_matrix(source_points)
        target_matrix = self._compute_normalization_matrix(target_points)

        return source_matrix, target_matrix

    def _compute_shape_matching_errors(self, H, shape_correspondences):
        '''
        H - a homography matrix from the source to the target
        shape_correspondences - a list of tuples of the form (source PlanarShape, target PlanarShape)
        '''

        return np.array([planar_geometry.apply_homography_to_line(H, source_shape).compute_shape_similarity(target_shape)
                for source_shape, target_shape in shape_correspondences])
        
    def compute_homography(self, source_image, target_image, shape_correspondences):
        '''
        source_image, target_image - np array of shape (n,m,3) and type np.uint8
        shape_correspondences - a list of tuples of the form (PlanarShape, PlanarShape).
        For the moment we are assuming that all shapes are lines.

        To find the homography, we first compute an intersection point for each pair of lines, and generate point correspondences.
        We then create an equation for each point and shape correspondence.
        '''

        #self._display_line_correspondences(shape_correspondences)

        #first calculate the normalization matrices
        source_M, target_M = self._compute_normalization_matrices(source_image, target_image, shape_correspondences)

        #now normalize the shapes
        normalized_shape_correspondences = [(planar_geometry.apply_homography_to_line(source_M, x), planar_geometry.apply_homography_to_line(target_M, y))
                                            for x,y in shape_correspondences]

        #we now compute the homography between the normalized shapes
        #first use the line correspondene to generate a point correspondence
        normalized_point_correspondences = self._build_point_correspondences(normalized_shape_correspondences)

        #self._display_point_correspondences(point_correspondences)
        
        #print 'got the point correspondences:'
        #print ', '.join(['(%s, %s)' % tuple(map(str,pc)) for pc in normalized_point_correspondences])
        
        #now use the correspondences to compute equations
        point_equations = np.vstack([self._compute_point_correspondence_equations(pc) for pc in normalized_point_correspondences])
        line_equations = np.vstack([self._compute_line_correspondence_equations(lc) for lc in normalized_shape_correspondences])

        #print 'point equations:'
        #print point_equations
        #print 'line equations:'
        #print line_equations
        
        homography_constraints = np.vstack([point_equations, line_equations])
        #homography_constraints = line_equations
        #homography_constraints = point_equations

        #normalize the constraints
        #homography_constraints = homography_constraints / np.linalg.norm(homography_constraints, axis=1).reshape((-1,1))
        
        #solve the equations with least squares
        H = self._solve_homography_constraints(homography_constraints)

        #before denormalizing, compute the shape matching errors
        shape_matching_errors = self._compute_shape_matching_errors(H, normalized_shape_correspondences)
        
        #the final homography matrix is (target_M^-1) * H * source_M
        H = np.dot(np.linalg.inv(target_M), np.dot(H, source_M))
        
        #print 'we got: '
        #print H

        #max_dist = 5.0
        #point_correspondences = self._build_point_correspondences(shape_correspondences)
        #src_pts = np.vstack([points[0][:2]/points[0][2] for points in point_correspondences]).reshape((-1,1,2))
        #dst_pts = np.vstack([points[1][:2]/points[1][2] for points in point_correspondences]).reshape((-1,1,2))
        #homography, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC,max_dist)

        #print 'a better homography is:'
        #print homography
        
        # #draw the transformed src pts on the target
        # transformed_source_points = cv2.perspectiveTransform(src_pts, H).reshape((-1,2))
        # target_image = target_image.copy()

        # for point in transformed_source_points:
        #     cv2.circle(target_image, tuple(np.int32(point).tolist()), 3, (255,0,0), thickness=-1)

        # for point in dst_pts.reshape((-1,2)):
        #     cv2.circle(target_image, tuple(np.int32(point).tolist()), 3, (0,255,0), thickness=-1)

        # cv2.imshow('the source and target points', target_image)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        return H, shape_matching_errors
