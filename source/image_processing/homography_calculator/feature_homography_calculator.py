from .homography_calculator import HomographyCalculator
from ..image_annotator.image_annotator import ImageAnnotator
from ...auxillary.mask_operations import apply_mask_to_image, get_mask_from_annotation, convert_to_cv_mask
from ...auxillary.equalize_image import equalize_image

from ....testers.image_processing.homography_calculator.homography_visualization import draw_images_side_by_side, draw_keypoints_side_by_side, draw_source_and_destination_points

import cv2
import numpy as np

class FeatureHomographyCalculator(HomographyCalculator):
    '''
    This implementation calculates the homography between images using features.

    Note that in this implementation there is the option of setting an annotation for each of the images.
    If this is done, we find keypoints for each of the annotated regions seperately, and then combine them in order to compute the homography.
    '''

    def __init__(self, flexibility=0.7, equalize=True, max_distortion=0.05, min_points_for_homography=30,
                 min_segment_area=100):
        '''
        The flexibility parameter should be between 0 and 1. It has the following meaning:
        Suppose p is a points in image1 and q1,q2 are the two points in image2 that are most similar to it. Then q1 will be declared as a match
        if and only if: feature_distance(p, q1) < flexibility * feature_distance(p, q2).
        I.e, q1 has to be closer than flexibility * the distance of the next closest match.
        A lower number will allow fewer point pairs to count as matches.
        
        If equalize is true, we first equalize the histograms before finding matches.

        max_distortion determines how far two points can be (in pixels) relative to the size of the image in order to count as a match.
        The smaller the distortion, the closer two points have to be.

        min_points_for_homography - the minimum number of points we need to compute a homography matrix
        
        min_segment_area - we only find source/dest pairs between image segments if both segments have at least this area.
        '''

        self._flexibility = flexibility
        self._equalize = equalize
        self._max_distortion = max_distortion
        self._min_points_for_homography = min_points_for_homography
        self._min_segment_area = min_segment_area
        
        self._ransac_thresh = 5.0 #the reprojection threshold used in the RANSAC algorithm.
        self._orb_param = 2       #a parameter we pass to the ORB feature detector
        self._annotation_mask_erosion = None
        
        #annotations - a pair of numpy arrays of shape (n,m) and type np.int32.
        #If it is not None, we segment the image by the values of the segmentation array.
        #We search for matches between segments of the images with the same values.
        #This can be useful if there are regions of the image with big differences in resolution.
        #In this case, the ORB algorithm would find very few features on the low resolution section if we asked it to
        #find features on the entire image at once.
        self._annotations = None
        
        return

    def set_annotations(self, annotation_1, annotation_2):
        self._annotations = (annotation_1, annotation_2)
                          
    def _get_quality_matches(self, des1, des2):
        '''
        des1, des2 - two lists of feature descriptors, one per image.
        
        Return a list of match objects corresponding to pairs of descriptors (d1 in des1, d2 in des2) that are similar.
        '''

        #if there are no descriptors, return an empy list
        if des1 is None or des2 is None or len(des1) == 0 or len(des2) == 0:
            return []

        #Find matches between the keypoints
        bf = cv2.BFMatcher()
        matches = bf.knnMatch(des1,des2, k=2)

        if len(matches) == 0:
            return []

        # Apply the ratio tests to find high quality matches
        good_matches = []
        for match_pair in matches:

            #if there is only one match in this match pair, we cannot be confident that it is a good match so skip it
            if len(match_pair) < 2:
                continue
            
            if match_pair[0].distance < self._flexibility*match_pair[1].distance:
                good_matches.append(match_pair[0])

        return good_matches

    def _get_close_matches(self, kp1, kp2, matches, max_dist):
        '''
        kp1, kp2 - a list of keypoints from two images.
        matches - a list of match objects. We return the ones whose distance in pixels is less that max_dist
        '''
        
        #find matches with small distance in image coordinates
        close_matches = []
        for m in matches:
            idx_1 = m.queryIdx
            idx_2 = m.trainIdx
            
            (x1,y1) = kp1[idx_1].pt
            (x2,y2) = kp2[idx_2].pt

            dist = ((x1-x2)**2 + (y1-y2)**2)**(0.5)
            
            if dist < max_dist:
                close_matches.append(m)

        return close_matches

    def _get_matches(self, gs_image_1, gs_image_2, mask_1, mask_2):
        '''
        gs_image is a numpy array of shape (n,m) and type np.uint8.
        mask is a numpy array of shape (n,m) and type np.float64

        We return a pair of lists (points1, points2). points1 is a list of points on image1 in the form of a numpy
        array with shape (number of points, 1, 2) and similarly, points2 is a list points
        on image2. For all i, points1[i] should be matched with points2[i] under a homography from image1 to image2.
        '''
        
        image_length = max(gs_image_1.shape[:2])
        
        #this is the maximum distance allowed between two matches. The assumtion
        #is that the camera moves slowly enough for this to be true.
        max_dist = image_length * self._max_distortion

        #initialize the feature detector
        orb = cv2.ORB(WTA_K=self._orb_param)

        if self._equalize:
            equalize_image(gs_image_1, mask_1)
            equalize_image(gs_image_2, mask_2)

            #draw_images_side_by_side(np.uint8(gs_image_1 * mask_1), np.uint8(gs_image_2 * mask_2), 'equalized images')
            
        #get keypoints and descriptors
        kp_1, des_1 = orb.detectAndCompute(gs_image_1, convert_to_cv_mask(mask_1))
        kp_2, des_2 = orb.detectAndCompute(gs_image_2, convert_to_cv_mask(mask_2))

        #print 'there are %d keypoints in img1 and %d keypoints in img2' % (len(kp_1),len(kp_2))

        #draw_keypoints_side_by_side(gs_image_1, kp_1, gs_image_2, kp_2)
            
        good_matches = self._get_quality_matches(des_1, des_2)
        close_matches = self._get_close_matches(kp_1, kp_2, good_matches, max_dist)

        #we now extract the list of pairs of matching points from the keypoints and the match objects in the close_matches list
        source_points = np.float32([kp_1[m.queryIdx].pt for m in close_matches]).reshape(-1,1,2)
        destination_points = np.float32([kp_2[m.trainIdx].pt for m in close_matches]).reshape(-1,1,2)

        #print 'after filtering the matches, there are %d keypoints' % len(source_points)
        #draw_source_and_destination_points(gs_image_1, source_points, gs_image_2, destination_points)
                
        return source_points, destination_points

    def _get_all_matches(self, image_1, image_2, mask_1, mask_2):
        '''
        image1, image2 - numpy arrays of shape (n,m,3) and type np.uint8
        mask1, mask2 - numpy arrays of shape (n,m) and type np.float64

        We return a pair of lists (points1, points2). points1 is a list of points on image1 in the form of a numpy array of shape
        (number of points, 1, 2), and similarly, points2 is a list points
        on image2. For all i, points1[i] should be matched with points2[i] under a homography from image1 to image2.
        
        We first convert the images to greyscale. We then extract the matches.
        If the user has set annotations arrays, we collect the matches from each segment separately.
        '''
        #get the grayscale versions of the images
        gs_image_1 = cv2.cvtColor(image_1, cv2.COLOR_BGR2GRAY)
        gs_image_2 = cv2.cvtColor(image_2, cv2.COLOR_BGR2GRAY)

        if self._annotations is None:
            return self._get_matches(gs_image_1, gs_image_2, mask_1, mask_2)

        #if we have annotations, find matches in each segment and add them together.
        #get all of the relevant values occuring in the annotation
        segment_values = self._annotations[0].get_relevant_annotation_values()

        #print 'the segment values are: ', segment_values
        
        source_points_by_segment = []
        destination_points_by_segment = []
        
        for segment_value in segment_values:
            #print '   processing segment number %d' % segment_value
            
            segment_masks = [annotation.get_value_mask(segment_value) for annotation in self._annotations]

            #if any of the segments are too small, skip them
            if any((sm.sum() < self._min_segment_area) for sm in segment_masks):
                continue

            #print '   area of segment 0: %d, area of segement 1: %d' % tuple(sm.sum() for sm in segment_masks)
            segment_source_points, segment_destination_points = self._get_matches(gs_image_1, gs_image_2,
                                                                                  segment_masks[0], segment_masks[1])
            #print '   got %d matching pairs from this segemnt.' % len(segment_source_points)

            source_points_by_segment.append(segment_source_points)
            destination_points_by_segment.append(segment_destination_points)

            
        return np.vstack(source_points_by_segment), np.vstack(destination_points_by_segment)

    def calculate_homography(self, image_1, image_2, mask_1=None, mask_2=None):
        '''
        image1, image2 - a numpy array of shape (n,m,3) and type np.uint8
        mask1, mask2 - a numpy array of shape (n,m) and type np.float64

        If the user has set annotations, we try to match up the images segment by segment.

        Return a homography matrix from the masked part of image 1 to the masked part of image2.
        If we cannot find enough data points to calculate the homography, return None.
        '''
        #print 'calculating homography...'
        #since we do not want to change the input images, make copies first
        image_1 = image_1.copy()
        image_2 = image_2.copy()
        
        if mask_1 is None:
            mask_1 = np.ones(image_1.shape[:2], np.float64)

        if mask_2 is None:
            mask_2 = np.ones(image_2.shape[:2], np.float64)

        #first get a list of source and destination points
        source_points, destination_points = self._get_all_matches(image_1, image_2, mask_1, mask_2)

        #if we have not found enough points, return none.
        if len(source_points) < self._min_points_for_homography:
            #print 'we only found %d matches. Returning None.' % len(source_points)
            return None

        #print 'computing homography matrix with %d matches' % len(source_points)
        
        #if we do have enough points, calculate the homography using these points
        M, mask = cv2.findHomography(source_points, destination_points, cv2.RANSAC, self._ransac_thresh)

        return M
        
