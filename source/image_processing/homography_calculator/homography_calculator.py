class HomographyCalculator(object):
    '''
    The job of this class is to find the planar homography between two (masked) images.
    
    Different subclasses may use different aspects of the images to compute the homography.
    For example, the simplest implementation uses the standard feature based approach.
    There is also an implementation that uses correspondences between geometric objects that are provided in addition to the images.
    We also have a hybrid approach that uses both.
    '''

    def calculate_homography(self, image1, image2, mask1, mask2):
        '''
        image1, image2 - numpy arrays of shape (n,m,3) and type np.uint8
        mask1, mask2 - numpy arrays of shape (n,m) and type np.float64

        If we managed to compute the homography, we return a 3x3 matrix representing a planar homography between the masked regions of the images.
        Otherwise, we return None.
        '''

        pass
