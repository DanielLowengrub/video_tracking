from .sidelines_extractor import SidelinesExtractor
from ...auxillary import pixel_gmm

MIN_SIDELINES_COVARIANCE = 10
NUM_LABELS = 3

class GMMSidelinesExtractor(SidelinesExtractor):
    '''
    This version of the sidelines extractor detects the sidelines by their pixel values.
    We do this by fitting a GMM to the pixels in the image, and then checking if there is a large number
    of pixels whose values are densly distributed. We assume that these correspond to sidelines.

    ASSUPMTION: We assume that the masked part of the given image contains only sidelines and noise (such as player).
                If the masked region contains field values, we will end up getting the field.

                Therefore, it is best to use this extractor on a portion of the image that is inside a contour.
    '''

    def __init__(self):
        #set up the pixel gmm that we use to detect sidelines
        #We use NUM_LABELS labels and the Hue/Saturation channels.
        self._pixelGMM = pixel_gmm.PixelGMM(NUM_LABELS, pixel_gmm.HS)
        
    def get_sidelines_mask(self, image, mask):

        #train the pixel gmm on the given image and mask
        self._pixelGMM.train(image, mask)

        #get a mask of all points that lie in a GMM class with covariance less than MIN_SIDELINES_COVARIANCE
        sidelines_mask = self._pixelGMM.get_mask_of_pixels_in_dense_cluster(MIN_SIDELINES_COVARIANCE)

        #we assume that any pixels in in a cluster with such a small covariance must be sidelines.
        return sidelines_mask
    
