class SidelinesExtractor(object):
    '''
    The goal of the sidelines extractor is to create a mask containing the portion of the image that consists of sidelines.
    '''

    def get_sidelines_mask(self, image, mask):
        '''
        image - a numpy array of shape (n,m,3) and type np.uint8
        mask - a numpy array of shape (n,m) and tupe np.float64

        Returns a numpy array of shape (n,m) and type np.float64.
        This is a mask on the original image which represents which parts are sidelines
        '''

        pass
