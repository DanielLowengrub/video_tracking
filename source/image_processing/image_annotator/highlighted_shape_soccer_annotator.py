import itertools
import numpy as np
import cv2
from soccer_annotation import SoccerAnnotation
from ..contour_generator.edges_contour_generator import EdgesContourGenerator
from ...auxillary.contour_shape_overlap.highlighted_shape import HighlightedShape
from ...data_structures.contour import Contour
from ...auxillary import mask_operations

class HighlightedShapeSoccerAnnotator(object):
    '''
    This class uses a field mask and highlighted shapes to annotate a soccer image.

    Specifically, we first find the contours in the image and subtract the highighted shapes.
    These modified contours are assumed to contain players. 
    The highilghted regions of the highlighted shapes are marked as sidelines.
    The rest of the pixels in the field mask are marked as grass.
    '''

    def __init__(self, canny_min, canny_max):
        #this is used to generate contours in the image
        self._contour_generator = EdgesContourGenerator(canny_min, canny_max)

    def annotate_image(self, image, field_mask, highlighted_shapes):
        '''
        image - a numpy array of shape (n,m,3) and type np.uint8
        field_mask - a numpy array of shape (n,m) and type np.float32
        highlighted_shapes - a list of HighlightedShape objects corresponding to shapes on the given image

        return - a SoccerAnnotation object
        '''

        #first find the contours in the image
        contours, contour_mask = self._contour_generator.generate_contours(image, field_mask)

        # image_with_contours = image.copy()
        # cv2.drawContours(image_with_contours, [c.get_cv_contour() for c in contours], -1, (0, 0, 255), 3)
        # cv2.imshow('image with contours', image_with_contours)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        #contour_mask = Contour.get_outer_mask_of_contours(image, contours)
        # cv2.imshow('contour mask', contour_mask)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        #subtract the highlighted shapes from the contours
        #recall that remove_shapes_from_contours returns a list of tuples of contours, where each tuple
        #corresponds to a connected component of the original contour mask.
        #since we do not care about the connected components, we chain these tuples into one list.
        player_contours = list(itertools.chain(*HighlightedShape.remove_shapes_from_contours(highlighted_shapes, contours)))

        # image_with_contours = image.copy()
        # cv2.drawContours(image_with_contours, [pc.get_cv_contour() for pc in player_contours], -1, (0, 0, 255), 3)
        # cv2.imshow('image with contours', image_with_contours)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        players_mask = Contour.get_outer_mask_of_contours(image, player_contours)

        # cv2.imshow('player mask', players_mask)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        #build the sideline mask from the masks of the highlighted shapes
        sidelines_mask = np.zeros(image.shape[:2], np.float32)
        for hs in highlighted_shapes:
            sidelines_mask = mask_operations.get_union(sidelines_mask, hs.generate_mask(image))

        sidelines_mask = sidelines_mask * contour_mask

        #the grass mask is the field mask, minus the player and sideline masks
        grass_mask = field_mask * (1 - players_mask) * (1 - sidelines_mask)

        #build a soccer annotation from these three masks
        annotation = SoccerAnnotation.from_masks(grass_mask, sidelines_mask, players_mask)

        return annotation
