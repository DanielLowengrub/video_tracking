import numpy as np
import cv2

from ..contour_classifier.stupid_has_sideline_classifier import StupidHasSidelineClassifier
from ..contour_classifier.stupid_multiple_player_classifier import StupidMultiplePlayerClassifier
from ..field_extractor.stupid_field_extractor import StupidFieldExtractor
from ..contour_generator.edges_contour_generator import EdgesContourGenerator
from ..sidelines_extractor.gmm_sidelines_extractor import GMMSidelinesExtractor

from .soccer_annotation import SoccerAnnotation

class GMMSoccerFieldAnnotator(object):
    '''
    This class uses a gaussian mixture model to annotate a soccer field based on color.
    '''

    def __init__(self):

        self._field_extractor = StupidFieldExtractor()
        self._contour_generator = EdgesContourGenerator()
        self._sidelines_extractor = GMMSidelinesExtractor()
        self._player_classifier = StupidMultiplePlayerClassifier()
        self._sidelines_classifier = StupidHasSidelineClassifier()
        
    def _get_sidelines_in_contour(self, contour):
        '''
        contour - a Contour object

        We assume that the contour contains only sidelines and players.
        Return a mask of the sidelines.
        '''
        
        contour_mask = contour.get_outer_mask()
        sidelines_mask = self._sidelines_extractor.get_sidelines_mask(contour.get_image(), contour_mask)

        return sidelines_mask
    
    def annotate_image(self, image, mask=None):
        '''
        image - a numpy array of shape (n,m,3) and type np.int32
        mask - a numpy array of shape (n,m) and type np.float32

        Return a SoccerAnnotation object which records an annotation of the masked region of the given image
        '''
        sidelines_mask = np.zeros(image.shape[:2], np.bool)
        players_mask = np.zeros(image.shape[:2], np.bool)

        #first get the grass using the field extractor
        grass_mask = self._field_extractor.get_field_mask(image, mask)
        
        #get all contours in the image
        contours = self._contour_generator.generate_contours(image, grass_mask)

        #for each of the contours,
        #1) first remove the interior of the image from the grass
        #2) divide the interior between sidelines and players
        for contour in contours:
            contour_mask = contour.get_outer_mask()
            grass_mask = np.logical_and(grass_mask, np.logical_not(contour_mask))

            #if this is a player contour, add this to the players mask
            if self._player_classifier.is_match(contour):
                players_mask = np.logical_or(players_mask, contour_mask)

            #if this contour contains sidelines, seek them out and add them to the sidelines mask.
            # The rest will go to the players mask.
            elif self._sidelines_classifier.is_match(contour):
                sidelines_in_contour = self._get_sidelines_in_contour(contour)
                players_in_contour = np.logical_and(contour_mask, np.logical_not(sidelines_in_contour))
            
                sidelines_mask = np.logical_or(sidelines_mask, sidelines_in_contour)
                players_mask = np.logical_or(players_mask, players_in_contour)

            #cv2.imshow('the contour mask', contour_mask)
            #cv2.waitKey(0)
            #cv2.destroyAllWindows()

        return SoccerAnnotation(grass_mask, sidelines_mask, players_mask)
        
    
