import numpy as np

class ImageAnnotator(object):
    '''
    The image annotators are responsible for segmenting an image into components.
    The specific kinds of components depend on the implementation.

    An image annotator generates an ImageAnnotation object.
    '''

    #the number that all implementations assign to the irrelevant pixels
    IRRELEVANT = 0
    
    def annotate_image(self, image, mask=None):
        '''
        image - a numpy array of shape (n,m,3) and type uint8

        Return a numpy array of shape (n,m) and type np.int32 corresponding to an anotation of the image.
        '''

        raise NotImplementedError('annotate_image has not been implemented')

    @classmethod
    def get_relevant_annotation_values(cls, annotation):
        '''
        Return a list of all the distinct values in the annotation, except for the irrelevant one(s)
        '''
        relevant_values = np.unique(annotation).tolist()
        relevant_values.remove(cls.IRRELEVANT)
        
        return relevant_values
    
    @classmethod
    def subdivide_annotation(cls, annotation, x_subdivision, y_subdivision):
        '''
        x_subdivision, y_subdivision - integers

        We break up the image into N = (x_subdivision * y_subdivision) regions. 
        x_subdivision is the number of vertical lines and y_subdivision is the number of horizontal lines.
        Then, each annotation value is turned into N different annotation values, one per region.
        '''
        subdivided_annotation = annotation.copy()
        
        x_axis_length = annotation.shape[1]
        y_axis_length = annotation.shape[0]

        #the x coordinates of the vertical lines that we will use to create the regions. This includes the left and right borders of the picture
        x_markers = np.linspace(0, x_axis_length, x_subdivision + 1)
        #the y coordinates of the horizontal lines that we will use to create the regions. This includes the top and bottom borders of the picture
        y_markers = np.linspace(0, y_axis_length, y_subdivision + 1)

        #store the top left and bottom right corners of each of the regions in the subdivided image
        regions = [np.array([[x_markers[i], y_markers[j]],
                             [x_markers[i+1], y_markers[j+1]]])
                   for i in range(x_subdivision)
                   for j in range(y_subdivision)]

        #we now go through all the regions, and change the values of the annotations in each region to a distinct value
        new_annotation_value = cls.IRRELEVANT + 1
        old_annotation_values = cls.get_relevant_annotation_values(annotation)
        
        for region in regions:
            x_start, y_start = region[0]
            x_end, y_end = region[1]
            annotated_region = subdivided_annotation[ y_start:y_end, x_start:x_end ]

            for old_annotation_value in old_annotation_values:
                annotated_region[annotated_region == old_annotation_value] = new_annotation_value
                new_annotation_value += 1

        return subdivided_annotation
                
