import numpy as np
from .image_annotator import ImageAnnotator

class SoccerFieldAnnotator(ImageAnnotator):
    '''
    This implementation of an image annotator that separates an image into 3 parts:
    GRASS - the grass on a soccer field
    SIDELINES - the sidelines on a soccer field
    PLAYERS - the players on the field
    '''

    #Note: All values less than 1 are assumed to not be on the field
    GRASS = 1
    SIDELINES = 2
    PLAYERS = 3
    
    @classmethod
    def get_grass_mask(cls, annotation):
        '''
        annotation - a numpy array of shape (n,m) and type np.int32. It should be the output of annotate_image applied to some image.

        return a mask, i.e, numpy array of shape (n,m) and type np.float64 with values:
        
        mask[x,y] = 1.0 if annotation[x,y] = GRASS, 0.0 elsewise.
        '''

        mask = np.zeros(annotation.shape, np.float64)
        mask[annotation == cls.GRASS] = 1.0

        return mask

    @classmethod
    def get_sidelines_mask(cls, annotation):
        '''
        annotation - a numpy array of shape (n,m) and type np.int32. It should be the output of annotate_image applied to some image.

        return a mask, i.e, numpy array of shape (n,m) and type np.float64 with values:
        
        mask[x,y] = 1.0 if annotation[x,y] = SIDELINES, 0.0 elsewise.
        '''

        mask = np.zeros(annotation.shape, np.float64)
        mask[annotation == cls.SIDELINES] = 1.0

        return mask

    @classmethod
    def get_players_mask(cls, annotation):
        '''
        annotation - a numpy array of shape (n,m) and type np.int32. It should be the output of annotate_image applied to some image.

        return a mask, i.e, numpy array of shape (n,m) and type np.float64 with values:
        
        mask[x,y] = 1.0 if annotation[x,y] = PLAYERS, 0.0 elsewise.
        '''

        mask = np.zeros(annotation.shape, np.float64)
        mask[annotation == cls.PLAYERS] = 1.0

        return mask

    @classmethod
    def get_soccer_field_mask(cls, annotation):
        '''
        annotation - a numpy array of shape (n,m) and type np.int32. It should be the output of annotate_image applied to some image.

        return a mask, i.e, numpy array of shape (n,m) and type np.float64 with values:
        
        mask[x,y] = 1.0 if annotation[x,y] != IRRELEVANT, 0.0 elsewise.
        In otherwords, this mask contains everything on the field
        '''

        mask = np.zeros(annotation.shape, np.float64)
        mask[annotation != cls.IRRELEVANT] = 1.0

        return mask
