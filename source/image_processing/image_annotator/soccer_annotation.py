import numpy as np
from image_annotation import ImageAnnotation

class SoccerAnnotation(ImageAnnotation):
    '''
    A SoccerAnnotation is an ImageAnnotation that separates an image into 3 parts:
    GRASS - the grass on a soccer field
    SIDELINES - the sidelines on a soccer field
    PLAYERS - the players on the field
    '''

    GRASS = 1
    SIDELINES = 2
    PLAYERS = 3
   
    def __init__(self, annotation_array):
        '''
        annotation_array - a numpy array with shape (n,m) and type int32.
        The elements with value GRASS correspond to grass pixels. Similar for SIDELINES and PLAYERS.
        '''
        super(SoccerAnnotation, self).__init__(annotation_array)

    @classmethod
    def from_masks(cls, grass_mask, sidelines_mask, players_mask):
        '''
        grass_mask - a numpy array with shape (n,m) and type np.float32
        sidelines_mask - a numpy array with shape (n,m) and type np.float32
        players_mask - a numpy array with shape (n,m) and type np.float32

        Return a SoccerAnnotation object which has grass in the pixels where grass_mask is true, and similarly for 
        the sidelines and player masks.
        '''

        annotation_array = np.ones(grass_mask.shape, np.int32) * cls.irrelevant_value()
        annotation_array[grass_mask > 0] = cls.grass_value()
        annotation_array[sidelines_mask > 0] = cls.sidelines_value()
        annotation_array[players_mask > 0] = cls.players_value()

        return cls(annotation_array)

    @classmethod
    def grass_value(cls):
        return cls.GRASS

    @classmethod
    def sidelines_value(cls):
        return cls.SIDELINES

    @classmethod
    def players_value(cls):
        return cls.PLAYERS
    
    def get_sidelines_mask(self):
        '''
        return a mask, i.e, numpy array of shape (n,m) and type np.float32 with values:
        
        mask[x,y] = 1.0 if annotation[x,y] = SIDELINES, 0.0 elsewise.
        '''
        return self.get_value_mask(self.SIDELINES)

    def get_players_mask(self):
        '''
        return a mask, i.e, numpy array of shape (n,m) and type np.float32 with values:
        
        mask[x,y] = 1.0 if annotation[x,y] = PLAYERS, 0.0 elsewise.
        '''
        return self.get_value_mask(self.PLAYERS)

    def get_grass_mask(self):
        '''
        return a mask, i.e, numpy array of shape (n,m) and type np.float32 with values:
        
        mask[x,y] = 1.0 if annotation[x,y] = GRASS, 0.0 elsewise.
        '''
        return self.get_value_mask(self.GRASS)

    def get_soccer_field_mask(self):
        '''
        return a mask, i.e, numpy array of shape (n,m) and type np.float32 with values:
        
        mask[x,y] = 1.0 if annotation[x,y] = GRASS, SIDELINES or PLAYERS, 0.0 elsewise.
        '''
        return self.get_values_mask((self.SIDELINES, self.PLAYERS, self.GRASS))

    def get_background_mask(self):
        '''
        return a mask, i.e, numpy array of shape (n,m) and type np.float32 with values:
        
        mask[x,y] = 1.0 if annotation[x,y] is NOT equal to GRASS, SIDELINES or PLAYERS
        '''
        return self.get_value_mask(self.irrelevant_value())
