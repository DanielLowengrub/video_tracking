import numpy as np

class ImageAnnotation(object):
    '''
    This class represents an annotation of an image.
    It supports various methods to use the annotation to create image masks, and also to generate new annotations based
    on this one.
    '''

    #the number that is assigned to the irrelevant pixels
    IRRELEVANT = 0
    
    def __init__(self, annotation_array):
        '''
        annotation_array - a numpy array of shape (n,m) and type np.int32
        '''

        self._annotation_array = annotation_array

    @classmethod
    def irrelevant_value(cls):
        return cls.IRRELEVANT

    def get_image_shape(self):
        '''
        return a tuple of integers (n,m,3).
        This records the shape of the image that this annotation is annotating.
        '''
        return self._annotation_array.shape + (3,)
    
    def get_annotation_array(self):
        return self._annotation_array

    def get_relevant_annotation_values(self):
        '''
        Return a list of all the distinct values in the annotation, except for the irrelevant one(s)
        '''
        relevant_values = np.unique(self._annotation_array).tolist()
        relevant_values.remove(self.irrelevant_value())
        
        return relevant_values

    def get_value_mask(self, value):
        '''
        value - an integer

        return a mask that has value 1.0 in the coordinates where the annotation is equal to the value, and 0.0 elsewhere.
        '''
        mask = np.zeros(self._annotation_array.shape, np.float32)
        mask[self._annotation_array == value] = 1.0

        return mask

    def get_values_mask(self, values):
        '''
        values - a list of integers

        return a mask that has value 1.0 in the coordinates where the annotation is equal to one of the values, 
        and 0.0 elsewhere.
        '''
        mask = np.zeros(self._annotation_array.shape, np.float32)
        in_values = np.in1d(self._annotation_array.flatten(), values).reshape(self._annotation_array.shape)
        mask[in_values] = 1.0

        return mask

    def subdivide_annotation(self, x_subdivision, y_subdivision, relevant_annotation_values=None):
        '''
        x_subdivision, y_subdivision - integers
        relevant_annotation_values - a list of annotation values. If this is None, we use self._get_relevant_annotation_values.
                                     The point of this argument is that sometimes we want to subdivide a 
                                     collection of annotations, and we want the annotation values in different subdivided
                                     annotations to correspond to the same region.

        We break up the image into N = (x_subdivision * y_subdivision) regions. 
        x_subdivision is the number of vertical lines and y_subdivision is the number of horizontal lines.
        Then, each annotation value in annotation_values is turned into N different annotation values, one per region.

        We return a new AnnotatedImage object with the subdivided annotation.
        '''
        #print 'original annotation values'
        subdivided_annotation_array = self._annotation_array.copy()
        
        x_axis_length = self._annotation_array.shape[1]
        y_axis_length = self._annotation_array.shape[0]

        #the x coordinates of the vertical lines that we will use to create the regions. This includes the left and right borders of the picture
        x_markers = np.linspace(0, x_axis_length, x_subdivision + 1).astype(np.int32)
        #the y coordinates of the horizontal lines that we will use to create the regions. This includes the top and bottom borders of the picture
        y_markers = np.linspace(0, y_axis_length, y_subdivision + 1).astype(np.int32)

        #store the top left and bottom right corners of each of the regions in the subdivided image
        regions = [np.array([[x_markers[i], y_markers[j]],
                             [x_markers[i+1], y_markers[j+1]]])
                   for i in range(x_subdivision)
                   for j in range(y_subdivision)]

        #we now go through all the regions, and change the values of the annotations in each region to a distinct value
        new_annotation_value = self.irrelevant_value() + 1
        old_annotation_values = relevant_annotation_values if (relevant_annotation_values is not None) else self.get_relevant_annotation_values()
        
        for region in regions:
            x_start, y_start = region[0]
            x_end, y_end = region[1]
            annotated_region_array = subdivided_annotation_array[ y_start:y_end, x_start:x_end ]

            for old_annotation_value in old_annotation_values:
                annotated_region_array[annotated_region_array == old_annotation_value] = new_annotation_value
                new_annotation_value += 1

        return type(self)(subdivided_annotation_array)
                
