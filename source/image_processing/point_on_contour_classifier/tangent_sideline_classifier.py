from point_on_contour_classifier import PointOnContourClassifier

class EllipseClassifier(PointOnContourClassifier):
    '''
    This implementation of PointOnContour classifier uses tangency data at the contour points to determine which points lie on an ellipse.
    '''
