import numpy as np
from point_on_contour_classifier import PointOnContourClassifier
from ...auxillary.fit_points_to_shape.fit_points_to_line import FitPointsToLine
from ...auxillary.fit_points_to_shape.fit_points_to_ellipse_stable import FitPointsToEllipseStable
from ..hough_transform.tangent_detector import TangentDetector
from ..hough_transform.tangent_to_ellipse_detector import TangentToEllipseDetector

class EllipseClassifier(PointOnContourClassifier):
    '''
    This implementation of PointOnContour classifier uses tangency data at the contour points to determine which points lie on an ellipse.
    '''

    def __init__(self, min_tangent_to_line_arc_length, max_tangent_to_line_geometric_residue,
                 min_tangent_to_ellipse_arc_length, max_tangent_to_ellipse_geometric_residue, min_tangent_to_ellipse_area,
                 theta_bin_width, radius_bin_width):
        '''
        min_tangent_to_line_arc_length - the minimum arc length that is needed to determine a tangent vector to a line
        max_tangent_to_line_geometric_residue - the minimum geometric residue that is allowed when fitting tangent vectors to lines

        min_tangent_to_ellipse_arc_length - the minimum arc length that is needed to determine a tangent vector to an ellipse
        max_tangent_to_ellipse_geometric_residue - the minimum geometric residue that is allowed when fitting tangent vectors to ellipses
        min_tangent_to_ellipse_area - we only use ellipses with at least this area to compute tangent lines to an ellipse.
        
        theta_bin_width - the width of the bins we use to discritize the angles of the lines
        radius_bin_width - the width of the bins we use to discritize the radii of the lines
        '''

        self._min_tangent_to_line_arc_length = min_tangent_to_line_arc_length
        self._max_tangent_to_line_geometric_residue = max_tangent_to_line_geometric_residue

        self._min_tangent_to_ellipse_arc_length = min_tangent_to_ellipse_arc_length
        self._max_tangent_to_ellipse_geometric_residue = max_tangent_to_ellipse_geometric_residue
        self._min_tangent_to_ellipse_area = min_tangent_to_ellipse_area

        self._theta_bin_width = theta_bin_width
        self._radius_bin_width = radius_bin_width

        self._line_fitter = FitPointsToLine()
        self._ellipse_fitter = FitPointsToEllipseStable()

        self._theta_bins = None
        self._radius_bins = None
        
        self._tangent_to_line_detector = None
        self._tangent_to_ellipse_detector = None

    def _compute_theta_radius_bins(self):
        self._theta_bins = np.arange(0, 181, self._theta_bin_width)

        L = np.linalg.norm(np.array(self._image.shape))
        self._radius_bins = np.arange(-L, L+1, self._radius_bin_width)

        return

    def _build_tangent_detectors(self):
        self._tangent_to_line_detector = TangentDetector(self._line_fitter, self._min_tangent_to_line_arc_length,
                                                         self._max_tangent_to_line_geometric_residue,
                                                         self._theta_bins, self._radius_bins)
        
        self._tangent_to_ellipse_detector = TangentToEllipseDetector(self._ellipse_fitter,
                                                                     self._min_tangent_to_ellipse_arc_length,
                                                                     self._max_tangent_to_ellipse_geometric_residue,
                                                                     self._min_tangent_to_ellipse_area,
                                                                     self._theta_bins, self._radius_bins)
    def classify_points(self, contour, point_mask=None):
        '''
        contour - a Contour object
        point_mask - a numpy array of length (number of points in contour) and type np.bool

        Return a numpy array of length (number of points on the contour) and type np.bool
        '''
        self._image = contour.get_image()
        self._compute_theta_radius_bins()
        self._build_tangent_detectors()

        #first get a mask of the points that lie on a line segment
        weighted_tangent_lines_dict = self._tangent_to_line_detector.get_weighted_tangent_lines_dict(contour, point_mask)
        tangent_point_on_line_mask = self._tangent_to_line_detector.get_point_mask()

        #now get a mask of the points that lie on an ellipse
        weighted_tangent_lines_dict = self._tangent_to_ellipse_detector.get_weighted_tangent_lines_dict(contour, point_mask)
        tangent_point_on_ellipse_mask = self._tangent_to_ellipse_detector.get_point_mask()

        #combine the two masks
        point_on_ellipse_mask = np.logical_or(tangent_point_on_line_mask, tangent_point_on_ellipse_mask)

        return point_on_ellipse_mask
        
        

