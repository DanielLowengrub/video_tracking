class PointOnContourClassifier(object):
    '''
    This is the base class for "point on contour classifiers".
    The job of these classifiers is to pick out points on a contour that satisfy some condition.
    '''

    def classify_points(self, contour, point_mask=None):
        '''
        contour - a Contour object
        point_mask - a numpy array of length (number of points in contour) and type np.bool

        Return a numpy array of length (number of points on the contour) and type np.bool
        '''

        raise NotImplementedError('classify_points has not been implemented')
