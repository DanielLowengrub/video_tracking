import numpy as np
from ..auxillary import planar_geometry, world_to_image_projections
from ..auxillary.rectangle import Rectangle
from ..auxillary.rectangles import Rectangles

class BallEvaluator(object):
    '''
    This class is used to evaluate how likely it is that a given point in world coordinates is the center of a ball.
    To evaluate the position, it is given an image and the camera that produced it.

    The evaluation is determined as follows:
    1) Let image_position be the corresponding position on the image. Build a patch around this point.
    2) transform the patch into coords in which the projection of the ball is the unit circle.
    3) divide the patch into regions depending on their distance to the origin.
    4) on each region, compute the mean saturation.
    5) convert a sequence s0,...,sk of average saturation values to "ratios" as follows:
       let s_min and s_max be the min and max values. replace si by ai such that si = ai*s_min + (1-ai)*s_max.
    6) assuming the patch is around a ball, the first value should be a0=1, and the ai's should quickely drop to zero 
       after that.
    '''

    PATCH_SIZE = 6    #To evaluate a ball position, we consider a patch whose center is the ball,
                      #and which extends by PATCH_SIZE in each direction.
    MAX_RADIUS = 5    #To evaluate a ball position, we consider concentric circles starting from radius 0, and up to MAX_RADIUS
    MIN_RADIUS = 4    #In order to evaluate a position, at least the first MIN_RADIUS concentric circles must show up on the
                      #patch

    #let s0,...,sk be the average saturation values on each of the concentric circles
    #above. Let s_min and s_max be the min and max values. we replace si by ai such
    #that si = ai*s_min + (1-ai)*s_max. If the concentric circles are centered around
    #a ball in the grass, we expect the sequence [a0,...,ak] to be equal to

    #TARGET_SATURATION_RATIO = [1,0.2,0,0,0]
    TARGET_SATURATION_RATIO = [1.0,0.03,0.02,0.02,0.02]
    
    EPSILON = 0.01      #use this to determine whether two saturations are equal
    DEFAULT_RATIO = 0.5 #If all of the saturations are equal (s0=...=sk), then we set the ratios to be ai=DEFAULT_RATIO

    def __init__(self):

        #to evaluate a position on the image, we average pixel values on concentric annuli (more precisely, ellipses)
        #around the point. The i-th annulus is the set of points x such that
        #start_i <= d(x,position)^2 < end_i
        squares = np.arange(0, self.MAX_RADIUS+1)**2
        self._radius_sq_intervals = [(start,end) for start,end in zip(squares[:-1],squares[1:])]
        
    def _compute_saturation_ratios(self, saturation):
        '''
        saturation - a numpy array with shape (n,k).

        return - a numpy array with shape (n,k).
        Suppose the i-th row is x1,...,xk. Let y_min and y_max be the min and max values. Then, we replace each value xj
        with a value alpha_j such that xj = alpha_j*y_min + (1-alpha_j)*y_max.

        If y_min = y_max then alpha is set by default to 0.5.
        '''
        min_sat = saturation.min(axis=1)
        max_sat = saturation.max(axis=1)

        saturation_ratio = np.empty(saturation.shape)
        
        # #if in a given row, min_value = max_value (i.e, the row has contant values),
        # #then the ratios are not well defined so we set them to a default ratio
        # constant_rows = np.abs(min_sat - max_sat) < self.EPSILON
        # saturation_ratio[constant_rows] = self.DEFAULT_RATIO

        # #compute the ratios of the other rows
        # non_constant_rows = np.logical_not(constant_rows)
        # min_sat = min_sat[non_constant_rows].reshape((-1,1))
        # max_sat = max_sat[non_constant_rows].reshape((-1,1))
        # saturation_ratio[non_constant_rows] = (saturation[non_constant_rows] - max_sat) / (min_sat - max_sat)

        min_sat = min_sat.reshape((-1,1))
        saturation_ratio = 1.0 / (1 + (saturation - min_sat))
        
        return saturation_ratio

    
    def _generate_radius_sq_map(self, ellipse):
        '''
        ellipse - a numpy array with shape (3,3) representing an ellipse
        
        return a tupe (rectangle, radius_sq_map) where
        radius_sq_map - numpy array with shape (2*PATCH_SIZE + 1, 2*PATCH_SIZE + 1) and type int32.
                        the value of the entry (y,x) represents the value of the normalized ellipse on the point 
                        center_of_ellipse+(x,y).
        rectangle - a Rectangle object defining where the radius_sq_map is located on the image. I.e, the center of the
                    rectangle is the center of the ellipse, and it's size is the same size as the radius_sq_map
        '''
        ellipse_center, M, b = planar_geometry.compute_normalized_ellipse(ellipse)
        rectangle_center = np.rint(ellipse_center).astype(np.int32)
        center_offset = rectangle_center - ellipse_center

        # print 'ellipse center: ', ellipse_center
        print 'integer ellipse center: ', rectangle_center
        # print 'center offset: ', center_offset
        
        x = np.arange(-self.PATCH_SIZE, self.PATCH_SIZE+1,1) + center_offset[0]
        y = np.arange(-self.PATCH_SIZE, self.PATCH_SIZE+1,1) + center_offset[1]

        XX, YY = np.meshgrid(x,y)
        ellipse_values = XX*XX*M[0,0] + 2*XX*YY*M[0,1] + YY*YY*M[1,1]

        top_left = rectangle_center - np.array([self.PATCH_SIZE, self.PATCH_SIZE])
        rectangle = Rectangle.from_top_left_and_shape(top_left, ellipse_values.shape)

        return rectangle, ellipse_values

    def compute_radius_sq_masks(self, camera_matrix, ball_radius, ball_position):
        '''
        camera_matrix - a CameraMatrix object
        ball_radius - a float
        ball_position - a numpy array with length 3.

        return - a tuple (rectangle, radius_sq_masks)
        rectangle - a Rectangle object
        radius_sq_masks - a list of MAX_RADIUS numpy arrays. Each one has the same shape as the rectangle and type bool.
        
        If one of the first MIN_RADIUS arrays are empty, return None.
        '''
        ball_ellipse = world_to_image_projections.project_spheres_to_image(camera_matrix, ball_radius, ball_position)[0]

        print 'ball ellipse: '
        print ball_ellipse
        
        rectangle, radius_sq_map = self._generate_radius_sq_map(ball_ellipse)
        rectangles = Rectangles.from_rectangle(rectangle)

        print 'radius sq map:'
        print radius_sq_map.astype(np.int32)
        
        radius_sq_masks = [np.logical_and(start <= radius_sq_map, radius_sq_map < end)
                           for start,end in self._radius_sq_intervals]

        #make sure that the first MIN_RADIUS arrays are not empty
        if all(mask.any() for mask in radius_sq_masks[:self.MIN_RADIUS]):
            return rectangle, radius_sq_masks

        #if one of them is empty, then the radius masks are declared to be degenerate
        else:
            return None, None
    
    def evaluate_radius_sq_masks(self, rectangles, radius_sq_masks, image_sat):
        '''
        rectangles - a Rectangles object.
        radius_sq_masks - a list of MAX_RADIUS numpy arrays with type np.bool and the same shape as the rectangles.
                         This list of masks represents an ellipse as follows: Let M be the normalized 2x2 matrix describing
                         the ellipse. I.e, if K is the center of the ellipse, then a point x is on the ellipse iff
                         f(x) = (x - K)^T * M * (x - K) = 1.
                         Then, the i-th mask marks the pixels for which i^2 <= f(x) < (i+1)^2

        image_sat - a numpy array with shape (n, m) and type np.uint8. It represents the saturation channel of an image.

        return - a numpy array with length (number of rectangles). The i-th number evaluates how close the region of
        the image inside the i-th rectangle is to being an ellipse whose shape is given by the radius masks.

        If one of the first MIN_RADIUS masks is empty, then we raise a ValueError exception.
        '''
        print 'evaluating radius masks on %d rectangles...' % rectangles.get_number_of_rectangles()
        
        #first get the slices corresponding to each rectangle.
        image_sat_slices = rectangles.get_slices(image_sat)
        
        #for each radius mask, sum up the values in the masked pixels in each of the slices
        #mean_saturation is a list with length at most MAX_RADIUS. Each entry is a numpy array with length
        #(number of rectangles)
        saturation = []
        for i in range(self.MAX_RADIUS):
            radius_sq_mask = radius_sq_masks[i]

            #if the radius mask is empty, stop computing the mean saturations
            if not np.any(radius_sq_mask):
                break

            mean_saturation = image_sat_slices[:,radius_sq_mask].mean(axis=1)
            saturation.append(mean_saturation)

        #create a numpy array with shape (num rectangles, k) where k <= MAX_RADIUS
        saturation = np.vstack(saturation).transpose()

        # print 'saturation: '
        # print saturation

        #if there we not enough non zero radius_sq masks, then there is not enought data to evaluate the rectangles.
        if saturation.shape[1] < self.MIN_RADIUS:
            raise ValueError('One of the first MIN_RADIUS radius masks was empty!')
        
        #convert the saturation values in each row to saturation ratios
        saturation = self._compute_saturation_ratios(saturation)

        # print 'saturation ratios:'
        # print saturation
        
        #compare these saturation ratios to the target saturation ratios
        #since the length of the rows in saturation may be less than the maximal possible number,
        #truncate the target saturationif necessary
        target_saturation = self.TARGET_SATURATION_RATIO[:saturation.shape[1]]

        return np.linalg.norm(saturation - target_saturation, axis=1)

    def evaluate_ball_position(self, image_sat, camera_matrix, ball_radius, ball_position):
        '''
        image_sat - a numpy array with shape (n, m) and type np.uint8. It represents the saturation channel of an image.
        camera_matrix - a CameraMatrix object
        ball_radius - a float
        ball_position - a numpy array with length 3.

        return - a float specifying how far the given position is from being a ball position. The smaller the number
        the better the fit.

        If there is not enough information to evaluate the position, return np.nan.
        '''
        print 'evaluating ball at position: ', ball_position
        
        rectangle, radius_sq_masks = self.compute_radius_sq_masks(camera_matrix, ball_radius, ball_position)

        if rectangle is None:
            return np.nan

        print 'image at rectangle:'
        print rectangle.get_slice(image_sat)
        
        rectangles = Rectangles.from_rectangle(rectangle)
        evaluation = self.evaluate_radius_sq_masks(rectangles, radius_sq_masks, image_sat)[0]

        print 'evaluation: ', evaluation
        return evaluation
