import numpy as np
import cv2

from .foreground_extractor import ForegroundExtractor
from ..contour_classifier.stupid_has_sideline_classifier import StupidHasSidelineClassifier
from ..contour_classifier.stupid_multiple_player_classifier import StupidMultiplePlayerClassifier
from ..field_extractor.stupid_field_extractor import StupidFieldExtractor
from ..contour_generator.edges_contour_generator import EdgesContourGenerator
from ..sidelines_extractor.gmm_sidelines_extractor import GMMSidelinesExtractor

class SimpleForegroundExtractor(ForegroundExtractor):
    '''
    A simple implementation of a foreground extractor. It works as follows:
    1) Use the SimpleContourGenerator to get the contours on the (masked part of the) image, and then use the SimpleMultiplePlayerClassifier
       and SimpleHasSidelineClassifier to find the ones which consist of players or contain sidelines.
    3) Use the GMMSidelineExtractor to remove the sidelines from those contours that contain sidelines.
      
    NOTE: We would usually use this class by passing it a pair (image, mask) where the mask represents the part of the image that is actually on the field.
          If the mask contains parts of the image that are not a soccer field, this class will have unexpected behaviour.

    WARNING: In this implementation we assume that if a contour contains sidelines, then the only other objects it contains are players.
    '''

    def __init__(self, player_classifier = StupidMultiplePlayerClassifier(),
                       has_sideline_classifier = StupidHasSidelineClassifier(),
                       contour_generator = EdgesContourGenerator(),
                       sideline_extractor = GMMSidelinesExtractor()):

        self._player_classifier = player_classifier
        self._has_sideline_classifier = has_sideline_classifier
        self._contour_generator = contour_generator
        self._sideline_extractor = sideline_extractor

    def _get_foreground_in_sideline_contour(self, contour):
        '''
        contour - a Contour object

        We assume that the contour contains only sidelines and players.
        Return a mask of the players.
        '''
        contour_mask = contour.get_outer_mask()

        
        cv2.imshow('the contour mask', contour_mask)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

        sidelines_mask = self._sideline_extractor.get_sidelines_mask(contour.get_image(), contour_mask)
        foreground_in_contour_mask = np.logical_and(contour_mask, np.logical_not(sidelines_mask))

        return foreground_in_contour_mask
    
    def get_foreground_mask(self, image, mask=None):
        
        foreground_mask = np.zeros(image.shape[:2], np.bool)
        
        #first get all contours in the image
        contours = self._contour_generator.generate_contours(image, mask)

        #get the ones which consist of players
        player_contours = [contour for contour in contours if self._player_classifier.is_match(contour)]

        #and the ones that contain sidelines
        sideline_contours = [contour for contour in contours if self._has_sideline_classifier.is_match(contour)]

        #add all of the masks from the player contours to the foreground mask
        for contour in player_contours:
            foreground_mask = np.logical_or(foreground_mask, contour.get_mask())

        #for each of the contours with sidelines, remove the sidelines and add the rest as foreground
        for contour in sideline_contours:
            fg = self._get_foreground_in_sideline_contour(contour)
            foreground_mask = np.logical_or(foreground_mask, fg)

        #convert the mask to np.float64 and return it
        return np.float64(foreground_mask)
            
        
        
        
