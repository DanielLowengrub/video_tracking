class ForegroundExtractor(object):
    '''
    The goal of the foreground extractor is to create a mask containing the portion of the image that is in the foreground.

    The main use of this class will be to extract the part of the soccer field containing the players, without the grass or sidelines.
    '''

    def get_foreground_mask(self, image, mask):
        '''
        image - a numpy array of shape (n,m,3) and type np.uint8
        mask - a numpy array of shape (n,m) and tupe np.float64

        Returns a numpy array of shape (n,m) and type np.float64.
        This is a mask on the original image which represents which parts are in the foreground.
        '''

        pass
