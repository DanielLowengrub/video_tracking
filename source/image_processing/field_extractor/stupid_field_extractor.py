import numpy as np
import cv2
from field_extractor import FieldExtractor

class StupidFieldExtractor(FieldExtractor):
    '''
    This is a silly implementation of a field extractor, just to get us off the ground.

    If determines if a pixel is in a field by checking if it's hue is in a certain range.
    '''

    def get_field_mask(self, image, mask=None):

        #first set all the masked parts of the image to zero so they don't get counted as grass.
        if mask is not None:
            image = cv2.bitwise_and(image, image, mask=mask)
            
        img_hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
        img_greyscale = img_hsv[:,:,0]
        grass_filter = (img_greyscale > 35) * (img_greyscale < 55)
        grass_filter = np.float64(grass_filter)

        kernel_open = np.ones((15,15), np.uint8)
        kernel_close = np.ones((30,30), np.uint8)
        opening = cv2.morphologyEx(grass_filter, cv2.MORPH_OPEN, kernel_open)
        closing = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel_close)

        return closing

