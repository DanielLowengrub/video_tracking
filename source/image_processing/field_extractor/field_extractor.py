class FieldExtractor(object):
    '''
    The job of a field extractor is to take an image, and return a mask highlighting the parts of the image that are a soccer field.
    This includes everything on the field such as players, referee and sidelines.

    Alternatively, the field extractor can return a contour outlining the region containing the field.
    '''

    def get_field_mask(self, image, mask):
        '''
        image - a numpy array of shape (n,m,3) and type np.uint8. From this image we will extract the field.
        mask - a numpy array of shape (n,m) and type np.float64. We will only operate on the pixels where the mask is 1.

        Return a mask on the image such that there is a 1 on all pixels in the field, and a 0 on the other pixels.
        The output is a numpy array of shape (n,m) and type np.float64.
        '''

        pass
