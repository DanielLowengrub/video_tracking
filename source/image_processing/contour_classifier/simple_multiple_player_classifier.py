from .contour_classifier import ContourClassifier


MAX_AREA = 2000 #any contour with greater area does not count as multiple players
MIN_AREA = 5    #any contour with smaller area does not count as multiple players
MAX_ASPECT_RATIO = 4 #any contour with greater aspect ratio does not count as multiple players

class SimpleMultiplePlayerClassifier(ContourClassifier):
    '''
    This subclass of ContourClassifier classifies contours containing multiple players (i.e, at least one) and nothing else.

    This is an extremely simple implementation so at the moment we are using just the size of the contour as an indicator.
    Later we will use more sophisticated things such as Haar Cascades or NN.
    '''

    def is_match(self, contour):
        '''
        We classify the contour based on the area, the area of it's convex hull, and it's aspect ratio.
        '''

        area = contour.get_area()
        area_of_convex_hull = contour.get_area_of_convex_hull()
        aspect_ratio = contour.get_aspect_ratio()

        return ((MIN_AREA <= area <= MAX_AREA) and
                (MIN_AREA <= area_of_convex_hull <= MAX_AREA) and
                (aspect_ration <= MAX_ASPECT_RATIO))
        
