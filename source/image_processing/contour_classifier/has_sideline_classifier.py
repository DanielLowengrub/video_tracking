from .contour_classifier import ContourClassifier


MIN_AREA = 2000 #any contour with smaller area is not assumed to contain a contour.

#Question: 1) If we use this with GMMs, can't we make the min area very small, and rely on the gmm to find the sidelines?
#          2) For GMM classification, wouldn't it help if we passed it the GMM model of the field? 

class StupidHasSidelineClassifier(ContourClassifier):
    '''
    This subclass of ContourClassifier classifies contours containing sidelines, and possibly other things.

    This is an extremely simple implementation so at the moment we are using just the size of the contour as an indicator.
    Later we will use more sophisticated things such as GMM, Haar Cascades, NN or other classifiers.
    '''

    def is_match(self, contour):
        '''
        We classify the contour based on the area, the area of it's convex hull, and it's aspect ratio.
        '''

        area = contour.get_area()
        area_of_convex_hull = contour.get_area_of_convex_hull()
        aspect_ratio = contour.get_aspect_ratio()

        return ((area > MAX_AREA) or
                (area_of_convex_hull > MAX_AREA))
