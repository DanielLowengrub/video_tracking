from .contour_classifier import ContourClassifier


MIN_AREA = 2000 #any contour with smaller area is not assumed to contain a sideline.
MIN_AREA_TO_OUTER_AREA_RATIO = 5
MIN_CONVEX_AREA_TO_OUTER_AREA_RATIO = 2
MIN_ASPECT_RATIO = 10

#Question: 1) If we use this with GMMs, can't we make the min area very small, and rely on the gmm to find the sidelines? Answer - No.
#          2) For GMM classification, wouldn't it help if we passed it the GMM model of the field? 

class StupidHasSidelineClassifier(ContourClassifier):
    '''
    This subclass of ContourClassifier classifies contours containing sidelines, and possibly other things.

    This is an extremely simple implementation so at the moment we are using just the size of the contour as an indicator.
    Later we will use more sophisticated things such as GMM, Haar Cascades, NN or other classifiers.
    '''

    def is_match(self, contour):
        '''
        We classify the contour based on the area, the area of it's convex hull, and it's aspect ratio.
        '''

        area = contour.get_area()
        outer_area = contour.get_outer_area()
        area_of_convex_hull = contour.get_area_of_convex_hull()
        aspect_ratio = contour.get_aspect_ratio()

        if area == 0:
            return False

        area_to_outer_area_ratio = area / float(outer_area)
        convex_area_to_outer_area_ratio = area_of_convex_hull / float(outer_area)
        
        return ((area > MIN_AREA) or
                (area_of_convex_hull > MIN_AREA) or
                (area_to_outer_area_ratio > MIN_AREA_TO_OUTER_AREA_RATIO) or
                (convex_area_to_outer_area_ratio > MIN_CONVEX_AREA_TO_OUTER_AREA_RATIO) or
                (aspect_ratio > MIN_ASPECT_RATIO) or
                (aspect_ratio < 1.0 /  MIN_ASPECT_RATIO))
