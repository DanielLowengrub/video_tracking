from .contour_classifier import ContourClassifier


MAX_AREA = 2000 #any contour with greater area does not count as multiple players
MIN_AREA = 5    #any contour with smaller area does not count as multiple players
MAX_ASPECT_RATIO = 10 #any contour with greater aspect ratio does not count as multiple players
MAX_AREA_TO_OUTER_AREA_RATIO = 2
MAX_CONVEX_AREA_TO_OUTER_AREA_RATIO = 2

class StupidMultiplePlayerClassifier(ContourClassifier):
    '''
    This subclass of ContourClassifier classifies contours containing multiple players (i.e, at least one) and nothing else.

    This is an extremely simple implementation so at the moment we are using just the size of the contour as an indicator.
    Later we will use more sophisticated things such as Haar Cascades or NN.
    '''

    def is_match(self, contour):
        '''
        We classify the contour based on the area, the area of it's convex hull, and it's aspect ratio.
        '''

        print 'testing if the following contour is a player contour...'
        area = contour.get_area()
        outer_area = contour.get_outer_area()
        area_of_convex_hull = contour.get_area_of_convex_hull()
        aspect_ratio = contour.get_aspect_ratio()

        if area < MIN_AREA:
            return False

        area_to_outer_area_ratio = area / float(outer_area)
        convex_area_to_outer_area_ratio = area_of_convex_hull / float(outer_area)

        #print 'contour stats:'
        #print '    area: ', area
        #print '    outer area: ', outer_area
        #print '    convex hull area: ', area_of_convex_hull
        #print '    convex area to outer area ratio: ', convex_area_to_outer_area_ratio
        #print '    aspect_ratio: ', aspect_ratio
        
        return ( (area <= MAX_AREA) and
                 (MIN_AREA <= area_of_convex_hull <= MAX_AREA) and
                 (aspect_ratio <= MAX_ASPECT_RATIO) and
                 (area_to_outer_area_ratio < MAX_AREA_TO_OUTER_AREA_RATIO) and
                 (convex_area_to_outer_area_ratio < MAX_CONVEX_AREA_TO_OUTER_AREA_RATIO))
        
