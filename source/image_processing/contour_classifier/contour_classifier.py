class ContourClassifier(object):
    '''
    A contour classifier has one simple job. The user passes it a Contour object and it determines whether or not the contour matches
    the  type of object that it is supposed to classify.

    For exmaple, a SinglePlayerClassifier will return True if and only if the contour contains a single player.
    '''

    def is_match(self, contour):
        '''
        contour - a Contour object.
        Return True if the contour matches the category we are classifying. Otherwise return False.
        '''

        pass
