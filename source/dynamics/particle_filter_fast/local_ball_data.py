import numpy as np
import cv2
import itertools
from ...auxillary.rectangle import Rectangle
from ...auxillary.rectangles import Rectangles
from ...auxillary import world_to_image_projections
from ...auxillary import planar_geometry, np_operations

class LocalBallData(object):
    '''
    This class keeps track of information in a frame that is specific to a ball.
    It stores a SoccerFieldGeometry object, a FrameData object, a list of masks describing a ball shape, 
    and a Rectangles object.

    In addition, it stores an integer valued array with maps particles to rectangles. The idea is that many ball particles
    will occupy the same rectangle on the image, so this allows us to reduce duplication of local calculations.
    '''
    
    MAX_DISTANCE_FROM_FIELD_YARDS = 1 #The farthest a ball is allowed to stray from the soccer field in yards
    
    def __init__(self, soccer_field, frame_data, ball_evaluator, radius_sq_masks, ball_rectangles, particle_to_rectangle_map):
        '''
        soccer_field - a SoccerFieldGeometry object
        frame_data -  FrameData object
        ball_evaluator - a BallEvaluator object
        radius_sq_masks - a list of masks with shape (a,b)
        ball_rectangles - a Rectangles object with shape (a,b)
        '''
        self._soccer_field = soccer_field
        self._frame_data = frame_data
        self._ball_evaluator = ball_evaluator
        self._radius_sq_masks = radius_sq_masks
        self._ball_rectangles = ball_rectangles
        self._particle_to_rectangle_map = particle_to_rectangle_map

    @classmethod
    def _all_balls_on_field(cls, soccer_field, ball_positions):
        '''
        soccer_field - a SoccerFieldGeometry object
        ball_positions - a numpy array with shape (num balls, 3)
        
        return - True iff all of the balls are over the field.
        '''
        max_distance_from_field = soccer_field.convert_yards_to_world_coords(cls.MAX_DISTANCE_FROM_FIELD_YARDS)
        field_rectangle = soccer_field.get_field_rectangle()
        return np.all(field_rectangle.contains_points(ball_positions[:,:2], max_distance_from_field))

    @classmethod
    def _build_radius_sq_masks(cls, soccer_field, frame_data, ball_evaluator, ball_positions):
        '''
        soccer_field - a SoccerFieldGeometry object
        frame_data - a FrameData object
        ball_evaluator - a BallEvaluator object
        ball_positions - a numpy array with shape (num balls, 3)
        
        return a tuple (rectangle, masks)

        rectangle - a Rectangle object
        masks - a list of numpy arrays with shape np.bool and the same shape as the rectangle.

        The rectangle represents a slice of the image surrounding the projection of the average ball position to the image.
        The radius_sq_masks describe the shape of the projected ellipse
        '''

        camera_matrix = frame_data.get_camera_matrix()
        ball_radius = soccer_field.get_soccer_ball_radius()
        average_position = ball_positions.mean(axis=0)

        rectangle, radius_sq_mask = ball_evaluator.compute_radius_sq_masks(camera_matrix, ball_radius, average_position)

        return rectangle, radius_sq_mask

    @classmethod
    def _group_by_image_position(cls, frame_data, ball_positions):
        '''
        frame_data - a FrameData object
        ball_positions - a numpy array with shape (num balls, 3)

        return a tuple (image_positions, particle_to_position_map)

        image_positions - a numpy array with shape (number of distinct image positions, 2) and type np.int32
        particle_to_position_map - numpy array with length (number of balls) and type np.int32
        
        particle_to_position_map[i] = the integer 0 <= j < len(image_positions) s.t the projection of ball_positions[i]
        to the image falls in the pixel image_positions[j].
        '''
        image_positions = frame_data.get_camera_matrix().project_points_to_image(ball_positions).astype(np.int32)

        # print 'all image positions:'
        # print image_positions
        
        group_representatives, group_ids = np_operations.group_by_row(image_positions)

        return group_representatives, group_ids

    @classmethod
    def _has_degenerate_rectangles(cls, frame_data, ball_rectangles):
        '''
        frame_data - FrameData object
        ball_rectangles - Rectangles object

        return - True iff all of the rectangles are in the frame data's image
        '''

        image_rectangle = Rectangles.from_array(frame_data.get_image())
        return not np.all(ball_rectangles.contained_in(image_rectangle))
    
    @classmethod
    def from_frame_and_positions(cls, soccer_field, frame_data, ball_evaluator, ball_positions):
        '''
        soccer_field - a SoccerFieldGeometry object
        frame_data - FrameData object
        ball_evaluator - a BallEvaluator object
        ball_positions - a numpy array with shape (num positions, 3) representing a list of world positions 

        The input is said to be degenerate if:
        * One of the balls is too far from the field
        * One of the balls is not on the image
        * The region on the image corresponding to the ball positions is not sufficiently ball like.
        
        If the data is degenerate, return None
        '''
        if not cls._all_balls_on_field(soccer_field, ball_positions):
            print 'Not all particles in field (mean position: %s). Returning None.' % ball_positions.mean(axis=0)
            return None
        
        #build the radius_sq_masks using the average position
        #radius_rectangle is a Rectangle object which specifies where the radius_sq_masks lie on the image
        radius_sq_rectangle, radius_sq_masks = cls._build_radius_sq_masks(soccer_field, frame_data, ball_evaluator,
                                                                          ball_positions)

        #check if the position on the image is not sufficiently ball like
        if radius_sq_rectangle is None:
            print 'Image patch not ball like. Returning None.'
            return None
        
        #build the particle to rectangle map.
        #This is done by grouping ball positions by the integer values of their positions on the image
        image_positions, particle_to_rectangle_map = cls._group_by_image_position(frame_data, ball_positions)

        # print 'image_positions:'
        # print image_positions
        # print 'particle to rectangle map:'
        # print particle_to_rectangle_map
        
        #build a collection of rectangles whose centers are the positions on the image, and whose shape is
        #equal to the shape of radius_sq_rectangle
        ball_rectangles = Rectangles.from_centers_and_rectangle(image_positions, radius_sq_rectangle)
        
        #make sure that all of the rectangles are in the image
        if cls._has_degenerate_rectangles(frame_data, ball_rectangles):
            print 'Not all rectangles on image. Returning None.'
            return None

        return cls(soccer_field, frame_data, ball_evaluator, radius_sq_masks, ball_rectangles, particle_to_rectangle_map)

    def _number_of_particles(self):
        '''
        return an integer. This is equal to the number of particles of the ball that are recorded in the data.
        This is the number of probabilities that are associated to the data.
        '''
        return len(self._particle_to_rectangle_map)

    def compute_log_probabilities(self):
        '''
        return a numpy array with length (number of particles).
        It represents the log probabilities of each of the particles in this local data object.
        '''
        edge_mask = self._frame_data.get_edge_mask()
        
        #first evaluate the rectangles
        rectangle_log_probs = self._ball_evaluator.evaluate_radius_sq_masks(self._ball_rectangles, self._radius_sq_masks,
                                                                            edge_mask)

        # print 'rectangle log probs:'
        # print rectangle_log_probs
        
        #then map the rectangle probabilities to the particle probabilities
        particle_log_probs = -rectangle_log_probs[self._particle_to_rectangle_map]

        return particle_log_probs
