import numpy as np
from ...auxillary import world_to_image_projections
from ...auxillary.interval import IntervalList
from ...preprocessing.team_labels_generator.team_label import TeamLabel

class TrackedPlayers(object):
    '''
    This stores a sequence of player positions, together with additional data such as position variance and team labels.
    It is used to record the output of a PlayerTracker object.
    '''

    def __init__(self, positions, position_variances, velocities, team_labels):
        '''
        positions - a dictionary whose keys are player ids and whose values are numpy arrays with shape (n, 2) and 
                    type np.float64 where n is the number of frames.
        position_variances - a dictionary whose keys are player ids and whose values are numpy arrays with shape (n, 2) and
                 type np.float64
        velocities - a dictionary whose keys are player ids and whose values are numpy arrays with shape (n, 2) and
                     type np.float64
        team_labels - a dictionary whose keys are player ids and whose values are numpy arrays with length n and type np.int32
        '''

        self._positions = positions
        self._position_variances = position_variances
        self._velocities = velocities
        self._team_labels = team_labels

    def get_copy(self):
        positions_copy = dict((k, v.copy()) for k,v in self._positions.iteritems())
        position_variances_copy = dict((k, v.copy()) for k,v in self._position_variances.iteritems())
        velocities_copy = dict((k, v.copy()) for k,v in self._velocities.iteritems())
        team_labels_copy = dict((k, v.copy()) for k,v in self._team_labels.iteritems())
        
        return TrackedPlayers(positions_copy, position_variances_copy, velocities_copy, team_labels_copy)

    def number_of_frames(self):
        return len(next(self._positions.itervalues()))
    
    def get_player_keys(self):
        return self._positions.keys()
    
    def get_labelled_positions_in_frame(self, frame_index):
        '''
        frame_index - an integer

        return a dictionary with items (player id, (team label, position)). These correspond to the player in the given frame.
        '''

        return dict((k, (self._team_labels[k][frame_index], self._positions[k][frame_index]))
                     for k in self._positions.iterkeys())

    def get_player_mask_in_frame(self, frame_index, image, camera_matrix, position_stdev, player_height):
        '''
        frame_index - an integer
        image - a numpy array with shape (n,m,3) and type np.uint8
        position_stdev - a numpy array with shape (2,) and type np.float64
        player_height - a float

        return a numpy array with shape (n,m) and type np.bool. It represents the regions of the image that are taken up by
        the players, up to the given variance.
        '''

        player_mask = np.zeros(image.shape[:2], np.bool)

        for player_positions in self._positions.itervalues():
            absolute_position = player_positions[frame_index]

            if np.any(np.isnan(absolute_position)):
                continue

            rectangle, mask = world_to_image_projections.project_cylinder_to_image(image, camera_matrix, absolute_position,
                                                                                   position_stdev, player_height)

            if rectangle is None:
                continue
            
            #add this mask to the player mask
            player_mask_slice = rectangle.get_slice(player_mask)
            player_mask_slice[...] = np.logical_or(mask, player_mask_slice)

        return player_mask

    def get_team_labels(self, player_key):
        return self._team_labels[player_key]

    def set_team_labels(self, player_key, team_labels):
        self._team_labels[player_key] = team_labels

    def get_positions(self, player_key):
        return self._positions[player_key]
    
    def get_position_intervals(self, player_key):
        '''
        player_key - an element of get_player_keys()
        return - a list of Interval objects. They represent the intervals for which the given player has a defined position.
        '''
        has_position = np.logical_not(np.isnan(self._positions[player_key]).any(axis=1))
        intervals = IntervalList.from_boolean_array(has_position).get_intervals()

        return intervals

    def redistribute(self, player_interval_lists):
        '''
        player_interval_lists - a list of lists of tuples of the form (player_key, interval)
        
        return - a TrackedPlayer object. It has one player for each of the input lists.
        Let L be one of the lists . The corresponding player p in the output TrackedPlayer object 
        is defined as follows:
        For each tuple (key_i, interval_i) in L, the properties of p in interval_i are equal to the properties
        of player key_i in that interval.
        '''

        new_positions = dict()
        new_position_variances = dict()
        new_velocities = dict()
        new_team_labels = dict()
        
        for i,player_interval_list in enumerate(player_interval_lists):
            print 'player interval list: ', player_interval_list
            
            #initialize the properties of the player corresponding to the player dictionary
            new_positions[i] = np.ones((self.number_of_frames(), 2), np.float64) * np.nan
            new_position_variances[i] = np.ones((self.number_of_frames(), 2), np.float64) * np.nan
            new_velocities[i] = np.ones((self.number_of_frames(), 2), np.float64) * np.nan
            new_team_labels[i] = np.ones((self.number_of_frames(),), np.int32) * TeamLabel.NA

            for player_key, interval in player_interval_list:
                interval.get_slice(new_positions[i])[...] = interval.get_slice(self._positions[player_key])
                interval.get_slice(new_position_variances[i])[...] = interval.get_slice(self._position_variances[player_key])
                interval.get_slice(new_velocities[i])[...] = interval.get_slice(self._velocities[player_key])
                interval.get_slice(new_team_labels[i])[...] = interval.get_slice(self._team_labels[player_key])

        return TrackedPlayers(new_positions, new_position_variances, new_velocities, new_team_labels)
    
    @classmethod
    def _swap_arrays(cls, array_0, array_1, pivot_index):
        '''
        array_i - numpy array
        pivot_index - an integer satisfying: 0 <= pivot_index <= min(len(array_0),len(array_1))

        return a pair of arrays. They are obtained by swapping the parts of the arrays that come after the pivot index.
        '''
        initial_arrays = tuple(array[:pivot_index] for array in (array_0, array_1))
        final_arrays = tuple(array[pivot_index:] for array in (array_0, array_1))

        return tuple(np.concatenate([initial_arrays[i], final_arrays[1-i]]) for i in range(2))
    
    @classmethod
    def swap_players(cls, tracked_players, player_key_0, player_key_1, pivot_index):
        '''
        tracked_players - a TrackedPlayers object
        player_key_i - the key of a player in tracked_players
        pivot_index - an integer

        swap the data of player_key_0 after pivot_index with the data of player_key_1 after pivot_index
        '''
        k0,k1 = player_key_0, player_key_1
        positions = tracked_players._positions
        position_variances = tracked_players._position_variances
        velocities = tracked_players._velocities
        team_labels = tracked_players._team_labels
        
        positions[k0], positions[k1] = cls._swap_arrays(positions[k0], positions[k1], pivot_index)
        position_variances[k0], position_variances[k1] = cls._swap_arrays(position_variances[k0], position_variances[k1],
                                                                          pivot_index)
        velocities[k0], velocities[k1] = cls._swap_arrays(velocities[k0], velocities[k1], pivot_index)
        team_labels[k0], team_labels[k1] = cls._swap_arrays(team_labels[k0], team_labels[k1], pivot_index)

        return
        
