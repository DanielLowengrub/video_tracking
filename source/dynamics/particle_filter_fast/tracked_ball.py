import numpy as np
import cv2
from .ball_particle_filter import BallParticleFilter

class TrackedBall(object):
    '''
    This represents a ball that it getting tracked via a particle filter.

    In particular, it provides the following functionality:
    
    * Get the mean position of the ball together with the standard deviation
    * Check if the ball matches a BallCandidate
    * keep track of the state of the player, and deactivate it if necessary
    '''
    
    def __init__(self, ball_id, ball_trajectory_candidate_id, start_frame, soccer_field, frame_data, ball_particle_filter,
                 on_ground, max_bad_frames, max_position_variance, ground_threshold, evaluation_threshold):
        '''
        ball_id - an integer
        start_frame - the index of the frame where the tracking started
        soccer_field - a SoccerFieldGeometry object
        frame_data - a PaddedFrameData object
        ball_particle_filter - a BallParticleFilter object
        on_ground - a boolean. If True, the tracked ball is rolling on the ground
        max_bad_frames - an integer. If the tracker has more than this number of failed frames in a row then it is decativated
        max_position_variance - the tracker is declared to have failed if the position variance is larger than this
        ground_threshold - the tracker is declared to have failed if it is lower than -ground_threshold
        '''
        
        self._ball_id = ball_id
        self._ball_trajectory_candidate_id = ball_trajectory_candidate_id
        self._start_frame = start_frame
        self._soccer_field = soccer_field
        self._frame_data = frame_data
        self._ball_particle_filter = ball_particle_filter
        self._on_ground = on_ground
        self._max_bad_frames = max_bad_frames
        self._max_position_variance = max_position_variance
        self._ground_threshold = ground_threshold
        self._evaluation_threshold = evaluation_threshold
        
        self._is_active = True
        self._number_of_consecutive_bad_frames = 0
        
        #an ExpectedBallData object
        self._expected_ball_data = self._ball_particle_filter.compute_expected_ball_data()

        #this is set at the end. A larger score means that the tracker was more successfull
        self._tracking_score = None
        
        #for debugging purposes, we store all of the positions and velocities from each frame.
        self._saved_mean_positions = []
        self._saved_mean_velocities = []
        self._saved_evaluations = []
        self._saved_image_positions = []
        
    def get_ball_id(self):
        return self._ball_id

    def get_ball_particle_filter(self):
        return self._ball_particle_filter

    def on_ground(self):
        return self._on_ground
    
    def is_active(self):
        return self._is_active

    def get_start_frame(self):
        return self._start_frame

    def get_number_of_frames(self):
        '''
        return the number of frames over which the ball has been successfully tracked
        '''
        return len(self._saved_mean_positions)
    
    def save_ball_data(self):
        '''
        Save various statistics of the tracker for debugging purposes.
        '''
        #only add information is the player is active
        if self.is_active():
            self._saved_mean_positions.append(self.get_expected_position())
            self._saved_mean_velocities.append(self.get_expected_velocity())
            self._saved_evaluations.append(self._expected_ball_data.get_evaluation())
            self._saved_image_positions.append(self.get_expected_image_position())
            
        return

    def _remove_bad_ball_data(self):
        '''
        remove the last _max_bad_frames from the saved data. 
        This is called after the tracked ball has been unsuccessfull for _max_bad_frames and hence deactivated.
        Since we are not confident that these last frames were successfull either, we removed them from the saved data.
        '''
        self._saved_mean_positions = self._saved_mean_positions[:-self._max_bad_frames]
        self._saved_mean_velocities = self._saved_mean_velocities[:-self._max_bad_frames]
        self._saved_evaluations = self._saved_evaluations[:-self._max_bad_frames]
        self._saved_image_positions = self._saved_image_positions[:-self._max_bad_frames]

        return
    
    def update_particle_filter(self):
        self._ball_particle_filter.update()
        
    def refresh_expected_ball_data(self):
        '''
        We compute the expected ball data using the expected position and variance of the ball.
        This should be done after the weights of the particle filter have been updated.
        '''

        #if the player is not active, do not do anything
        if not self.is_active():
            return
        
        self._expected_ball_data = self._ball_particle_filter.compute_expected_ball_data()

        return
    
    def get_expected_ball_data(self):
        return self._expected_ball_data
        
    def matches_ball_trajectory_candidate(self, ball_trajectory_candidate):
        '''
        ball_trajectory_candidate - BallTrajectoryCandidate object

        return True if this ball is a good match for the ball trajectory candidate. 
        I.e, that is is probable that this ball is the one that generated the data in the candidate.
        '''
        return self._expected_ball_data.matches_ball_trajectory_candidate(ball_trajectory_candidate)
        
    def get_expected_position(self):
        '''
        Return the expected position in world coordinates based
        '''
        return self._ball_particle_filter.get_expected_position()
        
    def get_position_variance(self):
        '''
        Return the variance of the position in world coordinates.
        '''
        return self._ball_particle_filter.get_position_variance()

    def get_expected_image_position(self):
        '''
        Return the expected position of the ball on the image.
        '''
        camera_matrix = self._frame_data.get_camera_matrix()
        world_position = self.get_expected_position()
        image_position = camera_matrix.project_points_to_image(world_position.reshape((1,3)))[0].astype(np.int32)
        return image_position
    
    def get_expected_velocity(self):
        '''
        Return the expected velocity in world coordinates
        '''
        return self._ball_particle_filter.get_expected_velocity()

    def get_velocity_variance(self):
        '''
        Return the variance of the velocity in world coordinates.
        '''
        return self._ball_particle_filter.get_velocity_variance()
    
    def deactivate_if_degenerate(self):
        '''
        Check if the ball tracker is degenerate. If it is, deactivate it and return true.
        If it is not, then return false.

        Note that this is not the same as checking if the tracker has lost the target.
        rather, we just check for degenerate values in the trackers parameters. For example, we make sure that the
        ball is still on the field and in the image.
        '''
        if self._ball_particle_filter.is_degenerate():
            self._is_active = False
            return True

        return False

    def _is_successfully_tracking(self):
        '''
        Return True iff the tracker is successfully tracking an object.
        This should only be called if the tracker is active
        '''
        print 'checking if ball %d is successfully tracking...' % self.get_ball_id()
        
        small_variance = np.linalg.norm(self.get_position_variance()) < self._max_position_variance
        above_ground = self.get_expected_position()[2] >= -self._ground_threshold
        good_evaluation = self._expected_ball_data.get_evaluation() <= self._evaluation_threshold
        
        print '   variance = %f.   small variance: %s' % (np.linalg.norm(self.get_position_variance()), small_variance)
        print '   height = %f.     above ground: %s' % (self.get_expected_position()[2], above_ground)
        print '   evaluation = %f. good evaluation: %s' % (self._expected_ball_data.get_evaluation(), good_evaluation)
        
        tracking_successfull = (small_variance and above_ground and good_evaluation)

        print '  tracking successfull: ', tracking_successfull

        return tracking_successfull
    
    def lost_target(self):
        '''
        return - True if the tracker has lost its target. In that case, deactivate the tracker.
        '''
        if not self.is_active():
            return True

        print 'evaluating the state of ball: ', self.get_ball_id()
        
        tracking_successfull = self._is_successfully_tracking()
        
        if not tracking_successfull:
            self._number_of_consecutive_bad_frames += 1
        else:
            self._number_of_consecutive_bad_frames = 0
            
        #if there are too many bad frames, deactivate the player and do not do anything else
        if self._number_of_consecutive_bad_frames > self._max_bad_frames:
            print 'deactivating the ball %d because of too many bad frames' % self.get_ball_id()
            print '   removing the last %d saved frames' % self._max_bad_frames
            self._is_active = False

            #also remove the last _max_bad_frames from the saved data
            self._remove_bad_ball_data()
            return True
        
        else:
            return False

    def compute_tracking_score(self):
        '''
        compute the score which indicates how well the tracker has done. This should be called after the tracking has been
        completed.
        A higher score indicates that the tracker has done better.
        '''
        #note the we record the *negative* of the mean evaluation since we want a larger number to indicate a better match
        self._tracking_score = -np.array(self._saved_evaluations).mean()
        return
    
    def get_positions(self):
        '''
        Return an array with shape (num active frames, 3) recording the absolute positions of the player.
        '''
        return np.vstack(self._saved_mean_positions)

    def get_image_positions(self):
        '''
        Return an array with shape (num active frames, 2) recording the image positions of the player.
        '''
        return np.vstack(self._saved_image_positions)
        
    def get_velocities(self):
        '''
        Return an array with shape (num active frames, 3) recording the velocity of the player.
        '''
        return np.vstack(self._saved_mean_velocities)

    def get_tracking_score(self):
        '''
        return a float which indicates how well the tracker has done. 
        A higher score indicates that the tracker has done better.

        This should only be called after "compute_tracking_score"
        '''
        return self._tracking_score
    
    def get_summary(self):
        summary = ''
        summary +=  '--------------\n'
        summary +=  'summary of  %d:\n' % self._ball_id
        summary +=  'start frame: %d, candidate id: %d\n' % (self._start_frame, self._ball_trajectory_candidate_id)
        summary +=  'is active: %s:\n' % self.is_active()

        if not self.is_active():
            return summary

        summary += 'expected evaluation: %f\n' % self._expected_ball_data.get_evaluation()
        summary += 'number of consecutive bad frames: %d\n' % self._number_of_consecutive_bad_frames
        summary += 'expected image position: %s\n' % self.get_expected_image_position()
        summary += '\n'
        summary += 'expected position: %s\n' % self.get_expected_position()
        summary += 'position variance: %s\n' % self.get_position_variance()
        summary += '\n'
        summary += 'expected velocity: %s\n' % self.get_expected_velocity()
        summary += 'velocity variance: %s\n' % self.get_velocity_variance()

        return summary

    @classmethod
    def from_initial_state_on_ground(cls, tracked_ball_id, ball_trajectory_candidate_id,
                                     frame_index, field_position, field_velocity, initial_velocity_stdev,
                                     num_particles, ball_evaluator, soccer_field, frame_data,
                                     max_bad_frames, max_position_variance, ground_threshold, evaluation_threshold):
        '''
        tracked_ball_id - an integer
        frame_index - an integer
        field position - a numpy array with length 2 and type np.float64
        field velocity - a numpy array with length 2 and type np.float64
        initial_velocity_stdev - a float
        num_particles - an integer
        ball_evaluator - a BallEvaluator object
        soccer_field - a SoccerFieldGeometry object
        frame_data - a FrameData object
        max_bad_frames - an integer. If the tracker has more than this number of failed frames in a row then it is decativated
        max_position_variance - the tracker is declared to have failed if the position variance is larger than this
        ground_threshold - the tracker is declared to have failed if it is lower than -ground_threshold
            
        return a TrackedBall object
        '''

        ball_particle_filter = BallParticleFilter.from_initial_state_on_ground(field_position, field_velocity,
                                                                               initial_velocity_stdev,
                                                                               num_particles, ball_evaluator,
                                                                               soccer_field, frame_data)

        on_ground = True
        
        return cls(tracked_ball_id, ball_trajectory_candidate_id, frame_index, soccer_field, frame_data, ball_particle_filter,
                   on_ground, max_bad_frames, max_position_variance, ground_threshold, evaluation_threshold)

    
    @classmethod
    def from_initial_state(cls, tracked_ball_id, ball_trajectory_candidate_id,
                           frame_index, initial_position, initial_velocity, initial_velocity_stdev,
                           num_particles, ball_evaluator, soccer_field, frame_data,
                           max_bad_frames, max_position_variance, ground_threshold, evaluation_threshold):
        '''
        tracked_ball_id - an integer
        frame_index - an integer
        initial_position - a numpy array with length 3 and type np.float64
        initial_velocity - a numpy array with length 3 and type np.float64
        initial_velocity_stdev - a float
        num_particles - an integer
        ball_evaluator - a BallEvaluator object
        soccer_field - a SoccerFieldGeometry object
        frame_data - a FrameData object
        max_bad_frames - an integer. If the tracker has more than this number of failed frames in a row then it is decativated
        max_position_variance - the tracker is declared to have failed if the position variance is larger than this
        ground_threshold - the tracker is declared to have failed if it is lower than -ground_threshold
            
        return a TrackedBall object
        '''

        ball_particle_filter = BallParticleFilter.from_initial_state(initial_position, initial_velocity,
                                                                     initial_velocity_stdev, num_particles, ball_evaluator,
                                                                     soccer_field, frame_data)

        on_ground = False
        
        return cls(tracked_ball_id, ball_trajectory_candidate_id, frame_index, soccer_field, frame_data, ball_particle_filter,
                   on_ground, max_bad_frames, max_position_variance, ground_threshold, evaluation_threshold)
