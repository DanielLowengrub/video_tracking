import numpy as np

class ParticleFilter(object):
    '''
    A Particle Filter holds a collection of particles.
    It has methods to update the particles, compute their probabilities and weights, and to resample them.

    update: This simply calls the update method in each of the particles.
    compute_particle_probabilities: compute the probabilities of each of the particles
    compute_particle_weights: compute the weights of the particles based on their probabilities.
    resample: sample N particles from the list using their weights, and clone them to create a new list of particles.
    '''

    LOG_STDEV = 2.0
    MIN_N_EFF = 150
    
    def __init__(self, particles):
        '''
        particles - a numpy array with shape (num particles, particle dimension)
        '''

        self._particles = particles

        #we initialize the particles to have uniform weights and zero probabilities
        self._uniform_weight = 1.0 / len(particles)
        self._particle_weights = np.zeros(len(self._particles), np.float32)
        self._particle_probabilities = np.zeros(len(self._particles), np.float32)
        self._particle_log_probabilities = np.zeros(len(self._particles), np.float32)

        self._set_uniform_weights()

    def _num_particles(self):
        return len(self._particles)

    def get_number_of_particles(self):
        return self._num_particles()
    
    def _set_uniform_weights(self):
        self._particle_weights[:] = self._uniform_weight

    def _set_probabilities_to_zero(self):
        self._particle_probabilities *= 0.0
        self._particle_log_probabilities *= 0.0
        
        return

    def update(self):
        '''
        Update the particles by one timestep
        '''
        raise NotImplementedError('update has not been implemented')

    def compute_particle_probabilities(self):
        '''
        Compute the probabilities of each of the particles.
        '''

        raise NotImplementedError('compute_player_probabilities has not been implemented')
    
    def compute_particle_weights(self):
        '''
        Compute weights for each of the particles based on their probability
        '''
        #multiply the weights of each particle by the particle probability
        self._particle_weights *= self._particle_probabilities

        #normalize the weights so that they sum to one
        total_weight = self._particle_weights.sum()
        self._particle_weights /= total_weight

        print 'weights range: (%.3f, %.3f)' % (self._particle_weights.min(), self._particle_weights.max())
        return

    def convert_log_probability_to_probability(self):
        '''
        First normalize the log probabilities to have mean 0 and the given stdev.
        Then exponentiate the resulting scaled log probabilities.
        '''
        print 'converting log probabilities to probabilities...'
        #print 'log prob range: (%f, %f)' % (self._particle_log_probabilities.min(), self._particle_log_probabilities.max())
        
        self._particle_log_probabilities -= self._particle_log_probabilities.mean()

        log_probabilities_std = self._particle_log_probabilities.std()
        if log_probabilities_std > 0:
            self._particle_log_probabilities *= (self.LOG_STDEV / log_probabilities_std)

        #print 'normalized log prob range: (%f, %f)' % (self._particle_log_probabilities.min(),
        #                                               self._particle_log_probabilities.max())

        self._particle_probabilities = np.exp(self._particle_log_probabilities)
        self._particle_probabilities /= self._particle_probabilities.sum() #NEW
        print 'prob range: (%.3f, %.3f)' % (self._particle_probabilities.min(), self._particle_probabilities.max())
        return
        
    def resample(self):
        '''
        Check if the effective number of particles is too small. If it is, resample the particles.
        We also set the weights to be uniform.
        '''
        N_eff = self._effective_number_of_particles()

        if N_eff > self.MIN_N_EFF:
            print 'not resampling with N_eff = ', N_eff
            return

        print 'resampling with N_eff = ', N_eff
        
        #sample a list of indices weighted by the values of particle_weights
        sampled_indices = np.random.choice(len(self._particle_weights), len(self._particle_weights), p=self._particle_weights)

        #use the sampled indices to create new particles
        self._particles = self._particles[sampled_indices]

        #we also have to update the weights and probabilities
        self._particle_probabilities = self._particle_probabilities[sampled_indices]
        self._particle_log_probabilities = self._particle_log_probabilities[sampled_indices]
        
        #set the weights of the particles to 1 / (number of particles).
        self._set_uniform_weights()

        return

    def _effective_number_of_particles(self):
        '''
        Return 1 / (sum of w^2, where w varies over all particle weights)
        '''
        return 1.0 / ( (self._particle_weights * self._particle_weights).sum() )
        
    def compute_joint_log_probabilities(self, other_particle_filters):
        '''
        We compute the joint log probabilities of each of the particles in this particle filter 
        w.r.t the other particle filters. 
        '''
        raise NotImplementedError('compute_joint_log_probabilities has not been implemented')
    
    def get_expected_state_vector(self):
        '''
        return - a numpy array with length k where k is the number of dimensions in a state.
        '''

        raise NotImplementedError('get_expected_state_vector has not been implemented.')

