import itertools
import collections
import numpy as np
import cv2
from .tracked_player import TrackedPlayer
from .tracked_players import TrackedPlayers
from .mean_field import MeanField, ParticleFilterGraph
from .padded_frame_data import PaddedFrameData
from .expected_player_data import ExpectedPlayerData
from ...preprocessing_full.team_labels_controller.team_label import TeamLabel
from ....testers.auxillary import primitive_drawing
from ...auxillary import planar_geometry

class PlayerTracker(object):
    '''
    The job of this class is to track a list of TrackedPlayer objects through a sequence of images

    It works as follows:
    Initialization:
        * We use the contours in the first frame to initialize a list of TrackedPlayers, one for each player contour.
        * Each TrackedPlayer is given PlayerParticleFilter which stores a list of PlayerParticles.
        
    Timestep:
        * Update positions of active and seeking players
        * Throw out degenerate players: degenerate = off screen or off field
        * Compute adjacencies between active players
        * Compute player probability and weights: For active adjacent players use mean field.
        * Resample particle filters
        * Add new seeking players: Currently, this is done based on player contours.
        * Evaluate players
        * Update player states: We deactivate active and seeking players that are performing poorly, and activate seeking players that are doing well.
    '''

    FRAME_PADDING = 50 #the number of pixels we pad the frame images with before tracking
    PARTICLES_PER_PLAYER = 1000 #300 #the number of particles we use to track an individual player

    #MEAN_FIELD_TOLERANCE = 2 #the tolerence determining the convergence of the mean field equations
    MEAN_FIELD_TOLERANCE = 1 #NEW #the tolerence determining the convergence of the mean field equations

    NUMBER_OF_PLAYERS = 25
    
    def __init__(self, frame_data_iterator, soccer_field, visualizer=None):
        '''
        frame_data_iterator - an iterator of FrameData objects
        soccer_field - a SoccerFieldGeometry object
        visualizer - a PlayerTrackerVisualizer object which is used to generate debugging data
        '''

        self._frame_data_iterator = frame_data_iterator
        self._soccer_field = soccer_field
        self._visualizer = visualizer
        
        self._current_frame_data = None
        
        #a dictionary of tracked_player objects that are successfully tracking a player. Keys are player ids.
        self._active_players = None

        #a dictionary of tracked_player objects that are trying to aquire a target player. Keys are player ids.
        self._seeking_players = None

        #a dictionary of tracked_player objects that are currently not tracking anything. Keys are player ids.
        self._dormant_players = None

        #the mean field equation solver
        self._mean_field = MeanField(self.MEAN_FIELD_TOLERANCE, visualizer=self._visualizer)

    def _active_and_seeking_players(self):
        '''
        Return an iterator over all active and seeking players.
        '''
        return itertools.chain(self._active_players.itervalues(), self._seeking_players.itervalues())

    def _get_active_or_seeking_player(self, player_id):
        '''
        player_id - the id of an active or seeking player.
        We return the player with this id
        '''
        if player_id in self._active_players:
            return self._active_players[player_id]

        elif player_id in self._seeking_players:
            return self._seeking_players[player_id]

        else:
            raise ValueError('player %d is not an active or seeking player' % player_id)
        
    def _all_players(self):
        '''
        Return an iterator over all players.
        '''
        return itertools.chain(self._active_players.itervalues(), self._seeking_players.itervalues(), self._dormant_players.itervalues())

    def _initialize(self):
        '''
        Initialize the lists of tracked players.
        '''

        self._active_players = dict()
        self._seeking_players = dict()
        self._dormant_players = dict()

        #grab the first frame data to initialize the dormant players
        frame_data = next(self._frame_data_iterator)
        self._current_frame_data = PaddedFrameData.from_frame_data(frame_data, self.FRAME_PADDING)

        #put it back into the iterator
        self._frame_data_iterator = itertools.chain([frame_data], self._frame_data_iterator)
        
        #set up some dormant players
        for player_id in range(self.NUMBER_OF_PLAYERS):
            tracked_player = TrackedPlayer.build_dormant_player(player_id, self.PARTICLES_PER_PLAYER,
                                                                self._soccer_field, self._current_frame_data)
            self._dormant_players[player_id] = tracked_player
                    
        print 'after initialization:'
        print 'the active players are: ', self._active_players.keys()
        print 'the seeking players are: ', self._seeking_players.keys()
        print 'the dormant players are: ', self._dormant_players.keys()
        return

    def _update_particle_filters(self):
        '''
        Update the particle filters in each of the active and seeking players
        Note that this changes their state vectors, but not the probabilities and weights.
        '''
        #Update the active and seeking particle filters to the current frame.
        for tracked_player in self._active_and_seeking_players():
            tracked_player.update_particle_filter()

        return
    
    def _deactivate_degenerate_players(self):
        '''
        Deactivate active and seeking players that are degenerate. This can happen if their image is off the screen,
        or their position is off the field.
        '''
        print 'deactivating degenerate players...'
        for player in list(self._active_and_seeking_players()):
            dict_with_player = self._active_players if player.is_active() else self._seeking_players

            if player.deactivate_if_degenerate():
                print '   player %d is degenerate.' % player.get_player_id()
                self._dormant_players[player.get_player_id()] = player
                del dict_with_player[player.get_player_id()]
        return

    def _build_particle_filter_graph(self):
        '''
        Find all pairs of players in the current frame that are adjacent. Return a tuple:
        (particle_filter_graph, isolated_particle_filters) where
        
        * particle_filter_graph - a ParticleFilterGraph object. Its keys are all the active particle filters that have
          overlap with a different active particle filter. Two such player share an edge if they overlap.

        * isolated_particle_filters = a dictionary containing a key,value pair (player id, players particle filter) 
          for each seeking player, and each active player that is NOT adjacent to another active player
        '''

        print 'computing particle filter graph...'
        
        #this dictinary has items (k, l1,...,ln) where k is a key of an active pf, and l1,...ln are keys of active pfs
        #that overlap with the k-th pf.
        neighbors = collections.defaultdict(list)
        
        #iterate over all pairs of active players
        for player_A, player_B in itertools.combinations(self._active_players.itervalues(), 2):
            if player_A.has_overlap_with_player(player_B):
                print 'players %d and %d are adjacent' % (player_A.get_player_id(), player_B.get_player_id())

                #add these players to the dictionary of adjacent players (if they are not already there)
                #use the player id as the key
                neighbors[player_A.get_player_id()].append(player_B.get_player_id())
                neighbors[player_B.get_player_id()].append(player_A.get_player_id())

        #use the neighbors dictionary to build a pf graph
        vertices = dict((k, self._active_players[k].get_player_particle_filter()) for k in neighbors.iterkeys())
        particle_filter_graph = ParticleFilterGraph(vertices=vertices, neighbors=neighbors)
        
        #we now get the dictionary of isolated players
        isolated_particle_filter_dict = dict()
        for player in self._active_and_seeking_players():
            if not player.get_player_id() in vertices:
                isolated_particle_filter_dict[player.get_player_id()] = player.get_player_particle_filter()

        print 'the adjacent player pairs are:'
        print neighbors.keys()
        print 'the adjacent player graph is:'
        print neighbors
        print 'the isolated players are:'
        print isolated_particle_filter_dict.keys()
        
        return particle_filter_graph, isolated_particle_filter_dict

    def _compute_probabilities_and_weights(self, frame_index=None):
        '''
        frame_index - an integer. This is used for debugging purposes only.

        Compute the probabilities and weights of all active and seeking players.
        This should be called after the positions of the tracked players have been updated.
        '''
        print 'computing probabilities and weights...'
        #the first step in computing the probabilities
        #is to determine which players are adjacent to others, and which are isolated
        particle_filter_graph, isolated_particle_filter_dict = self._build_particle_filter_graph()

        #use the pf graph and mean field to estimate the joint probabilities of the adjacent players
        #this also updates the player weights
        print '   using mean field to compute probabilities of %d players...' % len(particle_filter_graph.vertices)
        if len(particle_filter_graph.vertices) > 0:
            self._mean_field.set_particle_filter_graph(particle_filter_graph)
            self._mean_field.approximate_joint_probabilities(frame_index=frame_index)

        #compute the probabilities of the isolated (active and seeking) players as usual
        print '   computing probabilities for %d isolated players...' % len(isolated_particle_filter_dict)
        for player_id, particle_filter in isolated_particle_filter_dict.iteritems():
            print '   computing probabilities of player %d' % player_id
            particle_filter.compute_particle_probabilities()
            particle_filter.compute_particle_weights()

        #now that we have the updated weights for all the active and seeking players, resample the particle filters
        for tracked_player in self._active_and_seeking_players():
            #print 'resampling player ', tracked_player.get_player_id()
            tracked_player.get_player_particle_filter().resample()

        return

    def _compute_expected_player_obstructions(self):
        '''
        update the ExpectedPlayerData objects in the active and seeking players so that their obstruction masks reflect the
        obstructions coming from other players.
        '''
        expected_player_datas = [player.get_expected_player_data() for player in self._active_and_seeking_players()]

        
        ExpectedPlayerData.build_obstructions(expected_player_datas)
        return

    def _compute_expected_player_data(self):
        '''
        Refresh the player data of each of the seeking and active player.
        Also, compute the obstruction masks for each of these players. The obstruction mask is stored in the ExpectedPlayerData
        and it records which parts of this player are obstructed by other players.
        '''
        print 'refreshing expected player obstructions for %d players...' % len(list(self._active_and_seeking_players()))
        
        for player in self._active_and_seeking_players():
            player.refresh_expected_player_data()

        self._compute_expected_player_obstructions()

        return

    def _evaluate_players(self):
        '''
        Evaluate how well the players are doing. Based on this, change the player states.
        For example, an active player that is not performing well may become deactivated.

        Redistribute the players among the different player lists according to their state.
        '''
        #make one new dictionary for each state
        new_player_dictionaries = dict((state_id, dict()) for state_id in TrackedPlayer.all_state_ids())

        for player in self._all_players():

            #update the player state based on how well it is doing
            player.update_state()

            #then put it in the appropriate dictionary
            state_dictionary = new_player_dictionaries[player.get_state_id()]
            state_dictionary[player.get_player_id()] = player
                
        self._active_players = new_player_dictionaries[TrackedPlayer.active_id()]
        self._seeking_players = new_player_dictionaries[TrackedPlayer.seeking_id()]
        self._dormant_players = new_player_dictionaries[TrackedPlayer.dormant_id()]
        
        return

    def _update_team_labels(self, player_matches):
        '''
        player_matches - dictionary with items (player id, PlayerCandidate object that matches the player)

        for each of the players in the dictionary, update the player's team label with the player candidate's label.
        for players not in the dictionary, update the team label to "not available"
        '''
        print 'updating team labels with %d player matches...' % len(player_matches)
        
        for player in self._active_and_seeking_players():
            player_id = player.get_player_id()

            #if there is a player candidate that matches this player
            if player_id in player_matches:
                player_candidate = player_matches[player_id]
                player.update_team_label(player_candidate.team_label)

            #if there is no match, update it with a "not available" team label
            else:
                player.update_team_label(TeamLabel.NA)

        return
    
    def _generate_debugging_data(self, frame_index):
        '''
        Print out a summary of the current state of the player tracker, and save debugging information for
        later processing.
        '''
        #finally, print and save some debugging data
        print 'after updating the particle filter, the tracked players are:'
        
        for tracked_player in self._all_players():
            tracked_player.print_summary()

            #also, we save a snapshot of the player for debugging purposes
            tracked_player.save_player_data()

        print 'there are %d active players: %s' % (len(self._active_players),  self._active_players.keys())
        print 'there are %d seeking players: %s' % (len(self._seeking_players), self._seeking_players.keys())

        #now generate debugging visualizations
        frame_data = self._current_frame_data
        tracked_players = list(self._active_and_seeking_players())

        # image_copy = frame_data.get_image().copy()
        # H = frame_data.get_camera_matrix().get_homography_matrix()
        # for tp in tracked_players:
        #     points_on_image = planar_geometry.apply_homography_to_points(H, tp._player_particle_filter._position_slice())
        #     for p in points_on_image:
        #         primitive_drawing.draw_point(image_copy, p, (255,0,0))
                
        # cv2.imshow('image with points', image_copy)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        # self._visualizer.generate_player_ids(frame_index, frame_data, tracked_players)
        # self._visualizer.generate_image_and_absolute_positions(frame_index, frame_data, tracked_players)
        # self._visualizer.generate_player_particles(frame_index, frame_data, tracked_players)
        # self._visualizer.generate_player_masks(frame_index, frame_data, tracked_players)

        return
    
    # def _matches_active_or_seeking_player(self, player_candidate):
    #     '''
    #     player_candidate - a PlayerCandidate object. Return True if there is an active or seeking player that matches the
    #     player candidate.
    #     '''
    #     has_match = any(player.matches_player_candidate(player_candidate) for player in self._active_and_seeking_players())
    #     #print 'found match: ', has_match

    #     return has_match
        
    def _match_player_candidates_to_players(self, player_candidates):
        '''
        player_candidates - a list of PlayerCandidate objects.

        Return a a tuple (isolated_candidates, player_matches)
        isolate_candidates - a list of PlayerCandidate objects that do not match any active or seeking players
        player_matches - a dictionary with key values (player id, a PlayerCandidate that matches that player)
        '''
        print 'trying to match %d player candidates...' % len(player_candidates)
        
        isolated_candidates = []
        player_matches = dict()
        
        for pc in player_candidates:
            print 'finding matches for player candidate with position: ', pc.absolute_position
            #this is a list of all of the players that match the player candidate
            matching_players = [p for p in self._active_and_seeking_players() if p.matches_player_candidate(pc)]
            
            if len(matching_players) == 1:
                print '   the candidate matches a player, adding to player candidate matches.'
                player_matches[matching_players[0].get_player_id()] = pc
                
            elif len(matching_players) == 0:
                print '   the candidate does not match a player, adding it to list of isolated candidates'
                isolated_candidates.append(pc)

            else:
                print 'the candidate matches more than one player. throwing it out.'

        #isolated_candidates = [pc for pc in player_candidates if not self._matches_active_or_seeking_player(pc)]

        print 'returning %d isolated candidates.' % len(isolated_candidates)
        print 'with positions: '
        print sorted([pc.absolute_position for pc in isolated_candidates], key=lambda x: x[1])
        print 'the player positions are:'
        print sorted([p.get_expected_position() for p in self._active_and_seeking_players()], key=lambda x: x[1])
        
        return isolated_candidates, player_matches

    #TODO
    #To preserve continuity, we should always choose the dormant player that was last seen closest to the candidate contour
    def _add_seeking_players(self, player_candidates):
        '''
        player_candidates - a list of PlayerCandidate objects that we wish to turn into seeking players
        Try to change some of the dormant players into seeking players.
        To do this, we look at all player contours which do not have an overlap with an active or seeking player.
        We use each such contour to build a TrackedPlayer. 
        If the player matches the contour, we add this to the dictionary of seeking players.
        '''
        print 'adding seeking players...'

        #stop if there are no dormant players then there is nothing to do
        if len(self._dormant_players) == 0:
            return
            
        #isolated_player_candidates = self._find_isolated_player_candidates(player_candidates)

        print '   there are %d isolated player candidates.' % len(player_candidates)
        
        #try to turn the player candidates into seeking players
        for player_candidate in player_candidates:
            player_id, new_seeking_player = self._dormant_players.popitem()
            new_seeking_player.start_seeking(player_candidate)
            self._seeking_players[player_id] = new_seeking_player

            print 'created a new seeking player with id %d and team label %s at position %s' % (player_id, player_candidate.absolute_position, player_candidate.team_label)
            
            #stop if there are no more dormant players to add
            if len(self._dormant_players) == 0:
                return

        return    
    
    def _update(self, frame_index, frame_data):
        '''
        frame_index - the index of the frame we want to update to
        frame_data - a FrameData object which contains the data of the next frame.

        Update to the next timestep.
         '''

        print '--------------------------------------'
        print 'Updating to frame %d' % (frame_index)
        print '--------------------------------------'

        #update the frame data so that it contains the data of the frame that we are updating to.
        #note that this changes _current_frame_data, and so it is seen by all of the tracked players since they store a
        #reference to _current_frame_data
        self._current_frame_data.update_from_frame_data(frame_data)

        # print 'summary of frame we are updating to: '
        # print '   player candidates: '
        # print self._current_frame_data.get_player_candidates()
        
        # cv2.imshow('image', self._current_frame_data.get_image())
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()
        
        # cv2.imshow('fg mask', np.float32(self._current_frame_data.get_foreground_mask()))
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        #update the state vectors of the particle filters
        self._update_particle_filters()
        
        #since the positions of the players have changed, some players may have become degenerate.
        #for example, this includes players that have gone off the field, or that have gone off the screen.
        self._deactivate_degenerate_players()

        #also, we have to recompute the probabilities and weights of the players.
        self._compute_probabilities_and_weights(frame_index=frame_index)

        #after updating the player positions and probabilities, the expected player data has to be refreshed.
        #the ExpectedPlayerData objects record the expected positions of the players, together with the variance.
        self._compute_expected_player_data()
        
        #evaluate how well the players are doing and, based on this, update their states
        self._evaluate_players()

        #find which of the player candidates in this frame match players, and which are isolated
        player_candidates = self._current_frame_data.get_player_candidates()
        isolated_candidates, player_matches = self._match_player_candidates_to_players(player_candidates)

        #use the player candidate matches to update the team labels of the players
        self._update_team_labels(player_matches)
        
        #before adding new seeking players, display a summary of the state of the tracker
        self._generate_debugging_data(frame_index)
        
        #try to turn the isolated player candidates into seeking players
        self._add_seeking_players(isolated_candidates)
        
        return

    def track_players(self):
        '''
        Track the players in the given frames.
        '''

        self._initialize()
        next_frame_index = 0
        next_frame_data = next(self._frame_data_iterator, None)
        
        while next_frame_data is not None: # and next_frame_index < 50:
            # #DEBUGGING HACK: remove all the player candidates except for one of them in the first frame
            # if next_frame_index < 117:
            #     next_frame_index += 1
            #     next_frame_data = next(self._frame_data_iterator, None)
            #     continue
            
            # elif next_frame_index == 117:
            #     next_frame_data._player_candidates = [next_frame_data._player_candidates[4]]

            # else:
            #     next_frame_data._player_candidates = []
                
            self._update(next_frame_index, next_frame_data)

            next_frame_index += 1
            next_frame_data = next(self._frame_data_iterator, None)

        return

    def get_player_positions(self):
        '''
        Return a dictionary whose keys are player id's, and whose values are np arrays with shape
        (num frames, 2) corresponding to the absolute position of a player. If the player was not active in a certain frame,
        the value at that frame is [np.nan,np.nan]
        '''

        return dict((player.get_player_id(), player.get_positions()) for player in self._all_players())

    def get_player_velocities(self):
        '''
        Return a dictionary whose keys are player id's, and whose values are np arrays with shape
        (num frames, 2) corresponding to the velocity of a player. If the player was not active in a certain frame,
        the value at that frame is [np.nan,np.nan]
        '''

        return dict((player.get_player_id(), player.get_velocities()) for player in self._all_players())

    def get_player_position_variances(self):
        '''
        Return a dictionary whose keys are player id's, and whose values are np arrays with shape
        (num frames, 2) corresponding to the variance of the absolute position of a player. If the player was not active in a certain frame,
        the value at that frame is [np.nan,np.nan]
        '''

        return dict((player.get_player_id(), player.get_position_variances()) for player in self._all_players())

    def get_player_particle_positions(self):
        '''
        Return a dictionary whose keys are player id's, and whose values are np arrays with shape
        (num frames, num particles, 2) corresponding to the positions of the particles of a player. 
        If the player was not active in a certain frame, the values at that frame are nan
        '''

        return dict((player.get_player_id(), player.get_position_variances()) for player in self._all_players())

    def get_team_labels(self):
        '''
        Return a dictionary whose keys are player id's, and whose values are np arrays with length
        (num frames) corresponding to the team labels of the player at various frames
        '''

        return dict((player.get_player_id(), player.get_team_labels()) for player in self._all_players())

    def get_tracked_players(self):
        '''
        return - a TrackedPlayers object
        '''
        positions = self.get_player_positions()
        variances = self.get_player_position_variances()
        velocities = self.get_player_velocities()
        team_labels = self.get_team_labels()

        return TrackedPlayers(positions, variances, velocities, team_labels)
    
    # def get_player_position_entropies(self):
    #     '''
    #     Return a dictionary whose keys are player id's, and whose values are np arrays with shape
    #     (num frames, 2) corresponding to the entropy of the absolute position of a player. If the player was not active in a certain frame,
    #     the value at that frame is [np.nan,np.nan]
    #     '''

    #     return dict((player.get_player_id(), player.get_position_entropies()) for player in self._all_players())

    # def get_player_obstructed_percentages(self):
    #     '''
    #     Return a dictionary whose keys are player id's, and whose values are np arrays with length
    #     num frames corresponding to the obstruction percentage of a player. If the player was not active in a certain frame,
    #     the value at that frame is np.nan
    #     '''

    #     return dict((player.get_player_id(), player.get_obstructed_percentages()) for player in self._all_players())

    # def get_player_unobstructed_foreground_percentages(self):
    #     '''
    #     Return a dictionary whose keys are player id's, and whose values are np arrays with length
    #     num frames corresponding to the unobstructed foreground percentage of a player. If the player was not active in a certain frame,
    #     the value at that frame is np.nan
    #     '''

    #     return dict((player.get_player_id(), player.get_unobstructed_foreground_percentages()) for player in self._all_players())
