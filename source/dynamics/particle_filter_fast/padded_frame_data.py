import numpy as np
from .frame_data import FrameData, PlayerCandidate
from ...auxillary.camera_matrix import CameraMatrix
from ...auxillary import planar_geometry
from ...auxillary.rectangle import Rectangle

class PaddedFrameData(FrameData):
    '''
    This implementation of FrameData takes into account that the image and fg mask have been padded with zeros.
    The main differences between this and a FrameData object is that:
    * it stores a rectangle which represents the portion of the image and masks which are in the original non padded versions
    * it stores a bg mask in addition to a fg mask. The point is that because of the padding, the bg mask is not simply the
      complement of the fg mask.
    '''

    def __init__(self, padding, unpadded_rectangle, image, foreground_mask, background_mask, camera_matrix,
                 player_candidates, ball_trajectory_candidates = []):
        '''
        padding - an integer
        unpadded_rectangle - a Rectangle object
        image - a numpy array with shape (n,m,3) and type np.uint8
        foreground_mask, background_mask - a numpy array with shape (n,m) and type np.bool
        camera_matrix - a CameraMatrix object
        player_candidates - a list of PlayerCandidate objects
        ball_trajectory_candidates - a list of BallTrajectoryCandidate objects
        '''

        super(PaddedFrameData, self).__init__(image, foreground_mask, camera_matrix, player_candidates,
                                              ball_trajectory_candidates)

        self._padding = padding
        self._unpadded_rectangle = unpadded_rectangle
        self._background_mask = background_mask

    @classmethod
    def from_frame_data(cls, frame_data, padding):
        '''
        frame_data - a FrameData object
        padding - an integer

        Return a PaddedFrameData object which is obtained by padding the image and masks in the given fd by the specified
        padding.
        '''

        padded_image = np.pad(frame_data.get_image(), pad_width=((padding,padding),(padding,padding),(0,0)), mode='constant')
        padded_foreground_mask = np.pad(frame_data.get_foreground_mask(), padding, 'constant')
        padded_background_mask = np.pad((1 - frame_data.get_foreground_mask()), padding, 'constant')

        #since the coordinate system of the padded image is different, we have to update the camera matrix as well
        #first create a translation vector from the original coordinate system to the new one
        original_to_padded = np.array([padding, padding], np.float32)

        #use this to build a homography from the previous coordinate system to the new one.
        H = planar_geometry.translation_matrix(original_to_padded)

        #to get the new camera matrix, compose the old matrix C with H to get: H*C
        C = frame_data.get_camera_matrix().get_numpy_matrix()
        new_C = np.matmul(H,C)
        new_camera_matrix = CameraMatrix(new_C)

        #use the homography H to create a new list of player candidates whose contours are tranformed by H
        #the absolute positions are the same as in the original player candidates
        new_player_candidates = [PlayerCandidate(pc.contour.get_transformed_contour(H), pc.absolute_position, pc.team_label)
                                 for pc in frame_data.get_player_candidates()]
        
        #also update the images in the contours of the new player candidates
        for pc in new_player_candidates:
            pc.contour.set_image(padded_image)

        #create a new list of ball trajectory candidates whose
        new_ball_trajectory_candidates = [btc.apply_image_homography(H)
                                          for btc in frame_data.get_ball_trajectory_candidates()]
        
        #finally, we construct the rectangle around the unpadded portion
        top_left = np.array([padding, padding], np.int32)
        bottom_right = np.array([padded_image.shape[1], padded_image.shape[0]], np.int32) - top_left
        unpadded_rectangle = Rectangle(top_left, bottom_right)

        return cls(padding, unpadded_rectangle,
                   padded_image, padded_foreground_mask, padded_background_mask, new_camera_matrix,
                   new_player_candidates, new_ball_trajectory_candidates)

    def get_unpadded_rectangle(self):
        return self._unpadded_rectangle

    def get_background_mask(self):
        return self._background_mask

    def update_from_frame_data(self, frame_data):
        '''
        frame_data - a FrameData object

        use self._padding to create a PaddedFrameData object out of the given FrameData object.
        Then update the values of this object to those of this new PaddedFrameData object.
        '''

        new_padded_frame_data = self.from_frame_data(frame_data, self._padding)

        self._image              = new_padded_frame_data._image
        self._foreground_mask    = new_padded_frame_data._foreground_mask
        self._camera_matrix      = new_padded_frame_data._camera_matrix
        self._player_candidates  = new_padded_frame_data._player_candidates
        self._unpadded_rectangle = new_padded_frame_data._unpadded_rectangle
        self._background_mask    = new_padded_frame_data._background_mask

        return
