import numpy as np
from .particle_filter import ParticleFilter
from .local_ball_data import LocalBallData
from .expected_ball_data import ExpectedBallData
from ...auxillary import np_operations

class BallParticleFilter(ParticleFilter):
    '''
    This particle filter keeps track of a list of ball particles.
    It implements methods for computing the average position and velocity of the balls.

    IMPORTANT: After updating the filter, it is possible that the filter has become degenerate. The user should check if
               this is the case using the _is_degenerate method before trying to compute probabilities.
    '''

    DELTA_T = 1.0 / 25.0  #this is the number of seconds between consecutive frames
    POSITION_STDEV = 0.5  #After updating the position, we add a gaussian random variable with this standard deviation
    VELOCITY_STDEV = 2.0 #After updating the velocity, we add a gaussian random variable with this standard deviation

    DRAG_COEFF_YARDS_INV = 0.015 #alpha is usually 0.015m^(-1)
    DRAG_COEFF_ON_GROUND_YARDS_INV = 0.03
    GRAVITY_COEFF_YARDS = 9.83         #g as units m/s^2 so we multiple by the scale

    POSITION_INDICES = (0,3)
    VELOCITY_INDICES = (3,6)

    MAX_SPEED = 500.0
                                     
    def __init__(self, ball_particles, ball_evaluator, soccer_field, frame_data, local_ball_data, on_ground=False):
        '''
        ball_particles - a numpy array with shape (num particles, 6). Each particle is stored as a length 6 vector
                           [x,y,z,vx,xy,vz] where (x,y,z) is the position and (vx,vy,vx) is the velocity.
        local_ball_evaluator - a LocalBallEvaluator object. This is used to compute the probabilities of the particles.
        soccer_field - a SoccerFieldGeometry object
        frame_data - a FrameData object containing information about the current frame.
        local_ball_data - a LocalBallData object which contains the local data associated to the particles.
        on_ground - if this is true then we assume the ball is rolling on the ground as opposed to flying through the air.

        The local ball data is allowed to be None since it is built whenever the filter is updated.
        '''

        super(BallParticleFilter, self).__init__(ball_particles)

        self._local_ball_data = local_ball_data
        self._ball_evaluator = ball_evaluator
        self._frame_data = frame_data
        self._soccer_field = soccer_field
        self._on_ground = on_ground
        
        self._drag_coeff = self.DRAG_COEFF_YARDS_INV * (1.0 / self._soccer_field.get_yards_to_world_scale())

        if on_ground:
            self._gravity_term = np.zeros(3, dtype=np.float64)
            self._drag_coeff = self.DRAG_COEFF_ON_GROUND_YARDS_INV * (1.0 / self._soccer_field.get_yards_to_world_scale())
            
        else:
            gravity_coeff = self.GRAVITY_COEFF_YARDS * self._soccer_field.get_yards_to_world_scale()
            self._gravity_term = np.array([0,0,-gravity_coeff], np.float64)
            self._drag_coeff = self.DRAG_COEFF_YARDS_INV * (1.0 / self._soccer_field.get_yards_to_world_scale())
            
    @classmethod
    def from_initial_state(cls, position, velocity, velocity_stdev, num_particles, ball_evaluator, soccer_field, frame_data):
        '''
        position - a numpy array with length 3.
        velocity_stdev - a float
        num_particles - an integer.
        ball_evaluator - a BallEvaluator object
        soccer_field - a SoccerFieldGeometry object
        frame_data - a PaddedFrameData object

        Return a BallParticleFilter with num_particles particles, whose positions are initialized to the given position,
        and whose velocities are intialized with the given standard deviation.
        '''
        ball_particles = np.zeros((num_particles, 6), np.float32)
        ball_particles[:,cls.POSITION_INDICES[0]:cls.POSITION_INDICES[1]] = position

        velocity_noise = (velocity_stdev * np.random.normal(size=(num_particles, 3)))
        ball_particles[:,cls.VELOCITY_INDICES[0]:cls.VELOCITY_INDICES[1]] = velocity.reshape((1,3)) + velocity_noise
        
        positions = ball_particles[:,cls.POSITION_INDICES[0]:cls.POSITION_INDICES[1]]
        local_ball_data = LocalBallData.from_frame_and_positions(soccer_field, frame_data, ball_evaluator, positions)
        
        return cls(ball_particles, ball_evaluator, soccer_field, frame_data, local_ball_data)

    @classmethod
    def from_initial_state_on_ground(cls, field_position, field_velocity, velocity_stdev, num_particles, ball_evaluator,
                                     soccer_field, frame_data):
        '''
        field_position - a numpy array with length 2.
        field_velocity - a numpy array with length 2
        velocity_stdev - a float
        num_particles - an integer.
        ball_evaluator - a BallEvaluator object
        soccer_field - a SoccerFieldGeometry object
        frame_data - a PaddedFrameData object

        Return a BallParticleFilter with num_particles particles, whose positions are initialized to the given field position,
        with a height equal to the radius of the ball. the velocities are intialized with the given standard deviation in the 
        x-y plane and 0 in the z direction.

        This particle filter tracks a ball rolling on the ground.
        '''
        ball_particles = np.zeros((num_particles, 6), np.float32)
        ball_particles[:,cls.POSITION_INDICES[0]:cls.POSITION_INDICES[1]-1] = field_position
        ball_particles[:,cls.POSITION_INDICES[1]-1] = soccer_field.get_soccer_ball_radius()

        print 'set heights to: ', soccer_field.get_soccer_ball_radius()
        print ball_particles[0]
        
        #since the ball is on the ground, then there is only velocity in the x-y axis
        velocity_noise = (velocity_stdev * np.random.normal(size=(num_particles, 2)))
        ball_particles[:,cls.VELOCITY_INDICES[0]:cls.VELOCITY_INDICES[1]-1] = field_velocity.reshape((1,2)) + velocity_noise
        
        positions = ball_particles[:,cls.POSITION_INDICES[0]:cls.POSITION_INDICES[1]]
        local_ball_data = LocalBallData.from_frame_and_positions(soccer_field, frame_data, ball_evaluator, positions)
        
        return cls(ball_particles, ball_evaluator, soccer_field, frame_data, local_ball_data, on_ground=True)

    def _position_slice(self):
        '''
        Return a numpy array with shape (num particles, 2) which is a view of the positions in the particles.
        '''

        return self._particles[:,self.POSITION_INDICES[0]:self.POSITION_INDICES[1]]

    def _velocity_slice(self):
        '''
        Return a numpy array with shape (num particles, 2) which is a view of the positions in the particles.
        '''

        return self._particles[:,self.VELOCITY_INDICES[0]:self.VELOCITY_INDICES[1]]

    def reset(self, position):
        '''
        Reset the particle filter such that all particles have a the given position and zero velocity.
        '''
        self._position_slice()[...]        = position
        self._velocity_slice()[...]       *= 0.0
        self._acceleration_slice()[...]   *= 0.0

        #the local particle data is now incorrect so we update it to the new position
        #self._update_local_player_data()
        self._local_player_data = None
        
        #also reset the weights to uniform weights
        self._set_uniform_weights()
        
        return
        
    def get_expected_position(self):
        '''
        Return the weighted expected position of the player particles.
        '''
        weighted_positions = self._particle_weights[:,np.newaxis] * self._position_slice()
        return weighted_positions.sum(axis=0)

    def get_expected_state_vector(self):
        '''
        return the expected position of the player
        '''
        return self.get_expected_position()
    
    def get_position_variance(self):
        '''
        Return the weighted variance of the positions of the particles.        
        '''
        expected_position = self.get_expected_position()
        position_deviations = self._particle_weights[:,np.newaxis] * (self._position_slice() - expected_position)**2
        return position_deviations.sum(axis=0)

    def get_position_entropy(self):
        '''
        Return the entropy of the positions in each of the coordinates.
        '''
        positive_weights = self._particle_weights[self._particle_weights > 0]
        entropy = -(positive_weights * np.log(positive_weights)).sum()
        return entropy
    
    def get_expected_velocity(self):
        '''
        Return the weighted expected velocity of the particles.
        '''
        weighted_velocities = self._particle_weights[:,np.newaxis] * self._velocity_slice()
        return weighted_velocities.sum(axis=0)

    def get_velocity_variance(self):
        '''
        Return the weighted variance of the positions of the particles.        
        '''
        expected_velocity = self.get_expected_velocity()
        player_deviations = self._particle_weights[:,np.newaxis] * (self._velocity_slice() - expected_velocity)**2
        return player_deviations.sum(axis=0)
    
    def is_degenerate(self):
        '''
        Return true if any of the particles have degenerate local data.
        This happens if the position of the ball is off of the field, or of the camera projection of the ball
        is off of the image.
        '''
        return self._local_ball_data is None

    def _enforce_velocity_bounds(self):
        '''
        If a particle has a velocity whose norm exceeds the maximum, scale the velocity so that it's speed is within the
        allowed limits.
        '''
        velocity = self._velocity_slice()

        #make sure the velocity does not exceed the limit
        velocity_norms = np.linalg.norm(velocity, axis=1)
        speeding_indices = velocity_norms > self.MAX_SPEED

        rescaling_coefficients = self.MAX_SPEED / velocity_norms[speeding_indices]
        
        velocity[speeding_indices] *=  rescaling_coefficients[:,np.newaxis]
        
    def _update_local_ball_data(self):
        '''
        Compute the local ball data based on the current ball positions.
        '''
        positions = self._position_slice()
        self._local_ball_data = LocalBallData.from_frame_and_positions(self._soccer_field, self._frame_data,
                                                                       self._ball_evaluator, positions)
        return
        
    def update(self):
        '''
        Update the states of the particles.
        After updating the states, we set the probabilities of zero since they are not valid anymore.
        We also update the local player data.

        Important - It is up to the user to update the variables of self._frame_data.
        E.g, the user should set the image, foreground_mask and camera_matrix of self._frame_data to those of the frame
        we are updating to.
        '''
        #if self.is_degenerate():
        #    raise RuntimeError('Cannot update a degenerate filter.')
        
        positions = self._position_slice()
        velocities = self._velocity_slice()

        #if the ball is on the ground, then add noise only on the x-y axis
        if self._on_ground:
            std_shape = (self._num_particles(), 2)
            position_noise = self.POSITION_STDEV * np.random.normal(size=std_shape)
            velocity_noise = self.VELOCITY_STDEV * np.random.normal(size=(self._num_particles(),2))

            position_noise = np_operations.add_constant_column_to_right(position_noise, 0)
            velocity_noise = np_operations.add_constant_column_to_right(velocity_noise, 0)

        else:
            std_shape = (self._num_particles(), 3)
            position_noise = self.POSITION_STDEV * np.random.normal(size=std_shape)
            velocity_noise = self.VELOCITY_STDEV * np.random.normal(size=std_shape)
        
        positions += (self.DELTA_T * velocities) + position_noise
            
        drag_term = -self._drag_coeff * np.linalg.norm(velocities, axis=1).reshape((-1,1)) * velocities
        velocities += (self.DELTA_T * (drag_term + self._gravity_term)) + velocity_noise

        #make sure that the velocity and acceleration are not too large
        self._enforce_velocity_bounds()
        
        #recalculate the local player data
        self._update_local_ball_data()
        
        #reset the probabilities
        self._set_probabilities_to_zero()

    def compute_particle_probabilities(self):
        '''
        Compute the probabilities of the particles.

        We feed _local_player_data into  _local_ball_evaluator to compute the probabilities of the particles.
        '''

        if self.is_degenerate():
            raise RuntimeError('Cannot compute probabilities for a degenerate filter.')
        
        self._particle_log_probabilities[...] = self._local_ball_data.compute_log_probabilities()

        #now turn the log probabilities into probabilities
        self.convert_log_probability_to_probability()

        return

    def resample(self):
        '''
        This does the same thing as ParticleFilter.resample.
        The only difference is that after resampling it erases the local player data since it no longer reflects the particles
        '''
        #first call the parents resample method
        super(BallParticleFilter, self).resample()

        #then erase the local player data
        self._local_ball_data = None

        return
    
    def compute_expected_ball_data(self):
        '''
        return an ExpectedBallData object which records the expected values of the filter, together with their variance.
        '''
        expected_position = self.get_expected_position()
        position_variance = self.get_position_variance()
        ball_evaluation = self.evaluate_expected_position()

        print 'building expected ball data from position: ', expected_position
        print 'and evaluation: ', ball_evaluation
        ebd = ExpectedBallData.from_frame_data_and_position(self._soccer_field, self._frame_data,
                                                            expected_position, position_variance, ball_evaluation)

        return ebd        

    def evaluate_expected_position(self):
        '''
        return a float which indicates how well the expected position matches the image.
        The lower the number, the better the approximation.
        '''
        edge_mask = self._frame_data.get_edge_mask()
        camera_matrix = self._frame_data.get_camera_matrix()
        ball_radius = self._soccer_field.get_soccer_ball_radius()
        ball_position = self.get_expected_position()

        evaluation = self._ball_evaluator.evaluate_ball_position(edge_mask, camera_matrix, ball_radius, ball_position)

        return evaluation
