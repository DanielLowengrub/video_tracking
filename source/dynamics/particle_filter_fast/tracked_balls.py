import collections
import numpy as np
import itertools
from ...auxillary.interval import Interval

'''
A BallTrajectory stores the data related to the trajectory of a ball.
ball_trajectory_id - the id of the trajectory. This is the same as the id of the BallTrajectoryCandidate that was used to
                     initialize the trajectory.
start_frame - the first frame that the ball appears in
positions - a numpy array with shape (num frames, 3). It stores the positions of the ball.
image_positions - a numpy array with shape (num frames, 2). It stores the positions of the ball on the image.
on_ground - this specifies if the ball is rolling on the ground or moving through the air.
'''
BallTrajectory = collections.namedtuple('BallTrajectory', ['ball_trajectory_id', 'start_frame',
                                                           'positions', 'image_positions', 'on_ground'])

class TrackedBalls(object):
    '''
    This class records the output of the BallTracker.
    '''

    def __init__(self, ball_trajectory_dict):
        '''
        ball_trajectory_dict - a dictionary with items (ball trajectory id, BallTrajectory object)
        '''

        self._ball_trajectory_dict = ball_trajectory_dict

    def _get_trajectories_in_frame(self, frame_index, split_by_type=False):
        '''
        frame_index - an integer
        split_by_type - a boolean

        return a dictionary which is the subset of ball_trajectory_dict that consists of trajectories which contain the
        given frame index.
        if split_by_type is true then we return 3 dictionaries: start_trajectories, middle_trajectories, end_trajectories
        start_trajectories - the trajectories that start in this frame
        middle_trajectories - the trajectories that contain this frame
        end_trajectories - the trajectories that end in this frame
        '''
        start_trajectories = dict()
        middle_trajectories = dict()
        end_trajectories = dict()
        
        for k,bt in self._ball_trajectory_dict.iteritems():
            #check if the frame index is between the first and last frames of the trajectory
            end_frame = bt.start_frame + len(bt.positions) - 1
            
            if bt.start_frame == frame_index:
                start_trajectories[k] = bt
                
            elif bt.start_frame < frame_index < end_frame:
                middle_trajectories[k] = bt

            elif frame_index == end_frame:
                end_trajectories[k] = bt
                
        if split_by_type:
            return start_trajectories, middle_trajectories, end_trajectories

        #if we do not split by type then merge the dictionaries and return the union
        #to do this, add the contents of the middle and end dictionaries to the start dictionary
        start_trajectories.update(middle_trajectories)
        start_trajectories.update(end_trajectories)
        return start_trajectories
    
    def get_ball_trajectory_dict(self):
        '''
        return a dictionary with items (ball trajectory id, BallTrajectory object)
        '''
        return self._ball_trajectory_dict

    def add_trajectory(self, ball_trajectory_id, start_frame, positions, image_positions, on_ground):
        ball_trajectory = BallTrajectory(ball_trajectory_id, start_frame, positions, image_positions, on_ground)
        self._ball_trajectory_dict[ball_trajectory_id] = ball_trajectory
        return
    
    def has_ball_trajectory(self, ball_trajectory_id):
        return ball_trajectory_id in self._ball_trajectory_dict

    def get_ball_trajectory(self, ball_trajectory_id):
        return self._ball_trajectory_dict[ball_trajectory_id]

    def get_sorted_ball_trajectories(self):
        '''
        return a list of BallTrajectory objects, sorted by the start_frame from small to large
        '''
        return sorted(self._ball_trajectory_dict.values(), key=lambda x: x.start_frame)

    def remove_ball_trajectories(self, ball_trajectory_ids):
        '''
        ball_trajectory_ids - a list of ball trajectory ids
        remove those balls from the dictionary of tracked balls
        '''
        for bt_id in ball_trajectory_ids:
            del self._ball_trajectory_dict[bt_id]

        return
    
    def get_reversed_tracked_balls(self, last_frame_index):
        '''
        last_frame_index - an integer
        Return a new TrackedBalls object in which the positions in the trajectories are reversed. If a trajectory in this
        object ends at frame end_frame, then the reversed trajectory in the new TrackedBall object will start from frame
        (last_frame_index - end_frame) and the array of positions will be reversed.


        For example, suppose there is only one ball trajectory with start_frame=3 and positions [p0,p1]. Suppose
        last_frame_index = 9.
        Then, since the end frame of that trajectory is 4,
        the reversed TrackedBalls object would have a trajectory with start_frame=(9-4 = 5) and positions [p1,p0]
        '''

        reversed_ball_trajectory_dict = dict()
        for ball_id,ball_trajectory in self._ball_trajectory_dict.iteritems():
            print 'reversing trajectory:'
            print ball_trajectory
            
            #get the start position of the reversed trajectory
            end_frame = ball_trajectory.start_frame + len(ball_trajectory.positions) - 1
            new_start_frame = last_frame_index - end_frame
            # print 'new start frame: ', new_start_frame
            
            #the new positions are the same as the old ones but in the reverse order
            new_positions = ball_trajectory.positions[::-1]
            new_image_positions = ball_trajectory.image_positions[::-1]
            new_on_ground = ball_trajectory.on_ground
            
            # print 'new positions: '
            # print new_positions
            
            new_ball_trajectory = BallTrajectory(ball_id, new_start_frame, new_positions, new_image_positions, new_on_ground)

            print 'reversed trajectory:'
            print new_ball_trajectory
            reversed_ball_trajectory_dict[ball_id] = new_ball_trajectory

        return TrackedBalls(reversed_ball_trajectory_dict)
    
    def get_ball_positions_in_frame(self, frame_index, split_by_type=False):
        '''
        frame_index - an integer
        split_by_type - a boolean
        return - a tuple (abs positions in frame, img positions in frame)
        abs_positions_in_frame - a numpy array with shape (num balls in this frame, 3) 
        img_positions_in_frame - a numpy array with shape (num balls in this frame, 2) 

        If split_by_type is true, then return three tuples: positions_start, positions_middle, positions_end.
        positions_start - abs positions and image positions of balls that are the first ball in their trajectory
        positions_middle - abs positions and image positions of balls that are in the middle of their trajectory
        positions_end - abs positions and image positions of balls that are the last ball in their trajectory
        '''
        #this is a tuple of three dictionaries: (start_trajectories, middle_trajectories, end_trajectories)
        trajectory_dicts = self._get_trajectories_in_frame(frame_index, True)

        #this is a list of three lists: start_positions, middle_positions, end_positions
        abs_positions = [[bt.positions[frame_index-bt.start_frame] for bt in trajectory_dict.itervalues()]
                         for trajectory_dict in trajectory_dicts]

        #this is also a list of three lists: start_positions, middle_positions, end_positions
        image_positions = [[bt.image_positions[frame_index-bt.start_frame] for bt in trajectory_dict.itervalues()]
                           for trajectory_dict in trajectory_dicts]

        #turn the lists of positions into np arrays
        for i in range(len(abs_positions)):
            abs_positions[i] = np.empty((0,3)) if len(abs_positions[i]) == 0 else np.vstack(abs_positions[i])

        for i in range(len(image_positions)):
            image_positions[i] = np.empty((0,2)) if len(image_positions[i]) == 0 else np.vstack(image_positions[i])

        #if we are splitting the positions by type, then group the abs and image positions into pairs:
        #((start abs positions, start image positions), (middle abs positions, middle image positions),
        #(end abs positions, end middle positions))
        if split_by_type:
            return tuple(zip(abs_positions, image_positions))

        #if we are not supposed to split the positions by type then merge all of the abs positions into a single array.
        #same for the image positions
        abs_positions = np.vstack(abs_positions)
        image_positions = np.vstack(image_positions)

        return abs_positions, image_positions

    def get_enumerated_image_positions(self, ball_trajectory_id):
        '''
        ball_trajectory_id - the id of a ball trajectory
        return - a numpy array with shape (num frames, 3) and type np.int32. Each row is of the form:
        [frame index, image position x, image position y]
        it represents the image position at the given frame index of the ball trajectory with id ball_trajectory_id
        
        The frame indices are "absolute", in the sense that the index of the first ball is the start frame of the trajectory,
        not 0.
        '''
        bt = self._ball_trajectory_dict[ball_trajectory_id]
        frame_indices = np.arange(bt.start_frame, bt.start_frame + len(bt.image_positions))
        enumerated_image_positions = np.hstack([frame_indices.reshape((-1,1)), bt.image_positions]).astype(np.int32)

        return enumerated_image_positions

    def remove_degenerate_trajectories(self, min_trajectory_distance):
        '''
        min_trajectory_distance - a float

        remove trajectories that covered less than this distance.
        '''
        for bt_key,bt in self._ball_trajectory_dict.items():
            distance = np.linalg.norm(bt.positions[0] - bt.positions[-1])
            print 'trajectory %d has distance: %f.' % (bt_key, distance)
            if distance < min_trajectory_distance:
                print ' removing trajectory.'
                del self._ball_trajectory_dict[bt_key]

        return
    
    @classmethod
    def crop_trajectories(cls, ball_trajectory_0, ball_trajectory_1, frame_index, overlap_at_end_points=True):
        '''
        ball_trajectory_i - a BallTrajectory object
        backward_trajectory - a BallTrajectory object
        frame_index - an integer
        overlap_at_end_points - if this is True, then both of the cropped trajectories contain the given frame index. If
           it is False, then only the second one does
        return a pair (cropped_ball_trajectory_0, cropped_ball_trajectory_1).
        The cropped ball trajectory 0 contains the positions of trajectory 0 up to (and including) the frame index.
        The cropped backward trajectory contains the positions of the backward trajectory starting from the frame index.
        '''

        print 'cropping the following trajectories at index %d:' % frame_index
        print 'bt 0: ', ball_trajectory_0
        print 'bt 1: ', ball_trajectory_1
        
        cropped_bt_0_id              = ball_trajectory_0.ball_trajectory_id
        cropped_bt_0_start_frame     = ball_trajectory_0.start_frame
        relative_end_index           = frame_index - cropped_bt_0_start_frame
        
        if overlap_at_end_points:
            relative_end_index += 1
            
        cropped_bt_0_positions       = ball_trajectory_0.positions[:relative_end_index]
        cropped_bt_0_image_positions = ball_trajectory_0.image_positions[:relative_end_index]
        cropped_bt_0_on_ground       = ball_trajectory_0.on_ground

        cropped_bt_0 = BallTrajectory(cropped_bt_0_id, cropped_bt_0_start_frame, cropped_bt_0_positions,
                                      cropped_bt_0_image_positions, cropped_bt_0_on_ground)

        cropped_bt_1_id              = ball_trajectory_1.ball_trajectory_id
        cropped_bt_1_start_frame     = frame_index
        relative_start_index         = frame_index - ball_trajectory_1.start_frame
        cropped_bt_1_positions       = ball_trajectory_1.positions[relative_start_index:]
        cropped_bt_1_image_positions = ball_trajectory_1.image_positions[relative_start_index:]
        cropped_bt_1_on_ground       = ball_trajectory_1.on_ground

        cropped_bt_1 = BallTrajectory(cropped_bt_1_id, cropped_bt_1_start_frame, cropped_bt_1_positions,
                                      cropped_bt_1_image_positions, cropped_bt_1_on_ground)

        print 'the cropped trajectories are:'
        print 'cropped bt 0: ', cropped_bt_0
        print 'cropped bt 1: ', cropped_bt_1
        
        return cropped_bt_0, cropped_bt_1

    @classmethod
    def _find_first_frame_with_match(cls, trajectories, trajectory_match_thres):
        '''
        trajectories - a pair of BallTrajectory objects.

        return - an integer. it is the first frame for the the distance between the image positions of the trajectories is less
        than th trajectory match threshold

        If they do not match in *any* frame, return None.
        '''
        print 'finding first frame with match...'
        
        #check if the two trajectories overlap.
        #To do this, first find the common interval on which they are defined.
        frame_intervals = [Interval(t.start_frame, t.start_frame + len(t.positions)) for t in trajectories]

        print 'frame intervals: ', frame_intervals
        
        #if the frame intervals do not overlap, then there are no matches so return None
        if not frame_intervals[0].has_overlap(frame_intervals[1]):
            print 'the intervals do not overlap.'
            return None

        common_interval = frame_intervals[0].get_overlap(frame_intervals[1])

        #get the image positions of each of the trajectories in the common frame interval
        overlap_image_positions = [fi.get_relative_slice(t.image_positions, common_interval)
                                   for fi,t in zip(frame_intervals, trajectories)]

        print 'overlap image positions:'
        print overlap_image_positions
        
        #find the frame in which the positions are closest
        image_position_distances = np.linalg.norm(overlap_image_positions[0] - overlap_image_positions[1], axis=1)
        frame_has_match = image_position_distances < trajectory_match_thres

        print 'frame has match: ', frame_has_match
        
        #if the trajectories do not match anywhere
        if not np.any(frame_has_match):
            print 'there were no matching frames'
            return None

        #find the first frame in the common interval that has a match, and convert it from the common interval coordinates to
        #absolute coordinates
        first_frame_with_match = common_interval.relative_to_absolute_coords(np.argmax(frame_has_match))
        print 'the first frame with a match is: ', first_frame_with_match
        
        return first_frame_with_match
    
    @classmethod
    def _crop_tracked_balls(cls, forward_tracked_balls, backward_tracked_balls, trajectory_match_thresh):
        '''
        forward_tracked_balls - a TrackedBalls object
        backward_tracked_balls - a TrackedBalls object

        For each pair of a forward trajectory and a backwards trajectory (ft, bt), check if their image points ever overlap
        (i.e, the distance between them is less than the trajectory_match_thresh).
        If they do, cut off the part of ft that comes after the intersection, and the part of bt that comes before.
        '''
        print 'cropping tracked balls...'
        ft_dict = forward_tracked_balls.get_ball_trajectory_dict()
        bt_dict = backward_tracked_balls.get_ball_trajectory_dict()
        
        #make copies of the ball trajectory dictionaires
        cropped_ft_dict = dict()
        cropped_bt_dict = dict()

        #iterate over pairs of a forward trajectory and a backward trajectory
        for (ft_id, ft), (bt_id, bt) in itertools.product(ft_dict.iteritems(), bt_dict.iteritems()):

            print 'checking for a match with:'
            print '   ft: ', ft
            print '   bt: ', bt

            #if both trajectories have the same id, then they are not used to crop one another
            if ft_id == bt_id:
                continue
            
            #if one of these trajectories has already been cropped, skip this pair
            if (ft_id in cropped_ft_dict) or (bt_id in cropped_bt_dict):
                continue

            first_frame_with_match = cls._find_first_frame_with_match((ft,bt), trajectory_match_thresh)

            #if these forward and backward trajectories do not match, go to the next pair
            if first_frame_with_match is None:
                continue

            print '   found a match. adding the pair to the cropped dictionaries.'
            #if there is a match, cut off the forward trajectory after the match and the backward trajectory before the match
            cropped_ft, cropped_bt = cls.crop_trajectories(ft, bt, first_frame_with_match)
            cropped_ft_dict[ft_id] = cropped_ft
            cropped_bt_dict[bt_id] = cropped_bt

        print 'finished matching trajectories.'
        print 'cropped ft dict: '
        print cropped_ft_dict
        print 'cropped bt dict:'
        print cropped_bt_dict
        
        #move all of the forward trajectories that have not been matched with any backward trajectory to the cropped_ft_dict
        for ft_id, ft in ft_dict.iteritems():
            cropped_ft_dict.setdefault(ft_id, ft)

        #same for the backward trajectories
        for bt_id, bt in bt_dict.iteritems():
            cropped_bt_dict.setdefault(bt_id, bt)

        #make new TrackedBalls object out of the cropped trajectory dictionaries
        return TrackedBalls(cropped_ft_dict), TrackedBalls(cropped_bt_dict)
                            
            
    @classmethod
    def join_forward_and_backward(cls, forward_tracked_balls, backward_tracked_balls, trajectory_match_thresh):
        '''
        forward_tracked_balls - a TrackedBalls object
        backward_tracked_balls - a TrackedBalls object
        trajectory_match_thresh - two trajectories are said to match if there is a frame in which the distance
                                  between the trajectory positions is less than this number.

        return a TrackedBalls object.

        We assume that the set of ball ids of the trajectories in both the forward and backward TrackedBalls objects are equal.
        The output is obtained as follows:
        Let ball_id be the id of trajectory ft in forward_tracked_balls and trajectory bt in backward_tracked_balls.
        In particular, this means that the last position of bt is equal to the first position of ft.
        the output will have a trajectory with id ball_id which is the concatenation of bt and ft.
        '''
        print 'joining forward and backward tracked balls...'
        print 'initial forward ball trajectories:'
        print forward_tracked_balls._ball_trajectory_dict
        print 'initial backward ball trajectories:'
        print backward_tracked_balls._ball_trajectory_dict
        
        joined_bt_dict = dict()

        #crop the forward and backward ball trajectories. This is to make sure that there isn't a forward trajectory that
        #overlaps with a backward one
        forward_tracked_balls, backward_tracked_balls = cls._crop_tracked_balls(forward_tracked_balls, backward_tracked_balls,
                                                                                trajectory_match_thresh)
        
        for bt_id, forward_trajectory in forward_tracked_balls.get_ball_trajectory_dict().iteritems():

            #if there is no backward trajectory to join, set the joined trajectory to the forward trajectory
            if bt_id not in backward_tracked_balls.get_ball_trajectory_dict():
                joined_bt_dict[bt_id] = forward_trajectory
                continue
            
            backward_trajectory = backward_tracked_balls.get_ball_trajectory_dict()[bt_id]

            print 'joining:'
            print 'forward trajectory: ', forward_trajectory
            print 'backward trajectory: ', backward_trajectory
            
            #concatenate the forward and backward trajectories
            start_frame = backward_trajectory.start_frame
            positions = np.vstack([backward_trajectory.positions, forward_trajectory.positions[1:]])
            image_positions = np.vstack([backward_trajectory.image_positions, forward_trajectory.image_positions[1:]])
            on_ground = forward_trajectory.on_ground

            joined_bt_dict[bt_id] = BallTrajectory(bt_id, start_frame, positions, image_positions, on_ground)

        
        return cls(joined_bt_dict)
