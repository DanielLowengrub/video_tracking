import numpy as np
from .particle_filter import ParticleFilter
from .local_player_data_small import LocalPlayerDataSmall
from .expected_player_data import ExpectedPlayerData

class PlayerParticleFilter(ParticleFilter):
    '''
    This particle filter keeps track of a list of player particles.
    It implements methods for computing the average position, velocity and acceleration of the players.
    In addition, it implements a method for computing the joint probability of multiple player particles.

    IMPORTANT: After updating the filter, it is possible that the filter has become degenerate. The user should check if
               this is the case using the _is_degenerate method before trying to compute probabilities.
    '''

    DELTA_T = 1.0 / 25.0    #this is the number of seconds between consecutive frames
    POSITION_STDEV = 0.5    #After updating the position, we add a gaussian random variable with this standard deviation
    VELOCITY_STDEV = 3.0 #used to be 10.0  #After updating the velocity, we add a gaussian random variable with this standard deviation
    ACCELERATION_STDEV = 1.0#30.0 #same for acceleration

    POSITION_INDICES = (0,2)
    VELOCITY_INDICES = (2,4)
    ACCELERATION_INDICES = (4,6)

    MAX_SPEED = 55.0 #EXPERIMENTING (used to be 50.0)
    MAX_ACCELERATION = 250.0

    MAX_DISTANCE_FROM_CANDIDATE = 10 #we say that a player candidate matches a particle filter if distance between
                                     #the average filter position and the candidate position is less than this value
                                     
    def __init__(self, player_particles, local_data_evaluator, field_rectangle, frame_data, local_player_data):
        '''
        player_particles - a numpy array with shape (num particles, 6). Each particle is stored as a length 6 vector
                           [x,y,vx,vy,ax,ay] where (x,y) is the position, (vx,vy) velocity and (ax,ay) acceleration.
        local_data_evaluator - a LocalDataEvaluator object. This is used to compute the probabilities of the particles.
        field_rectangle - a Rectangle object containing the field in absolute coordinates.
        frame_data - a FrameData object containing information about the current frame.

        local_player_data - a MultiPlayerData object which contains the local data associated to the particles.

        The local player data is allowed to be None since it is built whenever the filter is updated.
        '''

        super(PlayerParticleFilter, self).__init__(player_particles)

        self._local_player_data = local_player_data
        self._local_data_evaluator = local_data_evaluator
        self._frame_data = frame_data
        self._field_rectangle = field_rectangle

    @classmethod
    def build_default_filter(cls, num_particles, local_data_evaluator, field_rectangle, frame_data):
        '''
        num_particles - an integer.
        local_data_evaluator - a LocalDataEvaluator object
        field_rectangle - a Rectangle object
        frame_data - a PaddedFrameData object

        Return a PlayerParticleFilter whose particles are set to default values.
        '''
        player_particles = np.zeros((num_particles, 6), np.float64)
        local_player_data = None
        
        return cls(player_particles, local_data_evaluator, field_rectangle, frame_data, local_player_data)

    @classmethod
    def from_initial_position(cls, position, num_particles, local_data_evaluator, field_rectangle, frame_data):
        '''
        position - a numpy array with length 2. 
        num_particles - an integer.
        local_data_evaluator - a LocalDataEvaluator object
        field_rectangle - a Rectangle object
        frame_data - a PaddedFrameData object

        Return a PlayerParticleFilter with num_particles particles, whose positions are initialized to the given position.
        '''
        player_particles = np.zeros((num_particles, 6), np.float64)
        player_particles[:,cls.POSITION_INDICES[0]:cls.POSITION_INDICES[1]] = position
        positions = player_particles[:,cls.POSITION_INDICES[0]:cls.POSITION_INDICES[1]]
        local_player_data = LocalPlayerDataSmall.from_frame_and_positions(field_rectangle, frame_data, positions)
        
        return cls(player_particles, local_data_evaluator, field_rectangle, frame_data, local_player_data)

    def _position_slice(self):
        '''
        Return a numpy array with shape (num particles, 2) which is a view of the positions in the particles.
        '''

        return self._particles[:,self.POSITION_INDICES[0]:self.POSITION_INDICES[1]]

    def _velocity_slice(self):
        '''
        Return a numpy array with shape (num particles, 2) which is a view of the positions in the particles.
        '''

        return self._particles[:,self.VELOCITY_INDICES[0]:self.VELOCITY_INDICES[1]]

    def _acceleration_slice(self):
        '''
        Return a numpy array with shape (num particles, 2) which is a view of the positions in the particles.
        '''

        return self._particles[:, self.ACCELERATION_INDICES[0]:self.ACCELERATION_INDICES[1]]
    
    def reset(self, position):
        '''
        Reset the particle filter such that all particles have a the given position, and zero velocity and acceleration
        '''
        self._position_slice()[...]        = position
        self._velocity_slice()[...]       *= 0.0
        self._acceleration_slice()[...]   *= 0.0

        #the local particle data is now incorrect so we update it to the new position
        #self._update_local_player_data()
        self._local_player_data = None
        
        #also reset the weights to uniform weights
        self._set_uniform_weights()
        
        return
    
    # def get_tracked_player(self):
    #     '''
    #     Return the TrackedPlayer that this player filter belongs to.
    #     '''
    #     return self._tracked_player
    
    def get_expected_position(self):
        '''
        Return the weighted expected position of the player particles.
        '''

        weighted_positions = self._particle_weights[:,np.newaxis] * self._position_slice()
        return weighted_positions.sum(axis=0)

    def get_expected_state_vector(self):
        '''
        return the expected position of the player
        '''
        return self.get_expected_position()
    
    def get_position_variance(self):
        '''
        Return the weighted variance of the positions of the player particles.        
        '''
        expected_position = self.get_expected_position()
        player_deviations = self._particle_weights[:,np.newaxis] * (self._position_slice() - expected_position)**2
        return player_deviations.sum(axis=0)

    def get_position_entropy(self):
        '''
        Return the entropy of the positions in each of the coordinates.
        '''
        positive_weights = self._particle_weights[self._particle_weights > 0]
        entropy = -(positive_weights * np.log(positive_weights)).sum()
        return entropy
    
    def get_expected_velocity(self):
        '''
        Return the weighted expected velocity of the player particles.
        '''
        weighted_velocities = self._particle_weights[:,np.newaxis] * self._velocity_slice()
        return weighted_velocities.sum(axis=0)

    def get_velocity_variance(self):
        '''
        Return the weighted variance of the positions of the player particles.        
        '''
        expected_velocity = self.get_expected_velocity()
        player_deviations = self._particle_weights[:,np.newaxis] * (self._velocity_slice() - expected_velocity)**2
        return player_deviations.sum(axis=0)
    
    def get_expected_acceleration(self):
        '''
        Return the weighted expected acceleration of the player particles.        
        '''
        weighted_accelerations = self._particle_weights[:,np.newaxis] * self._acceleration_slice()
        return weighted_accelerations.sum(axis=0)

    def is_degenerate(self):
        '''
        Return true if any of the particles have degenerate local data.
        This happens if the position of the player is off of the field, or of the camera projection of the player
        is off of the image.
        '''
        return self._local_player_data is None

    def _enforce_velocity_and_acceleration_bounds(self):
        '''
        If a particle has a velocity whose norm exceeds the maximum, scale the velocity so that it's speed is within the
        allowed limits. Same for acceleration.
        '''
        #print 'enforcing velocity and acceleration bounds...'
        
        velocity = self._velocity_slice()
        acceleration = self._acceleration_slice()

        #make sure the velocity does not exceed the limit
        velocity_norms = np.linalg.norm(velocity, axis=1)
        speeding_indices = velocity_norms > self.MAX_SPEED

        rescaling_coefficients = self.MAX_SPEED / velocity_norms[speeding_indices]
        
        velocity[speeding_indices] *=  rescaling_coefficients[:,np.newaxis]
        
        #same for acceleration
        acceleration_norms = np.linalg.norm(acceleration, axis=1)
        speeding_indices = acceleration_norms > self.MAX_ACCELERATION
        #print 'acceleration too large: ', speeding_indices
        
        rescaling_coefficients = self.MAX_ACCELERATION / acceleration_norms[speeding_indices]
        acceleration[speeding_indices] *= rescaling_coefficients[:,np.newaxis]
        
    def _update_local_player_data(self):
        '''
        Compute the local player data based on the current player positions.
        '''
        #print 'updating local player data...'
        positions = self._position_slice()
        self._local_player_data = LocalPlayerDataSmall.from_frame_and_positions(self._field_rectangle, self._frame_data,
                                                                                positions)

        return
        
    def update(self):
        '''
        Update the states of the particles.
        After updating the states, we set the probabilities of zero since they are not valid anymore.
        We also update the local player data.

        Important - It is up to the user to update the variables of self._frame_data.
        E.g, the user should set the image, foreground_mask and camera_matrix of self._frame_data to those of the frame
        we are updating to.
        '''
        #if self.is_degenerate():
        #    raise RuntimeError('Cannot update a degenerate filter.')
        
        positions = self._position_slice()
        velocities = self._velocity_slice()
        accelerations = self._acceleration_slice()

        std_shape = (self._num_particles(), 2)
        
        positions += (self.DELTA_T * velocities) + (self.POSITION_STDEV * np.random.normal(size=std_shape))
        velocities += (self.DELTA_T * accelerations) + (self.VELOCITY_STDEV * np.random.normal(size=std_shape))

        #EXPERIMENTING!! checking what happens if the acceleration is kept at zero, so the only change to the velocity comes
        #from noise
        accelerations += self.ACCELERATION_STDEV * np.random.normal(size=std_shape)

        #make sure that the velocity and acceleration are not too large
        self._enforce_velocity_and_acceleration_bounds()
        
        #recalculate the local player data
        self._update_local_player_data()
        
        #reset the probabilities
        self._set_probabilities_to_zero()

    def compute_particle_probabilities(self):
        '''
        Compute the probabilities of the particles.

        We feed _local_player_data into  _local_data_evaluator to compute the probabilities of the particles.
        '''

        if self.is_degenerate():
            raise RuntimeError('Cannot compute probabilities for a degenerate filter.')
        
        self._particle_log_probabilities[...] = self._local_data_evaluator.compute_log_probabilities(self._local_player_data)

        #now turn the log probabilities into probabilities
        self.convert_log_probability_to_probability()

        return

    def resample(self):
        '''
        This does the same thing as ParticleFilter.resample.
        The only difference is that after resampling it erases the local player data since it no longer reflects the particles
        '''
        #first call the parents resample method
        super(PlayerParticleFilter, self).resample()

        #then erase the local player data
        self._local_player_data = None

        return
    
    def _compute_average_player_data(self):
        '''
        Return a LocalPlayerData object which contains a single player, whose position is the average position of this
        particle filter. However, it still has the same number of particles as this particle filter.
        '''

        positions = self._position_slice().mean(axis=0).reshape((1,2))
        number_of_particles = len(self._particles)
        
        average_player_data = LocalPlayerDataSmall.from_frame_and_positions(self._field_rectangle, self._frame_data, positions,
                                                                            number_of_particles)

        return average_player_data

    #NOTE: It is admittedly confusing to have one function named "compute_average_.." and another "compute_expected_.."
    #This should be resolved in the next rewrite.
    def compute_expected_player_data(self):
        '''
        return an ExpectedPlayerData object which records the expected values of the filter, together with their variance.
        As opposed to _compute_average_local_player_data, this does not return a LocalPlayerData object, but rather an
        ExpectedPlayerData object which is basically a summary of a particle filter + local player data.
        '''
        expected_position = self.get_expected_position()
        position_variance = self.get_position_variance()

        epd = ExpectedPlayerData.from_frame_data_and_position(self._field_rectangle, self._frame_data,
                                                              expected_position, position_variance)

        return epd
        
    def compute_joint_log_probabilities(self, other_particle_filters):
        '''
        other_particle_filters - a list of PlayerParticleFilter objects

        compute the joint probability of the particles in this filter relative to the average positions of the
        given particle filters.
        '''

        if self.is_degenerate():
            raise RuntimeError('Cannot compute probabilities for a degenerate filter.')
        
        #first construct SinglePlayerData objects corresponding to the average positions of the other particle filters
        other_player_datas = [pf._compute_average_player_data() for pf in other_particle_filters]

        #now compute the joint probability of this local player data together with the other player data objects
        all_player_datas = [self._local_player_data] + other_player_datas
        self._particle_log_probabilities[...] = self._local_data_evaluator.compute_joint_log_probabilities(all_player_datas)

        #now turn the log probabilities into probabilities
        self.convert_log_probability_to_probability()
        
        return

    # def matches_player_candidate(self, player_candidate):
    #     '''
    #     player_candidate - a PlayerCandidate object

    #     Return True if this player is a good match for the player candidate. I.e, if it is very probable that the
    #     player candidate was generated by this player.

    #     In this version we mereley check if the absolute position of the player candidate is close to the expected position
    #     of this player filter.
    #     '''

    #     candidate_position = player_candidate.absolute_position
    #     expected_position = self.get_expected_position()

    #     return np.linalg.norm(candidate_position - expected_position) < self.MAX_DISTANCE_FROM_CANDIDATE
