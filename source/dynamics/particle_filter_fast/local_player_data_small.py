import numpy as np
import cv2
import itertools
from ...auxillary.rectangle import Rectangle
from ...auxillary.rectangles import Rectangles
from ...auxillary import world_to_image_projections
from ...auxillary import planar_geometry, np_operations

class LocalPlayerDataSmall(object):
    '''
    This class keeps track of information in a frame that is specific to a given player.
    It stores an image, a list of cropped foreground masks, a camera matrix, a player mask, a list of player rectangles,
    a np array of positions and a list of obs masks.

    The reason for storing a list is that we sometimes want to simultaneously deal with many particles associated to a given
    player. Each particle has its own position and obstruction mask.

    The "number_of_particles" in a local player data object is defined to be the number of local image regions that the object
    is keeping track of.

    There are 2 possible values for the tuple  (number of players, number of particles):
    (1,N): This happens when we are storing the position of a  single player P0, but want to evaluate it relative to 
           a player P1 which has N candidate positions
    (N,N): This is the usual case, in which we are evaluating N candidates for the position of a player.

    Note:
    * The number of obstruction masks is always equal to N. All obs masks have the same shape as the player mask.
    * The number of foreground masks is always equal to the number of player positions (either 1 or N)
      all fg masks have the same shape as the player mask.
    * The number of player rectangles is always equal to the number of player positions (either 1 or N).
    '''
    
    MIN_PLAYER_MASK_AREA = 5  #If the ellipse mask corresponding to a position is less than this, the player is degenerate
    MAX_DISTANCE_FROM_FIELD = 5 #The farthest a player is allowed to stray from the soccer field.
    
    def __init__(self, frame_data, absolute_positions, player_mask, rectangles, player_to_rectangle_map,
                 rectangle_foreground_masks, rectangle_background_masks, number_of_particles):
        '''
        frame_data -  PaddedFrameData object
        absolute_positions - a numpy array with shape (n,2) and type np.float32
        player_mask - a numpy array, with shape (a,b) and type np.bool.
        rectangles - a Rectangles object with shape (a,b). It contains k rectangles. k <= n
        particle_to_rectangles_map - a numpy array with length k and with values in [0,n)
        rectangle_foreground_masks - a numpy array with shape (k,a,b)
        rectangle_background_masks - a numpy array with shape (k,a,b)

        number_of_particles - an integer. This determines how many particles are associated to this local data object.

        The allowed values for (n,num particles) are (1,N) or (N,N) for some integer N>=1.
        '''
        self._frame_data = frame_data

        self._absolute_positions = absolute_positions
        self._player_mask = player_mask
        self._rectangles = rectangles
        self._player_to_rectangle_map = player_to_rectangle_map
        self._rectangle_foreground_masks = rectangle_foreground_masks
        self._rectangle_background_masks = rectangle_background_masks
        self._number_of_particles = number_of_particles

        #these are only populated if needed. They are np arrays with shape (n, a, b)
        self._player_rectangles = None
        self._player_foreground_masks = None
        self._player_background_masks = None
        
        #obstruction_rectangles should be a Rectangles object with (num particles) rectangles.
        #since this is needed only for joint probability calculations, we only construct it if necessary
        #the i-th obstruction rectangle specifies the location of the i-th obstruction mask (below)
        self._obstruction_rectangles = None
                
        #obstruction_masks should be a numpy array with shape (num particles,)+player_mask.shape
        #since it is only needed for joint probability calculations, we initialize it to None and only construct it if
        #necessary
        self._obstruction_masks = None

        #obstacle_distances is an array with shape (num particles, num obstacles)
        #it stores the distances of each of the particles to each obstacle.
        self._obstacle_distances = None

    @classmethod
    def _build_player_mask(cls, camera_matrix, absolute_positions):
        '''
        camera_matrix - a CameraMatrix object
        absolute_positions - a numpy array with shape (N,2)

        return a numpy array with shape (a,b) and type np.bool. This represents the mask of a player standing at the average
        position of the list of absolute positions.
        '''
        average_absolute_position = absolute_positions.mean(axis=0).reshape((1,2))
        player_ellipse = world_to_image_projections.project_players_to_image(camera_matrix, average_absolute_position)[0]

        #compute the bounding box of each of the ellipse
        ellipse_region = planar_geometry.get_ellipse_region(player_ellipse)
        
        #first compute the ellipse centers. These are simply the centers of the ellipse regions.
        ellipse_center = ellipse_region.mean(axis=0)

        #determine if the value of the ellipse equation on the center is positive or negative.
        #if it is negative, we multiply the ellipse equation by -1. This ensures that the inside of the ellipse is the region
        #where the equation is positive.
        value_at_center = planar_geometry.evaluate_ellipse_on_point(player_ellipse, ellipse_center)
        if value_at_center < 0:
            player_ellipse *= -1.0

        #to build the player mask, evalute the player_ellipse at each of the pixels in the ellipse_region
        #and check which ones are positive
        ellipse_region = np.int32(ellipse_region)
        x = np.arange(ellipse_region[0,0],ellipse_region[1,0]+1, 1)
        y = np.arange(ellipse_region[0,1],ellipse_region[1,1]+1, 1)
        XX,YY = np.meshgrid(x,y)

        #print '   XX shape: ', XX.shape
        #print '   YY shape: ', YY.shape
            
        ellipse_values = planar_geometry.evaluate_ellipse_on_grid(player_ellipse, XX, YY)
        player_mask = ellipse_values >= 0

        return player_mask

    @classmethod
    def _group_by_image_position(cls, image_positions):
        '''
        image_positions - a numpy array with shape (num players, 2)

        return a tuple (image_position_representatives, particle_to_position_map)

        image_position_representatives - a numpy array with shape (number of distinct image positions, 2) and type np.int32
        particle_to_position_map - numpy array with length (num players) and type np.int32
        
        particle_to_position_map[i] = the integer 0 <= j < len(image_position_representatives) s.t
        image_positions[i] = image_position_representatives[j].
        '''
        group_representatives, group_ids = np_operations.group_by_row(image_positions)

        return group_representatives, group_ids

    @classmethod
    def _build_rectangles(cls, player_mask_shape, camera_matrix, absolute_positions):
        '''
        player_mask_shape - a length 2 tuple
        camera_matrix - a CameraMatrix
        absolute_positions - a numpy array with shape (n, 2)

        return - a tuple (rectangles, particle to rectangle map)
        rectangles - a Rectangles object that stores k rectangles with shape player_mask_shape.
        particle_to_rectangle_map - a numpy array with length n. particle_to_rectangle_map[i] = j iff
                                    the j-th rectangle surrounds the projection of the i-th player.
        '''
        #use the camera matrix to project the absolute positions to image positions.
        H = camera_matrix.get_homography_matrix()
        image_positions = planar_geometry.apply_homography_to_points(H, absolute_positions).astype(np.int32)

        #extract a list of representatives of distinct image positions
        #the player_to_position_map satisfies: player_to_position_map[i] = j <=>
        #image_positions[i] = image_position_representatives[j]
        image_position_reps, player_to_rectangle_map = cls._group_by_image_position(image_positions)

        #Each the image position rep is the middle of the bottom edge of a rectangle with shape player_mask_shape = (a,b).
        #to get the top left corner, we subtract b from the positions y coordinate and a/2 from the x coordinate
        to_top_left = -np.array([player_mask_shape[1] / 2.0, player_mask_shape[0]])

        top_left_corners = np.int32(image_position_reps + to_top_left)

        return Rectangles(player_mask_shape, top_left_corners), player_to_rectangle_map
    
    @classmethod
    def _has_degenerate_players(cls, soccer_field_rectangle, frame_data, rectangles, absolute_positions):
        '''
        soccer_field_rectangle - a Rectangle object representing the location of the field in absolute coordinates
        absolute_positions - a numpy array with shape (n, 2) repersenting a list of player positions 
                             on the absolute z=0 plane

        return - True iff one of the players is degenerate. A player is degenerate if the absolute position is off of the field
        '''
        #make sure that all of the rectangles are on the image
        image_rectangle = Rectangles.from_array(frame_data.get_image())
        if not rectangles.contained_in(image_rectangle).all():
            return True

        #check if the players are close enough to the field
        point_in_field = soccer_field_rectangle.contains_points(absolute_positions, cls.MAX_DISTANCE_FROM_FIELD)
        if not point_in_field.all():
            return True

        #check if the the area of the intersection of any of the rectangle with the *unpadded* mask is too small.
        unpadded_rectangle = Rectangles.from_rectangle(frame_data.get_unpadded_rectangle())
        unpadded_areas = rectangles.get_intersection_areas(unpadded_rectangle)

        return (unpadded_areas < cls.MIN_PLAYER_MASK_AREA).any()

    @classmethod
    def _build_rectangle_background_masks(cls, frame_data, rectangle_foreground_masks, rectangles):
        '''
        frame_data - a PaddedFrameData object
        rectangle_foreground_masks - a numpy array with shape (k,a,b) and type np.bool
        rectangles - a Rectangles object with k rectangles with shape (a,b)

        return a numpy array with shape (k,a,b) which represents the background masks for each of the rectangles
        '''

        #check if all of the player rectangles are in the unpadded region of the image
        unpadded_rectangle = Rectangles.from_rectangle(frame_data.get_unpadded_rectangle())
        if rectangles.contained_in(unpadded_rectangle).all():
            #in this case, the background masks are just the complements of the fg masks
            #the reason is that if a pixel is not in the fg, this means it is in the bg and not in the padding region.
            return np.logical_not(rectangle_foreground_masks)

        else:
            #in this case, we have to take slices of the padded bg mask in the frame data to take the padding into account.
            return rectangles.get_slices(frame_data.get_background_mask())
        
    @classmethod
    def from_frame_and_positions(cls, soccer_field_rectangle, frame_data, absolute_positions, number_of_particles=None):
        '''
        soccer_field_rectangle - a Rectangle object representing the location of the field in absolute coordinates
        frame_data - a PaddedFrameData object
        absolute_positions - a numpy array with shape (num positions, 2) repersenting a list of player positions 
                             on the absolute z=0 plane
        number_of_particles - an integer. If len(absolute_positions)>1, it must be None. Otherwise, it represents the number
                              of player particles that are being compared to the single given player.
        '''

        #if there are more than one abs position, we are not allowed to specify the number of particles
        if len(absolute_positions) > 1 and (number_of_particles is not None):
            raise ValueError('the number of particles can only be specified when there is a single absolute position')

        #if number_of_particles is None, set the number of particles to be equal to the number of players
        elif number_of_particles is None:
            number_of_particles = len(absolute_positions)

        #build the player mask using the average player position
        player_mask = cls._build_player_mask(frame_data.get_camera_matrix(), absolute_positions)

        #build the player rectangles using the projections of the player positions onto the image
        rectangles, player_to_rectangle_map = cls._build_rectangles(player_mask.shape,
                                                                    frame_data.get_camera_matrix(),
                                                                    absolute_positions)

        #check if any of the players are degenerate
        if cls._has_degenerate_players(soccer_field_rectangle, frame_data, rectangles, absolute_positions):
            return None
        
        #use the rectangles to extract slices from the foreground mask
        rectangle_foreground_masks = rectangles.get_slices(frame_data.get_foreground_mask())

        # print '** type of foreground masks: ', foreground_masks.dtype
        # print '   type of player mask: ', player_mask.dtype
        
        rectangle_background_masks = cls._build_rectangle_background_masks(frame_data, rectangle_foreground_masks, rectangles)

        return cls(frame_data, absolute_positions, player_mask, rectangles, player_to_rectangle_map,
                   rectangle_foreground_masks, rectangle_background_masks, number_of_particles)

    def get_player_rectangle(self):
        '''
        If this player data stores the data of a single particle, return a Rectangle object which represents 
        that particles rectangle.
        Otherwise, a RuntimeError is raised.
        '''
        if self._rectangles.get_number_of_rectangles() == 1:
            return self._rectangles.get_rectangle_objects()[0]

        else:
            raise RuntimeError('This local data has more than one particle')
        
    def get_player_mask(self):
        return self._player_mask

    def get_foreground_mask(self):
        '''
        If this player data stores the data of a single particle, return a numpy array with shape (n,m) and type np.bool

        Otherwise, a RuntimeError is raised.
        '''
        if self._rectangles.get_number_of_rectangles() == 1:
            return self._rectangle_foreground_masks[0]

        else:
            raise RuntimeError('This local data has more than one particle')

    def _get_player_rectangles(self):
        '''
        return a Rectangles object with (number of players) rectangles. The i-th rectangle bounds the projection of an 
        ellipsoid around the i-th player.

        Recall that the number of players can be greater than the number of player rectangles since more than one player
        can be assigned to each rectangle.
        '''
        if self._player_rectangles is None:
            self._player_rectangles = self._rectangles.select_rectangles(self._player_to_rectangle_map)

        return self._player_rectangles
    
    def _get_player_foreground_masks(self):
        '''
        return - a numpy array with shape (number of players, a, b)

        Recall that the number of players can be greater than the number of player rectangles since more than one player
        can be assigned to each rectangle.
        '''
        if self._player_foreground_masks is None:
            #convert the rectangle foreground masks to a list of the masks that are assigned to each player.
            #for example:
            #player_fg_masks[0] = rectangle_fg_masks[player_to_rectangle_map[0]] = fg mask of rectangle assigned to 0-th player
            self._player_foreground_masks = self._rectangle_foreground_masks[self._player_to_rectangle_map]

        return self._player_foreground_masks

    def _get_player_background_masks(self):
        '''
        return - a numpy array with shape (number of players, a, b)

        Recall that the number of players can be greater than the number of player rectangles since more than one player
        can be assigned to each rectangle.
        '''
        if self._player_background_masks is None:
            #convert the rectangle background masks to a list of the masks that are assigned to each player.
            #for example:
            #player_bg_masks[0] = rectangle_bg_masks[player_to_rectangle_map[0]] = bg mask of rectangle assigned to 0-th player
            self._player_background_masks = self._rectangle_background_masks[self._player_to_rectangle_map]

        return self._player_background_masks
    
    def number_of_particles(self):
        '''
        return an integer. This is equal to the number of particles of the player that are recorded in the data.
        
        This is the number of probabilities that are associated to the data.
        
        Note: Even if there is only one player mask, if there are N obstruction masks then we are recording the local struture
        of the player relative to N different particles of another player. So in this case we return N, despite having 
        only 1 player.
        '''
        return self._number_of_particles

    def number_of_player_positions(self):
        '''
        return an integer. The number of player positions that are recorded in the data.
        Note that this can be equal to 1 even if self.number_of_particles() is greater than 1.
        This happens when we only store a single player position, but are comparing it to multiple position candidates
        of a different player.
        '''
        return len(self._absolute_positions)

    def initialize_obstruction_masks(self):
        '''
        reset the obstruction masks to be empty masks.
        '''
        if self._obstruction_masks is not None:
            self._obstruction_masks[...] = False

        else:
            self._obstruction_masks = np.zeros((self.number_of_particles(),) + self._player_mask.shape, np.bool)

        return

    def initialize_obstacle_distances(self):
        '''
        reset the obstacle distances to an empty array.
        '''

        self._obstacle_distances = np.empty((self.number_of_particles(), 0), np.float32)
        return
    
    def get_unobstructed_foreground_player_mask_areas(self):
        '''
        Return a numpy array with shape (k,a,b) and type np.bool, where k is equal to self.number_of_particles()
        It represents a list of masks. Each mask represents the unobstructed portion of the intersection of the
        foregound mask with each of the player masks.

        Note that if there are N player masks and N obstruction masks, we return an array with shape (N,a,b).
        Same for 1 player mask and N obstruction masks.
        '''

        #if there are no obstruction masks, calculate the areas of the foreground masks directly.
        #note that in this case we assume that the number of particles is equal to the number of players
        if self._obstruction_masks is None:
            #fg_areas is a numpy array with length (number of rectangles). Recall that there is one fg mask per rectangle,
            #and that more than one player can share the same rectangle
            rectangle_fg_areas = (self._rectangle_foreground_masks * self._player_mask).sum(axis=(1,2))

            #use the player_to_rectangle_map to map these to per player values
            #recall that player_to_rectangle_map[i] = index of the rectangle that is assigned to player i
            #for example, the first element of the righthand side is
            #rectangle_fg_areas[_player_to_rectangle_map[0]] = fg area of the rectangle assigned to the 0-th player.
            player_fg_areas = rectangle_fg_areas[self._player_to_rectangle_map]
            return player_fg_areas

        #in this case we do not assume that the number of particles is equal to the number of players
        else:
            #since the number of obstruction masks is equal to the number of particles, not the number of rectangles,
            #we first must convert the rectangle foreground masks to a list of the masks that are assigned to each
            #player.
            player_fg_masks = self._get_player_foreground_masks()

            #this is an array with length num_particles, which is the length of _obstruction_masks.
            particle_fg_areas =  ((player_fg_masks * self._player_mask) * np.logical_not(self._obstruction_masks)).sum((1,2))
            return particle_fg_areas
        
    def get_unobstructed_background_player_mask_areas(self):
        '''
        Return a numpy array with shape (k,a,b) and type np.bool, where k is equal to self.number_of_particles()
        It represents a list of masks. Each mask represents the unobstructed portion of the intersection of the
        background mask(s) and the player mask(s).

        Note that if there are N player masks and N obstruction masks, we return an array with shape (N,a,b).
        Same for 1 player mask and N obstruction masks.
        '''
        #if there are no obstruction masks, calculate the areas of the foreground masks directly.
        #note that in this case we assume that the number of particles is equal to the number of players
        if self._obstruction_masks is None:
            #fg_areas is a numpy array with length (number of rectangles). Recall that there is one fg mask per rectangle,
            #and that more than one particle can share the same rectangle
            rectangle_bg_areas = (self._rectangle_background_masks * self._player_mask).sum(axis=(1,2))

            #use the player_to_rectangle_map to map these to player values
            #recall that player_to_rectangle_map[i] = index of the rectangle that is assigned to player i
            #for example, the first element of the righthand side is
            #rectangle_bg_areas[_player_to_rectangle_map[0]] = bg area of the rectangle assigned to the 0-th player.
            player_bg_areas = rectangle_bg_areas[self._player_to_rectangle_map]
            return player_bg_areas

        #in this case we do not assume that the number of particles is equal to the number of players
        else:
            #since the number of obstruction masks is equal to the number of particles, not the number of rectangles,
            #we first must convert the rectangle backfround masks to a list of the masks that are assigned to each
            #player.
            player_bg_masks = self._get_player_background_masks()

            #this is an array with length num_particles, which is the length of _obstruction_masks.
            particle_bg_areas =  ((player_bg_masks * self._player_mask) * np.logical_not(self._obstruction_masks)).sum((1,2))
            return particle_bg_areas

    def get_obstacle_distances(self):
        '''
        return a numpy array with shape (num particles, num obstacles)
        '''
        if self._obstacle_distances is not None:
            return self._obstacle_distances

        else:
            return np.empty((self.number_of_particles(),0), np.float32)
    
    def _find_obstructed_particles(self, other):
        '''
        other - a LocalPlayerData object

        Return a numpy array with length self.number_of_particles() and type np.bool.
        
        It is defined as follows:
        The behaviour depends on the tuple (len(self._players), len(self._obstructions), len(other._players)).
        The possible values of this tuple are values are:
        (1,N,1), (1,N,N), (N,N,1), (N,N,N) where N=self.number_of_particles()

        In all cases we return a length N array.

        (1,N,1): Let obs = other player obstructs this player. We return the length N array [obs, obs, ..., obs]
        (1,N,N): output[i] == True <=> other._players[i] obstructs self._players[0]
        (N,N,1): output[i] == True <=> other._players[0] obstructs self._players[i]
        (N,N,N): output[i] == True <=> other._players[i] obstructs self._players[i]
        '''
        #this is the length of the output
        N = self.number_of_particles()
        camera_matrix = self._frame_data.get_camera_matrix()
        
        sq_dist_A = camera_matrix.compute_sq_distance_to_camera(self._absolute_positions)
        sq_dist_B = camera_matrix.compute_sq_distance_to_camera(other._absolute_positions)

        #the case (1,N,1)
        if (len(self._absolute_positions) == len(other._absolute_positions) == 1):
            return np.ones(N, dtype=np.bool) * (sq_dist_B < sq_dist_A)

        #the other 3 cases are treated by:
        return sq_dist_B < sq_dist_A
        
    def add_obstructions(self, other):
        '''
        other - a LocalPlayerData object.

        The behaviour depends on the tuple (len(self._players), len(self._obstructions), len(other._players)).
        The possible values of this tuple are values are:
        (1,N,1), (1,N,N), (N,N,1), (N,N,N) where N=self.number_of_particles()

        (1,N,1): compute the obstruction of this player by the other player and add it to each of the N obs masks
        (1,N,N): compute the obstruction of this player by the i-th other player and update the i-th obs mask in this data
        (N,N,1): compute the obstruction of the i-th player by the other player and update the i-th obs mask in this data
        (N,N,N): compute the obs of the i-th player by the i-th other player and update the i-th obs mask
        '''

        #if this player does not have obstruction masks, initialize them to empty masks
        if self._obstruction_masks is None:
            self.initialize_obstruction_masks()
            
        #first compute which of the players in this local data are behind the corresponding player in the other local data
        #is_obstructed is a numpy array whose length is equal to the number of particles in this object
        is_obstructed = self._find_obstructed_particles(other)

        #print 'is obstructed mask: ', is_obstructed
        
        #If the number of players in the other data is equal to the number of obstruction masks in this data,
        #then this means that the i-th other player should contribute to the i-th obstruction mask in this data.
        #so we filter the other players by is_obstructed.
        if other.number_of_player_positions() == self.number_of_particles():
            filtered_other_player_rectangles = other._get_player_rectangles().select_rectangles(is_obstructed)
            
        #if the number of players in the other data is not equal to the number of obstruction masks in this data,
        #then it must be that this data has N>1 obstruction masks and the other data has exactly 1 player.
        #so we do not filter the other players.
        #I.e, other_player_rectangles will contain exactly the rectangle of the single player.
        else:
            filtered_other_player_rectangles = other._get_player_rectangles()

        #select the player rectangles and obstruction masks that correspond to the players in this data that are obstructed
        #by the corresponding player in the other data
        if self.number_of_player_positions() == self.number_of_particles():
            filtered_player_rectangles = self._get_player_rectangles().select_rectangles(is_obstructed)
            
        else:
            filtered_player_rectangles = self._get_player_rectangles()

        #the length of _obstruction_masks is always equal to the number of particles
        filtered_obstruction_masks = self._obstruction_masks[is_obstructed]

        filtered_player_rectangles.transfer_slices(filtered_obstruction_masks,
                                                   filtered_other_player_rectangles, other._player_mask[np.newaxis,:])

        self._obstruction_masks[is_obstructed] = filtered_obstruction_masks
        
        return

    #NEW
    def add_obstacle_distances(self, other):
        '''
        other - a LocalPlayerData object

        Add the N distances between the particles in this local data and the particles in the other local data to
        _distances, where N=self.number_of_particles()

        Specifically, the behaviour depends on the tuple 
        (len(self._players), len(other._players)).
        The possible values of this tuple are values are:
        (1,1), (1,N), (N,1), (N,N) 

        (1,1): compute the distance of this player to the other player and add it to each of the N distances
        (1,N): compute the distance of this player to the i-th other player and add it to the i-th distance
        (N,1): compute the distance of the i-th player to the other player and update the i-th distance
        (N,N): compute the distance of the i-th player to the i-th other player and update the i-th distance
        '''
        
        #if this player does not have distances, initialize them
        #if self._obstacle_distances is None:
        #    self.initialize_obstacle_distances()
        
        #this is a numpy array with length max(num players in self, num players in other).
        #so the length is 1 if the above tuple is (1,1) and N otherwise
        obstacle_distances = np.linalg.norm(self._absolute_positions - other._absolute_positions, axis=1)

        #add these distances to _obstacle_distances
        #if there is only one obstacle distance, this means that both this local data and the other one contain only one
        #player. In this case, we want to add N copies of this distance to the _obstacle_distance array
        if len(obstacle_distances) == 1:
            obstacle_distances = obstacle_distances * np.ones(self.number_of_particles())

        #now obstacle_distance is a length N array. We add it as a column to the _obstacle_distance array
        self._obstacle_distances = np.hstack([self._obstacle_distances, obstacle_distances.reshape((-1,1))])
            
            

