import os
import cv2
from .player_tracker_controller import PlayerTrackerController
from ...auxillary.soccer_field_geometry import SoccerFieldGeometry


# PREPROCESSING_PARENT_DIRECTORY = '/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts'
# OUTPUT_PARENT_DIRECTORY =        '/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts'
# IMAGE_PARENT_DIRECTORY =         '/Users/daniel/Dropbox/soccer/daniel_refactored/test_data'
# INPUT_DATA_NAME = 'video7'

# PREPROCESSING_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
# OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
# IMAGE_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/images'
# INPUT_DATA_NAME = 'images-8_15-9_31'

# PREPROCESSING_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
# OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
# INPUT_DATA_NAME = 'images-0_00-1_00'

OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'
INPUT_DATA_NAME = 'images-0_00-10_00'

# PREPROCESSING_PARENT_DIRECTORY = '/home/daniel/soccer/output'
# OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'
# IMAGE_PARENT_DIRECTORY = '/home/daniel/soccer/images'
# INPUT_DATA_NAME = 'images-8_15-9_31'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing_full', 'data',
                                       INPUT_DATA_NAME, 'stage_3')
OUTPUT_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'player_tracker', 'data', INPUT_DATA_NAME)

IMAGE_DIRECTORY       = os.path.join(PREPROCESSING_DIRECTORY, 'image')
ANNOTATION_DIRECTORY  = os.path.join(PREPROCESSING_DIRECTORY, 'annotation')
CAMERA_DIRECTORY      = os.path.join(PREPROCESSING_DIRECTORY, 'camera')
PLAYERS_DIRECTORY     = os.path.join(PREPROCESSING_DIRECTORY, 'players')
TEAM_LABELS_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'team_labels')

def load_absolute_soccer_field():
    '''
    Return a tuple (soccer field, absolute image)
    '''
    ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)
    SCALE = 6
    FIELD_WIDTH = absolute_image.shape[1]-1
    FIELD_LENGTH = absolute_image.shape[0]-1
    GRID_WIDTH = 20
    GRID_LENGTH = 14
    soccer_field = SoccerFieldGeometry(SCALE, FIELD_WIDTH, FIELD_LENGTH, GRID_WIDTH, GRID_LENGTH)

    return soccer_field

def run_player_tracker(image_directory, annotation_directory, camera_directory, players_directory, team_labels_directory,
                      output_directory):
    
    soccer_field = load_absolute_soccer_field()
    player_tracker_controller = PlayerTrackerController(soccer_field)

    print 'running player tracker...'
    print 'output directory: ', output_directory
    
    player_tracker_controller.track_players(image_directory, annotation_directory, camera_directory,
                                            players_directory, team_labels_directory, output_directory)

    return

if __name__ == '__main__':
    run_player_tracker(IMAGE_DIRECTORY, ANNOTATION_DIRECTORY, CAMERA_DIRECTORY, PLAYERS_DIRECTORY, TEAM_LABELS_DIRECTORY,
                      OUTPUT_DIRECTORY)
    
