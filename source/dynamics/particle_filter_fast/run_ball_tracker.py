import os
import cv2
from .ball_tracker_controller import BallTrackerController
from ....source.auxillary.soccer_field_geometry import SoccerFieldGeometry

"""
This is used to track balls in a sequence of FrameData objects.
For each iterator if frame data objects that we load from the preprocessing data, track the balls in that frame sequence and
store them as a dictionary of BallTrajectory objects. If a trajectory was initialized with the ball trajectory candidate btc,
the key for the trajectory in the dictionary is the id of btc.
"""

# IMAGE_DIRECTORY = 'test_data/video7'
# PREPROCESSING_NAME = 'video7'

# PREPROCESSING_DATA_DIRECTORY = 'output/scripts/preprocessing/data'
# BALL_TRACKER_DATA_DIRECTORY = 'output/scripts/ball_tracker/data'

# ANNOTATION_DIRECTORY = os.path.join(PREPROCESSING_DATA_DIRECTORY, PREPROCESSING_NAME, 'annotation')
# PLAYERS_DIRECTORY = os.path.join(PREPROCESSING_DATA_DIRECTORY, PREPROCESSING_NAME, 'players')
# CAMERA_DIRECTORY = os.path.join(PREPROCESSING_DATA_DIRECTORY, PREPROCESSING_NAME, 'camera')
# TEAM_LABELS_DIRECTORY = os.path.join(PREPROCESSING_DATA_DIRECTORY, PREPROCESSING_NAME, 'team_labels')
# BALLS_DIRECTORY = os.path.join(PREPROCESSING_DATA_DIRECTORY, PREPROCESSING_NAME, 'ball_trajectories')

# OUTPUT_PARENT_DIRECTORY =        '/Users/daniel/Dropbox/soccer/daniel_refactored/output/scripts'
# IMAGE_PARENT_DIRECTORY =         '/Users/daniel/Dropbox/soccer/daniel_refactored/test_data'
# INPUT_DATA_NAME = 'video7'

OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
IMAGE_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/images'
INPUT_DATA_NAME = 'images-8_15-9_31'

# OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'
# IMAGE_PARENT_DIRECTORY = '/home/daniel/soccer/images'
# INPUT_DATA_NAME = 'images-8_15-9_31'

PREPROCESSING_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'preprocessing', 'data', INPUT_DATA_NAME)
BALL_TRACKER_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'ball_tracker', 'data', INPUT_DATA_NAME)
IMAGE_DIRECTORY = os.path.join(IMAGE_PARENT_DIRECTORY, INPUT_DATA_NAME)

ANNOTATION_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY,  'annotation')
PLAYERS_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'players')
CAMERA_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'camera')
TEAM_LABELS_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'team_labels')
BALL_TRAJECTORIES_DIRECTORY = os.path.join(PREPROCESSING_DIRECTORY, 'ball_trajectories')

FPS = 25
TRAJECTORY_MATCH_THRESH = 5 #debugging. was 2.
MIN_TRAJECTORY_DISTANCE = 10
NUM_PARTICLES = 1500
INITIAL_VELOCITY_STDEV = 10
INITIAL_HEIGHTS_YARDS = ()# experiment with only tracking things on the ground (0.1, 1, 2)
MAX_BAD_FRAMES = 3
MAX_POSITION_VARIANCE_AIR = 20
MAX_POSITION_VARIANCE_GROUND = 5
GROUND_THRESHOLD = 1
EVALUATION_THRESHOLD = -0.5

def load_absolute_soccer_field():
    '''
    Return a tuple (soccer field, absolute image)
    '''
    ABSOLUTE_FILE_NAME = 'test_data/soccer-field.png'
    absolute_image = cv2.imread(ABSOLUTE_FILE_NAME, 1)
    SCALE = 6
    FIELD_WIDTH = absolute_image.shape[1]-1
    FIELD_LENGTH = absolute_image.shape[0]-1
    GRID_WIDTH = 20
    GRID_LENGTH = 14
    soccer_field = SoccerFieldGeometry(SCALE, FIELD_WIDTH, FIELD_LENGTH, GRID_WIDTH, GRID_LENGTH)

    return soccer_field

def run_ball_tracker(image_directory, annotation_directory, camera_directory, players_directory, team_labels_directory,
                     balls_directory, output_directory):
    '''
    image_directory - a directory with files image_0.png,...,image_N.png
    annotation_directory - a directory with files annotation_0.png,...,annotation_N.png
    camera_directory - a directory with subdirs cameras_ik_jk/. each subdir has files: camera_0.npy,...,camera_{jk-ik}.npy
    players_directory - a directory with subdirs players_ik_jk/. each subdir has directories: 
                            players_0/,...,players_{jk-ik}/
    team_labels_directory - a directory with subdirs team_labels_ik_jk/. each subdir has files 
                                team_labels_0.npy,...,team_labels_{jk-ik}.npy
    balls_directory - a directory with subdirs ball_trajectories_ik_jk/. 
                          each subdir has files: ball_trajectories_0.pckl,...,ball_trajectories_{jk-ik}.pckl
        
    Track the balls through the frames and save the resulting trajectories in output_file.
    '''
    soccer_field = load_absolute_soccer_field()
    ball_tracker_controller = BallTrackerController(soccer_field, FPS, TRAJECTORY_MATCH_THRESH, MIN_TRAJECTORY_DISTANCE,
                                                    NUM_PARTICLES, INITIAL_VELOCITY_STDEV, INITIAL_HEIGHTS_YARDS,
                                                    MAX_BAD_FRAMES, MAX_POSITION_VARIANCE_AIR, MAX_POSITION_VARIANCE_GROUND,
                                                    GROUND_THRESHOLD, EVALUATION_THRESHOLD)
    
    ball_tracker_controller.track_balls(image_directory, annotation_directory, camera_directory,
                                        players_directory, team_labels_directory, balls_directory, output_directory)

    return


if __name__ == '__main__':
    run_ball_tracker(IMAGE_DIRECTORY, ANNOTATION_DIRECTORY, CAMERA_DIRECTORY, PLAYERS_DIRECTORY, TEAM_LABELS_DIRECTORY,
                     BALL_TRAJECTORIES_DIRECTORY, BALL_TRACKER_DIRECTORY)
