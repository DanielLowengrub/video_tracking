import re
import os
import pickle
import functools
from multiprocessing import Pool
from datetime import datetime
from .frame_data import FrameData
from .player_tracker import PlayerTracker
from ...preprocessing import file_sequence_loader

class PlayerTrackerController(object):
    '''
    The job of this class is to load preprocessing data, feed it to the PlayerTracker, and save the output in a directory.
    It is also used to load the saved output into a list of TrackedPlayers objects.
    '''

    INTERVAL_FILE_NAME = 'tracked_players_%d_%d.pckl'
    INTERVAL_FILE_REGEX = re.compile('tracked_players_([0-9]+)_([0-9]+)\.pckl')

    def __init__(self, soccer_field):
        '''
        soccer_field - a SoccerFieldGeometry object
        '''

        self._soccer_field = soccer_field

    @classmethod
    def _save_tracked_players(cls, interval, tracked_players, tracked_players_directory):
        '''
        interval - a tuple (i,j)
        tracked_players - a TrackedPlayers object

        save the tracked players in tracked_players_directory/tracked_players_i_j.pckl
        '''
        tracked_players_file = cls.INTERVAL_FILE_NAME % interval
        f = open(os.path.join(tracked_players_directory, tracked_players_file), 'wb')
        pickle.dump(tracked_players, f)
        f.close()
        
        return

    @classmethod
    def save_tracked_players(cls, indexed_tracked_players, output_directory):
        '''
        indexed_tracked_players - an iterator of tuples ((ik,jk), a TrackedPlayer object)
        output_directory - a directory name

        save the tracked players in the output directory.
        '''
        for interval, tracked_players in indexed_tracked_players:
            cls._save_tracked_players(interval, tracked_players, output_directory)

        return
    
    @classmethod
    def _load_tracked_players_file(cls, tracked_players_file):
        '''
        tracked_players_file - a file name. This is a pickle file that stores a TrackedPlayers object
        '''
        f = open(tracked_players_file, 'rb')
        tracked_players = pickle.load(f)
        f.close()

        return tracked_players
    
    @classmethod
    def load_tracked_players(cls, tracked_players_directory):
        '''
        tracked_players_directory - a directory with files tracked_players_ik_jk.pckl. 
                                The file tracked_players_ik_jk.pckl contains
                                a TrackedPlayers object which represents player in the frame interval (ik,jk)

        return - an iterator of tuples ((ik,jk), tracked_players_ik_jk) where tracked_players_ik_jk is a TrackedPlayers
                 object
        '''
        
        interval_files = file_sequence_loader.get_sorted_file_names(tracked_players_directory, cls.INTERVAL_FILE_REGEX)

        for interval_file in interval_files:
            interval_basename = os.path.basename(interval_file)
            interval = tuple(map(int,cls.INTERVAL_FILE_REGEX.match(interval_basename).groups()))
            print 'loading tracked players from: ', interval_file
            
            tracked_players = cls._load_tracked_players_file(interval_file)
            
            yield (interval, tracked_players)

    def _track_players_in_interval(self, interval_data, output_directory):
        '''
        interval_data - an IntervalData object
        output_directory - the directory where we should save the output
        '''
        #initialize the player tracker
        frame_data_iter = (FrameData.load_pd_tuple(fd.build_data()) for fd in interval_data)
        player_tracker = PlayerTracker(frame_data_iter, self._soccer_field)
        
        #track players over time
        #print 'tracking players in interval (%d,%d)...' % interval_data.interval
        player_tracker.track_players()
        
        #save the player data that we recorded
        tracked_players = player_tracker.get_tracked_players()
        self._save_tracked_players(interval_data.interval, tracked_players, output_directory)

        return
    
    def track_players(self, image_directory, annotation_directory, camera_directory, players_directory, team_labels_directory,
                      output_directory):
        '''
        image_directory - a directory that can be loaded by image_loader
        annotation_directory - a directory that can be loaded by AnnotationGenerator
        camera_directory - a directory which can be loaded by CameraSamplesInterpolator
        players_directory - a directory that can be loaded by PlayersGenerator
        team_labels_directory - a directory that can be loaded by TeamLabelsGenerator
        output_directory - a directory name

        track the players using the data in these directories and save the resulting TrackedPlayers objects to the output
        directory.
        '''
        from ...preprocessing_full.preprocessing_data_loader import PreprocessingDataLoader
        from ...preprocessing_full.preprocessing_data_type import PreprocessingDataType
        
        pd_directory_dict = {PreprocessingDataType.IMAGE:       image_directory,
                             PreprocessingDataType.ANNOTATION:  annotation_directory,
                             PreprocessingDataType.CAMERA:      camera_directory,
                             PreprocessingDataType.PLAYERS:     players_directory,
                             PreprocessingDataType.TEAM_LABELS: team_labels_directory}

        interval_data_iter = PreprocessingDataLoader.load_data(pd_directory_dict)

        pool_worker = functools.partial(track_players_in_interval_worker, self, output_directory)
        p = Pool(None)
        for retval in p.imap(pool_worker, interval_data_iter, chunksize=1):
            continue

        # #this is an iterator of tuples (interval, list of FrameData objects in this interval)
        # indexed_frame_data = FrameData.load_frame_datas(image_directory, annotation_directory, camera_directory,
        #                                                  players_directory, team_labels_directory)

        # for interval, frame_data_in_interval in indexed_frame_data:
        #     #initialize the player tracker
        #     player_tracker = PlayerTracker(frame_data_in_interval, self._soccer_field)

        #     #track players over time
        #     print 'tracking players in interval (%d,%d)...' % interval
        #     player_tracker.track_players()

        #     #save the player data that we recorded
        #     tracked_players = player_tracker.get_tracked_players()
        #     self._save_tracked_players(interval, tracked_players, output_directory)

        return

def track_players_in_interval_worker(pt_controller, output_directory, interval_data):
    '''
    pt_controller - a PlayerTrackerController object
    output_directory - the directory where we should save the output
    interval_data - an IntervalData object
    '''
    time_str = datetime.now().isoformat()
    print '[%s] tracking players in interval (%d,%d) ...' % ((time_str,) + interval_data.interval)

    pt_controller._track_players_in_interval(interval_data, output_directory)

    return
