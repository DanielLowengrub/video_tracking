import collections
import numpy as np
import cv2
import itertools
from ...auxillary import iterator_operations

'''
A PlayerCandidate stores information about a potential player in an image.
contour - a Contour object
absolute_position - a numpy array with length 2 and type np.float32
team_label - an integer, equal to one of the attributes of TeamValue
'''

PlayerCandidate = collections.namedtuple('PlayerCandidate', ['contour', 'absolute_position', 'team_label'])

class FrameData(object):
    '''
    This class stores data relevant to a given frame that is not associated to any specific player.
    In particular, we store the image, foreground mask and camera matrix.
    '''

    CANNY_MIN_VAL = 150
    CANNY_MAX_VAL = 200
    EDGE_KERNEL = np.ones((3,3))
    
    def __init__(self, image, foreground_mask, camera_matrix, player_candidates, ball_trajectory_candidates=[]):
        '''
        image - a numpy array with shape (n,m,3) and type np.uint8
        foreground_mask - a numpy array with shape (n,m) and type np.bool
        camera_matrix - a CameraMatrix object
        player_candidates - a list of PlayerCandiate objects
        ball_trajectory_candidates - a list of BallTrajectoryCandidate objects
        '''

        self._image = image
        self._foreground_mask = foreground_mask
        self._camera_matrix = camera_matrix
        self._player_candidates = player_candidates
        self._ball_trajectory_candidates = ball_trajectory_candidates
        
        self._image_hs = cv2.cvtColor(self._image, cv2.COLOR_BGR2HSV)[:,:,:2]
        self._edge_mask = self._compute_edge_mask(self._image)
        
    def _compute_edge_mask(self, image):
        edges = cv2.Canny(image.copy(), self.CANNY_MIN_VAL, self.CANNY_MAX_VAL)
        edges = cv2.morphologyEx(edges, cv2.MORPH_CLOSE, self.EDGE_KERNEL)
        edge_mask = edges > 0

        return edge_mask

    @classmethod
    def load_pd_tuple(cls, pd_tuple):
        '''
        pd_tuple - a PreprocessingDataTuple object
        return - a FrameData object
        '''
        foreground_mask = pd_tuple.annotation.get_players_mask() > 0
        player_candidates = [PlayerCandidate(player.contour, player.position, team_label)
                             for player,team_label in zip(pd_tuple.players, pd_tuple.team_labels)]
        
        return FrameData(pd_tuple.image, foreground_mask, pd_tuple.camera, player_candidates, ball_trajectory_candidates=[])
    
    @classmethod
    def load_frame_datas(cls, image_directory, annotation_directory, camera_directory, player_directory, team_directory,
                         ball_trajectory_directory=None, reverse=False):
        '''
        image_directory - a directory with files image_0.png,...,image_N.png
        annotation_directory - a directory with files annotation_0.png,...,annotation_N.png
        camera_directory - a directory with subdirs cameras_ik_jk/. each subdir has files: camera_0.npy,...,camera_{jk-ik}.npy
        player_directory - a directory with subdirs players_ik_jk/. each subdir has directories: 
                           players_0/,...,players_{jk-ik}/
        team_directory - a directory with subdirs team_labels_ik_jk/. each subdir has files 
                         team_labels_0.npy,...,team_labels_{jk-ik}.npy

        ball_directory - a directory with subdirs ball_trajectories_ik_jk/. 
                         each subdir has files: ball_trajectories_0.pckl,...,ball_trajectories_{jk-ik}.pckl

        return an iterator of tuples: ((ik,jk), Ik)
        Ik is an iterator of the form (fd_0,...,fd_{ik-jk}). It iterates over the FrameData objects for frames ik through jk.
        '''
        from ...preprocessing_full.preprocessing_data_loader import PreprocessingDataLoader
        from ...preprocessing_full.preprocessing_data_type import PreprocessingDataType
        
        pd_directory_dict = {PreprocessingDataType.IMAGE:       image_directory,
                             PreprocessingDataType.ANNOTATION:  annotation_directory,
                             PreprocessingDataType.CAMERA:      camera_directory,
                             PreprocessingDataType.PLAYERS:     player_directory,
                             PreprocessingDataType.TEAM_LABELS: team_directory}

        interval_data_iter = PreprocessingDataLoader.load_data(pd_directory_dict)
        indexed_frame_data = ((interval_data.interval, (cls.load_pd_tuple(fd.build_data()) for fd in interval_data))
                              for interval_data in interval_data_iter)
    
        return indexed_frame_data
    
    def update_frame_data(self, image, foreground_mask, camera_matrix, player_candidates, ball_trajectory_candidates):
        '''
        image - a numpy array with shape (n,m,3) and type np.uint8
        foreground_mask - a numpy array with shape (n,m) and type np.bool
        camera_matrix - a CameraMatrix object
        player_candidates - a list of PlayerCandidate objects
        ball_trajectory_candidates - a list of BallTrajectoryCandidate objects
        '''

        self._image = image
        self._foreground_mask = foreground_mask
        self._camera_matrix = camera_matrix
        self._player_candidates = player_candidates
        self._ball_trajectory_candidates = ball_trajectory_candidates
        
        self._edge_mask = self._compute_edge_mask(self._image)
        self._image_hs = cv2.cvtColor(self._image, cv2.COLOR_BGR2HSV)[:,:,:2]
        
    def update_from_frame_data(self, other):
        '''
        other - a FrameData object

        Update the variables of this frame date to those of the other one.
        '''

        self.update_frame_data(other._image, other._foreground_mask, other._camera_matrix,
                               other._player_candidates, other._ball_trajectory_candidates)

        return
    
    def get_image(self):
        return self._image

    def get_image_hs(self):
        return self._image_hs
    
    def get_image_sat(self):
        return self._image_hs[:,:,1]

    def get_edge_mask(self):
        return self._edge_mask
    
    def get_foreground_mask(self):
        return self._foreground_mask

    def get_camera_matrix(self):
        return self._camera_matrix

    def get_player_candidates(self):
        return self._player_candidates

    def get_ball_trajectory_candidates(self):
        return self._ball_trajectory_candidates

    def set_ball_trajectory_candidates(self, ball_trajectory_candidates):
        self._ball_trajectory_candidates = ball_trajectory_candidates
