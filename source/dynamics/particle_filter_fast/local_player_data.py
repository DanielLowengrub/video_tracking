import numpy as np
import cv2
from ...auxillary.rectangle import Rectangle
from ...auxillary import world_to_image_projections
from ...auxillary import planar_geometry

class LocalPlayerData(object):
    '''
    This class keeps track of information in a frame that is specific to a given player.
    It stores an rectangle on an image, a camera matrix, an image patch, a fg mask patch, a list of player masks
    a list of positions and a list of obs masks.

    The reason for storing a list is that we sometimes want to simultaneously deal with many particles associated to a given
    player. Each particle has its own position and obstruction mask.

    The "number_of_particles" in a local player data object is defined to be the number of local image regions that the object
    is keeping track of.

    There are 2 possible values for the tuple  (number of players, number of particles):
    (1,N): This happens when we are storing the position of a  single player P0, but want to evaluate it relative to 
           a player P1 which has N candidate positions
    (N,N): This is the usual case, in which we are evaluating N candidates for the position of a player.

    Note:
    * The number of obstruction masks is always equal to N. 
      In the case (1,N) above, the N obstruction masks correspond to
      the parts of player P0 that are obstructed by each of the candidates for P1.
      In the case of (N,N), then the N obstructions correspond to the obstructed regions of each of the candidates 
      for this player.

    * The number of player masks (either 1 or N) is always equal to the number of player positions (either 1 or N)
    '''
    
    MIN_PLAYER_MASK_AREA = 5  #If the ellipse mask corresponding to a position is less than this, the player is degenerate
    MAX_DISTANCE_FROM_FIELD = 5 #The farthest a player is allowed to stray from the soccer field.
    
    def __init__(self, region, camera_matrix, cropped_image, cropped_foreground_mask,
                 absolute_positions, cropped_player_masks,
                 cropped_obstruction_masks, degenerate_particle_mask):
        '''
        region - a Rectangle object. It represents the cropped region of the image that the cropped images/mask correspond to.
        camera_matrix - a CameraMatrix object

        cropped_image - a numpy array with shape (a,b,3) and type np.UINT8
        cropped_foreground_mask - a numpy array with shape (a,b) and type np.bool

        absolute_positions - a numpy array with shape (k,2) and type np.float32
        cropped_player_masks - a numpy array with shape (k,a,b) and type np.bool where k is an integer.

        cropped_obstruction_masks - a numpy array with shape (l,a,b) and type np.bool where l is an integer. 
        degenerate_particle_mask - a numpy array with length l and type np.bool. It is true in the i-th index if the i-th
                                   particle recorded by this local data is degenerate. In particular, if the i-th index is
                                   true then we set the i-th obstruction mask is zero and do not use it.

        The allowed values for (k,l) are (1,N) or (N,N) for some integer N>=1.
        For example, in some implementations there will be only one player mask and many obstruction masks.
        In other implementations where could be N player masks and N obs masks.
        '''

        self._region = region
        self._camera_matrix = camera_matrix
        
        self._cropped_image = cropped_image
        self._cropped_foreground_mask = cropped_foreground_mask

        self._absolute_positions = absolute_positions
        self._cropped_player_masks = cropped_player_masks
        
        self._cropped_obstruction_masks = cropped_obstruction_masks
        self._degenerate_particle_mask = degenerate_particle_mask

    @classmethod
    def _build_player_masks(cls, player_ellipses, ellipse_regions, region):
        '''
        player_ellipses - a numpy array with shape (n,3,3)
        ellipse_regions - a numpy array with shape (n,2,2)
        region - a Rectangle object

        return a numpy array with shape (n,a,b) and type np.bool where (a,b) is the shape of region.

        The i-th ellipse region is the axis aligned bounding box for the i-th ellipse.
        We assume that the region contains all of the ellipse regions.

        the i-th axb array in the output represents a mask for the i-th ellipse. Specifically, its shape is (a,b) and it
        is equal to True at a given pixel iff that pixel is in the i-th ellipse.
        '''

        #first compute the ellipse centers. These are simply the centers of the ellipse regions
        ellipse_centers = ellipse_regions.mean(axis=1)

        #for each ellipse, we determine if the value of the ellipse equation on the center is positive or negative.
        #if it is negative, we multiply the ellipse equation by -1. This ensures that the inside of the ellipse is the region
        #where the equation is positive.
        value_at_center = planar_geometry.evaluate_ellipses_on_points(player_ellipses, ellipse_centers)
        player_ellipses[value_at_center < 0] *= -1.0

        #transform the ellipses to the coordinate system of the region
        #this is the *inverse* of the matrix from image coords to region coords. I.e, it takes region
        #coords to image coords
        T_inv = planar_geometry.translation_matrix(region.get_top_left())
        player_ellipses = planar_geometry.apply_homography_inv_to_ellipses(T_inv, player_ellipses)

        #to build the player masks, we evalute the player_ellipses at each of the pixels in the region and check which
        #ones are positive
        x = np.arange(0,region.get_shape()[1],1)
        y = np.arange(0,region.get_shape()[0],1)
        XX,YY = np.meshgrid(x,y)

        # print 'XX shape: ', XX.shape
        # print 'XX type: ', XX.dtype
        
        ellipse_values = planar_geometry.evaluate_ellipses_on_grid(player_ellipses, XX, YY)
        player_masks = ellipse_values >= 0

        return player_masks


    @classmethod
    def _find_degenerate_particles(cls, soccer_field_rectangle, absolute_positions, cropped_player_masks, number_of_particles):
        '''
        soccer_field_rectangle - a Rectangle object representing the location of the field in absolute coordinates
        absolute_positions - a numpy array with shape (num_positions, 2) repersenting a list of player positions 
                             on the absolute z=0 plane
        cropped_player_masks - a numpy array with shape (n,a,b) and type np.bool. it represents a list of player masks
        number_of_particles - an integer

        return - a numpy array with length (number_of_particles) and type bool.

        If there is only one absolute_position and player mask, we check if it is degenerate and record this value
        in all of the entries of the output.

        If there is more than one, then num_positions = number_of_particles. In this case, we check if each of the players
        is degenerate and store the result in a length number_of_particles array.
        '''

        #check if the area of the player mask is large enough
        is_degenerate = cropped_player_masks.sum(axis=(1,2)) < cls.MIN_PLAYER_MASK_AREA

        #check if the player is close enough to the field
        point_in_field = soccer_field_rectangle.contains_points(absolute_positions, cls.MAX_DISTANCE_FROM_FIELD)
        is_degenerate *= np.logical_not(point_in_field)

        #if there is only one player, then make an array that contains the (single) value is_degenerate number_of_particles
        #times
        if len(absolute_positions) == 1:
            is_degenerate = np.array([is_degenerate[0] for i in range(number_of_particles)])
            
        return is_degenerate
        
    @classmethod
    def from_frame_and_positions(cls, soccer_field_rectangle, frame_data, absolute_positions, number_of_particles=None):
        '''
        soccer_field_rectangle - a Rectangle object representing the location of the field in absolute coordinates
        frame_data - a FrameData object
        absolute_positions - a numpy array with shape (num positions, 2) repersenting a list of player positions 
                             on the absolute z=0 plane
        number_of_particles - an integer. If len(absolute_positions)>1, it must be None. Otherwise, it represents the number
                              of player particles that are being compared to the single given player.
        '''

        #if there are more than one abs position, we are not allowed to specify the number of particles
        if len(absolute_positions) > 1 and (number_of_particles is not None):
            raise ValueError('the number of particles can only be specified when there is a single absolute position')

        #if number_of_particles is None, set the number of particles to be equal to the number of players
        elif number_of_particles is None:
            number_of_particles = len(absolute_positions)

        #now compute the player masks corresponding to each of the player positions. Recall that we can use the camera
        #to associate an ellipse on the image to each player position.
        
        #before calculating the player masks, find the bounding boxes of each of the ellipses,
        #and take their union in order to find the smallest portion of the image that contains all of the ellipses. This is
        #the region on which we will draw the masks.
        #this is a numpy array with shape (n,3,3)
        #print 'generating ellipses...'
        player_ellipses = world_to_image_projections.project_players_to_image(frame_data.get_camera_matrix(),
                                                                              absolute_positions)

        #compute the bounding boxs of each of the ellipses
        #this is a np array with shape (n,2,2). each (2,2) matrix stores a rectangle as: [[left,top],[right,bottom]]
        ellipse_regions = planar_geometry.get_ellipse_regions(player_ellipses)

        #the player data region is the smallest rectangle which contains all of the ellipse regions
        top_left = np.int32(ellipse_regions[:,0,:].min(axis=0))
        bottom_right = np.int32(ellipse_regions[:,1,:].max(axis=0))
        region = Rectangle(top_left, bottom_right)

        #throw out the parts of the region that are not in the image
        image_rectangle = Rectangle.from_array(frame_data.get_image())
        region = image_rectangle.intersection(region)

        if region is None:
            return None
        
        #use the region to construct the cropped image and fg mask
        cropped_image = region.get_slice(frame_data.get_image())
        cropped_foreground_mask = region.get_slice(frame_data.get_foreground_mask())

        #use the player ellipses to build the cropped player masks
        #print 'generating player masks...'
        cropped_player_masks = cls._build_player_masks(player_ellipses, ellipse_regions, region)
            
        #we initialize the obstruction masks to be empty masks. The number of obstruction masks is equal to the number
        #of particles
        cropped_obstruction_masks = np.zeros((number_of_particles,)+region.get_shape(), np.bool)

        #finally, check which players are degenerate
        degenerate_particle_mask = cls._find_degenerate_particles(soccer_field_rectangle,
                                                                  absolute_positions, cropped_player_masks,
                                                                  number_of_particles)

        return cls(region, frame_data.get_camera_matrix(), cropped_image, cropped_foreground_mask, absolute_positions, cropped_player_masks, cropped_obstruction_masks, degenerate_particle_mask)
    
    def get_cropped_image(self):
        return self._cropped_image

    def number_of_particles(self):
        '''
        return an integer. This is equal to the number of particles of the player that are recorded in the data.
        
        This is the number of probabilities that are associated to the data.
        
        Note: Even if there is only one player mask, if there are N obstruction masks then we are recording the local struture
        of the player relative to N different particles of another player. So in this case we return N, despite having 
        only 1 player.
        '''

        #we define the number of particles to be the number of obstruction masks.
        return len(self._cropped_obstruction_masks)

    def get_degenerate_particle_mask(self):
        '''
        return a numpy array with length number_of_particles() and type np.bool.
        '''
        return self._degenerate_particle_mask

    def is_degenerate(self):
        '''
        return true iff there exists a degenerate particle
        '''
        return np.any(self._degenerate_particle_mask)
    
    def get_unobstructed_foreground_player_masks(self):
        '''
        Return a numpy array with shape (k,a,b) and type np.bool, where k is equal to self.number_of_particles()
        It represents a list of masks. Each mask represents the unobstructed portion of the intersection of the
        foregound mask with each of the player masks.

        Note that if there are N player masks and N obstruction masks, we return an array with shape (N,a,b).
        Same for 1 player mask and N obstruction masks.
        '''

        #Note that if there are N player masks, the expression in the first parenthesis multiplies the fg mask by each of the
        #player masks.
        #Similarly, if there is only one player mask but N obs masks, the output contains the intersection of (fg and player)
        #with each of the obstruction masks.
        return (self._cropped_foreground_mask * self._cropped_player_masks) * (1 - self._cropped_obstruction_masks)

    def get_unobstructed_background_player_masks(self):
        '''
        Return a numpy array with shape (k,a,b) and type np.bool, where k is equal to self.number_of_particles()
        It represents a list of masks. Each mask represents the unobstructed portion of the intersection of the
        background mask(s) and the player mask(s).

        Note that if there are N player masks and N obstruction masks, we return an array with shape (N,a,b).
        Same for 1 player mask and N obstruction masks.
        '''

        #Note that if there are N player masks, the expression in the first parenthesis multiplies the bg mask by each of the
        #player masks.
        #Similarly, if there is only one player mask but N obs masks, the output contains the intersection of (bg and player)
        #with each of the obstruction masks.
        return ((1 - self._cropped_foreground_mask) * self._cropped_player_masks) * (1 - self._cropped_obstruction_masks)

    def _find_obstructed_particles(self, other):
        '''
        other - a LocalPlayerData object

        Return a numpy array with length self.number_of_particles() and type np.bool.
        
        It is defined as follows:
        The behaviour depends on the tuple (len(self._players), len(self._obstructions), len(other._players)).
        The possible values of this tuple are values are:
        (1,N,1), (1,N,N), (N,N,1), (N,N,N) where N=self.number_of_particles()

        In all cases we return a length N array.

        (1,N,1): Let obs = other player obstructs this player. We return the length N array [obs, obs, ..., obs]
        (1,N,N): output[i] == True <=> other._players[i] obstructs self._players[0]
        (N,N,1): output[i] == True <=> other._players[0] obstructs self._players[i]
        (N,N,N): output[i] == True <=> other._players[i] obstructs self._players[i]
        '''
        #this is the length of the output
        N = len(self._cropped_obstruction_masks)

        sq_dist_A = self._camera_matrix.compute_sq_distance_to_camera(self._absolute_positions)
        sq_dist_B = self._camera_matrix.compute_sq_distance_to_camera(other._absolute_positions)

        #the case (1,N,1)
        if (len(self._absolute_positions) == len(other._absolute_positions) == 1):
            return np.ones(N, dtype=np.bool) * (sq_dist_B < sq_dist_A)

        #the other 3 cases are treated by:
        return sq_dist_B < sq_dist_A
        
    def add_obstructions(self, other):
        '''
        other - a LocalPlayerData object.

        The behaviour depends on the tuple (len(self._players), len(self._obstructions), len(other._players)).
        The possible values of this tuple are values are:
        (1,N,1), (1,N,N), (N,N,1), (N,N,N) where N=self.number_of_particles()

        (1,N,1): compute the obstruction of this player by the other player and add it to each of the N obs masks
        (1,N,N): compute the obstruction of this player by the i-th other player and update the i-th obs mask in this data
        (N,N,1): compute the obstruction of the i-th player by the other player and update the i-th obs mask in this data
        (N,N,N): compute the obs of the i-th player by the i-th other player and update the i-th obs mask
        '''

        #first compute which of the players in this local data are behind the corresponding player in the other local data
        #is_obstructed is a numpy array whose length is equal to the length of self._cropped_obstruction_masks
        is_obstructed = self._find_obstructed_particles(other)

        #print 'is obstructed mask: ', is_obstructed
        
        #calculate the region that is common to the region in this player data and the other one
        intersection_region = self._region.intersection(other._region)

        #print 'intersection region: ', intersection_region
        
        #for each player mask in the other data that is obstructing the corresponding player in this data,
        #get the part of the obstructing player mask that lies in the intersection region
        
        #If the number of players in the other data is equal to the number of obstruction masks in this data,
        #then this means that the i-th other player should contribute to the i-th obstruction mask in this data.
        #so we filter the other players by is_obstructed.
        if len(other._cropped_player_masks) == len(is_obstructed):
            other_player_masks = other._cropped_player_masks[is_obstructed]

        #if the number of players in the other data is not equal to the number of obstruction masks in this data,
        #then it must be that this data has N>1 obstruction masks and the other data has exactly 1 player.
        #so we do not filter the other players.
        #I.e, other_player_masks will contain exactly the mask of the single player.
        else:
            other_player_masks = other._cropped_player_masks

        # print 'other player masks before slice:'
        # for i,m in enumerate(other_player_masks):
        #     cv2.imshow('other mask %d' % i, np.float32(m))
        #     cv2.waitKey(0)
        #     cv2.destroyAllWindows()

        sliced_other_player_masks = other._region.get_relative_slices(other_player_masks, intersection_region)

        # print 'other player masks after slice:'
        # for i,m in enumerate(sliced_other_player_masks):
        #     cv2.imshow('other mask %d' % i, np.float32(m))
        #     cv2.waitKey(0)
        #     cv2.destroyAllWindows()

        #for each player in this data that is obstructed by the corresponding player in the other data,
        #get the postion of that players obstruction mask that lies in the intersection region
        obstruction_masks = self._cropped_obstruction_masks[is_obstructed]
        sliced_obstruction_masks = self._region.get_relative_slices(obstruction_masks, intersection_region)

        # print 'taking slice of rectangle %s relative to rectangle %s.' % (str(intersection_region), str(self._region))
        # print 'this players obstruction mask slices:'
        # for m in sliced_obstruction_masks:
        #     cv2.imshow('obs mask', np.float32(m))
        #     cv2.waitKey(0)
        #     cv2.destroyAllWindows()

        #add the players masks from the other data to the obstruction masks in this data (i.e, bitwise or).
        #note that by the construction of obstruction_masks and player_masks, we only do this for players in the other data
        #that obstruct the corresponding player in this data.
        sliced_obstruction_masks += sliced_other_player_masks
        self._cropped_obstruction_masks[is_obstructed] = obstruction_masks

        return

        


