import itertools
import collections
import numpy as np
import cv2
from .tracked_ball import TrackedBall
from .tracked_balls import TrackedBalls, BallTrajectory
from ...image_processing.edge_ball_evaluator import EdgeBallEvaluator
from ....testers.auxillary import primitive_drawing

#TODO: frame_data_iterator should not be a constructer parameter, but rather should be passed to track_balls.
#track_balls will call _initialize to clear the variables that depend on the frame data.
class BallTracker(object):
    '''
    The job of this class is to track a list of TrackedBall objects through a sequence of images

    It works as follows:
    Initialization:
        * create an empty dictionary active_balls.
        * create an empty list height_groups.
        
    Timestep:
        * Update positions of active balls
        * Throw out degenerate balls: degenerate = off screen or off field
        * Compute ball probabilites and weights of active balls
        * Resample particle filters of active balls
        * Compute ExpectedBallData for the active tracked balls
        * Evaluate active balls by how well the tracking is going. 
          remove tracked balls that are performing poorly from the active_balls dictionary.
        * Add new tracked balls: For each of BallTrajectoryCandidate in the current frame, check if it matches an active
          tracked ball. If it does not, then use it to initialize new tracked balls at various heights. Add the tuple of 
          these tracked balls to height_groups. Also add all of these tracked balls to the active_balls dictionary.
    '''

    def __init__(self, soccer_field, frame_data_iterator, fps, num_particles, initial_velocity_stdev, initial_heights_yards,
                 max_bad_frames, max_position_variance_air, max_position_variance_ground,
                 ground_threshold, evaluation_threshold, visualizer=None):
        '''
        soccer_field - a SoccerFieldGeometry object
        frame_data_iterator - an iterator of FrameData objects
        fps - the framerate of the frame data iterator
        num_particles - the number of particles to use per ball
        initial_heights_yards - the heights at which to intialize tracked balls from ball trajectory candidates
        initial_velocity_stdev - the stdev of the velocity that a tracked ball starts with.
        max_bad_frames - an integer. If a ball tracker has more than this number of failed frames in a row then 
                         it is decativated
        max_position_variance_air - a ball tracker tracking a ball moving through the air is declared to have failed 
                                    if the position variance is larger than this
        max_position_variance_ground - a ball tracker tracking a ball moving on the air is declared to have failed 
                                    if the position variance is larger than this

        ground_threshold - a ball tracker is declared to have failed if it is lower than -ground_threshold
        evaluation_threshold - a ball tracker is declared to have failed if its evaluation is higher than this.
        visualizer - a BallTrackerVisualizer object which is used to generate debugging data
        '''
        self._soccer_field = soccer_field
        self._frame_data_iterator = frame_data_iterator
        self._fps = fps
        self._num_particles = num_particles
        self._initial_velocity_stdev = initial_velocity_stdev
        self._max_bad_frames = max_bad_frames
        self._max_position_variance_air = max_position_variance_air
        self._max_position_variance_ground = max_position_variance_ground
        self._ground_threshold = ground_threshold
        self._evaluation_threshold = evaluation_threshold
        self._visualizer = visualizer
        
        self._current_frame_data = None
        
        #a dictionary of TrackedBall objects that are successfully tracking a ball. Keys are ball ids.
        self._active_balls = dict()

        #each ball candidates is used to generate tracked balls at these heights.
        self._initial_heights = map(self._soccer_field.convert_yards_to_world_coords, initial_heights_yards)
        
        #a dictionary with items
        #(ball trajectory candidate id, list of TrackedBall objects).
        #The tuple consists of tracked balls that start from different heights of the same ball candidate.
        self._height_groups = collections.defaultdict(list)

        #this stores the id that will be assigned to the next tracked ball.
        self._available_tracked_ball_id = 0

        self._ball_evaluator = EdgeBallEvaluator()

        #these get populated after the tracking is over
        self._tracked_balls = None
        self._number_of_tracked_frames = None

    def _initialize(self):
        '''
        This is called before starting the tracker.
        '''
        self._tracked_balls = None
        self._number_of_tracked_frames = None

        return
    
    def _get_available_tracked_ball_id(self):
        available_id = self._available_tracked_ball_id
        self._available_tracked_ball_id += 1

        return available_id
    
    def _update_particle_filters(self):
        '''
        Update the particle filters in each of the active balls.
        Note that this changes their state vectors, but not the probabilities and weights.
        '''
        print 'updating particle filters...'
        for tracked_ball in self._active_balls.itervalues():
            tracked_ball.update_particle_filter()

        return
    
    def _deactivate_degenerate_balls(self):
        '''
        Deactivate active balls that are degenerate. This can happen if their image is off the screen,
        or their position is off the field.
        '''
        print 'deactivating degenerate balls...'
        for ball_id, tracked_ball in self._active_balls.items():
            if tracked_ball.deactivate_if_degenerate():
                print '   ball %d is degenerate. removing it from the active balls' % ball_id
                del self._active_balls[ball_id]

        print 'the active balls are now: ', self._active_balls.keys()
        
        return

    def _compute_probabilities_and_weights(self, frame_index=None):
        '''
        frame_index - an integer. This is used for debugging purposes only.

        Compute the probabilities and weights of all active balls.
        This should be called after the positions of the balls have been updated.
        '''
        print 'computing probabilities and weights for %d balls...' % len(self._active_balls)

        for ball_id, tracked_ball in self._active_balls.iteritems():
            print '   computing probabilities of ball %d' % ball_id
            particle_filter = tracked_ball.get_ball_particle_filter()
            particle_filter.compute_particle_probabilities()
            particle_filter.compute_particle_weights()

        #now that we have the updated weights for all the active balls, resample the particle filters
        for tracked_ball in self._active_balls.itervalues():
            tracked_ball.get_ball_particle_filter().resample()

        return

    def _compute_expected_ball_data(self):
        '''
        Refresh the expected ball data of each of the active balls.
        '''
        print 'refreshing expected ball data for %d balls...' % len(self._active_balls)
        
        for tracked_ball in self._active_balls.itervalues():
            tracked_ball.refresh_expected_ball_data()

        return

    def _evaluate_balls(self):
        '''
        Evaluate how well the tracked balls are doing. Remove badly performing balls from the active_balls dict.
        '''
        print 'evaluating active balls...'
        for ball_id, tracked_ball in self._active_balls.items():
            if tracked_ball.lost_target():
                print '   ball %d lost its target. removing from active balls' % ball_id
                del self._active_balls[ball_id]
        
        return
    
    def _generate_debugging_data(self, frame_index):
        '''
        Print out a summary of the current state of the ball tracker, and save debugging information for
        later processing.
        '''
        print 'after updating the particle filter, the tracked balls are:'
        
        for ball_id, tracked_ball in self._active_balls.iteritems():
            print tracked_ball.get_summary()

            #also, we save a snapshot of the ball for debugging purposes
            tracked_ball.save_ball_data()

        print 'there are %d active balls: %s' % (len(self._active_balls),  self._active_balls.keys())

        # image_copy = self._current_frame_data.get_image().copy()
        # camera_matrix = self._current_frame_data.get_camera_matrix()
        # alpha = 0.4
        
        # for tracked_ball in self._active_balls.itervalues():
        #     print 'drawing variance mask for ball: %d' % tracked_ball.get_ball_id()
        #     variance_rectangle = tracked_ball.get_expected_ball_data().get_variance_rectangle()
        #     variance_mask = tracked_ball.get_expected_ball_data().get_variance_mask()
            
        #     image_slice = variance_rectangle.get_slice(image_copy)

        #     if image_slice.shape[:2] != variance_mask.shape:
        #         continue

        #     blue_image = image_slice.copy()
        #     blue_image[variance_mask] = np.array([255,0,0], np.uint8)
        #     image_slice[...] = alpha*blue_image + (1-alpha)*image_slice
            
        # for tracked_ball in self._active_balls.itervalues():
        #     ball_position = tracked_ball.get_expected_position()
        #     image_position = camera_matrix.project_points_to_image(ball_position.reshape((1,3)))[0]
        #     primitive_drawing.draw_point(image_copy, image_position, (0,255,0), 1)

        # cv2.imshow('tracked balls', image_copy)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()
        
        return

    
    def _matches_active_ball(self, ball_trajectory_candidate):
        '''
        ball_trajectory_candidate - a BallTrajectoryCandidate object. 

        return True if there is an active ball that matches the ball candidate.
        '''
        has_match = any(ball.matches_ball_trajectory_candidate(ball_trajectory_candidate)
                        for ball in self._active_balls.itervalues())

        return has_match
        
    
    def _find_isolated_trajectory_candidates(self, ball_trajectory_candidates):
        '''
        ball_trajectory_candidates - a list of BallTrajectoryCandidate objects

        return - a list of BallTrajectoryCandidate objects consisting of the candidates that are not near any of the active
        balls
        '''
        isolated_candidates = [btc for btc in ball_trajectory_candidates if not self._matches_active_ball(btc)]

        print 'returning %d isolated candidates.' % len(isolated_candidates)
        print 'the isolated candidate ids are: ', [btc.get_id() for btc in isolated_candidates]
        
        # print 'with positions: '
        # print [pc.absolute_position for pc in isolated_candidates]
        return isolated_candidates

    def _add_active_ball_from_candidate_and_state(self, btc, frame_index, initial_position, initial_velocity, on_ground):
        '''
        btc - a BallTrajectoryCandidate object
        frame_index - an integer
        initial_position - a numpy array with length 3 and type np.float64
        initial_velcoity - a numpy array wuth length 3 and type np.float64
        on_ground - a boolean

        create a TrackedBall with the given initial position and velocity and add it to the active ball dictionary and
        the height group dictionary. 
        If on_ground is True, the TrackedBall is set to be rolling on the ground.
        '''
        height_group_id = btc.get_id()
        tracked_ball_id = self._get_available_tracked_ball_id()
        ball_radius = self._soccer_field.get_soccer_ball_radius()

        if on_ground:
            tracked_ball = TrackedBall.from_initial_state_on_ground(tracked_ball_id, height_group_id, frame_index,
                                                                    initial_position[:2], initial_velocity[:2],
                                                                    self._initial_velocity_stdev, self._num_particles,
                                                                    self._ball_evaluator, self._soccer_field,
                                                                    self._current_frame_data, self._max_bad_frames,
                                                                    self._max_position_variance_ground, self._ground_threshold,
                                                                    self._evaluation_threshold)
        else:
            tracked_ball = TrackedBall.from_initial_state(tracked_ball_id, height_group_id, frame_index,
                                                          initial_position, initial_velocity, self._initial_velocity_stdev,
                                                          self._num_particles, self._ball_evaluator, self._soccer_field,
                                                          self._current_frame_data, self._max_bad_frames,
                                                          self._max_position_variance_air, self._ground_threshold,
                                                          self._evaluation_threshold)

        #save the state of the state of the new tracked ball before it gets updated to the next frame.
        tracked_ball.save_ball_data()

        #add the tracked ball to the list of tracked balls associated to this ball trajectory candidate
        self._height_groups[height_group_id].append(tracked_ball)
        print '   added ball %d to height group %d' % (tracked_ball_id, height_group_id)
        
        #also add it to the list of active balls
        self._active_balls[tracked_ball_id] = tracked_ball

        return
    
    def _add_active_balls_from_candidate(self, ball_trajectory_candidate, frame_index):
        '''
        ball_trajectory_candidate - a BallTrajectoryCandidate 
        frame_index - the frame in which the ball trajectory was added

        Create a tuple of TrackedBall objects which start at the candidate's position on the field,
        and the at the heights _initial_heights. The tuple is added to height_groups, and all of the TrackedBalls are added to
        _active_balls.
        '''
        print 'adding active balls from candidate...'

        #if the candidate has a default state, then use that to build an active ball
        if ball_trajectory_candidate.has_default_state():
            print '   adding tracked ball with default state:'
            initial_position, initial_velocity, on_ground = ball_trajectory_candidate.get_default_state()
            print '   position: %s, velocity: %s, on_ground: %s', (initial_position, initial_velocity, on_ground)
            self._add_active_ball_from_candidate_and_state(ball_trajectory_candidate, frame_index,
                                                           initial_position, initial_velocity, on_ground)
            return

        #otherwise, add balls at different heights
        print '   adding tracked balls at different heights...'
        for height in self._initial_heights:
            initial_position, initial_velocity = ball_trajectory_candidate.compute_state(height, self._fps)
            on_ground = False
            self._add_active_ball_from_candidate_and_state(ball_trajectory_candidate, frame_index,
                                                           initial_position, initial_velocity, on_ground)
    
            
        #also add a ball that is rolling on the ground
        ball_radius = self._soccer_field.get_soccer_ball_radius()
        initial_position, initial_velocity = ball_trajectory_candidate.compute_state(ball_radius, self._fps)
        on_ground = True
        self._add_active_ball_from_candidate_and_state(ball_trajectory_candidate, frame_index,
                                                       initial_position, initial_velocity, on_ground)
                                                       
        
        return    
    
    def _update(self, frame_index, frame_data):
        '''
        frame_index - the index of the frame we want to update to
        frame_data - a FrameData object which contains the data of the next frame.

        Update to the next timestep.
         '''

        print '--------------------------------------'
        print 'Updating to frame %d' % (frame_index)
        print '--------------------------------------'

        #update the frame data so that it contains the data of the frame that we are updating to.
        #note that this changes _current_frame_data, and so it is seen by all of the tracked players since they store a
        #reference to _current_frame_data
        self._current_frame_data.update_from_frame_data(frame_data)

        print 'summary of frame we are updating to: '
        print '   ball trajectory candidates: '
        print self._current_frame_data.get_ball_trajectory_candidates()
        
        # cv2.imshow('image', self._current_frame_data.get_image())
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()
        
        #update the state vectors of the particle filters
        self._update_particle_filters()
        
        #since the positions of the players have changed, some players may have become degenerate.
        #for example, this includes players that have gone off the field, or that have gone off the screen.
        self._deactivate_degenerate_balls()

        #also, we have to recompute the probabilities and weights
        self._compute_probabilities_and_weights(frame_index=frame_index)

        #after updating the ball positions and probabilities, the expected ball data has to be refreshed.
        #the ExpectedBallData objects record the expected positions of the balls, together with the variance.
        self._compute_expected_ball_data()
        
        #evaluate how well the players are doing and remove the badly performing ones from _active_balls
        self._evaluate_balls()

        #find which of the ball trajectory candidates in this frame do not match any players
        ball_trajectory_candidates = self._current_frame_data.get_ball_trajectory_candidates()            
        isolated_candidates = self._find_isolated_trajectory_candidates(ball_trajectory_candidates)

        #before adding new active balls, display a summary of the state of the tracker
        self._generate_debugging_data(frame_index)
        
        #try to turn the isolated candidates into active balls
        for btc in isolated_candidates:
            self._add_active_balls_from_candidate(btc, frame_index)
        
        return

    def _build_ball_trajectory_from_height_group(self, ball_candidate_id, height_group):
        '''
        ball_candidate_id - the id of the height group
        height_group - a list of TrackedBall objects

        return a BallTrajectory object which represents the best tracked ball in the height group
        '''
        print 'building a ball trajectory out of height group %d.' % ball_candidate_id
        
        #compute the tracking score of all of the balls in the height group
        for tracked_ball in height_group:
            tracked_ball.compute_tracking_score()

        print '   the tracking scores are: ', [tb.get_tracking_score() for tb in height_group]
        print '   the number of frames:    ', [tb.get_number_of_frames() for tb in height_group]
        
        #get the tracked ball that has managed to track for the most frames.
        #in the case of a tie, use compare tracked balls tracking score
        best_tracked_ball = max(height_group, key=lambda x: (x.get_number_of_frames(), x.get_tracking_score()))

        start_frame = best_tracked_ball.get_start_frame()
        positions = best_tracked_ball.get_positions()
        image_positions = best_tracked_ball.get_image_positions()
        on_ground = best_tracked_ball.on_ground()
        ball_trajectory = BallTrajectory(ball_candidate_id, start_frame, positions, image_positions, on_ground)

        print '   returning trajectory: '
        print ball_trajectory
        
        return ball_trajectory
    
    def _build_tracked_balls(self):
        '''
        populate the dictionary _ball_trajectory_dict
        '''
        ball_trajectory_dict = dict()
        #use each height group to create a ball trajectory
        for ball_candidate_id, height_group in self._height_groups.iteritems():
            ball_trajectory = self._build_ball_trajectory_from_height_group(ball_candidate_id, height_group)
            ball_trajectory_dict[ball_candidate_id] = ball_trajectory

        self._tracked_balls = TrackedBalls(ball_trajectory_dict)
        
        return
    
    def track_balls(self):
        '''
        Track the balls in the given frames.
        '''
        next_frame_index = 0
        next_frame_data = next(self._frame_data_iterator, None)
        self._current_frame_data = next_frame_data
        
        while next_frame_data is not None:
            #DEBUGGING HACK: remove all the player candidates except for one of them in the first frame
            # if next_frame_index > 10:
            #     next_frame_index += 1
            #     next_frame_data = next(self._frame_data_iterator, None)
            #     continue
            
            # if next_frame_index == 131:
            #     next_frame_data._ball_trajectory_candidates = next_frame_data._ball_trajectory_candidates[:1]
            # else:
            #     next_frame_data._ball_trajectory_candidates = []
                
            self._update(next_frame_index, next_frame_data)

            next_frame_index += 1
            next_frame_data = next(self._frame_data_iterator, None)

        self._number_of_tracked_frames = next_frame_index
        
        #after tracking the balls, select the best tracked ball in each height group and use it to make a TrackedBalls
        self._build_tracked_balls()
        
        return

    def get_tracked_balls(self):
        '''
        return a dictionary with items (ball candidate id, a BallTrajectory object that was initialized using that candidate)
        '''

        return self._tracked_balls

    def get_number_of_tracked_frames(self):
        '''
        return the number of frames that were processed by the ball tracker
        '''
        return self._number_of_tracked_frames
