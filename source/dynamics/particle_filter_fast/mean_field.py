import numpy as np
import random
import collections

'''
ParticleFilterGraph - this stores an undirected graph of ParticleFilter objects. It is used as input to the MeanField.
vertices - a dictionary whose values are ParticleFilter obejcts
neighbors - a dictionary with items: (k, l1,...,ln) where:
            k: a key of a particle filter
            l1,...,ln: keys of particle filters that are neighbors of particle_filter[k].
'''
ParticleFilterGraph = collections.namedtuple('ParticleFilterGraph', ['vertices', 'neighbors'])

class MeanField(object):
    '''
    This provides an implementation of mean field monte carlo.

    It takes as input a list of particle filters (vertices), and a list of pairs (i,j) (edges). 
    It then approximates the joint probabilities of the particle filters (w.r.t the edges) and updates the weights of the particles they contain.
    '''

    def __init__(self, tolerance, max_iterations=5, visualizer=None):
        '''
        particle_filters - a list of particle filters
        tolerance - a positive float. This determines how small the changes in state have to be before we declare the mean field to have converged.
        visualizer - a PlayerTrackerVisualizer object that is used for generating debugging data
        '''

        #a ParticleFilterGraph
        self._particle_filter_graph = None
        
        self._tolerance = tolerance
        self._max_iterations = max_iterations
        
        self._previous_expected_states = None
        self._current_expected_states = None
        self._current_iteration = 0

        self._visualizer = visualizer
        
    def set_particle_filter_graph(self, particle_filter_graph):
        '''
        particle_filter_graph - a ParticleFilterGraph
        '''
        self._particle_filter_graph = particle_filter_graph
        return
    
    def _initialize_approximation_computation(self):
        self._current_iteration = 0
        self._previous_expected_states = None
        self._current_expected_states = None
        
    def _initialize_iteration(self):            
        #update the list of previous expected state vectors
        if self._current_expected_states is None:
            print 'there are %d particle filters' % len(self._particle_filter_graph.vertices)
            self._previous_expected_states = np.vstack([pf.get_expected_state_vector()
                                                        for pf in self._particle_filter_graph.vertices.itervalues()])
            
            print 'initial expected states: ', self._previous_expected_states
            
        else:
            self._previous_expected_states = self._current_expected_states
        
    def _update(self, frame_index=None):
        '''
        For each particle filter, we update it's probability using the expected positions of it's neighbors
        '''

        #update joint probabilities
        for particle_filter_id, particle_filter in self._particle_filter_graph.vertices.iteritems():
            neighbor_ids = self._particle_filter_graph.neighbors[particle_filter_id]
            print 'updating the distribution of particle filter: ', particle_filter_id
            print 'the neighbors of this particle filter are: ', neighbor_ids
            neighboring_particle_filters = [self._particle_filter_graph.vertices[neighbor_id] for neighbor_id in neighbor_ids]
            particle_filter.compute_joint_log_probabilities(neighboring_particle_filters)

            # self._visualizer.generate_mean_field(frame_index, self._current_iteration,
            #                                      particle_filter_id, particle_filter,
            #                                      neighbor_ids, neighboring_particle_filters)

        #now that the probabilities have changed, we must update the weights
        for particle_filter in self._particle_filter_graph.vertices.itervalues():
            particle_filter.compute_particle_weights()

        #since the weights have changed, we must update the expected states
        self._current_expected_states = np.vstack([pf.get_expected_state_vector()
                                                   for pf in self._particle_filter_graph.vertices.itervalues()])

    def _check_convergence(self):
        '''
        Return True if the mean field equations have converged.
        
        We currently do this by computing the maximum of: (norm of change in expected state) where we range over all particle filters.
        Return true if this number is below a predefined tolerence
        '''

        print 'checking for convergence...'
        print 'previous states:'
        print self._previous_expected_states
        print 'current states:'
        print self._current_expected_states
        
        state_difference_norms = np.linalg.norm(self._current_expected_states - self._previous_expected_states, axis=1)
        max_difference = state_difference_norms.max()

        print 'max difference: ', max_difference
        
        return max_difference < self._tolerance

    def approximate_joint_probabilities(self, frame_index=None):
        '''
        frame_index - an integer. This is used only for debugging purposes.
        '''

        print 'starting mean field computation...'
        self._initialize_approximation_computation()

        while self._current_iteration < self._max_iterations:
            print 'iteration: ', self._current_iteration
            
            self._initialize_iteration()
            self._update(frame_index)

            #if we have already converged, terminate the algorithm
            if self._check_convergence():
                return

            self._current_iteration += 1
            
        return
        
        
    
