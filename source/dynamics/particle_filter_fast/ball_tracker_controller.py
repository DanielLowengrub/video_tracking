import os
import pickle
import cv2
import re
import numpy as np
import itertools
from .ball_tracker import BallTracker
from .tracked_balls import TrackedBalls
from .frame_data import FrameData
from ...preprocessing import file_sequence_loader
from ...auxillary import iterator_operations

class BallTrackerController(object):
    '''
    This is in charge of loading preprocessing data, feeding it into a BallTracker, and saving the output.
    '''

    INTERVAL_FILE = 'tracked_balls_%d_%d.pckl'
    INTERVAL_FILE_REGEX = re.compile('tracked_balls_([0-9]+)_([0-9]+)\.pckl')

    def __init__(self, soccer_field, fps, trajectory_match_thresh, min_trajectory_distance,
                 num_particles, initial_velocity_stdev, initial_heights_yards,
                 max_bad_frames, max_position_variance_air, max_position_variance_ground,
                 ground_threshold, evaluation_threshold):
        '''
        soccer_field - a SoccerFieldGeometry object
        fps - the frame rate of the images we are tracking the ball in.
        trajectory_match_thresh - if the distance between two points on an image is less than theis then we assume that they
                                  come from the same ball
        min_trajectory_distance - throw out trajectories that covered less than this amount of distance.
        num_particles - the number of particles used to track each ball.
        initial_velocity_stdev - the standard deviation used to sample initial velocities for a ball trajectory.
        initial_heights_yards - the heights (in yards) that we try to initialize a ball trajectory to
        max_bad_frames - deactivate a tracked ball if it fails for this number of frames
        max_position_variance_air - a tracked ball that is moving through the air is declared to be "failed" 
                                if it's position variance exceeds this number
        max_position_variance_ground - a tracked ball that is moving on the ground is declared to be "failed" 
                                if it's position variance exceeds this number
        ground_threshold - a tracked ball is declared "failed" if it is at least this much below the ground
        evaluation_threshold - a tracked ball is declared "failed" if it's evaluation is LARGER than this
        '''

        self._soccer_field = soccer_field
        self._fps = fps
        self._trajectory_match_thresh = trajectory_match_thresh
        self._min_trajectory_distance = min_trajectory_distance
        self._num_particles = num_particles
        self._initial_velocity_stdev = initial_velocity_stdev
        self._initial_heights_yards = initial_heights_yards
        self._max_bad_frames = max_bad_frames
        self._max_position_variance_air = max_position_variance_air
        self._max_position_variance_ground = max_position_variance_ground
        self._ground_threshold = ground_threshold
        self._evaluation_threshold = evaluation_threshold

    @classmethod
    def save_tracked_balls_in_interval(cls, interval, tracked_balls, output_directory):
        '''
        interval - a tuple of integers (i,j)
        tracked_balls - a TrackedBalls object
        output_directory - a directory path

        save the tracked balls in output_directory/tracked_balls_i_j.pckl
        '''

        output_file = os.path.join(output_directory, cls.INTERVAL_FILE % interval)
        
        print 'saving the tracked balls in file: %s' % output_file
        f = open(output_file, 'wb')
        pickle.dump(tracked_balls, f)
        f.close()

        return

    @classmethod
    def save_tracked_balls(cls, indexed_tracked_balls, output_directory):
        '''
        indexed_tracked_balls - an iterator of tuples ((ik,jk), a TrackedBalls object)
        output_directory - a directory name

        save the tracked balls in the output directory.
        '''
        for interval, tracked_balls in indexed_tracked_balls:
            cls.save_tracked_balls_in_interval(interval, tracked_balls, output_directory)

        return

    
    @classmethod
    def _load_tracked_balls_file(cls, ball_trajectories_file):
        '''
        ball_trajectories_file - a file name. This is a pickle file that stores a dictionary of BallTrajectory objects
        '''
        f = open(ball_trajectories_file, 'rb')
        ball_trajectories = pickle.load(f)
        f.close()

        return ball_trajectories
    
    @classmethod
    def load_tracked_balls(cls, tracked_balls_directory):
        '''
        tracked_balls_directory - a directory with files tracked_balls_ik_jk.pckl. 
                                      each file contains a TrackedBalls object which corresond to ball trajectories
                                      in the frame interval (ik,jk)

        return - an iterator of tuples ((ik,jk), tracked_balls_ik_jk) where tracked_balls_ik_jk is a TrackedBalls object
        '''
        
        interval_files = file_sequence_loader.get_sorted_file_names(tracked_balls_directory, cls.INTERVAL_FILE_REGEX)

        for interval_file in interval_files:
            interval_basename = os.path.basename(interval_file)
            interval = tuple(map(int,cls.INTERVAL_FILE_REGEX.match(interval_basename).groups()))
            print 'loading tracked balls from: ', interval_file
            
            tracked_balls = cls._load_tracked_balls_file(interval_file)
            
            yield (interval, tracked_balls)

    def _set_reversed_frame_data_btcs(self, frame_data_iterator, ball_trajectory_dict):
        '''
        frame_data_iterator - an iterator of FrameData objects
        ball_trajectory_dict - a dictionary with items (ball trajectory id, BallTrajectory object)

        return - an iterator of FrameData objects

        For each FrameData object, in frame_data_iterator, do the following:
        let btcs be the ball trajectory candidates in the frame data. We only keep the ones whose id is a key in the
        ball trajectory dict. Let btc be such a ball trajectory candidate and let bt be the corresponding ball trajectory.
        We set the default state of btc using the height of bt in it's first frame.
        '''

        for frame_data in frame_data_iterator:
            print 'reversing ball trajectory candidates...'
            print '   old candidates: ', frame_data.get_ball_trajectory_candidates()
            reversed_ball_trajectory_candidates = []
            for btc in frame_data.get_ball_trajectory_candidates():
                ball_id = btc.get_id()
                
                #if the ball trajectory candidate's id does not appear in the ball trajectory dictionary, skip it
                if not (ball_id in ball_trajectory_dict):
                    continue

                #if it is, use the corresponding ball trajectory to set the candidates default state
                ball_trajectory = ball_trajectory_dict[ball_id]
                print '   reversing candidate %d with trajectory: %s' % (ball_id, ball_trajectory)
                height = ball_trajectory.positions[0][2]
                on_ground = ball_trajectory.on_ground
                reverse_velocity = True
                btc.set_default_state(height, self._fps, on_ground, reverse_velocity)

                reversed_ball_trajectory_candidates.append(btc)

            print '   reversed candidates: ', reversed_ball_trajectory_candidates
            frame_data.set_ball_trajectory_candidates(reversed_ball_trajectory_candidates)

            yield frame_data
                
    def _track_balls_in_interval(self, frame_data, reversed_frame_data):
        '''
        frame_data - an iterator of FrameData objects.
        reversed_frame_data - an iterator of FrameData objects.

        If frame_data stores the data of the frames: (frame_0,...,frame_N) then reversed_frame_data stores the data of the
        intervals (frame_N,...,frame_0)

        return a TrackedBalls object which represents the balls that we tracked in frames 0 to N.
        '''

        #first track the balls forward in time
        ball_tracker = BallTracker(self._soccer_field, frame_data, self._fps, self._num_particles,
                                   self._initial_velocity_stdev, self._initial_heights_yards,
                                   self._max_bad_frames, self._max_position_variance_air,
                                   self._max_position_variance_ground, self._ground_threshold, self._evaluation_threshold)

        ball_tracker.track_balls()
        
        #store the output of the tracking in the forward direction
        forward_tracked_balls = ball_tracker.get_tracked_balls()

        #only keep the ball trajectories in the reversed frame data whose ids appear in the trajectories in
        #forward_tracked_balls. This is because we want to track backwards in time only the trajectory candidates that
        #were successfully tracked forwards in time. This will allow us to glue the backwards trajectories to the forwards ones
        ball_trajectory_dict = forward_tracked_balls.get_ball_trajectory_dict()
        reversed_frame_data = self._set_reversed_frame_data_btcs(reversed_frame_data, ball_trajectory_dict)

        #track the balls in reverse, using only the reverse ball trajectory candidate dictionary
        ball_tracker = BallTracker(self._soccer_field, reversed_frame_data, self._fps, self._num_particles,
                                   self._initial_velocity_stdev, self._initial_heights_yards,
                                   self._max_bad_frames, self._max_position_variance_air,
                                   self._max_position_variance_ground, self._ground_threshold, self._evaluation_threshold)

        ball_tracker.track_balls()

        #store the output of the backwards tracking. Before saving it, reverse the trajectories so that they go forwards
        #in time
        last_frame_index = ball_tracker.get_number_of_tracked_frames() - 1
        backward_tracked_balls = ball_tracker.get_tracked_balls().get_reversed_tracked_balls(last_frame_index)

        #join the forward and backward TrackedBall objects into one TrackedBalls object
        tracked_balls = TrackedBalls.join_forward_and_backward(forward_tracked_balls, backward_tracked_balls,
                                                               self._trajectory_match_thresh)

        #remove the degenerate ball trajectories
        tracked_balls.remove_degenerate_trajectories(self._min_trajectory_distance)
        
        return tracked_balls
        
    def track_balls(self, image_directory, annotation_directory, camera_directory, players_directory, team_labels_directory,
                    balls_directory, output_directory):
        '''
        image_directory - a directory with files image_0.png,...,image_N.png
        annotation_directory - a directory with files annotation_0.png,...,annotation_N.png
        camera_directory - a directory with subdirs cameras_ik_jk/. each subdir has files: camera_0.npy,...,camera_{jk-ik}.npy
        players_directory - a directory with subdirs players_ik_jk/. each subdir has directories: 
                            players_0/,...,players_{jk-ik}/
        team_labels_directory - a directory with subdirs team_labels_ik_jk/. each subdir has files 
                                team_labels_0.npy,...,team_labels_{jk-ik}.npy
        balls_directory - a directory with subdirs ball_trajectories_ik_jk/. 
                          each subdir has files: ball_trajectories_0.pckl,...,ball_trajectories_{jk-ik}.pckl
        
        Track the balls through the frames and save the resulting trajectories in output_file.
        '''

        #this is an iterator of tuples of the form (interval, iterator of FrameData objects in interval)
        indexed_frame_data = FrameData.load_frame_datas(image_directory, annotation_directory, camera_directory,
                                                        players_directory, team_labels_directory, balls_directory,
                                                        reverse=False)

        #this is an iterator of tuples of the form (interval, iterator of FrameData objects in interval), but in each interval
        #the frame data iterator stores the frames in the reverse order.
        indexed_reversed_frame_data = FrameData.load_frame_datas(image_directory, annotation_directory, camera_directory,
                                                                 players_directory, team_labels_directory, balls_directory,
                                                                 reverse=True)

        #strip the intervals from the frame data
        intervals, frame_data = iterator_operations.iunzip(indexed_frame_data)
        reversed_frame_data = (reversed_fd_in_interval for interval,reversed_fd_in_interval in indexed_reversed_frame_data)

        fd_and_reversed_fd = itertools.izip(frame_data, reversed_frame_data)
        indexed_fd_and_reversed_fd = itertools.izip(intervals, fd_and_reversed_fd)
        
        for interval, (fd_in_interval, reversed_fd_in_interval) in indexed_fd_and_reversed_fd:
            print 'tracking the balls in interval: ', interval
            tracked_balls = self._track_balls_in_interval(fd_in_interval, reversed_fd_in_interval)
            self.save_tracked_balls_in_interval(interval, tracked_balls, output_directory) 
            
        return
    
