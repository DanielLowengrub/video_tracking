import numpy as np
import cv2
from local_player_data_small import LocalPlayerDataSmall
from ...auxillary import world_to_image_projections

class ExpectedPlayerData(object):
    '''
    This class stores information that is used to represent the expected state of a player, together with the variance.
    It is stored by a TrackedPlayer, and after the tracked player updates itself, it records its new state in an
    ExpectedPlayerData object, together with auxillary information that depends on the expected player state.

    It is similar to LocalPlayerData, except that instead of storing many particles, it stores only one "particle",
    and instead records information related to the variance. This is sort of a summary of LocalPlayerData object.

    It is mainly used to:
    1) check whether two players overlap
    2) check whether a player matches a PlayerCandidate
    3) determine whether to deactivate a player
    '''

    MATCHES_CANDIDATE_THRESHOLD = 0.5 #we say that a candidate with position p matches this player data object if
                                      #f(p; E,sigma) > thresh, where E is the player position and sigma is the position std.
                                      #f is the probability density function of N(E,sigma)
                                      #setting it to 0.5 means we allow a little more than one standard deviation.

    CONTOUR_IN_VAR_THRESH = 0.9 #we declare a contour to be contained in the variance mask if at least this percentage
                                #of it is contained in the obstruction mask

    PM_IN_CONTOUR_THRESH = 0.4 #we declare a player mask to be contained in a contour if at least
                               #this percentage of it is contained in the contour mask

    MIN_CONTOUR_AREA = 5 #if a contours area is less than this, it is assumed to have 0 area.
    
    MIN_VARIANCE = np.array([5,5], np.float32)**2
    
    def __init__(self, frame_data, position, position_variance,
                 player_rectangle, player_mask, foreground_mask,
                 variance_rectangle, variance_mask):
        '''
        frame_data - a PaddedFrameData object
        position - a numpy array with length 2
        position_variance - a numpy array with length 2
        player_rectangle - a Rectangle object
        variance_rectangle - a Rectangle object
        player_mask - a numpy array with the same shape as player_rectangle and type np.bool
        variance_mask - a numpy array with the same shape as variance_rectangle and type np.bool
        '''
        self._frame_data = frame_data
        self._position = position
        self._position_variance = position_variance
        self._player_rectangle = player_rectangle
        self._player_mask = player_mask
        self._foreground_mask = foreground_mask
        self._variance_rectangle = variance_rectangle
        self._variance_mask = variance_mask

        self._obstruction_mask = None
        
    @classmethod
    def from_frame_data_and_position(cls, soccer_field_rectangle, frame_data, position, position_variance):
        '''
        frame_data - a FrameData object
        position - a numpy array with length 2
        position_variance - a numpy array with length 2

        Let C be a cylinder positioned at the given position, whose base is an ellipse determined by the variance, and
        whose height is the average player height.
        We use the camera to project C onto the image and use this for the player_mask.
        The player rectangle is the AABB of the projection of C.
        '''
        #build a local player data object and extract the player mask, fg mask and player rectangle
        lpd = LocalPlayerDataSmall.from_frame_and_positions(soccer_field_rectangle, frame_data, position.reshape((-1,2)))

        if lpd is None:
            return None
        
        player_rectangle = lpd.get_player_rectangle()
        player_mask = lpd.get_player_mask()
        foreground_mask = lpd.get_foreground_mask()
                              
        image = frame_data.get_image()
        camera_matrix = frame_data.get_camera_matrix()
        variance_mask, variance_rectangle = world_to_image_projections.project_player_with_variance(image, camera_matrix,
                                                                                                    position,
                                                                                                    position_variance)

        return cls(frame_data, position, position_variance, player_rectangle, player_mask, foreground_mask,
                   variance_rectangle, variance_mask)

    @classmethod
    def build_obstructions(cls, expected_player_datas):
        '''
        expected_player_datas - a list of ExpectedPlayerData objects which all share the same frame_data object

        update the obstruction masks of each of the ExpectedPlayerData objects so that if player A is on front of player B,
        then player A's player mask is added to player B's obstruction mask
        '''
        for epd in expected_player_datas:
            epd._initialize_obstruction_mask()

        #first sort the player data objects by their distance to the camera, from close to far
        expected_player_datas.sort(key=lambda x: x._sq_distance_to_camera())

        #iterate over pairs (player A, player B) where player A is closer to the camera than player B
        for i,player_close in enumerate(expected_player_datas):
            for player_far in expected_player_datas[i+1:]:
                player_far._add_obstruction(player_close)

        return
    
    def _initialize_obstruction_mask(self):
        self._obstruction_mask = np.zeros(self._player_mask.shape, dtype=np.bool)
        
    def _sq_distance_to_camera(self):
        '''
        return the sq distance of _position to the camera
        '''
        camera_matrix = self._frame_data.get_camera_matrix()
        return camera_matrix.compute_sq_distance_to_camera(self._position.reshape((1,2)))[0]

    def _add_obstruction(self, other):
        '''
        other - an ExpectedPlayerData object

        add the other data's player mask to this data's obstruction mask
        '''
        self._player_rectangle.transfer_slice(self._obstruction_mask, other._player_rectangle, other._player_mask)
        return

    def get_player_mask(self):
        return self._player_mask

    def get_player_rectangle(self):
        return self._player_rectangle
    
    def has_overlap(self, other):
        '''
        other - an ExpectedPlayerData object

        return True iff the variance masks of the two objects overlap
        '''
        #print 'checking overlap...'
        
        #get the intersection of variance rectangles
        intersection = self._variance_rectangle.intersection(other._variance_rectangle)

        if intersection is None:
            return False
        
        # print '   this rectangle: ', self._variance_rectangle
        # print '   other rectangle: ', other._variance_rectangle
        # print '   intersection: ', intersection
        
        #get the part of the variance masks that lies in the intersection region
        this_mask = self._variance_rectangle.get_relative_slice(self._variance_mask, intersection)
        other_mask = other._variance_rectangle.get_relative_slice(other._variance_mask, intersection)

        return (this_mask * other_mask).sum() > 0

    def _probability_of_position(self, p):
        '''
        return the probability of the position p. 
        We use a gaussian distribution with mean _position and std _position_variance
        
        I.e, we return e^(-0.5 * ((p - _position)**2 / variance).sum())
        '''
        variance = np.maximum(self._position_variance, self.MIN_VARIANCE)
        return np.exp(-0.5 * ((p - self._position)**2 / variance).sum())

    def _contour_in_variance_mask_ratio(self, contour):
        '''
        conotur - a Contour object representing a contour on the soccer camera image.

        Return a number between 0 and 1. 0 means that the contour does not meet the variance mask,
        and 1 means that it is completely contained.
        '''
        #print 'variance rectangle: ', self._variance_rectangle
        
        #since the contour is on the global image, we first draw it on an image that is the size of the variance rectangle.
        contour_mask = contour.get_mask_in_rectangle(self._variance_rectangle)

        # cv2.imshow('variance mask', np.float32(self._variance_mask))
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        # cv2.imshow('contour mask', np.float32(contour_mask))
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        #now check how much of the contour mask is in the variance mask
        contour_area = float(contour_mask.sum())

        #if the contour area is too small, then return zero
        if contour_area < self.MIN_CONTOUR_AREA:
            #print 'no overlap'
            return 0

        overlap_area = float((contour_mask * self._variance_mask).sum())

        #print 'overlap area: ', overlap_area
        
        return overlap_area / contour_area

    def _player_mask_in_contour_ratio(self, contour):
        '''
        conotur - a Contour object representing a contour on the soccer camera image.

        Return a number between 0 and 1. 0 means that the contour does not meet the player mask,
        and 1 means that the player mask is contained in the contour.
        '''
        #since the contour is on the global image, we first draw it on an image that is the size of the player rectangle.
        contour_mask = contour.get_mask_in_rectangle(self._player_rectangle)

        # if int(self._position[0]) == 493:
        #     cv2.imshow('player mask', np.float32(self._player_mask))
        #     cv2.waitKey(0)
        #     cv2.destroyAllWindows()
            
        #     cv2.imshow('contour mask', np.float32(contour_mask))
        #     cv2.waitKey(0)
        #     cv2.destroyAllWindows()

        #check how much of the player mask is contained in the contour mask
        player_mask_area = float(self._player_mask.sum())
        overlap_area = float((self._player_mask * contour_mask).sum())

        return overlap_area / player_mask_area
    
    def matches_player_candidate(self, player_candidate):
        '''
        player_candidate - a PlayerCandidate object

        return True iff the position and contour of the player candidate are a good match for the position and player mask
        in this object.

        For the moment we only check if the player candidate's position is within _position_variance of _position
        '''
        # print '   matching player with position %s to candidate with position %s' % (self._position,
        #                                                                             player_candidate.absolute_position)

        contour_in_var_ratio = self._contour_in_variance_mask_ratio(player_candidate.contour)
        pm_in_contour_ratio = self._player_mask_in_contour_ratio(player_candidate.contour)
        
        # print '   contour in var ratio: %f, pm in contour ratio: %f' % (contour_in_var_ratio, pm_in_contour_ratio)
        
        has_match = (contour_in_var_ratio > self.CONTOUR_IN_VAR_THRESH) and (pm_in_contour_ratio > self.PM_IN_CONTOUR_THRESH)

        # print '   has match: ', has_match

        return has_match

    def get_unobstructed_foreground_percentage(self):
        '''
        return a number between 0 and 1. 
        It records the percentage of the player mask that is in the foreground and is unobstructed.
        '''
        #print 'computing ubobs fg percentage...'

        # cv2.imshow('player mask', np.float32(self._player_mask))
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        # cv2.imshow('fg mask', np.float32(self._foreground_mask))
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        # cv2.imshow('obs mask', np.float32(self._obstruction_mask))
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        unobs_fg_mask = self._player_mask * self._foreground_mask 

        if self._obstruction_mask is not None:
            unobs_fg_mask *= np.logical_not(self._obstruction_mask) #NEW
            #unobs_fg_mask *= 1 - self._obstruction_mask
            
        # cv2.imshow('unobs fg mask', np.float32(unobs_fg_mask))
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        total_area = float(self._player_mask.sum())
        unobs_fg_area = float(unobs_fg_mask.sum())

        return unobs_fg_area / total_area
