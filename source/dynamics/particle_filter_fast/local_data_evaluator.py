import numpy as np
import itertools

class LocalDataEvaluator(object):
    '''
    The goal of this class is to compute the probability of a LocalPlayerData object.
    Recall that the LocalPlayerData contains information about the image in the neighborhood of the approximated positions 
    of a player.
    This class uses that data to compute the probability of the player being in each of those positions.
    '''

    MIN_AREA = 1 #we only compute probabilities for regions whose area is bigger than this. Otherwise we do not have enough
                 #data

    #NEW
    def _compute_obstacle_penalty(self, obstacle_distances):
        '''
        obstacle_distances - a numpy array with shape (number of particles, number of obstacles)
        it records the distances between each particle and each obstacle.

        Return a numpy array of POSITIVE floats with lengh (number of particles).
        It represents the penalty that should be subtracted from the log probability of each particle, according to how
        close it is to the obstacle.
        '''

        #to a distance x we associate a penalty 1 / 2*(1 + x)
        penalty = 1.0 / (1.0 * (1 + obstacle_distances)) #USED TO BE 1.0 / (2.0 * ( ... ))

        #the penalty for each particle is the sum of the penalties associated to each obstruction
        penalty = penalty.sum(axis=1)

        return penalty
    
    def compute_log_probabilities(self, local_player_data):
        '''
        local_player_data - a LocalPlayerData object

        Return a numpy array with length N, where N = local_player_data.number_of_particles()

        Let F be the foreground, B the background and P the player mask. We return:
        area(F \cap P) - area(B \cap P)

        The point is that we want to reward player pixels that are in the fg, and penalize player pixels in the bg.
        However, we ignore pixels that are obstructed from the camera.
        '''
        #these are np arrays with length N
        fg_area = local_player_data.get_unobstructed_foreground_player_mask_areas()
        bg_area = local_player_data.get_unobstructed_background_player_mask_areas()

        #compute the largest unobstructed area among the particles
        total_area = np.float32(fg_area + bg_area)
        max_area = total_area.max()               
        
        #if the maximal area is too small
        if max_area < self.MIN_AREA:
            raise RuntimeError('all of the particles have no unobstructed area!')
        
        #log_probability = np.float32(fg_area - bg_area)
        log_probability = np.float32(fg_area - bg_area) / max_area

        #subtract the penalty associated to the obstacle distances
        obstacle_penalty = self._compute_obstacle_penalty(local_player_data.get_obstacle_distances())

        print 'distances: '
        print local_player_data.get_obstacle_distances()[:5]
        print 'obstacle penalty:'
        print obstacle_penalty[:5]
        print 'current log prob:'
        print log_probability[:5]
        
        log_probability -= obstacle_penalty
        
        return log_probability

    def compute_joint_log_probabilities(self, local_player_datas):
        '''
        local_player_datas - a list of LocalPlayerData objects which all have the same number of particles

        Return a numpy array with length N, where N = local_player_datas[0].number_of_particles()
        This represents the joint log probability of the particles.    
        '''
        #first we have to reset the obstruction masks in each of the player datas
        for lpd in local_player_datas:
            lpd.initialize_obstruction_masks()
            lpd.initialize_obstacle_distances()
            
        #for each pair of players, update their obstruction masks by how much they obstruct one another
        for pd_A,pd_B in itertools.combinations(local_player_datas, 2):
            pd_A.add_obstructions(pd_B)
            pd_A.add_obstacle_distances(pd_B)
            pd_B.add_obstructions(pd_A)
            pd_B.add_obstacle_distances(pd_A)

        #now each player knows which parts of it are obstructed by other players.
        #so we compute the log probs of each player individually and take the sum
        return sum(self.compute_log_probabilities(lpd) for lpd in local_player_datas)
