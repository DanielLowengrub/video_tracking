import numpy as np
import cv2
from ...auxillary import planar_geometry, world_to_image_projections

class ExpectedBallData(object):
    '''
    This class stores information that is used to represent the expected state of a ball, together with the variance.
    It is stored by a TrackedBall, and after the tracked ball updates itself, it records its new state in an
    ExpectedBallData object, together with auxillary information that depends on the expected ball state.

    It is similar to LocalBallData, except that instead of storing many particles, it stores only one "particle",
    and instead records information related to the variance. This is sort of a summary of LocalBallData object.

    It is mainly used to:
    1) check whether a ball matches a BallCandidate
    3) determine whether to deactivate a ball
    '''
    MIN_VARIANCE = 3
    
    def __init__(self, variance_rectangle, variance_mask, ball_evaluation):
        '''
        variance_rectangle - a Rectangle object
        variance_mask - a numpy array with the same shape as variance_rectangle and type np.bool
        ball_evaluation - a float. The smaller the float the closer the position is to being a ball
        '''
        self._variance_rectangle = variance_rectangle
        self._variance_mask = variance_mask
        self._ball_evaluation = ball_evaluation
        
    @classmethod
    def from_frame_data_and_position(cls, soccer_field, frame_data, ball_position, ball_position_variance, ball_evaluation):
        '''
        frame_data - a FrameData object
        ball_position - a numpy array with length 3
        ball_position_variance - a numpy array with length 3
        ball_evaluation - a float. The smaller the float the closer the position is to being a ball
        return - an ExpectedBallObject

        Let E be the ellipsoid centeres at the ball position  with axes: ball_radius + sqrt(position_variance)
        The returned expected ball data has a variance mask that is equal to the projection of E to the image.
        '''
        camera_matrix = frame_data.get_camera_matrix()

        ball_radius = soccer_field.get_soccer_ball_radius()
        ball_position_variance = np.maximum(cls.MIN_VARIANCE, ball_position_variance)
        
        variance_ellipsoid_axes = ball_radius + ball_position_variance
        variance_ellipse = camera_matrix.project_ellipsoid_to_image(ball_position, variance_ellipsoid_axes)
        C = variance_ellipse.get_parametric_representation()
        variance_rectangle, variance_mask = planar_geometry.build_ellipse_mask(C)
        
        return cls(variance_rectangle, variance_mask, ball_evaluation)
    
    def matches_ball_trajectory_candidate(self, ball_trajectory_candidate):
        '''
        ball_trajectory_candidate - a BallTrajectoryCandidate object

        return True iff the position of the ball trajectory candidate is a good match for the variance mask.
        '''

        #first check if the image position of the candidate is inside the varaince rectangle
        image_position = ball_trajectory_candidate.get_image_position()
        
        if not self._variance_rectangle.contains_point(image_position):
            return False

        #if it is in the rectangle, check if it is in the variance mask.
        relative_position = self._variance_rectangle.convert_point_to_rectangle_coords(image_position)
        relative_position = np.rint(relative_position).astype(np.int32)
        in_variance_mask = self._variance_mask[relative_position[1], relative_position[0]]

        return in_variance_mask
    
    def get_evaluation(self):
        '''
        return a float. the smaller the number, the closer the expected data is to being a ball.
        '''

        return self._ball_evaluation

    def get_variance_rectangle(self):
        return self._variance_rectangle

    def get_variance_mask(self):
        return self._variance_mask
