import numpy as np
import cv2
from .player_particle_filter import PlayerParticleFilter
from .local_data_evaluator import LocalDataEvaluator
from ...preprocessing_full.team_labels_controller.team_label import TeamLabel
from ...auxillary.interval import IntervalList
from ...auxillary import np_operations

class TrackedPlayer(object):
    '''
    This represents a player that it getting tracked via a particle filter.

    In particular, it provides the following functionality:
    
    * Get the mean position of the player together with the standard deviation (using the player particles it has pointers to)
    * Check if the player matches a contour / is contained in a contour
    * keep track of the state of the player, and switch between states if necessary. The states are:
      - active
      - dormant
      - seeking
    '''

    CONTOUR_OVERLAP_THRESH = 0.1 #we declare a contour to have an overlap with the obstruction mask if at least CONTOUR_OVERLAP_THRESH amount of it is contained in the obstruction mask

    CONTOUR_CONTAINMENT_THRESH = 0.9 #we declare a contour to be contained in the obstruction mask if at least CONTOUR_OVERLAP_THRESH amount of it is contained in the obstruction mask

    CONTAINED_IN_CONTOUR_THRESH = 0.4 #we declare a player mask to be contained in a contour if at least CONTOUR_OVERLAP_THRESH amount of it is contained in the contour mask
    
    MIN_CONTOUR_AREA = 5 #if a contours area is less than this, it is assumed to have 0 area.

    MAX_BAD_ACTIVE_FRAMES = 10 #if an active tracker has failed for this many frames in a row, deactive it
    MAX_BAD_SEEKING_FRAMES = 1 #if a seeking tracker has failed for this many frames in a row, deactive it
    NUM_SEEKING_FRAMES = 10 #if a seeking tracker has been seeking for this number of frames, make it an active player
    
    MAX_POSITION_VARIANCE = 25
    MIN_UNOBS_FG_PERCENTAGE = 0.4

    MAX_OTHER_TEAM_RATIO = 0.1 #we only declare a player to be on team A if the player was labelled as team B less than
                               #this percentage of the time. Same for B swapped with A.
    MAX_UNDETERMINED_RATIO = 0.85 #we only declare a player to be on team A or B if the player was labelled
                                  #"undetermined" for less than this percentage of the time.
                                       
    #These are the states a player can be in
    STATE_NAMES = ['ACTIVE', 'DORMANT', 'SEEKING']
    ACTIVE = 0 #we are successfully tracking a target
    DORMANT = 1 #not tracking anything
    SEEKING = 2 #attempting to aquire a target
    
    def __init__(self, player_id, soccer_field, frame_data, local_data_evaluator):
        '''
        player_id - an integer
        soccer_field - a SoccerFieldGeometry object
        frame_data - a PaddedFrameData object
        '''
        
        self._player_id = player_id
        self._frame_data = frame_data
        self._soccer_field = soccer_field
        self._local_data_evaluator = local_data_evaluator
        
        #NOTE: In the future, we will initialize to a DORMANT state.
        self._state = self.ACTIVE #we inialize to an active state.

        #this is a TeamLabel object
        self._team_label = None
        
        #this is a particle filter that approximates the probability distribution of the player state
        self._player_particle_filter = None
        
        #an ExpectedPlayerData object
        self._expected_player_data = None

        self._number_of_seeking_frames = None
        self._number_of_consecutive_bad_frames = None
        
        #for debugging purposes, we store all of the positions and velocities from each frame.
        #we also save the player states
        #I.e, saved_particles[i] contains all of the particles in frame i
        self._saved_particle_positions = []
        self._saved_mean_positions = []
        self._saved_position_variances = []
        #self._saved_velocities = []
        self._saved_mean_velocities = []
        self._saved_player_states = []
        self._saved_team_labels = []
        
    @classmethod
    def active_id(cls):
        return cls.ACTIVE

    @classmethod
    def seeking_id(cls):
        return cls.SEEKING

    @classmethod
    def dormant_id(cls):
        return cls.DORMANT

    @classmethod
    def all_state_ids(cls):
        return (cls.ACTIVE, cls.SEEKING, cls.DORMANT)
    
    def _reset_position(self, absolute_position):
        '''
        absolute_position - a numpy array of length 2 representing a position in absolute coordinates
        We set the player's position to absolute_position, and delete position dependent variables.
        '''
        #this sets the position of the particle filter to the given position,
        #and initializes the other particle filter variables
        self._player_particle_filter.reset(absolute_position)
        
        self._expected_player_data = None

        return
    
    def start_seeking(self, player_candidate):
        '''
        player_candidate - a PlayerCandidate object

        Reset the particle filter using the position of the player candidate
        Also initialize the state related data.
        '''
        self._reset_position(player_candidate.absolute_position)
        
        self._state = self.SEEKING
        self._number_of_seeking_frames = 0
        self._number_of_consecutive_bad_frames = 0

        return
        
    def get_player_id(self):
        return self._player_id

    def update_particle_filter(self):
        '''
        Update the state vector of the particle filter. This should only be called if the tracked player is in
        an active or seeking state.
        '''

        self._player_particle_filter.update()

        
        if not self._player_particle_filter.is_degenerate():
            lpd = self._player_particle_filter._local_player_data
            print 'num distinct rectangles: %d' % lpd._rectangles.get_number_of_rectangles()

        return

    def get_player_particle_filter(self):
        return self._player_particle_filter
    
    def save_player_data(self):
        '''
        Save various statistics of the player tracker for debugging purposes.
        '''
        #we record the state for debugging purposes
        self._saved_player_states.append(self._state)

        #if the player is dormant, there is no info to add
        if self.is_dormant():
            self._saved_mean_positions.append(np.array([np.nan, np.nan]))
            self._saved_position_variances.append(np.array([np.nan, np.nan]))
            self._saved_particle_positions.append(None)
            self._saved_mean_velocities.append(np.array([np.nan, np.nan]))
            self._saved_team_labels.append(TeamLabel.NA)
            return
        
        self._saved_mean_positions.append(self.get_expected_position())
        self._saved_position_variances.append(self.get_position_variance())
        self._saved_particle_positions.append(self._player_particle_filter._position_slice().copy())
        self._saved_mean_velocities.append(self.get_expected_velocity())
        self._saved_team_labels.append(self._team_label)
        
        return

    def refresh_expected_player_data(self):
        '''
        We compute the expected player data using the expected position and variance of the player.
        This should be done after the weights of the particle filter have been updated.
        '''

        #if the player is dormant, set the obstruction mask to None and do not do anything else
        if self.is_dormant():
            return
        
        self._expected_player_data = self._player_particle_filter.compute_expected_player_data()

        return

    def update_team_label(self, team_label):
        self._team_label = team_label

    def _get_active_and_seeking_intervals(self):
        '''
        return a list of Interval objects which indicate the frames in which the player state was active or seeking.
        '''

        states_array = np.array(self._saved_player_states)
        active_or_seeking = np.logical_or(states_array == self.ACTIVE, states_array == self.SEEKING)

        intervals = IntervalList.from_boolean_array(active_or_seeking).get_intervals()

        return intervals

    def _find_dominant_label(self, labels):
        '''
        labels - a numpy array of integers representing player labels.
        return the dominant label - either TEAM_A, TEAM_B or NA
        '''
        print '   finding dominant label in: ', labels
        
        if len(labels) == 0:
            print '   there are no labels so returning UNDETERMINED'
            return TeamLabel.UNDETERMINED

        #we now determine the dominant label by seeing which one is most common
        undetermined_ratio = (labels == TeamLabel.UNDETERMINED).sum() / float(len(labels))

        #check if UNDETERMINED occured too much
        if undetermined_ratio > self.MAX_UNDETERMINED_RATIO:
            print '   undetermined_ratio=%f so returning UNDETERMINED' % undetermined_ratio
            return TeamLabel.UNDETERMINED

        num_determined = float((labels != TeamLabel.UNDETERMINED).sum())
        team_a_ratio = (labels == TeamLabel.TEAM_A).sum() / num_determined
        team_b_ratio = (labels == TeamLabel.TEAM_B).sum() / num_determined

        #check if team A dominated
        if team_b_ratio < self.MAX_OTHER_TEAM_RATIO:
            print '   team_b_ratio=%f so team A dominated' % team_b_ratio
            return TeamLabel.TEAM_A

        #check if team B dominated
        if team_a_ratio < self.MAX_OTHER_TEAM_RATIO:
            print '   team_a_ratio=%f so team B dominated' % team_a_ratio
            return TeamLabel.TEAM_B

        #no team dominated
        print '   no team dominated so returning UNDETERMINED'
        return TeamLabel.UNDETERMINED

    def _finalize_team_labels(self):
        '''
        For each sequence of frames in which the player was seeking or active, check if one of the team labels occured more
        than MIN_TEAM_LABEL_RATIO precentage of the time. If so, set the label to be that one during these frames.
        '''

        print 'finalizing team labels of player %d...' % self.get_player_id()
        
        self._saved_team_labels = np.array(self._saved_team_labels)
        print 'labels: ', self._saved_team_labels
        
        active_and_seeking_intervals = self._get_active_and_seeking_intervals()
        print 'active and seeking intervals: ', active_and_seeking_intervals
        
        for interval in active_and_seeking_intervals:
            print '   computing label for interval: ', interval
            
            labels_in_interval = interval.get_slice(self._saved_team_labels)
            available_labels = labels_in_interval[labels_in_interval != TeamLabel.NA]
            dominant_label = self._find_dominant_label(available_labels)

            print '   dominant label: ', dominant_label
            
            #set all of the labels in this interval to the dominant one
            labels_in_interval[...] = dominant_label

        print 'the finalized labels:'
        print self._saved_team_labels
        
        return
    
    def get_expected_player_data(self):
        return self._expected_player_data
        
    def matches_player_candidate(self, player_candidate):
        '''
        player_candidate - PlayerCandidate object

        return True if this player is a good match for the player candidate. I.e, that is is probable that this player is the
        one that generated the data in the player candidate.
    
    '''
        return self._expected_player_data.matches_player_candidate(player_candidate)

    def has_overlap_with_player(self, other):
        '''
        other_player - a TrackedPlayer object

        Return true if this player overlaps with the other one in the image.
        '''
        #print 'checking overlap between player %d and player %d' % (self.get_player_id(), other.get_player_id())
        return self._expected_player_data.has_overlap(other._expected_player_data)
        
    def get_expected_position(self):
        '''
        Return the expected position in absolute coordinates based on the PlayerParticles and their weights.
        '''
        return self._player_particle_filter.get_expected_position()
        
    def get_position_variance(self):
        '''
        Return the variance of the position in absolute coordinates.
        '''
        return self._player_particle_filter.get_position_variance()

    def get_position_entropy(self):
        '''
        Return the entropy of the position on absolute coordinates
        '''
        return self._player_particle_filter.get_position_entropy()
    
    def get_expected_velocity(self):
        '''
        Return the expected velocity in absolute coordinates based on the PlayerParticles and their weights.
        '''
        return self._player_particle_filter.get_expected_velocity()

    def get_velocity_variance(self):
        return self._player_particle_filter.get_velocity_variance()
    
    def get_expected_acceleration(self):
        '''
        Return the expected position in absolute coordinates based on the PlayerParticles and their weights.
        '''
        return self._player_particle_filter.get_expected_acceleration()

    def deactivate_if_degenerate(self):
        '''
        Check if the player is degenerate. If it is, deactivate it and return true.
        If it is not, then return false.

        Note that this is not the same as checking if the tracker has lost the target.
        rather, we just check for degenerate values in the trackers parameters.
        '''
        if self._player_particle_filter.is_degenerate():
            self._state = self.DORMANT
            return True

        return False

    def is_active(self):
        return self._state == self.ACTIVE

    def is_seeking(self):
        return self._state == self.SEEKING

    def is_dormant(self):
        return self._state == self.DORMANT

    def set_state_to_active(self):
        self._state = self.ACTIVE
        
    def get_state_name(self):
        return self.STATE_NAMES[self._state]

    def get_state_id(self):
        return self._state

    def _is_successfully_tracking(self):
        '''
        Return True iff the player is succeessfully tracking an object.
        This should only be called if the player is active or seeking
        '''
        print 'checking if player %d is successfully tracking...' % self.get_player_id()
        
        normed_variance = np.linalg.norm(self.get_position_variance())
        unobs_fg_percentage = self._expected_player_data.get_unobstructed_foreground_percentage()
        
        print '   normed variance: ', normed_variance
        print '   unobstructed_foreground_percentage: ', unobs_fg_percentage
        
        tracking_successfull = ((normed_variance < self.MAX_POSITION_VARIANCE) and
                                (unobs_fg_percentage > self.MIN_UNOBS_FG_PERCENTAGE))

        print '  tracking successfull: ', tracking_successfull

        return tracking_successfull
    
    def update_state(self):
        '''
        If the player is active:
           * if the tracking has been going poorly for MAX_POOR_ACTIVE_FRAMES frames, make it dormant.
        If the player is seeking:
           * if the player has been doing poorly for MAX_POOR_SEEKING_FRAMES frames, make it dormant.
           * if the player has been doing well for NUM_SEEKING_FRAMES frame, then make it active.
       
        '''
        if self.is_dormant():
            return

        print 'updating the state of player: ', self.get_player_id()
        
        tracking_successfull = self._is_successfully_tracking()
        
        if not tracking_successfull:
            self._number_of_consecutive_bad_frames += 1
        else:
            self._number_of_consecutive_bad_frames = 0
            
        if self.is_active():
            #if there are too many bad frames, deactivate the player and do not do anything else
            if self._number_of_consecutive_bad_frames > self.MAX_BAD_ACTIVE_FRAMES:
                print 'deactivating the active player %d because of too many bad frames' % self.get_player_id()
                self._state = self.DORMANT
                return
            
        if self.is_seeking():
            self._number_of_seeking_frames += 1
            
            #if there are too many bad frames, deactivate the player and do not do anything else
            if self._number_of_consecutive_bad_frames > self.MAX_BAD_SEEKING_FRAMES:
                print 'deactivating the seeking player %d because of too many bad frames' % self.get_player_id()
                self._state = self.DORMANT
                return

            #if we managed to stay in seeking mode for enough frames, go into active mode
            if self._number_of_seeking_frames == self.NUM_SEEKING_FRAMES:
                print 'turning the seeking player %d into an active one.' % self.get_player_id()
                self._state = self.ACTIVE
                return

        return
    
    def get_positions(self):
        '''
        Return an array with shape (num frames, 2) recording the absolute positions of the player.
        If a player was dormant in frame i, the value at that position is np.nan,np.nan.
        '''
        return np.vstack(self._saved_mean_positions)

    def get_velocities(self):
        '''
        Return an array with shape (num frames, 2) recording the velocity of the player.
        If a player was dormant in frame i, the value at that position is np.nan,np.nan.
        '''
        return np.vstack(self._saved_mean_velocities)

    # def get_position_entropies(self):
    #     '''
    #     Return an array with shape (num frames, 2) recording the position entropies of the players.
    #     If a player was not active in frame i, the value at that position is np.nan,np.nan.
    #     '''
    #     return np.array(self._saved_position_entropies)

    def get_position_variances(self):
        '''
        Return an array with shape (num frames, 2) recording the position variances of the players.
        If a player was dormant in frame i, the value at that position is np.nan,np.nan.
        '''
        return np.vstack(self._saved_position_variances)

    def get_particle_positions(self):
        '''
        return a numpy array with shape (num frames, num particles) recording the positions of the player particles
        in each frame.
        If the player was dormant in a given frame, the row for that frame is nan.
        '''
        num_particles = self._player_particle_filter.get_number_of_particles()
        nan_row = np.empty((num_particles, 2))
        nan_row[...] = np.nan

        return np.stack(((nan_row if x is None else x) for x in self._saved_particle_positions))

    def get_team_labels(self):
        '''
        return a numpy array with length num_frames and type np.int32 which records the team labels.
        '''

        #self._finalize_team_labels()
        
        return np.array(self._saved_team_labels)
    
    # def get_obstructed_percentages(self):
    #     '''
    #     Return an array with length num frames recording the obstruction percentages of the players.
    #     If a player was not active in frame i, the value at that position is np.nan
    #     '''
    #     return np.array(self._saved_obstructed_percentages)

    # def get_unobstructed_foreground_percentages(self):
    #     '''
    #     Return an array with length num frames recording the unobstructed foreground percentages of the players.
    #     If a player was not active in frame i, the value at that position is np.nan
    #     '''
    #     return np.array(self._saved_unobstructed_foreground_percentages)
        
    def print_summary(self):
        
        print '-------'
        print 'summary of player %d:' % self._player_id
        print 'player state: ', self.get_state_name()

        if self.is_dormant():
            return
        
        print 'number of consecutive bad frames: ', self._number_of_consecutive_bad_frames
        
        if self.is_seeking():
            print 'number of seeking frames: ', self._number_of_seeking_frames

        print ' '
        print 'expected position: ', self.get_expected_position()
        print 'position variance: ', self.get_position_variance()
        print ' '
        print 'expected velocity: ', self.get_expected_velocity()
        print 'velocity variance: ', self.get_velocity_variance()
        print ' '
        print 'expected acceleration: ', self.get_expected_acceleration()
        print 'num particles: %d' % len(self._player_particle_filter._particles)            
        print 'team label: ', self._team_label
        
        #best_particle = max(self._player_particle_filter._particles, key=lambda x: x.get_probability())
        #worst_particle = min(self._player_particle_filter._particles, key=lambda x: x.get_probability())

        return

    @classmethod
    def build_dormant_player(cls, player_id, num_particles, soccer_field, frame_data):
        '''
        player_id - an integer
        num_particles - an integer

        Return a TrackedPlayer object with the given id with a particle filter that has the specified number of particles.
        The state of the player is set to DORMANT
        Note that we do not initialize the position, histogram, or anything else that depends on these.
        '''
        local_data_evaluator = LocalDataEvaluator()
        tracked_player = cls(player_id, soccer_field, frame_data, local_data_evaluator)
        field_rectangle = soccer_field.get_field_rectangle()
        particle_filter = PlayerParticleFilter.build_default_filter(num_particles, local_data_evaluator, field_rectangle,
                                                                    frame_data)
        tracked_player._player_particle_filter = particle_filter
        tracked_player._state = cls.DORMANT

        return tracked_player

    @classmethod
    def from_player_candidate(cls, player_id, player_candidate, num_particles, soccer_field, frame_data):
        '''
        player_id - an integer
        soccer_field - a SoccerFieldGeomety object
        frame_data - a PaddedFrameData object
        num_particles - an integer
        player_candidate - a PlayerCandidate object
        '''

        print 'constructing a tracked player with id: ', player_id
        local_data_evaluator = LocalDataEvaluator()
        tracked_player = cls(player_id, soccer_field, frame_data, local_data_evaluator)

        #create the particle filter
        position = player_candidate.absolute_position
        field_rectangle = soccer_field.get_field_rectangle()
        particle_filter = PlayerParticleFilter.from_initial_position(position, num_particles, local_data_evaluator,
                                                                     field_rectangle, frame_data)
        tracked_player._player_particle_filter = particle_filter
        tracked_player._team_label = player_candidate.team_label
        
        return tracked_player
        
