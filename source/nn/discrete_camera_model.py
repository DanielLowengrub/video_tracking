import tensorflow as tf
import numpy as np
from .conv_model import ConvModel

class DiscreteCameraModel(ConvModel):
    '''
    This is a 2 layered nn which is used to solve the following problem:
    INPUT: 64x64 single channel image containing a circle
    OUTPUT: a length 2 vector representing the center of the circle
    '''

    IMAGE_SHAPE = [128,128,1]
    PROB_SIZE   = 16 + 8 #16 bins for the z angle and 8 bins for the focus
    FILTER_SHAPE_1 = [3, 3, 1,  8]
    FILTER_SHAPE_2 = [3, 3, 8,  8]
    FILTER_SHAPE_3 = [3, 3, 8,  32]
    FILTER_SHAPE_4 = [3, 3, 32, 32]
    FC_SIZE_1 = 1024
    FC_SIZE_2 = 256
    
    def __init__(self, batch_size):
        super(DiscreteCameraModel, self).__init__(batch_size)

        self._build_graph()

    def _build_placeholders(self):
        self._image_ph          = tf.placeholder(tf.float32, [self._batch_size] + self.IMAGE_SHAPE)
        self._true_prob_ph      = tf.placeholder(tf.float32, [self._batch_size, self.PROB_SIZE])
        return

    def _build_model_op(self):
        '''
        Build a network: image -> conv -> conv -> pool -> conv -> conv -> pool -> fc -> fc -> fc -> softmax
        '''
        #this layer has shape [batch_size, image_height, image_width, FILTER_SHAPE_1[3]]
        self._conv1, self._filt1, self._bias1 = self._conv_layer(self._image_ph, self.FILTER_SHAPE_1)
        #this layer has shape [batch_size, image_height, image_width, FILTER_SHAPE_2[3]]
        self._conv2, self._filt2, self._bias2 = self._conv_layer(self._conv1,    self.FILTER_SHAPE_2)

        #this layer has shape [batch_size, image_height/2, image_width/2, FILTER_SHAPE_1[3]
        self._pool1 = tf.nn.max_pool(self._conv2, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')

        #this layer has shape [batch_size, image_height, image_width, FILTER_SHAPE_3[3]]
        self._conv3, self._filt3, self._bias3 = self._conv_layer(self._pool1, self.FILTER_SHAPE_3)
        #this layer has shape [batch_size, image_height, image_width, FILTER_SHAPE_4[3]]
        self._conv4, self._filt4, self._bias4 = self._conv_layer(self._conv3, self.FILTER_SHAPE_4)

        #this layer has shape [batch_size, image_height/4, image_width/4, FILTER_SHAPE_4[3]
        self._pool2 = tf.nn.max_pool(self._conv4, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')

        #this layer has shape [batch_size, FC_SIZE_1]
        self._fc1 = self._fc_layer(self._pool2, self.FC_SIZE_1)
        self._relu1 = tf.nn.relu(self._fc1)
        
        #this layer has shape [batch_size, FC_SIZE_1]
        self._fc2 = self._fc_layer(self._relu1,  self.FC_SIZE_2)
        self._relu2 = tf.nn.relu(self._fc2)

        self._logits = self._fc_layer(self._relu2, self.PROB_SIZE)
        return

    def _build_loss_op(self):
        self._loss_op = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=self._logits, labels=self._true_prob_ph))
        return

    def _build_evaluation_op(self):
        self._evaluation_op = None
        return

    def _build_prediction_op(self):
        self._prediction_op = tf.nn.softmax(self._logits)
        return

    def _get_scaled_images(self, input_data_batch):
        input_data_batch = input_data_batch.reshape((-1,) + tuple(self.IMAGE_SHAPE))
        return (input_data_batch - 127.0) / 255.0
    
    def get_feed_dict(self, input_data_batch, true_values_batch=None):
        feed_dict = dict()
        feed_dict[self._image_ph] = self._get_scaled_images(input_data_batch)

        if true_values_batch is not None:
            feed_dict[self._true_prob_ph] = true_values_batch

        return feed_dict
        
    def get_internal_repr(self, sess, feed_dict):
        image_str = 'image:\n%s' % ((feed_dict[self._image_ph][0,:,:,0]>0).astype(np.int32),)
        # filt1_str = 'filter 1:\n%s' % self._get_filter_repr(sess, self._filt1)
        # filt2_str = 'filter 2:\n%s' % self._get_filter_repr(sess, self._filt2)
        # conv1_str = 'conv layer 1:\n%s' % self._get_conv_repr(sess, feed_dict, self._conv1)
        # conv2_str = 'conv layer 2:\n%s' % self._get_conv_repr(sess, feed_dict, self._conv2)

        pool = sess.run(self._pool2, feed_dict=feed_dict)
        pool_str = 'max pool:\n' + '\n'.join(['channel %d:\n%s' % (i, (pool[0,:,:,i] > 0).astype(np.int32))
                                              for i in range(pool.shape[3])])
        return '\n'.join([image_str, pool_str])
