import collections

class LayerParameters(object):
    '''
    This is the base class for classes that store parameters that are used to build a layer of a NN.
    '''

    def __init__(self, trainable=True, visualize=False):
        '''
        name - a string
        trainable - a boolean
        visualize - a boolean
        '''
        self._trainable = trainable
        self._visualize = visualize

    @property
    def trainable(self):
        return self._trainable
    
    @property
    def visualize(self):
        return self._visualize

class ConvolutionParameters(LayerParameters):
    '''
    This is used to build a convolution layer
    '''
    def __init__(self, filter_size, num_output_channels, trainable=True, visualize=False, has_relu=True):
        '''
        filter_size - the size of the filter that will be convolved with the previous layer
        num_output_channels - an int
        '''
        super(ConvolutionParameters, self).__init__(trainable, visualize)
        self._filter_size = filter_size
        self._num_output_channels = num_output_channels
        self._has_relu = has_relu
        
    @property
    def filter_size(self):
        return self._filter_size

    @property
    def num_output_channels(self):
        return self._num_output_channels

    @property
    def has_relu(self):
        return self._has_relu
    
class DilatedConvolutionParameters(ConvolutionParameters):
    '''
    This is used to build a dilated convolution.
    '''
    def __init__(self, filter_size, rate, num_output_channels, trainable=True, visualize=False, has_relu=True):
        '''
        has_relu - if True, then a relu will be applied to the output of the convolution
        '''
        super(DilatedConvolutionParameters, self).__init__(filter_size, num_output_channels, trainable,
                                                           visualize, has_relu)
        self._rate = rate

    @property
    def rate(self):
        return self._rate
    
class UpsampleParameters(LayerParameters):
    '''
    this is used to build an upsampling layer
    '''
    def __init__(self, factor, trainable=True, visualize=False):
        '''
        factor - an int. the amount that the image is scaled up by
        '''
        super(UpsampleParameters, self).__init__(trainable, visualize, has_relu=True)
        self._factor = factor

    @property
    def factor(self):
        return self._factor
