import tensorflow as tf

class TFModel(object):
    '''
    This is a base class for tensorflow models.
    Its interface provides methods to train, save and load a model.
    '''
    TRAINING_SUMMARY   = 'training_summary'
    VALIDATION_SUMMARY = 'validation_summary'
    
    def __init__(self, batch_size):
        '''
        batch_size - the number of data points processed at once.
        '''
        self._batch_size = batch_size


    def _build_graph(self):
        tf.get_collection(self.TRAINING_SUMMARY)
        tf.get_collection(self.VALIDATION_SUMMARY)
        
        self._build_placeholders()
        self._build_model_op()
        
        self._build_loss_op()
        self._build_prediction_op()
        self._build_evaluation_op()

    def _build_placeholders(self):
        '''
        build the placeholders in the graph
        '''
        raise NotImplementedError('build_placeholders has not been implemented')

    def _build_model_op(self):
        '''
        build the graph that computes the output of the model
        '''
        raise NotImplementedError('build_model_op has not been implemented')
    
    def _build_loss_op(self):
        '''
        return the graph that computes the loss of the model w.r.t a batch of input and the true values.
        '''
        raise NotImplementedError('build_loss_op has not been implemented')

    def _build_evaluation_op(self):
        '''
        return the graph that evaluates the model w.r.t a batch of input and the true values.
        '''
        raise NotImplementedError('build_evaluation_op has not been implemented')

    def _build_prediction_op(self):
        '''
        return the graph that predicts the values of a batch of data using the model.
        '''
        raise NotImplementedError('build_prediction_op has not been implemented')

    def get_internal_repr(self, sess, feed_dict):
        '''
        Return a string that contains a representation of the internal model parameters.
        This is used for debugging.
        '''
        return ''
    
    def get_batch_size(self):
        return self._batch_size
    
    def get_loss_op(self):
        return self._loss_op

    def get_evaluation_op(self):
        return self._evaluation_op
    
    def get_prediction_op(self):
        return self._prediction_op
    
    def get_feed_dict(self, input_data_batch, true_values_batch=None):
        '''
        input_data_batch - a tensorflow array with shape [batch_size, ...]
        true_values_batch - a tensorflow array with shape [batch_size, ...]

        If we only want to feed the prediction op then true_values_batch can be None.

        return - a dictionary that can be fed to the loss, evaluation and training ops.
        '''
        raise NotImplementedError('get_feed_dict has not been implemented')

    def get_static_variable_summary(self, sess):
        '''
        return - a string. It should contain a summary of static model parameters that we want to print before
           the model is run. It should be ready to print to the console.
        '''
        return ''
