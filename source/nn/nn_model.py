import tensorflow as tf
import numpy as np
from .tf_model import TFModel

class NNModel(TFModel):
    '''
    This is a base class for neural net models.
    it contains methods that are used in all NN models.
    '''
    def __init__(self, batch_size):
        super(NNModel, self).__init__(batch_size)
        
    def _fc_layer(self, bottom_layer, output_size):
        '''
        bottom_layer - a tf array with shape [batch_size, d0,...dk]. It represents the current bottom layer of the nn.
        output_size  - an integer

        add a fully connected layer to the graph.
        the current bottom layer will now be [batch_size, output_size]
        '''        
        bottom_shape = bottom_layer.get_shape().as_list()
        dim = reduce(lambda x,y: x*y, bottom_shape[1:], 1)

        #reshape the bottom layer to have shape [batch_size, dim]
        x = tf.reshape(bottom_layer, [self._batch_size, dim])

        #create a new variable containing a matrix with size [dim, output_size]
        weights = tf.Variable(tf.truncated_normal([dim, output_size], stddev=1.0 / np.sqrt(float(output_size))))
        #and a vector with shape [output_size]
        biases = tf.Variable(tf.zeros([output_size]))

        fc = tf.add(tf.matmul(x, weights), biases)
        return fc

