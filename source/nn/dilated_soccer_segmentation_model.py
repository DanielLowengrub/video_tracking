import tensorflow as tf
import numpy as np
from .conv_model import ConvModel
from .layer_parameters import ConvolutionParameters, DilatedConvolutionParameters

class DilatedSoccerSegmentationModel(ConvModel):
    '''
    This is a model for segmenting an image into a number of classes.
    INPUT:  352x624 BGR image
    OUTPUT: 352x624 np array with type np.int32
    '''

    IMAGE_SHAPE = [352, 624, 3]
    NUM_CLASSES = 4
    
    _parameter_dict = {'conv_1_1':         ConvolutionParameters(3, 8, visualize=True),
                       'conv_1_2':         ConvolutionParameters(3, 16),
                       'dilated_conv_2_1': DilatedConvolutionParameters(3, 2, 16),
                       'conv_2_2':         ConvolutionParameters(3, 32),
                       'dilated_conv_3_1': DilatedConvolutionParameters(3, 4, 32),
                       'conv_fc_1':        ConvolutionParameters(1, 128),
                       'conv_logits':      ConvolutionParameters(1,NUM_CLASSES, has_relu=False)}

    #the colors representing each class
    CMAP = np.array([[0,   0,   0  ], #background
                     [255, 0,   0  ], #grass
                     [255, 255, 255], #sideline
                     [0,   255, 0  ], #player
                     [127, 127, 127]], np.uint8) #unknown
                    
    def __init__(self, batch_size, channel_mean=np.array([127,127,127])):
        super(DilatedSoccerSegmentationModel, self).__init__(batch_size)
        self._channel_mean = np.float32(channel_mean)
        self._build_graph()
        
    def _build_placeholders(self):
        with tf.name_scope('input'):
            self._image_ph             = tf.placeholder(tf.float32, [self._batch_size] + self.IMAGE_SHAPE,
                                                        name='images')
            self._true_segmentation_ph = tf.placeholder(tf.int32,
                                                        [self._batch_size, self.IMAGE_SHAPE[0],
                                                         self.IMAGE_SHAPE[1]], name='segmentations')

            self._build_image_channel_histograms(self._image_ph, 'image_channels')
        return

    def _build_model_op(self):
        '''
        Build the  network. The final output is a tensor with shape:
        [batch_size, image_height, image_width, num_classes]
        '''
        #all layers have shape [batch_size, image_height, image_width, ...]
        self._conv_1_1 = self._conv_layer(self._image_ph, 'conv_1_1')
        self._conv_1_2 = self._conv_layer(self._conv_1_1, 'conv_1_2')
        
        self._dilated_conv_2_1 = self._dilated_conv_layer(self._conv_1_2, 'dilated_conv_2_1')
        self._conv_2_2 = self._conv_layer(self._dilated_conv_2_1, 'conv_2_2')

        self._dilated_conv_3_1 = self._dilated_conv_layer(self._conv_2_2, 'dilated_conv_3_1')
        
        #this layer has shape [batch_size, image_height, image_width, ...]
        self._conv_fc_1 = self._conv_layer(self._dilated_conv_3_1, 'conv_fc_1')

        #this layer has shape [batch_size, image_height, image_width, NUM_CLASSES]
        self._logits = self._conv_layer(self._conv_fc_1, 'conv_logits')

        self._visualize_logits(self._logits, 'logits_visualization')
        
        return

    def _build_loss_op(self):
        with tf.name_scope('loss_op'):
            #the "unknown" class has value NUM_CLASSES. I.e, it is the last class
            known_mask         = tf.not_equal(self._true_segmentation_ph, self.NUM_CLASSES)
            known_logits       = tf.boolean_mask(self._logits, known_mask)
            known_segmentation = tf.boolean_mask(self._true_segmentation_ph, known_mask)

            self._known_logits = known_logits
            self._known_true_segmentation = known_segmentation
        
            self._loss_op = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(logits=known_logits,
                                                                                          labels=known_segmentation,
                                                                                          name='softmax'))

            tf.summary.scalar('loss', self._loss_op, collections=[self.TRAINING_SUMMARY])
            
        return

    def _build_evaluation_op(self):
        with tf.name_scope('evaluation_op'):
            #calculate the ratio of pixels that we correctly predicted.
            #we only consider pixels whose ground truth is known
            known_prediction = tf.to_int32(tf.argmax(self._known_logits, axis=1))
            is_correct = tf.to_float(tf.equal(known_prediction, self._known_true_segmentation))
            self._evaluation_op = tf.reduce_mean(is_correct)
            tf.summary.scalar('precision', self._evaluation_op, collections=[self.TRAINING_SUMMARY])
            
        return

    def _build_prediction_op(self):
        with tf.name_scope('prediction_op'):
            self._prediction_op = tf.argmax(self._logits, axis=3)
            
        return

    def _visualize_logits(self, logits, name):
        '''
        logits - a tf array with shape [batch size, height, width, num classes]
        add an image op to the graph that visualizes the logits
        '''
        with tf.name_scope(name):
            #convert the logits to predictions.
            top_labels = tf.nn.top_k(logits, k=3)[1]
            top_labels = tf.unstack(top_labels, num=3, axis=3)
            top_labels = tf.concat(axis=1, values=top_labels)
            
            #turn the top label predictions into colors
            logits_img = tf.gather(self.CMAP, top_labels)
            img = tf.cast(self._unscaled_images(self._image_ph), tf.uint8)
            gt  = tf.gather(self.CMAP, self._true_segmentation_ph)
            img = tf.concat(axis=1, values=[img, gt, logits_img])
            
            print 'shape of logits visualization: ', img.get_shape().as_list()
            tf.summary.image('logits', img, collections=[self.TRAINING_SUMMARY])

        return
    
    def _scaled_images(self, images):
        return (images - self._channel_mean) / 255.0

    def _unscaled_images(self, scaled_images):
        return (scaled_images * 255) + self._channel_mean
    
    def get_feed_dict(self, input_data_batch, true_values_batch=None):
        feed_dict = dict()
        feed_dict[self._image_ph] = self._scaled_images(input_data_batch)

        if true_values_batch is not None:
            feed_dict[self._true_segmentation_ph] = true_values_batch

        return feed_dict

    def get_static_variable_summary(self, sess):
        return 'channel mean: %s' % self._channel_mean
