class DataSet(object):
    '''
    This represents a data set that is used for training neural networks.
    '''

    def initialize_epoch(self):
        '''
        Prepare the data set for a new epoch.
        '''
        raise NotImplemented('initialize_epoch has not been implemented')

    def has_batch(self):
        '''
        return True iff the data set has another batch
        '''
        raise NotImplemented('has_batch has not been implemented')

    def next_batch(self):
        '''
        return a tuple: (input, ground_truth)
        both are numpy arrays with shape (batch_size, ...)
        '''
        raise NotImplemented('batch_size has not been implemented')
