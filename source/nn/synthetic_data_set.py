import numpy as np
from .data_set import DataSet

class SyntheticDataSet(DataSet):
    '''
    This represents a data set which is generated on the fly
    '''
    def __init__(self, batch_size, batches_per_epoch):
        '''
        batch_size - an int. The number of training pairs in a batch
        batches_per_epoch - an int. The number of batches in an epoch
        '''
        self._batch_size = batch_size
        self._batches_per_epoch = batches_per_epoch
        self._batch = 0
        
    def initialize_epoch(self):
        '''
        Prepare the data set for a new epoch.
        '''
        self._batch = 0

    def has_batch(self):
        '''
        return True iff the data set has another batch
        '''
        return self._batch + 1 < self._batches_per_epoch

    def next_batch(self):
        '''
        return a tuple: (input, ground_truth)
        both are numpy arrays with shape (batch_size, ...)
        '''
        self._batch += 1
        
        training_pairs = [self._build_training_pair() for i in range(self._batch_size)]
        input_array = np.stack([tp[0] for tp in training_pairs])
        gt_array    = np.stack([tp[1] for tp in training_pairs])

        return input_array, gt_array

    def _build_training_pair(self):
        '''
        return - a tuple (input, ground_truth)
        '''
        raise NotImplemented('_build_training_pair has not been implemented')

