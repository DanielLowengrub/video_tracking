import tensorflow as tf
import numpy as np
from .conv_model import ConvModel
from .layer_parameters import ConvolutionParameters, UpsampleParameters

class SimpleSegmentationModel(ConvModel):
    '''
    This is a simple model for segmenting an image into a number of classes
    INPUT: 64x64 single channel image
    OUTPUT: 
    '''

    IMAGE_SHAPE = [64,64,1]
    NUM_CLASSES = 4
    
    _parameter_dict = {'conv_1_1': ConvolutionParameters(3, 8, visualize=True),
                       'conv_1_2': ConvolutionParameters(3, 8),
                       'conv_2_1': ConvolutionParameters(3, 16),
                       'conv_2_2': ConvolutionParameters(3, 16),
                       'conv_3_1': ConvolutionParameters(3, 32),
                       'conv_3_2': ConvolutionParameters(3, 32),
                       'conv_4_1': ConvolutionParameters(3, 64),
                       'conv_4_2': ConvolutionParameters(3, 64),
                       'conv_5_1': ConvolutionParameters(3, 64),
                       'conv_5_2': ConvolutionParameters(3, 64),
                       'conv_fc_1': ConvolutionParameters(3, 128),
                       'conv_coarse_logits': ConvolutionParameters(1,NUM_CLASSES),
                       'upsampled_logits_1': UpsampleParameters(2),
                       'upsampled_logits_2': UpsampleParameters(2),
                       'upsampled_logits_3': UpsampleParameters(2),
                       'upsampled_logits_4': UpsampleParameters(2),
                       'logits': UpsampleParameters(2)}

    #the colors representing each class
    CMAP = np.array([[0,   0,   0  ], #background
                     [255, 0,   0  ], 
                     [0,   255, 0  ],
                     [0,   255, 255]], np.uint8)
                    
    def __init__(self, batch_size):
        super(SimpleSegmentationModel, self).__init__(batch_size)
        self._build_graph()

    def _build_placeholders(self):
        with tf.name_scope('input'):
            self._image_ph             = tf.placeholder(tf.float32, [self._batch_size] + self.IMAGE_SHAPE, name='images')
            self._true_segmentation_ph = tf.placeholder(tf.int32,
                                                        [self._batch_size, self.IMAGE_SHAPE[0], self.IMAGE_SHAPE[1]],
                                                        name='segmentations')
        return

    def _build_model_op(self):
        '''
        Build the  network. The final output is a tensor with shape:
        [batch_size, image_height, image_width, num_classes]
        '''
        #this layer has shape [batch_size, image_height, image_width, ...]
        self._conv_1_1 = self._conv_layer(self._image_ph, 'conv_1_1')
        #this layer has shape [batch_size, image_height, image_width, ...]
        self._conv_1_2 = self._conv_layer(self._conv_1_1, 'conv_1_2')

        #this layer has shape [batch_size, image_height/2, image_width/2, ...]
        self._pool_1 = tf.nn.max_pool(self._conv_1_2, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME', name='pool_1')

        #this layer has shape [batch_size, image_height/2, image_width/2, ...]
        self._conv_2_1 = self._conv_layer(self._pool_1, 'conv_2_1')
        #this layer has shape [batch_size, image_height/2, image_width/2, ...]
        self._conv_2_2 = self._conv_layer(self._conv_2_1, 'conv_2_2')

        #this layer has shape [batch_size, image_height/4, image_width/4, ...]
        self._pool_2 = tf.nn.max_pool(self._conv_2_2, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME', name='pool_2')

        #this layer has shape [batch_size, image_height/4, image_width/4, ...]
        self._conv_3_1 = self._conv_layer(self._pool_2, 'conv_3_1')
        #this layer has shape [batch_size, image_height/4, image_width/4, ...]
        self._conv_3_2 = self._conv_layer(self._conv_3_1, 'conv_3_2')

        #this layer has shape [batch_size, image_height/8, image_width/8, ...]
        self._pool_3 = tf.nn.max_pool(self._conv_3_2, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME', name='pool_3')

        #this layer has shape [batch_size, image_height/8, image_width/8, ...]
        self._conv_4_1 = self._conv_layer(self._pool_3, 'conv_4_1')
        #this layer has shape [batch_size, image_height/8, image_width/8, ...]
        self._conv_4_2 = self._conv_layer(self._conv_4_1, 'conv_4_2')

        #this layer has shape [batch_size, image_height/16, image_width/16, ...]
        self._pool_4 = tf.nn.max_pool(self._conv_4_2, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME', name='pool_4')

        # #this layer has shape [batch_size, image_height/16, image_width/16, ...]
        # self._conv_5_1 = self._conv_layer(self._pool_4, 'conv_5_1')
        # #this layer has shape [batch_size, image_height/16, image_width/16, ...]
        # self._conv_5_2 = self._conv_layer(self._conv_5_1, 'conv_5_2')

        # #this layer has shape [batch_size, image_height/32, image_width/32, ...]
        # self._pool_5 = tf.nn.max_pool(self._conv_5_2, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME', name='pool_5')

        #this layer has shape [batch_size, image_height/32, image_width/32, ...]
        self._conv_fc_1 = self._conv_layer(self._pool_4, 'conv_fc_1')

        #this layer has shape [batch_size, image_height/32, image_width/32, NUM_CLASSES]
        self._conv_coarse_logits = self._conv_layer(self._conv_fc_1, 'conv_coarse_logits')

        #visualize the deepest layer
        self._visualize_logits(self._conv_coarse_logits, 'depth_5_logits_visualization')
        
        #this layer has shape [batch_size, image_height/16, image_width/16, NUM_CLASSES]
        self._upsample_1 = self._upsample_layer(self._conv_coarse_logits, 'upsampled_logits_1')
        #this layer has shape [batch_size, image_height/8, image_width/8, NUM_CLASSES]
        self._upsample_2 = self._upsample_layer(self._upsample_1, 'upsampled_logits_2')
        #this layer has shape [batch_size, image_height/4, image_width/4, NUM_CLASSES]
        self._upsample_3 = self._upsample_layer(self._upsample_2, 'upsampled_logits_3')
        # #this layer has shape [batch_size, image_height/2, image_width/2, NUM_CLASSES]
        # self._upsample_4 = self._upsample_layer(self._upsample_3, 'upsampled_logits_4')
        
        #this layer has shape [batch_size, image_height, image_width, NUM_CLASSES]
        self._logits = self._upsample_layer(self._upsample_3, 'logits')
        return

    def _build_loss_op(self):
        with tf.name_scope('loss_op'):
            #the "unknown" class has value NUM_CLASSES. I.e, it is the last class
            known_mask = tf.not_equal(self._true_segmentation_ph, self.NUM_CLASSES)
            known_logits = tf.boolean_mask(self._logits, known_mask)
            known_segmentation = tf.boolean_mask(self._true_segmentation_ph, known_mask)

            self._known_logits = known_logits
            self._known_segmentation = known_segmentation
        
            self._loss_op = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(logits=known_logits, labels=known_segmentation,
                                                                                          name='softmax'))

            tf.summary.scalar('loss', self._loss_op)
            
        return

    def _build_evaluation_op(self):
        self._evaluation_op = None
        return

    def _build_prediction_op(self):
        with tf.name_scope('prediction_op'):
            self._prediction_op = tf.argmax(self._logits, axis=3)
            
        return

    def _visualize_logits(self, logits, name):
        '''
        logits - a tf array with shape [batch size, height, width, num classes]
        add an image op to the graph that visualizes the logits
        '''
        with tf.name_scope(name):
            #convert the logits to predictions.
            pred = tf.argmax(logits, axis=3)

            #turn the predictions into colors
            img = tf.gather(self.CMAP, pred)
            print 'shape of logits visualization: ', img.get_shape().as_list()
            tf.summary.image('logits', img)

        return
    
    def _get_scaled_images(self, input_data_batch):
        input_data_batch = input_data_batch.reshape((-1,) + tuple(self.IMAGE_SHAPE))
        return (input_data_batch - 127.0) / 255.0
    
    def get_feed_dict(self, input_data_batch, true_values_batch=None):
        feed_dict = dict()
        feed_dict[self._image_ph] = self._get_scaled_images(input_data_batch)

        if true_values_batch is not None:
            feed_dict[self._true_segmentation_ph] = true_values_batch

        return feed_dict
        
