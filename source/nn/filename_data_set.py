from .data_set import DataSet
import random
import numpy as np

class FilenameDataSet(object):
    '''
    This represents a data set that is loaded from a list of filenames
    '''
    def __init__(self, batch_size, training_pair_filenames):
        '''
        batch_size - an integer
        training_pair_filenames - a list of tuples: (input_filename, ground_truth_filename)
        '''
        self._batch_size = batch_size
        self._training_pair_filenames = training_pair_filenames
        self._current_index = 0
        
    def initialize_epoch(self):
        '''
        Prepare the data set for a new epoch.
        '''
        self._current_index = 0
        random.shuffle(self._training_pair_filenames)

    def has_batch(self):
        '''
        return True iff the data set has another batch
        '''
        return self._current_index + self._batch_size <= len(self._training_pair_filenames)

    def next_batch(self):
        '''
        return a tuple: (input, ground_truth)
        both are numpy arrays with shape (batch_size, ...)
        '''
        filename_pairs = self._training_pair_filenames[self._current_index:self._current_index + self._batch_size]
        training_pairs = [self._load_training_pair(*fp) for fp in filename_pairs]
        
        input_array = np.stack([tp[0] for tp in training_pairs])
        gt_array    = np.stack([tp[1] for tp in training_pairs])

        self._current_index += self._batch_size

        return input_array, gt_array

    def _load_training_pair(self, input_filename, gt_filename):
        '''
        input_filename - a string. it is the name of the file containing the input
        gt_filename - a string. it is the name of the file containing the ground truth

        return - a tuple (input, ground_truth)
        both are numpy arrays with shape (batch_size, ...)
        '''
        raise NotImplementedError('_load_training_pair has not been implemented')
