import tensorflow as tf
from .nn_model import NNModel

class SimpleCircleModel(NNModel):
    '''
    This is a 2 layered nn which is used to solve the following problem:
    INPUT: 64x64 single channel image containing a circle
    OUTPUT: a length 2 vector representing the center of the circle
    '''

    FIRST_LAYER_SIZE  = 256
    SECOND_LAYER_SIZE = 64
    IMAGE_SHAPE = [64,64]
    
    def __init__(self, batch_size, evaluation_thresh):
        super(SimpleCircleModel, self).__init__(batch_size)

        self._evaluation_thresh = evaluation_thresh

    
        self._build_graph()

    def _build_placeholders(self):
        self._image_ph          = tf.placeholder(tf.float32, [self._batch_size] + self.IMAGE_SHAPE)
        self._true_center_ph    = tf.placeholder(tf.float32, [self._batch_size, 2])
        return

    def _build_model_op(self):
        '''
        Build a fully connected nn with 2 layers
        '''
        #this layer has shape [batch_size, MIDDLE_LAYER_SIZE]
        self._fc1 = self._fc_layer(self._image_ph, self.FIRST_LAYER_SIZE)
        self._fc2 = self._fc_layer(self._fc1, self.SECOND_LAYER_SIZE)

        #this layer has shape [batch_size, 2]
        self._center = self._fc_layer(self._fc2, 2)

        return

    def _build_loss_op(self):
        self._loss_op = tf.reduce_mean(tf.pow(self._center - self._true_center_ph, 2), name='loss')
        return

    def _build_evaluation_op(self):
        is_correct = tf.less(tf.abs(self._center - self._true_center_ph), self._evaluation_thresh)
        self._evaluation_op = tf.div(tf.reduce_sum(tf.to_int32(is_correct)), self._batch_size, name='evaluation')
        return

    def _build_prediction_op(self):
        self._prediction_op = self._center
        return

    def get_feed_dict(self, input_data_batch, true_values_batch=None):
        feed_dict = dict()
        feed_dict[self._image_ph] = input_data_batch

        if true_values_batch is not None:
            feed_dict[self._true_center_ph] = true_values_batch

        return feed_dict
        
