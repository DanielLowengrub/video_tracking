import tensorflow as tf
import numpy as np
from .conv_model import ConvModel
from .layer_parameters import ConvolutionParameters, DilatedConvolutionParameters

class DilatedSegmentationModel(ConvModel):
    '''
    This is a simple model for segmenting an image into a number of classes
    INPUT: 64x64 single channel image
    OUTPUT: 
    '''

    IMAGE_SHAPE = [64,64,1]
    NUM_CLASSES = 3
    
    _parameter_dict = {'conv_1_1':         ConvolutionParameters(3, 4, visualize=True),
                       'dilated_conv_1_2': DilatedConvolutionParameters(3, 2, 4),
                       'conv_2_1':         ConvolutionParameters(3, 8),
                       'dilated_conv_2_2': DilatedConvolutionParameters(3, 2, 8),
                       'conv_3_1':         ConvolutionParameters(3, 8),
                       'dilated_conv_3_2': DilatedConvolutionParameters(3, 2, 8),
                       'conv_4_1':         ConvolutionParameters(3, 16),
                       'dilated_conv_4_2': DilatedConvolutionParameters(3, 2, 16),
                       'conv_fc_1':        ConvolutionParameters(3, 64),
                       'conv_logits':      ConvolutionParameters(1,NUM_CLASSES)}

    #the colors representing each class
    CMAP = np.array([[0,   0,   0  ], #background
                     [255, 255, 255], #sideline
                     [0,   255, 0  ]], np.uint8) #player
                    
    def __init__(self, batch_size):
        super(DilatedSegmentationModel, self).__init__(batch_size)
        self._build_graph()

    def _build_placeholders(self):
        with tf.name_scope('input'):
            self._image_ph             = tf.placeholder(tf.float32, [self._batch_size] + self.IMAGE_SHAPE,
                                                        name='images')
            self._true_segmentation_ph = tf.placeholder(tf.int32,
                                                        [self._batch_size, self.IMAGE_SHAPE[0], self.IMAGE_SHAPE[1]],
                                                        name='segmentations')
        return

    def _build_model_op(self):
        '''
        Build the  network. The final output is a tensor with shape:
        [batch_size, image_height, image_width, num_classes]
        '''
        #all layers have shape [batch_size, image_height, image_width, ...]
        self._conv_1_1 = self._conv_layer(self._image_ph, 'conv_1_1')
        #self._dilated_conv_1_2 = self._dilated_conv_layer(self._conv_1_1, 'dilated_conv_1_2')

        self._conv_2_1 = self._conv_layer(self._conv_1_1, 'conv_2_1')
        # self._dilated_conv_2_2 = self._dilated_conv_layer(self._conv_2_1, 'dilated_conv_2_2')

        # self._conv_3_1 = self._conv_layer(self._dilated_conv_2_2, 'conv_3_1')
        # self._dilated_conv_3_2 = self._dilated_conv_layer(self._conv_3_1, 'dilated_conv_3_2')

        # self._conv_4_1 = self._conv_layer(self._dilated_conv_3_2, 'conv_4_1')
        # self._dilated_conv_4_2 = self._dilated_conv_layer(self._conv_4_1, 'dilated_conv_4_2')

        #this layer has shape [batch_size, image_height, image_width, ...]
        self._conv_fc_1 = self._conv_layer(self._conv_2_1, 'conv_fc_1')

        #this layer has shape [batch_size, image_height, image_width, NUM_CLASSES]
        self._logits = self._conv_layer(self._conv_fc_1, 'conv_logits')

        self._visualize_logits(self._logits, 'logits_visualization')
        
        return

    def _build_loss_op(self):
        with tf.name_scope('loss_op'):
            #the "unknown" class has value NUM_CLASSES. I.e, it is the last class
            known_mask   = tf.not_equal(self._true_segmentation_ph, self.NUM_CLASSES)
            known_logits = tf.boolean_mask(self._logits, known_mask)
            known_segmentation = tf.boolean_mask(self._true_segmentation_ph, known_mask)

            self._known_logits = known_logits
            self._known_true_segmentation = known_segmentation
        
            self._loss_op = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(logits=known_logits,
                                                                                          labels=known_segmentation,
                                                                                          name='softmax'))

            tf.summary.scalar('loss', self._loss_op, collections=[self.TRAINING_SUMMARY])
            
        return

    def _build_prediction_op(self):
        with tf.name_scope('prediction_op'):
            self._prediction_op = tf.argmax(self._logits, axis=3)
            
        return

    def _build_evaluation_op(self):
        with tf.name_scope('evaluation_op'):
            #calculate the ratio of pixels that we correctly predicted.
            #we only consider pixels whose ground truth is known
            known_prediction = tf.to_int32(tf.argmax(self._known_logits, axis=1))
            is_correct = tf.to_float(tf.equal(known_prediction, self._known_true_segmentation))
            self._evaluation_op = tf.reduce_mean(is_correct)
            tf.summary.scalar('precision', self._evaluation_op, collections=[self.TRAINING_SUMMARY])
            
        return

    def _visualize_logits(self, logits, name):
        '''
        logits - a tf array with shape [batch size, height, width, num classes]
        add an image op to the graph that visualizes the logits
        '''
        with tf.name_scope(name):
            #convert the logits to predictions.
            pred = tf.argmax(logits, axis=3)

            #turn the predictions into colors
            prediction_img = tf.gather(self.CMAP, pred)
            tf.summary.image('logits', prediction_img, collections=[self.TRAINING_SUMMARY])
            tf.summary.image('image', self._image_ph, collections=[self.TRAINING_SUMMARY])
            
        return
    
    def _get_scaled_images(self, input_data_batch):
        input_data_batch = input_data_batch.reshape((-1,) + tuple(self.IMAGE_SHAPE))
        return (input_data_batch - 127.0) / 255.0
    
    def get_feed_dict(self, input_data_batch, true_values_batch=None):
        feed_dict = dict()
        feed_dict[self._image_ph] = self._get_scaled_images(input_data_batch)

        if true_values_batch is not None:
            feed_dict[self._true_segmentation_ph] = true_values_batch

        return feed_dict
        
