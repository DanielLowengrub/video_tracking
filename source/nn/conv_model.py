import tensorflow as tf
from .nn_model import NNModel
import numpy as np

class ConvModel(NNModel):
    '''
    This is a base class for ConvNet models.
    '''

    def __init__(self, batch_size):
        super(ConvModel, self).__init__(batch_size)

    def _conv_layer(self, bottom_layer, name, stdev=0.01):
        '''
        bottom_layer - a tf array with shape [batch_size, image_height, image_width, num_input_channels]
        name - a string. it is a key in parameter_dict

        return - a tuple (output, filt, bias)
        output - numpy array with shape [batch_size, image_height, image_width, num_output_channels]
        filt - the filter variable
        bias - the bias variable
        '''
        with tf.name_scope(name):
            filter_size = self._parameter_dict[name].filter_size
            num_output_channels = self._parameter_dict[name].num_output_channels
            
            batch_size = bottom_layer.get_shape().as_list()[0]
            image_height = bottom_layer.get_shape().as_list()[1]
            image_width = bottom_layer.get_shape().as_list()[2]
            num_input_channels = bottom_layer.get_shape().as_list()[3]

            filter_shape = [filter_size, filter_size, num_input_channels, num_output_channels]
            filt = tf.Variable(tf.truncated_normal(filter_shape, 0.0, stdev), name='filter')
            #add a histogram of the filter values
            #print 'setting up filter histograms...'
            tf.summary.histogram('%s: filter histogram' % name, filt,
                                 collections=[self.TRAINING_SUMMARY])

            if self._parameter_dict[name].visualize:
                #reshape the filter to a stack of num_output_channels images. each image has shape
                #(filter_size, filter_size, num_input_channels)
                filt_image = tf.transpose(filt, perm=[3,0,1,2])
                tf.summary.image(name + '_filters', filt_image, max_outputs=num_output_channels,
                                 collections=[self.TRAINING_SUMMARY])
                
            #this is an array with shape [batch_size, image_height, image_width, num_output_channels
            conv = tf.nn.conv2d(bottom_layer, filt, [1, 1, 1, 1], padding='SAME', name='conv')
            
            #this is an array with shape [num_output_channels]
            conv_biases = tf.Variable(tf.zeros((num_output_channels,)), name='bias')
            bias = tf.add(conv, conv_biases, name='conv_plus_bias')

            if self._parameter_dict[name].has_relu:
                output = tf.nn.relu(bias, name='relu')
            else:
                output = bias

            #add a histogram of the mean abs values of each channel in the activation layer
            mean_abs_values = tf.reduce_mean(tf.abs(output), [0,1,2])
            tf.summary.histogram('%s: channel abs values' % name, mean_abs_values,
                                 collections=[self.TRAINING_SUMMARY])
            
        return output

    def _compute_filter_size(self, factor):
        '''
        Find the filter size given the desired factor of upsampling.
        '''
        return 2 * factor - factor % 2

    def _build_bilinear_filter(self, size):
        '''
        size - an integer
        Make a 2D (size,size) bilinear kernel suitable for upsampling.
        '''
        #for example, if the factor is 2 then the size is 4. if the factor is 3 then the size is 5.
        factor = (size + 1) / 2
        
        if size % 2 == 1:
            center = factor - 1
        else:
            center = factor - 0.5

        #in position (x,y) of the bilinear filter we put the value: (1 - |x-center|)*(1 - |y-center|)
        og = np.ogrid[:size, :size]
        bilinear_filter = (1 - np.abs(og[0] - center) / (factor)) * \
                          (1 - np.abs(og[1] - center) / (factor))

        return bilinear_filter
    
    def _upsample_layer(self, bottom_layer, name):
        '''
        bottom_layer - a tf array with shape [batch_size, height, width, num_input_channels]
        name - a string. it is a key in parameter_dict

        return - a tf tensor with shape [batch_size, height*factor, width*factor, num_input_channels]
        the output is obtained by upsampling the input
        '''
        with tf.name_scope(name):
            #calculate the filter size
            factor = self._parameter_dict[name].factor
            filter_size = self._compute_filter_size(factor)

            #build a bilinear filter with the specified size
            bilinear_filter = self._build_bilinear_filter(filter_size)

            print 'building upsample layer with factor: %d and filter size: %d' % (factor, filter_size)
            print '   initializing filter to:'
            print bilinear_filter
        
            #the actual filter is initialized to have a copy of the bilinear filter at each channel
            num_channels = bottom_layer.get_shape().as_list()[3]
            np_filt = np.zeros((filter_size, filter_size, num_channels, num_channels), dtype=np.float32)
            for i in xrange(num_channels):
                np_filt[:,:,i,i] = bilinear_filter

            batch_size = bottom_layer.get_shape().as_list()[0]
            new_height = bottom_layer.get_shape().as_list()[1] * factor
            new_width = bottom_layer.get_shape().as_list()[2] * factor
            output_shape = [batch_size, new_height, new_width, num_channels]
            print '   output_shape: ', output_shape
        
            strides = [1, factor, factor, 1]

            filt = tf.Variable(np_filt, name='filter')
            upsample = tf.nn.conv2d_transpose(bottom_layer, filt, output_shape, strides,
                                              padding='SAME', name='upsample')
            relu = tf.nn.relu(upsample, name='relu')
            
        return relu

    def _build_identity_filter(self, filter_size, num_channels):
        '''
        filter_size - an odd integer
        num_channels - an integer

        return - a numpy array with shape [filter_size, filter_size, num_channels, num_channels]
        '''
        filt = np.zeros((filter_size, filter_size, num_channels, num_channels), np.float32)
        center = (filter_size - 1) / 2

        for i in range(num_channels):
            filt[center, center, i, i] = 1.0

        #print 'identity filter (size=%d, num_channels=%d):' % (filter_size, num_channels)
        #print filt
        
        return filt
    
    def _dilated_conv_layer(self, bottom_layer, name, stdev=0.01):
        '''
        bottom_layer - a tf array with shape [batch_size, height, width, num_input_channels]
        name - a string. it is a key in parameter_dict

        return - a tf tensor with shape [batch_size, height, width, num_output_channels]
        the output is obtained by convolving a dilated filter with the bottom layer
        '''
        with tf.name_scope(name):
            filter_size         = self._parameter_dict[name].filter_size
            num_output_channels = self._parameter_dict[name].num_output_channels
            rate                = self._parameter_dict[name].rate
            
            batch_size = bottom_layer.get_shape().as_list()[0]
            num_input_channels = bottom_layer.get_shape().as_list()[3]
            filter_shape = [filter_size, filter_size, num_input_channels, num_output_channels]

            np_filt = self._build_identity_filter(filter_size, num_input_channels)
            filt    = tf.Variable(np_filt, name='dilated_filter')
            tf.summary.histogram('%s: filter_histogram' % name, filt, collections=[self.TRAINING_SUMMARY])

            #this is an array with shape [batch_size, image_height, image_width, num_output_channels
            conv = tf.nn.atrous_conv2d(bottom_layer, filt, rate, padding='SAME', name='atrous_conv')
            
            #this is an array with shape [num_output_channels]
            conv_biases = tf.Variable(tf.zeros((num_output_channels,)), name='bias')
            bias = tf.add(conv, conv_biases, name='atrous_conv_plus_bias')
            relu = tf.nn.relu(bias, name='relu')
            
            #add a histogram of the mean abs values of each channel in the activation layer
            mean_abs_values = tf.reduce_mean(tf.abs(relu), [0,1,2])
            tf.summary.histogram('%s: channel abs values' % name, mean_abs_values,
                                 collections=[self.TRAINING_SUMMARY])
            
        return relu

    def _build_image_channel_histograms(self, images, name):
        '''
        images - a tf tensor with shape [batches, height, width, channels]
        name - a string. the name for the channel histograms

        add a histogram summary op to the graph for each channel.
        for each channel it records the histogram of values in that channel.
        '''
        with tf.name_scope(name):
            num_channels = images.get_shape().as_list()[3]
            channels = tf.unstack(images, num=num_channels, axis=3)
            for i,channel in enumerate(channels):
                hist_name = '%s: channel %d' % (name, i)
                tf.summary.histogram(hist_name, channel, collections=[self.TRAINING_SUMMARY])

        return
                
    def _get_filter_repr(self, sess, filt):
        '''
        sess - a tf session
        filt - a tf array with shape [height, width, channels]

        return - a string
        '''
        filt = sess.run(filt)
        filt_strs = ['channel %d:\n%s' % (i,filt[:,:,0,i]) for i in range(filt.shape[3])]
        return '\n'.join(filt_strs)

    def _get_conv_repr(self, sess, feed_dict, conv):
        '''
        conv - a tf array with shape [batch_size, height, width, nchannels]
        '''
        conv = sess.run(conv, feed_dict=feed_dict)
        conv_strs = ['channel %d:\n%s' % (i,conv[0,:,:,i]) for i in range(conv.shape[3])]
        return '\n'.join(conv_strs)



