import tensorflow as tf
from .conv_model import ConvModel

class ConvCircleModel(ConvModel):
    '''
    This is a 2 layered nn which is used to solve the following problem:
    INPUT: 64x64 single channel image containing a circle
    OUTPUT: a length 2 vector representing the center of the circle
    '''

    IMAGE_SHAPE = [64,64,1]

    FILTER_SHAPE_1 = [3, 3, 1, 6]
    FC_SIZE_1 = 128
    FC_SIZE_2 = 32
    
    def __init__(self, batch_size, evaluation_thresh):
        super(ConvCircleModel, self).__init__(batch_size)

        self._evaluation_thresh = evaluation_thresh

    
        self._build_graph()

    def _build_placeholders(self):
        self._image_ph          = tf.placeholder(tf.float32, [self._batch_size] + self.IMAGE_SHAPE)
        self._true_center_ph    = tf.placeholder(tf.float32, [self._batch_size, 2])
        return

    def _build_model_op(self):
        '''
        Build a network: image -> conv -> pool -> fc -> fc -> [cx, cy]
        '''
        #this layer has shape [batch_size, image_height, image_width, FILTER_SHAPE_1[3]]
        self._conv1, filt1, bias1 = self._conv_layer(self._image_ph, self.FILTER_SHAPE_1)

        #this layer has shape [batch_size, image_height/2, image_width/2, FILTER_SHAPE_1[3]
        self._pool = tf.nn.max_pool(self._conv1, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')

        #this layer has shape [batch_size, FC_SIZE_1]
        self._fc1 = self._fc_layer(self._pool, self.FC_SIZE_1)

        #this layer has shape [batch_size, FC_SIZE_1]
        self._fc2 = self._fc_layer(self._fc1,  self.FC_SIZE_2)

        #this layer has shape [batch_size, 2]
        self._center = self._fc_layer(self._fc2, 2)

        return

    def _build_loss_op(self):
        self._loss_op = tf.reduce_mean(tf.pow(self._center - self._true_center_ph, 2), name='loss')
        return

    def _build_evaluation_op(self):
        is_correct = tf.less(tf.abs(self._center - self._true_center_ph), self._evaluation_thresh)
        self._evaluation_op = tf.div(tf.reduce_sum(tf.to_int32(is_correct)), self._batch_size, name='evaluation')
        return

    def _build_prediction_op(self):
        self._prediction_op = self._center
        return

    def get_feed_dict(self, input_data_batch, true_values_batch=None):
        feed_dict = dict()
        feed_dict[self._image_ph] = input_data_batch.reshape((-1,) + tuple(self.IMAGE_SHAPE))

        if true_values_batch is not None:
            feed_dict[self._true_center_ph] = true_values_batch

        return feed_dict
        
