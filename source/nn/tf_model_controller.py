import tensorflow as tf
import numpy as np
import time
import itertools
import sys
from .tf_model import TFModel

class TFModelController(object):
    '''
    This class is used to train, save and load TFModels.
    '''
    def __init__(self, learning_rate, max_training_epochs, num_batches_per_summary, num_batches_per_checkpoint,
                 decay_rate=None, decay_step=None):
        self._learning_rate = learning_rate
        self._max_training_epochs = max_training_epochs
        self._num_batches_per_summary = num_batches_per_summary
        self._num_batches_per_checkpoint = num_batches_per_checkpoint
        
        self._decay_rate = decay_rate
        self._decay_step = decay_step

    @classmethod
    def _get_batch(cls, it, batch_size):
        '''
        it - an iterator of numpy arrays with shape [n1,...,nk].
        return - a numpy array with shape [batch_size, n1, ..., nk]
        if there are not enough arrays left to make a batch, return None
        '''
        batch = list(itertools.islice(it, 0, batch_size))

        if len(batch) < batch_size:
            return None

        return np.stack(batch)

    def _build_train_op(self, loss_op):
        '''
        loss_op - a tf tensor with shape []. It represents a loss function.
        
        return - a tf op that tries to minimize the loss
        '''
        with tf.name_scope('train_op'):
            #if there is no decay rate
            if self._decay_rate is None:
                train_op = tf.train.AdamOptimizer(self._learning_rate, name='adam').minimize(loss_op)
                self._current_learning_rate = self._learning_rate

            else:
                global_step = tf.Variable(0, trainable=False)
                self._current_learning_rate = tf.train.exponential_decay(self._learning_rate, global_step,
                                                                         self._decay_step, self._decay_rate)
                train_op = tf.train.AdamOptimizer(self._current_learning_rate, name='adam').minimize(loss_op,
                                                                                                     global_step=global_step)
                self._global_step = global_step

            tf.summary.scalar('learning rate', self._current_learning_rate,
                              collections = [TFModel.TRAINING_SUMMARY])
        return train_op

    def _build_validation_precision_ph(self):
        with tf.name_scope('validation_precision_ph'):
            validation_prec_ph = tf.placeholder(tf.float32, [])
            tf.summary.scalar('validation precision', validation_prec_ph,
                              collections=[TFModel.VALIDATION_SUMMARY])

        return validation_prec_ph
    
    def _initialize(self, log_path, tf_model):
        '''
        log_path - a string. the path to the log directory
        tf_model - a TFModel object

        initialize the model controller so that it is ready to train the given model
        '''
        self._tf_model = tf_model
        self._loss_op   = tf_model.get_loss_op()
        self._eval_op   = tf_model.get_evaluation_op()
        self._train_op  = self._build_train_op(self._loss_op)
        self._validation_prec_ph = self._build_validation_precision_ph()
        
        self._saver = tf.train.Saver()
        self._training_summary_op = tf.summary.merge_all(key=TFModel.TRAINING_SUMMARY)
        self._validation_summary_op = tf.summary.merge_all(key=TFModel.VALIDATION_SUMMARY)
        self._writer = tf.summary.FileWriter(log_path, graph=tf.get_default_graph())

        return
    
    def _run_training_epoch(self, sess, output_file, training_data_set):
        '''
        sess - a tf Session
        training_data_set - a DataSet object

        run all of the training data set through the training op
        '''
        while training_data_set.has_batch():
            #get the input and ground truth
            input_batch, gt_batch = training_data_set.next_batch()
            start = time.time()
            feed_dict = self._tf_model.get_feed_dict(input_batch, gt_batch)
            _, loss_value = sess.run([self._train_op, self._loss_op], feed_dict=feed_dict)
            duration = time.time() - start
                
            if self._batch % self._num_batches_per_summary == 0:
                #evaluate the model and run the training summary op
                precision, summary = sess.run([self._eval_op, self._training_summary_op], feed_dict=feed_dict)
                        
                #write the summary to tensorboard
                self._writer.add_summary(summary, self._batch)

                #print a summary to the console
                print 'Batch %d: loss = %.5f, precision = %.5f (%.3f sec)' % (self._batch, loss_value,
                                                                              precision, duration)
                                                                              
                if self._decay_rate is not None:
                    print '   global step: %d, learning rate: %f' % (sess.run(self._global_step),
                                                                     sess.run(self._current_learning_rate))
                sys.stdout.flush()

            if self._batch % self._num_batches_per_checkpoint == 0:
                print 'saving checkpoint to: %s' % output_file
                self._saver.save(sess, output_file, global_step=self._batch)

            self._batch += 1
            
        return
    
    def _evaluate_validation_set(self, sess, validation_data_set):
        '''
        sess - a tf Session
        validation_data_set - a DataSet object

        run the validation set through the train_op and record the precision
        '''
        print 'evaluating validation set...'
        start = time.time()
        total_precision = 0.0
        num_batches = 0
        
        while validation_data_set.has_batch():
            input_batch, gt_batch = validation_data_set.next_batch()

            feed_dict = self._tf_model.get_feed_dict(input_batch, gt_batch)
            precision = sess.run(self._eval_op, feed_dict=feed_dict)
            total_precision += precision
            num_batches += 1
            
        total_precision /= num_batches
        summary = sess.run(self._validation_summary_op,
                           feed_dict={self._validation_prec_ph: total_precision})
        self._writer.add_summary(summary, self._batch)

        print 'validation set precision: %0.5f (%.3f sec)' % (total_precision, time.time()-start)
        return

    def _display_training_parameters(self, sess, tf_model):
        '''
        sess - a tf Session
        tf_model - a TFModel object

        print a summary of the model parameters, together with training params like the learning rate
        '''
        print 'using a model with parameters:'
        print tf_model.get_static_variable_summary(sess)
        print 'max training epochs: ', self._max_training_epochs
        print 'num batches per summary: ' , self._num_batches_per_summary
        print 'num batches per checkpoint: ', self._num_batches_per_checkpoint
        print 'decay rate: ', self._decay_rate
        print 'decay step: ', self._decay_step
        print '\n'

        return

    def train(self, output_file, log_path, tf_model, training_data_set, validation_data_set,
              initialization_file=None):
        '''
        tf_model - a TFModel object
        input_data_iter  - an iterator of numpy arrays
        true_values_iter - an iterator of numpy arrays

        train the model and save it in the output file.
        '''
        print 'initializing training process...'
        print 'output file: ', output_file
        print 'log path   : ', log_path
        
        self._initialize(log_path, tf_model)

        #start a tf session
        with tf.Session() as sess:
            if initialization_file is None:
                sess.run(tf.global_variables_initializer())
            else:
                self._saver.restore(sess, initialization_file)

            #print a message containing the static training and model parameters
            self._display_training_parameters(sess, tf_model)
            
            self._epoch = 0
            self._batch  = 0
            
            print 'training...'
            while self._epoch < self._max_training_epochs:
                print '--------------------------------'
                print 'Epoch: %d' % self._epoch
                
                training_data_set.initialize_epoch()
                if validation_data_set is not None:
                    validation_data_set.initialize_epoch()

                self._run_training_epoch(sess, output_file, training_data_set)

                if validation_data_set is not None:
                    self._evaluate_validation_set(sess, validation_data_set)

                self._epoch += 1

            print 'done. saving to: ', output_file

            #save the variables to a file
            self._saver.save(sess, output_file)

        return

    def predict(self, model_parameter_file, tf_model, input_data_iter):
        '''
        model_parameter_file - the filename of a file containing the model parameters
        tf_model - a TFModel object
        input_data_iter - an iterator of numpy arrays

        return - a numpy array with shape (num input arrays, ...)
        '''
        prediction_op = tf_model.get_prediction_op()
        saver = tf.train.Saver()

        prediction_batches = []
        
        with tf.Session() as sess:
            saver.restore(sess, model_parameter_file)

            #print a message containing the static training and model parameters
            self._display_training_parameters(sess, tf_model)

            while True:
                input_data_batch = self._get_batch(input_data_iter, tf_model.get_batch_size())
                if input_data_batch is None:
                    break
                
                feed_dict = tf_model.get_feed_dict(input_data_batch)
                prediction_batch = sess.run(prediction_op, feed_dict=feed_dict)
                prediction_batches.append(prediction_batch)

            predictions = np.vstack(prediction_batches)

        return predictions
            

