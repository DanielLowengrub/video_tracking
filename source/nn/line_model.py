import tensorflow as tf
from .tf_model import TFModel

class LineModel(TFModel):
    '''
    This is a simple model that fits pairs (x,y) to a line: y = ax +b.
    It is used for debugging purposes.
    '''

    def __init__(self, batch_size, evaluation_thresh):
        super(LineModel, self).__init__(batch_size)
        self._evaluation_thresh = evaluation_thresh

        self._build_graph()

    def _build_placeholders(self):
        self._x_ph          = tf.placeholder(tf.float32, [self._batch_size])
        self._true_y_ph = tf.placeholder(tf.float32, [self._batch_size])
        return
    
    def _build_model_op(self):
        '''
        Build a simple linear model: y = ax + b. Its variables are a and b.
        '''
        self._a = tf.Variable([1.0], name='a')
        self._b = tf.Variable([0.0], name='b')

        self._y = tf.add(tf.multiply(self._a,self._x_ph), self._b)

        return

    def _build_loss_op(self):
        print 'building loss op...'
        self._loss_op = tf.reduce_mean(tf.pow(self._y - self._true_y_ph, 2), name='loss')
        print 'loss op: ', self._loss_op
        return

    def _build_evaluation_op(self):
        is_correct = tf.less(tf.abs(self._y - self._true_y_ph), self._evaluation_thresh)
        self._evaluation_op = tf.div(tf.reduce_sum(tf.to_int32(is_correct)), self._batch_size, name='evaluation')
        return

    def _build_prediction_op(self):
        self._prediction_op = self._y
        return

    def get_feed_dict(self, input_data_batch, true_values_batch=None):
        feed_dict = dict()
        feed_dict[self._x_ph] = input_data_batch

        if true_values_batch is not None:
            feed_dict[self._true_y_ph] = true_values_batch

        return feed_dict
        
