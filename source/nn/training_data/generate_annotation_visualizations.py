import numpy as np
import cv2
import os
import re
from ...preprocessing_full.annotation_training_data_controller.annotation_training_data_controller import AnnotationTrainingValues
from ...auxillary import mask_operations
#OUTPUT_PARENT_DIRECTORY = '/Users/daniel/Documents/soccer/output'
OUTPUT_PARENT_DIRECTORY = '/home/daniel/soccer/output'


NN_PARENT_DIRECTORY = os.path.join(OUTPUT_PARENT_DIRECTORY, 'nn_training_data', 'image_annotation', 'dataset_0')
NN_IMAGE_DIRECTORY  = os.path.join(NN_PARENT_DIRECTORY, 'image')
NN_TRUE_DIRECTORY = os.path.join(NN_PARENT_DIRECTORY, 'true')
NN_TRUE_VISUALIZATION_DIRECTORY = os.path.join(NN_PARENT_DIRECTORY, 'true_visualization')

LABEL_COLOR_DICT = {AnnotationTrainingValues.BACKGROUND: (0,0,0),
                    AnnotationTrainingValues.GRASS:      (0,255,0),
                    AnnotationTrainingValues.SIDELINE:   (255,255,255),
                    AnnotationTrainingValues.PLAYER:     (255,0,0),
                    AnnotationTrainingValues.UNKNOWN:    (127, 127, 127)}

ALPHA = 0.7
IMG_REGEX = re.compile('(.*)\.png')

def generate_visualization(image, true_annotation):
    visualization = image.copy()
    
    for label, color in LABEL_COLOR_DICT.iteritems():
        mask = (true_annotation == label)
        mask_operations.draw_mask(visualization, mask, color, ALPHA)

    return visualization

def generate_visualizations(image_dir, true_dir, true_visualization_dir):
    #load the images and true annotations
    for img_basename in os.listdir(image_dir):
        m = IMG_REGEX.match(img_basename)

        #if this is not an image file
        if m is None:
            continue

        basename_prefix = m.groups()[0]
        img_filename = os.path.join(image_dir, img_basename)
        true_filename = os.path.join(true_dir, basename_prefix + '.npy')
        true_visualization_filename = os.path.join(true_visualization_dir, basename_prefix + '.png')

        image = cv2.imread(img_filename, 1)
        true_annotation = np.load(true_filename)
        true_visualization = generate_visualization(image, true_annotation)

        cv2.imwrite(true_visualization_filename, true_visualization)

    return

if __name__ == '__main__':
    generate_visualizations(NN_IMAGE_DIRECTORY, NN_TRUE_DIRECTORY, NN_TRUE_VISUALIZATION_DIRECTORY)
    
