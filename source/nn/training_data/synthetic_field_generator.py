class SyntheticFieldGenerator(object):
    '''
    This is used to generate a top down view of a soccer field (in color).
    '''

    def __init__(self, soccer_field, sideline_thickness, grass_means, grass_stds, sideline_mean, sideline_std):
        '''
        soccer_field - a SoccerFieldGeometry object
        grass_means - a np array with shape (2,3)
        grass_stds - a np array with length (2,3)
        sideline_mean - a np array with shape (3,)
        sideline_std - a np array with length (3,)
        '''
        self._soccer_field = soccer_field
        self._grass_means = grass_means

    def generate_soccer_field_image(self):
        '''
        return a numpy array with shape (soccer_field.height, soccer_field.width, 3)
        '''

class SyntheticBackgroundGenerator(object):
    '''
    This is used to generate an image that is filled with a soccer field background.
    '''

    def __init__(self, image_shape, bg_means, bg_stds):
        '''
        image_shape - a tuple of ints (height, width)
        bg_means - a numpy array with shape (n,3)
        bg_stds  - a numpy array with shape (n,3)
        '''

    def generate_background_image(self):
        '''
        return a numpy array with shape image_shape[::-1].
        '''
