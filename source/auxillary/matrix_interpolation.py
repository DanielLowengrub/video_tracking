import numpy as np
from scipy.linalg import sqrtm

def interpolate_between_invertible_matrices(A, B, alpha, precision):
    '''
    A, B - invertible matrices given as numpy arrays.
    alpha - a float between 0 and 1
    precision - an integer

    We return an approximation to
    e^(alpha*log(A) + (1-alpha)*log(B)

    which still guarantees that if A = B, then the output is always equal to A and B.

    To do this, we first find the smallest k such that k*2^(-N) < alpha. Set l = 2^N - k. We return:
    e^(k*2^(-N)*log(A) + l*2^(-N)*log(B))

    To compute this, let A' be the matrix obtained by taking the square root of A N times. B' is obtained similarly. We return:
    
    (A')^k * (B')^l
    '''
    N = precision
    
    k = int( (2**N) * alpha )
    l = 2**N - k

    for i in range(N):
        A = sqrtm(A)
        B = sqrtm(B)

    A = np.matrix(A)
    B = np.matrix(B)

    print '----------------------'
    print 'interpolating between:'
    print A
    print 'and'
    print B
    print 'with alpha %f and precision %d. we got: ' % (alpha, precision)
    print 'k = %d, l = %d' % (k,l)
    interpolated_matrix = (A**k) * (B**l)

    print interpolated_matrix
    
    return interpolated_matrix
