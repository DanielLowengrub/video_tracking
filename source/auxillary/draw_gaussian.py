import numpy as np
import cv2

def draw_transformed_gaussian(image_shape, transformation_matrix, center, alpha):
    '''
    image_shape: a tuple (n,m)
    transformation_matrix: a transformation matrix M from image coordinates to a different coordinate system in which the gaussian is defined.
    center: a length 2 numpy vector representing the center of the gaussian
    alpha: a negative number representing the std of the gaussian

    We return a numpy matrix with shape (n,m) and type np.float64. At pixel (n,m) we store the value:
    
    e ^ (alpha * |M(m,n) - center|
    
    Note that we've exchanged the m and n because the pixel at image[n][m] corresponds to position (m,n). I.e, 
    the x axis runs across the top of the image.
    '''
    
    #get a list of all of the coordinates in the image so that we can translate them to absolute coordinates.
    xx = np.arange(image_shape[0])
    yy = np.arange(image_shape[1])
    image_coordinates = np.float32(np.dstack(np.meshgrid(yy, xx))).reshape(-1, 1, 2)
    transformed_coordinates = cv2.perspectiveTransform(image_coordinates, transformation_matrix).reshape((-1,2))

    #now get the distance squared of each coordinate from the center
    vector_to_center = transformed_coordinates - center
    distance_squared = (vector_to_center * vector_to_center).sum(axis=1)

    #we now compute e^(alpha * dist_sq) for each coord
    probabilities = np.exp(alpha * distance_squared)

    #reshape the probabilities to the shape of the image
    image_position_probabilities = probabilities.reshape(image_shape[:2])
    
    return image_position_probabilities
    
