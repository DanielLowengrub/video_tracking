import numpy as np
import cv2
from .mask_operations import convert_to_cv_mask
#########################
# A collection of functions that are useful for manipulating histograms
########################

def get_normalized_hs_histogram(image, mask=None, nbins=(10,10)):
    '''
    image - a numpy array of shape (n,m,3) and type np.uint8
    mask  - a numpy array of shape (n,m) and type np.float32

    return a numpy array of shape (nbins, nbins) and type np.float32.
    This 2d array is a 2d histogram of the masked part of the image. This histogram is normalized to 1.
    '''

    if mask is None:
        mask = np.ones(image.shape[:2])

    if mask.sum() == 0:
        print 'image shape: ', image.shape
        print 'mask area: ', mask.sum()
    
    cv_mask = convert_to_cv_mask(mask)
    
    #convert the image to HSV space
    image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    
    #compute the histogram using the HS channels
    hue_range = [0, 180]
    saturation_range = [0,256]
    
    hist = cv2.calcHist([image], [0,1], cv_mask, nbins, hue_range + saturation_range)
    hist /= np.linalg.norm(hist)
    
    return hist
    
