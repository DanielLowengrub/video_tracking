import numpy as np

#############################
# Here we provide a simple implementation of image equalization.
# We don't use the one in opencv since it doesn't give an option of using a mask.
#############################

def equalize_image(gs_image, mask=None):
    '''
    gs_image - a numpy array of shape (n,m) and type np.uint8 corresponding to a greyscale image
    mask - a numpy array of shape (n,m) and type np.float64

    Equalize the image using only the pixels whose mask value is greater than 0.
    '''
    if mask is None:
        mask = np.ones(gs_image.shape, np.float64)
        
    #first get a list of all values that are in the mask
    pixel_values = gs_image[mask > 0]

    #now create the histogram using 256 bins.
    nbins = 256
    hist, bins = np.histogram(pixel_values, nbins, normed=True)

    #get the cumulative distribution function
    cdf = hist.cumsum()

    #normalize the cdf so that the largest value is 255
    cdf = 255 * cdf / cdf[-1]

    #equalize the pixel values by sending a pixel value v to cdf(v).
    equalized_pixel_values = np.interp(pixel_values, bins[:-1], cdf)

    #update the image using these new pixel values
    gs_image[mask>0] = equalized_pixel_values

    return
