import collections

class IteratorLen(collections.Iterator):
    '''
    This represents an iterator which has a known length.
    It can be used like an iterator, but also implements the __len__ method

    For example, let x = IteratorLen(3, iter([1,2,3]))
    then len(x) = 3 and list(x) = [1,2,3]
    '''

    def __init__(self, length, iterator):
        '''
        length - an integer
        iterator - an iterator containing length objects
        '''

        self._length = length
        self._iterator = iterator

    @classmethod
    def from_list(cls, l):
        '''
        l - a list

        return - an IteratorLen object whose length is the length of the list and whose iterator is the list iterator.
        '''
        return cls(len(l), iter(l))
    
    def __len__(self):
        return self._length

    def next(self):
        #print 'getting next elem of iterator: ', self._iterator
        return next(self._iterator)
