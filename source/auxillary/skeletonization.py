import numpy as np
import cv2
from scipy.ndimage import morphology

def compute_skeleton(mask, kernel_radius=3, thickness_map=False):
    '''
    mask - a numpy array with shape (n,m) and type np.bool
    return - a numpy array with shape (n,m) and type np.bool. It represents the skeleton of the input mask.
    '''
    kernel = np.ones((kernel_radius,kernel_radius), np.bool)
    
    #initialize the skeleton to be empty
    skel = np.zeros(mask.shape, np.int32)

    thickness = 1
    while np.any(mask):
        #subtract the opening of the mask from the mask. The idea is that if there is a thin line in the mask,
        #then it will disappear in the opening. So after subtracting the opening, the thin line will remain.
        opening = morphology.binary_opening(mask, kernel)
        mask_minus_opening = np.logical_and(mask, np.logical_not(opening))

        #add these pixels to the skeleton
        skel[mask_minus_opening] = thickness

        #erode the mask. this turns thick lines into thinner lines.
        mask = morphology.binary_erosion(mask, kernel)
        thickness += 1

    if not thickness_map:
        skel = skel > 0

    return skel
        
    
