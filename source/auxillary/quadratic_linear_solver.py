import numpy as np
from numpy.polynomial.polynomial import polyval, polymul, polyroots
import itertools

####################################
# This file solves a system of equations with the following form:
# Fix some n. For j = 0,...,m we have:
#
# F_{j,0}(r,s_j) = Q_{j,0}(r) + a_{j,0} * s_j = 0
# ...
# F_{j,n}(r,s_j) = Q_{j,n}(r) + a_{j,n} * s_j = 0
#
# Where Q_{i,j}(r) = ar^2 + br + c is a quadratic function in r.
#
# We return the numbers (r, s_0,...,s_m) that minimize the sum of squares \sum_j\sum_i (F_{i,j}^2)
####################################

def compute_G(Q, alpha):
    '''
    Q - a numpy array with size (n,3). For each n, Q[n] represents a quadratic function in one variable
    alpha - a numpy array of length n

    We use the equation (d/ds) \sum_i (F[i])^2 = (\sum_i alpha[i]^2)*s + (\sum_i alpha[i]*Q[i]) = 0
    To express s in terms of Q. 
    Specifically s is equal to the quadratic form:
    s = G(r) = (-1 / (\sum_i alpha[i]^2)) * (\sum_i alpha[i]*Q[i])

    Return G which is a numpy array with length 3
    '''

    # print 'computing G with'
    # print 'Q:'
    # print Q
    # print 'alpha:'
    # print alpha
    
    alpha_sum_squares = (alpha * alpha).sum()

    # print '   alpha_sum_squares:'
    # print alpha_sum_squares

    #this is an array with length 3.
    #alpha_Q = \sum_i (alpha[i] * Q[i]) which is a length 3 vector
    alpha_Q = (alpha[:,np.newaxis] * Q).sum(axis=0)

    # print '   sum_i alpha[i]*Q[i]:'
    # print alpha_Q
    
    G =  -alpha_Q / alpha_sum_squares

    # print '   G:'
    # print G

    return G

def compute_F(Q, alpha, G):
    '''
    Q - a numpy array with size (n,3). For each n, Q[n] represents a quadratic function in one variable
    alpha - a numpy array of length n
    G - a numpy array with length 3. This represents a polynomial in r which is equal to s.

    We return and array F with shape (n,3) where: 
    F[i](r) = Q[i](r) + alpha[i]*G(r) 
    '''
    # print 'computing F with:'
    # print 'Q:'
    # print Q
    # print 'alpha:'
    # print alpha
    # print 'G:'
    # print G
    
    #multiply each element of alpha with G to get an array with shape (n,3)
    alpha_G = alpha[:,np.newaxis] * G
    # print '   alpha * G:'
    # print alpha_G
    
    #we can now express F[i] as Q[i](r) + alpha[i] * G(r)
    #this is a numpy array with shape (n,3)
    F = Q + (alpha_G)

    # print '   F:'
    # print F

    return F

def compute_dF(Q):
    '''
    Q - a numpy array with size (n,3). 
    For each n, Q[n] represents a quadratic function in one variable

    We return a numpy array with shape (n,2), where each row represents the derivative of the quadratic equation in that row.
    '''
    # print 'computing dF with Q:'
    # print Q
    
    dF = np.hstack([Q[:,1][:,np.newaxis], 2*Q[:,2][:,np.newaxis]])

    # print '   dF:'
    # print dF

    return dF

def padded_polymul(p, q):
    '''
    p,q - two one dimensional numpy arrays
    We return an array with length (len(p)-1) + (len(q)-1) which represents the product of the polynomials represented by p and q.
    If deg(pq) < deg(p) + deg(q), we pad the result of polymul(p,q) with zeros on the right.
    For example, if p = [1,0] and q = [1,1] we return [1, 1, 0]
    '''

    # print 'multiplying %s and %s' % (str(p), str(q))
    
    pq = polymul(p,q)

    # print 'pq: ', pq
    deg_pq = len(p) + len(q) - 2

    padding = np.zeros(deg_pq - (len(pq) - 1))
    # print 'padding: ', padding
    return np.hstack([pq, padding])

def solve_least_squares(quadratic_linear_systems, epsilon=10**(0)):
    '''
    quadratic_linear_systems - a list of tuples of the form (Q,alpha) where:
    Q - a numpy array with size (n,3). For each n, Q[n] represents a quadratic function in one variable
    alpha - a numpy array of length n

    Each quadratic linear system gives us a system of equations: F[i](r,s) = Q[i](r) + alpha[i]*s = 0

    We return an array with shape (number of solutions, 1 + number of systems). E
    each row is of the form (r, s_0, ..., s_m) and satisfies \sum_j \sum_i (F_{j,i})^2 < epsilon
    '''

    #alpha = alpha.reshape(alpha.shape + (1,))
    #print 'solving least squares with:'
    #print 'Q:'
    #print Q
    #print 'alpha:'
    #print alpha
    #print 'shape of alpha: ', alpha.shape
    
    #m = Q.shape[0]
    #n = Q.shape[1]
    
    #for each system of equations, we differentiate the sum of squares by s and set the result to zero.
    #This allows us to express s in terms of the Q. We thus get one quadratic form G(r) for each system of equations.
    #This is a numpy array with shape (j,3)
    Gs = [compute_G(Q,alpha) for Q,alpha in quadratic_linear_systems]

    #print '   G:'
    #print G

    #for each system of equations, we substitute G(r) with s to get a system of quadratic functions in the variable r
    Fs = [compute_F(Q, alpha, G) for (Q,alpha),G in zip(quadratic_linear_systems, Gs)]
    
    #for each system of equations, differentiate the sum of squares by r and set it to zero to get a degree three equation in r
    #first differentiate each F by r. This is a numpy array with shape (n,2)
    dFs = [compute_dF(Q) for Q,alpha in quadratic_linear_systems]

    #print '   dF:'
    #print dF

    #for each system of equations FdF[i] is the product of the degree 2 polynomial F[i] and the degree 1 polynomial dF[i]
    FdFs = [np.array([padded_polymul(f, df) for f,df in zip(F,dF)]) for F,dF in zip(Fs, dFs)]

    # print '   FdF:'
    # print '\n'.join(map(str,FdFs))
    
    #we get a degree 3 polynomial by taking the FdFs over all systems of equations
    #for each FdF, take the sum along the columns to get a single array of length 4.
    r_polys = [FdF.sum(axis=0) for FdF in FdFs]

    #now take the sum of all of the degree 4 polynomials
    #I.e, stack up the coefficients and take the column wise sum
    r_poly = np.array(r_polys).sum(axis=0)

    # print '   r_poly:'
    # print r_poly
    
    #solve for r
    real_roots = map(np.real, filter(np.isreal, polyroots(r_poly)))

    # print '   we found the roots:'
    # print real_roots

    # print '   validating roots...'
    # for r in real_roots:
    #     print ' '*6 + 'for r=%f we have:' % r
    #     for i,F in enumerate(Fs:)
    #         print ' '*6 + 'F_%d(r) = ', np.dot(F, np.array([1,r,r**2]))
            
    #take the real root that minimizes the sum of squares
    #first construct a numpy array with shape (m,n,5) where F_squared[j,i] represents the polynomial F[i,j]^2
    #F_squared = np.array([[padded_polymul(F[j,i], F[j,i]) for i in range(n)] for j in range(m)])

    #print '   F*F:'
    #print F_squared
    
    #F_squared = F_squared.reshape((-1, 5))

    #this is a length 5 array representing the sum of all of the polynomials in F_squared
    #F_sum_squares = F_squared.sum(axis=0)

    #print '   sum_j sum_i F[j,i]^2 :'
    #print F_sum_squares

    #for each root r, compute the \sum_{systems of equations} \sum_i F[i](r)^2
    #this is a numpy array of length (num roots)
    sum_squares = np.array([sum(itertools.chain(*([polyval(r, f)**2 for f in F] for F in Fs))) for r in real_roots])
    
    #choose the r that minimizes F_sum_squares
    #print '\sum_{systems of equations} \sum_i F[i](r)^2:'
    #r = real_roots[0]
    #print 'the array: ', list(itertools.chain(*([polyval(r, f)**2 for f in F] for F in Fs)))
    #print 'the sum: ',np.sum(np.array([[polyval(r, f)**2 for f in F] for F in Fs]).flatten())
    #print sum_squares
    #print [polyval(x, F_sum_squares) for x in real_roots]
    
    valid_r = np.array([r for r,ss in zip(real_roots, sum_squares) if ss < epsilon])

    #now use each of the valid r's to compute the values of s=G(r) in each system of equations
    #this is an array with shape (number of valid r's, 1 + number of systems of equations)
    r_and_s = np.array([[r] + [polyval(r, G) for G in Gs] for r in valid_r])

    return r_and_s
    
