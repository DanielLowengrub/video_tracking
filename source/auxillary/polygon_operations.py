import numpy as np
import itertools
from ..auxillary import planar_geometry
from ..auxillary.fit_points_to_shape.line import Line
"""
This file contains methods for manipulating polygons.
Polygons are represented by a list of tuple (a,b,c) representing lines: ax + by + c = 0.
The polygon is the intersection of the set of halfspaces: ax + by + c >= 0
"""

def shape_to_polygon(height, width):
    '''
    height, width - floats
    return - a numpy array with shape (4,3). It represents the polygon corresponding to the rectangle with given
       height and width, whose top left corner is (0,0)
    '''
    min_x, max_x = 0, width
    min_y, max_y = 0, height

    #the left line is: x - min_x = 0
    left_line = np.array([1, 0, -min_x])

    #the right line is: max_x - x = 0
    right_line = np.array([-1, 0, max_x])

    #the top line is: y - min_y = 0
    top_line = np.array([0, 1, -min_y])

    #the bottom line is: max_y - y = 0
    bottom_line = np.array([0, -1, max_y])

    return np.vstack([left_line, top_line, right_line, bottom_line])

def compute_visible_polygon(H, image_shape):
    '''
    H - a numpy array with shape (3,3). It represents a homography from a plane in the world to the image plane.
    image_shape - a tuple (height, width)

    return - a numpy array with shape (n, 3). It represents the polygon on the world plane which contains the
       points in the plane that are visible in the image.
    '''
    
    #first compute the polygon surrounding the image
    image_polygon = shape_to_polygon(*image_shape)

    #map the polygon to the world plane
    visible_polygon = np.matmul(H.transpose(), image_polygon.transpose()).transpose()

    #add the line at infinity
    visible_polygon = np.vstack([visible_polygon, H[2:]])
    
    #find the anchor point
    H_inv = np.linalg.inv(H)
    image_center = np.array([image_shape[1], image_shape[0]]) / 2.0
    world_anchor = planar_geometry.apply_homography_to_point(H_inv, image_center)
    projective_world_anchor = np.append(world_anchor, 1)
    
    #make sure that the lines have the right sign
    polygon_signs = np.sign(np.matmul(visible_polygon, projective_world_anchor))
    visible_polygon *= polygon_signs.reshape((-1,1))

    return visible_polygon

def point_in_polygon(point, polygon):
    '''
    polygon - a numpy array with shape (num lines, 3)
    point - a numpy array with shape (2,)

    return - True iff the point is in the polygon
    '''
    for l in polygon:
        if l[0]*point[0] + l[1]*point[1] + l[2] < -planar_geometry.EPSILON:
            return False

    return True

def convert_to_points(polygon):
    '''
    polygon - a numpy array with shape (num lines, 3)
    return - a numpy array with shape (num points, 2) which represents a list of points in clockwise order
       whose convex hull is the polygon
    '''

    points = []
    lines = map(Line.from_parametric_representation, polygon)
    
    #for each pair of lines, compute the intersection point
    for line_pair in itertools.combinations(lines, 2):
        p = planar_geometry.affine_line_line_intersection(*line_pair)
        if p is not None and point_in_polygon(p, polygon):
            points.append(p)

    if len(points) == 0:
        return np.empty((0,2), np.float32)
    
    #sort the points in clockwise order
    points = np.vstack(points)
    center = np.mean(points, axis=0)
    thetas = [np.arctan2(p[1]-center[1], p[0]-center[0]) for p in points]
    points = points[np.argsort(thetas)]

    return points

def compute_bounded_visibility_polygon_points(H, image_shape, world_shape):
    '''
    H - a numpy array with shape (3,3). It represents a homography from a plane in the world to the image plane.
    image_shape - a tuple (height, width)
    world_shape - a tuple (height, width)

    return - a numpy array with shape (num points, 2) representing the part of the world rectangle that is visible
       in the image
    '''

    visible_polygon = compute_visible_polygon(H, image_shape)
    world_polygon   = shape_to_polygon(*world_shape)
    visible_polygon = np.vstack([visible_polygon, world_polygon])

    visible_poly_points = convert_to_points(visible_polygon)

    return visible_poly_points

