import numpy as np
import cv2
from rectangle import Rectangle
from ..auxillary import planar_geometry, np_operations
from ..auxillary.fit_points_to_shape.line import Line
from ...testers.auxillary import primitive_drawing

########################
# This file contains a collection of methods that project shapes in world space onto an image using a camera matrix.
#######################
PLAYER_HEIGHT = 13
PLAYER_WIDTH = 4

def project_player_to_image(camera_matrix, player_absolute_position):
    '''
    camera_matrix - a CameraMatrix object
    player_absolute_position - a length 2 numpy vector

    Return an Ellipse object corresponding to the projection of the player onto the image
    '''

    #get the position and axes of an ellipsoid containing the player
    ellipsoid_axes = np.array([PLAYER_WIDTH / 2.0, PLAYER_WIDTH / 2.0, PLAYER_HEIGHT / 2.0], np.float32)
    ellipsoid_position = np.array([player_absolute_position[0], player_absolute_position[1], ellipsoid_axes[2]])

    #use the camera matrix to project this ellipsoid to the image
    return camera_matrix.project_ellipsoid_to_image(ellipsoid_position, ellipsoid_axes)

def _get_player_ellipsoids_inv(player_absolute_positions):
    '''
    player_absolute_position - a numpy array with shape (n,2) which represents a list of absolute position
    
    Return a shape (n,4,4) numpy array. The i-th 4x4 matrix is Q^-1, where Q is the parametric rep of the player ellipsoid
    positioned at absolute_player_positions[i]
    '''
    #first compute the inverse of the 4x4 translation matrix from the given position to the origin
    #i.e, it is the translation matrix TO the center of the player ellipsoid
    T_inv = np.zeros((len(player_absolute_positions),4,4), np.float32)
    T_inv[:,(0,1,2,3),(0,1,2,3)] = 1.0
    T_inv[:,:2,3] = player_absolute_positions
    T_inv[:,2,3] = PLAYER_HEIGHT / 2.0

    #compute the inverse of the 4x4 parameterization of the ellipsoid at the origin
    axes = np.array([PLAYER_WIDTH / 2.0, PLAYER_WIDTH / 2.0, PLAYER_HEIGHT / 2.0], np.float32)
    Q_inv = np.diag([axes[0]**2, axes[1]**2, axes[2]**2, -1])

    #the inverse of the i-th ellipsoid parametrization is (Ti^tr * Q * Ti)^-1 = Ti^-1 * Q^-1 * Ti^-tr
    T_inv_tr = np.transpose(T_inv, (0,2,1))
    return np.float32(np.matmul(T_inv, np.matmul(Q_inv, T_inv_tr)))

def _get_spheres_inv(radius, centers):
    '''
    radius - a float
    centers - a numpy array with shape (num spheres, 3)

    return - a numpy array with shape (num spheres, 4, 4). The i-th 4x4 matrix is the *inverse* of the 4x4 matrix representing
    a sphere with center centers[i] and the given radius.
    '''

    #first compute the inverses of the 4x4 translation matrix from the given positions to the origin
    #i.e, it is the translation matrix TO the centers of the spheres
    T_inv = np.zeros((len(centers),4,4), np.float32)
    T_inv[:,(0,1,2,3),(0,1,2,3)] = 1.0
    T_inv[:,:3,3] = centers

    #compute the inverse of the 4x4 parameterization of a sphere at the origin with the specified radius
    Q_inv = np.diag([1, 1, 1, -1.0 / (radius**2)])

    #the inverse of the i-th ellipsoid parametrization is (Ti^tr * Q * Ti)^-1 = Ti^-1 * Q^-1 * Ti^-tr
    T_inv_tr = np.transpose(T_inv, (0,2,1))
    return np.float32(np.matmul(T_inv, np.matmul(Q_inv, T_inv_tr)))

def _get_cylinder(absolute_position, axes):
    '''
    absolute_position - a length 2 numpy array (cx,cy)
    axes - a length 2 numpy array (ax,ay)

    return - a 4x4 matrix representing the cylinder: ((x - cx)/ax)^2 + ((y - cy)/ay)^2 = 1 in R^3
    '''
    #Let T be the translation matrix that sends (cx,cy,0) to (0,0,0).
    #Let S be the matrix diag(1/ax^2, 1/ay^2, 0, 1)
    #Then, the 4x4 matrix representing the cylinder is: T^t * S * T.

    T = np.zeros((4,4), np.float64)
    T[(0,1,2,3),(0,1,2,3)] = 1.0
    T[:2,3] = -absolute_position

    S = np.diag(np.hstack([1.0 / (axes**2), np.array([0,-1])])).astype(np.float64)
    
    return np.matmul(T.transpose(), np.matmul(S, T))
    
def _project_ellipsoids_inv_to_image(camera_matrix, ellipsoids):
    '''
    camera_matrix - a CameraMatrix object
    ellipsoids - a numpy array with shape (num ellipsoids, 4, 4)

    return - a numpy array with shape (num ellipsoids, 3, 3) which represents the projections of the *inverses* of the
             ellipsoids to the image.

    Note: If you want to project a list of ellipsoids Q to the image, you must pass Q_inv to this method, where Q_inv
    stores the inverses to each of the matrices in Q.
    '''
    #if an ellipsoid is rep by Q, and the camera matrix is P, the projection onto the image is represented by:
    #(P * Q^-1 * P^tr)^-1
    P = camera_matrix.get_numpy_matrix()
    ellipses = np.linalg.inv(np.matmul(P, np.matmul(ellipsoids, P.transpose())))

    return ellipses

def project_players_to_image(camera_matrix, player_absolute_positions):
    '''
    camera_matrix - a CameraMatrix object
    player_absolute_positions - a numpy array with shape (n,2) which represents a list of absolute position

    Return a numpy array with length (n,3,3) which represents the projections of players standing at each of the positions
    onto the image.
    '''

    #first construct a list of player ellipsoids positioned at the player positions
    #since we actually need the *inverse* of the ellipsoids, we compute the inverses directly
    Q_inv = _get_player_ellipsoids_inv(player_absolute_positions)

    #if the ellipsoid is rep by Q, and the camera matrix is P, the projection onto the image is the rep by:
    #(P * Q^-1 * P^tr)^-1
    P = camera_matrix.get_numpy_matrix()
    ellipses = np.linalg.inv(np.matmul(P, np.matmul(Q_inv, P.transpose())))

    return ellipses

def project_spheres_to_image(camera_matrix, radius, centers):
    '''
    camera_matrix a CameraMatrix object
    radius - a float. the radius of the spheres
    csnters - a numpy array with shape (num spheres, 3).

    return a numpy array with shape (num spheres, 3, 3) which represents the projections of the spheres onto the image.
    '''
    #first construct the inverses of the 3x3 matrices representing the spheres
    Q_inv = _get_spheres_inv(radius, centers)

    ellipses = _project_ellipsoids_inv_to_image(camera_matrix, Q_inv)

    return ellipses

############
#BUG: Instead of projecting points to construct the rectangles, we should use the end points of the projected ellipses.
#
#Specifically, on each projected ellipse, find the axis that makes the largest angle with the line connecting the ellipse centers.
#Denote the axes by [v1,v2], [w1,w2]. possible swap v1 and v2 such that dist(v1,w1) < dist(v1,w2).
#Then draw the rectangle (v1, v2, w2, w1)
###########
# def _project_player_with_variance(image, camera_matrix, player_absolute_position, position_variance):
#     '''
#     image - a numpy array with shape (n,m,3) and type np.uint8
#     camera_matrix - a CameraMatrix object
#     player_absolute_position - a length 2 numpy vector
#     position_variance - a length 2 numpy vector

#     Return a numpy array of shape (n,m) and type np.float32

#     We create an "ellipse x I" (i.e, a cylinder whose top and bottom are ellipses) at the position of the players where the shape of the ellipse is determined by the variance.
#     We project the result to the image and return a mask of the result.

#     Let (x,y) be the position of the player in absolute coordinates and let (vx, vy) be the variance in the position. Let h be the player height.
#     Then, we define two ellipses:
#     E_bottom = ellipse on the {z=0} plane with center (x,y) and axes (vx,vy)
#     E_top = ellipse on the {z=h} plane with center (x,y) and axes (vx,vy)

#     and two rectangles
#     Rx = the contour (x-vx, y, 0), (x+vz, y, 0), (x+vx, y, h), (x-vx, y, h)
#     Rx = the contour (x, y-vy, 0), (x, y+vy, 0), (x, y+vy, h), (x, y-vy, h)

#     Then, we project both ellipses and both rectangles to the image. The union of the projections is our desired output.
#     '''

#     #first get the two ellipses
#     bottom_position = np.append(player_absolute_position, 0)
#     top_position = np.append(player_absolute_position, PLAYER_HEIGHT)
#     xy_axes = np.array([PLAYER_WIDTH, PLAYER_WIDTH], np.float32) + np.sqrt(position_variance)

#     #project the ellipses onto the image
#     bottom_ellipse = camera_matrix.project_ellipse_to_image(bottom_position, xy_axes)
#     top_ellipse = camera_matrix.project_ellipse_to_image(top_position, xy_axes)

#     #get the two rectangles
#     x_width = np.array([xy_axes[0], 0, 0])
#     x_rectangle_world_points = np.float32(np.vstack([bottom_position - x_width, bottom_position + x_width,
#                                                      top_position + x_width, top_position - x_width]))
#     y_width = np.array([0, xy_axes[1], 0])
#     y_rectangle_world_points = np.float32(np.vstack([bottom_position - y_width, bottom_position + y_width,
#                                                      top_position + y_width, top_position - y_width]))

#     #project the rectangle to the image
#     x_rectangle = camera_matrix.project_points_to_image(x_rectangle_world_points)
#     y_rectangle = camera_matrix.project_points_to_image(y_rectangle_world_points)

#     #now draw the ellipses and rectanges on the output mask
#     output = np.zeros(image.shape[:2], np.float32)

#     bottom_ellipse.compute_geometric_parameters()
#     center, axes, angle = bottom_ellipse.get_geometric_parameters(in_degrees=True)
#     cv2.ellipse(output, tuple(np.int32(center).tolist()), tuple(np.int32(axes).tolist()), angle, 0, 360, 1, -1, cv2.CV_AA)

#     top_ellipse.compute_geometric_parameters()
#     center, axes, angle = top_ellipse.get_geometric_parameters(in_degrees=True)
#     cv2.ellipse(output, tuple(np.int32(center).tolist()), tuple(np.int32(axes).tolist()), angle, 0, 360, 1, -1, cv2.CV_AA)

#     cv2.drawContours(output, [np.int32(x_rectangle.reshape((-1,1,2)))], -1, 1, -1)
#     cv2.drawContours(output, [np.int32(y_rectangle.reshape((-1,1,2)))], -1, 1, -1)

#     return output

def get_axis_with_largest_angle(ellipse, direction):
    '''
    ellipse - an ellipse object
    direction - a length 2 numpy vector

    return a numpy vector of length 2

    We find which axis makes a larger angle with the given direction.
    We return the direction of this axis, normalized such that (axis direction) dot (direction_perp) >= 0
    (direction_perp is direction rotated 90 degrees counter clockwise)
    '''

    direction_perp = np.array([-direction[1], direction[0]])
    center, axes, angle = ellipse.get_geometric_parameters()

    axis_directions = np.array([[np.cos(angle),  np.sin(angle)],
                                [-np.sin(angle), np.cos(angle)]])

    cos_angles = np.abs((axis_directions * direction).sum(axis=1))

    axis_direction_index = min(range(2), key=lambda x: cos_angles[x])
    axis_direction = axis_directions[axis_direction_index]

    #now orient this axis direction so that cos of the angle it makes with direction_perp is positive.
    if np.dot(axis_direction, direction_perp) < 0:
        axis_direction *= -1

    return axis_direction * axes[axis_direction_index]
    
    

def project_player_with_variance(image, camera_matrix, player_absolute_position, position_variance):
    '''
    image - a numpy array with shape (n,m,3) and type np.uint8
    camera_matrix - a CameraMatrix object
    player_absolute_position - a length 2 numpy vector
    position_variance - a length 2 numpy vector

    Return a tuple (mask, rectangle)
    mask - numpy array of shape (n,m) and type np.bool
    retangle - a Rectangle object with shape (n,m)

    We create an "ellipse x I" (i.e, a cylinder whose top and bottom are ellipses) at the position of the players where the shape of the ellipse is determined by the variance.
    We project the result to the image and return a mask of the result.

    Let (x,y) be the position of the player in absolute coordinates and let (vx, vy) be the variance in the position. Let h be the player height.
    Then, we define two ellipses:
    E_bottom = ellipse on the {z=0} plane with center (x,y) and axes (vx,vy)
    E_top = ellipse on the {z=h} plane with center (x,y) and axes (vx,vy)

    and project them to the image.

    Now let L be the line connecting the (projected) centers.
    Choose axes A1 on E1 and A2 on E2 which make the largest angle with L.

    Then, choose endpoints A1 = (v1,v2), A2=(w1,w2) such that dist(v1,w1) < dist(v1,w2)

    Finally, add the rectangle (v1,v2,w2,w1) to the output mask
    '''    
    #first get the two ellipses
    bottom_position = np.append(player_absolute_position, 0)
    top_position = np.append(player_absolute_position, PLAYER_HEIGHT)
    xy_axes = np.array([PLAYER_WIDTH, PLAYER_WIDTH], np.float32) + np.sqrt(position_variance)

    #project the ellipses onto the image
    bottom_ellipse = camera_matrix.project_ellipse_to_image(bottom_position, xy_axes)
    top_ellipse = camera_matrix.project_ellipse_to_image(top_position, xy_axes)
    bottom_ellipse.compute_geometric_parameters()
    top_ellipse.compute_geometric_parameters()

    bottom_center, bottom_axes, bottom_angle = bottom_ellipse.get_geometric_parameters(in_degrees=True)
    top_center, top_axes, top_angle = top_ellipse.get_geometric_parameters(in_degrees=True)

    L = top_center - bottom_center
    L = L / np.linalg.norm(L)
    
    #for each ellipse, find which of the axes makes a larger angle with the L between the centers
    bottom_axis_direction = get_axis_with_largest_angle(bottom_ellipse, L)
    top_axis_direction = get_axis_with_largest_angle(top_ellipse, L)

    #create a contour connecting these axes
    contour = np.vstack([bottom_center - bottom_axis_direction,
                         bottom_center + bottom_axis_direction,
                         top_center + top_axis_direction,
                         top_center - top_axis_direction])
    
    #create a Rectangle object that contains both the ellipses and the rectangle
    #to do this, first get axis aligned bounding boxes for each of the ellipses, and take their union
    bottom_bb = bottom_ellipse.get_bounding_rectangle()
    top_bb = top_ellipse.get_bounding_rectangle()
    player_rectangle = top_bb.union(bottom_bb)

    #make sure the player rectangle is on the image
    image_rectangle = Rectangle.from_array(image)
    player_rectangle = image_rectangle.intersection(player_rectangle)
    
    #now draw the ellipses and rectange on the player rectangle
    output = np.zeros(player_rectangle.get_shape(), np.float32)

    #translate the ellipses to the coordinate system of the player rectangle
    bottom_center -= player_rectangle.get_top_left()
    top_center    -= player_rectangle.get_top_left()
    cv2.ellipse(output, tuple(np.int32(bottom_center).tolist()), tuple(np.int32(bottom_axes).tolist()),
                bottom_angle, 0, 360, 1, -1, cv2.CV_AA)
    cv2.ellipse(output, tuple(np.int32(top_center).tolist()), tuple(np.int32(top_axes).tolist()),
                top_angle, 0, 360, 1, -1, cv2.CV_AA)

    #also translate the contour to the player rectangle coordinates, and draw it
    contour -= player_rectangle.get_top_left()
    cv2.drawContours(output, [np.int32(contour.reshape((-1,1,2)))], -1, 1, -1)

    output = output > 0
    
    return output, player_rectangle

def build_player_masks(camera, absolute_positions):
    '''
    camera - a CameraMatrix object
    absolute_positions - a numpy array with shape (num players, 2)

    return a tuple (rectangles, player_masks)
    rectangles - a list of Rectangle objects
    player_mask - a list of numpy arrays with type np.bool. The i-th player mask has the same shape as the i-th rectangle
    '''

    player_ellipses = project_players_to_image(camera, absolute_positions)
    
    #compute the bounding boxs of each of the ellipses
    #this is a np array with shape (n,2,2). each (2,2) matrix stores a rectangle as: [[left,top],[right,bottom]]
    ellipse_regions = planar_geometry.get_ellipse_regions(player_ellipses).astype(np.int32) #NEW!! (added int32)
    
    #compute masks for each of the ellipses
    player_masks = [planar_geometry.build_mask_from_ellipse_and_region(ellipse, ellipse_region)
                    for ellipse,ellipse_region in zip(player_ellipses, ellipse_regions)]

    #and build the rectangles
    player_rectangles = [Rectangle.from_top_left_and_shape(region[0], pm.shape)
                         for region,pm in zip(ellipse_regions, player_masks)]

    return player_rectangles, player_masks
        
def _extract_lines(B):
    '''
    B - a symmetric 3x3 matrix with rank 2.
    
    return - two vectors l1,l2 such that: B = l1^T*l2 + l2^T*l1

    Algorithm:
    1) It is not hard to prove that adj(B) = p^T*p where p=l1 x l2. So any non zero column of adjB is equal to p.
    2) Furthermore, 2*l1^T*l2 = B + S_p where S_p is the anti-symmetric matrix associated to p. Let:
       B1 = B + S_p. Then any non zero row of B1 is l2, and any non zero column is l1.
    '''

    adjB = np_operations.adjugate(B)

    
    #choose the column with the largest norm to be p
    best_column = np.argmax(np.linalg.norm(adjB, axis=0))
    p = adjB[:,best_column]
    Sp = np_operations.anti_symmetric(p)

    print 'p: ', p
    
    #now try to find an x such that B + x*Sp has rank 1.
    #to do this, find any non zero minor of B + x*Sp and take any of the roots of it's determinant
    poly_adj = np_operations.poly_adjugate(B, Sp)
    best_index = np.unravel_index(np.argmax(np.linalg.norm(poly_adj, axis=2)), (3,3))
    best_determinant = poly_adj[best_index]
    determinant_roots = np.poly1d(best_determinant).r

    print 'best determinant: ', best_determinant
    print 'roots: ', determinant_roots
    
    x = determinant_roots[0]

    B1 = B + x*Sp

    print 'B1: '
    print B1
    print 'detM00: ', (B1[1,1]*B1[2,2] - B1[1,2]*B1[2,1])
    best_column = np.argmax(np.linalg.norm(B1, axis=0))
    l1 = B1[:,best_column]

    #choose the row of B1 with the largest norm to be l2
    best_row = np.argmax(np.linalg.norm(B1, axis=1))
    l2 = B1[best_row,:]
    
    return l1,l2

def project_infinite_cylinder_to_image(camera_matrix, absolute_position, axes):
    '''
    camera - a CameraMatrix object
    position - a numpy array with length 2
    axes - a numpy array with length 2

    return - a tuple (L1, L2) of line objects.

    Let C be the cylinder ((x/axes[0])^2 + (y/axes[1])^2 = 1).
    The lines are the two lines whose union is the projection of the cylinder onto the image.

    We do this as follows:
    1) Compute the 4x4 matrix which represents the cylinder. This is a rank 3 matrix.
    2) The set of tangent planes to the cylinder is given by the equations: 
       (h^T*C^+*h, (0,0,1,0)*H) where C^+ is the pseudo-inverse.
    3) The set of tangent lines to the projection of the cylinder is given by the equations:
       (l^T*B^+*l, (0,0,1,0)*P^T*l) where B^+ = P*C^+*P^T
    4) The two lines are the projective intersection points of the conic B^+ and the line (0,0,1,0)*P^T.
    '''
    # print 'absolute position: ', absolute_position
    # print 'axes: ', axes
    
    #first get the 4x4 matrix representing the dual of C
    #this is the dual centered at the origin
    C = _get_cylinder(absolute_position, axes)    
    pinvC = np.linalg.pinv(C)

    #get the dual of the projection to the image
    P = camera_matrix.get_numpy_matrix()
    
    conic_eq = np.matmul(P, np.matmul(pinvC, P.transpose()))

    #compute (0,0,1,0) * P^T
    line_eq = np.matmul(np.array([0,0,1,0]), P.transpose())

    #the lines we want are the intersection points of the line and the conic
    lines = planar_geometry.projective_line_conic_intersection(line_eq, conic_eq)

    # print 'lines: ', lines
    
    if len(lines) != 2:
        raise RuntimeError('the projection of the cylinder is not traced by 2 lines!?')
    
    return tuple(Line.from_parametric_representation(l) for l in lines)

def project_cylinder_to_image(image, camera_matrix, absolute_position, axes, height):
    '''
    image - a numpy array with shape (n,m,3) and type np.uint8
    camera - a CameraMatrix object
    position - a numpy array with length 2
    axes - a numpy array with length 2
    height - a float

    return a tuple (rectangle, mask).
    rectangle - a Rectangle with shape (n',m')
    mask - a numpy array with shape (n',m') and type bool.

    Let C be the cylinder with the given height. It's base is an ellipse with the given axes whose center is the position.
    The rectangle contains the projection of the cylinder to the image. The mask represents the parts of the rectangle
    that are in the projection of the cylinder.
    '''
    absolute_position = absolute_position.astype(np.float64)
    axes = axes.astype(np.float64)

    # print 'absolute position: ', absolute_position
    # print 'axes: ', axes
    
    #project the base ellipse and the top ellipse
    bottom_position = np.append(absolute_position, 0)
    top_position = np.append(absolute_position, height)

    #project the ellipses onto the image
    bottom_ellipse = camera_matrix.project_ellipse_to_image(bottom_position, axes)
    top_ellipse = camera_matrix.project_ellipse_to_image(top_position, axes)
    bottom_ellipse.compute_geometric_parameters()
    top_ellipse.compute_geometric_parameters()
    
    
    #project the circumference of the cylinder to get two lines. The lines should be tangent to the ellipses.
    cylinder_lines = project_infinite_cylinder_to_image(camera_matrix, absolute_position, axes)

    # primitive_drawing.draw_ellipse(image, bottom_ellipse, (255,0,0))
    # primitive_drawing.draw_ellipse(image, top_ellipse, (255,0,0))
    # for l in cylinder_lines:
    #     primitive_drawing.draw_line(image, l, (255,0,0))

    #compute the tangency points of each of the lines with each of the ellipses
    L0_tangency_points = [planar_geometry.tangent_ellipse_intersection(cylinder_lines[0], E)
                          for E in (bottom_ellipse,top_ellipse)]
    
    L1_tangency_points = [planar_geometry.tangent_ellipse_intersection(cylinder_lines[1], E)
                          for E in (bottom_ellipse,top_ellipse)]
    

    tangency_points = np.vstack(L0_tangency_points + L1_tangency_points[::-1])
    contour = tangency_points.reshape((-1,1,2))
    
    # print 'tangency points: ', tangency_points
    
    # for p in tangency_points:
    #     primitive_drawing.draw_point(image, p, (0,255,0))
        
    # cv2.imshow('cylinder lines and ellipses', image)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    #create a rectangle that contains the two ellipses
    bottom_bb = bottom_ellipse.get_bounding_rectangle()
    top_bb = top_ellipse.get_bounding_rectangle()
    player_rectangle = top_bb.union(bottom_bb)

    #print 'player rectangle: ', player_rectangle
    
    #make sure the player rectangle is on the image
    image_rectangle = Rectangle.from_array(image)
    player_rectangle = image_rectangle.intersection(player_rectangle)

    #if the intersection with the image is empty
    if player_rectangle is None:
        return None, None
    
    #now draw the ellipses and rectange on the player rectangle
    output = np.zeros(player_rectangle.get_shape(), np.float64)

    #translate the ellipses to the coordinate system of the player rectangle
    bottom_center, bottom_axes, bottom_angle = bottom_ellipse.get_geometric_parameters(in_degrees=True)
    top_center, top_axes, top_angle = top_ellipse.get_geometric_parameters(in_degrees=True)

    bottom_center -= player_rectangle.get_top_left()
    top_center   -= player_rectangle.get_top_left()
    
    cv2.ellipse(output, tuple(np.int32(bottom_center).tolist()), tuple(np.int32(bottom_axes).tolist()),
                bottom_angle, 0, 360, 1, -1, cv2.CV_AA)
    cv2.ellipse(output, tuple(np.int32(top_center).tolist()), tuple(np.int32(top_axes).tolist()),
                top_angle, 0, 360, 1, -1, cv2.CV_AA)

    #also translate the contour to the player rectangle coordinates, and draw it
    contour -= player_rectangle.get_top_left()
    cv2.drawContours(output, [np.int32(contour.reshape((-1,1,2)))], -1, 1, -1)

    return player_rectangle, (output > 0)
    
