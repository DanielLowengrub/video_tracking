import tensorflow as tf

"""
This is a collection of commonly used tensorflow functions
"""

def normalized_vectors(vectors):
    '''
    vectors - a tf array with shape (num vectors, vector length) and type np.float32

    Return a tf array with shape (num vectors, vector length) and type float32. The i-th row of the output
    is the normalization of the i-th row of vectors
    '''

    vector_norms = tf.sqrt(tf.reduce_sum(tf.pow(vectors, 2), 1))

    #in order to divide vectors be vector_norms we need to reshape vector norms to have shape (num vectors, 1)
    norms_shape = tf.concat(axis=0, values=[tf.shape(vectors)[:1], [1]])
    vector_norms = tf.reshape(vector_norms, norms_shape)
    
    return vectors / vector_norms

def apply_inverse_homography_to_lines(H, lines):
    '''
    H - a (3,3) tensor of type np.float32 representing a homography matrix
    lines - a tensor with shape (number of lines, 3) and type np.float32

    Each row of lines represents the parameters (a,b,c) of the line a*x + b*y + c = 0

    Return a tensor with shape (number of lines, 3) and type np.float32.

    If L represents a line in the lines array, the corresponding line of the output is L' such that
    L' = (H^T)L. I.e, L' is the image of L under the homography H^{-1}.
    
    Recall that if L the the original line and M is a homography, the p\inL <=> L^Tp = 0. So:
    L'^Tq=0 <=> L^T*M^{-1}*q <=> (M^{-T}L)^Tq = 0. So L' = ((M^{-1})^T)L
    '''
    
    new_lines =  tf.transpose(tf.matmul(tf.transpose(H), tf.transpose(lines)))

    return new_lines

def apply_inverse_homography_to_ellipse(H, E):
    '''
    H - a (3,3) tensor of type np.float32 representing a homography matrix
    E - a tensor with shape (3, 3) and type np.float32

    Return a tensor with shape (3, 3) and type np.float32.

    The output E' satisfies:
    E' = (H^T)*E*H
    I.e, E' is the image of E under the homography H^{-1}.
    '''

    return tf.matmul(tf.transpose(H), tf.matmul(E, H))

def apply_cameras_to_points(cameras, points):
    '''
    cameras - a tensor with shape (n, 3, 4)
    points  - a tensor with shape (n, 3)

    return  - a tensor with shape (n,2)
    '''
    #add a column of ones to the points
    points = tf.pad(points-1, [[0,0],[0,1]], 'CONSTANT') + 1
    
    #turn the points into a batch of column vectors
    points_shape = tf.concat(axis=0, values=[tf.shape(points)[:1], [4,1]])
    points = tf.reshape(points, points_shape)

    #multiply each of the camera matrices with each of the points
    #this gives us an array with shape (n,3,1)
    projective_image_points = tf.matmul(cameras, points)

    #reshape the image points to have shape (n,3)
    projective_image_points_shape = tf.concat(axis=0, values=[tf.shape(projective_image_points)[:1], [3]])
    projective_image_points = tf.reshape(projective_image_points, projective_image_points_shape)

    #convert to affine coordinates
    image_points = projective_image_points[:,:2] / projective_image_points[:,2:]
    
    return image_points

def multiply_matrices(M, matrices):
    '''
    M - a tensor with shape (n,m)
    matrices - a tensor with shape (k, m, p)

    return a tensor with shape (k, n, p)

    For each i, output[i,:,:] = M * matrices[i,:,:]
    '''
    #we first build a tensor with shape (m, k*p) which is obtained by stacking up the given
    #matrices side by side
    N = tf.concat(axis=1, values=tf.unstack(matrices))

    #We now multiply by M, and get the matrices M*matrices[i] lines up side by side
    #this is an array with shape (n, k*p)
    MN = tf.matmul(M,N)

    #finally, we have to repack the output into an array with shape (k,n,p)
    #this is an array with shape (3,) that stores the values (k,n,p)
    kpn = tf.concat(axis=0, values=[tf.shape(matrices)[:1], tf.shape(matrices)[2:], tf.shape(M)[:1]])

    #to repack MN, we first transpose it to get a (k*p, n) array
    output = tf.transpose(MN)
    #we then reshape it to have shape (k,p,n).
    output = tf.reshape(MN, kpn)
    #finally, we transpose each of the matrices output[i,:,:] so that output has shape (k, n, p)
    output = tf.transpose(output, perm = [0,2,1])

    return output

def rot_x(theta):
    '''
    theta - a tf scalar. represents an angle in radians
    returns - a tf array with shape [3,3]. It is the tf version of world_rotations.rot_x
    '''
    output_flat =  tf.stack([1, 0,              0            ,
                            0, tf.cos(theta), -tf.sin(theta),
                            0, tf.sin(theta),  tf.cos(theta)])
    return tf.to_float(tf.reshape(output_flat, [3,3]))

def rot_z(theta):
    '''
    theta - a tf scalar. represents an angle in radians
    returns - a tf array with shape [3,3]. It is the tf version of world_rotations.rot_z
    '''
    output_flat =  tf.stack([tf.cos(theta), -tf.sin(theta), 0,
                            tf.sin(theta),  tf.cos(theta), 0,
                            0,              0,             1])
    
    return tf.to_float(tf.reshape(output_flat, [3,3]))
