import tensorflow as tf
import numpy as np

class TFMVOptimizer(object):
    '''
    A tensor flow optimizer minimizes a given function with respect to a tuple of arguments.
    '''

    def __init__(self, learning_rate, max_epochs, local_minimum_threshold):
        '''
        learning_rate - the step sizes in the gradient direction that we take in each iteration
        max_epochs - the maximum number of iterations
        local_minimum_threshold - we decide that we are in a local minimum if the cost function changes by less than this.
        '''

        self._learning_rate = learning_rate
        self._max_epochs = max_epochs
        self._local_minimum_threshold = local_minimum_threshold

    def minimize(self, initial_values, cost_function, print_progress_interval=-1):
        '''
        initial_values - a tuple of numpy arrays
        cost_function - a function whose number of arguments is equal to the length of the intial_values. The i-th argument
           is a tf array with the same shape as initial_values[i]. The cost function returns a scalar.
        print_progress_interval - we print the progress every print_progress_interval epochs. If the value is negative,
                                  do not print anything.
        Return a tuple of numpy arrays with the same shapes and types as the initial_values, which 
        locally minimizes the cost function.
        '''
        optimized_values = tuple(tf.Variable(val, name='initial_value_%d' % i) for i,val in enumerate(initial_values))
        cost = cost_function(*optimized_values)
        
        optimizer = tf.train.GradientDescentOptimizer(self._learning_rate).minimize(cost)

        sess = tf.Session()
    
        #initialize the variables
        sess.run(tf.global_variables_initializer())

        prev_training_cost = 0.0
        print 'initial cost: ', sess.run(cost)
        
        for epoch_i in range(self._max_epochs):
            sess.run(optimizer)
            training_cost = sess.run(cost)

            if (print_progress_interval > 0) and (epoch_i % print_progress_interval == 0):
                #print the cost after this step
                print '\n\nsummary of epoch %d:' % epoch_i
                print '   training cost: %f' % training_cost
                print '   training cost diff: ', np.abs(prev_training_cost - training_cost)
                # current_value = sess.run(optimized_value)
                # print '   current value: '
                # print current_value

            # Allow the training to quit if we've reached a local minimum
            if np.abs(prev_training_cost - training_cost) < self._local_minimum_threshold:
                print 'reached local minimum thresh: ', np.abs(prev_training_cost - training_cost)
                break
            
            prev_training_cost = training_cost

        #get the final optimized value
        optimized_values = tuple(sess.run(optimized_value) for optimized_value in optimized_values)

        sess.close()

        return optimized_values
    
