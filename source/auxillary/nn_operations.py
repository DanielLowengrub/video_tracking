import numpy as np

def weighted_one_hot(x, bins):
    '''
    x - a float
    bins - a np array: [b0,...,bn] such that: b0 <= x <= bn and b0 < b1 < ... < bn

    return - a numpy array with length n+1: prob = [p0,...,pn]

    The output has the following property: Let i be the unique integer satisfying:
    bins[i-1] <= x < bins[i]

    then for all j=/=i,i-1 we have prob[j] = 0. Furthermore, prob[i-1] and prob[i] are defined such that:
    * prob[i-1] + prob[i] = 1
    * prob[i-1]*bins[i-1] + prob[i]*bins[i] = x
    '''
    prob = np.zeros(len(bins), np.float32)

    if x == bins[-1]:
        prob[-1] = 1.0
        return prob

    #the index of the bin to the right of x
    x_bin = np.digitize(x, bins)

    #set the value of the bin to the right of x (br) to p, and the bin to the left (bl) to q such that:
    #p + q = 1
    #p*br + q*bl = theta
    bin_width = bins[x_bin] - bins[x_bin-1]
    prob[x_bin]   = (x - bins[x_bin-1])/bin_width
    prob[x_bin-1] = (bins[x_bin] - x)  /bin_width

    return prob

def inv_weighted_one_hot(prob, bins):
    '''
    prob - a numpy array: [p0,...,pn]
    bins - a numpy array: [b0,...,bn]

    we assume that prob is the output of weighted_one_hot(x, bins) for some x.
    return - x
    '''
    #x_bin is the index that satisfies: bins[x_bin-1] <= x < bins[x_bin]
    x_bin = np.argmax(prob)

    #in this case, bins[0] <= x <= bins[1], so we must increment x_bin by 1
    if x_bin == 0:
        x_bin += 1
        
    #in this case, the current value for x_bin satisfies: bins[x_bin] <= x <= bins[x_bin+1] so we must
    #increment x_bin by 1
    elif (x_bin < len(prob)-1) and (prob[x_bin-1] < prob[x_bin+1]):
        x_bin += 1

    x = prob[x_bin-1]*bins[x_bin-1] + prob[x_bin]*bins[x_bin]

    return x
