import numpy as np

class Interval(object):
    '''
    This class represents a 1D interval [a,b).
    '''

    def __init__(self, a, b):
        '''
        a,b integers.
        We construct the interval [a,b)
        '''

        self._a = a
        self._b = b

    def __str__(self):
        return str((self._a, self._b))

    def __repr__(self):
        return str(self)
    
    def __hash__(self):
        return hash((self._a, self._b))
    
    @classmethod
    def from_unordered_pair(cls, a, b):
        '''
        a,b - integers
        We first sort a and b such that a<b, and then return the interval [a,b)
        '''

        if a <= b:
            return cls(a,b)
        return cls(b,a)

    def length(self):
        return self._b - self._a
    
    def contains_point(self, point):
        return self._a <= point < self._b
    
    def get_copy(self):
        return Interval(self._a, self._b)
    
    def get_slice(self, array):
        return array[self._a : self._b]

    def get_tuple(self):
        '''
        return a tuple (a,b)
        '''
        return (self._a, self._b)
    
    def get_relative_slice(self, array, child_interval):
        '''
        array - a numpy array with shape (n,...) where n = length of this interval
        child_interval - an interval object that is contained in this interval

        return - a slice of the array along the 0-th axis. 
        Suppose the 0-th axis of the array is placed at the position of this interval. The slice corresponds
        to the elements in that are in the child array.
        '''
        start_index = child_interval._a - self._a
        end_index = child_interval._b - self._a

        return array[start_index:end_index]

    def relative_to_absolute_coords(self, i):
        '''
        i - an integer satisfying: 0 <= i < length of this interval. This represents a position in the 
            intervals internal coordinate system
        return - an integer which is the position of i in the absolute coordinate system. I.e, _a + i
        '''
        return self._a + i

    def relative_to_absolute_interval(self, relative_interval):
        '''
        relative_interval - an Interval object with endpoints (i,j) which satisfy: 0 <= i < j <= interval.length(). 
           It represents a sub interval of this interval.
        return - an Interval object wich represents sub_interval in absolute coordinates.
        '''
        #simply add self._a to each of the relative_interval endpoints
        return Interval(relative_interval._a + self._a, relative_interval._b + self._a)
    
    def first_element(self):
        return self._a

    def last_element(self):
        return self._b

    def has_overlap(self, other):
        '''
        Return True if the intervals overlap
        '''

        #if this interval comes before the other one
        if self._a <= other._a:
            return self._b > other._a

        #if this interval comes after the other one
        return other._b > self._a

    def can_merge(self, other):
        '''
        Return True if we can merge this interval with the other one.
        This holds iff the intervals overlap, or the end of one is equal to the beginning of the other one.
        '''
        endpoints = (self._a, self._b)
        if (other._a in endpoints) or (other._b in endpoints):
            return True

        return self.has_overlap(other)

    def contains_interval(self, other):
        '''
        return True iff this interval contains the other one.
        '''

        return (self._a <= other._a) and (other._b <= self._b)
    
    def get_overlap(self, other):
        '''
        Return the interval obtained by intersecting this interval with the other one.
        We assume that the user has checked that there is indeed an intersection using has_intersection(other)
        '''
        a = max(self._a, other._a)
        b = min(self._b, other._b)
        return Interval(a,b)
    
    def merge(self, other):
        '''
        Return a new interval whci is obtained by merging this one with the other one.
        If there is no overlap between the intervals, an exception is raised
        '''

        if not self.can_merge(other):
            raise ValueError('We can only merge intervals that have an overlap')

        a = min(self._a, other._a)
        b = max(self._b, other._b)

        return Interval(a,b)

    def interval_dilation(self, dilation_radius):
        '''
        dilation_radius - a float
        return a new Interval that has been dilated on both ends by the amount: dilation_radius
        '''
        return Interval(self._a - dilation_radius, self._b + dilation_radius)

    def interval_erosion(self, erosion_radius):
        '''
        erosion_radius - a float
        return a new Interval that has been eroded on both ends by the amount: dilation_radius
        '''
        new_a = self._a + erosion_radius
        new_b = self._b - erosion_radius

        if new_a >= new_b:
            return None
        
        return Interval(new_a, new_b)

class IntervalList(object):
    '''
    This represents a list of non overlapping intervals.
    '''

    def __init__(self):

        #this will contain an ordered list of intervals
        self._intervals = []

    @classmethod
    def from_boolean_array(cls, array):
        '''
        array - a numpy array of type np.bool
        We construct an IntervalList representing boundary points of the sequences of true values in the array.
        '''

        #find the boundaries of the sequences of true values
        padded_array = np.concatenate(([False], array, [False]))
        boundary_indices = np.where(np.diff(padded_array))[0]

        #line up the boundary indices into tuples of (start boundary, end boundary)
        boundaries = boundary_indices.reshape(-1,2)

        #finally, create one interval for each pair of boundaries
        interval_list = cls()
        interval_list._intervals = [Interval(boundary[0], boundary[1]) for boundary in boundaries]

        return interval_list
        
    def add_interval(self, interval, in_order=False):
        '''
        Add the given interval to the list.
        If in_order is True, we assume that the given interval comes after the last interval that is 
        currently in the list.
        '''
        if not in_order:
            raise NotImplementedError('We currently only support in order additions')

        if len(self._intervals) == 0:
            self._intervals = [interval]
            return
        
        #try to merge the given interval with the last one in the list
        if self._intervals[-1].can_merge(interval):
            self._intervals[-1] = self._intervals[-1].merge(interval)
            return

        #if we did not manage to merge it, add the new interval to the list
        self._intervals.append(interval)

        return
    
    def get_intervals(self):
        return self._intervals

