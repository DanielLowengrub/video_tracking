import fit_points_to_shape
from .fit_points_to_shape import FitPointsToShape
from .line import Line
import numpy as np

class FitPointsToLine(FitPointsToShape):
    '''
    This implementation of FitPointsToShape fits the points to a line and returns a Line object.
    '''

    def __init__(self):
        super(FitPointsToLine, self).__init__()

        self._epsilon = 10 ** (-5) #our threshold for being equal to 0
        return


    def _denormalize_shape_parameters(self, normalization_matrix, line_parameters):
        '''
        We simply take the product line_parameters * normalization_matrix
        '''
        return np.dot(line_parameters, normalization_matrix)
    
    def _get_weights_by_interval_length(self, points):
        '''
        points - a numpy array of shape (number of points, 1, 2)
        
        Return a numpy array W of length (number of points) where:
        
        W[i] = (distance(points[i], points[i-1])/2 + distance(points[i], points[i+1])/2)
        '''

        points = points.reshape((-1,2))

        #this holds pairs: (points[0], points[1]), (points[1], points[2]), ... , (points[N-1], points[N])
        point_to_next_point = points[1:] - points[:-1]
        distance_to_next_point = np.linalg.norm(point_to_next_point, axis=1)
        distance_to_prev_point = np.hstack([np.zeros(1), distance_to_next_point])
        distance_to_next_point = np.hstack([distance_to_next_point, np.zeros(1)])

        weights = 0.5*distance_to_prev_point + 0.5*distance_to_next_point

        return weights
        
    def fit_points(self, points, weight_by_interval_length=False):
        '''
        Fit the points to a line.
        Return Line object, algebraic residue

        We solve this using an explicit solution to linear least squares.
        The algebraic residue is used to evaluate the quality of the fit.
        '''
        points = np.float64(points)
        
        #first we normalize the points
        #normalization_matrix = self._get_normalization_matrix(points, normalization_type = fit_points_to_shape.RADIAL)
        #normalized_points = self._normalize_points(normalization_matrix, points)
        normalized_points = points
        
        #get the weights
        weights = np.ones(len(normalized_points))
        if weight_by_interval_length:
            weights = self._get_weights_by_interval_length(normalized_points)

        #print 'the weights are:'
        #print weights
        
        weights_sq = weights * weights
        
        #reshape the points to get one row of x coords and one row of y coords
        A = normalized_points.reshape((-1, 2)).transpose()
        N = A.shape[1] #the total number of points

        #the weighted sums
        Sums = (A * weights_sq).sum(axis=1)
        SumX = Sums[0]           #the sum of x_i * w_i^2
        SumY = Sums[1]           #the sum of y_i * w_i^2

        SumXY = (A[0] * A[1] * weights_sq).sum() #the sum of x_i * y_i * w_i^2
        SumXX = (A[0] * A[0] * weights_sq).sum() #the sum of x_i^2 * w_i^2
        SumYY = (A[1] * A[1] * weights_sq).sum() #the sum of y_i^2 * w_i^2

        SumWW = weights_sq.sum() #the sum of w_i^2
        
        #first try to fit the points to the line y = m*x + c
        denominator1 = SumX * SumX - SumXX * SumWW
        denominator2 = SumY * SumY - SumYY * SumWW

        #print 'denominator for y = mx + c: ', denominator1
        #print 'denominator for x = my + c: ', denominator2

        if max(abs(denominator1), abs(denominator2)) < self._epsilon:
            return None, None
        
        #if the denomonator1 is larger, we fit the points to y = m*x + c
        if abs(denominator1) > abs(denominator2):
            numerator = SumX * SumY - SumXY * SumWW
            m = numerator / denominator1
            c = (SumY - m*SumX) / SumWW

            line_parameters = np.array([m, -1, c])

        #otherwise, try to fit to x = my + c
        else:
            numerator = SumY * SumX - SumXY * SumWW
            m = numerator / denominator2
            c = (SumX - m*SumY) / SumWW
        
            line_parameters = np.array([1, -m, -c])

        #compute the algebraic residue.
        #we do this before denormalizing so that it makes sense to compare fittings of different points.
        alpha = np.linalg.norm(line_parameters[:2])
        line_parameters = line_parameters / alpha
        algebraic_residue = ( (line_parameters[0] * A[0]) + (line_parameters[1] * A[1]) + line_parameters[2]) * weights
        algebraic_residue = (algebraic_residue * algebraic_residue).sum()

        self._algebraic_residue = algebraic_residue
        
        #line_parameters = self._denormalize_shape_parameters(normalization_matrix, line_parameters)
        self._fitted_shape = Line(*line_parameters.tolist())
        self._geometric_residue = self._fitted_shape.get_geometric_residue(points)
        
        return self._fitted_shape, self._algebraic_residue
