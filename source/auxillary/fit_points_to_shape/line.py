import planar_shape
import numpy as np
from ...auxillary import planar_geometry

DEBUG = False

class Line(planar_shape.PlanarShape):
    '''
    This simplest kind of a planar shape.
    
    We represent a line as the set of zeros to the equation a*x + b*y + c = 0 and we store the tuple (a, b, c).

    Note that we normalize the line parameters so that the algebraic and geometric distance coincide.
    '''

    def __init__(self, a, b, c):
        super(Line, self).__init__(planar_shape.LINE)

        #normalize the line
        alpha = np.linalg.norm(np.array([a, b]))
        
        self._a = a / alpha
        self._b = b / alpha
        self._c = c / alpha

        #this vector is the tangent vector for all points on the line
        self._tangent_vector = np.array([-self._b, self._a])
        self._normal_vector = np.array([self._a, self._b])
        
    def __str__(self):
        return 'Line: (%f, %f, %f)' % (self._a, self._b, self._c)

    def __repr__(self):
        return str(self)
    
    @classmethod
    def from_point_and_direction(self, point, direction):
        '''
        point - a numpy array of length 2
        direction - a numpy array of length 2
        '''

        point = np.float64(point)
        direction = np.float64(direction)

        #get the normal vector to the line
        normal_vector = planar_geometry.perp(direction)
        normal_vector /= np.linalg.norm(normal_vector)
        
        #project the point onto this normal vector
        distance_from_origin = np.dot(point, normal_vector)

        return Line(normal_vector[0], normal_vector[1], -distance_from_origin)

    @classmethod
    def from_parametric_representation(cls, parametric_representation):
        return Line(*parametric_representation.tolist())
    
    def get_parametric_representation(self):
        return np.array([self._a, self._b, self._c])
    
    def get_algebraic_distance_from_point(self, point):
        return self._a * point[0] + self._b * point[1] + self._c

    def get_geometric_distance_from_point(self, point):
        return np.abs(self.get_algebraic_distance_from_point(point))
    
    def get_algebraic_distance_from_points(self, points):
        points = points.reshape((-1,2))
        return np.abs(self._a * points[:,0] + self._b * points[:,1] + self._c)
    
    def get_algebraic_residue(self, points):
        points = points.reshape((-1,1,2))
        res = self._a * points[:,0,0] + self._b * points[:,0,1] + self._c
        return (res*res).sum()

    def get_geometric_residue(self, points):
        return self.get_algebraic_residue(points)
    
    def get_tangent_vector(self, point):
        return self._tangent_vector

    def get_normal_vector(self):
        return self._normal_vector
    
    def get_closest_point_to_origin(self):
        '''
        Return the point on the line closest to the origin
        '''
        return -self._c * np.array([self._a, self._b])
    
    def get_angle_radius(self, degrees=False):
        '''
        Return a pair (theta, r) such that -pi < theta <= pi, r>0 and this line is equal to the set of points (x,y) satisfying:
        r = x*cos(theta) + y*sin(theta)
        
        if degrees is False, the angle is in radians, and is between -pi and pi. 
        Otherwise it is in degrees, and is between -180 and 180.
        '''
        r = -self._c

        #r must be positive
        if r >= 0:
            theta = np.arctan2(self._b,self._a)

        #if r is not positive, multiply all line coeffs by -1
        else:
            r *= -1
            theta = np.arctan2(-self._b, -self._a)

        if degrees:
            theta *= 180 / np.pi

        return theta, r

    def get_closest_point_on_line(self, point):
        '''
        Return the point on the line that is closest to the given point.
        '''

        #project the point onto the normal vector
        x = np.dot(self._normal_vector, point)

        residue = x + self._c


        #correct by the residue distance
        return point - (residue * self._normal_vector)

    def compute_shape_similarity(self, other):
        '''
        other - a PlanarShape of the same type (line or ellipse)
        
        Return a number that measures the similarity of the shapes. A smaller number means that the shapes are more similar.
        For a line we compute the distance between the (radius, theta) parameters.

        Note that the angle is measured in radians, so the shapes should be scaled so that the radii of the lines are ~1.
        If for example the radii are very large, then they will dominate the line similarity metric.
        '''

        theta, radius = self.get_angle_radius()
        other_theta, other_radius = other.get_angle_radius()

        return np.linalg.norm([theta - other_theta, radius - other_radius])
    
    def is_parallel(self, other, cos_angle_threshold):
        '''
        Return true if this line is parallel to the other line, wrt the given threshold
        '''
        if DEBUG:
            print 'checking if %s is parallel to %s' % (str(self), str(other))
        
        #recall that the tangent vectors have already been normalized
        cos_theta = np.dot(self._tangent_vector, other._tangent_vector)

        #check if cos_theta is close to 1 or -1
        is_parallel =  min(np.abs(1 - cos_theta), np.abs((-1) - cos_theta))  < cos_angle_threshold

        if DEBUG:
            print '   cos(theta) = ', cos_theta
            print '   %f < %f: %s' % (min(np.abs(1-cos_theta), np.abs(-1-cos_theta)), cos_angle_threshold, is_parallel)

        return is_parallel
