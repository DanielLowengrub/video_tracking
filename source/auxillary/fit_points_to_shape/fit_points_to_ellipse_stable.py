from .fit_points_to_ellipse import FitPointsToEllipse
from .fit_points_to_shape import FitPointsToShape
from .ellipse import Ellipse
import numpy as np
from scipy.linalg import eig

class FitPointsToEllipseStable(FitPointsToEllipse):
    '''
    This is a numerically stable implementation of FitPointsToEllipse based on the paper:
    "Numerically Stable Direct Least Squares Fitting of Ellipses"
    '''

    def __init__(self):
        super(FitPointsToEllipse, self).__init__()

        #if S_3 has a condition number that is bigger than this number, then we can not find an ellipse
        self._max_S_3_condition_number = 10**10

        #we only take eigenvalues that correspond to an ellipse with a discriminant bigger than this number
        self._min_discriminant = 10**(-10)
        
        self._C_1 = np.zeros((3,3)) #this is the matrix we use to compute the discriminant
        self._C_1[0][2] = 2
        self._C_1[1][1] = -1
        self._C_1[2][0] = 2

        self._C_1_inv = np.linalg.inv(self._C_1)
        
        return


    def _get_scatter_matrices(self, points):
        '''
        Return the scatter matrices of the points: (S_1, S_2, S_3)

        The scatter matrices are defined as follows:
        Let D_1 be the matrix with one row of the form [x^2 xy y^2] per point (x,y)
        Let D_2 be the matrix with one row of the form [x y 1] per point (x,y)

        Then,
        S_1 = D_1^T * D_1
        S_2 = D_1^T * D_2
        S_3 = D_2^T * D_2
        '''

        A = points.reshape(-1, 2).transpose()
        X = A[0]   #a vector of the x coordinates
        Y = A[1]   #a vector of the y coordinates

        D_1_transpose = np.vstack([X*X, X*Y, Y*Y])
        D_2_transpose = np.vstack([X, Y, np.ones(len(X))])
        
        D_1 = D_1_transpose.transpose()
        D_2 = D_2_transpose.transpose()

        S_1 = np.dot(D_1_transpose, D_1)
        S_2 = np.dot(D_1_transpose, D_2)
        S_3 = np.dot(D_2_transpose, D_2)
        
        return S_1, S_2, S_3

    def _get_reduced_scatter_matrix(self, points):
        '''
        compute the "reduced scatter matrix" as in the above referenced paper.
        It is defined to be:
        C_1^-1 * (S_1 - (S_2 * S_3^-1 * S_2^T))

        Where S_i are the scatter matrices.

        We also return T = -S_3^-1 * S_2^T for future use.
        '''
        S_1, S_2, S_3 = self._get_scatter_matrices(points)
        
        #we first check if S_3 is invertible. If it is not, then the points are not on an ellipse.
        if np.linalg.cond(S_3) > self._max_S_3_condition_number:
            #print 'S_3 is not invertible. Returning None.'
            return None, None

        S_3_inv = np.linalg.inv(S_3)
        T = -np.dot(S_3_inv, S_2.transpose())
        M = np.dot(self._C_1_inv, S_1 + np.dot(S_2, T))

        return M, T

    def _choose_eigenvector(self, eigenvectors):
        '''
        eigenvectors - a numpy array of shape (3, number of eigenvectors). I.e, each column is a different
        eigenvector.
        Find the eigenvector u for which 4*u[0]u[2] - u[1]^2 is positive
        '''

        discriminants = 4 * eigenvectors[0,:] * eigenvectors[2,:] - eigenvectors[1,:] * eigenvectors[1,:]
        
        indices_of_good_eigenvectors = (discriminants > self._min_discriminant).nonzero()[0]

        #if there is not exactly one good eigenvalue, the points do not lie on an ellipse
        if len(indices_of_good_eigenvectors) != 1:
            #print 'There was not exactly one eigenvalue woth positive discriminant. Returning None.'
            return None

        return eigenvectors[:, indices_of_good_eigenvectors[0]]
    
    def fit_points(self, points):
        '''
        Fit the points to an ellipse.
        '''
        points = np.float64(points).reshape((-1,1,2))
        
        #first we normalize the points
        #normalization_matrix = self._get_normalization_matrix(points)
        #normalized_points = self._normalize_points(normalization_matrix, points)
        normalized_points = points
        
        M, T = self._get_reduced_scatter_matrix(normalized_points)
        if M is None:
            self._fitted_shape = None
            self._algebraic_residue = None
            self._geometric_residue = None
            return None, None

        #now find the eigenvalues and eigenvectors of M
        eigenvalues, eigenvectors = eig(M)

        #choose the eigenvector corresponding to an ellipse
        a_1 = self._choose_eigenvector(eigenvectors)
        if a_1 is None:
            self._fitted_shape = None
            self._algebraic_residue = None
            self._geometric_residue = None
            return None, None

        a_2 = np.dot(T, a_1)
        
        #stack up a_1 and a_2 to get the ellipse parameters
        ellipse_parameters = np.float64(np.hstack([a_1, a_2]))
        
        #now denormalize the ellipse parameters. I.e, find the ellipse passing
        #through the original points. (the ones we had before normalizing them)
        #ellipse_parameters = self._denormalize_shape_parameters(normalization_matrix, ellipse_parameters)

        #construct an ellipse object and calculate the algebraic residue
        self._fitted_shape = Ellipse(*ellipse_parameters.tolist())
        self._algebraic_residue = self._fitted_shape.get_algebraic_residue(points)
        self._geometric_residue = self._fitted_shape.get_geometric_residue(points)
        
        return self._fitted_shape, self._algebraic_residue

        
