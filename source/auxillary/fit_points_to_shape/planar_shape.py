import numpy as np

LINE = 0
ELLIPSE = 1

class PlanarShape(object):
    '''
    This represents a shape on a plane.

    The most important method is distance_from_point which returns the distance from a point to the shape
    '''

    def __init__(self, shape_type):
        '''
        shape_type is either LINE or ELLIPSE
        '''

        self._shape_type = shape_type

        return

    def is_line(self):
        return self._shape_type == LINE

    def is_ellipse(self):
        return self._shape_type == ELLIPSE

    def get_shape_type(self):
        return self._shape_type
    
    def get_geometric_distance_from_point(self, point):
        '''
        point - a numpy array of shape (2,)
        Return the geometric distance from the point to the shape.
        '''

        pass

    def get_algebraic_distance_from_point(self, point):
        '''
        point - a numpy array of shape (2,)
        Return the algebraic distance from the point to the shape.
        '''

        pass


    def get_algebraic_residue(self, points):
        '''
        points - a numpy array of shape (number of points, 1, 2)

        Return the sum of squares of the algebraic distances of each of the points from the shape.
        '''

        pass

    def get_geometric_residue(self, points):
        '''
        points - a numpy array of shape (number of points, 1, 2)

        Return the sum of squares of the geometric distances of each of the points from the shape.
        '''

        pass

    def get_tangent_vector(self, point):
        '''
        point - a numpy array of length 2 and type np.float64.
        
        We assume the point lines on the shape and return the tangent vector to the shape at the point.
        '''

        pass

    def compute_shape_similarity(self, other):
        '''
        other - a PlanarShape of the same type (line or ellipse)
        
        Return a number that measures the similarity of the shapes. A smaller number means that the shapes are more similar.
        '''

        raise NotImplementedError('compute_shape_similarity has not been implemeneted')
    
    def get_direction(self, pointA, pointB):
        '''
        pointA, pointB - numpy arrays of length 2 and type np.float64
        
        We assume that pointA lies on the shape. We return 1 if the vector (pointB - pointA) forms and angle of
        less than pi/2 with the tangent vector to the shape at pointA. Otherwise we return -1.

        Note that we are assuming that we have chosen an orientation for the shape. Flipping the orientation
        will flip the output of this method, but we only care that the assignments be consistent with the chosen orientation.
        '''

        tangent_vector = self.get_tangent_vector(pointA)
        AtoB = pointB - pointA
        
        if np.dot(tangent_vector, AtoB) >= 0:
            return 1

        else:
            return -1

