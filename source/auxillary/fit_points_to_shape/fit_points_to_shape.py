import numpy as np

MIN_LENGTH_TO_RESCALE = 2 #do not rescale an axis if the difference between the largest coordinate and the smallest one is less than this number

COORDINATE_WISE = 0
RADIAL = 1

class FitPointsToShape(object):
    '''
    This class is in charge of fitting a collection of points to a certain geometric shape.

    After fitting the points, we return a PlanarShape object.
    '''

    def __init__(self):
        
        #the residuals we get after fitting the points
        self._algebraic_residue = None
        self._geometric_residue = None
        self._fitted_shape = None

    def _get_normalization_matrix(self, points, normalization_type = COORDINATE_WISE):
        '''
        Return a 3x3 matrix M such that if we apply M as a projective transformation
        to each of the points, then the new mean is 0 and both maxX-minX and maxY-minY are equal to 0.5.
        '''
        points = points.reshape((-1,2))
        mean = points.mean(axis=0)
        T = -mean
        
        max_coords = points.max(axis=0)
        min_coords = points.min(axis=0)
        coord_lengths = max_coords - min_coords

        #if we are scaling each coordinate individually
        if normalization_type == COORDINATE_WISE:
            coord_lengths[coord_lengths < MIN_LENGTH_TO_RESCALE] = 2.0
            S = 2.0 / coord_lengths

        #if we are scaling them both together
        if normalization_type == RADIAL:
            max_length = coord_lengths.max()
            S = (2.0 / max_length) * np.ones(2)
            
        #print 'T: ', T
        #print 'S: ', S
        
        #the matrix M translates by T and then scales by S
        M = np.array([[S[0], 0   , S[0]*T[0]],
                      [0   , S[1], S[1]*T[1]],
                      [0   , 0    , 1        ]])

        return M

    def _normalize_points(self, normalization_matrix, points):
        '''
        Apply the normalization matrix to each of the points and return the result.
        Note that since the normalization matrix is a projective transformation, we exchange each point (x,y) with (x,y,1).
        '''
        points = points.reshape((-1,2))
        #add a column of ones
        points = np.hstack([points, np.ones(len(points)).reshape((-1,1))])
        normalized_points = np.dot(normalization_matrix, points.transpose()).transpose()
        
        return normalized_points[:,:2].reshape((-1,1,2))

    def _denormalize_shape_parameters(self, normalization_matrix, shape_parameters):
        '''
        normalization_matrix - a numpy array of shape (3,3) and type np.float64.
        shape_parameters - depends on the implementation

        Let N be the normalization matrix. Let S be the shape parameters. We return new shape parameters S' such that
        S'(x) = S(Nx) for all points x.
        '''

        pass
    
    def fit_points(self, points):
        '''
        points - a numpy array of shape (number of points, 1, 2)

        Return a tuple (PlanarShape, algebraic residue) where the shape is the one that best fits the points. If no good fit is found,
        return None.
        '''

        pass

    def get_algebraic_residue(self):
        '''
        Return the sum of squares error of the approximation.
        '''

        return self._algebraic_residue

    def get_geometric_residue(self):
        return self._geometric_residue
    
    def get_shape(self):
        return self._fitted_shape
