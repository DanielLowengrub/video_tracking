from fit_points_to_shape import FitPointsToShape
from .ellipse import Ellipse
import numpy as np
from scipy.linalg import eigh

class FitPointsToEllipse(FitPointsToShape):
    '''
    This implementation of FitPointsToShape fits the points to an ellipse and returns an Ellipse object.
    We use the method from "Direct Least Squares Fitting of Ellipses". To summarize, we use the list of points
    to form a matrix D is the usual way and express the problem as the linear least squares problem
    Dx = 0

    In addition, we impose the quadratic constraint x^T * C * x = 1 where C is chosen so that this means "the discriminant of the conic is -1".

    By setting S = D^T * D we can reduce this constrained problem to the generalized eigenvalue problem
    
    S*x = lambda * C * x

    where S is positive definite and C is symmetric. In order to solve the problem, we take the (unique!) eigenvector that has a positive eigenvalue.
    '''

    def __init__(self):
        super(FitPointsToShape, self).__init__()

        self._epsilon = 10 ** (-5) #our threshold for being equal to 0

        self._C = np.zeros((6,6)) #this is the matrix we
        self._C[0][2] = 2
        self._C[1][1] = -1
        self._C[2][0] = 2
        
        return

    def _denormalize_shape_parameters(self, normalization_matrix, ellipse_parameters):
        '''
        Return a new set of ellipse parameters a',...,f' such that if normalization_matrix*(x,y) = (u,v) then:
        a'*x^2 + ... d'*x + ... + f' = a*u^2 + ... + d*u + ... + f

        To do this, we first find the 3x3 matrix P corresponding to the ellipse parameters (a,...,f) such that
        [x y 1] * P * [x y 1]^T = a*x^2 + ... + f

        Then the ellipse parameters we want have the 3x3 matrix: normalization_matrix^T * P * normalization_matrix
        '''
        E = ellipse_parameters
        P = np.array([[    E[0], 0.5*E[1], 0.5*E[3]],
                      [0.5*E[1],     E[2], 0.5*E[4]],
                      [0.5*E[3], 0.5*E[4],     E[5]]])
        
        new_P = np.dot(normalization_matrix.transpose(), np.dot(P, normalization_matrix))

        new_E = np.array([new_P[0][0], 2*new_P[0][1], new_P[1][1],
                          2*new_P[0][2], 2*new_P[1][2], new_P[2][2]])
        return new_E
    
    def _get_scatter_matrix(self, points):
        '''
        Return the scatter matrix of the points.
        The scatter matrix is defined to be D^T * D
        where D is the design matrix.

        Recall that the design matrix has one row per point (x,y), and each row is of the form
        [x**2 xy y**2 x y 1]
        '''

        A = points.reshape(-1, 2).transpose()
        X = A[0]   #a vector of the x coordinates
        Y = A[1]   #a vector of the y coordinates

        D_transpose = np.vstack([X*X, X*Y, Y*Y, X, Y, np.ones(len(X))])
        D = D_transpose.transpose()

        return np.dot(D_transpose, D), D
        
    def fit_points(self, points):
        '''
        Fit the points to an ellipse.
        '''
        points = np.float64(points)
        
        #first we normalize the points
        normalization_matrix = self._get_normalization_matrix(points)
        normalized_points = self._normalize_points(normalization_matrix, points)

        S, D = self._get_scatter_matrix(normalized_points)
        
        #we now solve the generalized eigenproblem: C*a = lambda * S * a
        #Note that we put S on the righthand side (unlike what was done in the paper) since numpy expects the thing on the RIGHT
        #to be positive definite.

        try:
            eigen_values, eigen_vectors = eigh(self._C, S)
        except np.linalg.linalg.LinAlgError:
            #if we could not diagonalize the matrix, return None
            return None, None

        #use the eigenvector with positive eigenvalue
        index_of_positive = (eigen_values > self._epsilon).nonzero()[0][0]
        ellipse_parameters = eigen_vectors[:, index_of_positive]

        mu = np.dot(ellipse_parameters, self._C)
        mu = np.dot(mu, ellipse_parameters)
        mu = 1 / np.sqrt(mu)
        ellipse_parameters *= mu
        
        #now denormalize the ellipse parameters. I.e, find the ellipse passing
        #through the original points. (the ones we had before normalizing them)
        ellipse_parameters = self._denormalize_shape_parameters(normalization_matrix, ellipse_parameters)

        #construct an ellipse object and calculate the algebraic residue
        self._fitted_shape = Ellipse(*ellipse_parameters.tolist())
        self._algebraic_residue = self._fitted_shape.get_algebraic_residue(points)

        return self._fitted_shape, self._algebraic_residue
    
        
