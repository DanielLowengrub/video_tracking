import planar_shape
import numpy as np
import cv2
from ..rectangle import Rectangle

class Ellipse(planar_shape.PlanarShape):
    '''
    This implements the Ellipse shape.
    
    We represent an ellipse as the set of zeros to the equation
    a*x^2 + b*xy + c*y^2 + d*x + e*y + f
    and we store the tuple (a, b, c, d, e, f).

    In addition to the functions inherited from PlanarShape, we implement some
    ellipse specific functions such as returning the center and the long / short axes.
    '''

    def __init__(self, a, b, c, d, e, f):
        super(Ellipse, self).__init__(planar_shape.ELLIPSE)

        self._a = a
        self._b = b
        self._c = c
        self._d = d
        self._e = e
        self._f = f

        self._center = None
        self._axes = None
        self._angle = None
        self._has_geometric_parameters = False
        
        self._epsilon = 10**(-10)
        self._is_degenerate = None
        self.compute_geometric_parameters()
        
    def __str__(self):
        self.compute_geometric_parameters()
        return 'Ellipse: center=%s, axes=%s, angle=%f' % (str(self._center), str(self._axes), self._angle)

    def _repr__(self):
        return str(self)
    
    @classmethod
    def from_parametric_representation(cls, parametric_ellipse):
        '''
        parametric_ellipse is a numpy array of shape (3,3). Its values are
        (a    b/2  d/2)
        (b/2  c    e/2)
        (d/2  e/2    f)
        '''
        C = parametric_ellipse
        
        return cls(C[0,0], 2*C[0,1], C[1,1], 2*C[0,2], 2*C[1,2], C[2,2])
    
    @classmethod
    def from_geometric_parameters(cls, center, axes, angle):
        '''
        center - a length 2 numpy array
        axes - a length 2 numpy array
        angle - a number

        Return an ellipse with the specified center. axes[0] is the length of the major axis and axes[1] is the length of the minor axis.
        angle is the rotation of the major axis from the x axis in radians
        '''
        center = np.float64(center)
        axes = np.float64(axes)
        angle = np.float64(angle)
        
        #first construct a rotation matrix in the opposite direction
        rotation_matrix = np.array([[np.cos(-angle), -np.sin(-angle)],
                                    [np.sin(-angle), np.cos(-angle)]], np.float64)
        diag = np.diag(1.0 / (axes * axes))
        
        Q = np.dot(rotation_matrix.transpose(), np.dot(diag, rotation_matrix))
        K = center

        #we now can express the points on the ellipse as the points X satisfying:
        #(X-K)^T * Q * (X-K) = 1
        #
        #We can rewrite this as:
        #
        #X^T * Q * X - X^T * (Q + Q^T) * K + K^T * Q * K - 1 = 0
        #
        #However, note that
        # X^T[a    0.5b]X + X^T [d] + f = 0
        #    [0.5b    c]        [e]
        #So:
        B = -np.dot(Q + Q.transpose(), K)

        a = Q[0][0]
        b = 2 * Q[1][0]
        c = Q[1][1]
        d = B[0]
        e = B[1]
        f = np.dot(K.transpose(), np.dot(Q, K)) - 1

        ellipse = cls(a, b, c, d, e, f)

        #also set the geometric parameters so we dont have to recalculate them
        
        return ellipse

    def get_parameters(self):
        '''
        Return a numpy array of length 6 with values:
        [a, b, c, d, e, f]
        '''

        return np.array([self._a, self._b, self._c, self._d, self._e, self._f], np.float64)
    
    def get_conic_string(self):
        return '%fx^2 + %fxy + %fy^2 + %fx + %fy + %f' % tuple(self.get_parameters().tolist())

    def get_parametric_representation(self):
        '''
        We return the 3x3 matrix:

        (a    b/2  d/2)
        (b/2  c    e/2)
        (d/2  e/2    f)
        '''

        C = np.zeros((3,3), np.float32)
        C[0,0] = self._a
        C[0,1] = C[1,0] = self._b / 2
        C[1,1] = self._c
        C[0,2] = C[2,0] = self._d / 2
        C[1,2] = C[2,1] = self._e / 2
        C[2,2] = self._f

        return C

    def get_dual(self):
        '''
        We return a 3x3 matrix C such that for any length 3 vector l, C*l = 0 iff l represents a line that is tangent to a point on the ellipse.

        Recall that if C is the parametric representation of the ellipse, then the dual is simply the inverse C^{-1}.
        '''

        C = self.get_parametric_representation()

        return np.linalg.inv(C)

    def get_dual_ellipse(self):
        '''
        return an Ellipse object that represents the dual to this ellipse.
        '''
        return Ellipse.from_parametric_representation(self.get_dual())
    
    def get_algebraic_distance_from_point(self, point):
        if self._is_degenerate:
            raise RuntimeError('can not compute distance to a degenerate ellipse')
        
        return (self._a * point[0]**2 + self._b * point[0]*point[1] + self._c * point[1]**2 +
                self._d * point[0] + self._e * point[1] + self._f)

    def get_geometric_distance_from_points(self, points):
        '''
        points - a numpy array with shape (num points, 2)
        We return a number array of length (num points) containing the geometric distances of each of the points to the ellipse.

        Here we use an approximation to the geometric distance.

        Let Q(P) be the algebraic distance of the point P from the ellipse. Our approximation of the geometric distance is:

        |Q(P)| / ||gradQ(P)||

        Note that gradQ = (2a*x + b*y + d, 2c*y + b*x + e) 
        '''
        if self._is_degenerate:
            raise RuntimeError('can not compute distance to a degenerate ellipse')
        
        points = points.reshape((-1,1,2))
        algebraic_residues = self._get_algebraic_residues(points)

        gradients_x = (2 * self._a * points[:,0,0]) + (self._b * points[:,0,1]) + self._d
        gradients_y = (2 * self._c * points[:,0,1]) + (self._b * points[:,0,0]) + self._e

        gradients = np.vstack([gradients_x, gradients_y]).transpose()
        gradient_norms = np.linalg.norm(gradients, axis=1)
        geometric_distances = np.abs(algebraic_residues) / gradient_norms

        return geometric_distances

        
    def _get_algebraic_residues(self, points):
        '''
        points - a numpy array of shape (number of points, 1, 2)
        Return a numpy array of length (number of points) containing the squares of the algebraic distances of each of the points to the ellipse.
        '''
        if self._is_degenerate:
            raise RuntimeError('can not compute residues with a degenerate ellipse')
        
        points = points.reshape((-1,1,2))
        res = (self._a * points[:,0,0]**2 + self._b * points[:,0,0]*points[:,0,1] + self._c * points[:,0,1]**2 +
                self._d * points[:,0,0] + self._e * points[:,0,1] + self._f)

        return res
    
    def get_algebraic_residue(self, points):
        if self._is_degenerate:
            raise RuntimeError('can not compute residues with a degenerate ellipse')

        res = self._get_algebraic_residues(points)

        return (res * res).sum()

    def get_geometric_residue(self, points):
        '''
        Here we use an approximation to the geometric distance.

        Let Q(P) be the algebraic distance of the point P from the ellipse. Our approximation of the square of the geometric distance is:

        |Q(P)|**2 / (|gradQ(P)|**2)

        Note that gradQ = (2a*x + b*y + d, 2c*y + b*x + e) 
        '''
        if self._is_degenerate:
            raise RuntimeError('can not compute residues with a degenerate ellipse')

        points = np.float64(points).reshape((-1,1,2))
        algebraic_residues = self._get_algebraic_residues(points)

        gradients_x = (2 * self._a * points[:,0,0]) + (self._b * points[:,0,1]) + self._d
        gradients_y = (2 * self._c * points[:,0,1]) + (self._b * points[:,0,0]) + self._e

        gradients = np.vstack([gradients_x, gradients_y]).transpose()

        geometric_residues_squared = (algebraic_residues * algebraic_residues) / ( (gradients * gradients).sum(axis=1) )

        return geometric_residues_squared.sum()

    def get_tangent_vector(self, point):
        '''
        We find the normal vector to the point and rotate by 90 degrees.
        '''
        if self._is_degenerate:
            raise RuntimeError('can not compute tangent to a degenerate ellipse')

        normal_x = (2 * self._a * point[0]) + (self._b * point[1]) + self._d
        normal_y = (2 * self._c * point[1]) + (self._b * point[0]) + self._e

        normal = np.array([-normal_y, normal_x])
        length = np.linalg.norm(normal)

        return normal / length

        
    def _get_quadratic_form(self):
        '''
        We compute the quadratic form corresponding to the algebraic representation.
        In other words, we return a 2x2 matrix Q such that
        
        ax^2 + bxy + cy^2 = [x y] Q [x]
                                    [y]
        '''

        Q = np.zeros((2,2))
        Q[0][0] = self._a
        Q[0][1] = Q[1][0] = self._b / 2
        Q[1][1] = self._c

        return Q

    def _get_translation(self):
        '''
        Return a length 2 vector B such that
        ax^2 + bxy + cy^2 + dx + ey = [x y] Q [x] + B^T [x]
                                              [y]       [y]
        '''
        if self._is_degenerate:
            raise RuntimeError('can not compute parameters of a degenerate ellipse')

        B = np.array([self._d, self._e])
        return B
    
    def _get_center(self):
        '''
        Return the center of the ellipse as a length 2 numpy array.

        We use the formula (see http://www.geometrictools.com/Documentation/InformationAboutEllipses.pdf):

        Center = (Q[1][1]B[0] - Q[0][1]B[1], Q[0][0]B[1] - Q[0][1]B[0]) / 2*(Q[0][1]^2 - Q[0][0]Q[1][1])
        '''
        Q = self._Q
        B = self._B

        center = np.array([Q[1][1]*B[0] - Q[0][1]*B[1], Q[0][0]*B[1] - Q[0][1]*B[0]]) / (2*(Q[0][1]**2 - Q[0][0]*Q[1][1]))

        return center

    def _get_normalized_form(self):
        '''
        Return a 2x2 array M such that if K is the center of the ellipse:

        X^T*Q*X + B^T*X + S = 0 iff and only if (X-K)^T * M * (X-K) = 1
        '''
        Q = self._Q
        S = self._S
        K = self._center

        # print 'Q: '
        # print Q

        # print 'S:'
        # print S

        # print 'K: ', K
        
        mu = 1 / (Q[0][0]*(K[0]**2) + 2*Q[0][1]*K[0]*K[1] + Q[1][1]*(K[1]**2) - S)

        # print 'mu: ', mu
        
        return mu * Q

    def _get_axes(self):
        '''
        Return a tuple (major axis, minor axis).
        We do this by explicitly diagonalizing the matrix M.
        '''

        M = self._M

        A = M[0][0] + M[1][1]
        B = np.sqrt( (M[0][0] - M[1][1])**2 + 4*(M[0][1]**2) )

        lambda_1 = (A + B) / 2
        lambda_2 = (A - B) / 2

        # print 'lambda_1 = ', lambda_1
        # print 'lambda_2 = ', lambda_2

        if min(lambda_1, lambda_2) < self._epsilon:
            self._is_degenerate = True
            return None

        else:
            self._is_degenerate = False
            
        major_axis = 1 / np.sqrt(lambda_2)
        minor_axis = 1 / np.sqrt(lambda_1)

        return np.array([major_axis, minor_axis])

    def _get_angle(self):
        '''
        Return the angle (in radians) that the major axis forms with the x axis.

        Note that if theta is the correct angle, then if we make a change of coordinates
        x = u*cos(theta) - v*sin(theta)
        y = u*sin(theta) + v*cos(theta)

        then the coefficent of uv should be 0 since the ellipse is axis aligned in the (u,v) coordinates.
        it is easy to calculate this coefficient and see that it is equal to:
        2*M[0][1]*cos(2*theta) + (M[1][1] - M[0][0])*sin(2*theta)
        
        so if (M[1][1] - M[0][0]) is not close to zero,
        tan(2*theta) = -2*M[0][1] / (M[1][1] - M[0][0])

        and if it is, then find the eigenvectors and eigenvalues of M.
        let u be the vector corresponding to the eigenvector whose eigenvalue has a large absolute value.

        return atan2(u[1], u[0])
        cos(2*theta) = 0 => theta is in {pi/4, 3*pi/4}

        Now, in this case, the matrix M must have the form 
        [a b]
        [b a]

        and the eigenvalues are a+b with eigenvector [1 1] and a-b with eigenvector [1 -1].
        So, if |a+b| > |a-b|, then the major axis is on 
        '''

        Q = self._Q
        M = self._M

        if np.abs(M[1][1] - M[0][0]) < self._epsilon:
            eigenvalues, eigenvectors = np.linalg.eig(M)

            index_of_major_axis = np.abs(eigenvalues).argmin()
            major_axis = eigenvectors[:,index_of_major_axis]

            return np.arctan2(major_axis[1], major_axis[0])

        if np.abs(M[0][1]) < self._epsilon:
            #if the larger axis is the x axis
            if M[0][0] < M[1][1]:
                return 0.0
            else:
                return np.pi / 2.0

        theta = 0.5 * np.arctan2(-2*M[0][1], (M[1][1] - M[0][0]))
        return theta

    def is_degenerate(self):
        '''
        Return true iff the normalized form of the ellipse has an eigen value smaller than EPSILON
        '''
        self.compute_geometric_parameters()
        return self._is_degenerate
    
    def compute_geometric_parameters(self):
        '''
        Compute the geometric representation of the ellipse in terms of the center, the major / minor axes, 
        and the rotation.
        '''

        if self._has_geometric_parameters:
            return
        
        #an alternative representation that is easier to work with. If X = (x,y) then
        #ax^2 + bxy + cy^2 + dx + ey + f = X^T*Q*X + B^T*X + S
        self._Q = self._get_quadratic_form()
        self._B = self._get_translation()
        self._S = self._f

        #the geometric representation
        self._center = self._get_center()

        self._M = self._get_normalized_form()

        self._axes = self._get_axes()

        if not self._is_degenerate:
            self._angle = self._get_angle()

        self._has_geometric_parameters = True
        
        return
    
    def get_geometric_parameters(self, in_degrees=False):
        '''
        Return a tuple (center, axis_lengths, rotation) where:
        position is a numpy vecor of length 2 and type np.float64
        axis_lengths is a numpy vecor of length 2 and type np.float64
        rotation is a number.

        if in_degrees is True then we return the angle in degrees.

        if the ellipse is degenerate then we raise a RuntimeError exception
        '''
        # if not self._has_geometric_parameters:
        #     self.compute_geometric_parameters()

        if self._is_degenerate:
            raise RuntimeError('The ellipse is degenerate and so it has no geometric parameters')
        
        angle = self._angle
        if in_degrees:
            angle *= 180.0 / np.pi
            
        return self._center, self._axes, angle

    def get_center(self):
        if self._is_degenerate:
            raise RuntimeError('The ellipse is degenerate and so it has no geometric parameters')

        return self._center
    
    def get_aa_bounding_box(self):
        '''
        Return: (width, height) of an axis aligned bounding box of the ellipse relative to the center of the ellipse
        The format of the output is such that it is compatible with opencv.
        
        Note that (x,y) is the center of the ellipse, and (x-width, y-height) is the top left corner.

        To do this, we first suppose that the ellipse is centered at the origin and has the equation:
        F = ax^2 + bxy + cy^2 = 1

        Then, it is not hard to prove that:
        grad(F)_x = 0  <=>  y^2 = 4a/(4ac - b^2) so the height of the bb is sqrt(4a/(4ac - b^2))
        grad(F)_y = 0  <=>  x^2 = 4c/(4ac - b^2) so the width  of the bb is sqrt(4c/(4ac - b^2))
        '''

        # if not self._has_geometric_parameters:
        #     self.compute_geometric_parameters()

        if self._is_degenerate:
            raise RuntimeError('The ellipse is degenerate and so it has no bounding box')

        #Get the correct a,b and c coefficients from the normalized matrix M
        a = self._M[0,0]
        b = 2 * self._M[0,1]
        c = self._M[1,1]

        denom = 4*a*c - b**2

        height = np.sqrt(4*a / denom)
        width = np.sqrt(4*c / denom)

        return int(width), int(height)

    def get_bounding_rectangle(self):
        '''
        return the smallest Rectangle object containing the ellipse.
        '''
        if self._is_degenerate:
            raise RuntimeError('The ellipse is degenerate and so it has no bounding rectangle')

        width, height = self.get_aa_bounding_box()
        top_left = np.int32(self.get_center() - np.array([width, height]))
        bottom_right = np.int32(self.get_center() + np.array([width, height]))
        
        return Rectangle(top_left, bottom_right)
    
    def get_area(self):
        # if not self._has_geometric_parameters:
        #     self.compute_geometric_parameters()

        if self._is_degenerate:
            raise RuntimeError('The ellipse is degenerate and so it has no area')

        return np.pi * self._axes[0] * self._axes[1]
        
    def compute_shape_similarity(self, other):
        '''
        other - a PlanarShape of the same type (line or ellipse)
        
        Return a number that measures the similarity of the shapes. A smaller number means that the shapes are more similar.
        For a line we compute the distance between the (radius, theta) parameters.
        '''
        self.compute_geometric_parameters()
        other.compute_geometric_parameters()

        if self._is_degenerate or other._is_degenerate:
            raise RuntimeError('can not compute similarity since one of the shapes is degenerate')
        
        center, axes, angle = self.get_geometric_parameters()
        other_center, other_axes, other_angle = other.get_geometric_parameters()

        params = np.hstack([center, axes, np.array(angle)])
        other_params = np.hstack([other_center, other_axes, np.array(other_angle)])

        print 'computing shape similarity...'
        print 'this shapes parameters: ', params
        print 'other shapes parameters: ', other_params
        return np.linalg.norm(params - other_params)

    def get_mask(self, image):
        '''
        image - a numpy array with shape (n,m,3) and type np.uint8

        return a numpy array with shape (n,m) and type np.float32.
        It has the value 1 on pixels that are in the ellipse.
        '''
        mask = np.zeros(image.shape[:2], np.float32)
        self.compute_geometric_parameters()

        if self._is_degenerate:
            raise RuntimeError('The ellipse is degenerate and so it has no mask')

        center, axes, angle = self.get_geometric_parameters(in_degrees=True)
        cv2.ellipse(mask, tuple(np.int32(center).tolist()), tuple(np.int32(axes).tolist()), angle, 0, 360, 1, -1, cv2.CV_AA)

        return mask
    
