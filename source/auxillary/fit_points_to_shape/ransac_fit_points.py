import numpy as np
import cv2

class RansacFitPoints(object):
    '''
    This implements a RANSAC algorithm that is used to fit a set of points to a shape if there are 
    some outliers in the points.
    '''

    def __init__(self, shape_fitter, min_points_to_fit, num_iterations, distance_to_shape_threshold, min_percentage_in_shape):
        '''
        shape_fitter - a FitPointsToShape object
        min_points_to_fit - The smallest number of points that can be used to fit the shape
        num_iterations - the number of iterations in the RANSAC algorithm
        distance_to_shape_threshold - we use this threshold to determine if a point lies on a shape
        min_percentage_in_shape - we only record shapes such that at least min_percentage_in_shape of 
           the points are within distance_to_shape_threshold of the shape.
        '''

        self._shape_fitter = shape_fitter
        self._min_points_to_fit = min_points_to_fit
        self._num_iterations = num_iterations
        self._distance_to_shape_threshold = distance_to_shape_threshold
        self._min_percentage_in_shape = min_percentage_in_shape

    def _sample_inlier_candidates(self, points):
        '''
        Return a random sample of the points of size min_points_to_fit
        '''
        sample_indices = np.random.choice(len(points), self._min_points_to_fit, replace=False)
        return points[sample_indices]

    def _get_initial_debugging_image(self, image, points):
        print points
        debugging_image = np.zeros(image.shape, np.uint8)
        color = (0,0,255)
        radius = 2
        #draw all of the points in red
        for point in points:
            cv2.circle(debugging_image, tuple(np.int32(point).tolist()), radius, color, thickness=-1)
        
        cv2.imshow('the points', debugging_image)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

        return debugging_image

    def _get_inlier_image(self, debugging_image, inliers):
        inlier_image = debugging_image.copy()
        color = (255,0,0)
        radius = 2
        #draw all of the points in red
        for point in inliers:
            cv2.circle(inlier_image, tuple(np.int32(point).tolist()), radius, color, thickness=-1)
        
        cv2.imshow('the inliers', inlier_image)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

        return inlier_image

    def _get_fitted_ellipse_image(self, image, ellipse):
        ellipse.compute_geometric_parameters()

        fitted_ellipse_image = image.copy()
        center_fitted, axes_fitted, angle_fitted = ellipse.get_geometric_parameters(in_degrees=True)
        color_fitted = (0,255,0)
        
        cv2.ellipse(fitted_ellipse_image, tuple(np.int32(center_fitted).tolist()), tuple(np.int32(axes_fitted).tolist()), angle_fitted, 0, 360, color_fitted, 1, cv2.CV_AA)
        
        cv2.imshow('the fitted ellipse', fitted_ellipse_image)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

        return fitted_ellipse_image
    
    def fit_points(self, points, image=None):
        '''
        points - a numpy array of shape (num points, 2)
        image - an image used to display debugging data

        Return a tuple shape, mask of inliers
        '''

        if len(points) < self._min_points_to_fit:
            return None, None, None
        
        debug = (image is not None)
        points = points.reshape((-1,2))
        
        if debug:
            print 'starting RANSAC with %d points...' % len(points)
            debugging_image = self._get_initial_debugging_image(image, points)
        
        best_shape = None
        best_inlier_mask = None
        best_geometric_residue = np.inf

        for i in range(self._num_iterations):
            if debug:
                print 'iteration: ', i
            
            #sample a subset of the points
            initial_inlier_candidates = self._sample_inlier_candidates(points)

            if debug:
                print '   the initial inlier candidates:'
                initial_inliers_image = self._get_inlier_image(debugging_image, initial_inlier_candidates)
            
            #fit a shape to these points
            shape_candidate, algebraic_residue = self._shape_fitter.fit_points(initial_inlier_candidates)
            if shape_candidate is None:
                continue
            
            if debug:
                print '   we fit the following shape:'
                fitted_shape_image = self._get_fitted_ellipse_image(initial_inliers_image, shape_candidate)
            
            #collect all points that fit the shape candidate
            geometric_distances = shape_candidate.get_geometric_distance_from_points(points)

            if debug:
                print '   the geometric distances are: '
                print geometric_distances
            
            inlier_candidates_mask = (geometric_distances < self._distance_to_shape_threshold)
            inlier_candidates = points[inlier_candidates_mask]

            if debug:
                inliers_image = self._get_inlier_image(fitted_shape_image, inlier_candidates)
            
            #if there are enough inliers, update the best shape
            inlier_ratio = float(len(inlier_candidates)) / len(points)

            if debug:
                print '   inlier ratio: ', inlier_ratio
                
            if inlier_ratio >= self._min_percentage_in_shape:
                geometric_residue = self._shape_fitter.get_geometric_residue()

                if debug:
                    print '   the geometric residue of these inliers is: ', geometric_residue
                    
                if geometric_residue < best_geometric_residue:
                    if debug:
                        print '   updating best model'
                        
                    best_geometric_residue = geometric_residue
                    best_shape = shape_candidate
                    best_inlier_mask = inlier_candidates_mask

        #finally, fit a shape to all of the inliers
        if best_inlier_mask is not None:
            best_inliers = points[best_inlier_mask]
            best_shape, algebraic_residue = self._shape_fitter.fit_points(best_inliers)

            if best_shape is not None:
                best_geometric_residue = self._shape_fitter.get_geometric_residue()
                #print 'bgr: ', best_geometric_residue
                #print '   found a shape with an inlier ratio %f and residue %f.' % (float(len(best_inliers)) / len(points), best_geometric_residue)
                
        return best_shape, best_geometric_residue, best_inlier_mask
