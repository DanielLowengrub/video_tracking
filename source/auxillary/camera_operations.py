import numpy as np
from ...source.auxillary.camera_matrix import CameraMatrix
from ...source.auxillary import planar_geometry, world_rotation
import cv2
import collections

"""
In this module we use the following coordinate systems:
WORLD:               CAMERA:     IMAGE:

      Z                 y          ------- u
      |                 | z       |    y
      |____ X           |/        |    |
      /                  ---x     | (uo,vo)-- x
     /                            v 
    Y
    
    The world coords are (X,Y,Z), the camera coords are (x,y,z) and the image coords are (u,v)
    Note that XxY = -Z. I.e, we are using a coordinate system with orientation -1.

A camera is represented by the following parameters:
theta_x, theta_z, focus, aspect, camera_center, camera_position

These parameters define a camera in the following way:
Let:
T     = camera_position
uo,vo = camera_center

Then the rotation matrix from camera coordinates (x,y,z) to world coordinates (X,Y,Z) is given by:
Rinv = rot_z(theta_z) * rot_x(theta_x)

I.e, first rotate the YZ plane clockwise by theta_x radians around the X axis then rotate the XY plane by theta_z radians
around the Z axis. Note that this means that the x axis of the camera is always on the XY world plane.

The intrinsic camera matrix is given by:
C = [focus 0             uo]
    [0     -aspect*focus vo]
    [0     0             1 ]

Finally, the camera matrix is as usual: P = C * [R | -RT] where R = Rinv^-1
"""

'''
This stores the parameters of a camera.
theta_x, theta_z, focus, aspect - floats
camera_center - a numpy array with shape (2,)
camera_position - a numpy array with shape (3,)
'''
CameraParameters = collections.namedtuple('CameraParameters', ['theta_x', 'theta_z', 'focus', 'aspect', 'camera_center',
                                                               'camera_position'])

def build_camera(camera_parameters):
    '''
    camera_parameters - a CameraParameters object.
    return - a CameraMatrix object
    '''
    R = np.matmul(world_rotation.rot_x(-camera_parameters.theta_x),
                  world_rotation.rot_z(-camera_parameters.theta_z))
    T  = camera_parameters.camera_position
    
    f = camera_parameters.focus
    a = camera_parameters.aspect
    uo,vo = camera_parameters.camera_center

    C = np.array([[f,  0,    uo],
                  [0,  -a*f, vo],
                  [0,  0,    1]])

    camera = CameraMatrix.from_position_rotation_internal_parameters(T, R, C)

    return camera

def apply_camera(image, camera, output_image_shape, pad_with_zero=False):
    '''
    image - a numpy array representing an image
    camera - a CameraMatrix object
    output_image_shape - a tuple (height, width)
    pad_with_zero - if True, the image is padded with zeros before being transformed.
       this can be done to fix a bug in cv2.warpPerspective

    we consider the image as being in world coordinates by putting it in the plane z=0.
    we then use the camera to map it to an image with the given shape.
    '''
    H = camera.get_homography_matrix()

    if pad_with_zero:
        #pad all axes with zero
        image = np.pad(image, 1, mode='constant', constant_values=0)

        #also shift the homography by (-1,-1) (i.e, (0,0) -> (-1,-1))
        M = planar_geometry.translation_matrix(np.array([-1,-1], np.float32))
        H = np.matmul(H, M)

    output_image = cv2.warpPerspective(image, H, output_image_shape[::-1])
    return output_image

def _get_camera_coordinate_change(C):
    #we want to transform the camera parameters so that
    #the diagonal of C is (focus, -focus, 1), where focus is some positive number.
    M = np.eye(3)
    if C[0,0] < 0:
        M[0,0] = -1

    if C[1,1] > 0:
        M[1,1] = -1

    if C[2,2] < 0:
        M[2,2] = -1

    return M

def _compute_angles_from_rotation_matrix(R):
    '''
    R - a numpy array with shape (3,3)
    return - a tuple (theta_x, theta_z)

    the output is calculated such that:
    R^-1 ~ rot_z(theta_z)*rot_x(theta_x)
    '''
    theta_x = -np.arctan2(-R[1,2], R[2,2])
    theta_z = -np.arctan2(-R[0,1], R[0,0])
    return theta_x, theta_z

def compute_camera_parameters(camera):
    '''
    camera - a CameraMatrix object
    return - a CameraParameters object
    '''
    camera_position = camera.get_camera_position()
    C = camera.get_internal_parameters()
    R = camera.get_world_to_camera_rotation_matrix()
    
    #transform the camera coordinates so that y is pointing up
    M = _get_camera_coordinate_change(C)
    C = np.matmul(C, M)
    R = np.matmul(M, R)
    
    #scale C so that C[2,2]=1
    C /= C[2,2]
    
    #get the angles from the camera direction
    theta_x, theta_z = _compute_angles_from_rotation_matrix(R)

    camera_center = C[:2, 2]
    focus  = C[0,0]
    aspect = -C[1,1] / focus
    
    return CameraParameters(theta_x, theta_z, focus, aspect, camera_center, camera_position)
