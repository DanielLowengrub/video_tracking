import numpy as np
import scipy.linalg
import cv2
from ..data_structures.contour import Contour
from ..auxillary import np_operations

##############################
# A collection of basic 2D geometry methods
#############################

EPSILON = 10**(-5)
ELLIPSE_INTERSECTION_PREC = 10
LINE_REGION_INTERSECTION_PREC = 5

TANGENT_THRESH = 10**(-1) #the maximum allowed distance between a tangent line and the ellipse

def perp(vector):
    '''
    vector - a length 2 numpy array
    return a vector perpendicular to the given one
    '''

    return np.array([-vector[1], vector[0]], np.float64)

def rotation_matrix(angle):
    '''
    angle - an angle in radians

    return the 2x2 rotation matrix that rotates vectors by the specified angle
    '''

    rotation_matrix = np.array([[np.cos(angle), -np.sin(angle)],
                                [np.sin(angle), np.cos(angle)]], np.float64)

    return rotation_matrix

def translation_matrix(translation):
    '''
    translation - a numpy array with shape (2,) and type np.float32

    Return a numpy array with shape (3,3) and type np.float32. It represents a homography which translates the plane
    by translation.
    '''
    T = np.eye(3, dtype = np.float64)
    T[:2,2] = translation

    return T

def scaling_matrix(scale):
    '''
    scale - a numpy array with shape (2,) and type np.float32

    Return a numpy array with shape (3,3) and type np.float32. It represents a homography which scales the plane
    by scale[0] in the x direction and scale[1] in the y direction.
    '''
    return np.diag(np.append(scale, 1)).astype(np.float64)

def rotate_vector(vector, angle):
    '''
    vector - a length 2 numpy array
    angle - an angle in radians

    Return a length 2 vector which is the rotation of the input vector by the angle
    '''

    R = rotation_matrix(angle)
    return np.dot(R, vector)

def rotate_vectors(vectors, angle):
    '''
    vector - a numpy array with shape (num points, 2)
    angle - an angle in radians

    Return a numpy array with shape (num points, 2) which is the rotation of the input vector by the angle
    '''

    R = rotation_matrix(angle)
    rotated_vectors = np.dot(R, vectors.transpose()).transpose()
    return rotated_vectors

def compute_jac_H(H, p):
    '''
    H - a numpy array with shape (3,3)
    p - a numpy array with shape (2,)

    return - a numpy array with shape (2,2). It represents the jacobian of the function:
    x -> H(x)
    at the point p
    '''
    x,y = p
    jacH00 = -H[0,1]*H[2,0]*y + H[0,0]*H[2,1]*y - H[0,2]*H[2,0] + H[0,0]*H[2,2]
    jacH01 =  H[0,1]*H[2,0]*x - H[0,0]*H[2,1]*x - H[0,2]*H[2,1] + H[0,1]*H[2,2]
    jacH10 = -H[1,1]*H[2,0]*y + H[1,0]*H[2,1]*y - H[1,2]*H[2,0] + H[1,0]*H[2,2]
    jacH11 =  H[1,1]*H[2,0]*x - H[1,0]*H[2,1]*x - H[1,2]*H[2,1] + H[1,1]*H[2,2]
    
    jacH = np.array([[jacH00, jacH01],
                     [jacH10, jacH11]])
    denom = (H[2,0]*x + H[2,1]*y + H[2,2])**2

    return jacH / denom

def apply_homography_to_point(H, point):
    '''
    H - a 3x3 numpy array representing a homography matrix
    point - a numpy array with shape (2,) and type np.float32

    Return a numpy array with shape (2,) and type np.float32 which represents the image of the point under H.
    '''

    projective_source_point = np.append(point, 1)
    projective_target_point = np.dot(H, projective_source_point)
    target_point = projective_target_point[:2] / projective_target_point[2]

    return target_point

def apply_homography_to_points(H, points):
    '''
    H - a 3x3 numpy array representing a homography matrix
    points - a numpy array with shape (num points, 2) and type np.float32

    Return a numpy array with shape (num points, 2) and type np.float32 which represents the images of the points under H.
    '''
    # print 'H:'
    # print H
    # print 'applying homography to points:'
    # print points
    
    ones = np.ones((points.shape[0],1), np.float64)
    projective_source_points = np.hstack([points, ones])
    projective_target_points = np.dot(H, projective_source_points.transpose()).transpose()
    target_points = projective_target_points[:,:2] / projective_target_points[:,2].reshape((-1,1))

    # print 'projective source points: '
    # print projective_source_points
    # print 'projective target points: '
    # print projective_target_points
    # print 'target points:'
    # print target_points
    
    return target_points

def apply_homography_to_line(H, line):
    '''
    H - a 3x3 numpy array representing a homography matrix
    line - Line object

    Return a new Line object representing the image of the given line under the homography.
    
    If L the the original line, the p\inL <=> L^Tp = 0. So:
    L'^Tq=0 <=> L^T*H^{-1}*q <=> (H^{-T}L)^Tq = 0. So L' = H^{-T}L
    '''
    from .fit_points_to_shape.line import Line

    line_parameters = line.get_parametric_representation()
    
    new_line_parameters =  np.dot(np.linalg.inv(H).transpose(), line_parameters)

    return Line.from_parametric_representation(new_line_parameters)

def apply_homography_to_ray(H, ray):
    '''
    H - a numpy array with shape (3,3)
    ray - a numpy array with shape (2,2) representing a ray. It has the form: 
       [[source_x, source_y],
        [direction_x, direction_y]]
    '''
    p = ray[0]
    v = ray[1]

    #get the jacobian of H at p
    jacH = compute_jac_H(H,p)
    
    Hp = apply_homography_to_point(H, p)
    Hv = np.matmul(jacH, v)

    return np.vstack([Hp, Hv])

def apply_homography_to_ellipse(H, ellipse):
    '''
    H a 3x3 matrix representing a homography matrix
    ellipse - an Ellipse object

    Return a new Ellipe object obtained by applying H to the given ellipse

    If E is the original ellipse, then p\inE <=> p^T*E*p = 0 So:
    q^T*E'*q = 0 <=> (H^-1q)^T*E*(H^-1q) = 0 <=> q^T*H^{-T}*E*H^{-1}*q = 0. So E' = H^{-T}*E*H^{-1}.
    '''
    from .fit_points_to_shape.ellipse import Ellipse
    
    E = ellipse.get_parametric_representation()
    H_inverse = np.linalg.inv(H)
    new_E = np.dot(H_inverse.transpose(), np.dot(E, H_inverse))

    # print 'H_inverse: '
    # print H_inverse
    # print 'new E:'
    # print new_E
    
    new_ellipse = Ellipse.from_parametric_representation(new_E)
    # print 'new ellipse: '
    # print new_ellipse

    return new_ellipse

def apply_homography_to_conic(H, conic):
    '''
    H a 3x3 matrix representing a homography matrix
    conic - a numpy array with shape (3,3)

    Return a numpy array with shape (3,3) which represents the transformation of the original conic by H.

    If C is the original conic, then p\inC <=> p^T*C*p = 0 So:
    q^T*C'*q = 0 <=> (H^-1q)^T*C*(H^-1q) = 0 <=> q^T*H^{-T}*C*H^{-1}*q = 0. So C' = H^{-T}*C*H^{-1}.
    '''
    H_inverse = np.linalg.inv(H)
    new_conic = np.dot(H_inverse.transpose(), np.dot(conic, H_inverse))

    # print 'H_inverse: '
    # print H_inverse
    # print 'new E:'
    # print new_E
    
    return new_conic

def apply_homography_to_shape(H, planar_shape):
    '''
    H a 3x3 matrix representing a homography matrix
    planar_shape - a PlanarShape object

    Return a new PlanarShape obtained by applying H to the given shape
    '''
    #print 'applying homography to: ', planar_shape
    if planar_shape.is_line():
        return apply_homography_to_line(H, planar_shape)
    
    elif planar_shape.is_ellipse():
        return apply_homography_to_ellipse(H, planar_shape)

    else:
        raise ValueError('can only apply homography to lines and ellipses')

def apply_homography_to_mask(H, source_mask, target_mask_shape):
    '''
    H - a numpy array with shape (3,3) and type np.float64. It represents a homography.
    source_mask - a numpy array with shape (n,m) and type np.bool.
    target_mask_shape - a tuple (height, width) representing the shape of the target mask.

    return a numpy array M with shape "target_mask_shape" and type np.bool. M[y,x]=True iff
    source_mask[H^-1([x,y])] = True
    '''
    from .rectangle import Rectangle
    
    #compute the homography from the target to the source
    H_inv = np.linalg.inv(H)
    target_mask = np.zeros(target_mask_shape, dtype=np.bool)
    
    #first record the (x,y) values at each point in the target mask
    x_coords = np.arange(0,target_mask_shape[1],1).astype(np.float64)
    y_coords = np.arange(0,target_mask_shape[0],1).astype(np.float64)
    XX,YY = np.meshgrid(x_coords, y_coords)

    #this is a numpy array with shape target_mask_shape+(2,). At position [y,x] is stores the arraye [x,y]
    target_points = np.dstack([XX,YY])
    
    #for each pixel in the target mask, record which point in the source gets mapped to it
    #this is also a numpy array with shape target_mask_shape+(2,)
    source_points = apply_homography_to_points(H_inv, target_points.reshape((-1,2))).reshape(target_points.shape)
    source_points = np.int32(source_points)
    
    #record which of the source points are in the rectangle containing the source mask
    source_rectangle = Rectangle.from_array(source_mask)
    in_source_rectangle = source_rectangle.contains_points(source_points.reshape((-1,2))).reshape(target_mask_shape)
    
    #if the source point corresponding to a pixel p IS NOT in the source mask rectangle,
    #then it is not the transformation of ANY source point and so it can not be in the target mask
    target_mask[np.logical_not(in_source_rectangle)] = False

    #if the source point (x,y) corresponding to a pixel p IS in the source mask rectangle,
    #then check if it lies in the source mask. Recall that the source point (x,y) lies in the source mask
    #iff source_mask[y,x] is true.
    source_points_in_rectangle = source_points[in_source_rectangle]
    target_mask[in_source_rectangle] = source_mask[source_points_in_rectangle[:,1], source_points_in_rectangle[:,0]]

    return target_mask

def compute_homography_to_unit_circle(ellipse):
    '''
    ellipse - an Ellipse object
    Return a homography H taking the ellipse to the unit circle at the origin
    '''

    ellipse.compute_geometric_parameters()
    center, axes, angle = ellipse.get_geometric_parameters()
    radius = axes[0]

    #first translate the center to the origin
    T = np.diag(np.ones(3))
    T[:2,2] = -center.transpose()

    #then rotate by -angle
    R = np.diag(np.ones(3))
    R[:2,:2] = rotation_matrix(-angle)
    
    #then scale by 1/axes
    S = np.diag(np.append(1/axes, 1))

    return np.dot(S,np.dot(R,T))

def compute_homography_to_unit_line(line):
    '''
    line - a numpy array with length 3

    return - a numpy array H with shape (3,3). H has the property: H^-T*line = (1,0,0)
    '''
    #We first complete the line to an orthogonal system
    #A is the matrix that projects onto the complement of the line
    scale = np.linalg.norm(line)
    normed_line = line / scale
    A = np.eye(3, dtype=np.float64) - np.matmul(normed_line.reshape((3,1)), normed_line.reshape((1,3)))

    #uv is a matrix whose columns u and v for an orthogonal basis with the line
    uv = scipy.linalg.orth(A)
    
    #R is a rotation matrix that takes (1,0,0) to the normed line
    R = np.hstack([normed_line.reshape((3,1)), uv])

    #R^-T takes the normed line to (1,0,0). So (scale * R)^-T takes the line to (1,0,0)    
    return scale * R.transpose()

#NOTE: This is obsolete. Instead use compute_homography_to_unit_line which transforms a line (a,b,c) to the line (1,0,0).
#this served the same purpose as transforming to the y axis
def compute_homography_to_y_axis(line):
    '''
    line - a Line
    Return a homography that transforms the line to the y axis
    '''

    L = line.get_parametric_representation()
    H = np.diag(np.ones(3))
    
    #First rotate the vector L[:2] to the vector (1,0). This transforms the line to (x=-L[2]).
    v = L[:2]
    v_perp = perp(v)
    R = np.vstack([v, v_perp]).transpose()
    H[:2,:2] = np.linalg.inv(R)

    #Then translate by (L[2], 0)
    H[:2,2] = np.array([L[2], 0])

    return H

def line_ellipse_intersection(line, ellipse):
    '''
    line - a Line 
    ellipse - an Ellipse

    Return either 2 points or no points, depending on whether there is an intersection.
    If there are two points, we return them in a 2x2 numpy array
    '''
    ellipse.compute_geometric_parameters()
    center, axes, angle = ellipse.get_geometric_parameters()
    #print 'intersecting the line: ', line
    #print 'with the ellipse: center=%s, axes=%s, angle=%s' % (center, axes, angle)
    
    #first transform the ellipse to the unit circle
    H = compute_homography_to_unit_circle(ellipse)

    #transform the line and intersect it with the unit circle
    a,b,c = apply_homography_to_line(H, line).get_parametric_representation().tolist()

    #print 'the transformed line has parameters: %f, %f, %f' % (a,b,c)
    
    #compute the discriminant. if it is negative, there are no intersection points
    D = 1 - c**2
    if D <= EPSILON:
        return []

    #if a>=b, solve for y and then x = (c - by)/a
    #else, solve for x.
    if np.abs(a) >= np.abs(b):
        #note that a^2+b^2=1 which simplifies the solution for (-b*y - c) + a^2*y^2 - a^2 = 0
        y_list = [-b*c + i*a*np.sqrt(D) for i in (-1,1)]
        x_list = [(-c - b*y)/a for y in y_list]

    else:
        x_list = [-a*c + i*b*np.sqrt(D) for i in (-1,1)]
        y_list = [(-c - a*x)/b for x in x_list]

    intersection_points = np.vstack([np.array([x,y]) for x,y in zip(x_list, y_list)])
    
    #finally, we must transform the points back to the original coordinates
    intersection_points = cv2.perspectiveTransform(intersection_points.reshape((-1,1,2)), np.linalg.inv(H)).reshape((-1,2))

    return intersection_points

def tangent_ellipse_intersection(line, ellipse):
    '''
    line - a Line 
    ellipse - an Ellipse
    
    return - a point

    We assume that the line is tangent to the ellipse. Return the tangency point.
    If the line is not tangent a ValueError is raised.
    '''
    ellipse.compute_geometric_parameters()
    center, axes, angle = ellipse.get_geometric_parameters()
    #print 'intersecting the line: ', line
    #print 'with the ellipse: center=%s, axes=%s, angle=%s' % (center, axes, angle)
    
    #first transform the ellipse to the unit circle
    H = compute_homography_to_unit_circle(ellipse)

    #transform the line and intersect it with the unit circle
    line = apply_homography_to_line(H, line)
    a,b,c = line.get_parametric_representation().tolist()

    #print 'normalized tangent line: ', line
    #recall that the line is equal to the set of points (x,y) such that (normal to line) * (x,y) = -c.
    #since the line is tangent to the unit circle, c must satisfy |c| = 1 and the tangency point is
    #-c * normal_to_line

    # print 'line: ', line
    # print 'type of line: ', line.get_parametric_representation().dtype
    
    #make sure that |c| is near 1
    if np.abs(np.abs(c) - 1) > TANGENT_THRESH:
        raise ValueError('the line is not tangent to the ellipse!')

    intersection_point = -c * line.get_normal_vector()

    #transform the point back to the original coordinates
    intersection_point = cv2.perspectiveTransform(intersection_point.reshape((1,1,2)), np.linalg.inv(H)).reshape((2,))

    return intersection_point

def projective_line_line_intersection(line_1, line_2):
    '''
    line_i - a tuple of Line object
    If the lines are not equal (up to a threshold), return their intersection as a length 3 array. Else, return None.
    '''
        
    #get the parametric representation of the intersection point
    parametric_lines = [line.get_parametric_representation() for line in (line_1, line_2)]
    projective_intersection_point = np.cross(*parametric_lines)

    #if the lines are equal return None
    if np.linalg.norm(projective_intersection_point) < EPSILON:
        return None

    return projective_intersection_point

def affine_line_line_intersection(line_1, line_2):
    '''
    line_i - a tuple of Line object
    '''
    projective_intersection_point = projective_line_line_intersection(line_1, line_2)
    if projective_intersection_point is None or np.abs(projective_intersection_point[2]) < EPSILON:
        return None

    return projective_intersection_point[:2] / projective_intersection_point[2]

def ellipse_ellipse_intersection(ellipse_1, ellipse_2):
    '''
    ellipse_i - an Ellipse object
    return a tuple of points which represent the intersection points.

    Algorithm:
    1) Compute homography H which takes ellipse_1 to a unit circle.
       So after this step, the equations for the ellipses are:
       x^2 + y^2 - 1 = 0
       ax^2 + bxy + cy^2 + dx + ey + f = 0
    2) Using elimination theory (in Sage), it is easy to see that the above two equations impose the following equation on x:
       (a^2 + b^2 - 2*a*c + c^2)*x^4 + (2*a*d - 2*c*d + 2*b*e)*x^3 + (-b^2 + 2*a*c - 2*c^2 + d^2 + e^2 + 2*a*f - 2*c*f)*x^2 + 
       (2*c*d - 2*b*e + 2*d*f)*x + c^2 - e^2 + 2*c*f + f^2
       We use this to find up to 4 solutions for x. We group them by multiplicity. e.g: ((x1,1), (x2,3))
    3) for each solution for (x,k) s.t x^2 <= 1 there are k possible solutions for y: y^2 = 1 - x^2.
       If k=2, then this gives us two intersection points: (x,y1), (x,y2)
       If k=1, then check which of the two solutions for y give a point (x,y) on the second ellipse
    4) Use H to map the intersection points from 3 back to the original coordinate system
    '''
    #transform ellipse_1 to the unit circle
    H = compute_homography_to_unit_circle(ellipse_1)
    ellipse_2 = apply_homography_to_ellipse(H, ellipse_2)
    a,b,c,d,e,f = ellipse_2.get_parameters()
    
    #compute the x coords of the intersection points
    p = [a**2 + b**2 - 2*a*c + c**2, 2*a*d - 2*c*d + 2*b*e, -b**2 + 2*a*c - 2*c**2 + d**2 + e**2 + 2*a*f - 2*c*f, 
         2*c*d - 2*b*e + 2*d*f, c**2 - e**2 + 2*c*f + f**2]

    roots = np.around(np.roots(p), decimals=ELLIPSE_INTERSECTION_PREC)
    real_roots = np.real(roots[np.isreal(roots)])
    # print 'real roots: ', real_roots
    x_coords, multiplicities = np.unique(real_roots, return_counts=True)

    #for each x coord, compute the possible y coords
    intersection_points = []
    for x, m in zip(x_coords, multiplicities):
        # print 'considering: (%f, %d)' % (x,m)
        
        #if x^2 > 1, there are no real solutions
        if x**2 > 1:
            continue

        #compute the two y values are the 2 solutions to x^2 + y^2 = 1. These give to possible intersection points.
        y = np.sqrt(1 - x**2)
        points = np.array([[x,y], [x,-y]])

        # print '   points: '
        # print points
        #if the multiplicity is 2, then they are both solutions
        if m == 2:
            # print '   adding both points'
            intersection_points.append(points)

        #otherwise, find the one that lies on ellipse_2
        else:
            errors = np.abs(np.array([ellipse_2.get_algebraic_distance_from_point(p) for p in points]))
            closest_index = np.argmin(errors)
            closest_point = points[closest_index]

            # print 'errors: ', errors
            # print 'adding point: ', closest_point
            intersection_points.append(closest_point)

    #transform the intersection points back into the original coordinate system
    H_inv = np.linalg.inv(H)
    intersection_points = np.vstack(intersection_points)
    intersection_points = apply_homography_to_points(H_inv, intersection_points)

    return intersection_points

#TODO:
#Since the dual to an ellipse is not necessarily an ellipse, if we want this to work we have to implement a
#conic_conic_intersection method. This is similar to ellipse_ellipse_intersection, except that we have to distinguish
#between three cases:
#1) b^2 - 4*a*c = 0 for one of the ellipses. In this case, one of the ellipses can be brought to the form:
#    x^2 - y = 0 => y = x^2. So we can substitue y=x^2 in the second conic to solve for x.
#2) b^2 - 4*a*c > 0 for one of the conics. In this case, that conic can be converted to a unit ellipse and we can proceed
#   as in the ellipse_ellipse intersection.
#3) b^2 - 4*a*c < 0 for both of the conics. In this case, one of the conics can be brought to the form: x^2 - y^2 - 1 = 0 and
#   we can proceed via elimination theory as in 2
def compute_bitangent_lines(ellipse_1, ellipse_2):
    '''
    ellipse_i - Ellipse objects
    
    return - a tuple of four Line objects which represent the bitangent lines.
    '''

    #compute the dual ellipses
    dual_1 = ellipse_1.get_dual_ellipse()
    dual_2 = ellipse_2.get_dual_ellipse()
    
    #the duals of the bitangent lines are the intersection points of the duals
    bitangent_points = conic_conic_intersection(dual_1, dual_2)

    #turn the dual bitangents into lines
    bitangent_lines = [Line.from_parametric_representation(p) for p in bitangent_points]

    return bitangent_lines

def projective_line_conic_intersection(line, conic):
    '''
    line - a numpy array with length 3
    conic - a numpy array with shape (3,3). It has to be symmetric but not necessarily invertable

    return - a tuple numpy arrays with length 3.

    The output arrays represent the real intersection points in P^2 of the line and the conic

    Algorithm:
    let line: (u,v,w)
    let conic: C

    1) Find a rotation matrix R such that R^T * (u,v,w) = (1,0,0)
    2) Replace C with R^T*C*R which is rep by: ax^2 + bxy + cy^2 + dxz + eyx + fz^2
    3) since the line is now (x=0), we have to solve: cy^2 + 2*eyz + fz^2 = 0
    4) First try to find points where y=1, then where z=1.
    5) map the points from 4) back to the original coordinate system
    '''

    #H is a homography that takes the line to (1,0,0)
    H = compute_homography_to_unit_line(line)

    #transform the conic to the new coordinate system
    C = apply_homography_to_conic(H, conic)

    # print 'the new conic: '
    # print C
    
    #In the new coordinate system, the equation for the line is (x=0)
    #so we have to solve the equation cy^2 + 2*eyz + fz^2 up to a scalar
    c,e,f = C[1,1], C[1,2], C[2,2]

    intersection_points = []
    
    #first try to solve it with y=1. This means that: c + 2e*z + f*z^2
    z_poly = np.poly1d([f, 2*e, c])
    z_roots = z_poly.r[np.isreal(z_poly.r)]

    # print 'z poly:'
    # print z_poly
    # print 'z roots: ', z_roots

    intersection_points += [np.array([0,1,z]) for z in z_roots]

    #then find solutions with z=1
    y_poly = np.poly1d([c, 2*e, f])
    y_roots = y_poly.r[np.isreal(y_poly.r)]

    # print 'y poly: '
    # print y_poly
    # print 'y roots: ', y_roots
    intersection_points += [np.array([0,y,1]) for y in y_roots]

    #remove duplicate intersection points. to do this, we first normalize the intersection points so that identical lines
    #will only differ by a factor of +-1. Then, for all points with positive y coord, we scale so that that coord is positive.
    #similar for the points with positive z coord. After this scaling, two intersection points are unique iff they represent
    #the same line.
    intersection_points = np.vstack(intersection_points)
    # print 'intersection points: '
    # print intersection_points

    y_not_zero = np.abs(intersection_points[:,1]) > EPSILON
    y_zero_z_not_zero = (np.abs(intersection_points[:,2]) > EPSILON) * np.logical_not(y_not_zero)    
    intersection_points[y_not_zero] /= intersection_points[y_not_zero][:,1].reshape((-1,1))
    intersection_points[y_zero_z_not_zero] /= intersection_points[y_zero_z_not_zero][:,2].reshape((-1,1))

    # print 'scaled intersection points:'
    # print intersection_points

    intersection_points = np_operations.find_unique_rows(intersection_points, decimals=ELLIPSE_INTERSECTION_PREC)
    
    # print 'unique intersection points: '
    # print intersection_points

    # print 'evaluating C on points: ', [np.matmul(p, np.matmul(C,p)) for p in intersection_points]

    #if there were no real intersection points, return an empty tuple
    if len(intersection_points) == 0:
        return ()

    #if there were, map them back to the original coordinates
    H_inv = np.linalg.inv(H)
    intersection_points = np.vstack(intersection_points)
    intersection_points = np.dot(H_inv, intersection_points.transpose()).transpose()

    return intersection_points

def compute_homography_on_line(line, projective_point_correspondences):
    '''
    line - a Line
    point_correspondences - a list of at least three tuples (point on line, transformation of point). Each point is a projective point given as a length 3 array.

    Return a homography matrix H such that the restriction of H to the line transforms the given points to the given transformed points.
    '''
    if len(projective_point_correspondences) < 3:
        raise ValueError('There are not enough points to compute a line homography')
    
    source_points = np.vstack([pc[0] for pc in projective_point_correspondences])
    target_points = np.vstack([pc[1] for pc in projective_point_correspondences])
    
    #First compute a homography M that takes the line to the line (x=0).
    #We will then find an appropriate homography H from (x=0), and the final homography will be HM.
    M = compute_homography_to_y_axis(line)
    new_source_points = np.dot(M, source_points.transpose()).transpose()

    #we now have a list of source points on the x=0 line, and a list of target points we want to map them to.
    #each pair pf points gives us equations for the second two columns of H. We get all these equations and then solve.
    equations = []
    for source_point, target_point in zip(new_source_points, target_points):
        y,z = source_point[1:].tolist() #x=0
        a,b,c = target_point.tolist()

        #since h32*y + z = mu*c, we have the following two equations:
        #h12*y + h13*z = mu*a => h12*yc + h13*zc = h32*ya + za => (yc, zc, 0,  0,  -ya, za)
        #h22*y + h23*z = mu*b => h22*yc + h23*zc = h32*yb + zb => (0,  0,  yc, zc, -yb, zb)
        equations.append(np.array([y*c, z*c, 0,   0,   -y*a, z*a]))
        equations.append(np.array([0,   0,   y*c, z*c, -y*b, z*b]))

    #solve the equations and obtain the last two columns of H
    equations = np.vstack(equations)
    column_coefficients = np.linalg.lstsq(equations[:,:-1], equations[:,-1])[0]
    last_columns = np.append(column_coefficients,1).reshape((3,2))

    # print 'the last to columns:'
    # print last_columns
    #the first column of H is not defined, so we take the cross product of the last two columns to guarentee that we get an invertible matrix
    first_column = np.cross(last_columns[:,0], last_columns[:,1])
    H = np.vstack([first_column, last_columns.transpose()]).transpose()

    #finally, multiply with M to get the homography from the original coordinate system
    return np.dot(H,M)

def get_image_region(image):
    '''
    return a numpy array with shape (2,2) that represents the region surrounding the image.
    '''
    image_region = np.array([[0,0], [image.shape[1], image.shape[0]]], np.float64)
    return image_region

def point_in_region(point, region):
    '''
    point - a length 2 numpy array
    region - a numpy array [[x start, y start], [x end, y end]]
    '''
    point = point.astype(np.float64)
    region = region.astype(np.float64)
    
    in_region = (region[0]-EPSILON <= point).all() and (point <= region[1]+EPSILON).all()
 
    return in_region

def points_in_region(points, region):
    '''
    points - a numpy array with shape (num points, 2)
    region - a numpy array [[x start, y start], [x end, y end]]

    return a numpy array with shape (num points,) and type bool which is true at index i iff points[i] is in the region
    '''
    points = points.astype(np.float64)
    region = region.astype(np.float64)

    in_region = (region[0]-EPSILON <= points).all(axis=1) * (points <= region[1]+EPSILON).all(axis=1)
    return in_region

def get_region_boundary_lines(region):
    '''
    region - a numpy array [[x start, y start], [x end, y end]] of type np.float32
    Return a list of Line objects corresponding to the boundaries of the region
    '''
    from .fit_points_to_shape.line import Line
    
    region = np.float64(region)
    boundary_lines = [Line.from_point_and_direction(region[0], np.array([1,0], np.float32)),
                      Line.from_point_and_direction(region[0], np.array([0,1], np.float32)),
                      Line.from_point_and_direction(region[1], np.array([1,0], np.float32)),
                      Line.from_point_and_direction(region[1], np.array([0,1], np.float32))]
    
    return boundary_lines

def line_region_intersection(line, region):
    '''
    line - a Line object
    region - a numpy array [[x start, y start], [x end, y end]] of type np.float32

    return a numpy array with shape (2,2). Each row represents one of the intersection points of the line with the rectangle.
    '''

    #first get the lines surrounding the region
    boundary_lines = get_region_boundary_lines(region)

    #intersect the give line with the boundary lines
    intersection_candidates = [affine_line_line_intersection(line, bl) for bl in boundary_lines]
    intersection_candidates = [ic for ic in intersection_candidates if ic is not None and point_in_region(ic, region)]

    if len(intersection_candidates) == 0:
        return None

    #find the unique intersection points, up to a specified precision
    intersection_candidates = np.vstack(intersection_candidates)
    intersection_points = np_operations.find_unique_rows(intersection_candidates, decimals=LINE_REGION_INTERSECTION_PREC)
    return intersection_points

def ray_region_intersection(ray, region):
    '''
    ray - a numpy array with shape (2,2). [origin, direction]
    region - a numpy array [[x start, y start], [x end, y end]] of type np.float32

    return - a numpy array with shape (n,2) where n=0,1,2.
    It represents the intersection points of the ray with the region.
    '''
    from .fit_points_to_shape.line import Line
    
    origin, direction = ray
    #get the line traced by the ray
    line = Line.from_point_and_direction(origin, direction)

    intersection_points = line_region_intersection(line, region)
    if intersection_points is None or len(intersection_points)==0:
        return np.empty((0,2))
    
    #check which of the points are on the ray
    from_origin = intersection_points - origin
    intersection_signs = np.sign(np.matmul(direction, from_origin.transpose()))
    
    return intersection_points[intersection_signs > 0]

def line_segment_region_intersection(line_segment, region):
    '''
    line_segment - a numpy array with shape (2,2). [point 1, point 2]
    region - a numpy array [[x start, y start], [x end, y end]] of type np.float32
    
    return - a numpy array with shape (2,2). It represents the line segment obtained by
       intersecting the given line segment and the region.
       if the line segment does not intersect the region, return None
    '''
    from .fit_points_to_shape.line import Line

    #intersect the line spanned by the points with the region
    origin = line_segment[0]
    direction = line_segment[1] - line_segment[0]
    segment_length = np.linalg.norm(direction)
    direction /= segment_length
    line = Line.from_point_and_direction(origin, direction)

    intersection_points = line_region_intersection(line, region)
    
    if intersection_points is None or len(intersection_points) < 2:
        return None

    #sort the intersection points by their position on the line w.r.t the given origin and direction
    positions_on_line = np.matmul(direction, (intersection_points - origin).transpose()) / segment_length
    sorted_indices = np.argsort(positions_on_line)
    positions_on_line = positions_on_line[sorted_indices]
    intersection_points = intersection_points[sorted_indices]
    
    #if both intersection points are not in the line segment then
    #the line segement misses the region
    if np.all(positions_on_line <= 0) or np.all(positions_on_line >= 1):
        return None

    #the first intersection point is not in the segment
    elif positions_on_line[0] < 0:
        #the second intersection point is in the segment
        if positions_on_line[1] < 1:
            return np.vstack([line_segment[0], intersection_points[1]])
        else:
            return line_segment

    #the first intersection point is in the segment
    else:
        #the second intersection point is in the segment
        if positions_on_line[1] < 1:
            return intersection_points
        else:
            return np.vstack([intersection_points[0], line_segment[1]])

#NOTE: This function is OBSELETE!! Instead you should use Contour.get_transformed_contour(H)
def apply_homography_to_contours(H, contours):
    '''
    H - a homography matrix
    contours - a list of Contour objects
    
    Return a list of Contour objects obtained by applying H to each of the contours
    '''
    transformed_contours = []
       
    for contour in contours:
        cv_contour = np.float32(contour.get_cv_contour())
        new_cv_contour = cv2.perspectiveTransform(cv_contour, H)
        transformed_contours.append(Contour(np.int32(new_cv_contour), contour.get_image()))
        
    return transformed_contours


def _get_ellipses_coefficients(C):
    '''
    C - a numpy array with shape (n,3,3) and type np.float32. It represents a list of ellipses.

    return the tuple (C[:,0,0], C[:,0,1], C[:,0,2], C[:,1,1], C[:,1,2], C[:,2,2]).
    '''

    return (C[:,0,0], C[:,0,1], C[:,0,2], C[:,1,1], C[:,1,2], C[:,2,2])

def _get_ellipse_coefficients(C):
    '''
    C - a numpy array with shape (3,3) and type np.float32. It represents an ellipses.

    return the tuple (C[0,0], C[0,1], C[0,2], C[1,1], C[1,2], C[2,2]).
    '''

    return (C[0,0], C[0,1], C[0,2], C[1,1], C[1,2], C[2,2])

def _lines_ellipses_intersection(L, C):
    '''
    L - a numpy array with shape (n,3) which represents a list of parametric lines
    E - a numpy array with shape (n,3,3) which represents a list of parametric ellipses

    We assume that for every i, |L[i,0]| > |L[i,1]|

    return a numpy array with shape (n,2,2) which represents the list of pairs of intersection points of the lines
    and ellipses.
    '''

    #we denote the line equations by: q*x + r*y + s and the ellipse equations by:
    #a*x^2 + 2*b*xy + d*y^2 + 2*c*x + 2*e*y + f
    #by our assumptions, |q| > |r|
    q = L[:,0]
    r = L[:,1]
    s = L[:,2]

    a,b,c,d,e,f = _get_ellipses_coefficients(C)
    #since |q| > |r|, we can express x as: x = -(r*y + s)/q
    #after the substitution r = r/q, s = s/q we get:
    #x = -(r*y + s)
    #by plugging this into the ellipse equation we get the following equation in y:
    #(a*r^2 - 2*b*r + d)*y^2 + (2*a*r*s - 2*c*r - 2*b*s + 2*e)*y + a*s^2 - 2*c*s + f =
    #A*y^2 + B*y + C
    #where r' = r/q and s' = s/q
    r /= q
    s /= q

    A = a*r**2 - 2*b*r + d
    B = 2*a*r*s - 2*c*r - 2*b*s + 2*e
    C = a*s**2 -2*c*s + f

    #now solve the quadratic equation to get the two y values
    disc = B**2 - 4*A*C
    Y0 = (-B - np.sqrt(disc)) / (2*A)
    Y1 = (-B + np.sqrt(disc)) / (2*A)
    
    #solve for x using: x = -(r*y +s)
    X0 = -(r*Y0 + s)
    X1 = -(r*Y1 + s)

    #we want to return an array with shape (n,2,2) such that the i-th index stores [[X0[i],Y0[i]],[X1[i], Y1[i]]
    intersection_points = np.vstack([X0,Y0,X1,Y1]).transpose().reshape((-1,2,2))
    
    return intersection_points

def lines_ellipses_intersection(L, C):
    '''
    L - a numpy array with shape (n,3) which represents a list of parametric lines
    E - a numpy array with shape (n,3,3) which represents a list of parametric ellipses

    return a numpy array with shape (n,2,2) which represents the list of pairs of intersection points of the lines
    and ellipses.
    '''
    intersection_points = np.zeros((len(L),2,2), np.float32)

    #denote the line equation by q*x + r*y +s
    #we treat the cases of |q|>|r| and |q|<|r| seperately
    q_bigger = np.abs(L[:,0]) >= np.abs(L[:,1])
    r_bigger = np.logical_not(q_bigger)
    
    #for the |q| > |r| case, use the _lines_ellipse_intersection function
    L_q = L[q_bigger]
    C_q = C[q_bigger]
    L_r = L[r_bigger]
    C_r = C[r_bigger]
    
    intersection_points[q_bigger] = _lines_ellipses_intersection(L_q, C_q)

    #for the |q| < |r| case, swap the x and y coordinates, proceed as in the |q|>|r| case, and swap back
    #after swapping the coordinates, the new line eq is: L1*x + L0*y + L2 = 0
    L_r_swapped = np.vstack([L_r[:,1], L_r[:,0], L_r[:,2]]).transpose()

    #and the new ellipse matrix is:
    #[C11, C01, C12]
    #[C10, C00, C02]
    #[C21, C20, C22]
    C_r_swapped = np.vstack([C_r[:,1,1], C_r[:,0,1], C_r[:,1,2],
                             C_r[:,1,0], C_r[:,0,0], C_r[:,0,2],
                             C_r[:,2,1], C_r[:,2,0], C_r[:,2,2]]).transpose().reshape((-1,3,3))

    intersection_points[r_bigger] = _lines_ellipses_intersection(L_r_swapped, C_r_swapped)

    #swap back the x and y coordinates
    intersection_points[r_bigger] = intersection_points[r_bigger][:,:,(1,0)]

    return intersection_points

def get_ellipse_regions(C):
    '''
    C - a numpy array with shape (n,3,3) and type np.float32. It represents a list of ellipses.
    Return a numpy array with shape (n,2,2) and type np.float32. It represents the axis aligned bounding boxs of the ellipses.
    '''
    a,b,c,d,e,f = _get_ellipses_coefficients(C)
    
    #Let (Jx,Jy) denote the gradient of the ellipse equation f(x,y) = (x,y,1) * C * (x,y,1)^T
    #to find the right and left sides of the bounding box, we find the two points on the ellipse for which
    #Jy = 2*(b*x + d*y + e) = 0
    #To do this, let L be the line defined by Jy=0. The left and right sides of the box are the x coordinates of the
    #intersection points of L with the ellipse C.
    L = np.vstack([b, d, e]).transpose()
    intersection_x_coords = lines_ellipses_intersection(L, C)[:,:,0]

    left = intersection_x_coords.min(axis=1)
    right = intersection_x_coords.max(axis=1)

    #similarly, to get the top and bottom coords, intersect the line:
    #Jx = 2*(a*x + b*y + c) = 0
    #with the ellipse and take the y coords
    L = np.vstack([a, b, c]).transpose()
    intersection_y_coords = lines_ellipses_intersection(L, C)[:,:,1]

    top = intersection_y_coords.min(axis=1)
    bottom = intersection_y_coords.max(axis=1)

    #return a numpy array with shape (n,2,2) where the i-th 2x2 array is
    #[left[i], top[i]]
    #[right[i], bottom[i]]
    return np.vstack([left, top, right, bottom]).transpose().reshape((-1,2,2))

def get_ellipse_region(C):
    '''
    C - a numpy array with shape (3,3) and type np.float32. It represents an ellipse.
    return - a numpy array with shape (2,2). It represents the AABB of the ellipse.
    '''

    return get_ellipse_regions(C.reshape((1,3,3))).reshape((2,2))

def apply_homography_inv_to_ellipses(H, ellipses):
    '''
    H - a numpy array with shape (3,3). It represents a homography matrix.
    ellipses - a numpy array with shape (n,3,3). It represents a list of ellipse parameterizations

    return a numpy array with shape (n,3,3). The i-th output 3x3 matrix represents the image of the i-th input ellipse
    under the homography H^-1.
    '''

    return np.matmul(H.transpose(), np.matmul(ellipses, H))

def apply_homography_inv_to_ellipse(H, ellipse):
    '''
    H - a numpy array with shape (3,3). It represents a homography matrix.
    ellipses - a numpy array with shape (3,3). It represents an ellipse parameterization

    return a numpy array with shape (3,3). It represents the image of the input ellipse
    under the homography H^-1.
    '''

    return np.matmul(H.transpose(), np.matmul(ellipse, H))

def _get_quadratic_form_and_translation(C):
    '''
    C - a numpy array with shape (3,3) representing an ellipse.
    return - a tuple (Q, B)
    
    Suppose that 
    C = [a   b/2 d/2]
        [b/2 c   e/2]
        [d/2 e   f/2]

    Then 
    Q = [a   b/2]
        [b/2 c]

    B = [d e]
    '''
    Q = C[:2,:2]
    B = 2*C[:2,2]

    return Q, B

def _compute_ellipse_center(Q, B):
    '''
    Q - a numpy array with shape (2,2)
    B - a numpy array with length 2

    return - a numpy array with length 2

    We assume that Q and B are the quadratic form and translation of an ellipse as in _get_quadratic_form_and_translation.
    We use the formula (see http://www.geometrictools.com/Documentation/InformationAboutEllipses.pdf):
    center = (Q[1][1]B[0] - Q[0][1]B[1], Q[0][0]B[1] - Q[0][1]B[0]) / 2*(Q[0][1]^2 - Q[0][0]Q[1][1])
    '''
    center = np.array([Q[1][1]*B[0] - Q[0][1]*B[1], Q[0][0]*B[1] - Q[0][1]*B[0]]) / (2*(Q[0][1]**2 - Q[0][0]*Q[1][1]))
    return center

def compute_ellipse_center(C):
    '''
    C - a numpy array with shape (3,3) representing an ellipse.

    Return the center of the ellipse.
    '''
    Q, B = _get_quadratic_form_and_translation(C)
    
    return _compute_ellipse_center(Q,B)

def compute_normalized_ellipse(C):
    '''
    C - a numpy array with shape (3,3) representing an ellipse.

    Return a tuple K, M, b.
    K is the center of the ellipse and M is a (2,2) array such that:
    x in the ellipse <=> (x-K)^T * M * (x-K) + b = 0

    We use the formula (see http://www.geometrictools.com/Documentation/InformationAboutEllipses.pdf):
    M = (1 / (Q[0][0]*(K[0]**2) + 2*Q[0][1]*K[0]*K[1] + Q[1][1]*(K[1]**2) - S)) * Q
    where K is the center of the ellipse, Q is the associated quadratic form (see _get_quadratic_form_and_translation),
    and S = C[2,2]
    '''
    Q, B = _get_quadratic_form_and_translation(C)
    K = _compute_ellipse_center(Q,B)
    S = C[2,2]

    mu = 1 / (Q[0][0]*(K[0]**2) + 2*Q[0][1]*K[0]*K[1] + Q[1][1]*(K[1]**2) - S)
    M = mu *Q

    return K,M,B

def evaluate_ellipse_on_point(C, point):
    '''
    C - a numpy array with shape (3,3)
    points - a numpy array with length 2

    return - a float.
    '''

    return evaluate_ellipses_on_points(C.reshape((1,3,3)), point.reshape((1,2)))[0]

def evaluate_ellipses_on_points(C, points):
    '''
    C - a numpy array with shape (n,3,3)
    points - a numpy array with shape (n,2)

    return - a numpy array with length n. The i-th entry is the evaluation of the equation of the i-th ellipse on the
    i-th point.
    '''
    x,y = (points[:,0], points[:,1])
    a,b,c,d,e,f = _get_ellipses_coefficients(C)
    #the ellipse equation is: a*x^2 + 2*b*x*y + d*y^2 + 2*c*x + 2*e*y + f

    return a*x*x + 2*b*x*y + d*y*y + 2*c*x + 2*e*y + f

def evaluate_ellipse_on_grid(C, XX, YY):
    '''
    C - a numpy array with shape (3,3)
    XX, YY - a numpy array with shape (a,b). These correspond to vectors (x,y) positioned on a grid.
             I.e, (XX[i,j],YY[i,j]) is a vector at the point (i,j) on the grid.

    return - a numpy array with shape (a,b). The axb array is obtained by evaluating the ellipse equation at the
             vectors at each of the points of the grid. I.e, if A is the output array, then A has shape (a,b) and
             A[i,j] = evaluate the ellipse at (XX[i,j], YY[i,j])
    '''
    a,b,c,d,e,f = _get_ellipse_coefficients(C)

    return a*XX*XX + 2*b*XX*YY + d*YY*YY + 2*c*XX + 2*e*YY + f

def evaluate_ellipses_on_grid(C, XX, YY):
    '''
    C - a numpy array with shape (n,3,3)
    XX, YY - a numpy array with shape (a,b). These correspond to vectors (x,y) positioned on a grid.
             I.e, (XX[i,j],YY[i,j]) is a vector at the point (i,j) on the grid.

    return - a numpy array with shape (n,a,b). The i-th axb array is obtained by evaluating the ellipse equation at the
             vectors at each of the points of the grid. I.e, if A is the i-th array, then A has shape (a,b) and
             A[i,j] = evaluate the ellipse at (XX[i,j], YY[i,j])
    '''
    coeff_shape = (C.shape[0],1,1)
    a,b,c,d,e,f = [coeff.reshape(coeff_shape) for coeff in _get_ellipses_coefficients(C)]

    return a*XX*XX + 2*b*XX*YY + d*YY*YY + 2*c*XX + 2*e*YY + f

def build_mask_from_ellipse_and_region(ellipse, ellipse_region):
    '''
    ellipse - a numpy array with shape (3,3) representing an ellipse
    ellipse_region - a numpy array with shape (2,2) representing the bounding box of the ellipse: 
    [top left corner, bottom right corner]

    return a numpy array with the same shape as the ellipse region and type np.bool
    '''

    ellipse_center = ellipse_region.mean(axis=0)

    #determine if the value of the ellipse equation on the center is positive or negative.
    #if it is negative, we multiply the ellipse equation by -1. This ensures that the inside of the ellipse is the region
    #where the equation is positive.
    value_at_center = evaluate_ellipse_on_point(ellipse, ellipse_center)
    if value_at_center < 0:
        ellipse *= -1.0

    #to build the player mask, evalute the player_ellipse at each of the pixels in the ellipse_region
    #and check which ones are positive
    ellipse_region = np.int32(ellipse_region)
    x = np.arange(ellipse_region[0,0],ellipse_region[1,0]+1, 1)
    y = np.arange(ellipse_region[0,1],ellipse_region[1,1]+1, 1)
    XX,YY = np.meshgrid(x,y)

    #print '   XX shape: ', XX.shape
    #print '   YY shape: ', YY.shape
            
    ellipse_values = evaluate_ellipse_on_grid(ellipse, XX, YY)
    ellipse_mask = ellipse_values >= 0

    return ellipse_mask

def build_ellipse_masks(ellipses):
    '''
    ellipses - a numpy array with shape (num ellipses, 3, 3)

    return a tuple (rectangles, masks)
    rectangles - a list of Rectangle objects
    masks - a list of numpy arrays with type np.bool and such that the i-th array has the same shape as the i-th rectangle
    '''
    from .rectangle import Rectangle
    
    #compute the bounding boxs of each of the ellipses
    #this is a np array with shape (n,2,2). each (2,2) matrix stores a rectangle as: [[left,top],[right,bottom]]
    ellipse_regions = get_ellipse_regions(ellipses)
    
    #compute masks for each of the ellipses
    ellipse_masks = [build_mask_from_ellipse_and_region(ellipse, ellipse_region)
                    for ellipse,ellipse_region in zip(ellipses, ellipse_regions)]
    
    #and build the rectangles
    ellipse_rectangles = [Rectangle.from_top_left_and_shape(region[0].astype(np.int32), pm.shape)
                          for region,pm in zip(ellipse_regions, ellipse_masks)]

    return ellipse_rectangles, ellipse_masks

def build_ellipse_mask(ellipse):
    '''
    ellipse - a numpy array with shape (3,3) representing an ellipse.

    return a tuple (rectangle, mask)
    rectangle - a Rectangle object. It represents the AABB of the ellipse.
    mask - a numpy array with the same shape as the rectangle and type bool. It represents the part of the rectangle
           that is inside the ellipse.
    '''

    rectangles, masks = build_ellipse_masks(ellipse.reshape((1,3,3)))

    rectangle = rectangles[0]
    mask = masks[0]

    return rectangle, mask

def apply_homography_to_cv_contour(H, source_cv_contour):
    '''
    H - a homography matrix TO the image
    source_cv_contour - a numpy array with shape (n,1,2) and type np.int32
    
    return a numpy array with shape (n,1,2) and type np.int32 which represents an opencv contour that is
           the transformation of the given contour by H
    '''

    #get the opencv contour corresponding to the rectangle
    target_cv_contour = apply_homography_to_points(H, source_cv_contour.reshape((-1,2))).reshape((-1,1,2)).astype(np.int32)

    return target_cv_contour

def interpolate_on_circle(start, end, weights):
    '''
    start - a numpy array with length 2 and norm 1
    end - a numpy array with length 2 and norm 1
    start_weights - a list of N floats between 0 and 1

    return a numpy array with shape (N,2). The i-th entry represents a point on the circle between start and end,
    whose angle is the weighted average: w*(start angle) + (1-w)*end_angle where w=weights[i]
    '''

    #compute the angles that each of the points make with the x axis
    start_angle = np.arctan2(start[1], start[0])
    end_angle = np.arctan2(end[1], end[0])

    #we want the difference of the angles to be less than or equal to pi. This is because we want to interpolate
    #along the shortest arc between the two points
    if np.abs(end_angle - start_angle) > np.pi:
        #if the difference is too large, then we can fix it by adding 2pi to the smaller angle
        if end_angle < start_angle:
            end_angle += 2*np.pi
        else:
            start_angle += 2*np.pi

    # print 'start angle: ', start_angle
    # print 'end angle:   ', end_angle
    
    interpolated_angles = (start_angle * weights) + (end_angle * (1 - weights))

    # print 'interpolated angles: ', interpolated_angles
    
    interpolated_points = np.vstack([np.cos(interpolated_angles), np.sin(interpolated_angles)]).transpose()

    return interpolated_points

def compute_homography_to_unit_square(height, width):
    '''
    height, width - floats
    return - a homography matrix
    
    the output homography maps the rectangle:
    (0,0) -----   (width, 0)
      |             |
      |             |
      |             |
    (height,0) -- (width, height)

    to the square with edge length 2 and center (0,0).
    It is called the "unit square" because it has "radius" 1.
    '''
    bottom_right = np.array([width, height], np.float32)
    
    #first get a matrix that translates the rectangle center to the origin
    center = bottom_right / 2.0
    T = translation_matrix(-center)
    
    #then scale by the inverse of the shape
    scale = 2.0 / bottom_right
    S = scaling_matrix(scale)
    
    return np.dot(S,T)

def compute_anchor_sign_from_target_anchor(H, tgt_anchor):
    '''
    H - a np array with shape (3,3) representing a homography
    tgt_anchor - a np array with shape (2,) that represents a point in the target image of H

    return - an integer. -1, 0 or 1. It represents the side of the line at infty in the source image that gets mapped
       to the part of the target image containing the target anchor
    '''
    H_inv = np.linalg.inv(H)
    src_anchor = apply_homography_to_point(H_inv, tgt_anchor)
    
    line_at_infty = H[2]
    projective_src_anchor = np.append(src_anchor, 1)
    src_anchor_sign = np.sign(np.matmul(line_at_infty, projective_src_anchor))

    return src_anchor_sign

def compute_anchor_sign_from_target_shape(H, target_shape):
    '''    
    H - a np array with shape (3,3) representing a homography
    target_shape - a tuple (width, height)

    return the anchor sign corresponding to the target anchor which lies in the middle of the target image
    '''
    tgt_anchor = np.array([target_shape[1], target_shape[0]]) / 2.0
    return compute_anchor_sign_from_target_anchor(H, tgt_anchor)
    
    
def affine_to_projective_points(affine_points):
    '''
    affine_points - a numpy array with shape (n,2) and type np.float32
    return - a numpy array with shape (n,3) and type np.float32. Each row in the output is obtained by the
       corresponding input row by appending a 1
    '''
    return np.pad(affine_points, ((0,0),(0,1)), mode='constant', constant_values=1)
