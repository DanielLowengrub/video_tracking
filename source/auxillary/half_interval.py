import numpy as np
from .interval import Interval

class HalfInterval(object):
    '''
    This class represents a half open interval [a,infty) or (-infty,a]
    '''

    def __init__(self, a, direction):
        '''
        a - a float
        direction - a float. 1 or -1
        
        construct the interval [a,infty) if direction==1, and (-infty,a) if direction==-1
        '''

        self._a = a
        self._direction = direction

    def __str__(self):
        if self._direction > 0:
            return '[%s, oo)' % self._a
        else:
            return '(-oo, %s]' % self._a

    def __repr__(self):
        return str(self)
    
    def __hash__(self):
        return hash((self._a, self._direction)
    
    def contains_point(self, point):
        return self._direction * np.sign(point - self._a)) >= 0
    
    def get_copy(self):
        return HalfInterval(self._a, self._direction)
        
    def end_point(self):
        return self._a

    def direction(self):
        return self._direction

    def _has_overlap_with_interval(self, interval):
        '''
        interval - an Interval
        return - True iff this half interval overlaps with the interval
        '''
        #if we are intersecting [self._a, infty) with [interval._a, interval._b)
        if self._direction == 1:
            return self._a < interval.last_element()

        #if we are intersecting (-infty, self._a] with [interval._a, interval._b)
        else:
            return interval.first_element() < self._a

    def has_overlap(self, other):
        '''
        other - an Interval
        Return True if the intervals overlap
        '''
        if type(other) is Interval:
            return self._has_overlap_with_interval(other)
        
        if self._direction == other._direction:
            return True

        #we are intersecting  (-infty,other._a] with [self._a,infty)
        if self._direction > other._direction:
            return self._a < other._a

        #we are intersecting  (-infty,self._a] with [other._a,infty)
        else:
            return other._a < self._a

    def _can_merge_with_interval(self, interval):
        '''
        interval - an Interval object
        return - True iff we can merge this half interval with the interval
        '''
        #if we are merging [self._a, infty) with [interval._a, interval._b)
        if self._direction == 1:
            return self._a <= interval.last_element()

        #if we are merging (-infty, self._a] with [interval._a, interval._b)
        else:
            return interval.first_element() <= self._a

    def can_merge(self, other):
        '''
        Return True if we can merge this half interval with the other one.
        '''
        if type(other) is Interval:
            return self._can_merge_with_interval(other)
        
        return self._direction == other._direction

    
    def _contains_interval(self, interval):
        '''
        interval - an Interval
        return - True iff this half interval contains the interval
        '''
        #if we are comparing [self._a, infty) to [interval._a, interval._b)
        if self._direction == 1:
            return self._a <= interval.last_element()

        #if we are comparing (-infty, self._a] to [interval._a, interval._b)
        else:
            return interval.last_element() <= self._a

    def contains_interval(self, other):
        '''
        return True iff this half interval contains the other one.
        '''
        if type(other) is Interval:
            return self._contains_interval(other)

        return (self._a <= other._a) and (other._b <= self._b)

    def _get_overlap_with_interval(self, other):
        '''
        return - the interval obtained by intersecting this half interval with the interval
        We assume that the user has checked that there is indeed an intersection using has_overlap_with_interval(interval)
        '''
        #if we are computing the overlap of [self._a, infty) and [interval._a, interval._b)
        if self._direction == 1:
            a = max(self._a, interval.first_element())
            b = interval.last_element()
            
        #if we are computing the overlap of (-infty, self._a] and [interval._a, interval._b)
        else:
            a = interval.first_element()
            b = min(self._a, interval.last_element())

        return Interval(a,b)

    def get_overlap(self, other):
        '''
        return - the HalfInterval obtained by intersecting this interval with the other one.
        We assume that the user has checked that there is indeed an intersection using has_overlap(other)
        '''
        if type(other) is Interval:
            return self._get_overlap_with_interval(other)
            
        #if we are computing the overlap of [self._a, infty) and [other._a, infty)
        if self._direction == 1 and other._direction == 1:
            a = max(self._a, other._a)
            return HalfInterval(a, self._direction)
        
        #if we are computing the overlap of [self._a, infty) and (-infty, other._a]
        elif self._direction == 1 and other._direction == -1:
            return Interval(self._a, other._a)

        #if we are computing the overlap of [other._a, infty) and (-infty, self._a]
        elif self._direction == 1 and other._direction == -1:
            return Interval(other._a, self._a)

        #if we are computing the overlap of (-infty, self._a] and (-infty, other._a]
        else:
            a = min(self._a, other._a)
            return HalfInterval(a, self._direction)

    def _merge_with_interval(self, interval):
        '''
        return - the HalfInterval obtained by merging this ray with the interval
        If there is no overlap between the ray and interval, an exception is raised
        '''
        if not self.can_merge_with_interval(interval):
            raise ValueError('We can only merge ray and intervals that have an overlap')
        
        #if we are merging [self._a, infty) and [interval._a, interval._b)
        if self._direction == 1:
            a = min(self._a, interval.first_element())
            
        #if we are merging (-infty, self._a] and [interval._a, interval._b)
        else:
            a = max(self._a, interval.last_element())

        return HalfInterval(a,self._direction)

    def merge(self, other):
        '''
        Return a new HalfInterval which is obtained by merging this one with the other one.
        If there is no overlap between the intervals, an exception is raised
        '''
        if type(other) is Interval:
            return self._merge_with_interval(other)
        
        if not self.can_merge(other):
            raise ValueError('We can only merge half intervals that have an overlap')

        #if we are merging [self._a, infty) and [other._a, infty)
        if self._direction == 1:
            a = min(self._a, other._a)

        #if we are merging (-infty, self._a] and (-infty, other._a]
        else:
            a = max(self._a, other._a)

        return HalfInterval(a, self._direction)

    def interval_dilation(self, dilation_radius):
        '''
        dilation_radius - a float
        return a new HalfInterval that has been dilated by the amount: dilation_radius
        '''
        return HalfInterval(self._a - dilation_radius*self._direction, self._direction)

    def interval_erosion(self, erosion_radius):
        '''
        erosion_radius - a float
        return a new HalfInterval that has been eroded by the amount: dilation_radius
        '''
        return HalfInterval(self._a + dilation_radius*self._direction, self._direction)

    def length(self):
        raise RuntimeError('a half interval does not have a length')

    def get_slice(self, array):
        if self._direction == 1:
            return array[self._a:]

        else:
            return array[:self._a]

    def get_tuple(self):
        raise NotImplementedError('get_tuple has not been implemented for a half interval')

    def _get_relative_slice_with_interval(self, array, child_interval):
        a = child_interval.first_element() - self._a
        b = child_interval.last_element() - self._a
        return array[a:b]
        
    def get_relative_slice(self, array, child_interval):
        if type(child_interval) is Interval:
            return self._get_relative_slice_with_interval(array, child_interval)

        if self._direction == 1 and other._direction == 1:
            a = other._a - self._a
            return array[a:]

        elif self._direction == -1 and other._direction == -1:
            a = self._a - other._a
            return array[:-a]

        else:
            raise RuntimeError('the child interval is not contained in this interval')
        

    def relative_to_absolute_coords(self, i):
        raise NotImplementedError('relative_to_absolute_coordinates has not been implemented for half open intervals')

    def relative_to_absolute_interval(self, interval):
        raise NotImplementedError('relative_to_absolute_interval has not been implemented for half open intervals')

    def first_element(self):
        raise NotImplementedError('first_element has not been implemented for half open intervals')

    def last_element(self):
        raise NotImplementedError('last_element has not been implemented for half open intervals')
