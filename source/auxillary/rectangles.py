import numpy as np
import cv2
from .rectangle import Rectangle

class Rectangles(object):
    '''
    This stores a list of rectangles that all have the same shape.
    The point is that this can be implemented more efficiently that a list of Rectangle objects.
    '''

    TL_X = 0
    TL_Y = 1
    BR_X = 2
    BR_Y = 3

    EMPTY_RECTANGLE = np.zeros(4, dtype=np.int32)
    
    def __init__(self, shape, top_left_corners):
        '''
        shape - a tuple (height, width).
        top_left_corners - a numpy array with length (n,2) and type np.int32
        '''

        self._shape = shape

        bottom_right_corners = top_left_corners + np.array([shape[1],shape[0]])

        #each row of the rectangles array stores a length 4 array: [top left x, top left y, bottom right x, bottom right y]
        self._rectangles = np.hstack([top_left_corners, bottom_right_corners])

    @classmethod
    def from_array(cls, array):
        '''
        array - a numpy array with shape (a,b)
        return a Rectangles object with shape (a,b) and only one rectangle: [0,0,a,b]
        '''

        return cls(array.shape, np.zeros((1,2), dtype=np.int32))

    @classmethod
    def from_rectangle(cls, rectangle):
        '''
        rectangle - a Rectangle object
        
        return a Rectangles object with a single rectangle which is the given rectangle.
        '''

        return cls(rectangle.get_shape(), rectangle.get_top_left().reshape((1,2)))

    @classmethod
    def from_centers_and_rectangle(cls, centers, rectangle):
        '''
        centers - a numpy array with shape (num centers, 2) and type np.int32
        rectangle - a Rectangle object

        return - a Rectangles object with (num centers) rectangles that have the same shape as the given rectangles.
        The i-th rectangle has center centers[i]. Note that this only makes sense if both the height and width of the
        rectangle are odd, since otherwise the center is not defined.
        If the height or width are even, a ValueError is raised.
        '''
        
        height, width = rectangle.get_shape()

        if (height % 2 == 0) or (width % 2 == 0):
            raise ValueError('Both the height and width of the rectangle must be odd')

        #this is the vector from the center of the rectangle to the top left corner
        #print 'height: %s, width: %s' % (height, width)
        center_to_top_left = -(np.array([width-1, height-1]) / 2)
        #print 'center_to_top_left: ', center_to_top_left

        top_left_corners = centers + center_to_top_left

        return cls(rectangle.get_shape(), top_left_corners)
    
    def _top_left_coords_of_rectangles(self, rectangles):
        return rectangles[:,:self.BR_X]

    def _bottom_right_coords_of_rectangles(self, rectangles):
        return rectangles[:,self.BR_X:]
    
    def _top_left_coords(self):
        return self._top_left_coords_of_rectangles(self._rectangles)

    def _bottom_right_coords(self):
        return self._bottom_right_coords_of_rectangles(self._rectangles)

    def get_shape(self):
        return self._shape

    def get_number_of_rectangles(self):
        return len(self._rectangles)
    
    def select_rectangles(self, indices):
        '''
        indices - either:
                  * a numpy array with length self.number_of_rectangles and type np.bool OR
                  * a numpy array with shape (n,) and type np.int32

        return - either:
                  * a Rectangles object whith contains only the rectangles _rectangles[i] for which indices[i]=True
                  * a Rectangles object with n rectangles. The i-th rectangle is equal to the indices[i]-th rectangle of this
                    object
        '''
        return Rectangles(self._shape, self._top_left_coords()[indices])

    def get_slices(self, array):
        '''
        array - a numpy array with at least 2 dimensions. It has shape (a,b,c0,c1,..).
                We assume that all of the rectangles are contained in the rectangle [(0,0),(a,b)].

        return a numpy array with shape (n,)+self._shape+(c0,c1,..). The i-th array in the output is the slice of the
        input array by the i-th rectangle.
        '''

        return np.stack([array[rect[self.TL_Y]:rect[self.BR_Y], rect[self.TL_X]:rect[self.BR_X]]
                         for rect in self._rectangles])

    def contained_in(self, other):
        '''
        other - a Rectangles object
        The tuple (len(self._rectangles), len(other._rectangles)) must be
        equal to one of the following: (1,N), (N,1), (N,N)

        return True iff:
        (1,N): self._rectangles[0] is contained in all of other._rectangles[i]
        (N,1): all of self._rectangles[i] are contained in other._rectangles[0]
        (N,N): for all i, self._rectangles[i] is contained in other._rectangles[i]
        '''

        #check if the top left coords in _rectangles are greater or equal that those of other._rectangles
        top_left_check = np.all(self._top_left_coords() >= other._top_left_coords(), axis=1)

        #check if all of the bottom right coords in _rectangles are smaller or eq than those of other._rectangles
        bottom_right_check = np.all(self._bottom_right_coords() <= other._bottom_right_coords(), axis=1)

        return top_left_check * bottom_right_check

    def _get_intersections(self, other):
        '''
        rectangles - a Rectangles object. 
        The tuple (len(self._rectangles), len(other._rectangles)) must be
        equal to one of the following: (1,N), (N,1), (N,N)

        return a numpy array with shape (N, 4) defined as follows:
        (1,N): the i-th row of the output records the intersection of self._rectangles[i] with other._rectangles[0]
        (N,1): the i-th row of the output records the intersection of self._rectangles[0] with other._rectangles[i]
        (N,N): the i-th row of the output records the intersection of self._rectangles[i] with other._rectangles[i]

        If an intersection is empty, the corresponding row of the output is nan.
        '''
        intersections_top_left = np.maximum(self._top_left_coords(), other._top_left_coords())
        intersections_bottom_right = np.minimum(self._bottom_right_coords(), other._bottom_right_coords())

        intersections = np.hstack([intersections_top_left, intersections_bottom_right])

        #find the empty intersections and set them to nan
        empty_mask = (intersections_top_left >= intersections_bottom_right).any(axis=1)
        intersections[empty_mask] = self.EMPTY_RECTANGLE
        
        return intersections

    def get_intersection_areas(self, other):
        '''
        other - a Rectangles object

        The tuple (len(self._rectangles), len(other._rectangles)) must be
        equal to one of the following: (1,N), (N,1), (N,N)

        return a numpy array with shape (N) defined as follows:
        (1,N): the i-th row of the output records the area of the intersection of self._rectangles[i] with other._rectangles[0]
        (N,1): the i-th row of the output records the area of the intersection of self._rectangles[0] with other._rectangles[i]
        (N,N): the i-th row of the output records the area of the intersection of self._rectangles[i] with other._rectangles[i]
        '''

        #first build the intersection rectangles.
        intersection_rectangles = self._get_intersections(other)
        intersections_top_left = self._top_left_coords_of_rectangles(intersection_rectangles)
        intersections_bottom_right = self._bottom_right_coords_of_rectangles(intersection_rectangles)
        
        intersections_shape = intersections_bottom_right - intersections_top_left
        intersections_area = intersections_shape[:,0] * intersections_shape[:,1]

        return intersections_area

    def transfer_slices(self, target_array, source_rectangles, source_array):
        '''
        target_array - an array with shape (k,a,b,c0,c1,...)
                       and (a,b) is the shape of this rectangle
        source_rectangles - a Rectangles object with l rectangles and shape (a',b')
        source_array - a numpy array with shape (m, a',b',c0,c1,...)

        The tuple (len(_rectangles), k, l, m) must be
        equal to one of the following: (1,N,1,1), (1,N,1,N), (1,N,N,1), (1,N,N,N), (N,N,1,1), (N,N,1,N), (N,N,N,1), (N,N,N,N)
    
        Said differently, suppose len(target_array) = N.
        Then:
        * len(_rectangles) and len(source_rectangles) can be either 1 or N
        * len(source_array) can be 1 or N

        The function does the following: It first generates N intersections between the rectangle(s) in this object and the
        rectangle(s) in source_rectangles. If len(_rectangles) = len(source_rectangles) = 1 then we generate N copies of the
        intersection between these two rectangles.

        Let I be the i-th intersection. 
        It then takes the slice of source_array[i] (or source_array[0] if there is only one) corresponding to I, 
        and puts it in the slice of target_array[i] correponding to I.

        To be explicit, the behavior of this method depends on the above tuple in the following way:
        (1,N,1,1): for each i, let I be the intersection of self._rectangles[0] and source_rectangles._rectangles[0].
        We take the slice of source_array[0] corresponding to I and put it in the slice of target_array[i] corresponding to I.

        (1,N,1,N): for each i, let I be the intersection of self._rectangles[0] and source_rectangles._rectangles[0].
        We take the slice of source_array[i] corresponding to I and put it in the slice of target_array[i] corresponding to I.

        (1,N,N,1): for each i, let I be the intersection of self._rectangles[0] and source_rectangles._rectangles[i].
        We take the slice of source_array[i] corresponding to I and put it in the slice of target_array[0] corresponding to I.

        (1,N,N,N): for each i, let I be the intersection of self._rectangles[i] and source_rectangles._rectangles[i].
        We take the slice of source_array[i] corresponding to I and put it in the slice of target_array[0] corresponding to I.

        (N,N,1,1): for each i, let I be the intersection of self._rectangles[i] and source_rectangles._rectangles[0].
        We take the slice of source_array[0] corresponding to I and put it in the slice of target_array[i] corresponding to I.

        (N,N,1,N): for each i, let I be the intersection of self._rectangles[i] and source_rectangles._rectangles[0].
        We take the slice of source_array[i] corresponding to I and put it in the slice of target_array[i] corresponding to I.
        
        (N,N,N,1): for each i, let I be the intersection of self._rectangles[i] and source_rectangles._rectangles[i].
        We take the slice of source_array[0] corresponding to I and put it in the slice of target_array[i] corresponding to I.
        
        (N,N,N,N): for each i, let I be the intersection of self._rectangles[i] and source_rectangles._rectangles[i].
        We take the slice of source_array[i] corresponding to I and put it in the slice of target_array[i] corresponding to I.
        '''
        #first get an array containing the intersections of the _rectangles with rectangle
        intersection_rectangles = self._get_intersections(source_rectangles)

        #get the intersection rectangles in the coordinate system of the source rectangles
        source_rectangles = intersection_rectangles - np.hstack([source_rectangles._top_left_coords()]*2)

        #get the intersection rectangles in the coordinate system of _rectangles
        target_rectangles = intersection_rectangles - np.hstack([self._top_left_coords()]*2)

        for i,ta in enumerate(target_array):

            #there is only one intersection, use that one to transfer from the source array to target array
            if len(intersection_rectangles) == 1:
                intersection_rectangle = intersection_rectangles[0]
                source_rectangle = source_rectangles[0]
                target_rectangle = target_rectangles[0]

            #otherwise, there must be len(target_array) of them so take the i-th one
            else:
                intersection_rectangle = intersection_rectangles[i]
                source_rectangle = source_rectangles[i]
                target_rectangle = target_rectangles[i]
                
            #if the intersection is empty, continue
            if (intersection_rectangle == self.EMPTY_RECTANGLE).all():
                continue

            #if there is only one source array, transfer from that one to the target array
            if len(source_array) == 1:
                sa = source_array[0]
            #otherwise, there must be len(target_array) source arrays so take the i-th one
            else:
                sa = source_array[i]

            #take the part of the source array that is in the intersection
            source_slice = sa[source_rectangle[self.TL_Y]:source_rectangle[self.BR_Y],
                              source_rectangle[self.TL_X]:source_rectangle[self.BR_X]]


            #get the part of the target array that is in the intersection
            target_slice = ta[target_rectangle[self.TL_Y]:target_rectangle[self.BR_Y],
                              target_rectangle[self.TL_X]:target_rectangle[self.BR_X]]

            # print 'source slice shape: ', source_slice.shape
            # cv2.imshow('source slice', np.float32(source_slice))
            # cv2.waitKey(0)
            # cv2.destroyAllWindows()

            # print 'target slice shape: ', target_slice.shape
            # cv2.imshow('target slice', np.float32(target_slice))
            # cv2.waitKey(0)
            # cv2.destroyAllWindows()

            #put the source slice into the target slice
            target_slice[...] = source_slice

        return

    def get_rectangle_objects(self):
        '''
        return - a list of Rectangle objects that represent the rectangles in this object
        '''

        return [Rectangle(rect[:self.BR_X], rect[self.BR_X:]) for rect in self._rectangles]

    
