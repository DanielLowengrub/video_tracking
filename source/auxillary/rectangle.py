import numpy as np
import cv2

class Rectangle(object):
    '''
    This is a simple class that keeps track of a rectangular region on an image.
    The main methods it provides are:
    * compute the intersection of two rectangles
    * compute the union of two rectangles
    * use a rectangle to cut out a piece of an image
    * convert a rectangle to the coordinate system of a different rectangle
    '''

    def __init__(self, top_left, bottom_right):
        '''
        top_left - a numpy array of length 2 and type np.int32
        bottom_right - a numpy array of length 2 and type np.int32
        '''

        self._top_left = top_left
        self._bottom_right = bottom_right

    def __repr__(self):
        return 'Rectangle. top left: [%d,%d], bottom right: [%d,%d]' % tuple(self._top_left.tolist() +
                                                                             self._bottom_right.tolist())

    @classmethod
    def from_array(cls, array):
        '''
        return a Rectangle whose top left corner is (0,0) and whose bottom right corner is (array.shape[1],array,shape[0])
        '''
        top_left = np.array([0,0])
        bottom_right = np.array([array.shape[1], array.shape[0]])
        return cls(top_left, bottom_right)

    @classmethod
    def from_top_left_and_shape(cls, top_left, shape):
        '''
        top_left - a numpy array with shape (2,)
        shape - a tuple (height, width)
        
        return - a Rectangle object with the specified shape whose top left corner is top_left
        '''
        bottom_right = top_left + np.array([shape[1], shape[0]])
        return Rectangle(top_left, bottom_right)
    
    def get_top_left(self):
        return self._top_left

    def get_bottom_right(self):
        return self._bottom_right
    
    def get_shape(self):
        '''
        Return a tuple (height, width)
        '''
        return (self._bottom_right[1] - self._top_left[1], self._bottom_right[0] - self._top_left[0])

    def get_homography_to_rectangle_coords(self):
        '''
        Return a numpy array with shape (3,3) and type np.float32.
        It represents the homography matrix from global image coordinates to rectangle coordinates.
        For example, it maps the top left corner of the rectangle to (0,0).

        In practice, this is simply a translation by -(_top_left)
        '''
        from ..auxillary import planar_geometry
        
        top_left_to_origin = -self._top_left
        H = planar_geometry.translation_matrix(top_left_to_origin)

        return H

    def get_homography_from_rectangle_coords(self):
        '''
        Return a numpy array with shape (3,3) and type np.float32.
        It represents the homography matrix from rectangle coordinates to global image coordinates.
        For example, it maps the point (0,0) to the top left corner of the rectangle.

        In practice, this is simply a translation by (_top_left)
        '''
        from ..auxillary import planar_geometry
        
        origin_to_top_left = self._top_left
        H = planar_geometry.translation_matrix(origin_to_top_left)

        return H

    def convert_point_to_rectangle_coords(self, point):
        '''
        point - a numpy array with shape (2,)
        return - a numpy array with shape (2,) which represents the point in rectangle coordinates.
        '''

        return point - self._top_left
        
    def contains_points(self, points, padding=0):
        '''
        points - a numpy array with shape (n,2)
        padding - an integer

        return a numpy array with length n and type np.bool. the i-th entry is True iff 
        the distance from the i-th point to the interior of the rectangle is lesseq to padding
        '''
        
        return (self._top_left - padding <= points).all(axis=1) * (points < self._bottom_right + padding).all(axis=1)

    def contains_point(self, point, padding=0):
        '''
        point - a numpy array with shape (2,)
        padding - an integer

        return True iff the point is within padding of the rectangle
        '''
        return self.contains_points(point.reshape((1,2)), padding)[0]
    
    def intersection(self, other):
        '''
        We return a Rectangle object corresponding to the intersection of this rectangle and the given one.
        If the intersection is empty, return None.
        '''

        left_x_coord = max(self._top_left[0], other._top_left[0])
        right_x_coord = min(self._bottom_right[0], other._bottom_right[0])

        top_y_coord = max(self._top_left[1], other._top_left[1])
        bottom_y_coord = min(self._bottom_right[1], other._bottom_right[1])

        top_left_intersection = np.array([left_x_coord, top_y_coord])
        bottom_right_intersection = np.array([right_x_coord, bottom_y_coord])

        #if the intersection is empty, return None
        if (top_left_intersection >= bottom_right_intersection).any():
            return None
        
        return Rectangle(top_left_intersection, bottom_right_intersection)

    def union(self, other):
        '''
        We return a Rectangle object corresponding to the union of this rectangle and the given one.
        More presicely, we return the smalles rectangle that contains this one and the other one.
        '''

        left_x_coord = min(self._top_left[0], other._top_left[0])
        right_x_coord = max(self._bottom_right[0], other._bottom_right[0])

        top_y_coord = min(self._top_left[1], other._top_left[1])
        bottom_y_coord = max(self._bottom_right[1], other._bottom_right[1])

        top_left_union = np.array([left_x_coord, top_y_coord])
        bottom_right_union = np.array([right_x_coord, bottom_y_coord])

        return Rectangle(top_left_union, bottom_right_union)

    def contained_in(self, other):
        '''
        check if this rectangle is contained in the other rectangle.
        '''

        return (self._top_left >= other._top_left).all() and (self._bottom_right <= other._bottom_right).all()
    
    def _convert_to_relative_coordinates(self, child_rectangle):
        '''
        child_rectangle - a Rectangle object
        
        We convert the child rectangle to the coordinate system of this rectangle.
        We assume that the child is contained in this rectangle.
        '''

        if not child_rectangle.contained_in(self):
            raise ValueError('The so called child rectangle is not actually contained in this rectangle.')

        top_left = child_rectangle._top_left - self._top_left
        bottom_right = child_rectangle._bottom_right - self._top_left

        return Rectangle(top_left, bottom_right)

    def get_slice(self, array):
        '''
        array - a numpy array that has at least 2 dimensions
        return a slice of the array corresponding to this rectangle.
        '''
        return array[self._top_left[1] : self._bottom_right[1], self._top_left[0] : self._bottom_right[0]]

    def get_slices(self, array):
        '''
        array - a numpy array that has at least 3 dimensions. It represents a list of arrays with at least two dimensions.
        return an array which represents a list of the slices of each of the input arrays by this rectangle.
        '''

        return array[:,self._top_left[1] : self._bottom_right[1], self._top_left[0] : self._bottom_right[0]]

    def get_relative_slice(self, array, child_rectangle):
        '''
        array - a numpy array whose shape is equal to the shape of this rectangle
        child_rectangle - a rectangle object contained in this rectangle.

        return - a slice of the array corresponding to the region in this rectangle cut out by the child rectangle.

        example:
        array = [0 1]
                [0 2]
        this rectangle: top_left = (2,2), bottom_right = (4,4)
        child_rectangle: top_left = (3,2), bottom_right = (4,4)
        return:
        [1]
        [2]
        '''

        #first convert the child rectangle to the coordinate system of this one
        relative_child_rectangle = self._convert_to_relative_coordinates(child_rectangle)

        return relative_child_rectangle.get_slice(array)

    def get_relative_slices(self, array, child_rectangle):
        '''
        array - a numpy array whose shape is (n,a,b) where (a,b) is the shape of the rectangle
        child_rectangle - a rectangle object contained in this rectangle.

        return - an array with shape (n,a',b') where (a',b') is the shape of the child array. This is a view of the
        given array, obtained by taking a slice of each of the arrays array[i,:] cut out by the child rectangle.
        '''

        #first convert the child rectangle to the coordinate system of this one
        relative_child_rectangle = self._convert_to_relative_coordinates(child_rectangle)

        return relative_child_rectangle.get_slices(array)

    def insert_array(self, array, child_array):
        '''
        array, child_array - numpy arrays.
        
        We assume that child_array has the same shape as the rectangle, and that this rectangle is contained in the array.
        We insert the child array into the array, in the location determined by the rectangle.
        '''

        self.get_slice(array)[:,:] = child_array

    def insert_arrays(self, array, child_array):
        '''
        array, child_array - numpy arrays with at least 3 dimensions.
        
        Suppose the rectangle has shape (a,b) and the child has shape (a',b')
        We assume that array has shape (n,a,b,...) and child_array has shape (n,a',b',...), 
        and that this rectangle is contained in the array.
        For each 0<=i<n, we insert child_array[i] into array[i], in the location determined by the rectangle.
        '''
        self.get_slices(array)[...] = child_array

    def insert_relative_array(self, array, child_array, child_rectangle):
        '''
        array - a numpy array with the same shape as this rectangle
        child_array - a numpy array with the same shape as child_rectangle
        child_rectangle - a Rectangle contained in this rectangle.

        We insert the child array into the array, at the location determined by the child rectangle.

        example:
        array = [0 0]
                [0 0]
        child_array = [1]
                      [2] 
        this rectangle: top_left = (2,2), bottom_right = (4,4)
        child_rectangle: top_left = (3,2), bottom_right = (4,4)
        return:
        [0 1]
        [0 2]
        '''

        #first convert the child rectangle to the coordinate system of this one
        relative_child_rectangle = self._convert_to_relative_coordinates(child_rectangle)

        #then insert the child array into the relative child rectangle
        relative_child_rectangle.insert_array(array, child_array)

    def insert_relative_arrays(self, array, child_array, child_rectangle):
        '''
        Let (a,b) be the shape of this rectangle.

        array - a numpy array with shape (n,a,b,...)
        child_array - a numpy array with the shape (n,a',b',...)
        child_rectangle - a Rectangle contained in this rectangle with shape (a',b')

        For each 0<=i<n, insert child_array[i] into array[i], at the location determined by the child rectangle.
        '''

        #first convert the child rectangle to the coordinate system of this one
        relative_child_rectangle = self._convert_to_relative_coordinates(child_rectangle)

        #then insert the child array into the relative child rectangle
        relative_child_rectangle.insert_arrays(array, child_array)

    def transfer_slice(self, target_array, source_rectangle, source_array):
        '''
        target_array - a numpy array with the same shape as this rectangle
        source_rectangle - a Rectangle
        source_array - a numpy array with the same shape as the source rectangle

        Let I be the intersection of this rectangle and the source. This method takes the slice of source_array corresponding
        to I and puts it in the slice of the target array corresponding to I
        '''

        intersection = self.intersection(source_rectangle)

        #if the intersection is empty, there is nothing to do
        if intersection is None:
            return

        source_slice = source_rectangle.get_relative_slice(source_array, intersection)
        self.insert_relative_array(target_array, source_slice, intersection)

        return

    def to_cv_contour(self):
        '''
        return an opencv contour containing the vertices of the rectangle.
        '''
        top_right = np.array([self._bottom_right[0], self._top_left[1]])
        bottom_left = np.array([self._top_left[0], self._bottom_right[1]])
        
        cv_contour = np.vstack([self._top_left, top_right, self._bottom_right, bottom_left]).reshape((-1,1,2))
        return cv_contour
    
    @classmethod
    def contour_bounding_rectangle(cls, contour):
        '''
        contour - a contour object
        Return a rectangle bounding the contour.
        '''
        x,y,w,h = cv2.boundingRect(contour.get_cv_contour())
        top_left = np.array([x,y])
        bottom_right = np.array([x+w, y+h])
        return cls(top_left, bottom_right)
    
    def __str__(self):
        return 'top_left: ' + str(self._top_left) +  ' bottom_right: ' +  str(self._bottom_right)
