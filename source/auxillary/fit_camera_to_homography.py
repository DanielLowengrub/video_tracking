import numpy as np
import functools
from scipy import optimize

"""
This file provide a solution to the following problem:
Suppose we have a homography matrix H, the position of the camera T and the camera aspect ratio k. 
We also have approximate values for the internal
parameter matrix C and the world to camera matrix R.
Goal: Find the matrices R and C which minimize the sum of squares of:

(C * [R | -R*T])[:,(0,1,3)] - H

We assume that R has the form:
[r0 -r1 0]
[0   0  1]
[r1 r0  0]

and that C has the form:
[a 0   ux]
[0 k*a uy]
[0 0   b ]
"""

def error(k,T,H,a,b,ux,uy,r0,r1):
    '''
    k,a,b,ux,uy,r0,r1 - floats
    T - numpy array with shape (3,)
    H - numpy array with shape (3,3)

    Let C and R be as above. return sum of square of (C * [R | -R*T])[:,(0,1,3)] - H
    '''
    return (a**2*r0**2*T[0]**2 + 2*a*ux*r0*r1*T[0]**2 + b**2*r1**2*T[0]**2 + ux**2*r1**2*T[0]**2 + uy**2*r1**2*T[0]**2 +
            2*a*ux*r0**2*T[0]*T[1] - 2*a**2*r0*r1*T[0]*T[1] + 2*b**2*r0*r1*T[0]*T[1] + 2*ux**2*r0*r1*T[0]*T[1] +
            2*uy**2*r0*r1*T[0]*T[1] - 2*a*ux*r1**2*T[0]*T[1] + b**2*r0**2*T[1]**2 + ux**2*r0**2*T[1]**2 + uy**2*r0**2*T[1]**2 -
            2*a*ux*r0*r1*T[1]**2 + a**2*r1**2*T[1]**2 + 2*a*k*uy*r1*T[0]*T[2] + 2*a*k*uy*r0*T[1]*T[2] + a**2*k**2*T[2]**2 +
            a**2*r0**2 + b**2*r0**2 + ux**2*r0**2 + uy**2*r0**2 + a**2*r1**2 + b**2*r1**2 + ux**2*r1**2 + uy**2*r1**2 +
            2*a*r0*T[0]*H[0,2] + 2*ux*r1*T[0]*H[0,2] + 2*ux*r0*T[1]*H[0,2] - 2*a*r1*T[1]*H[0,2] + 2*uy*r1*T[0]*H[1,2] +
            2*uy*r0*T[1]*H[1,2] + 2*a*k*T[2]*H[1,2] + 2*b*r1*T[0] + 2*b*r0*T[1] - 2*a*r0*H[0,0] - 2*ux*r1*H[0,0] -
            2*ux*r0*H[0,1] + 2*a*r1*H[0,1] - 2*uy*r1*H[1,0] - 2*uy*r0*H[1,1] - 2*b*r1*H[2,0] - 2*b*r0*H[2,1] + H[0,0]**2 +
            H[0,1]**2 + H[0,2]**2 + H[1,0]**2 + H[1,1]**2 + H[1,2]**2 + H[2,0]**2 + H[2,1]**2 + 1)

def derror_da(k,T,H,a,b,ux,uy,r0,r1):
    return (2*a*r0**2*T[0]**2 + 2*ux*r0*r1*T[0]**2 + 2*ux*r0**2*T[0]*T[1] - 4*a*r0*r1*T[0]*T[1] - 2*ux*r1**2*T[0]*T[1] -
            2*ux*r0*r1*T[1]**2 + 2*a*r1**2*T[1]**2 + 2*k*uy*r1*T[0]*T[2] + 2*k*uy*r0*T[1]*T[2] + 2*a*k**2*T[2]**2 +
            2*a*r0**2 + 2*a*r1**2 + 2*r0*T[0]*H[0,2] - 2*r1*T[1]*H[0,2] + 2*k*T[2]*H[1,2] - 2*r0*H[0,0] + 2*r1*H[0,1])

def derror_db(k,T,H,a,b,ux,uy,r0,r1):
    return (2*b*r1**2*T[0]**2 + 4*b*r0*r1*T[0]*T[1] + 2*b*r0**2*T[1]**2 + 2*b*r0**2 + 2*b*r1**2 + 2*r1*T[0] + 2*r0*T[1] -
            2*r1*H[2,0] - 2*r0*H[2,1])

def derror_dux(k,T,H,a,b,ux,uy,r0,r1):
    return (2*a*r0*r1*T[0]**2 + 2*ux*r1**2*T[0]**2 + 2*a*r0**2*T[0]*T[1] + 4*ux*r0*r1*T[0]*T[1] - 2*a*r1**2*T[0]*T[1] +
            2*ux*r0**2*T[1]**2 - 2*a*r0*r1*T[1]**2 + 2*ux*r0**2 + 2*ux*r1**2 + 2*r1*T[0]*H[0,2] + 2*r0*T[1]*H[0,2] -
            2*r1*H[0,0] - 2*r0*H[0,1])

def derror_duy(k,T,H,a,b,ux,uy,r0,r1):
    return (2*uy*r1**2*T[0]**2 + 4*uy*r0*r1*T[0]*T[1] + 2*uy*r0**2*T[1]**2 + 2*a*k*r1*T[0]*T[2] + 2*a*k*r0*T[1]*T[2] +
            2*uy*r0**2 + 2*uy*r1**2 + 2*r1*T[0]*H[1,2] + 2*r0*T[1]*H[1,2] - 2*r1*H[1,0] - 2*r0*H[1,1])

def derror_dr0(k,T,H,a,b,ux,uy,r0,r1):
    return (2*a**2*r0*T[0]**2 + 2*a*ux*r1*T[0]**2 + 4*a*ux*r0*T[0]*T[1] - 2*a**2*r1*T[0]*T[1] + 2*b**2*r1*T[0]*T[1] +
            2*ux**2*r1*T[0]*T[1] + 2*uy**2*r1*T[0]*T[1] + 2*b**2*r0*T[1]**2 + 2*ux**2*r0*T[1]**2 + 2*uy**2*r0*T[1]**2 -
            2*a*ux*r1*T[1]**2 + 2*a*k*uy*T[1]*T[2] + 2*a**2*r0 + 2*b**2*r0 + 2*ux**2*r0 + 2*uy**2*r0 + 2*a*T[0]*H[0,2] +
            2*ux*T[1]*H[0,2] + 2*uy*T[1]*H[1,2] + 2*b*T[1] - 2*a*H[0,0] - 2*ux*H[0,1] - 2*uy*H[1,1] - 2*b*H[2,1])

def derror_dr1(k,T,H,a,b,ux,uy,r0,r1):
    return (2*a*ux*r0*T[0]**2 + 2*b**2*r1*T[0]**2 + 2*ux**2*r1*T[0]**2 + 2*uy**2*r1*T[0]**2 - 2*a**2*r0*T[0]*T[1] +
            2*b**2*r0*T[0]*T[1] + 2*ux**2*r0*T[0]*T[1] + 2*uy**2*r0*T[0]*T[1] - 4*a*ux*r1*T[0]*T[1] - 2*a*ux*r0*T[1]**2 +
            2*a**2*r1*T[1]**2 + 2*a*k*uy*T[0]*T[2] + 2*a**2*r1 + 2*b**2*r1 + 2*ux**2*r1 + 2*uy**2*r1 + 2*ux*T[0]*H[0,2] -
            2*a*T[1]*H[0,2] + 2*uy*T[0]*H[1,2] + 2*b*T[0] - 2*ux*H[0,0] + 2*a*H[0,1] - 2*uy*H[1,0] - 2*b*H[2,0])

def J_error(k,T,H,a,b,ux,uy,r0,r1):
    return np.array([derror_da(k,T,H,a,b,ux,uy,r0,r1), derror_db(k,T,H,a,b,ux,uy,r0,r1), derror_dux(k,T,H,a,b,ux,uy,r0,r1),
                     derror_duy(k,T,H,a,b,ux,uy,r0,r1), derror_dr0(k,T,H,a,b,ux,uy,r0,r1), derror_dr1(k,T,H,a,b,ux,uy,r0,r1)])

def rotation_constraint(a,b,ux,uy,r0,r1):
    return r0**2 + r1**2 - 1

def J_rotation_constraint(a,b,ux,uy,r0,r1):
    return np.array([0,0,0,0,2*r0,2*r1])

def fit_camera_to_homography(C, R, T, H):
    '''
    C - a numpy array with shape (3,3), which is upper triangular
    R - a numpy array with shape (3,3), which is orthogonal
    T - a numpy array with shape (3,)
    H - a numpy array with shape (3,3), representing a homography matrix

    return - a pair (C_opt, R_opt) such that:
    * R_opt, C_opt are of the form specified in the docstring for this file
    * They minimize the sum of squares error: (C * [R | -R*T])[:,(0,1,3)] - H, where T and H are fixed
    '''

    #set up the function to optimize, and it's jacobian
    aspect = C[1,1] / C[0,0]
    f =  lambda x: error(aspect, T, H, x[0], x[1], x[2], x[3], x[4], x[5])
    Jf = lambda x: J_error(aspect, T, H, x[0], x[1], x[2], x[3], x[4], x[5])

    #get the initial values from C and R
    x0 = np.array([C[0,0],C[2,2],C[0,2],C[1,2],R[0,0],R[2,0]])

    #if R[1,2] is -1 instead of 1, then invert the initial values
    if R[1,2] < 0:
        x0 *= -1

    #set up the constraint dictionary
    h =  lambda x: rotation_constraint(*x)
    Jh = lambda x: J_rotation_constraint(*x)
    constraint_dict = {'type':'eq', 'fun':h, 'jac':Jh}

    #optimize C and R
    #print 'starting optimization...'
    ret = optimize.minimize(f, x0, jac=Jf, method='SLSQP',constraints=constraint_dict, options={'maxiter':1000})
    #print ret
    
    #recover C and R from the optimized vector
    a,b,ux,uy,r0,r1 = ret.x.tolist()

    C_opt = np.array([[a, 0, ux],[0,aspect*a, uy], [0,0,b]])
    R_opt = np.array([[r0,-r1,0],[0,0,1],[r1,r0,0]])

    return C_opt, R_opt
