import tensorflow as tf
import numpy as np

class TFOptimizer(object):
    '''
    A tensor flow optimizer minimizes a given function with a specified initial value.
    '''

    def __init__(self, learning_rate, max_epochs, local_minimum_threshold):
        '''
        learning_rate - the step sizes in the gradient direction that we take in each iteration
        max_epochs - the maximum number of iterations
        local_minimum_threshold - we decide that we are in a local minimum if the cost function changes by less than this.
        '''

        self._learning_rate = learning_rate
        self._max_epochs = max_epochs
        self._local_minimum_threshold = local_minimum_threshold

        self._sess = None

    def initialize_session(self, sess, initial_value, cost_function):
        self._sess = sess
        self._optimized_var = tf.Variable(initial_value, name='optimized_value')
        self._cost = cost_function(self._optimized_var)
        self._optimizer = tf.train.GradientDescentOptimizer(self._learning_rate).minimize(self._cost)
        return
    
    def minimize(self, initial_value, feed_dict, print_progress_interval=-1):
        '''
        initial_value - a numpy array
        cost_function - a function with one argument that is a tensorflow array with the same shape and type as 
                        the initial value, and which outputs a number.
        print_progress_interval - we print the progress every print_progress_interval epochs. If the value is negative,
                                  do not print anything.
        Return a numpy array with the same shape and type as initial_value that locally minimizes the cost function.
        '''
        assign_optimized_var = tf.assign(self._optimized_var, initial_value)
    
        #initialize the variable
        self._sess.run(assign_optimized_var)
        
        prev_training_cost = 0.0
        for epoch_i in range(self._max_epochs):
            self._sess.run(self._optimizer, feed_dict=feed_dict)
            training_cost = self._sess.run(self._cost, feed_dict=feed_dict)

            if (print_progress_interval > 0) and (epoch_i % print_progress_interval == 0):
                #print the cost after this step
                print '\n\nsummary of epoch %d:' % epoch_i
                print '   training cost: %f' % training_cost
                
            # Allow the training to quit if we've reached a local minimum
            if np.abs(prev_training_cost - training_cost) < self._local_minimum_threshold:
                break
            
            prev_training_cost = training_cost

        #get the final optimized value
        optimized_value = self._sess.run(self._optimized_var)

        #sess.close()

        return optimized_value
    
