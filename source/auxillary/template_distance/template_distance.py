class TemplateDistance(object):
    '''
    The job of a TemplateDistance class is to compute the distance between two image templates.
    Different implementation use different methods to compute the distance, such as the raw normed distance between pixel values,
    or by comparing the histogram.
    '''

    def compute_distance(self, image_A, image_B, mask):
        '''
        image_A, image_B - numpy arrays of shape (n,m,3) and type uint8
        mask - a numpy array of shape (n,m) and type np.float64

        We return the distance between the images, where we take into account only the pixels in the masked region.
        '''

        pass
