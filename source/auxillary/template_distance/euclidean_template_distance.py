from .template_distance import TemplateDistance

class EuclideanTemplateDistance(TemplateDistance):
    '''
    This implementation of TemplateDistance simply computes the average euclidean destance between the two images.
    '''

    def compute_distance(self, image_A, image_B, mask):

        #first compute the norm of the difference at each pixel
        norms_of_difference = np.linalg.norm(np.int32(cropped_texture_map) - np.int32(cropped_image), axis=2)

        #weight by the foreground mask, remove obstructed regions and take the sum
        evaluation_on_unobstructed_region = (norms_of_difference * cropped_foreground_mask * (1 - cropped_obstruction_mask)).sum()

        #the area of the region that is both in the foreground and unobstructed
        area_of_unobstructed_region = (cropped_foreground_mask * (1 - cropped_obstruction_mask)).sum()

        normalized_evaluation_on_unobstructed_region = evaluation_on_unobstructed_region / area_of_unobstructed_region
        
