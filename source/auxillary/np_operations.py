import numpy as np
import itertools
from scipy import spatial
from scipy.sparse import csgraph

"""
Commonly used numpy patterns
"""

def add_constant_column_to_right(array, constant_value):
    '''
    array - a numpy array with shape (n,m)
    contant_value - a number with the same type as the array
    
    return - a numpy array with shape (n,m+1) which is equal to "array" with a column of "constant_value" added to the right.
    '''
    return np.pad(array, ((0,0),(0,1)), 'constant', constant_values=constant_value)

def group_by_row(array, return_group_rep_indices=False):
    '''
    array - a numpy array with shape (n,m)

    return a tuple (group_representatives, group_ids). If return_group_representative_indices is True, return a tuple
    (group_representatives, group_ids, group_representative_indices)
    the number of groups is the number of unique rows in the array. The group id's are assigned from 0,...,num_groups-1.

    group_representatives - a numpy array with shape (num groups, m). The rows contain exactly one element of each group.
    group_ids - a numpy array with shape (n,) and type np.int32. Then the values of group_ids are between 0 and num_groups-1.
                It has the property that: array[i] == array[j] <=> group_ids[i] == group_ids[j]
    group_representative_indices - a numpy array with shape (num groups,) and type np.int32. The i-th element is the index of
                                   the i-th group representative.
    In other words, the array group_ids assigns a group id from the ids (0,...,num groups-1)
    to each of the rows in array, where two rows are in the same group iff they are equal.
    '''

    #first sort the rows lexicographically and store the sorted indices
    #i.e, array[sorted_indices[i]] <= array[sorted_indices[i+1]]
    #in other words, if sorted_array is the sorted array, sorted_indices[i] is the index of the i-th element of
    #sorted_array in the array
    sorted_indices = np.lexsort(array.transpose())

    #let sorted_array be the array sorted by row as above.
    #build an array is_new of type bool that has the property that:
    #sorted_array[i+1] is the first element of it's kind in sorted_array <=> is_new[i]
    #note the first element is clearly new.
    is_new = np.any(np.diff(array[sorted_indices], axis=0) != 0, axis=1)
    is_new = np.hstack([[True], is_new])
                       
    #the group representatives are the elements of the sorted array that are new
    group_representatives = array[sorted_indices[is_new]]

    #this array assigns group ids to the elements of sorted_array.
    #subtract one so that the indices go from 0 and not 1. The first element always has index 0
    group_ids_of_sorted_array = np.cumsum(is_new) - 1
    
    #group_ids stores the group ids of the original array.
    group_ids = np.empty(len(sorted_indices), dtype=np.int32)
    group_ids[sorted_indices] = group_ids_of_sorted_array

    if not return_group_rep_indices:
        return group_representatives, group_ids

    else:
        #this stores the indices of the group representatives in the sorted array
        group_rep_sorted_array_indices = np.where(is_new)
        group_rep_indices = sorted_indices[group_rep_sorted_array_indices]
        
        return group_representatives, group_ids, group_rep_indices
    
def find_unique_rows(array, decimals=None):
    '''
    array - a numpy array with shape (n,m)
    decimal_places - an integer
    
    return a numpy array with shape (k,m) and the same type as array which contains the unique rows up to the specified number
    of decimal places.
    '''

    if decimals is None:
        array_copy = array

    else:
        array_copy = np.around(array, decimals=decimals)

    #group the rows
    group_representatives, group_ids, group_rep_indices = group_by_row(array_copy, True)

    return array[group_rep_indices]

def stack_images(images):
    '''
    images - a list of np arrays with shape (ni,mi,3) and type np.uint8
    
    return a numpy array with shape (n,m,3) where n = max(ni) and m = sum(mi)
    it contains the images stacked ontop of one another.
    '''

    output_width = max(image.shape[1] for image in images)

    #we have to pad the images so that they all have the same width
    padded_images = []
    for image in images:
        total_padding = output_width - image.shape[1]
        padding_left = total_padding / 2
        padding_right = total_padding - padding_left

        padded_image = np.pad(image, pad_width=((0,0),(padding_left,padding_right),(0,0)), mode='constant')
        padded_images.append(padded_image)

    return np.vstack(padded_images)

def batch_multiply_matrix_vector(A, x):
    '''
    A - a numpy array with shape (m,n) or (l,m,n)
    x - a numpy array with shape (l,n)
    return - a numpy array with shape (l,m) 
    '''
    l = x.shape[0]
    m,n = A.shape[-2:]

    x_vertical = x.reshape((l,n,1))
    Ax = np.matmul(A, x_vertical)
    return Ax.reshape((l,m))

def batch_multiply_vector_matrix(x, A):
    '''
    A - a numpy array with shape (m,n) or (l,m,n)
    x - a numpy array with shape (l,m)
    return - a numpy array with shape (l,n) 
    '''
    l = x.shape[0]
    m,n = A.shape[-2:]

    x_horizontal = x.reshape((l,1,m))
    xA = np.matmul(x_horizontal, A)
    return xA.reshape((l,n))

def batch_multiply_vector_matrix_vector(x, A, y):
    '''
    A - a numpy array with shape (m,n) or (l,m,n)
    x - a numpy array with shape (l,m)
    y - a numpy array with shape (l,n)
    return - a numpy array with shape (l,)
    '''
    l = x.shape[0]
    m,n = A.shape[-2:]

    x_horizontal = x.reshape((l,1,m))
    y_vertical   = y.reshape((l,n,1))
    return np.matmul(np.matmul(x_horizontal, A), y_vertical).reshape((l,))

def batch_transpose(A):
    '''
    A - a numpy array with shape (l,m,n) or (m,n)
    return - a numpy array with shape (l,n,m) or (n,m)
    '''
    if len(A.shape) == 2:
        return np.transpose(A)
    else:
        return np.transpose(A, (0,2,1))
    
def _minor(A, i, j):
    '''
    A - an (n,m) array
    i,j - integers. 0 <= i < n, 0 <= j < m

    return: The i,j minor of A.
    '''
    n,m = A.shape[:2]
    return A[np.array(range(i) + range(i+1,n)).reshape((-1,1)), np.array(range(j)+range(j+1,m))]

def adjugate(A):
    '''
    A - an (n,m) array
    return adj(A)
    '''
    n,m = A.shape
    adjA = np.empty(A.shape)

    for i,j in itertools.product(range(n), range(m)):
        Mji = _minor(A,j,i)
        adjA[i,j] = ((-1)**(i+j)) * np.linalg.det(Mji)

    return adjA

def poly_adjugate(A, B):
    '''
    A, B - numpy arrays with shape (3,3)

    return - a numpy array with shape (3,3,3). The element (i,j) stores the coefficients of 
    the determinant of the ji-th minor of A + x*B
    '''
    #this is a (n,n,3) array representing A + x*B. As usual, the coefficients are ordered from high degree to low degree
    P = np.dstack([B, A])

    #initialize the array of determinants
    adjP = np.zeros((3,3,3))

    for i,j in itertools.product(range(3), range(3)):
        #M is an array with shape (2,2,2)
        M = _minor(P,j,i)

        #convert the elements of M to polynomials
        m00, m01, m10, m11 = [np.poly1d(m) for m in M.reshape((4,2))]

        #compute the determinant as a polynomial.
        detM = m00*m11 - m10*m01
        d = len(detM.coeffs)-1
        
        #store the coefficients in adjP_ij. Since detM only has d (= the degree of the polynomial) terms,
        #store detM in the LAST d elements of adjP_ij
        adjP[i,j][2-d:] = ((-1)**(i+j)) * detM

    return adjP

def anti_symmetric(v):
    '''
    v - a length 3 numpy array
    return the antisymmetric matrix associated to v.
    '''
    S_v = np.array([[0    , v[2],-v[1]],
                    [-v[2], 0   , v[0]],
                    [ v[1],-v[0], 0   ]])

    return S_v

def find_points_in_mask(mask):
    '''
    mask - a np array with shape (n,m) and type np.bool

    return - a numpy array with shape (N,2) and type np.int32. N is the number of pixels (x,y) for which mask[y,x] is True.
    the output array is a list of all such pixels.
    '''

    y_coords, x_coords = np.where(mask)
    coords = np.vstack([x_coords, y_coords]).transpose()

    return coords

def substitute_values(array, substitution_dict):
    '''
    array - a numpy array of type np.int32
    substitution_dict - a dictionary with items (source value, target value)

    all values in the array must be keys in the value_dict.

    return a numpy array with the same shape as array, and for every (multi)index I, output[I] = value_dict[array[I]]
    '''

    source_values = sorted(substitution_dict.keys())
    target_values = np.array([substitution_dict[sv] for sv in source_values])
    
    #index is a one dimensional array which satisfies: source_values[index[i]] = array.ravel()[i]
    #I.e, for each value x in the array, it stores the index of x in source_values
    index = np.digitize(array.ravel(), source_values, right=True)

    #replace the value source_values[i] with target_values[i]
    changed_array = target_values[index]

    return changed_array

def find_slice(interval, array):
    '''
    interval - a tuple of integers (i,j)
    array - a numpy array with shape (n,) and type np.int32 which is sorted from small to big

    return a slice object s which is the greatest slice such that for all x in array[s], i <= x <= j
    '''
    #this is the smallest integer k such that array[k] >= i
    start = np.searchsorted(array, interval[0], side='left')

    #this is the largest integer such k such that array[k-1] <= j
    end   = np.searchsorted(array, interval[1], side='right')

    return slice(start, end)

def group_points_by_distance(points, max_neighbor_distance):
    '''
    points - a numpy array with shape (num points, num dimensions)
    clustering_radius - a float

    create a graph whose vertices are points. there is an edge between two points if their distance is less than 
    max_neighbor_distance. We label each connected component of this graph by an integer.

    return - num_groups, labels
    num_groups - the number of CC of the graph
    labels - a numpy array with shape (num points,) and type np.int32. The i-th entry is the label of the component that 
    contains the point points[i]
    '''
    #build a num_points x num_points distance matrix
    distances = spatial.distance.squareform(spatial.distance.pdist(points))
    edges = distances <= max_neighbor_distance
    
    #find the connected components of the graph whose edge matrix is "distances"
    num_components, point_labels = csgraph.connected_components(edges)

    return num_components, point_labels

def find_local_maxima(values, bandwidth):
    '''
    values - a numpy array with shape (n,)
    return - a numpy array with shape (n,) and type np.bool. An entry i has value True iff values[i] is maximal among the 
    values values[i-bandwidth:i+bandwidth]
    '''
