import numpy as np
import cv2
from .fit_points_to_shape.ellipse import Ellipse

class CameraMatrix(object):
    '''
    This contains basic functionality for camera matrices.

    * It contains a method for constructing the camera matrix out of a 2D homography matrix, assuming that we have some additional 3D points.
    * It also contains a method for projecting an ellipsoid onto the image plane.
    '''

    def __init__(self, camera_matrix):
        '''
        camera_matrix - a numpy array of shape (3,4) and type np.float32.
        '''

        self._camera_matrix = np.float64(camera_matrix)

        #These are the components of the camera matrix. They are only computed if needed.
        self._C = None #the internal camera parameters. A 3x3 matrix
        self._R = None #the rotation matrix
        self._T = None #the translation vector

        self._camera_position = None

        self._has_camera_parameters = False
        
    @classmethod
    def _compute_alpha(cls, homography, world_positions, image_positions):
        H = homography
        
        x = world_positions[:,0]
        y = world_positions[:,1]
        z = world_positions[:,2]

        u = image_positions[:,0]
        v = image_positions[:,1]

        a   = v * (H[2,0]*x + H[2,1]*y + H[2,2])
        b   =     (H[1,0]*x + H[1,1]*y + H[1,2])

        alpha = (a - b) / z
        
        return alpha.mean()

    @classmethod
    def from_planar_homography(cls, homography, world_positions, image_positions):
        '''
        homography - a numpy array of shape (3,3) and type np.float32. 
                     This represents a homography from the world z=0 plane to an image plane
        world_positions - a numpy array of shape (n,3) and type np.float32
        image_positions - a numpy array of shape (n,2) and type np.float32

        We return a CameraMatrix that is equal to the given homography when restricted to the Z=0 plane, and which sends the i-th world
        point to the i-th image point.

        HACK - we currently assume that the camera preserves vertical lines!!
        '''

        #we first compute the value CM[1,2]
        alpha = cls._compute_alpha(homography, world_positions, image_positions)

        #now insert this column into the homography matrix
        col = np.array([0,alpha,0])
        camera_matrix = np.hstack([homography[:,:2], col.reshape(-1,1), homography[:,2].reshape(-1,1)])

        return cls(camera_matrix)

    @classmethod
    def from_camera_position(cls, camera_position, camera_rotation, focus):
        '''
        R - the 3x3 rotation matrix from the world coordinates to the camera coordinates
        P - the position of the camera in the world
        f - in camera coordinates, the objects are projected onto the z=f plane. AKA the focus.
        '''

        # calibration matrix
        R = camera_rotation
        P = camera_position
        
        C = np.array([[focus, 0, 0],
                      [0, focus, 0],
                      [0, 0,     1]])

        # transformation matrix from projective world coordinates to camera coordinates
        R_RP = np.hstack([R, np.dot(R,-P.reshape((-1,1)))])

        #the camera matrix from world coordinates to image coordinates
        camera_matrix = np.dot(C, R_RP)

        return cls(camera_matrix)

    @classmethod
    def from_camera_position_and_direction(cls, camera_position, camera_direction, f):
        '''
        camera_position - a numpy array with shape (3,)
        camera_direction - a numpy array with shape (3,)
        f - a float

        return a CameraMatrix object representing a camera that is positioned at the given position, and that is pointing
        in the given direction. We assume that the z-axis of the world is mapped to the y-axis of the image.
        '''
        #the camera Z axis. I.e, where the camera is pointing.
        CZ = camera_direction
        CZ = CZ / np.linalg.norm(CZ)

        #print 'CZ: ', CZ
        #now find the camera rotation matrix R. We already have the Z axis
        #The new y axis will be the projection of the old z axis onto the plane defined by CZ
        z_axis = np.array([0,0,1])
        CY = z_axis - np.dot(z_axis, CZ)*CZ
        CY = CY / np.linalg.norm(CY)

        #the camera X axis is orthogonal to CY and CZ
        CX = np.cross(CY, CZ)

        #R is the transformation from camera coordinates to world coordinates
        R = np.vstack([CX, CY, CZ]).transpose()

        #print 'R: '
        #print R
        #invert it to get the transformation from world coordinates to camera coordinates
        R = np.linalg.inv(R)
        
        print 'R:'
        print R
        
        return cls.from_camera_position(camera_position, R, f)

    @classmethod
    def from_homography_and_alpha(cls, H, alpha):
        '''
        H - a numpy array with shape (3,3) and type np.float32
        alpha - a float

        return a CameraMatrix object whose matrix C satisfies: C[:,(0,1,3)] = H, C[:,2] = [0,alpha,0]
        '''
        col = np.array([0,alpha,0])
        camera_matrix = np.hstack([H[:,:2], col.reshape(-1,1), H[:,2].reshape(-1,1)])

        return cls(camera_matrix)

    @classmethod
    def from_position_rotation_internal_parameters(cls, T, R, C):
        RT = np.hstack([R, -np.matmul(R, T).reshape((3,1))])
        camera_matrix = np.matmul(C, RT)
        return cls(camera_matrix)
    
    def get_numpy_matrix(self):
        return self._camera_matrix

    def get_alpha(self):
        return self._camera_matrix[1,2]
    
    def compute_camera_parameters(self):
        '''
        Compute the internal camera parameters (focus, image center), rotation matrix, and translation (i.e, the position of the camera in world coordinates)
        '''
        if self._has_camera_parameters:
            return
        
        #first perform RQ decomposition
        retval, self._C, self._R, qx, qy, qz = cv2.RQDecomp3x3(self._camera_matrix[:,:3])

        #then use the camera parameters to compute the camera position
        self._T = np.dot(np.linalg.inv(self._C), self._camera_matrix[:,3])
        self._camera_position = -np.dot(np.linalg.inv(self._R), self._T)

        self._has_camera_parameters = True
        
    def get_camera_position(self):
        self.compute_camera_parameters()
        return self._camera_position

    def get_internal_parameters(self):
        self.compute_camera_parameters()
        return self._C

    def get_world_to_camera_rotation_matrix(self):
        self.compute_camera_parameters()
        return self._R
    
    def _get_ellipsoid_parameterization(self, world_position, axes):
        '''
        world position - a numpy vector of length 3
        axes - a numpy vector of length 3

        Return a (4,4) numpy array representing the given ellipsoid.
        '''
        #first compute the 4x4 translation matrix from the given position to the origin
        T = np.eye(4)
        T[:3,3] = -world_position

        #compute the 4x4 parameterization of the ellipsoid at the origin
        Q = np.diag([1.0/(axes[0]**2), 1.0/(axes[1]**2), 1.0/(axes[2]**2), -1])
        
        return np.float64(np.dot(T.transpose(), np.dot(Q, T)))

    def project_points_to_image(self, world_points):
        '''
        world_points - a numpy array with shape (number of points, 3) and type np.float32
        
        return a numpy array with shape (number of points, 2) and type np.float32.

        We project each of the points to image coordinates and return the result.
        '''

        return cv2.perspectiveTransform(world_points.reshape((-1,1,3)), self._camera_matrix).reshape((-1,2))

    def project_point_to_image(self, world_point):
        '''
        world_point - a numpy array with shape (3,)
    
        return - a numpy array with shape (2,) which is the projection of the world point to the image
        '''
        return self.project_points_to_image(world_point.reshape((1,3)))[0]
    
    def project_ellipsoid_to_image(self, world_position, axes):
        '''
        world position - a numpy vector of length 3
        axes - a numpy vector of length 3

        We construct an ellipsoid with the given position and axes.
        We return an Ellipse object which is the projection of the given ellipsoid on the image plane.
        '''

        ellipsoid = self._get_ellipsoid_parameterization(world_position, axes)

        Q = np.linalg.inv(ellipsoid)
        P = self._camera_matrix
        C = np.linalg.inv(np.dot(P, np.dot(Q, P.transpose())))

        return Ellipse.from_parametric_representation(C)

    def project_ellipse_to_image(self, world_position, xy_axes):
        '''
        world_position - a numpy array of length 3
        xy_axes - a numpy array of length 2

        Let E be the ellipse on the {z = world_position[2]} plane whose center is world_position[:2] and whose axes are xy_axes.
        We return an Ellipse object representing the projection of the ellipse to the image.
        '''
        
        #We get an Ellipse object representing the ellipse on the xy plane.
        world_ellipse = Ellipse.from_geometric_parameters(world_position[:2], xy_axes, 0.0)
        world_dual = np.matrix(world_ellipse.get_dual())
        P = np.matrix(self._camera_matrix)
        h = world_position[2]
        
        #Recall that if l is a line on the image then P^t * l represents plane in the world which projects onto the line via the camera matrix P.
        #Therefore, a line on the image is dual to the projection of the ellipse iff
        #(intersection of P^t*l and the plane {z=h}) is in the dual to the world ellipse iff
        #A_h * P^t * l is in the dual to the world ellipse where
        #      (1 0 0 0)
        #A_h = (0 1 0 0)
        #      (0 0 h 1)
        
        A = np.matrix(np.zeros((3,4), np.float32))
        A[0,0] = A[1,1] = A[2,3] = 1
        A[2,2] = h

        image_dual = P * A.T * world_dual * A * P.T
        image_ellipse = Ellipse.from_parametric_representation(np.array(image_dual.I))
        
        return image_ellipse        

    def compute_sq_distance_to_camera(self, world_points):
        '''
        world_points - a numpy array with shape (num pts, 2) or (num pts, 3). 
        In the first case it represents a list of points in the z=0 plane. In the second case it represents a list of world
        points.

        Return an array with length num_pts. The i-th entry is equal to the square of the distance 
        of the i-th points from the camera origin. If the points are in the z=0 plane, we return the distance to the
        projection of the camera to the z=0 plane.
        '''
        self.compute_camera_parameters()
        
        #dims is equal to either 2 or 3
        dims = world_points.shape[1]

        dist_sq = world_points - self._camera_position[:dims]
        dist_sq = (dist_sq * dist_sq).sum(axis=1)

        return dist_sq
        
    def sort_indices_by_distance_to_camera(self, world_points):
        '''
        world_points - a numpy array with shape (number of points, 3)
        return a list of integers L, such that world_points[L[i]] is closer to the camera than world_points[L[j]] for all i<j.
        '''
        self.compute_camera_parameters()
        
        dist_sq = world_points - self._camera_position
        dist_sq = (dist_sq * dist_sq).sum(axis=1)
        
        sorted_indices = range(len(world_points))
        sorted_indices.sort(key = lambda x: dist_sq[x])
        
        return sorted_indices

    def get_homography_matrix(self):
        '''
        Return the homography matrix corresponding to the restriction of the camera matrix to the z=0 plane.
        '''

        #for the homography matrix, remove the third column
        homography_matrix = self._camera_matrix[:,(0,1,3)]

        return homography_matrix

    def compute_preimage_line(self, image_point):
        '''
        image_point - a numpy array with length 2
        return a numpy array with shape (2,4). Each row is the parametric representation of a plane.
        The intersection of the two planes is the preimage of the image point under the camera map in world coordinates
        '''
        #see the method compute_preimage_planes for an explanation
        preimage_planes = self._camera_matrix[:2] - (image_point.reshape((2,1)) * self._camera_matrix[2])
        return preimage_planes
    
    def compute_preimage_on_plane(self, image_point, world_plane):
        '''
        point - a numpy array with shape (2,) which represents a point in image coordinates
        world_plane - a numpy array with shape (4,) which represents a plane in world coords.

        return - a numpy array with shape (3,). It represents a point on the world plane whose image is the image point.

        Algorithm:

        Suppose the camera matrix is C=(cij) and the image point is (x,y). Then (X,Y,Z) is in the preimage of (x,y) iff:
        [c00 c01 c02 c03][X]      [x]
        [c10 c11 c12 c13][Y] \sim [y]
        [c20 c21 c22 c23][Z]      [1]
                         [1]
        
        This means that: c00X + c01Y + c02Z + c03 = x(c20X + c21Y + c22Z + c23) =>
                         (c00 - x*c20, c01 - x*c21, c02 - x*c22) * (X, Y, Z) + c03 - x*c23 = 0   similarly:
                         (c10 - y*c20, c11 - y*c21, c12 - y*c22) * (X, Y, Z) + c13 - y*c23 = 0

        So the preimage of image_point is the line defined by the intersection of the planes:
        (c00 - x*c20, c01 - x

*c21, c02 - x*c22, c03 - x*c23)
        (c10 - y*c20, c11 - y*c21, c12 - y*c22, c13 - y*c23)

        We return the intersection of these planes and the world_plane.
        '''

        #first build the two planes defining the line that is the preimage of the point
        preimage_planes = self._camera_matrix[:2] - (image_point.reshape((2,1)) * self._camera_matrix[2])

        #intersect the preimage planes and the world planes
        world_planes = np.vstack([preimage_planes, world_plane])

        #set up a system of equations Ax=b such that x=(X,Y,Z) satisfies the system iff (X,Y,Z) lies on all 3 world planes
        A = world_planes[:,:3]
        b = -world_planes[:,3]

        world_point = np.linalg.lstsq(A,b)[0]

        return world_point
