#############################
# This file contains a barebones graph implementation that is stored in a way that it optimized for iterating over triangles.
#############################

class Vertex(object):
    '''
    Each vertex has an id, and stores the id's of it's neighbors that have a larger id.
    '''

    def __init__(self):
        self._vertex_id = None
        self._neighbor_ids = []

    def set_id(self, vertex_id):
        self._vertex_id = vertex_id

    def has_id(self):
        return self._vertex_id is not None

    def get_id(self):
        return self._vertex_id
    
    def add_neighbor_id(self, neighbor_id):
        self._neighbor_ids.append(neighbor_id)

    def finalize_neighbors(self):
        self._neighbor_ids = set(self._neighbor_ids)
        
    def get_neighbor_ids(self):
        return self._neighbor_ids


class Edge(object):
    '''
    An edge stores a tuple of vertices (sorted by id's).
    '''

    def __init__(self, vertex_a, vertex_b):
        self._vertices = (vertex_a, vertex_b)

    def __str__(self):
        return '(%d,%d)' % tuple(vertex.get_id() for vertex in self._vertices)
    
    def get_vertices(self):
        return self._vertices

    def sort_vertices(self):
        self._vertices = sorted(self._vertices, key=lambda vertex: vertex.get_id())
        
    def get_opposing_vertex_ids(self):
        '''
        Return the set of id's of all the vertices that have id's larger than both of the vertices in this edge, and form a triangle with this vertex.
        '''

        #we take the intersection of the set of neighbors of each of the vertices
        return self._vertices[0].get_neighbor_ids().intersection(self._vertices[1].get_neighbor_ids())

class Graph(object):
    '''
    We store a dictionary of vertices whose keys are the vertex id's and whose values are vertices.
    We also store a dictionary whose keys are sorted tuples of vertex id's, and whose values are edges.
    '''

    def __init__(self, edges):
        self._vertex_dict = dict()
        self._edge_dict = dict()

        for edge in edges:
            self._add_edge(edge)
                
        for vertex in self._vertex_dict.itervalues():
            vertex.finalize_neighbors()

    def number_of_vertices(self):
        return len(self._vertex_dict)

    def number_of_edges(self):
        return len(self._edge_dict)
    
    def _add_edge(self, edge):
        '''
        edge - an Edge object
        We add the edge to the graph.
        '''

        #we first give the vertices in the edge id's
        next_id = len(self._vertex_dict)
        for vertex in edge.get_vertices():
            if not vertex.has_id():
                vertex.set_id(next_id)
                self._vertex_dict[next_id] = vertex
                next_id += 1

        #we now sort the vertices according to their id's
        edge.sort_vertices()

        #now that the vertices are sorted, add the one with the larger id to the neighbor list of the other one
        small_vertex, large_vertex = edge.get_vertices()
        small_vertex.add_neighbor_id(large_vertex.get_id())

        #finally, add the edge to the dictionary, and finalize the vertes neighbor lists
        self._edge_dict[(small_vertex.get_id(), large_vertex.get_id())] = edge

        return

    def get_triangle_iterator(self):
        '''
        Return an iterator for triangles of edges of the form. Each such triangle is returned as a tuple of the form: ((vertex_1, vertex_2, vertex_3), (edge1, edge2, edge3)
        '''

        for (small_id, large_id), edge in self._edge_dict.iteritems():
            #we search for all triangles that can be constructed using this edge
            #to do this, we compute the set of vertices whose id's are larger than the id's in this edge, and which are connected to both of the vertices
            #in this edge
            opposing_vertex_ids = edge.get_opposing_vertex_ids()
            small_vertex, large_vertex = edge.get_vertices()
            
            #return the triangles corresponding the three vertices with ids (small vertex id, large vertex id, opposing vertex id)
            for opposing_id in opposing_vertex_ids:
                vertices = (small_vertex, large_vertex, self._vertex_dict[opposing_id])
                edges = (edge, self._edge_dict[(large_id, opposing_id)], self._edge_dict[(small_id, opposing_id)]) 
                yield  (vertices, edges)
                
