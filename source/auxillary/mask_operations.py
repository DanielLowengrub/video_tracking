import numpy as np
import cv2
from scipy.ndimage import morphology

###################
# This is a collection of frequently used operations between masks
# and images. In general:
#
# An image is a numpy array of shape (n,m,3) and type np.uint8
# A mask is a numpy array of shape (n,m) and type np.float64
#
# UPDATE: Now masks have type np.bool

def get_union(mask_1, mask_2):
    '''
    mask_1, mask_2 - a numpy array with shape (n,m) and type np.float32
    
    We convert both masks to bool by mask = mask > 0. We then take the logical "or" of both masks, and cast the result back to np.float32
    '''

    #convert the masks to boolean
    mask_1 = mask_1 > 0
    mask_2 = mask_2 > 0

    union = np.logical_or(mask_1, mask_2)

    return np.float32(union)

def apply_mask_to_image(image, mask):
    '''
    image is a numpy array with shape (n,m,3) and type np.uint8.
    mask is a numpy array with shape (n,m) and type np.float64
    
    return the result of multiplying the length 3 vector image[i][j] by the scalar mask[i][j] for all (i,j)
    '''
    #we first have to reshape the mask so that it has the same number of dimensions as the image
    mask_shape = mask.shape
    fat_mask_shape = (mask_shape[0], mask_shape[1], 1)
    fat_mask = mask.reshape(fat_mask_shape)
    
    return np.uint8(fat_mask * image)

def mix_images_with_mask(image_a, image_b, mask):
    '''
    both images are numpy arrays with shape (n,m,3) and type np.uint8.
    mask is a numpy array with shape (n,m) and type np.float64
    
    return the image:
    [image_a * mask] + [image_b * (1 - mask)]
    '''
    
    return apply_mask_to_image(image_a, mask) + apply_mask_to_image(image_b, 1 - mask)

def get_mask_from_annotation(annotation, selected_value, erosion = None):
    '''
    annotation - a numpy array of shape (n,m) and type np.int32
    selected_value - an integer.
    erosion - an integer

    Return a mask with shape (n,m) with a one in each of the positions where
    the annotation array has value selected_value.

    If erosion is set, we erode the mask with a kernel of np.ones((erosion, erosion)).
    This is useful if we don't want pixels that are outside the mask to leak into the masked image.
    
    '''

    mask = np.zeros(annotation.shape, np.float64)
    mask[annotation == selected_value] = 1

    #if the erosion parameter is set, erode the mask before returning it
    if erosion is not None:
        kernel = np.ones((erosion, erosion), np.uint8)
        cv_mask = np.uint8(mask)
        cv_mask = cv2.erode(cv_mask, kernel, iterations=1)
        mask = np.float64(cv_mask)
        
    return mask

def get_percentage_of_containment(mask_A, mask_B):
    '''
    mask_A, mask_B - numpy arrays of shape (n,m) and type np.float32

    Return (area of (mask_A and mask_B)) / (area of mask_A)
    '''

    area_A_and_B = (mask_A * mask_B).sum()
    area_A = mask_A.sum()

    return area_A_and_B / area_A

def filter_points(points, mask):
    '''
    points - a numpy array of shape (num points, 2) and type np.float32
    mask - a numpy array of shape (n,m) and type np.float32

    return a numpy array with shape (num points that are in the mask, 2),
    which is the subset of points (x,y) such that mask[y,x] == 1
    '''

    points_as_indices = np.int32(points)
    in_mask = mask[points_as_indices[:,1], points_as_indices[:,0]]

    return points[in_mask == 1.0]

def find_points_in_mask(mask):
    '''
    mask - a numpy array of shape (n,m) and type np.float32
    return a numpy array with shape (num points, 2) and type np.float32. It represents the points (x,y) such that
    mask[y,x] > 0.
    '''
    image_indices = np.argwhere(mask)
    points = np.vstack([image_indices[:,1], image_indices[:,0]]).transpose()
    points = np.float32(points)

    return points

def convert_to_cv_mask(mask):
    '''
    mask - a numpy array of shape (n,m) and type np.float64

    Convert this to an array of type np.uint8, and scale so that 1.0 becomes 255
    '''
    cv_mask = np.uint8( 255 * mask)

    return cv_mask

def convert_from_cv_mask(cv_mask):
    '''
    mask - a numpy array of shape (n,m) and type np.uint8 with values between 0 and 255

    return a numpy array with shape (n,m) and type np.float32 with values between 0 and 1
    '''

    mask = np.float32(cv_mask) / 255.0

    return mask
    
def convert_to_image(mask):
    '''
    mask - a numpy array of shape (n,m) and type np.float64

    We return an image with shape (n,m,3) and type np.uint8 such that pixel (n,m,k) has value mask[n,m] * 255
    '''

    mask = mask.reshape((mask.shape[0], mask.shape[1], 1)) * np.ones((mask.shape[0], mask.shape[1], 3))
    return np.uint8(mask * 255)

def draw_mask(image, mask, color, alpha):
    '''
    image - a numpy array with shape (n,m,3) and type np.uint8
    mask  - a numpy array with shape (n,m) and type np.float32
    color - a length 3 numpy array with type np.uint8
    alpha - a float between 0 and 1

    We replace the pixel image[x,y] with:
    (1-alpha)*image[x,y] + alpha*mask[x,y]*color
    '''

    #we create an array whose [x,y] coordinate contains mask[x,y]*color
    colored_image = np.zeros(image.shape, np.uint8)
    colored_image[:,:] = color
    colored_image = mix_images_with_mask(colored_image, image, mask)
    
    image[:,:,:] = (1-alpha)*image + alpha*colored_image

def apply_closing(mask, kernel):
    '''
    mask - a numpy array with shape (n,m) and type np.bool
    kernel - a numpy array with shape (l,l) and type np.bool

    return - a numpy array with shape (n,m) and type np.bool
    '''
    radius = kernel.shape[0] / 2

    #pad by the radius on all sides so that the borders of the mask are not removed by the closing operation
    mask = np.pad(mask, radius, mode='constant')
    mask = morphology.binary_closing(mask, kernel)

    #remove the padding
    mask = mask[radius:-radius, radius:-radius]
    
    return mask
        
