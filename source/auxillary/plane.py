import numpy as np

class Plane(object):
    '''
    This represents a Plane in 3D space. It stores both the subspace containing the plane, as well as a coordinate system.
    It has methods that allow one to transfer from plane coordinates to world coordinates.
    '''

    def __init__(self, plane_origin, plane_x_axis, plane_y_axis):
        '''
        plane_origin - a numpy array with length 3. It is a point on the plane
        plane_x_axis - a numpy array with length 3 and norm 1. It represents a vector in world coordinates such that
                       plane_origin + s*plane_x_axis is containing in the plane for all s
        plane_y_axis - a numpy array with length 3 and norm 1 that is perpendicular to plane_x_axis. 
                       It represents a vector in world coordinates such that
                       plane_origin + t*plane_y_axis is containing in the plane for all t

        The map from plane coordinates (x,y) to world coordinates (X,Y,Z) is: 
        (x,y) -> plane_origin + x*plane_x_axis + y*plane_y_axis 
        '''
        self._plane_origin = plane_origin
        self._plane_x_axis = plane_x_axis / np.linalg.norm(plane_x_axis)
        self._plane_y_axis = plane_y_axis / np.linalg.norm(plane_y_axis)

        #C is a matrix whose columns are the axes of the plane
        self._C = np.vstack([self._plane_x_axis, self._plane_y_axis]).transpose()

    def __repr__(self):
        return 'Plane: [origin=%s, x_axis=%s, y_axis=%s]' % (self._plane_origin, self._plane_x_axis, self._plane_y_axis)
    
    def get_parametric_representation(self):
        '''
        return a numpy array with length 4: [a b c d] such that a point (x,y,z) is in the plane iff ax+by+cz+d = 0
        '''
        #get a vector that is normal to the plane
        n = np.cross(self._plane_x_axis, self._plane_y_axis)
        n /= np.linalg.norm(n)
        
        #evaluate it on the origin
        d = -np.dot(n, self._plane_origin)

        #if n=[a,b,c] then we now know that for any point on the plane, ax + by + cz + d = 0
        return np.append(n, d)

    def plane_positions_to_world_coordinates(self, plane_positions):
        '''
        plane_positions - a numpy array with shape (n,2). It represents a list of points in plane coordinates.
        return - a numpy array with shape (n,3). It represents the list of points in world coordinates
        '''
        return self._plane_origin + (plane_positions[:,:1]*self._plane_x_axis) + (plane_positions[:,1:]*self._plane_y_axis)

    def plane_position_to_world_coordinates(self, plane_position):
        '''
        plane_position - a numpy array with shape (2,). It represents a point in plane coordinates.
        return - a numpy array with shape (3,). It represents the point in world coordinates
        '''
        return self.plane_positions_to_world_coordinates(plane_position.reshape((1,2)))[0]
    
    def compute_line_intersection(self, world_line):
        '''
        world_line - a numpy array with shape (2,4) which represents two lines in world coords.

        return - a numpy array with shape (2,) which represents the intersection of the line and plane in plane coords.

        Let [a b c d] represent the line aX + bY + cZ + d = 0 in world coordinates. 
        Let the plane origin be [p0, p1, p2], the x axis be [u0, u1, u2] and the y axis be [v0, v1, v2].

        Then, the point [s, t] in plane coordinates corresponds to the point:
        p + s*u + t*v = [p0 + s*u0 + t*v0, p1 + s*u1 + t*v1, p2 + s*u2 + t*v2]

        So the line imposes the following relation on s and t:
        [a,b,c].p + s*[a,b,c].u + t*[a,b,c].v + d = 0  => s*[a,b,c].u + t*[a,b,c].v + (d + [a,b,c].p) = 0
        
        The two lines gives us two such equations, and we solve them for s and t.
        Specifically, let L denote the 2x3 matrix of line coefficients [a,b,c], let d denote the length 2 vector of the d 
        coefficients and let C denote the 3x2 matrix whose columns are u and v.
        Then, we can write the equations for s and t as: A*[s,t]^T = y where:
        A = M * C and
        y = -((M * p) + d)
        '''        
        #M is a 2x3 matrix and d is a length 2 vector
        M = world_line[:,:3]
        d = world_line[:,3]
        
        #A is a 2x2 matrix and y is a length 2 vector
        A = np.matmul(M, self._C)
        y = -(np.matmul(M, self._plane_origin) + d)

        #the plane coordinates (s,t) should satisfy: A*[s,t]^T = y
        plane_coords = np.linalg.lstsq(A, y)[0]
        return plane_coords

    def compute_lines_intersection(self, world_lines):
        '''
        world_lines - a numpy array with shape (n,2,4). It represents n world lines.
        return - a numpy array with shape (n,2). 
           It represents the intersection points of the lines with the plane in plane coods.

        See the documentation for compute_line_intersection
        '''
        #M is a np array with shape (n,2,3) and d is a np array with shape (n,2)
        M = world_lines[:,:,:3]
        d = world_lines[:,:,3]
        
        #A is a numpy array with shape (n,2,2). It is obtained by multiplying each of the matrices in M by the 3x2 matrix C
        #y is a numpy array with shape (n,2). It is obtained by multiplying each of the matrices in M by the origin
        A = np.matmul(M, self._C)
        y = -(np.matmul(M, self._plane_origin) + d)

        #the plane coordinates (s,t) should satisfy: A*[s,t]^T = y
        #invert the matrices in A
        A_inv = np.linalg.inv(A)
        plane_coords = np.matmul(A_inv, y.reshape((-1,2,1))).reshape((-1,2))
        return plane_coords

