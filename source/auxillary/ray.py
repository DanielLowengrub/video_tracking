import numpy as np

class Ray(object):
    '''
    This class represents a half open interval [a,infty) or (-infty,a]
    '''

    def __init__(self, a, direction):
        '''
        a - a float
        direction - a float. 1 or -1
        
        construct the interval [a,infty) if direction==1, and (-infty,a] if direction==-1
        '''

        self._a = a
        self._direction = direction

    def __str__(self):
        if self._direction > 0:
            return '[%s, oo)' % self._a
        else:
            return '(-oo, %s]' % self._a

    def __repr__(self):
        return str(self)
    
    def __hash__(self):
        return hash((self._a, self._direction)
    
    def contains_point(self, point):
        return self._direction * np.sign(point - self._a)) >= 0
    
    def get_copy(self):
        return Ray(self._a, self._direction)
        
    def end_point(self):
        return self._a

    def direction(self):
        return self._direction

    def has_overlap(self, other):
        '''
        other - a Ray
        Return True if the rays overlap
        '''
        if self._direction == other._direction:
            return True

        #we are intersecting  (-infty,other._a] with [self._a,infty)
        if self._direction > other._direction:
            return self._a < other._a

        #we are intersecting  (-infty,self._a] with [other._a,infty)
        else:
            return other._a < self._a

    def has_overlap_with_interval(self, interval):
        '''
        interval - an Interval
        return - True iff this ray overlaps with the interval
        '''
        #if we are intersecting [self._a, infty) with [interval._a, interval._b)
        if self._direction == 1:
            return self._a < interval.last_element()

        #if we are intersecting (-infty, self._a] with [interval._a, interval._b)
        else:
            return interval.first_element() < self._a
        
    def can_merge(self, other):
        '''
        Return True if we can merge this ray with the other one.
        '''
        return self._direction == other._direction

    def can_merge_with_interval(self, interval):
        '''
        interval - an Interval object
        return - True iff we can merge this ray with the interval
        '''
        #if we are merging [self._a, infty) with [interval._a, interval._b)
        if self._direction == 1:
            return self._a <= interval.last_element()

        #if we are merging (-infty, self._a] with [interval._a, interval._b)
        else:
            return interval.first_element() <= self._a

    def contains_ray(self, other):
        '''
        return True iff this interval contains the other one.
        '''

        return (self._a <= other._a) and (other._b <= self._b)

    def contains_interval(self, interval):
        '''
        interval - an Interval
        return - True iff this ray contains the interval
        '''
        #if we are comparing [self._a, infty) to [interval._a, interval._b)
        if self._direction == 1:
            return self._a <= interval.last_element()

        #if we are comparing (-infty, self._a] to [interval._a, interval._b)
        else:
            return interval.last_element() <= self._a

    def get_overlap(self, other):
        '''
        return - the ray obtained by intersecting this interval with the other one.
        We assume that the user has checked that there is indeed an intersection using has_overlap(other)
        '''
        #if we are computing the overlap of [self._a, infty) and [other._a, infty)
        if self._direction == 1:
            a = max(self._a, other._a)

        #if we are computing the overlap of (-infty, self._a] and (-infty, other._a]
        else:
            a = min(self._a, other._a)

        return Ray(a, self._direction)

    def get_overlap_with_interval(self, other):
        '''
        return - the interval obtained by intersecting this ray with the interval
        We assume that the user has checked that there is indeed an intersection using has_overlap_with_interval(interval)
        '''
        #if we are computing the overlap of [self._a, infty) and [interval._a, interval._b)
        if self._direction == 1:
            a = max(self._a, interval.first_element())
            b = interval.last_element()
            
        #if we are computing the overlap of (-infty, self._a] and [interval._a, interval._b)
        else:
            a = interval.first_element()
            b = min(self._a, interval.last_element())

        return Interval(a,b)

    def merge(self, other):
        '''
        Return a new ray which is obtained by merging this one with the other one.
        If there is no overlap between the rays, an exception is raised
        '''

        if not self.can_merge(other):
            raise ValueError('We can only merge rays that have an overlap')

        #if we are merging [self._a, infty) and [other._a, infty)
        if self._direction == 1:
            a = min(self._a, other._a)

        #if we are merging (-infty, self._a] and (-infty, other._a]
        else:
            a = max(self._a, other._a)

        return Ray(a, self._direction)

    def get_merge_with_interval(self, interval):
        '''
        return - the ray obtained by merging this ray with the interval
        If there is no overlap between the ray and interval, an exception is raised
        '''
        if not self.can_merge_with_interval(interval):
            raise ValueError('We can only merge ray and intervals that have an overlap')
        
        #if we are merging [self._a, infty) and [interval._a, interval._b)
        if self._direction == 1:
            a = min(self._a, interval.first_element())
            
        #if we are merging (-infty, self._a] and [interval._a, interval._b)
        else:
            a = max(self._a, interval.last_element())

        return Ray(a,self._direction)

    def ray_dilation(self, dilation_radius):
        '''
        dilation_radius - a float
        return a new Ray that has been dilated by the amount: dilation_radius
        '''
        return Ray(self._a - dilation_radius*self._direction, self._direction)

    def ray_erosion(self, erosion_radius):
        '''
        erosion_radius - a float
        return a new Ray that has been eroded by the amount: dilation_radius
        '''
        return Ray(self._a + dilation_radius*self._direction, self._direction)
