import numpy as np

"""
This module contains commands to create 3D rotation matrices. We use the following coordinate system:
      Z               
      |               
      |____ X         
      /               
     /                
    Y
"""
def rot_x(theta):
    '''
    theta - a float. represents an angle in radians
    returns the rotation matrix that rotates theta radians around the x axis in the clockwise direction.
    E.g, if theta=pi/2, it fixes X, sends Y to Z and Z to -Y.
    '''
    return np.array([[1, 0,              0            ],
                     [0, np.cos(theta), -np.sin(theta)],
                     [0, np.sin(theta),  np.cos(theta)]])

def rot_z(theta):
    '''
    theta - a float. represents an angle in radians
    returns the rotation matrix that rotates theta radians around the z axis in the clockwise direction.
    E.g, if theta=pi/2 it fixes Z, sends X to Y and Y to -X.
    '''
    return np.array([[np.cos(theta), -np.sin(theta), 0],
                     [np.sin(theta),  np.cos(theta), 0],
                     [0,              0,             1]])
