import numpy as np
import cv2

#different combinations of channels we will use
HS = 0 # Hue Saturation
BGR = 1 #Blue Green Red

class PixelGMM(object):
    '''
    The goal of the PixelGMM class is to run a gmm algorithm on the values of pixels in an image.
    This is used primarily to find regions of an image that have very similar pixel values.
    '''

    def __init__(self, num_labels, channels = HS):
        '''
        num_labels - an integer. This is the number of gaussians we use to model the image.
        channels - this can be HS or BGR
        '''

        self._num_labels = num_labels
        self._channels = channels

        #this is the EM we use to calculate the GMM
        self._em = cv2.EM(num_labels, cv2.EM_COV_MAT_GENERIC)
        self._image = None
        self._mask = None

        #this map has the same height and width as the image. At each point, it's value corresponds to
        #the label it was given by the GMM algorithm.
        #it has type np.int32
        self._label_map = None
        
    def _get_samples(self):
        '''
        Get a list of samples to feed to the GMM based on the image and mask.

        After converting the image to the specified channels, we then return a list of pixel values
        by running through the pixels of the image row by row, and collecting the values of all
        pixels in the mask.
        '''

        if self._channels == HS:
            image_hsv = cv2.cvtColor(self._image, cv2.COLOR_BGR2HSV)
            hue_and_saturation_values = image_hsv[:, :, 0:2][self._mask > 0]
            return np.float32(hue_and_saturation_values)

        elif self._channels == BGR:
            bgr_values = self._image[self._mask > 0]
            return np.float32(hue_and_saturation_values)


    def _build_label_map(self, sample_labels):
        '''
        sample_labels - the labels that the em gave the masked pixels. it is a list of integers, one per pixel.

        Populate the label map. By this we mean that at the pixel (n,m) in the label map we put the value:
        * if mask[n][m] = 0, put -1
        * otherwise, put the label that was given to this pixel by the em algorithm
        '''
        label_map_shape = self._image.shape[:2]
        self._label_map = np.ones(label_map_shape, np.int32)

        #initialize everything to -1
        self._label_map = -1 * self._label_map

        #we now label the pixels that are in the masked region.
        #these are lists of all the x/y coordinates of pixels with a nonzero mask value
        mask_x_coords, mask_y_coords = np.nonzero(self._mask)
        num_coords = len(mask_x_coords)
        
        #for each of these pixels, record their label
        for i in range(num_coords):
            x = mask_x_coords[i]
            y = mask_y_coords[i]
            label = sample_labels[i][0]

            self._label_map[x][y] = label

        return

    def _get_dense_labels(self, max_covariance):
        '''
        Return a list of all of the labels whose covaraince is less than max_covariance.
        '''
        #print 'getting dense labels...'
        
        dense_labels = []
        covs = self._em.getMatVector('covs')

        for label, cov in enumerate(covs):
            s, u, vt = cv2.SVDecomp(cov)
            min_cov = np.min(np.sqrt(s) * 3.0)

            #print 'min cov: ', min_cov
            
            if min_cov < max_covariance:
                dense_labels.append(label)

        return dense_labels

    def train(self, image, mask=None):
        '''
        Fit a gmm model to the masked parts of the image.
        '''

        self._image = image
        self._mask = mask

        if mask is None:
            self._mask = np.ones(image.shape[:2], np.float64)
            
        #first get samples from the image
        samples = self._get_samples()

        #now train the gmm on these samples
        sample_labels = self._em.train(samples)[2]

        #we use the labels to construct a map showing the label of each pixel
        self._build_label_map(sample_labels)

        return

    def get_label_map(self):
        '''
        Return a numpy array whose shape is (n,m) and with type np.int32.
        At position (i,j), the value is -1 if that pixel is not in the mask. Otherwise
        the value is equal to the label of that pixel.
        '''

        return self._label_map
    
    def get_mask_of_pixels_in_dense_cluster(self, max_covariance):
        '''
        Return a mask containing all of the pixels whose label lies in a cluster
        with a covariance that is smaller than the specified one.
        '''
        boolean_output_mask = np.zeros(self._mask.shape, np.bool)
        
        #first get a list of the labels with a small enough covariance
        chosen_labels = self._get_dense_labels(max_covariance)

        #for each of the chosen labels, mark the pixels in the image with this label
        for chosen_label in chosen_labels:
            boolean_output_mask = np.bitwise_or(boolean_output_mask, self._label_map == chosen_label)

        return np.float64(boolean_output_mask)
                                    
