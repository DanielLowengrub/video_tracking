import numpy as np
import cv2
from .interval_on_shape import IntervalOnShape
from ..interval import Interval
from ...auxillary import planar_geometry

DEBUG = False

class IntervalOnLine(IntervalOnShape):
    '''
    This implementation of IntervalOnShape represents an interval on a line.
    The interval is stored as an Interval object.
    '''

    def __init__(self, line_with_position, first_position, last_position):
        '''
        line_with_position - a LineWithPosition object
        first_position, last_position - floats
        ray_direction - a float. The default is 0. Otherwise, it is 1 or -1

        if ray_direction is 0, the we build the interval [first_position, last_position).
        otherwise, we build a ray that starts at first_position, and goes in the given direction
        '''
        super(IntervalOnLine, self).__init__(line_with_position)
        
        self._interval = Interval(first_position, last_position)

    def __hash__(self):
        return hash(self._interval)

    def __repr__(self):
        return str(self)
    
    def __str__(self):
        return str(self._interval)
    
    @classmethod
    def load_from_shape_and_interval_array(cls, line_with_position, interval_array):
        return cls(line_with_position, *interval_array)
    
    def save_interval_to_array(self):
        return np.array([self._interval.first_element(), self._interval.last_element()])
    
    def length(self):
        return self._interval.length()

    def has_overlap(self, other):
        return self._interval.has_overlap(other._interval)

    def get_overlap(self, other):
        if not self.has_overlap(other):
            raise ValueError('tried to compute overlap of two shapes that do not overlap')

        interval = self._interval.get_overlap(other._interval)
        return [IntervalOnLine(self._shape_with_position, interval.first_element(), interval.last_element())]
    
    def can_merge(self, other):
        return self._interval.can_merge(other._interval)
    
    def contains_interval(self, other):
        return self._interval.contains_interval(other._interval)
    
    def merge(self, other):
        if not self.can_merge(other):
            raise ValueError('Can not merge with a shape if there is no overlap')

        interval = self._interval.merge(other._interval)
        
        return IntervalOnLine(self._shape_with_position, interval.first_element(), interval.last_element())

    def compute_middle_position(self):
        return (self._interval.first_element() + self._interval.last_element()) / 2.0

    def get_mask(self, image, padding, thickness):
        mask = np.zeros(image.shape[:2], np.float32)
        #if the 2*padding is longer than the line, then there is nothing to draw
        if 2*padding > self.length():
            return mask
        
        #we draw a rectangle surrounding the interval
        #we first must get opposing corners of this rectangle
        line = self._shape_with_position.get_shape()
        a,b,c = line.get_parametric_representation()
        normal = np.array([a,b])
        
        middle_left = self._shape_with_position.compute_absolute_position(self._interval.first_element() + padding)
        top_left = middle_left + thickness*normal
        bottom_left = middle_left - thickness*normal
        
        middle_right = self._shape_with_position.compute_absolute_position(self._interval.last_element() - padding)
        top_right = middle_right + thickness*normal
        bottom_right = middle_right - thickness*normal
        
        rectangle_points = np.int32(np.vstack([bottom_left, bottom_right, top_right, top_left]).reshape((-1,1,2)))

        cv2.fillConvexPoly(mask, rectangle_points, 1.0)
        #cv2.rectangle(mask, tuple(np.int32(top_left).tolist()), tuple(np.int32(bottom_right).tolist()), 1.0, -1)

        return mask

    def _compute_endpoint_signs(self, H, anchor_sign):
        '''
        H - a numpy array with shape (3,3) representing a homography
        anchor_sign - an integer. either 1 or -1. It represents the side of the line at infty that we want to use.
        
        return - a numpy array with shape (2,) and type np.int32.
           for a point p, the "sign of p" is defined to be 
              1 if dot(p,line_at_infty)*anchor_sign > EPSILON, 
              0 if dot(p,line_at_infty)*anchor_sign \in (-EPSILON, EPSILON)
             -1 if dot(p,line_at_infty)*anchor_sign < -EPSILON
        '''
        if DEBUG:
            print 'computing endpoint signs of the interval: ', self
            print 'shape with position: ', self._shape_with_position
            print 'line at infinity: ', H[2]
            print 'anchor sign: ', anchor_sign
            
        line_at_infty = H[2]
        
        #compute the positions of the endpoint of the interval
        positions = np.array(self._interval.get_tuple())
        end_points = self._shape_with_position.compute_absolute_positions(positions)
        projective_end_points = planar_geometry.affine_to_projective_points(end_points)
        end_point_vals = np.matmul(line_at_infty, projective_end_points.transpose())

        if DEBUG:
            print 'positions: ', positions
            print 'end points:\n', end_points
            print 'end_point_vals: ', end_point_vals
            
        end_point_signs = np.zeros(len(end_point_vals), np.int32)
        end_point_signs[end_point_vals*anchor_sign >  planar_geometry.EPSILON] =  1
        end_point_signs[end_point_vals*anchor_sign < -planar_geometry.EPSILON] = -1

        if DEBUG:
            print 'end_point_signs: ', end_point_signs
            
        return end_point_signs

    def _compute_target_points_of_half_interval(self, H, target_region, end_point_signs):
        '''
        H - a numpy array with shape (3,3) and type np.float32
        target_region - a numpy array with shape (2,2)
        end_point_signs - a numpy array with shape (2,) and type np.int32.
           It represents the signs of the endpoints wrt to the line at infinity. 
           Exactly one of them must be equal to 1.

        If end_points[0]==1, then we consider the half interval [interval._a, oo).
        Otherwise, we consider the half interval (-oo, interval._b)

        We map the half interval to the image and intersect it with the image region
        '''
        #build a ray out of the interval
        positions = np.array(self._interval.get_tuple())
        points = self._shape_with_position.compute_absolute_positions(positions)
            
        if end_point_signs[0] == 1 and end_point_signs[1] < 1:
            origin = points[0]
            direction = points[1] - origin

        elif end_point_signs[0] < 1 and end_point_signs[1] == 1:
            origin = points[1]
            direction = points[0] - origin

        else:
            raise RuntimeError('exactly one of the endpoints must have a positive sign')

        direction /= np.linalg.norm(direction)
        source_ray = np.vstack([origin, direction])

        #map the ray to the image
        target_ray = planar_geometry.apply_homography_to_ray(H, source_ray)
        target_origin, target_direction = target_ray

        #intersect the target ray with the image region
        intersection_points = planar_geometry.ray_region_intersection(target_ray, target_region)

        if DEBUG:
            print 'finding target points from ray...'
            print '   source ray: origin=%s, direction=%s' % (origin, direction)
            print '   target ray: origin=%s, direction=%s' % (target_origin, target_direction)
            print '   intersection points:\n', intersection_points
            
        if planar_geometry.point_in_region(target_origin, target_region) and len(intersection_points)==1:
            return np.vstack([target_origin, intersection_points[0]])

        elif not planar_geometry.point_in_region(target_origin, target_region) and len(intersection_points)==2:
            return intersection_points

        else:
            return None
    
    def apply_homography(self, H, target_line_with_position, target_region, anchor_sign):
        '''
        H - a numpy array with shape (3,3) and type np.float32 representing a homography matrix
        target_line_with_position - a LineWithPosition object
        
        return - an IntervalOnLine object whose underlying LineWithPosition is target_line_with_position
        We assume that target_line_with_position is the image of self._shape_with_position under H.
        '''
        if DEBUG:
            print 'applying homography to the interval: ', self._interval
            print '   source line with position: ', self._shape_with_position
            print '   target line with position: ', target_line_with_position

        #determine which side of the line at infinity the points are on wrt the anchor
        end_point_signs = self._compute_endpoint_signs(H, anchor_sign)

        #if there are no end points that are on the positive side of the line at infty
        if not np.any(end_point_signs == 1):
            return None

        #if both of the end points are on the positive side
        if np.all(end_point_signs == 1):
            #apply the homography to the endpoints of the interval
            positions = np.array(self._interval.get_tuple())
            points = self._shape_with_position.compute_absolute_positions(positions)
            target_points = planar_geometry.apply_homography_to_points(H, points)
            target_points = planar_geometry.line_segment_region_intersection(target_points, target_region)
            
        #only one of the points is on the positive side
        else:
            target_points = self._compute_target_points_of_half_interval(H, target_region, end_point_signs)

        if target_points is None:
            return None

        if DEBUG:
            print '   target points: ', target_points
            
        #use the target points to create an interval on the target shape
        target_interval = target_line_with_position.get_interval_on_shape(*target_points)
        
        return target_interval

    def snap_to_shape(self, new_line_with_position):
        '''
        line_with_position - a LineWithPosition object
        return - an IntervalOnLine object whose underlying shape in new_line_with_position
        '''
        positions = np.array(self._interval.get_tuple())
        points = self._shape_with_position.compute_absolute_positions(positions)
        
        #use the points to create an interval on the new shape
        new_interval = new_line_with_position.get_interval_on_shape(*points)

        return new_interval

    def interval_dilation(self, dilation_radius):
        '''
        dilation_radius - a float
        return a new IntervalOnLine whose interval has been dilated on both ends by the amount: dilation_radius
        '''
        dilated_interval = self._interval.interval_dilation(dilation_radius)
        endpoints = dilated_interval.get_tuple()
        return IntervalOnLine(self._shape_with_position, endpoints[0], endpoints[1])

    def interval_erosion(self, erosion_radius):
        '''
        erosion_radius - a float
        return a new IntervalOnLine whose interval has been eroded on both ends by the amount: eroded_radius.
        if the interval disappears after the erosion, return None
        '''
        eroded_interval = self._interval.interval_erosion(erosion_radius)
        
        if eroded_interval is None:
            return None

        endpoints = eroded_interval.get_tuple()
        return IntervalOnLine(self._shape_with_position, endpoints[0], endpoints[1])

    @classmethod
    def fill_interval_gaps(cls, line_with_position, intervals):
        '''
        line_with_position - a LineWithPosition object
        intervals - a list of IntervalOnLine objects. They represent intervals on the LineWithPosition
        return - an IntervalOnLine object. It represents the result of filling the gaps between the input intervals.
        '''
        first_element = min(iol._interval.first_element() for iol in intervals)
        last_element =  max(iol._interval.last_element()  for iol in intervals)

        return cls(line_with_position, first_element, last_element)
    
