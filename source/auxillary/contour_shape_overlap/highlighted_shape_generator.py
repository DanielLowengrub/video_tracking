from .highlighted_shape import HighlightedShape
from .interval_on_shape import IntervalOnShape

DEBUG = False

class HighlightedShapeGenerator(object):
    '''
    This class is in charge of building HighlightedShape objects.

    In general, it does this based on the input of:
       * a ShapeWithPosition object
       * a specified thickness of the shape
       * a list of contours

    Then, it proceeds as follows:
       1) For each contour, get a list of IntervalOnShape objects containing this contour
       2) Merge the intervals from step 1
       3) Use the intervals in step 2 to create a HighlightedShape object
    '''
            
    def generate_highlighted_shape(self, shape_with_position, thickness, contours):
        '''
        shape_with_position - either a LineWithPosition object or an EllipseWithPosition object, depending on _shape_type
        thickness - a float
        contours - a list of Contour objects

        Return a tuple (HighlightedShape object, list of contour masks)
        '''

        if DEBUG:
            print 'generating highlighted shape...'
            print 'shape: ', shape_with_position
            
        contour_masks = []
        all_intervals_on_shape = []

        for contour in contours:
            if DEBUG:
                print '   computing the interval and contour mask...'
                
            intervals_on_shape, contour_mask = shape_with_position.find_intervals(contour, thickness)
            all_intervals_on_shape += intervals_on_shape
            contour_masks.append(contour_mask)

        merged_intervals_on_shape = IntervalOnShape.merge_intervals(all_intervals_on_shape)
        highlighted_shape = HighlightedShape(shape_with_position, thickness, merged_intervals_on_shape)

        return highlighted_shape, contour_masks
