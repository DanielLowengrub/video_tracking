import numpy as np
#TODO:
#Note that since we do not allow intervals that go all the way around the loop, there is a well defined order and distance between points on an interval
#We should implement these.
class IntervalOnLoop(object):
    '''
    This is the base class for an interval [a,b] on a circle of values ranging from 0 to N (i.e, the quotient R/NR). In particular, it is possible that a > b.
    In that case, [a,b] would correspond to all values x such that either a<=x<N or 0<=x<=b.

    In general, we do not allow degenerate intervals of the form [a,a].
    We *do* however allow the interval [0,N] which represents the full loop.
    We do *not* allow [a,N] for a=/=0 since it would be equivalent to [a,0].

    In summery, the allowed values for a,b are:
    1) 0 <= a < b < N corresponding to the interval [a,b]
    2) 0 <= b < a < N corresponding to the interval [0,b] \union [a,N]
    3) a=0, b=N       corresponding to the interval [0,N]

    Coordinate Systems:
    We use two coordinate systems to represent points on the loop
    * Absolute coordinates - this is a number in the half open interval [0,N)
    * Relative coordinates - this is a number in the interval [0, length of the interval].

    In relative coordinates, 0 denotes the first point in the interval, and x denotes the point whose global coordinates are (first point on interval + x (mod N)).
    '''

    def __init__(self, a, b, max_value):
        '''
        a,b,max_value - numbers such that 0 <= a,b <= max_value and a =/= b

        We construct the interval [a,b] on the circle R/NR.
        '''

        if (a > 0 and max(a,b) >= max_value) or (a < 0) or (b < 0):
            raise ValueError('[%s,%s] is not a valid interval on a loop.' % (str(a),str(b)))

        if a == b:
            raise ValueError('[%s,%s] is a degenerate interval' % (str(a),str(b)))
        
        self._a = a
        self._b = b
        self._N = max_value

        self._full_loop = (a==0 and b==self._N)
        self._standard = (not self._full_loop) and (self._a <= self._b)
        
    @classmethod
    def build_full_loop(cls, max_value):
        '''
        Return an interval corresponding to the entire loop
        '''

        full_loop = cls(0,max_value,max_value)
        return full_loop

    def __repr__(self):
        return str(self)
    
    def __str__(self):
        if self._is_full_loop():
            return '[0, %s]' % str(self._N)
                                   
        return '[%s, %s]' % (str(self._a), str(self._b))

    def _is_standard(self):
        '''
        Return true if this interval is [a,b].
        Return false if it is [a,N) union [0,b]
        '''
        return self._standard

    def _is_full_loop(self):
        '''
        return true if this interval spans the entire loop.
        '''
        return self._full_loop

    def is_full_loop(self):
        '''
        return true if this interval spans the entire loop.
        '''
        return self._is_full_loop()
    
    def first_element(self):
        return self._a

    def last_element(self):
        return self._b

    def get_standard_interval_tuples(self):
        '''
        Return a list of tuples of the form (a,b) where a<b, such that the union of these intervals is this interval 
        '''

        if self._is_full_loop() or self._is_standard():
            return [(self._a, self._b)]

        return [(0,self._b), (self._a, self._N)]
    
    def get_slice(self, array):
        '''
        This only works if this interval is an integer valued interval on a loop with integer length
        array - a numpy array with length self._N
        '''

        if self._is_full_loop():
            return array
        
        if self._is_standard():
            return array[self._a : self._b + 1]

        else:
            return np.concatenate([array[self._a :], array[:self._b+1]], axis=0)
        
    def contains_point(self, x):
        '''
        x - a number representing a point on the loop in global coordinates
        Return True iff x is in the interval where 0<= x < N
        '''

        if not 0 <= x < self._N:
            raise ValueError('The given value is not on the loop')

        #if the interval is the entire loop, return true
        if self._is_full_loop():
            return True
        
        #if the interval is a "standard" interval, then just check if x is between a and b
        if self._is_standard():
            return (self._a <= x <= self._b)

        #otherwise, since a > b, this interval contains the values [a,N) \union [0,b]
        return (self._a <= x < self._N) or (0 <= x <= self._b)


    def get_length(self):
        '''
        Return the length of this interval
        '''
        if self._is_full_loop():
            return self._N
        
        if self._is_standard():
            return self._b - self._a

        else:
            return (self._N - self._a) + self._b

    def _is_valid_relative_position(self, relative_position):
        '''
        relative_position - a position on the interval in relative coordinates
        '''

        return 0 <= relative_position <= self.get_length()
    
    def get_absolute_position(self, relative_position):
        '''
        relative_position - a position on the interval in relative coordinates
        return this position in global coordinates
        '''

        if not self._is_valid_relative_position(relative_position):
            raise ValueError('%f is not a valid relative position' % relative_position)

        x = self._a + relative_position

        if x < self._N:
            return x
        else:
            return x - self._N
        
    def get_sub_interval(self, relative_first_element, relative_last_element):
        '''
        relative_start, relative_end - numbers
        These denote positions on the interval in relative coordinates
        Return an new IntervalOnLoop which starts at relative_start and ends at relative_end
        '''

        if not relative_first_element < relative_last_element:
            raise ValueError('relative_start must be smaller than relative_end')

        absolute_first_element = self.get_absolute_position(relative_first_element)
        absolute_last_element = self.get_absolute_position(relative_last_element)

        return IntervalOnLoop(absolute_first_element, absolute_last_element, self._N)
    
    def has_overlap(self, other):
        '''
        Check if this interval has an overlap with the other interval.
        If the two intervals do not have the name max_value, we raise an exception.

        Note that if two intervals have an overlap then they will have a non degenerate intersection.
        For example, [0,2] and [2,3] do not overlap.
        '''

        if not self._N == other._N:
            raise ValueError('We can not compare intervals that are on loops of different sizes')

        return len(self.get_intersection(other)) > 0

    def contains_interval(self, other):
        '''
        Return true iff this interval contains the other one
        '''

        if not self._N == other._N:
            raise ValueError('We can not compare intervals that are on loops of different sizes')

        if self._is_full_loop():
            return True

        #if this interval is standard, then if the other one is contained in it it must be standard as well.
        #in this case, just check the end points
        if self._is_standard():
            if other._is_standard():
                return (self._a <= other._a) and (other._b <= self._b)
            else:
                return False

        #if this interval is not standard and the other one is, then check if [a',b']
        #is contained in either [a,N) or [0,b]
        if other._is_standard():
            return (self._a <= other._a) or (other._b <= self._b)

        #if both intervals are non standard, then [a',N) must be contained in [a,N) and [0,b'] must be contained in [0,b]
        return (self._a <= other._a) and (other._b <= self._b)
    
    def can_merge(self, other):
        '''
        Check if it is possible to merge this interval with the other interval.
        This is true if either the intervals overlap, or if they share a common endpoint
        '''

        if not self._N == other._N:
            raise ValueError('We can not compare intervals that are on loops of different sizes')

        #check if they share an endpoint
        endpoints = (self._a, self._b)
        if (other._a in endpoints) or (other._b in endpoints):
            return True

        return self.has_overlap(other)
    
    def get_intersection(self, other):
        '''
        Return the intersection of the two intervals as a tuple of intervals (since it is possible that the intersection is two intervals). 
        If the intervals do not overlap, we return an empty tuple.
        '''
        if not self._N == other._N:
            raise ValueError('We can not compare intervals that are on loops of different sizes')

        #if one of the intervals is the entire loop, return the other interval
        if self._is_full_loop():
            return (other,)
        
        if other._is_full_loop():
            return (self,)
        
        #if both intervals are standard, then we just intersect [a,b] with [a',b']
        if self._is_standard() and other._is_standard():
            a = max(self._a, other._a)
            b = min(self._b, other._b)
            if a < b:
                return (IntervalOnLoop(a, b, self._N),)
            else:
                return ()


        #if this interval is standard but the other one is not, return [a,b] \cap ([a',N) union [0,b'))
        if self._is_standard():
            intervals = []
            #first get the intersection of [a,b] with [a',N)
            a = max(self._a, other._a)
            if a < self._b:
                intervals.append(IntervalOnLoop(a, self._b, self._N))

            #now get the intersection of [a,b] with [0,b']
            b = min(self._b, other._b)
            if self._a < b:
                intervals.append(IntervalOnLoop(self._a, b, self._N))

            return tuple(intervals)

        #if the other interval is standard, flip the order of the intervals
        if other._is_standard():
            return other.get_intersection(self)

        #print 'both non standard'
        #finally, we deal with the case that both intervals are non-standard
        #this means that we are intersecting ([a,N) union [0,b]) and ([a',N) union [0,b])
        intervals = []

        #first get ([a,N)\cap[a',N))\union([0,b]\cap[0,b'])
        a = max(self._a, other._a)
        b = min(self._b, other._b)
        intervals.append(IntervalOnLoop(a,b,self._N))

        #now get the intersection [a,N) \cap [0,b']. This is only non empty if a < b'
        if self._a < other._b:
            intervals.append(IntervalOnLoop(self._a, other._b, self._N))

        #finally, get the intersection [0,b] \cap [a',N) which is non empty only if a' < b
        #note that if the previous intersection was non empty, then this one will be empty
        if other._a < self._b:
            intervals.append(IntervalOnLoop(other._a, self._b, self._N))

        return tuple(intervals)

    def merge(self, other):
        '''
        other - another IntervalOnLoop object. It should have an overlap with this one. Otherwise an exception is raised
        Return a new interval that is the union of this one and the other one.
        '''

        if not self.can_merge(other):
            raise ValueError('We can only merge intervals that have an overlap')

        #if one of the intervals is the full loop, return it
        if self._is_full_loop():
            return self
        
        if other._is_full_loop():
            return other

        #if both of the intervals are standard, we can merge them in the usual way
        if self._is_standard() and other._is_standard():
            a = min(self._a, other._a)
            b = max(self._b, other._b)
            return IntervalOnLoop(a, b, self._N)

        #if only this one is standard
        if self._is_standard() and (not other._is_standard()):
            #we split up the other interval into two pieces: [0,b'] and [a', N)
            if other._b > 0:
                interval_1 = IntervalOnLoop(0, other._b, self._N)
                can_merge_1 = self.can_merge(interval_1)

            else:
                interval_1 = None
                can_merge_1 = False
                
            interval_2 = IntervalOnLoop(other._a, 0, self._N)
            can_merge_2 = self.can_merge(interval_2)

            #print 'interval 1: ', str(interval_1)
            #print 'can merge 1: ', can_merge_1
            #print 'interval 2: ', str(interval_2)
            #print 'can merge 2: ', can_merge_2

            #if [a,b] has an overlap with both [0,b'] and [a',N)
            if  can_merge_1 and can_merge_2:
                if self._a == 0 and (self._b < other._a):
                    #we are of the form [0,b] and are merging with [0,b'] and [a',N) where b < a'
                    if self._b < other._a:
                        b = max(self._b, other._b)
                        return IntervalOnLoop(other._a, b, self._N)
                else:
                    return self.build_full_loop(self._N)

            #if [a,b] has an overlap with [0,b'] but not with [a',N), then the merged interval is [a', max(b,b')]
            if can_merge_1 and (not can_merge_2):
                a = other._a
                b = max(self._b, other._b)
                return IntervalOnLoop(a, b, self._N)

            #if [a,b] has an overlap only with [a',N), the merged interval is [min(a,a'), b']
            if (not can_merge_1) and can_merge_2:
                #if this interval is of the form [0,b] and we are merging it with [a',N)
                if self._a == 0:
                    if self._b < other._a:
                        return IntervalOnLoop(other._a, self._b, self._N)
                    else:
                        return IntervalOnLoop.build_full_loop(self._N)
                
                a = min(self._a, other._a)
                b = other._b
                return IntervalOnLoop(a, b, self._N)
            
        #if only the other one is standard
        if (not self._is_standard()) and other._is_standard():
            return other.merge(self)

        #if neither of them are standard. So we are merging
        #([0,b] union [a,N)) and ([0,b'] union [a',N))
        a = min(self._a, other._a)
        b = max(self._b, other._b)

        #if b >=a, then the union of the intervals in the entire loop
        if b >= a:
            return self.build_full_loop(self._N)

        return IntervalOnLoop(a, b, self._N)
            
    def interval_dilation(self, dilation_radius):
        '''
        dilation_radius - a float
        return a new IntervalOnLoop that has been dilated on both ends by the amount: dilation_radius
        '''
        if self._is_full_loop():
            return self.build_full_loop(self._N)
        
        #this is the length of the complement of this interval
        complement_length = self._N - self.get_length()

        #if the complement of the dilated interval will be empty
        if complement_length <= 2*dilation_radius:
            return self.build_full_loop(self._N)

        dilated_a = self._a - dilation_radius
        dilated_b = self._b + dilation_radius

        if dilated_a < 0:
            dilated_a += self._N

        if dilated_b >= self._N:
            dilated_b -= self._N

        return IntervalOnLoop(dilated_a, dilated_b, self._N)

    def interval_erosion(self, erosion_radius):
        '''
        erosionn_radius - a float
        return a new IntervalOnLoop that has been eroded on both ends by the amount: erosion_radius
        '''
        if self._is_full_loop():
            return self.build_full_loop(self._N)

        if 2*erosion_radius >= self.get_length():
            return None
        
        eroded_a = self._a + erosion_radius
        eroded_b = self._b - erosion_radius
        
        if eroded_a >= self._N:
            eroded_a -= self._N

        if eroded_b < 0:
            eroded_b += self._N

        return IntervalOnLoop(eroded_a, eroded_b, self._N)
