import numpy as np
from .interval_on_loop import IntervalOnLoop
from ..interval import IntervalList

class IntervalOnContour(object):
    '''
    This class represents an interval on a contour.
    Since a contour is a closed loop of points, we store the interval as an IntervalOnLoop
    '''

    def __init__(self, contour, first_index, last_index):
        '''
        contour - a Contour object representing the contour that this interval is on.
        start_index, end_index - integers

        We construct an interval starting contour[start_index] and ending at contour[end_index]
        '''

        self._contour = contour
        self._interval = IntervalOnLoop(first_index, last_index, len(contour.get_cv_contour()))

        self._points = self._compute_points()

    def __str__(self):
        return str(self._interval)
    
    @classmethod
    def from_point_mask(cls, contour, point_mask):
        '''
        point_mask - a numpy array of type np.bool and length (number of points in the contour)
        We return a list of IntervalOnContour objects, corresponding to the connected components of the true values in the point mask.
        '''

        #print 'constructing intervals from a point mask'
        #print 'point mask: ', point_mask
        #first find the connected components of the point mask
        intervals = IntervalList.from_boolean_array(point_mask).get_intervals()
        intervals = [interval for interval in intervals if interval.last_element() > interval.first_element()+1]
        
        #print 'we found the intervals: '
        #print [str(interval) for interval in intervals]
        
        #create an interval_on_contour object from each of the intervals
        #note that we subtract 1 from the last element in the interval since we want the last point in the interval to be on the contour.
        intervals_on_contour = [cls(contour, interval.first_element(), interval.last_element()-1) for interval in intervals]
                                

        #print 'trying to merge the first and last intervals...'
        #if the first interval contains the first point and the last interval contains the last point and these intervals are not equal
        if len(intervals)>2 and intervals[0].contains_point(0) and intervals[-1].contains_point(len(point_mask)-1):
            first_index = intervals[-1].first_element()
            last_index = intervals[0].last_element()
            merged_interval_on_contour = cls(contour, first_index, last_index-1)
            intervals_on_contour = intervals_on_contour[1:-1] + [merged_interval_on_contour]

            #print 'merged intervals. the new list of intervals is: ', [str(i) for i in intervals_on_contour]
            
        return intervals_on_contour

    def _compute_points(self):
        '''
        Return a numpy array of shape (number of points in contour interval, 2) and type np.int32
        '''
        all_points = self._contour.get_cv_contour().reshape((-1,2))
        return self._interval.get_slice(all_points)
            
    def get_points(self):
        '''
        Return a numpy array of shape (number of points in contour interval, 2) and type np.int32
        This represents the points that are in the contour interval.
        '''

        return self._points

    def get_absolute_index(self, relative_index):
        '''
        relative_index - an integer representing the index of a point on the interval
        Return the index of this point in the underlying contour
        '''
        return self._interval.get_absolute_position(relative_index)
    
    def get_sub_interval(self, relative_first_index, relative_last_index):
        '''
        relative_first_index, relative_last_index - integers
        
        These integers must satisfy
        0 <= first_index < last_index < number of points in the current interval

        We return a new IntervalOnContour containing the points self._points[sub_interval_first_index : sub_interval_last_index]
        '''

        sub_interval_on_loop = self._interval.get_sub_interval(relative_first_index, relative_last_index)
        return IntervalOnContour(self._contour, sub_interval_on_loop.first_element(), sub_interval_on_loop.last_element())
