import numpy as np
from .shape_with_position import ShapeWithPosition
from .interval_on_line import IntervalOnLine
from ..fit_points_to_shape.line import Line
from ...auxillary import planar_geometry
from ..interval import IntervalList

class LineWithPosition(ShapeWithPosition):
    '''
    This implements a Line together with information about the position of a point on the line.
    We define the closest point P on the line to the origin to have position 0.
    The positions of all other points Q are determined by projecting (Q-P) to the line.
    '''

    def __init__(self, line):
        '''
        line - a Line object
        '''

        super(LineWithPosition, self).__init__(line)

        #we now record the closest point to the origin
        self._origin = self._shape.get_closest_point_to_origin()

        #and also the normal vector pointing in the positive direction along the line
        self._positive_direction = self._shape.get_tangent_vector(self._origin)


    def __repr__(self):
        return '(LineWithPosition: origin=%s, positive_direction=%s)' % (self._origin, self._positive_direction)
    
    @classmethod
    def load_from_array(cls, array):
        '''
        array - a numpy array
        '''
        line = Line(*array.tolist())
        return cls(line)
        
    def _is_on_shape(self, points, thickness):
        '''
        points - a numpy array with shape (num points, 2)

        Return an array of length (num points) and type bool which indicates which of the points are within thickness of the shape
        '''
        distance_from_shape = self._shape.get_algebraic_distance_from_points(points)

        #print 'the distances of the points from the shape:'
        #print distance_from_shape
        
        return (distance_from_shape < thickness)

    def _find_intervals_in_position_sequence(self, positions):
        '''
        points - a list of positions

        Return a list of IntervalOnLine objects corresponding to the intervals on the line that contain these points.
        '''
        if len(positions) < 2:
            return []
        #print 'finding interval on line in position sequence: ', positions
        first_position = positions.min()
        last_position = positions.max()

        #print 'first position: ', first_position
        #print 'last position: ', last_position
        #print str((first_position, last_position))
        intervals = [IntervalOnLine(self, first_position, last_position)]
        
        return intervals

    def _find_subsequences_in_position_sequence(self, positions, position_mask):
        '''
        positions - a numpy array with length (num positions)
        position_mask - a numpy array with length (num_positions) and type np.float32

        Return a the list subsequences of the given list of positions such that any to adjacent positions are contained in the same subsequence.
        '''

        intervals = IntervalList.from_boolean_array(position_mask).get_intervals()
        return [interval.get_slice(positions) for interval in intervals if interval.first_element() < interval.last_element()]

    # def _find_intervals_in_point_sequence(self, points):
    #     '''
    #     points - a numpy array of shape (num points, 2)

    #     Return a list of IntervalOnLine objects corresponding to the intervals on the line that contain these points.
    #     '''

    #     #print 'finding intervals in the point sequence: '
    #     #print points
        
    #     #we simply compute the min and max of the positions
    #     positions = self.compute_positions(points)
    #     first_position = positions.min()
    #     last_position = positions.max()

    #     intervals = [IntervalOnLine(self, first_position, last_position)]

    #     #print 'we found the interval:'
    #     #print intervals[0]
        
    #     return intervals
    
    def compute_positions(self, points):
        '''
        point - a numpy array of shape (number of points, 2)

        Return a numpy array of length (number of points) which records the positions of each of the points on the shape
        '''

        #we first get the positions relative to the origin
        points = points - self._origin

        #we now compute the dot product of each of the points with _positive_direction
        points = points.transpose()
        positions = np.dot(self._positive_direction, points)

        return positions

    def compute_absolute_position(self, position_on_shape):
        '''
        position_on_shape - a float

        Return the global coordinates of the point on the shape at the given position
        '''

        #print 'computing the global coords of: ', position_on_shape
        #print 'origin = ', self._origin
        #print 'positive direction = ', self._positive_direction
        return self._origin + position_on_shape*self._positive_direction

    def compute_absolute_positions(self, positions_on_shape):
        '''
        positions_on_shape - a numpy array of length (num positions) and type np.float32

        Return a numpy array with shape (num points, 2) containing the global coordinates of the points on the shape at the given positions
        '''

        return self._origin + positions_on_shape.reshape((-1,1))*self._positive_direction


    def get_intervals_between_positions(self, positions):
        '''
        positions - a numpy array with length (num positions)

        Return a list of intervals connecting each pair of consecutive points
        '''
        
        if len(positions) < 2:
            return []
        
        positions = np.sort(positions)
        return [IntervalOnLine(self, start_position, end_position) for start_position, end_position in zip(positions[:-1], positions[1:])]
    
    def position_sampling(self, region, step_size=1.0):
        '''
        region - a numpy array [[x start, y start], [x end, y end]]
        step_size - a float in pixel units
        
        Return a list of positions on the shape that starts at the first position that is in the region, and ends at the last one.
        If all of the shape is in the region, the positions cover the entire shape.
        The distance in pixels between the points corresponding to consecutive positions is step_size.

        Note that if the shape enters and exits the region multiple times, not all positions will correspond to points in the region.
        '''

        #we find the intersection points of the line with the lines defining the region that are contained in the region.
        #if the line intersects the region there will be two. Otherwise there will be none.
        boundary_lines = planar_geometry.get_region_boundary_lines(region)
        intersection_points = [planar_geometry.affine_line_line_intersection(self._shape, line) for line in boundary_lines]
        #print 'intersection points: ', intersection_points
        #print 'region: ', region
        intersection_points = [point for point in intersection_points if (point is not None) and planar_geometry.point_in_region(point, region)]

        #print 'intersection points: ', intersection_points
        
        #if there are no intersection points, return an empty list
        if len(intersection_points) == 0:
            return []

        #otherwise, find the positions of the points
        intersection_positions = sorted(self.compute_positions(np.vstack(intersection_points)))

        #print 'intersection positions: ', intersection_positions
        
        positions = np.arange(intersection_positions[0], intersection_positions[1], step_size)

        return positions

    def apply_homography(self, H):
        '''
        H - a numpy array with shape (3,3) and type np.float64. It represents a homography
        return - a LineWithPosition object
        '''
        target_line = planar_geometry.apply_homography_to_line(H, self._shape)
        return LineWithPosition(target_line)

    
