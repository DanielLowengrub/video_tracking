class IntervalOnShape(object):
    '''
    This represents on interval on a ShapeWithPosition object.

    Since the nature of such an interval depends on the shape, this class mostly functions as an interface.
    '''

    def __init__(self, shape_with_position):
        '''
        shape_with_position - a ShapeWithPosition object
        '''
        self._shape_with_position = shape_with_position

    def __hash__(self):
        '''
        IntervalOnShape objects must implement the hash function in order to efficiently merge intervals
        '''
        raise NotImplementedError('hash was not implemented')

    def __str__(self):
        raise NotImplementedError('str was not implemented')

    @classmethod
    def load_from_shape_and_interval_array(cls, shape_with_position, array):
        '''
        shape_with_position - a ShapeWithPosition object
        array - a numpy array representing an interval on the given shape.
        '''

        raise NotImplementedError('load_from_shape_and_array has not been implemented')

    def get_shape_with_position(self):
        return self._shape_with_position
    
    def save_interval_to_array(self):
        '''
        Return a numpy array representing this interval.
        Note that we do NOT save the shape itself.
        '''

        raise NotImplementedError('save_interval_to_array has not been implemented')
    
    def length(self):
        '''
        Return the length of this interval.
        '''
        raise NotImplementedError('length was not implemented')

    def has_overlap(self, other):
        '''
        other - an IntervalOnShape
        return True iff this interval has on overlap with the other one
        '''

        raise NotImplementedError('has_overlap has not been implemented')

    def get_overlap(self, other):
        '''
        other - an IntervalOnShape
        return - a list of IntervalOnShape objects representing the intersection of this interval and the 
           other one. It is a list because, e.g in the case of an interval on an ellipse, the intersection of
           two intervals can be two intervals.
        '''
        raise NotImplementedError('get_overlap has not been implemented')

    def can_merge(self, other):
        '''
        other - an IntervalOnShape
        return True iff this interval can be merged with the other one
        '''

        raise NotImplementedError('can_merge has not been implemented')
    
    def contains_interval(self, other):
        '''
        other - an IntervalOnShape
        return True iff this interval contains the other one
        '''

        raise NotImplementedError('contains_interval has not been implemented')
    
    def merge(self, other):
        '''
        other - an IntervalOnShape
        We return a new interval that it contains the union of this one and the other one.
        
        If there is no overlap between the two, a ValueError is raised.
        '''

        raise NotImplementedError('merge has not been implemented')

    def position_linspace(self, step_size=1.0):
        '''
        step_size - a float in pixel units

        Return a numpy vector of absolute positions starting with the first position of the interval, ending at the last position, and with the given step size in pixels.
        '''

        raise NotImplementedError('sample_positions has not been implemented')

    def compute_middle_position(self):
        '''
        Return the position on the shape that is in the middle of this interval.
        '''

        raise NotImplementedError('compute_middle_position has not been implemented')
    
    def get_mask(self, image, padding, thickness):
        '''
        image - a numpy array of shape (n,m,3) and type np.uint8
        thickness - a float
        padding - a float

        Return a mask of this interval (with the given thickness) on the image.
        We disgard the parts of the image that are within padding of the end points of the interval.
        '''

        raise NotImplementedError('get_mask has not been implemented')

    def apply_homography(self, H, target_shape_with_position, target_region, anchor_sign):
        '''
        H - a numpy array with shape (3,3) and type np.float32 representing a homography matrix
        target_shape_with_position - a ShapeWithPosition object

        We assume that target_shape_with_position is the image of self._shape_with_position under H.
        '''
        raise NotImplementedError('apply_homography has not been implemented')

    def snap_to_shape(self, new_shape_with_position):
        '''
        shape_with_position - a ShapeWithPosition object
        return - an IntervalOnShape object whose underlying shape is new_shape_with_position
        '''
        raise NotImplementedError('snap_to_shape has not been imlemented')
    
    def interval_dilation(self, dilation_radius):
        '''
        dilation_radius - a float
        return a new IntervalOnShape whose intervals have been dilated on both ends by the amount: dilation_radius
        '''
        raise NotImplementedError('interval_dilation has not been imlemented')

    def interval_erosion(self, erosion_radius):
        '''
        erosionradius - a float
        return a new IntervalOnShape whose intervals have been eroded on both ends by the amount: erosion_radius
        '''
        raise NotImplementedError('interval_erosion has not been imlemented')

    @classmethod
    def fill_interval_gaps(self, intervals):
        '''
        intervals - a list of IntervalOnShape objects
        return - an IntervalOnShape object. It represents the result of filling the gaps between the input intervals.
        '''
        raise NotImplementedError('fill_interval_gaps has not been implemented')
    
    @classmethod
    def merge_intervals(self, intervals_on_shape):
        '''
        intervals_on_shape - a list of IntervalOnShape objects

        Return a new list of IntervalOnShape objects, which is the union of the given intervals.
        '''

        #print 'merging the intervals: ', [str(interval) for interval in intervals_on_shape]
        
        merged_intervals = set()
        #sort the given intervals by length so that we start with the bigger ones
        #the reason is to increase the chances that successive intervals get merged
        intervals_on_shape.sort(key = lambda x: x.length(), reverse=True)
        
        for interval_on_shape in intervals_on_shape:
            #this is the new merged interval we are going to ass
            new_merged_interval = interval_on_shape

            #print '   adding the interval: ', str(new_merged_interval)
            
            #find all of the ones that it overlaps with
            for merged_interval in list(merged_intervals):
                if new_merged_interval.can_merge(merged_interval):
                    #if the new_merged_interval has an overlap with merged_interval,
                    #then absorb merged_interval into new_merged_interval
                    #print '   merging the intervals %s and %s' % (str(new_merged_interval), str(merged_interval))
                    new_merged_interval = new_merged_interval.merge(merged_interval)

                    #and remove merged_interval from the set of merged_intervals
                    #(since it is now part of new_merged_interval)
                    merged_intervals.remove(merged_interval)

            #print '   the merged interval is: ', str(new_merged_interval)
            merged_intervals.add(new_merged_interval)

        #print 'the final merged_intervals dictinary is:'
        #print [str(interval) for interval in merged_intervals]
        
        return list(merged_intervals)

    @classmethod
    def intersect_interval_lists(cls, intervals_a, intervals_b):
        '''
        intervals_a, intervals_b - a list of IntervalOnShape objects
        return - a list of IntervalOnShape objects representing the intersections of the given intervals.

        Note: we assume that each of the input lists consist of disjoint intervals.
        '''
        # print 'intersecting interval lists...'
        overlaps = []

        #note that:
        #(A1 \cup ... \cup An) \cap (B1 \cup ... \cup Bm) = (A1 \cap B1) \cup (A1 \cap B2) \cup ...
        for interval_a in intervals_a:
            for interval_b in intervals_b:
                if interval_a.has_overlap(interval_b):
                    overlaps += interval_a.get_overlap(interval_b)

        return overlaps
                
