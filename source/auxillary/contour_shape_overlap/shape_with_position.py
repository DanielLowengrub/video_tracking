import numpy as np
import cv2
import itertools
from .interval_on_contour import IntervalOnContour
from ...auxillary import planar_geometry

class ShapeWithPosition(object):
    '''
    This is a wrapper for a PlanarShape object that provides a notion of "position" on the shape.
    It's main functionality is to create IntervalOnShape objects. For example, given a contour,
    it finds the smallest set of intervals containing that contour.
    '''

    def __init__(self, shape):
        '''
        shape - a PlanarShape object
        '''

        self._shape = shape

    def get_shape(self):
        return self._shape

    def save_to_array(self):
        '''
        Return a numpy array which represents this object
        '''
        return self._shape.get_parametric_representation()

    @classmethod
    def load_from_array(cls, array):
        '''
        array - a numpy array
        Return the ShapeWithPosition object represented by the array
        '''

        raise NotImplementedError('load_from_array has not been implemented')
    
    def compute_positions(self, points):
        '''
        point - a numpy array of shape (number of points, 2)

        Return a numpy array of length (number of points) which records the positions of each of the points on the shape
        '''
        raise NotImplementedError('compute_positions has not been implemented')

    def compute_position(self, point):
        return self.compute_positions(point.reshape((-1,2)))[0]
    
    def compute_absolute_position(self, position_on_shape):
        '''
        position_on_shape - a float

        Return the global coordinates of the point on the shape at the given position
        '''
        raise NotImplementedError('compute_absolute_position has not been implemented')

    def compute_absolute_positions(self, positions_on_shape):
        '''
        positions_on_shape - a numpy array with length (num positions)

        Return the a numpy array with shape (num positions, 2) representing the global coordinates of the points on the 
        shape at the given position
        '''
        raise NotImplementedError('compute_absolute_positions has not been implemented')

    def get_intervals_between_positions(self, positions):
        '''
        positions - a numpy array with length (num positions)

        Return a list of intervals connecting each pair of consecutive points
        '''

        raise NotImplementedError('get_intervals_between_positions has not been implemented')
    
    def _is_on_shape(self, points, thickness):
        '''
        points - a numpy array with shape (num points, 2)

        Return an array of length (num points) and type bool which indicates which of the points are within thickness of the shape
        '''

        raise NotImplementedError('_is_on_shape has not been implemented')

    def _find_intervals_in_position_sequence(self, positions):
        '''
        points - a list of positions

        Return a list of IntervalOnShape objects corresponding to the intervals on the shape that contain these points.
        '''

        raise NotImplementedError('_find_intervals_in_position_sequence has not been implemented')

    def _find_intervals_in_point_sequence(self, points):
        '''
        points - a numpy array of shape (num points, 2)

        Return a list of IntervalOnLine objects corresponding to the intervals on the line that contain these points.
        '''

        positions = self.compute_positions(points)
        return self._find_intervals_in_position_sequence(positions)

    
    def _find_subsequences_in_position_sequence(self, positions, position_mask):
        '''
        positions - a numpy array with length (num positions)
        position_mask - a numpy array with length (num_positions) and type np.float32

        Return a the list subsequences of the given list of positions such that any to adjacent positions are contained in the same subsequence.
        '''
        raise NotImplementedError('_find_subsequences_in_position_sequence has not been implemented')
    
    def _find_intervals_in_masked_position_sequence(self, positions, position_mask):
        '''
        positions - a numpy array with length (num positions)
        position_mask - a numpy array with length (num_positions) and type np.float32

        Return a the list of intervals with the smallest total length which contains the part of the shape lying between each two consecutive points that are in the mask.
        '''
        #print 'finding intervals in masked position sequence...'
        
        #first find the connected subsequences of the position sequence
        position_subsequences = self._find_subsequences_in_position_sequence(positions, position_mask)

        #print '   position subsequences:'
        #print position_subsequences
        
        #turn the subsequences into intervals on the shape
        return list(itertools.chain(*(self._find_intervals_in_position_sequence(subsequence) for subsequence in position_subsequences)))

    
    def _find_subsequences_on_shape(self, contour, thickness):
        '''
        contour - a Contour object
        thickness - a float

        Return a tuple (subsequences, contour_mask) where subsequences is a list of subsequences of points on the contour that are within thickness from the shape.
        contour_mask is a mask representing the points that are in one of the subsequences.
        '''
        image = np.zeros((352,624,3), np.uint8)
        
        #print 'finding subsequences on shape...'
        points = contour.get_cv_contour().reshape((-1,2))
        is_on_shape = self._is_on_shape(points, thickness)
                   
        #print 'is on shape mask:'
        #print is_on_shape
        
        #use the mask is_on_shape to find a list of intervals on the contour consisting of points that are on the shape
        intervals_on_contour = IntervalOnContour.from_point_mask(contour, is_on_shape)
        subsequences = [interval_on_contour.get_points() for interval_on_contour in intervals_on_contour]

        #print 'we found the subsequences:'
        #for subsequence in subsequences:
        #    print subsequence
            
        return subsequences, is_on_shape
    
    def find_intervals(self, contour, thickness):
        '''
        contour - a Contour object
        thickness - a float

        Return a tuple (list of IntervalOnShape objects, contour_mask)

        These represent the list of intervals with smallest total length that contain all of the points on the contour that are on the thickened shape
        '''

        # print 'finding intervals on the shape for the contour:'
        # print contour.get_cv_contour()
        
        #first get a list of subsequences of the contour that are on the shape
        subsequences_on_shape, contour_mask = self._find_subsequences_on_shape(contour, thickness)

        # print 'the subsequences are:'
        # print subsequences_on_shape
        
        #for each such sequence, build an IntervalOnShape object
        intervals_on_shape = list(itertools.chain(*(self._find_intervals_in_point_sequence(s)
                                                    for s in subsequences_on_shape)))

        return intervals_on_shape, contour_mask

    def get_interval_on_shape(self, point_1, point_2):
        '''
        point_i - a numpy array with shape (2,) and type np.float64
        Return an interval whose end points are the given points.
        '''
        return self._find_intervals_in_point_sequence(np.vstack([point_1, point_2]))[0]

    def get_interval_from_position_and_increment(self, position, increment):
        '''
        position - a position on the shape
        increment - a float

        Return the interval (position, position + increment)
        '''
        #print 'finding interval from position: %f and incrememnt %f' % (position, increment)
        
        positions = np.vstack([position, position + increment / 2.0, position + increment])

        #print 'positions: '
        #print positions
        
        return self._find_intervals_in_position_sequence(positions)[0]

    def position_sampling(self, region, step_size=1.0):
        '''
        region - a numpy array [[x start, y start], [x end, y end]]
        step_size - a float in pixel units
        
        Return a list of positions on the shape that starts at the first position that is in the region, and ends at the last one.
        If all of the shape is in the region, the positions cover the entire shape.
        The distance in pixels between the points corresponding to consecutive positions is step_size.

        Note that if the shape enters and exits the region multiple times, not all positions will correspond to points in the region.
        '''

        raise NotImplementedError('position_sampleing has not been implemented')

    def apply_homography(self, H):
        '''
        H - a numpy array with shape (3,3) and type np.float64. It represents a homography
        return - a ShapeWithPosition object
        '''
        raise NotImplementedError('apply_homography has not been implemented')
    
    def compute_shape_mask_intersection(self, mask):
        '''
        mask - numpy array with shape (n,m) and type np.float32

        Return a list of IntervalOnShape objects that cover the region of the shape contained in the mask.
        '''
        mask_region = np.array([[0,0],
                                [mask.shape[1], mask.shape[0]]])
        
        #sample points on the shape inside the region of the mask
        sampled_positions = self.position_sampling(mask_region, step_size=0.5)

        sampled_points = np.int32(self.compute_absolute_positions(sampled_positions))

        #print 'sampled points:'
        #print sampled_points
        
        #check which points are in the region of the mask
        is_point_in_mask_region = (mask_region[0] <= sampled_points).all(axis=1) * (sampled_points < mask_region[1]-1).all(axis=1)
        points_in_mask_region = sampled_points[is_point_in_mask_region]
        indices_of_points_in_mask_region = np.where(is_point_in_mask_region)
        
        #check which of the points in the region of the mask are actually in the mask
        is_point_in_mask_region_in_mask = mask[points_in_mask_region[:,1], points_in_mask_region[:,0]]

        #record which of the original sampled points are in the mask
        is_point_in_mask = is_point_in_mask_region
        is_point_in_mask[indices_of_points_in_mask_region] = is_point_in_mask_region_in_mask

        #print 'is point in mask:'
        #print is_point_in_mask
        
        #use the positions and the point mask to build a list of intervals
        intervals_on_shape = self._find_intervals_in_masked_position_sequence(sampled_positions, is_point_in_mask)

        return intervals_on_shape
