from .contour_shape_overlap import ContourShapeOverlapGenerator

class ContourLineOverlapGenerator(ContourShapeOverlapGenerator):
    '''
    This implementation of the ContourShapeOverlapGenerator assumes that the input shapes are Line objects.
    '''

    
