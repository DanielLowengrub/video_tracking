from ..contour_shape_overlap import interval_on_line
from ...auxillary.half_interval import HalfInterval

class HalfIntervalOnLine(interval_on_line.IntervalOnLine):
    '''
    This implementation of IntervalOnLine uses HalfIntervals as the underlying interval.
    '''
    
