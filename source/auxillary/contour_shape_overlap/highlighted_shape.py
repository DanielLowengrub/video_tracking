import numpy as np
import os
import cv2
from line_with_position import LineWithPosition
from ellipse_with_position import EllipseWithPosition
from interval_on_shape import IntervalOnShape
from interval_on_line import IntervalOnLine
from interval_on_ellipse import IntervalOnEllipse
from ..mask_operations import get_union
from ...image_processing.contour_generator.contour_from_mask_generator import ContourFromMaskGenerator
from ...auxillary import planar_geometry
from ..fit_points_to_shape.line import Line
import scipy.ndimage
import operator
import matplotlib.pyplot as plt

#TODO: remove_shapes_from_contours will crash if the length of the contour list is zero.
DEBUG = False

class HighlightedShape(object):
    '''
    This represents a shape which has certain areas highlighted.
    We use this to record which parts of an abstract shape are actually drawn in an image.

    In particular, highlighted_shapes can generate masks that show which parts of an image are contained in the highlighted part of a shape.
    '''

    LINE = 0
    ELLIPSE = 1

    SHAPE_FILENAME = 'shape_with_position.npy'
    INTERVALS_ON_SHAPE_FILENAME = 'intervals_on_shape.npy'
    THICKNESS_FILENAME = 'thickness.npy'

    DIST_TO_INFTY = 10**5 #the distance to the line at infinity from the origin 
    MIN_COMPONENT_AREA = 10
    
    _contour_generator = ContourFromMaskGenerator()

    def __init__(self, shape_with_position, thickness, intervals_on_shape):
        '''
        shape_with_position - a ShapeWithPosition object
        thickness - a float
        intervals_on_shape - a list of IntervalOnShape objects. The underlying ShapeWithPosition of each of these intervals should be the given shape_with_position.
        '''

        self._shape_with_position = shape_with_position
        self._thickness = thickness
        self._intervals_on_shape = intervals_on_shape

    def __str__(self):
        intervals_string = ', '.join(map(str, self._intervals_on_shape))
        return 'Highlighted Shape. shape = %s, intervals: %s' % (str(self.get_shape()), intervals_string)

    def __repr__(self):
        return str(self)
    
    def get_shape_type(self):
        #print 'shape type: ', self._shape_with_position.get_shape()._shape_type
        if self.is_line():
            return self.LINE
        elif self.is_ellipse():
            return self.ELLIPSE
        else:
            raise RuntimeError('The shape type could not be determined')

    def is_line(self):
        return self._shape_with_position.get_shape().is_line()

    def is_ellipse(self):
        return self._shape_with_position.get_shape().is_ellipse()
        
    def get_shape(self):
        return self._shape_with_position.get_shape()

    def get_shape_with_position(self):
        return self._shape_with_position
    
    def get_thickness(self):
        return self._thickness
    
    @classmethod
    def load_from_directory(cls, directory_abs_path, shape_type):
        '''
        directory_abs_path - an absolute path to a directory containing highlighted shape data
        shape_type - either HighlightedShape.LINE or HighlightedShape.ELLIPSE
        
        The directory should contain the files shape_with_position.npy, intervals_on_shape.npy and thickness.npy
        '''
        #load the shape array
        shape_filename = os.path.join(directory_abs_path, cls.SHAPE_FILENAME)
        shape_array = np.load(shape_filename)
        #print 'shape array: ', shape_array
        
        #load the intervals array
        intervals_on_shape_filename = os.path.join(directory_abs_path, cls.INTERVALS_ON_SHAPE_FILENAME)
        intervals_on_shape_array = np.load(intervals_on_shape_filename)
        #print 'intervals:'
        #print intervals_on_shape_array
        
        if shape_type == cls.LINE:
            shape_with_position = LineWithPosition.load_from_array(shape_array)
            intervals_on_shape = [IntervalOnLine.load_from_shape_and_interval_array(shape_with_position, interval) for interval in intervals_on_shape_array]

        elif shape_type == cls.ELLIPSE:
            shape_with_position = EllipseWithPosition.load_from_array(shape_array)
            intervals_on_shape = [IntervalOnEllipse.load_from_shape_and_interval_array(shape_with_position, interval) for interval in intervals_on_shape_array]

        thickness_filename = os.path.join(directory_abs_path, cls.THICKNESS_FILENAME)
        thickness = np.load(thickness_filename)[0]
        #print 'thickness: ', thickness
        
        return cls(shape_with_position, thickness, intervals_on_shape)

    def save_to_directory(self, directory_abs_path):
        '''
        directory_abs_path - the absolute path to a directory
        '''
        shape_filename = os.path.join(directory_abs_path, self.SHAPE_FILENAME)
        shape_array = self._shape_with_position.save_to_array()
        np.save(shape_filename, shape_array)

        intervals_on_shape_filename = os.path.join(directory_abs_path, self.INTERVALS_ON_SHAPE_FILENAME)
        intervals_on_shape_array = np.vstack([interval_on_shape.save_interval_to_array() for interval_on_shape in self._intervals_on_shape])
        np.save(intervals_on_shape_filename, intervals_on_shape_array)

        thickness_filename = os.path.join(directory_abs_path, self.THICKNESS_FILENAME)
        thickness_array = np.array([self._thickness])
        np.save(thickness_filename, thickness_array)

        return

    def generate_mask(self, image, min_interval_length=None, padding=None, thickness=None):
        '''
        image - a numpy array of shape (n,m,3) and type np.uint8, or an array with shape (n,m) and type np.float32.
        min_interval_length - a float
        padding - a float. 
        Return a mask on the image containing the highlighted portions of the shape. 
        We exclude intervals whose length is less than min_interval_length.
        Also, we do not highlight the portions of the interval that are within padding from the end points.
        '''

        if thickness is None:
            thickness = self._thickness

        if padding is None:
            padding = 0
            
        #we find masks for each of the intervals on the shape and take their union
        mask_complement = np.ones(image.shape[:2], np.float32)

        for interval in self._intervals_on_shape:
            #print 'considering interval with length: ', interval.length()
            if min_interval_length is not None and interval.length() < min_interval_length:
                continue
            mask_complement *= (1 - interval.get_mask(image, padding, thickness))

        return 1 - mask_complement

    def contains_interval(self, interval_on_shape):
        '''
        interval_on_shape - an IntervalOnShape object whose underlying shape is the same as _shape_with_position
        
        Return true if the interval is contained in the highlighted region of the shape
        '''

        return any(interval.contains_interval(interval_on_shape) for interval in self._intervals_on_shape)

    def contains_highlighted_shape(self, other):
        '''
        other - a HighlightedShape whose underlying ShapeWithPosition is the same as this shapes
        return - True if all of the highlighted region of other is contained in the highlighted region of this shape
        '''
        return all(self.contains_interval(ios) for ios in other._intervals_on_shape)
    
    def number_of_intervals(self):
        return len(self._intervals_on_shape)

    def highlighted_arc_length(self):
        '''
        Return the sum of the lengths of the intervals
        '''
        return sum(ios.length() for ios in self._intervals_on_shape)
    
    def remove_small_intervals(self, min_interval_length):
        '''
        min_interval_length - a float
        
        remove the intervals whose length is less than min_interval_length
        '''
        #print 'interval lengths: ', [ios.length() for ios in self._intervals_on_shape]
        self._intervals_on_shape = [ios for ios in self._intervals_on_shape if ios.length() >= min_interval_length]
        return

    def add_mask(self, mask):
        '''
        mask - numpy array with shape (n,m) and type np.float32
        
        We return a new HighlightedShape object such that all of the masked portions of the shape are highlighted, in addition to the original intervals.
        '''
        #print 'adding mask...'
        #print 'current intervals: ', map(str,self._intervals_on_shape)
        
        #first get a list of intervals obtained from intersecting the shape with the mask
        new_intervals = self._shape_with_position.compute_shape_mask_intersection(mask)

        #print 'new intervals: %s' % ', '.join(map(str, new_intervals))
        
        #now merge these intervals with the existing ones
        all_intervals = IntervalOnShape.merge_intervals(new_intervals + self._intervals_on_shape)

        return HighlightedShape(self._shape_with_position, self._thickness, all_intervals)

    def _can_apply_homography_to_shape(self, H, anchor_sign):
        '''
        H - a numpy array with shape (3,3) representing a homography
        anchor_sign - an integer. either 1 or -1. It represents the side of the line at infty that we want to use.

        return - True iff it is possible to apply the homography to the underlying shape
        '''
        if self.is_line():
            return True

        #if the line at infinity of H is very far from the origin (0,0), then return true
        if np.linalg.norm(H[2,:2]) < H[2,2] / self.DIST_TO_INFTY:
            return True
        
        #we now assume the shape is an ellipse.
        #make sure that the ellipse does not intersect the line at infty,
        #and that the center of the ellipse is on the same side of the line as the anchor point
        line_at_infty = Line.from_parametric_representation(H[2])
        ellipse = self._shape_with_position.get_shape()

        #if the line at infty intersects the ellipse
        if len(planar_geometry.line_ellipse_intersection(line_at_infty, ellipse)) > 0:
            return False

        #check if the center is on the same side as the anchor point
        center = ellipse.get_center()
        projective_center = np.append(center, 1)
        center_sign = np.sign(np.matmul(H[2], projective_center))

        return center_sign == anchor_sign

    def apply_homography(self, H, target_region, anchor_sign):
        '''
        H - a numpy array with shape (3,3) and type np.float64. It represents a homography
        target_region - a numpy array with shape (2,2). It represents the region of the target plane that the intervals
           can be in.
        anchor_sign - an integer. either 1 or -1. It represents the side of the line at infty that we want to use.

        return - a HighlightedShape object. It represents the image of the highlighted shape under the homography.
            If we did not manage to apply the homography, return None
        '''
        if not self._can_apply_homography_to_shape(H, anchor_sign):
            return None
        
        #first compute the new shape with position
        target_shape_with_position = self._shape_with_position.apply_homography(H)

        if self.is_ellipse() and target_shape_with_position.get_shape().is_degenerate():
            return None
        
        #then apply the homography to each of the intervals on the shape
        target_intervals_on_shape = [ios.apply_homography(H, target_shape_with_position, target_region, anchor_sign)
                                     for ios in self._intervals_on_shape]
        target_intervals_on_shape = [ios for ios in target_intervals_on_shape if ios is not None]
        
        return HighlightedShape(target_shape_with_position, self._thickness, target_intervals_on_shape)

    def snap_to_shape(self, new_shape_with_position):
        '''
        new_shape_with_position - a ShapeWithPosition object with the same type as self._shape_with_position

        return a HighlightedShape object whose underlying ShapeWithPosition is new_shape_with_position. 
        We try to transfer the intervals in this shape to new_shape_with_position
        '''

        new_intervals_on_shape = [ios.snap_to_shape(new_shape_with_position) for ios in self._intervals_on_shape]
        return HighlightedShape(new_shape_with_position, self._thickness, new_intervals_on_shape)

    def interval_dilation(self, dilation_radius):
        '''
        dilation_radius - a float
        return a new HighlightedShape whose intervals have been dilated on both ends by the amount: dilation_radius
        '''
        dilated_intervals_on_shape = [ios.interval_dilation(dilation_radius)
                                      for ios in self._intervals_on_shape]
        dilated_intervals_on_shape = IntervalOnShape.merge_intervals(dilated_intervals_on_shape)
        return HighlightedShape(self._shape_with_position, self._thickness, dilated_intervals_on_shape)

    def interval_erosion(self, erosion_radius):
        '''
        erosion_radius - a float
        return a new HighlightedShape whose intervals have been eroded on both ends by the amount: erosion_radius
        '''
        eroded_intervals_on_shape = [ios.interval_erosion(erosion_radius)
                                     for ios in self._intervals_on_shape]
        dilated_intervals_on_shape = [ios for ios in eroded_intervals_on_shape if ios is not None]
        return HighlightedShape(self._shape_with_position, self._thickness, dilated_intervals_on_shape)

    def interval_closing(self, closing_radius):
        '''
        closing_radius - a float
        return - a new HighlightedShape such that any gaps between intervals with length <= closing_radius have been filled
        '''
        dilation = self.interval_dilation(closing_radius)
        closing = dilation.interval_erosion(closing_radius)
        return closing
    
    def intersect_intervals(self, other):
        '''
        other_highlighted_shape - a HighightedShape object with the same ShapeWithPosition as this shape.
        return - a HighightedShape with the same ShapeWithPosition as this shape. It's intervals are the
           intersection of this shapes intervals and the other shapes intervals.
        '''
        intersections = IntervalOnShape.intersect_interval_lists(self._intervals_on_shape,
                                                                 other._intervals_on_shape)

        return HighlightedShape(self._shape_with_position, self._thickness, intersections)
    
    def fill_interval_gaps(self):
        '''
        return - a HighlightedShapes objects in which gaps between intervals have been filled
        '''
        if len(self._intervals_on_shape) == 0:
            return HighlightedShape(self._shape_with_position, self._thickness, self._intervals_on_shape)

        ios_class = type(self._intervals_on_shape[0])
        interval = ios_class.fill_interval_gaps(self._shape_with_position, self._intervals_on_shape)
        return HighlightedShape(self._shape_with_position, self._thickness, [interval])
        
    @classmethod
    def remove_shapes_from_contours(cls, highlighted_shapes, contours):
        '''
        highlighted_shapes - a list of highlighted shapes
        contours - a list of Contour objects that are all on the same image
        
        Return a list of tuples of Contour objects.

        The output has the following meaning. First of all, removing the highlighted shapes from the image breaks up the input
        contours into a new (longer)
        list of contours C1,...,Cn. We say that two contours Ci,Cj are in the same connected component if they become 
        connected as the thickness of the removed shapes
        goes to zero.
        The tuples in the output correspond to connected components of C1,...,Cn

        For example, suppose the input contour traced out a single line L passing through the center of a circle A, 
        together with a disjoint circle B. Suppose that the highlighted shape was the line L.
        Removing the line L generates 3 contours: 
        Two contours C1,C2 corresponding to the two halves of the circle A, and one contour C3 corresponding to the circle B.
        In this case, we would return [(C1,C2), (C3)]
        '''
        if len(highlighted_shapes) == 0:
            return [(contour,) for contour in contours]
        
        #we first create a mask that corresponds to the interiors of the contours
        image = contours[0].get_image()
        contour_mask = contours[0].get_outer_mask()
        contour_mask = reduce(get_union, (contour.get_outer_mask() for contour in contours[1:]), contour_mask)

        if DEBUG:
            cv2.imshow('the contour mask', contour_mask)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

        #now subtract the highlighted shape mask of each of the shapes
        sub_contour_mask = reduce(operator.mul, (1-hs.generate_mask(image) for hs in highlighted_shapes), contour_mask)

        if DEBUG:
            cv2.imshow('sub contour mask', sub_contour_mask)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

        #we now color the new contour mask by the connected components, with the notion of components that we explained above.
        #to do this, first dilate the contour mask by the thickness of the shapes we removed
        max_thickness = max(shape.get_thickness() for shape in highlighted_shapes)
        kernel_dilate = np.ones((max_thickness+5,max_thickness+5), np.uint8)
        dilated_sub_contour_mask = cv2.dilate(sub_contour_mask, kernel_dilate)

        #then intersect the dilated mask with the original one so that we only add pixels that were contained in the original highlighted shapes
        dilated_sub_contour_mask *= contour_mask

        if DEBUG:
            cv2.imshow('the dilated contour mask', dilated_sub_contour_mask)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

        #now label the pixels in the dilated mask by the connected components
        labelled_array, num_features = scipy.ndimage.measurements.label(np.int32(dilated_sub_contour_mask))

        #print 'we found %d components' % num_features

        # print 'showing the connected components of the histogram...'
        # p = plt.imshow(labelled_array, interpolation="nearest")
        # plt.colorbar(p)
        # plt.show()
    
        sub_contour_tuples = []
        #finally, find the contours in new_contour_mask and group them by the labels of the pixels they contain.
        for label in range(1,num_features+1):
            dilated_sub_contour_component_mask = np.float32(labelled_array == label)
            sub_contour_component_mask = sub_contour_mask * dilated_sub_contour_component_mask

            if sub_contour_component_mask.sum() < cls.MIN_COMPONENT_AREA:
                #print '   removing component with area: ', sub_contour_component_mask.sum()
                continue
            
            # cv2.imshow('extracting contours from this component', sub_contour_component_mask)
            # cv2.waitKey(0)
            # cv2.destroyAllWindows()

            sub_contours_in_component = cls._contour_generator.generate_contours(image, sub_contour_component_mask)
            if len(sub_contours_in_component) > 0:
                sub_contour_tuples.append(tuple(sub_contours_in_component))
            
        return sub_contour_tuples

