import numpy as np
from .shape_with_position import ShapeWithPosition
from .interval_on_ellipse import IntervalOnEllipse
from ..fit_points_to_shape.ellipse import Ellipse
from ..planar_geometry import perp, rotate_vector, rotate_vectors
from ..interval import IntervalList
from ...auxillary import planar_geometry

class EllipseWithPosition(ShapeWithPosition):
    '''
    This implements an Ellipse together with information about the position of a point on the ellipse.
    The position of a point is determined to be the angle between L1 and L2, where L1 is the line between the point and the center of the ellipse,
    and L2 is the longer axis of the ellipse.
    '''

    CIRCLE_DEGREES = 360
    RADIANS_TO_DEGREES = 180 / np.pi
    DEGREES_TO_RADIANS = np.pi / 180
    EPSILON = 10**(-5)
    
    def __init__(self, ellipse):
        '''
        ellipse - an Ellipse object
        '''

        super(EllipseWithPosition, self).__init__(ellipse)

        ellipse.compute_geometric_parameters()
        self._ellipse_center, self._axis_lengths, self._angle = ellipse.get_geometric_parameters()
        
    @classmethod
    def load_from_array(cls, array):
        '''
        array - a numpy array containing a the parameters of the ellipse: [a,b,c,d,e,f]
        '''
        ellipse = Ellipse.from_parametric_representation(array)
        return cls(ellipse)

    def get_geometric_parameters(self, in_degrees=False):
        return self._shape.get_geometric_parameters(in_degrees)
    
    def _is_on_shape(self, points, thickness):
        '''
        points - a numpy array with shape (num points, 2)

        Return an array of length (num points) and type bool which indicates which of the points are within thickness of the shape
        '''
        distance_from_shape = self._shape.get_geometric_distance_from_points(points)

        return (distance_from_shape < thickness)

    def _find_intervals_in_position_sequence(self, positions):
        '''
        points - a list of positions

        Return a list of IntervalOnEllipse objects corresponding to the intervals on the ellipse that contain these points.
        '''

        #print 'finding intervals in sequence:'
        #print positions
        
        if len(positions) < 2:
            return []
        
        #for each pair of consecutive positions, create the interval between them
        intervals = [IntervalOnEllipse.build_short_interval(self,p1,p2) for p1,p2 in zip(positions[:-1], positions[1:])]

        #print 'intervals: '
        #print ', '.join(map(str,intervals))
        
        #then merge the intervals. we do not have to verify the possibility of merging consecutive intervals since at least their endpoints should match up.
        merged_interval = intervals[0]
        for interval in intervals:
            #print 'adding interval: ', interval
            merged_interval = merged_interval.merge(interval)
            #print 'merged_interval: ', merged_interval
        
        return [merged_interval]

    def _find_subsequences_in_position_sequence(self, positions, position_mask):
        '''
        positions - a numpy array with length (num positions)
        position_mask - a numpy array with length (num_positions) and type np.float32

        Return a the list subsequences of the given list of positions such that any to adjacent positions are contained in the same subsequence.
        '''

        intervals = IntervalList.from_boolean_array(position_mask).get_intervals()
        subsequences =  [interval.get_slice(positions) for interval in intervals if interval.first_element() < interval.last_element()]

        #try to merge the first and last sequence
        if len(subsequences) >= 2 and position_mask[0] and position_mask[-1]:
            #print 'concatenating the subsequences %s and %s.' % (subsequences[0], subsequences[-1])
            subsequences[0] = np.hstack([subsequences[-1],subsequences[0]])
            subsequences = subsequences[:-1]

        return subsequences

    # def _find_intervals_in_point_sequence(self, points):
    #     '''
    #     points - a numpy array of shape (num points, 2)

    #     Return a list of IntervalOnEllipse objects corresponding to the intervals on the ellipse that contain these points.
    #     '''

    #     positions = self.compute_positions(points)

    #     #for each pair of consecutive points, create the interval between them
    #     intervals = [IntervalOnEllipse.build_short_interval(self,p1,p2) for p1,p2 in zip(positions[:-1], positions[1:])]
        
    #     #then merge the intervals. we do not have to verify the possibility of merging consecutive intervals since at least their endpoints should match up.
    #     merged_interval = intervals[0]
    #     for interval in intervals:
    #         #print 'adding interval: ', interval
    #         merged_interval.merge(interval)
    #         #print 'merged_interval: ', merged_interval

    #     #print 'found the merged interval: '
    #     #print merged_interval
        
    #     return [merged_interval]

    def compute_positions(self, points):
        '''
        point - a numpy array of shape (number of points, 2)

        Return a numpy array of length (number of points) which records the positions of each of the points on the shape
        '''

        #first transform the points to the ellipse coordinate system
        points = points - self._ellipse_center
        points = rotate_vectors(points, -self._angle)
        points = points / self._axis_lengths
        
        point_directions = points / (np.linalg.norm(points, axis=1)).reshape((-1,1))

        # print 'point directions:'
        # print point_directions
        
        #now compute the angle in radians between the point directions and the long axis
        cos_angles_to_long_axis = point_directions[:,0]
        cos_angles_to_short_axis = point_directions[:,1]
        angles = np.arccos(cos_angles_to_long_axis) * self.RADIANS_TO_DEGREES

        # print 'cos_angles_to_long_axis: ', cos_angles_to_long_axis
        # print 'cos_angles_to_short_axis: ', cos_angles_to_short_axis
        # print 'angles: '
        # print angles
        
        #if the vector to a point is pointing away from the direction of the short axis,
        #then we need to add 180 degrees to the angle
        away_from_short_axis = cos_angles_to_short_axis < -self.EPSILON
        angles[away_from_short_axis] = self.CIRCLE_DEGREES - angles[away_from_short_axis]

        
        return angles

    def compute_absolute_position(self, position_on_shape):
        '''
        position_on_shape - a float

        Return the global coordinates of the point on the shape at the given position
        '''

        #first get the angle of the point in the ellipse coordinates
        relative_angle = position_on_shape * self.DEGREES_TO_RADIANS

        #then get the position of the point in ellipse coordinates
        position_in_ellipse_coords = np.array([self._axis_lengths[0] * np.cos(relative_angle), self._axis_lengths[1] * np.sin(relative_angle)])

        #finally, we transform from ellipse coordinates to absolute coordinates
        absolute_position = self._ellipse_center + rotate_vector(position_in_ellipse_coords, self._angle)

        return absolute_position

    def compute_absolute_positions(self, positions_on_shape):
        '''
        positions_on_shape - a numpy array of length (num positions) and type np.float32

        Return a numpy array with shape (num points, 2) containing the global coordinates of the points on the shape at the given positions
        '''

        #first get the angle of the point in the ellipse coordinates
        relative_angles = positions_on_shape * self.DEGREES_TO_RADIANS

        #then get the position of the point in ellipse coordinates
        positions_in_ellipse_coords = np.vstack([self._axis_lengths[0] * np.cos(relative_angles), self._axis_lengths[1] * np.sin(relative_angles)]).transpose()

        #finally, we transform from ellipse coordinates to absolute coordinates
        absolute_positions = self._ellipse_center + rotate_vectors(positions_in_ellipse_coords, self._angle)

        return absolute_positions

    def get_intervals_between_positions(self, positions):
        '''
        positions - a numpy array with length (num positions)

        Return a list of intervals connecting each pair of consecutive points. The last interval in the output is between
        positions[-1] and positions[0].
        '''
        
        if len(positions) < 2:
            return []
        
        positions = np.sort(positions)
        intervals = [IntervalOnEllipse(self, start_position, end_position)
                     for start_position, end_position in zip(positions[:-1], positions[1:])
                     if np.abs(start_position - end_position) > self.EPSILON]
        intervals.append(IntervalOnEllipse(self, positions[-1], positions[0]))

        return intervals
    
    def position_sampling(self, region, step_size=1.0):
        '''
        region - a numpy array [[x start, y start], [x end, y end]]
        step_size - a float in pixel units
        
        Return a list of positions on the ellipse that contain the interval that starts at the first position that is in the region, and ends at the last one.
        If all of the shape is in the region, the positions cover the entire shape.
        The distance in pixels between the points corresponding to consecutive positions is step_size.

        Note that if the shape enters and exits the region multiple times, not all positions will correspond to points in the region.
        '''

        #since the ellipse is bounded, we simply return positions evenly spaced around the entire ellipse
        #we approximate the position step size so that one step roughly corresponds to one pixel
        position_step_size = self.RADIANS_TO_DEGREES * (1 / self._axis_lengths[0])

        positions = np.arange(0, self.CIRCLE_DEGREES, position_step_size)

        return positions

    def apply_homography(self, H):
        '''
        H - a numpy array with shape (3,3) and type np.float64. It represents a homography
        return - an EllipseWithPosition object
        '''
        target_ellipse = planar_geometry.apply_homography_to_ellipse(H, self._shape)
        return EllipseWithPosition(target_ellipse)
