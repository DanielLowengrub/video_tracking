import numpy as np
import cv2
from ...auxillary import planar_geometry
from .interval_on_shape import IntervalOnShape
from .interval_on_loop import IntervalOnLoop

class IntervalOnEllipse(IntervalOnShape):
    '''
    This implementation of IntervalOnShape represents an interval on an ellipse.
    The interval is stored as an IntervalOnLoop object.
    '''
    FULL_CIRCLE_ANGLE = 360 #we use degrees to store the position on an ellipse
    
    def __init__(self, ellipse_with_position, first_position, last_position):
        '''
        ellipse_with_position - an EllipseWithPosition object
        '''
        super(IntervalOnEllipse, self).__init__(ellipse_with_position)

        self._interval = IntervalOnLoop(first_position, last_position, self.FULL_CIRCLE_ANGLE)

    def __hash__(self):
        return hash((self._interval.first_element(), self._interval.last_element()))

    def __repr__(self):
        return str(self)
    
    def __str__(self):
        return str(self._interval)

    @classmethod
    def get_position_mod_full_circle_angle(cls, position):
        '''
        position - an angle in degrees
        Return an equivilent angle in the range [0, 360)
        '''

        if 0 <= position < cls.FULL_CIRCLE_ANGLE:
            return position

        elif position < 0:
            N = int((cls.FULL_CIRCLE_ANGLE - position) / cls.FULL_CIRCLE_ANGLE)
            return position + N*cls.FULL_CIRCLE_ANGLE
        
        elif position >= cls.FULL_CIRCLE_ANGLE:
            N = int(position / cls.FULL_CIRCLE_ANGLE)
            return position - N*cls.FULL_CIRCLE_ANGLE
            
    @classmethod
    def load_from_shape_and_interval_array(cls, ellipse_with_position, interval_array):
        return cls(ellipse_with_position, interval_array[0], interval_array[1])

    @classmethod
    def build_short_interval(cls, ellipse_with_position, position_1, position_2):
        '''
        ellipse_with_position - and EllipseWithPosition object
        position_i - two angles in degrees 

        We return an IntervalOnEllipse object corresponding to the shortest interval that starts and ends on the given angles
        '''

        #print 'building short interval from the positions %f and %f' % (position_1, position_2)

        #we first make sure that the positions are in the range [0, 360)
        position_1 = cls.get_position_mod_full_circle_angle(position_1)
        position_2 = cls.get_position_mod_full_circle_angle(position_2)
        
        interval_a = IntervalOnLoop(position_1, position_2, cls.FULL_CIRCLE_ANGLE)
        interval_b = IntervalOnLoop(position_2, position_1, cls.FULL_CIRCLE_ANGLE)

        interval = interval_a if interval_a.get_length() <= interval_b.get_length() else interval_b

        return IntervalOnEllipse(ellipse_with_position, interval.first_element(), interval.last_element())

    @classmethod
    def build_full_interval(cls, ellipse_with_position):
        '''
        ellipse_with_position - an EllipseWithPosition object
        
        Return an interval on this ellipse that covers the entire ellipse
        '''

        return IntervalOnEllipse(ellipse_with_position, 0, cls.FULL_CIRCLE_ANGLE)
                                 
    def save_interval_to_array(self):
        return np.array([self._interval.first_element(), self._interval.last_element()])
    
    def length(self):
        return self._interval.get_length()

    def has_overlap(self, other):
        return self._interval.has_overlap(other._interval)

    def get_overlap(self, other):
        if not self.has_overlap(other):
            raise ValueError('tried to compute overlap of two shapes that do not overlap')

        intervals = self._interval.get_intersection(other._interval)
        return [IntervalOnEllipse(self._shape_with_position, interval.first_element(), interval.last_element())
                for interval in intervals]
    
    def contains_interval(self, other):
        return self._interval.contains_interval(other._interval)
    
    def can_merge(self, other):
        return self._interval.can_merge(other._interval)
    
    def merge(self, other):
        if not self.can_merge(other):
            raise ValueError('Can not merge the two intervals.')

        interval = self._interval.merge(other._interval)
        return IntervalOnEllipse(self._shape_with_position, interval.first_element(), interval.last_element())

    def compute_middle_position(self):
        '''
        return - a float. It represents the position on the ellipse that is in the middle of the interval
        '''
        #first get the middle of the interval in the internal interval coordinates
        relative_middle_position = self.length() / 2.0

        return self._interval.get_absolute_position(relative_middle_position)
    
    def get_mask(self, image, padding, thickness):
        mask = np.zeros(image.shape[:2], np.float32)
        #if the 2*padding is longer than the length of the interval, then there is nothing to draw
        #if 2*padding > self.length():
        #    return mask
        if padding > 0:
            raise NotImplementedError('padding is not implemented for masks of ellipse intervals')
        
        center, axes, angle = self._shape_with_position.get_geometric_parameters(in_degrees=True)

        #we now compute the starting and ending angles of the ellipse
        start_end_angles = self._interval.get_standard_interval_tuples()
        for start_angle,end_angle in start_end_angles:
            cv2.ellipse(mask, tuple(np.int32(center).tolist()), tuple(np.int32(axes).tolist()), angle, start_angle, end_angle, 1, thickness, cv2.CV_AA)
        return mask
    
    def apply_homography(self, H, target_ellipse_with_position, target_region, anchor_sign):
        '''
        H - a numpy array with shape (3,3) and type np.float32 representing a homography matrix
        target_ellipse_with_position - a EllipseWithPosition object
        
        return - an IntervalOnEllipse object whose underlying EllipseWithPosition is target_ellipse_with_position
        We assume that target_ellipse_with_position is the image of self._shape_with_position under H.

        WARNING: We assume that the homography preserves orientation!!!
        '''
        # print 'applying homography to interval on ellipse: ', self._interval

        if self._interval.is_full_loop():
            return self.build_full_interval(target_ellipse_with_position)
        
        #apply the homography to the endpoints of the interval
        positions = np.array([self._interval.first_element(), self._interval.last_element()])
        points = self._shape_with_position.compute_absolute_positions(positions)
        target_points = planar_geometry.apply_homography_to_points(H, points)

        # print '   positions: ', positions
        # print '   points: ', points
        # print '   target_points: ', target_points

        #use the target points to create an interval on the target shape
        target_positions = target_ellipse_with_position.compute_positions(target_points)
        # print '   target_positions: ', target_positions
        target_interval = IntervalOnEllipse(target_ellipse_with_position, target_positions[0], target_positions[1])
        # print '   target_interval: ', target_interval._interval
        
        return target_interval

    def snap_to_shape(self, new_ellipse_with_position):
        '''
        ellipse_with_position - a EllipseWithPosition object
        return - an IntervalOnEllipse object whose underlying shape in new_ellipse_with_position
        '''
        #print 'snapping the interval on ellipse: ', self._interval
        if self._interval.is_full_loop():
            return self.build_full_interval(new_ellipse_with_position)

        #record the positions of the beginning, middle and end of the interval.
        #we record the middle to guarentee that all intervals between these points are "short"
        positions = np.array([self._interval.first_element(), self.compute_middle_position(), self._interval.last_element()])
        points = self._shape_with_position.compute_absolute_positions(positions)

        # print 'start, middle, end positions: ', positions
        # print 'start, middle, end points:\n', points
        
        #use the three points to create an interval on the new shape
        #specifically, we compute the union of the short interval between points[0] and points[1], and the short interval
        #between points[1] and points[2]
        new_positions = new_ellipse_with_position.compute_positions(points)
        new_intervals = new_ellipse_with_position.get_intervals_between_positions(new_positions)[:-1]
        new_interval = IntervalOnShape.merge_intervals(new_intervals)[0]

        # print 'new positions: ', new_positions
        # print 'new intervals: ', [ioe._interval for ioe in new_intervals]
        # print 'new interval:  ', new_interval
        
        return new_interval

    def interval_dilation(self, dilation_radius):
        '''
        dilation_radius - a float
        return a new IntervalOnEllipse whose interval has been dilated on both ends by the amount: dilation_radius
        '''
        dilated_interval = self._interval.interval_dilation(dilation_radius)
        return IntervalOnEllipse(self._shape_with_position,
                                 dilated_interval.first_element(), dilated_interval.last_element())

    def interval_erosion(self, erosion_radius):
        '''
        erosion_radius - a float
        return a new IntervalOnEllipse whose interval has been eroded on both ends by the amount: eroded_radius.
        if the interval disappears after the erosion, return None
        '''
        erosion_interval = self._interval.interval_erosion(erosion_radius)
        if erosion_interval is None:
            return None
        
        return IntervalOnEllipse(self._shape_with_position,
                                 erosion_interval.first_element(), erosion_interval.last_element())

                                 

    @classmethod
    def fill_interval_gaps(cls, ellipse_with_position, intervals):
        '''
        ellipse_with_position - an EllipseWithPosition object
        intervals - a list of IntervalOnEllipse objects
        return - an IntervalOnEllipse object. It represents the result of filling the gaps between the input intervals.

        NOTE: For the moment, we are lazy and fill in the entire ellipse. Maybe it would be better to only fill gaps
        that are shorter than a certain arc length.
        '''
        return cls.build_full_interval(ellipse_with_position)
    
