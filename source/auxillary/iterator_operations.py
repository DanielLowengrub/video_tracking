import itertools

def pairwise_iterator(iterator):
    '''
    iterator - an iterator

    Return a new iterator whose elements are (x[i],x[i+1]) where x[i] are the elements of iterator.
    '''

    #make two copies of the iterator
    a,b = itertools.tee(iterator)

    #take off the first element of b
    next(b,None)

    return itertools.izip(a,b)

def iunzip(iterator):
    '''
    iterator - an iterator of tuples: ((x0,y0), (x1,y1),...)
    
    return - a pair of iterators X:(x0,x1,...), Y:(y0,y1,...)
    '''

    X, Y = itertools.tee(iterator, 2)
    X = (x for x,y in X)
    Y = (y for x,y in Y)

    return X,Y
    
def find_sequence(pred, iterator):
    '''
    Return a pair (list of elements, updated iterator)
    the list is a list of elements [x1,...,xn] from the iterator such that x1 is the first element in the iterator
    for which pred(x) = True, and x_{n+1} is the first element after x1 such that cond(x)=False
    the updated iterator starts with x_{n+1}.

    If none of the elements satisfy pred, or if the iterator is empty, then we return ([], empty iterator)
    '''

    sequence = []
    x = next(iterator, None)
    
    while (x is not None):
        #if x satisfies the condition, add it to the sequence and continue to the next element
        if pred(x):
            sequence.append(x)
            x = next(iterator, None)
            continue

        else:
            #if we have not yet found any elements satisfying the condition, check the next element
            if len(sequence) == 0:
                x = next(iterator, None)
                continue

            #if we have already found elements in the sequence, then stop iterating
            #we add x to the beginning of the iterator because we want it to be part of the updated iterator we return
            else:
                iterator = itertools.chain([x], iterator)
                break

        
    return sequence, iterator

def find_initial_sequence(pred, iterator):
    '''
    pred - a function that takes elements of the iterator as arguments and returns true or false.
    iterator - an iterator

    return a tuple (list, iterator)
    Lets denote the elements of the iterator by i0,i1,...
    list - a list [i0,...,ik] such that pred(i0)=...=pred(ik)=True, and pred(i_{k+1})=False.
    iterator - an iterator with elements (i_{k+1},i_{k+1},...)
    '''
    initial_sequence = []
    x = next(iterator, None)

    while x is not None:
        if pred(x):
            initial_sequence.append(x)
            x = next(iterator, None)
            continue
        else:
            #since x is not in the initial sequence, put it back on the iterator
            iterator = itertools.chain([x], iterator)
            break

    return initial_sequence, iterator

def extract_intervals(iterator, intervals):
    '''
    sequence - an iterator for the elements x0,x1,...,xN
    intervals - an iterator for tuples (start0, end0), ... ,(startM, endM) where 
                0 <= start0 < end0 < start1 < end1 ... N
    

    return an iterator of iterators I0,I1,... where Ik is an iterator for the elements x_start_k,...,x_end_k

    Important: You must finish iterating over the elements of Ik before starting to iterate over I{k+1}.
    '''
    # print 'iterator: ', list(iterator)
    # print 'intervals: ', list(intervals)
    
    #the elements of iterator are (x_{current_position}, x_{current_position + 1},...)
    current_position = 0

    for start,end in intervals:
        #first skip from current_position to start.
        #Recall that current_position <= start,
        #and that the iterator currently has the elements (x_{current_position},...)
        skip_size = start - current_position

        #we yield the elements (x_start,...,x_end)
        #also, before doing that we update the current position to end+1 since after yielding,
        #the iterator will have the elements (x_{end+1},...)
        current_position = end+1
        interval_length = end-start
        yield itertools.islice(iterator, skip_size, skip_size + interval_length+1)

def extract_subsequence(iterator, subsequence_indices):
    '''
    iterator - an iterators of elements x0,x1,...
    subsequence_indices - an iterator of integers i0,i1,...
    
    return an iterator of elements x_{i0},x_{i1},...
    '''

    #the current position in the iterator
    current_position = 0

    for i in subsequence_indices:
        #skip forward by (i - current_position) in the iterator
        skip_size = i - current_position
        current_position = i+1

        yield next(itertools.islice(iterator, skip_size, None))

def extract_local_maxima(items, key, bandwidth):
    '''
    items - an iterator of pairs (index, value)
    key - a function which excepts a value from an item and return a number
    bandwidth - an integer

    return - an iterator of items. It contains the items (i,v) that satisfy:
    for all items (k,w) such that i-bandwidth <= k <= i+bandwidth, key(v) >= keys(w)
    '''
    #we store two lists.
    #left_items stores the items (index,value) such that  current_index-bandwidth <= index < current_index
    #right_items stores the items (index,value) such that current_index < index <= current_index+bandwidth
    
    left_items = []
    right_items = []
    current_item = next(items, None)
    
    while current_item is not None:
        current_index, current_value = current_item

        #search the item iterator for new items to add to the right_item list. I.e, items whise index is less than
        #or equal to current_index+bandwidth
        new_right_items, items = find_initial_sequence(lambda x: x[0]<=current_index+bandwidth, items)

        #add these to the current list
        right_items = right_items + new_right_items

        #remove the items in left_items whose index is smaller than current_index-bandwidth
        left_items = [item for item in left_items if current_index-bandwidth<=item[0]]

        # print '   current index,key: ', (current_index, key(current_value))
        # print '   left index,item:   ', [(i,key(v)) for i,v in left_items]
        # print '   right index,item:   ', [(i,key(v)) for i,v in right_items]
        
        is_local_maximum = all(key(current_value) >= key(value) for index,value in left_items+right_items)
        if is_local_maximum:
            yield current_item

        #prepare for the next iteration
        #the current_item will be to the left of the next current_item
        left_items.append(current_item)

        #if the right_items list is not empty, then the first element on that list is the new current_item
        if len(right_items) > 0:
            current_item = right_items[0]
            right_items = right_items[1:]
            
        else:
            current_item = next(items, None)

def izip_dictionary(iterator_dict):
    '''
    iterator_dict - a dictionary of iterators: {k1:(x11,x12,..), k2:(x21,x22,...), ...}
    
    return - an iterator of dictionaries: ({k1:x11, k2:x21,...}, {k1:x12, k2:x22, ...}, ...)
    '''
    keys = iterator_dict.keys()
    iterators = [iterator_dict[k] for k in keys]

    #create an iterator of tuples: ((x11,x21,..), (x12,x22,...), ...)
    tuple_iterator = itertools.izip(*iterators)

    #turn each tuple into a dictionary using the keys
    dict_iterator = (dict(zip(keys,values)) for values in tuple_iterator)

    return dict_iterator

def merge_indexed_and_non_indexed(indexed_iterators, iterators):
    '''
    indexed_iterators - a list of iterators. The l-th indexed iterator is an iterator of elements of the form:
                        [((i0,j0), In_0), ((i1,j1), In_1), ..., ((iK,jK), In_K)]
                        where In_k is an iterator of (jk-ik) objects. 0 <= n <= N.
                        We think of (ik,jk) as an interval and In_k as the elements of indexed_iterator[l] that are associated
                        to this interval.

    iterators is a list of iterators J0,J1,....,JM. All of the iterators Jm must have at least jK+1 elements.

    return - an iterator of tuples 
    [((i0,j0), L0), ((i1,j1), L1), ..., ((iK,jK), LK)]
    where Lk is an iterator of (jk-ik) tuples:
    [(x0_0,...,xN_0,y0_0,...,yM_0), (x0_1,...,yM_1), ..., (x0_{jk-ik}, yM_{jk-ik})]
    xn_l is the l-th element of In_l. ym_l is the (ik + l)-th element of Jm.

    Example:
    indexed_iterators = [I0] = [((0,2), (0,1,2)), ((3,7),(3,4,5,6,7))] <- these are already broken up into intervals
    iterators = [J0] = [(0,2,4,6,...,14)]                              <- these are not broken into intervals

    return:
    [((0,2), zip((0,1,2),(0,2,4)) ), ((3,7), zip((3,4,5,6,7),(6,8,10,12,14)) )]
    Note for instance that in the first tuple: ((0,2), ((0,1,2),(0,2,4)) ), 
    we have zipped the iterator in I0 corresponding to the interval (0,2), and the slice of J0: J0[0:2+1]
    '''
    
    print 'merging indexed and non indexed...'
    if len(indexed_iterators) == 0:
        raise ValueError('there must be at least one indexed iterator')
    
    M = len(iterators)

    #make M+2 copies of the first indexed operator
    iter_copies = itertools.tee(indexed_iterators[0], M+2)
    indexed_iterators[0] = iter_copies[0]

    #use the rest of the copies to build iterators that store the intervals
    interval_iterators = [(interval for interval,I in iter_copy) for iter_copy in iter_copies[1:]]

    #use the first M interval iterators to regroup each of the iterators J in "iterators" by the intervals
    iterators = [extract_intervals(J, interval_iterator)
                 for J,interval_iterator in zip(iterators, interval_iterators)]
    
    #the last interval iterator is used to record the intervals themselves
    interval_iterator = interval_iterators[-1]

    #strip of iterator (ik,jk) off of the elements in the indexed iterators
    indexed_iterators = [(I for interval,I in indexed_iterator) for indexed_iterator in indexed_iterators]

    all_iterators = itertools.izip(*(indexed_iterators + iterators))
    
    return itertools.izip(interval_iterator, all_iterators)

