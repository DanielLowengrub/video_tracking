import numpy as np
import cv2
from .fit_points_to_shape.line import Line
from .fit_points_to_shape.ellipse import Ellipse
from .contour_shape_overlap.highlighted_shape import HighlightedShape
from .contour_shape_overlap.line_with_position import LineWithPosition
from .contour_shape_overlap.ellipse_with_position import EllipseWithPosition
from .contour_shape_overlap.interval_on_line import IntervalOnLine
from .contour_shape_overlap.interval_on_ellipse import IntervalOnEllipse
import planar_geometry
from ..auxillary import mask_operations
from ..auxillary.rectangle import Rectangle

class SoccerFieldGeometry(object):
    '''
    This class encodes the geometry of a soccer field. It is used to get the size of the field, as well as the equations for the various sidelines.
    '''

    #the dimensions of various parts of the field in yards
    GOAL_AREA_WIDTH = 6
    GOAL_AREA_LENGTH = 20

    PENALTY_AREA_WIDTH = 18
    PENALTY_AREA_LENGTH = 44

    PENALTY_CIRCLE_CENTER_DEPTH = 12
    PENALTY_CIRCLE_RADIUS = 10

    CENTER_CIRCLE_RADIUS = 10

    SOCCER_BALL_RADIUS_IN_YARDS = 4.3 / 36.0 #radius of ball in yards
    PLAYER_HEIGHT_IN_YARDS = 2 #height of player
    
    #The type of line in a goal or penalty area
    TOP = 'top'#0
    BOTTOM = 'bottom'#1
    SIDE = 2

    #the side of the field
    RIGHT = 'right'#3
    LEFT = 'left'#4

    #the type of sideline
    RIGHT_SIDELINE = 0
    LEFT_SIDELINE = 1
    TOP_SIDELINE = 2
    BOTTOM_SIDELINE = 3

    #the type of shape on the field
    SIDELINE = 'sideline'#0 #There are 4 types of this line: RIGHT_SIDELINE,...,BOTTOM_SIDELINE
    PENALTY_AREA = 'penalty' #1 #there are 4 types of this line: TOP, BOTTOM, RIGHT, LEFT. TOP (BOTTOM) refers to the line that is common to the top (bottom) of the penalty area.
    GOAL_AREA = 'goal' #2 #similarly there are 4 types of lines here
    CENTER_LINE = 'center line' #3
    PENALTY_CIRCLE = 'penalty circle' #4 #there are two types of ellipses here, RIGHT and LEFT
    CENTER_CIRCLE = 'center circle' #5

    THICKNESS = 3
    
    def __init__(self, yards_to_units_ratio, field_width, field_length, grid_width=None, grid_length=None):
        '''
        yards_to_units_ratio - this is the conversion from yards to the units that field_width and field_length are in
        field_width - the x axis distance spanned by the field
        field_length - the y axis distance spanned by the field
        grid_width   - the number of squares in each row of the grid
        grid_length  - the number of squares in each column of the grid
        Note that these are the only parameters that are not fixed by the rules. Everything else can be computed based on these.
        '''
        self._scale = yards_to_units_ratio
        self._field_width = field_width
        self._field_length = field_length

        #we build a dictionary that stores the highlighted shapes in the field.
        self._highlighted_shape_dict = dict()
        self._build_highlighted_shape_dictionary()

        self._penalty_spots = self._build_penalty_spots()
        
        #also build a grid of points
        self._point_grid = None
        if (grid_width is not None) and (grid_length is not None):
            self._grid_points = self._build_point_grid(grid_width+1, grid_length+1)

        #store the soccer ball radius in absolute coordinates units
        self._soccer_ball_radius = self._scale * self.SOCCER_BALL_RADIUS_IN_YARDS
        self._player_height = self._scale * self.PLAYER_HEIGHT_IN_YARDS
        
    def get_highlighted_shape(self, shape_key):
        '''
        shape_key - the key describing the shape. it is either:
        (SIDELINE, RIGHT), ..., (SIDELINE, BOTTOM)
        (GOAL_AREA, RIGHT), (GOAL_AREA, LEFT), (GOAL_AREA, TOP), (GOAL_AREA, BOTTOM)
        (PENALY_AREA, RIGHT),...,(PENALTY_AREA, BOTTOM)
        CENTER_LINE, CENTER_CIRCLE
        (PENALTY_CIRCLE,RIGHT), (PENALTY_CIRCLE_LEFT)
        '''
        return self._highlighted_shape_dict[shape_key]

    def field_width(self):
        '''
        Return the width of the soccer field. This is the size of the x axis.
        '''
        return self._field_width

    def field_length(self):
        '''
        return the length of the soccer field. This is the size of the y axis.
        '''
        return self._field_length

    def get_field_rectangle(self):
        '''
        Return a Rectangle object bounding the soccer field.
        '''
        top_left = np.array([0,0], np.int32)
        bottom_right = np.array([self.field_width(), self.field_length()], np.int32)

        return Rectangle(top_left, bottom_right)

    def get_soccer_ball_radius(self):
        '''
        return the radius of a soccer ball in the units of the absolute field.
        '''
        return self._soccer_ball_radius

    def get_player_height(self):
        '''
        return the height of a player in world coordinates
        '''
        return self._player_height
    
    def convert_yards_to_world_coords(self, length_in_yards):
        '''
        length_in_yards - float
        return a float. Convert the given length to world coordinates.
        '''
        return self._scale * length_in_yards

    def get_yards_to_world_scale(self):
        return self._scale

    def get_penalty_spots(self):
        '''
        return a tuple of points (left penalty spot, right penalty spot). Each point is a length 2 numpy array.
        '''
        return self._penalty_spots
    
    @classmethod
    def format_key(cls, key):
        if isinstance(key, tuple):
            return '_'.join(key)

        else:
            return key
        
    def get_highlighted_shape_dict(self, format_keys = False):
        if format_keys:
            return dict( (self.format_key(k), v) for k,v in self._highlighted_shape_dict.iteritems())
        
        return self._highlighted_shape_dict
    
    def get_grid_points(self):
        '''
        return a numpy array with shape (num grid points, 2)
        '''
        return self._grid_points

    def get_sideline_mask(self, thickness):
        '''
        thickness - a float
        return a numpy array with the same shape as the absolute field and type np.float32
        It has a 1 exactly at point that are within thickness of a sideline.
        '''

        sideline_mask = np.zeros((self.field_length()+1, self.field_width()+1), np.float32)

        for highlighted_shape in self._highlighted_shape_dict.itervalues():
            shape_mask = highlighted_shape.generate_mask(sideline_mask, thickness=thickness)
            sideline_mask = mask_operations.get_union(sideline_mask, shape_mask)

        return sideline_mask
    
    def _build_goal_area_lines(self):
        '''
        Populate the highlighted_shape dictionary with goal area lines.
        '''

        #start with the top and bottom goal line
        y_positions = {(self.GOAL_AREA, self.TOP):(self._field_length - self._scale * self.GOAL_AREA_LENGTH)/2.0,
                       (self.GOAL_AREA, self.BOTTOM):(self._field_length + self._scale * self.GOAL_AREA_LENGTH)/2.0}
        for key, y_position in y_positions.iteritems():
            line_with_position = LineWithPosition(Line.from_point_and_direction(np.array([0, y_position]), np.array([1,0])))
            interval_1 = line_with_position.get_interval_on_shape(np.array([0, y_position]) ,
                                                                  np.array([self._scale * self.GOAL_AREA_WIDTH, y_position]))
            interval_2 = line_with_position.get_interval_on_shape(np.array([self._field_width-self._scale*self.GOAL_AREA_WIDTH, y_position]),
                                                                  np.array([self._field_width, y_position]))
            self._highlighted_shape_dict[key] = HighlightedShape(line_with_position, self.THICKNESS, [interval_1, interval_2])

        #now the right and left goal line
        x_positions = {(self.GOAL_AREA, self.LEFT):self._scale * self.GOAL_AREA_WIDTH,
                       (self.GOAL_AREA, self.RIGHT):self._field_width - self._scale * self.GOAL_AREA_WIDTH}
        for key, x_position in x_positions.iteritems():
            line_with_position = LineWithPosition(Line.from_point_and_direction(np.array([x_position, 0]), np.array([0,1])))
            interval = line_with_position.get_interval_on_shape(np.array([x_position, (self._field_length - self._scale * self.GOAL_AREA_LENGTH)/2.0]),
                                                                np.array([x_position, (self._field_length + self._scale * self.GOAL_AREA_LENGTH)/2.0]))
            self._highlighted_shape_dict[key] = HighlightedShape(line_with_position, self.THICKNESS, [interval])

        return

    def _build_penalty_area_lines(self):
        '''
        populate the highlighted shape dictionary with penalty area lines
        '''
        #start with the top and bottom goal line
        y_positions = {(self.PENALTY_AREA, self.TOP):(self._field_length - self._scale * self.PENALTY_AREA_LENGTH)/2.0,
                       (self.PENALTY_AREA, self.BOTTOM):(self._field_length + self._scale * self.PENALTY_AREA_LENGTH)/2.0}
        
        for key, y_position in y_positions.iteritems():
            line_with_position = LineWithPosition(Line.from_point_and_direction(np.array([0, y_position]), np.array([1,0])))
            interval_1 = line_with_position.get_interval_on_shape(np.array([0, y_position]) ,
                                                                  np.array([self._scale * self.PENALTY_AREA_WIDTH, y_position]))
            interval_2 = line_with_position.get_interval_on_shape(np.array([self._field_width-self._scale*self.PENALTY_AREA_WIDTH, y_position]),
                                                                  np.array([self._field_width, y_position]))
            self._highlighted_shape_dict[key] = HighlightedShape(line_with_position, self.THICKNESS, [interval_1, interval_2])

        #now the right and left goal line
        x_positions = {(self.PENALTY_AREA, self.LEFT):self._scale * self.PENALTY_AREA_WIDTH,
                       (self.PENALTY_AREA, self.RIGHT):self._field_width - self._scale * self.PENALTY_AREA_WIDTH}
        
        for key, x_position in x_positions.iteritems():
            line_with_position = LineWithPosition(Line.from_point_and_direction(np.array([x_position, 0]), np.array([0,1])))
            interval = line_with_position.get_interval_on_shape(np.array([x_position, (self._field_length - self._scale * self.PENALTY_AREA_LENGTH)/2.0]),
                                                                np.array([x_position, (self._field_length + self._scale * self.PENALTY_AREA_LENGTH)/2.0]))
            self._highlighted_shape_dict[key] = HighlightedShape(line_with_position, self.THICKNESS, [interval])

        return

    def _build_sidelines(self):
        '''
        populate the highlighted shape dictionary with sidelines
        '''

        #top and bottom sidelines
        y_positions = {(self.SIDELINE, self.TOP):0, (self.SIDELINE, self.BOTTOM):self._field_length}

        for key, y_position in y_positions.iteritems():
            line_with_position = LineWithPosition(Line.from_point_and_direction(np.array([0, y_position]), np.array([1,0])))
            interval = line_with_position.get_interval_on_shape(np.array([0, y_position]),
                                                                np.array([self._field_width, y_position]))
            self._highlighted_shape_dict[key] = HighlightedShape(line_with_position, self.THICKNESS, [interval])

        #now the right and left sidelines
        x_positions = {(self.SIDELINE, self.LEFT):0, (self.SIDELINE, self.RIGHT):self._field_width}
        
        for key, x_position in x_positions.iteritems():
            line_with_position = LineWithPosition(Line.from_point_and_direction(np.array([x_position, 0]), np.array([0,1])))
            interval = line_with_position.get_interval_on_shape(np.array([x_position, 0]),
                                                                np.array([x_position, self._field_length]))
            self._highlighted_shape_dict[key] = HighlightedShape(line_with_position, self.THICKNESS, [interval])

        return

    def _build_center_line(self):
        '''
        populate the highlighted shape dictionary with the center line
        '''
        line_with_position = LineWithPosition(Line.from_point_and_direction(np.array([self._field_width/2.0, 0]), np.array([0,1])))
        interval = line_with_position.get_interval_on_shape(np.array([self._field_width/2.0, 0]),
                                                            np.array([self._field_width/2.0, self._field_length]))
        self._highlighted_shape_dict[self.CENTER_LINE] = HighlightedShape(line_with_position, self.THICKNESS, [interval])

        return
    
    def _build_penalty_circles(self):
        '''
        populate the highlighted shape dictionary with the semi-circles of the penalty area
        '''
        axes = np.array([self._scale * self.PENALTY_CIRCLE_RADIUS, self._scale * self.PENALTY_CIRCLE_RADIUS])

        #the right penalty circle
        center = np.array([self._field_width - self._scale*self.PENALTY_CIRCLE_CENTER_DEPTH, self._field_length/2.0])
        ellipse_with_position = EllipseWithPosition(Ellipse.from_geometric_parameters(center, axes, 0))

        #the compute the intersection points of the circle with the right penalty area line
        penalty_area_line = self._highlighted_shape_dict[(self.PENALTY_AREA, self.RIGHT)].get_shape()
        intersection_points = planar_geometry.line_ellipse_intersection(penalty_area_line, ellipse_with_position.get_shape())
        positions_on_ellipse = ellipse_with_position.compute_positions(intersection_points)
        interval = IntervalOnEllipse.build_short_interval(ellipse_with_position, positions_on_ellipse[0], positions_on_ellipse[1])
        self._highlighted_shape_dict[(self.PENALTY_CIRCLE, self.RIGHT)] = HighlightedShape(ellipse_with_position, self.THICKNESS, [interval])

        #the left penalty circle
        center = np.array([self._scale*self.PENALTY_CIRCLE_CENTER_DEPTH, self._field_length/2.0])
        ellipse_with_position = EllipseWithPosition(Ellipse.from_geometric_parameters(center, axes, 0))

        #the compute the intersection points of the circle with the left penalty area line
        penalty_area_line = self._highlighted_shape_dict[(self.PENALTY_AREA, self.LEFT)].get_shape()
        intersection_points = planar_geometry.line_ellipse_intersection(penalty_area_line, ellipse_with_position.get_shape())
        positions_on_ellipse = ellipse_with_position.compute_positions(intersection_points)
        interval = IntervalOnEllipse.build_short_interval(ellipse_with_position, positions_on_ellipse[0], positions_on_ellipse[1])
        self._highlighted_shape_dict[(self.PENALTY_CIRCLE, self.LEFT)] = HighlightedShape(ellipse_with_position, self.THICKNESS, [interval])

        return

    def _build_penalty_spots(self):
        '''
        return a tuple of points (left penalty spot, right penalty spot). Each point is a length 2 numpy array.
        '''
        left_penalty_spot = np.array([self._scale*self.PENALTY_CIRCLE_CENTER_DEPTH, self._field_length/2.0])
        right_penalty_spot = np.array([self._field_width - (self._scale*self.PENALTY_CIRCLE_CENTER_DEPTH),
                                        self._field_length/2.0])

        return left_penalty_spot, right_penalty_spot
    
    def _build_center_circle(self):
        axes = np.array([self._scale * self.CENTER_CIRCLE_RADIUS, self._scale * self.CENTER_CIRCLE_RADIUS])
        #the right penalty circle
        center = np.array([self._field_width/2.0, self._field_length/2.0])
        ellipse_with_position = EllipseWithPosition(Ellipse.from_geometric_parameters(center, axes, 0))
        interval = IntervalOnEllipse.build_full_interval(ellipse_with_position)
        self._highlighted_shape_dict[self.CENTER_CIRCLE] = HighlightedShape(ellipse_with_position, self.THICKNESS, [interval])

        return
    
    def _build_point_grid(self, grid_width, grid_length):
        '''
        Return a list of points. The points lie on a grid with 
        grid_width squares in the x direction and grid_length squares in the y directionx
        '''

        x_ticks = np.linspace(0, self._field_width, grid_width)
        y_ticks = np.linspace(0, self._field_length, grid_length)

        grid_points = np.dstack(np.meshgrid(x_ticks, y_ticks)).reshape((-1,2))

        return grid_points


    def _build_highlighted_shape_dictionary(self):
        '''
        Return a dictionary with the following keys:
        (SIDELINE, RIGHT), ..., (SIDELINE_BOTTOM)
        (GOAL_AREA, RIGHT), (GOAL_AREA, LEFT), (GOAL_AREA, TOP), (GOAL_AREA, BOTTOM)
        (PENALY_AREA, RIGHT),...,(PENALTY_AREA, BOTTOM)
        CENTER_LINE, CENTER_CIRCLE
        (PENALTY_CIRCLE,RIGHT), (PENALTY_CIRCLE_LEFT)

        The values are the corresponding highlighted shapes
        '''

        self._build_goal_area_lines()
        self._build_penalty_area_lines()
        self._build_sidelines()
        self._build_center_line()
        self._build_penalty_circles()
        self._build_center_circle()
        
    def _build_matching_graph(self):
        '''
        Return a ShapeMatchingGraph object
        '''

        #the vertices of the graph are given by the highlighted shape dictionary
        #we now define the edges
        pass

                                       
