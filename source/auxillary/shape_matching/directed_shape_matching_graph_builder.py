import networkx as nx
import itertools, collections
import numpy as np
from ...auxillary import planar_geometry
from .directed_shape_matching_graph import DirectedShapeMatchingGraph
from .shape_matching_graph_builder import ShapeMatchingGraphBuilder
from .shape_intersection_type import ShapeIntersectionType
from ..fit_points_to_shape.line import Line
from ..contour_shape_overlap.line_with_position import LineWithPosition

DEBUG = False

'''
This named tuple stores two versions of a HighlightedShape.
original - the "original" version
noisy - the shape we get when we add the region of a "noise mask" to the highlighted part of the shape.
'''
HighlightedShapeWithNoise = collections.namedtuple('HighlightedShapeWithNoise', ['original', 'noisy'])

class DirectedShapeMatchingGraphBuilder(ShapeMatchingGraphBuilder):
    '''
    This class is in charge of building a shape matching graph out of a list of highlghted shapes on an image.
    '''

    EPSILON = 10**(-5)
    
    def __init__(self, highlighted_line_threshold, highlighted_ellipse_threshold, parallel_lines_threshold):
        '''
        highlighted_line_threshold - this threshold is used to determine if a line is highlighted on a 
                                     certain side of an intersection
        highlighted_ellipse_threshold - this threshold is used to determine if an ellipse is highlighted on a 
                                        certain side of an intersection
        parallel_lines_threshold - if two lines have an angle that is smaller than this we declare them to be parallel
        '''

        super(DirectedShapeMatchingGraphBuilder, self).__init__(highlighted_line_threshold, highlighted_ellipse_threshold,
                                                                parallel_lines_threshold)
        

    def _compute_intersection_type_at_point(self, image, hswn_a, hswn_b, point):
        '''
        image - a numpy array with shape (n,m,3) and type np.uint8
        hswn_a/b - a HighlightedShapeWithNoise object
        point - a length 2 np array

        return a pair of ShapeIntersectionType objects. The first one is the intersection type from a to b and the second
        is the intersection type from b to a.
        '''
        #if the point is not on the image, return one NA edge in each direction
        if not self._point_in_image(point, image):
            return tuple(ShapeIntersectionType.build_not_available_intersection() for i in range(2))
        
        #determine the intersection type based on how much of the highlighted portion
        #of the shapes touch the intersection point
        original_num_directions = tuple(self._compute_number_of_highlighted_directions(hswn.original, point)
                                        for hswn in (hswn_a, hswn_b))
        noisy_num_directions = tuple(self._compute_number_of_highlighted_directions(hswn.noisy, point)
                                     for hswn in (hswn_a, hswn_b))
        
        original_intersection_type = ShapeIntersectionType(original_num_directions)
        noisy_intersection_type = ShapeIntersectionType(noisy_num_directions)

        # print '   original num directions: ', original_num_directions
        # print '   noisy num directions: ', noisy_num_directions

        #if the noisy version does not agree with the original one, then return one NA edge in each direction
        if not (original_intersection_type == noisy_intersection_type):
            return (ShapeIntersectionType.build_not_available_intersection() for i in range(2))

        #if they agree, the the original intersection type is the type of the edge from hswn_a to hswn_b, and the reversed
        #type is the type of the edge from hswn_b to hswn_a
        reversed_intersection_type = ShapeIntersectionType(tuple(reversed(original_num_directions)))
        return (original_intersection_type, reversed_intersection_type)

    def _compute_line_line_intersection_types_on_edges(self, image, hlwn_a, hlwn_b):
        '''
        image - numpy array with shape (n,m,3) and type np.uint8
        hlwn_a, hlwn_b - HighlightedLineWithNoise objects

        return a tuple (types_0, types_1). types_0 is list of the types of the edges from hlwn_a to hlwn_b, 
        and types_1 is a list of the types of the edge in the opposite direction. All types are ShapeIntersectionType objects.

        If there are no edges, return None.
        '''
        if DEBUG:
            print 'computing intersection types for lines...'
        lines = (hlwn_a.original.get_shape(), hlwn_b.original.get_shape())

        #first check if the lines are parallel
        #if so, return one edge in each direction, both with a "parallel" intersection type
        if lines[0].is_parallel(lines[1], self._parallel_lines_threshold):
            #print '   the lines are parallel'
            return tuple([ShapeIntersectionType.build_parallel_intersection()] for i in range(2))
        
        intersection_point = planar_geometry.affine_line_line_intersection(*lines)

        #check if there exists an intersection point
        if intersection_point is None:
            #if the points do not intersect there are no edges
            return None

        if DEBUG:
            print '   intersection point: ', intersection_point
        intersection_types_on_edges = self._compute_intersection_type_at_point(image, hlwn_a, hlwn_b, intersection_point)

        if DEBUG:
            print '   intersection_types_on_edges: ', intersection_types_on_edges
            
        #there in only a single edge in each direction
        return tuple([it] for it in intersection_types_on_edges)
    
    def _compute_line_ellipse_intersection_types_on_edges(self, image, hlwn, hewn):
        '''
        image - numpy array with shape (n,m,3) and type np.uint8
        highlighted_line_with_noise - a HighlightedShapeWithNoise
        highlighted_ellipse_with_noise - a HighlightedShapeWithNoise

        return a tuple (types_0, types_1). types_0 is a list of the types of the edges from hlwn to hewn,
        and types_1 is a list of the types of the edges in the opposite direction. 
        If there are no edges, return None.
        '''
        if DEBUG:
            print 'computing intersection types for line and ellipse...'
        intersection_points = planar_geometry.line_ellipse_intersection(hlwn.original.get_shape(), hewn.original.get_shape())
        if DEBUG:
            print '   intersection points: ', intersection_points

        #if there are no intersection points, then there are no edges between the shapes
        if len(intersection_points) == 0:
            return None

        #the first list stores the intersection types of edges from the line to the ellipse.
        #the second stores intersection types of edges from the ellipse to the line
        intersection_types_on_edges = ([], [])

        #for each intersection point there are two intersection types, one from the line to the ellipse and one from the
        #ellipse to the line. 
        for point in intersection_points:
            line_to_ellipse_type, ellipse_to_line_type = self._compute_intersection_type_at_point(image, hlwn, hewn, point)
            intersection_types_on_edges[0].append(line_to_ellipse_type)
            intersection_types_on_edges[1].append(ellipse_to_line_type)
                
        return intersection_types_on_edges
        
    def _compute_intersection_dictionary(self, image, highlighted_shape_with_noise_dict):
        '''
        image - numpy array with shape (n,m,3) and type np.uint8
        highlighted_shape_with_noise_dict - a dictionary of HighlightedShapeWithNoise objects

        return a dictionary whose keys are tuples (i,j), 0<=i,j<number of highlighted shapes.
        The values are lists of ShapeIntersectionType objects.
        '''        
        intersection_dictionary = dict()

        #iterate over all pairs (without order) of highlighted shapes
        #for each pair, store the intersection types of edges between the shapes in both directions
        #the variable intersection_types and the tuple "edges" have the following meaning:
        #intersection_types[i] is a list of the types of all of the edges between edges[i][0] and edges[i][1], i=0,1
        for (key_a,hswn_a), (key_b,hswn_b) in itertools.combinations(highlighted_shape_with_noise_dict.iteritems(), 2):
            if DEBUG:
                print 'computing intersection types for %s and %s...' % (key_a, key_b)
            intersection_types_on_edges = None
            if hswn_a.original.is_line() and hswn_b.original.is_line():
                intersection_types_on_edges = self._compute_line_line_intersection_types_on_edges(image, hswn_a, hswn_b)
                edges = ((key_a, key_b), (key_b, key_a))
            elif hswn_a.original.is_line() and hswn_b.original.is_ellipse():
                intersection_types_on_edges = self._compute_line_ellipse_intersection_types_on_edges(image, hswn_a, hswn_b)
                edges = ((key_a, key_b), (key_b, key_a))
            elif hswn_a.original.is_ellipse() and hswn_b.original.is_line():
                intersection_types_on_edges = self._compute_line_ellipse_intersection_types_on_edges(image, hswn_b, hswn_a)
                edges = ((key_b, key_a), (key_a, key_b))

            if DEBUG:
                print '   intersection types on edges: ', intersection_types_on_edges
                print '   edges: ', edges
            
            if intersection_types_on_edges is not None:
                #for each of the edges that have been added, record the intersection types of that edge
                intersection_dictionary.update(zip(edges, intersection_types_on_edges))

        return intersection_dictionary

    def _build_highlighted_shape_with_noise_dict(self, noise_mask, highlighted_shape_dict):
        '''
        noise_mask - numpy array with shape (n,m) and type np.float32
        highlighted_shape_dict - a dictionary of HighlightedShape objects

        Return a new dictionary, whose keys are the same as the highlighted_shape_dict, and whose values are HighlightedShapeWithNoise objects.
        For each HighlightedShape object in the highlighted shape dict, we use the noise_mask to create a noisy verion of the highighted shape,
        and store the original version and the noisy version in a HighlightedShapeWithNoise object.
        '''
        return dict((key, HighlightedShapeWithNoise(hs, hs.add_mask(noise_mask)))
                    for key,hs in highlighted_shape_dict.iteritems())

    def build_shape_matching_graph(self, image, noise_mask, highlighted_shape_dict):
        '''
        image - numpy array with shape (n,m,3) and type np.uint8
        noise_mask - numpy array with shape (n,m) and type np.float32
        highlighted_shape_dict - a dictionary of HighlightedShape objects

        We return a ShapeMatchingGraph whose vertices are the highlighted shapes.
        The edges are determined by the intersections of the highlighted portions of the shapes.
        The mask represents a portion of the image that we are uncertain about. 
        So we actually compute the edges twice - once where we ignore the mask and once when we highlight the shapes 
        in all of the regions where the mask is true.
        
        If there is a difference, we mark the edge as 'NA'.
        '''

        highlighted_shape_with_noise_dict = self._build_highlighted_shape_with_noise_dict(noise_mask, highlighted_shape_dict)
        #store it for debugging
        self._highlighted_shape_with_noise_dict = highlighted_shape_with_noise_dict
        
        # print 'highlighted shapes with noise:'
        # for k,hswn in highlighted_shape_with_noise_dict.iteritems():
        #     print 'highlighted shape %s:' % str(k)
        #     print '   original shape: %s' % str(hswn.original) 
        #     print '   noisy_shape: %s' %  str(hswn.noisy)
        
        #build a dictionary that records the intersection type of each pair of shapes
        intersection_dictionary = self._compute_intersection_dictionary(image, highlighted_shape_with_noise_dict)

        if DEBUG:
            print 'the intersection dictionary:'
            print '\n'.join('%s: (%s)' % (edge, ', '.join(map(str, intersection_types)))
                            for edge,intersection_types in intersection_dictionary.iteritems())

        #finally, use the intersection dictionary to build a graph
        return DirectedShapeMatchingGraph(highlighted_shape_dict, intersection_dictionary)
            
