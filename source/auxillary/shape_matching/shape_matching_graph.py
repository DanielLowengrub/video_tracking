import itertools
import networkx as nx
from ..contour_shape_overlap.highlighted_shape import HighlightedShape
import matplotlib.pyplot as plt


class ShapeMatchingGraph(object):
    '''
    This stores a configuration of highlighted shapes. We use these configurations to match up two collections of shapes.

    A configuration is a list of highlihgted shapes, together with the relationship of each pair of shapes.
    We match collections by finding mappings from one list of objects to the other such that the shape-shape relationships are preserved.

    Two highlighted shapes can have the following types of intersections:
    T - the underlying shapes intersect, and the highlighted region of one shape touches one side of the intersection point, whereas the other touches both sides
    X - the underlying shapes intersect, and the highlighted regions of both shapes touch both sides on the intersection point
    L - the underlying shapes intersect, and the highlighted regions of both shapes touch one side of the intersection point
    PARALLEL - the underlying shapes are parallel, and there are no other shapes between them
    NA - the underlying shapes intersect, but because of noise in the image, we can not determine the type

    If the underlying shapes intersect, but the the highlighted regions do not satisfy any of the above criteria, we say that the shapes do not intersect.
    '''

    #These are the different type of shape-shape intersections.
    T = 'T' #One shape passes through the other
    X = 'X' #both shapes pass through one another
    L = 'L' #neither shape passes through the other
    PARALLEL = '||' #the shapes are parallel
    NA = 'NA' #there is not enough information to determine if or how the shapes intersect.

    SHAPE_NAMES = {HighlightedShape.LINE:'line', HighlightedShape.ELLIPSE:'ellipse'}

    SHAPE_TYPE = 'shape_type'
    SHAPE_OBJECT = 'shape_object'
    INTERSECTION_TYPE = 'intersection_type'

    def __init__(self, shape_dict, shape_relation_dict):
        '''
        shape_dict - a dictionary with items (shape index, highlighted shape object)
        shape_relation_dict - a dictionary with items ((shape index 1, shape index 2), intersection type) where intersection type = T,X,L,PARALLEL or NA
        '''

        #we build a networkx graph where the shapes are nodes and the relations are edges
        self._G = nx.MultiGraph()

        #add the nodes
        self._G.add_nodes_from([(shape_key, {self.SHAPE_TYPE:self.SHAPE_NAMES[shape.get_shape_type()],
                                             self.SHAPE_OBJECT:shape})
                                for shape_key,shape in shape_dict.iteritems()])

        #add the edges
        for shape_keys, intersection_types in shape_relation_dict.iteritems():
            self._G.add_edges_from([shape_keys +  ({self.INTERSECTION_TYPE:intersection_type},)
                                    for intersection_type in intersection_types])

        #print 'edges: '
        #print self._G.edges(data=True)

    def __str__(self):
        node_string = '\n'.join('%s: shape type = %s' % (k, d[self.SHAPE_TYPE]) for k,d in self._G.nodes(data=True))
        edge_string = '\n'.join('%s: intersection type = %s' % ((n1,n2), str(d[self.INTERSECTION_TYPE])) for n1,n2,d in self._G.edges(data=True))
        return 'nodes:\n%s\nedges:\n%s' % (node_string, edge_string)

    def __repr__(self):
        return str(self)

    def __getitem__(self, node_key):
        #the node is a tuple (node, node attribute dictionary)
        highlighted_shape = self._G.node[node_key][self.SHAPE_OBJECT]
        return highlighted_shape
    
    def get_highlighted_shape(self, shape_key):
        '''
        shape_key - a node in the shape matching graph

        return a HighlightedShape object which corresponds to the shape at that node.
        '''

        return self._G.node[shape_key][self.SHAPE_OBJECT]
    
    def get_highlighted_shapes_iter(self):
        '''
        return a list of HighlightedShape objects. It is a list of the highlighted shapes in this graph.
        '''
        #nodes_iter is an iterator of tuples (node key, {node attribute dictionary})
        nodes_iter = self._G.nodes_iter(data=True)
        highlighted_shapes_iter = (node[1][self.SHAPE_OBJECT] for node in nodes_iter)
        return highlighted_shapes_iter

    def number_of_nodes(self):
        return len(self._G)
    
    @classmethod
    def _node_matcher(cls, node_attributes_1, node_attributes_2):
        '''
        Return true iff the shapes corresponding to the nodes have the same type.
        '''

        return node_attributes_1[cls.SHAPE_TYPE] == node_attributes_2[cls.SHAPE_TYPE]

    @classmethod
    def _edge_matcher(cls, edge_attributes_1, edge_attributes_2):
        '''
        edge_attributes_i holds a dictionary with items (edge index, attribute dictionary)
        Return true iff there is some permutation of the edges such that the corresponding ShapeIntersectionTypes match
        '''
        #print 'edge attributes 1: ', edge_attributes_1
        #print 'edge attributes 2: ', edge_attributes_2
        
        #first make sure that both dictionaries store the same number of edges
        if not len(edge_attributes_1) == len(edge_attributes_2):
            return False

        intersection_types_1 = [ea[cls.INTERSECTION_TYPE] for ea in edge_attributes_1.itervalues()]
        intersection_types_2 = [ea[cls.INTERSECTION_TYPE] for ea in edge_attributes_2.itervalues()]
        
        #check if there is some permutation of intersection_types_2 such that the types match intersection types 1
        intersection_types_permutations = itertools.permutations(intersection_types_2)
        return any(all(it_1.matches(it_2) for it_1,it_2 in zip(intersection_types_1, intersection_types_permutation))
                   for intersection_types_permutation in intersection_types_permutations)

    def compute_graph_embeddings(self, subgraph):
        '''
        subgraph - a ShapeMatchingGraph

        Return a list of all of the embeddings of sub_graph into this graph.
        An embedding F is encoded as a list of tuples of the form (node in sub graph, F(node) in this graph)
        '''

        #initialize the graph matcher
        graph_matcher = nx.algorithms.isomorphism.GraphMatcher(self._G, subgraph._G, node_match=self._node_matcher, edge_match=self._edge_matcher)

        subgraph_isomorphisms = graph_matcher.subgraph_isomorphisms_iter()
        return list(subgraph_isomorphisms)

    def find_parallel_line_components(self):
        '''
        return a tuple of sets (component_0, ..., components_n).
        Each component contains a set of shape keys.
        Each component represents a connected component of the set of lines in the shape matching graph w.r.t equivalence
        relation: L1 ~ L2 <=> L1 is parallel to L2.
        '''
        #first create a subgraph of the _shape_matching_graph which contains only the line nodes, and the edges whose type
        #is "parallel"
        line_nodes = [node for node,node_attr in self._G.nodes(data=True) if node_attr[self.SHAPE_OBJECT].is_line()]
        parallel_edges = [(start_node, end_node) for start_node,end_node,edge_attr in self._G.edges(data=True)
                          if edge_attr[self.INTERSECTION_TYPE].is_parallel()]
        
        parallel_graph = nx.Graph()
        parallel_graph.add_nodes_from(line_nodes)
        parallel_graph.add_edges_from(parallel_edges)

        parallel_components = nx.connected_components(parallel_graph)

        return tuple(parallel_components)
            
    def draw_graph(self):
        '''
        Use matplotlib to display the graph
        '''

        pos = nx.spring_layout(self._G, k=100)

        nx.draw_networkx_nodes(self._G, pos, node_size=1000)
        nx.draw_networkx_edges(self._G, pos)
        node_labels = nx.get_node_attributes(new_graph,self.SHAPE_TYPE)
        nx.draw_networkx_labels(new_graph, pos, labels = node_labels)
        edge_labels = nx.get_edge_attributes(new_graph,self.INTERSECTION_TYPE)
        edge_labels = dict((k,str(v)) for k,v in edge_labels.iteritems())
        #print 'edge labels: ', edge_labels
        nx.draw_networkx_edge_labels(new_graph, pos, edge_labels = edge_labels)
        plt.show()

            

        
