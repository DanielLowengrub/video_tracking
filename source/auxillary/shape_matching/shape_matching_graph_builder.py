import networkx as nx
import itertools, collections
import numpy as np
from ...auxillary import planar_geometry
from .shape_matching_graph import ShapeMatchingGraph
from .shape_intersection_type import ShapeIntersectionType
from ..fit_points_to_shape.line import Line
from ..contour_shape_overlap.line_with_position import LineWithPosition

'''
This named tuple stores two versions of a HighlightedShape.
original - the "original" version
noisy - the shape we get when we add the region of a "noise mask" to the highlighted part of the shape.
'''
HighlightedShapeWithNoise = collections.namedtuple('HighlightedShapeWithNoise', ['original', 'noisy'])

class ShapeMatchingGraphBuilder(object):
    '''
    This class is in charge of building a shape matching graph out of a list of highlghted shapes on an image.
    '''

    EPSILON = 10**(-5)
    LINE_LINE_INTERSECTION_KEY = 0
    
    def __init__(self, highlighted_line_threshold, highlighted_ellipse_threshold, parallel_lines_threshold):
        '''
        highlighted_line_threshold - this threshold is used to determine if a line is highlighted on a certain side of an intersection
        highlighted_ellipse_threshold - this threshold is used to determine if an ellipse is highlighted on a certain side of an intersection
        parallel_lines_threshold - if two lines have an angle that is smaller than this we declare them to be parallel
        '''

        self._highlighted_line_threshold = highlighted_line_threshold
        self._highlighted_ellipse_threshold = highlighted_ellipse_threshold
        self._parallel_lines_threshold = parallel_lines_threshold


    def _point_in_image(self, point, image):
        return (0 <= point).all() and (point < np.array([image.shape[1], image.shape[0]])).all()
    
    def _compute_number_of_highlighted_directions(self, highlighted_shape, point):
        '''
        highlighted_shape - a HighlightedShape object
        point - a length 2 numpy array

        Return either 0,1 or 2 depending on the number of sides of the point that are highlighted.
        For example:
        ====*====  => 2
        ----*====  => 1
        ====*----  => 1
        ----*----  => 0
        '''

        #print 'computing the number of highlighted directions on the shape:'
        #print str(highlighted_shape)
        #print 'w.r.t point: ', point
        
        #get the position of the point on the line
        shape_with_position = highlighted_shape.get_shape_with_position()
        position = shape_with_position.compute_position(point)

        #print '   point position: ', position
        
        #get intervals the two intervals *----- and -----* that start and end at the given position
        interval_length = self._highlighted_line_threshold if highlighted_shape.is_line() else self._highlighted_ellipse_threshold
        intervals = [shape_with_position.get_interval_from_position_and_increment(position + i*highlighted_shape.get_thickness(), i*interval_length) for i in (-1,1)]

        #print '   test intervals: ', map(str, intervals)
        
        #check how many of the intervals in contained in the highlighted shape
        num_intervals_in_highlighted_region = sum(highlighted_shape.contains_interval(interval) for interval in intervals)

        return num_intervals_in_highlighted_region

    def _compute_line_line_intersection_type(self, image, highlighted_line_with_noise_a, highlighted_line_with_noise_b):
        '''
        image - numpy array with shape (n,m,3) and type np.uint8
        highlighted_line_with_noise_a, highlighted_line_with_noise_b - HighlightedLineWithNoise objects

        We return either None, or one of the intersection types defined in ShapeMatchingGraph
        '''
        original_highlighted_lines = (highlighted_line_with_noise_a.original, highlighted_line_with_noise_b.original)
        noisy_highlighted_lines = (highlighted_line_with_noise_a.noisy, highlighted_line_with_noise_b.noisy)

        # print 'computing intersection type of:'
        # for i in range(2):
        #     print '   original: %s, noisy: %s' % (str(original_highlighted_lines[i]), str(noisy_highlighted_lines[i]))
        
        lines = tuple(ohl.get_shape() for ohl in original_highlighted_lines)

        #first check if the lines are parallel
        if lines[0].is_parallel(lines[1], self._parallel_lines_threshold):
            #print '   the lines are parallel'
            return ShapeIntersectionType.build_parallel_intersection()
        
        projective_intersection = planar_geometry.projective_line_line_intersection(*lines)

        #check if the point is in affine space
        if np.abs(projective_intersection[2]) < self.EPSILON:
            #if it is not, then the points do not intersect (since we already know they are not parallel
            return None

        #get the affine intersection point
        intersection_point = projective_intersection[:2] / projective_intersection[2]

        # print '   intersection point: ', intersection_point
        
        #if the point is not on the image, return NA
        if not self._point_in_image(intersection_point, image):
            return ShapeIntersectionType.build_not_available_intersection()
        
        #determine the intersection type based on how much of the highighted portion of the lines touch the intersection point
        original_num_directions = tuple(self._compute_number_of_highlighted_directions(ohl, intersection_point) for ohl in original_highlighted_lines)
        noisy_num_directions = tuple(self._compute_number_of_highlighted_directions(nhl, intersection_point) for nhl in noisy_highlighted_lines)

        original_intersection_type = ShapeIntersectionType(original_num_directions)
        noisy_intersection_type = ShapeIntersectionType(noisy_num_directions)

        # print '   original num directions: ', original_num_directions
        # print '   noisy num directions: ', noisy_num_directions
        
        if not (original_intersection_type == noisy_intersection_type):
            return ShapeIntersectionType.build_not_available_intersection()

        return original_intersection_type

    def _compute_line_ellipse_intersection_type(self, image, highlighted_line_with_noise, highlighted_ellipse_with_noise):
        '''
        image - numpy array with shape (n,m,3) and type np.uint8
        highlighted_line_with_noise - a HighlightedShapeWithNoise
        highlighted_ellipse_with_noise - a HighlightedShapeWithNoise

        Return a either None, or a dictionary of intersection types parameterized by the two intersection points.
        '''
        original_highlighted_line = highlighted_line_with_noise.original
        noisy_highlighted_line = highlighted_line_with_noise.noisy
        original_highlighted_ellipse = highlighted_ellipse_with_noise.original
        noisy_highlighted_ellipse = highlighted_ellipse_with_noise.noisy

        print 'computing intersection type of:'
        print 'original: %s, noisy: %s' % (str(original_highlighted_line), str(noisy_highlighted_line))
        print 'original: %s, noisy: %s' % (str(original_highlighted_ellipse), str(noisy_highlighted_ellipse))
        
        intersection_points = planar_geometry.line_ellipse_intersection(original_highlighted_line.get_shape(), original_highlighted_ellipse.get_shape())

        print '   intersection points: ', intersection_points
        
        if len(intersection_points) == 0:
            return None

        intersection_types = []

        for point in intersection_points:
            print '   computing intersection type of point: %s' % point
            if not self._point_in_image(point, image):
                print '   intersection point is not in image. setting type to NA'
                intersection_types.append(ShapeIntersectionType.build_not_available_intersection())
                continue

            #determine the intersection type based on how much of the highighted portion of the lines touch the intersection point
            original_num_line_directions = self._compute_number_of_highlighted_directions(original_highlighted_line, point)
            noisy_num_line_directions = self._compute_number_of_highlighted_directions(noisy_highlighted_line, point)
            original_num_ellipse_directions = self._compute_number_of_highlighted_directions(original_highlighted_ellipse, point)
            noisy_num_ellipse_directions = self._compute_number_of_highlighted_directions(noisy_highlighted_ellipse, point)

            #print '   original num line directions: %s, noisy num line directions: %s' % (original_num_line_directions, noisy_num_line_directions)
            #print '   original num ellipse directions: %s, noisy num ellipse directions: %s' % (original_num_ellipse_directions, noisy_num_ellipse_directions)

            original_intersection_type = ShapeIntersectionType((original_num_line_directions, original_num_ellipse_directions))
            noisy_intersection_type = ShapeIntersectionType((noisy_num_line_directions, noisy_num_ellipse_directions))

            if original_intersection_type == noisy_intersection_type:
                intersection_types.append(original_intersection_type)
            else:
                intersection_types.append(ShapeIntersectionType.build_not_available_intersection())

        return tuple(intersection_types)

    def _find_consecutive_parallel_lines(self, image, parallel_line_dict):
        '''
        parallel_line_dict - a dictionary of parallel Line objects

        Return a list of pairs (key_1,key_2) such that there are no lines between parallel_lines[key_1] and parallel_lines[key_2] 
        '''
        keys = parallel_line_dict.keys()

        #print 'finding consecutive parallel lines with keys: ', keys
        
        #build a line pointing in the normal direction to the common direction of the lines
        normal_direction = parallel_line_dict[keys[0]].get_normal_vector()
        image_center = np.array([image.shape[1]/2.0, image.shape[0]/2.0])
        normal_line = Line.from_point_and_direction(image_center, normal_direction)

        #print 'normal line: ', normal_line
        #get the positions of the intersection points of the parallel lines with the "normal line"
        projective_intersection_points = [planar_geometry.projective_line_line_intersection(normal_line, parallel_line_dict[k]) for k in keys]
        intersection_points = [point[:2] / point[2] for point in projective_intersection_points]
        normal_line_with_position = LineWithPosition(normal_line)
        intersection_point_positions = np.array([normal_line_with_position.compute_position(point) for point in intersection_points])

        #print 'intersection_point_positions: ', intersection_point_positions
        
        #now sort the parallel lines according to their positions on the normal line
        sorted_indices = np.argsort(intersection_point_positions)

        #print 'sorted_indices: ', sorted_indices
        #return a list of pairs (sorted_indices[i], sorted_indices[i+1])
        return [(keys[i], keys[j]) for i,j in zip(sorted_indices[:-1], sorted_indices[1:])]

        
    def _compute_intersection_dictionary(self, image, highlighted_shape_with_noise_dict):
        '''
        image - numpy array with shape (n,m,3) and type np.uint8
        highlighted_shape_with_noise_dict - a dictionary of HighlightedShapeWithNoise objects

        return a dictionary whose keys are tuples (i,j), 0<=i<j=number of highlighted shapes.
        The values are the ShapeIntersectionType objects.
        '''

        #this dictionary has items ((shape a, shape b), dictionary of intersection types of the intersections of the shapes)
        #if the two shapes are lines, there is only one intersection point and we store its type with the key self.LINE_LINE_INTERSECTION_KEY.
        #if one of the shapes is an ellipse, we index the intersection types by the intersection point.
        
        intersection_dictionary = dict()
        parallel_lines_graph = nx.Graph()

        #iterate over all pairs of highlighted shapes
        for (key_a,hswn_a), (key_b,hswn_b) in itertools.combinations(highlighted_shape_with_noise_dict.iteritems(), 2):
            intersection_types = None
            if hswn_a.original.is_line() and hswn_b.original.is_line():
                intersection_types = (self._compute_line_line_intersection_type(image, hswn_a, hswn_b),)

            elif hswn_a.original.is_line() and hswn_b.original.is_ellipse():
                intersection_types = self._compute_line_ellipse_intersection_type(image, hswn_a, hswn_b)

            elif hswn_a.original.is_ellipse() and hswn_b.original.is_line():
                intersection_types = self._compute_line_ellipse_intersection_type(image, hswn_b, hswn_a)

            #record the intersection type
            if intersection_types is not None:
                #if the lines are parallel, add an edge to the parallel lines graph
                #if len(intersection_types)==1 and intersection_types[0].is_parallel():
                #    parallel_lines_graph.add_edge(key_a, key_b)
                #else:
                intersection_dictionary[(key_a, key_b)] = intersection_types

        #we now consider all connected components of parallel lines.
        #for parallel_line_keys in nx.connected_components(parallel_lines_graph):
        #    parallel_line_dict = dict((k, highlighted_shape_with_noise_dict[k].original.get_shape()) for k in parallel_line_keys)
        #    #print '   parallel line dict:'
        #    #print '\n'.join('%s: %s' % (k,str(v)) for k,v in parallel_line_dict.iteritems())
        #    
        #    intersection_dictionary.update((k, (ShapeIntersectionType.build_parallel_intersection(),))
        #                                   for k in self._find_consecutive_parallel_lines(image, parallel_line_dict))
            
        return intersection_dictionary

    def _build_highlighted_shape_with_noise_dict(self, noise_mask, highlighted_shape_dict):
        '''
        noise_mask - numpy array with shape (n,m) and type np.float32
        highlighted_shape_dict - a dictionary of HighlightedShape objects

        Return a new dictionary, whose keys are the same as the highlighted_shape_dict, and whose values are HighlightedShapeWithNoise objects.
        For each HighlightedShape object in the highlighted shape dict, we use the noise_mask to create a noisy verion of the highighted shape,
        and store the original version and the noisy version in a HighlightedShapeWithNoise object.
        '''
        return dict((key, HighlightedShapeWithNoise(hs, hs.add_mask(noise_mask))) for key,hs in highlighted_shape_dict.iteritems())

    def build_shape_matching_graph(self, image, noise_mask, highlighted_shape_dict):
        '''
        image - numpy array with shape (n,m,3) and type np.uint8
        noise_mask - numpy array with shape (n,m) and type np.float32
        highlighted_shape_dict - a dictionary of HighlightedShape objects

        We return a ShapeMatchingGraph whose vertices are the highlighted shapes.
        The edges are determined by the intersections of the highlighted portions of the shapes.
        The mask represents a portion of the image that we are uncertain about. So we actually compute the edges twice - once where we ignore the mask
        and once when we highlight the shapes in all of the regions where the mask is true.
        If there is a difference, we mark the edge as 'NA'.
        '''

        highlighted_shape_with_noise_dict = self._build_highlighted_shape_with_noise_dict(noise_mask, highlighted_shape_dict)
        #store it for debugging
        self._highlighted_shape_with_noise_dict = highlighted_shape_with_noise_dict
        
        # print 'highlighted shapes with noise:'
        # for k,hswn in highlighted_shape_with_noise_dict.iteritems():
        #     print 'highlighted shape %s:' % str(k)
        #     print '   original shape: %s' % str(hswn.original) 
        #     print '   noisy_shape: %s' %  str(hswn.noisy)
        
        #build a dictionary that records the intersection type of each pair of shapes
        intersection_dictionary = self._compute_intersection_dictionary(image, highlighted_shape_with_noise_dict)

        # print 'the intersection dictionary:'
        # print '\n'.join('%s: (%s)' % (edge, ', '.join(map(str, intersection_types)))
        #                               for edge,intersection_types in intersection_dictionary.iteritems())

        #finally, use the intersection dictionary to build a graph
        return ShapeMatchingGraph(highlighted_shape_dict, intersection_dictionary)
            
