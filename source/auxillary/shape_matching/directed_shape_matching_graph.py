import itertools
import networkx as nx
from ..contour_shape_overlap.highlighted_shape import HighlightedShape
from .shape_matching_graph import ShapeMatchingGraph


class DirectedShapeMatchingGraph(ShapeMatchingGraph):
    '''
    This stores a configuration of highlighted shapes. We use these configurations to match up two collections of shapes.

    A configuration is a list of highlihgted shapes, together with the relationship of each ordered pair of shapes.
    We match collections by finding mappings from one list of objects to the other such that the shape-shape relationships are preserved.

    Two highlighted shapes can have the following types of intersections:
    (1,2) - the underlying shapes intersect, and the highlighted region of shape A touches one side of the intersection point, 
        whereas shape B touches both sides. In this case we have an edge labelled (1,2) from A to B: A -(1,2)-> B and an
        edge labelled (2,1) from B to A: B -(2,1)-> A
    (0,2) - the underlying shapes intersect, and the highlighted region of shape A touches zero sides of the 
            intersection point, whereas shape B touches both sides. 
            In this case we have an edge labelled (0,2) from A to B: A -(0,2)-> B
            and an edge labelled (2,0) from B to A: B -(2,0)-> A
    X - the underlying shapes intersect, and the highlighted regions of both shapes touch both sides on the intersection point.
        In this case, there are edges from A to B and from B to A: A -X-> B, B -X-> A
    L - the underlying shapes intersect, and the highlighted regions of both shapes touch one side of the intersection point.
        In this case there are edges from A to B and from B to A: A -L-> B, B -L-> A
    PARALLEL - the underlying shapes are parallel, and there are no other shapes between them.
        In this case there are edges from A to B and from B to A: A -||-> B, B -X-> A
    NA - the underlying shapes intersect, but because of noise in the image, we can not determine the type
        In this case there are edges from A to B and from B to A: A -NA-> B, B -NA-> A

    If the underlying shapes intersect, but the the highlighted regions do not satisfy any of the above criteria, we say that the shapes do not intersect.
    '''

    def __init__(self, shape_dict, shape_relation_dict):
        '''
        shape_dict - a dictionary with items (shape index, highlighted shape object)
        shape_relation_dict - a dictionary with items ((shape index 1, shape index 2), intersection_types) where 
            intersection_types is a list of ShapeIntersectionType objects
        '''

        #we build a networkx directed graph where the shapes are nodes and the relations are edges
        self._G = nx.MultiDiGraph()

        #add the nodes
        self._G.add_nodes_from([(shape_key, {self.SHAPE_TYPE:self.SHAPE_NAMES[shape.get_shape_type()],self.SHAPE_OBJECT:shape})
                                for shape_key,shape in shape_dict.iteritems()])

        #add the edges
        for shape_keys, intersection_types in shape_relation_dict.iteritems():
            self._G.add_edges_from([shape_keys +  ({self.INTERSECTION_TYPE:intersection_type},)
                                    for intersection_type in intersection_types])

    def compute_graph_embeddings(self, subgraph):
        '''
        subgraph - a ShapeMatchingGraph

        Return a list of all of the embeddings of sub_graph into this graph.
        An embedding F is encoded as a list of tuples of the form (node in sub graph, F(node) in this graph)
        '''

        #initialize the graph matcher
        graph_matcher = nx.algorithms.isomorphism.DiGraphMatcher(self._G, subgraph._G, node_match=self._node_matcher,
                                                                 edge_match=self._edge_matcher)

        subgraph_isomorphisms = graph_matcher.subgraph_isomorphisms_iter()
        return list(subgraph_isomorphisms)
            

        
