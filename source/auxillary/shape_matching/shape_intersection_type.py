#WARNING: The old version of ShapeMatchingGraphBuilder assumes that the variable _number_of_highlighted_sides is sorted!!


class ShapeIntersectionType(object):
    '''
    objects of this class represent the type of an intersection between two HighlightedShape objects.

    We currently implement a basic version that records the following information about an intersection:
    * (i,j) - i,j = 0,1,2. i records the number of directions away from the intersection point on the first shape 
      that are highlighted. j does the same for the second shape.
    
    for example:
                |
                |
          ------*======
                |
                |
    would have the value (0,1) since the first (vertical) shape is not highlighted at all near the point, and the second shape is highlighted on only one side.

    * PARALLEL - true or false. true iff the shapes are two parallel lines and there are no parallel lines between them.
    * NA - true or false. true iff there is not enough information to determine the intersection type

    We also implement methods for comparing intersection types, and hashing.
    '''

    NUM_SIDES_STRINGS = {(2,2):'X', (1,2):'-|', (2,1):'|-', (1,1):'L'}
    PARALLEL_STRING = '||'
    NOT_AVAILABLE_STRING = 'NA'

    def __init__(self, number_of_highlighted_sides, parallel=False, not_available=False):
        '''
        number_of_highlighted_sides - a tuple (i,j) where i,j = 0,1,2. See docstring for explanation. 
            It can be None if either parallel or not_available are True.
        parallel - True iff the shapes are two parallel lines and there are no parallel lines between them.
        not_available - True iff there is not enough information to determine the intersection.
        '''
        
        self._number_of_highlighted_sides = number_of_highlighted_sides
        self._parallel = parallel
        self._not_available = not_available

    def __repr__(self):
        return str(self)
    
    def __str__(self):
        #print 'num sides: ', self._number_of_highlighted_sides
        if self._parallel:
            return self.PARALLEL_STRING

        if self._not_available:
            return self.NOT_AVAILABLE_STRING

        if self._number_of_highlighted_sides in self.NUM_SIDES_STRINGS:
            return self.NUM_SIDES_STRINGS[self._number_of_highlighted_sides]

        return str(self._number_of_highlighted_sides)

    def __hash__(self):
        if self._parallel:
            return hash(self.PARALLEL_STRING)

        if self._not_available:
            return hash(self.NOT_AVAILABLE_STRING)

        return hash(self._number_of_highlighted_sides)

    def __eq__(self, other):
        if self._not_available and other._not_available:
            return True

        if self._parallel and other._parallel:
            return True

        return self._number_of_highlighted_sides == other._number_of_highlighted_sides

    def is_parallel(self):
        return self._parallel

    def matches(self, other):
        '''
        Return true iff this shape intersection matches the other one.
        This happens if either one of the intersections is NA, or the intersections are equal
        '''

        if self._not_available or other._not_available:
            return True

        return self == other
    
    
    @classmethod
    def build_parallel_intersection(cls):
        return cls((0,0), True, False)

    @classmethod
    def build_not_available_intersection(cls):
        return cls((0,0), False, True)

        
