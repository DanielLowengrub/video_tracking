from multiprocessing import Process, Event, Queue
import signal
import sys

"""
This file contains an implementation of a pool of workers that write an iterator of data.
It is designed such that it can exit gracefully when the writing process fails, or
the user sends a KeyboardInterrupt exception.
In particular, it does not exit in the middle of writing a piece of data
"""

class DataProcessor(object):
    '''
    This class is in charge of processing data such that it can exit quickly if exit_event is set, without corrupting
    the data.
    '''
    def initialize(self):
        return

    def close(self):
        return
    
    def process_data(self, data, exit_event):
        '''
        data - an object
        exit_event - an Event object
        '''
        raise NotImplementedError('process_data has not been implemented')

class Worker(Process):
    '''
    This is a Process which applies a DataWriter to elements of a Queue. It exits quickly and gracefully
    if exit_event is set.
    '''
    def __init__(self, data_processor, queue, exit_event):
        '''
        data_processor - a DataWriter object
        queue - a Queue. The worker passes elements of the queue to the data writer.
        exit_event - an Event object. The worker should exit quickly if this becomes set.
        '''
        super(Worker, self).__init__()

        self._data_processor = data_processor
        self._queue = queue
        self._exit_event = exit_event
        
    def _initialize_run(self):
        '''
        this is called after the Worker process is spawned.
        '''
        signal.signal(signal.SIGINT, signal.SIG_IGN)
        self._data_processor.initialize()
        return

    def _cleanup_run(self):
        '''
        this is called before the Worker process is terminated.
        '''
        self._data_processor.close()
        return
    
    def run(self):
        '''
        This implements the run method of the parent Process.
        '''
        self._initialize_run()

        #keep processing the data in the queue until we get a None object
        for data in iter(self._queue.get, None):
            if self._exit_event.is_set():
                break
            
            self._data_processor.process_data(data, self._exit_event)

        self._cleanup_run()
        return
                 
class Pool(object):
    '''
    This class spawns threads containing worker processes and uses them to write an iterator of data with multiple
    threads.
    '''
    def __init__(self, num_workers, data_processor, data_iter):
        self._data_processor = data_processor
        self._data_iter = data_iter
        self._queue = Queue()
        self._exit_event = Event()

        self._workers = [Worker(data_processor, self._queue, self._exit_event) for i in range(num_workers)]

    def _run_no_workers(self):
        '''
        process the data on the same processor (no multiprocessing)
        '''
        self._data_processor.initialize()
        for data in self._data_iter:
            self._data_processor.process_data(data, None)

        self._data_processor.close()
        return
    
    def run(self):
        if len(self._workers) == 0:
            self._run_no_workers()
            return
        
        try:
            #spawn the worker threads
            for worker in self._workers:
                worker.start()

            #put the data in the queue
            for data in self._data_iter:
                self._queue.put(data)

            #add senetils for the end of the data
            for worker in self._workers:
                self._queue.put(None)
        except:
            print 'could not set up the data writing pool'
            raise

        try:
            for worker in self._workers:
                worker.join()

        except:
            #notify the writers that they should stop writing
            self._exit_event.set()

            #wait for them to finish up
            for worker in self._workers:
                worker.join()
        return
