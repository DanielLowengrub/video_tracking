import numpy as np

class FindShapeInCvContour(object):
    '''
    The job of this class is to find a shape in a (opencv) contour object.

    We do this as follows:
    Parameters:
        * fit_points - a FitPointsToShape object that we use to fit a collection of points to a shape
        * min_points_in_shape - an integer
        * extension_step_size - an integer
        * scanning_step_size - an integer
        * first_point = 0

    1) Set the range of points we are considering to [first_point, first_point + min_points_in_shape]. Call this point_range.
    2) Use fit_points to try to fit the points in point_range to a shape. If this fails, set the point range to point_range + [scanning_step_size, scanning_step_size]
       and go back to 2. Otherwise, go to step 3.
    3) Now that we've succeeded to match the points in point_range to a shape, keep adding extension_size amount of points to each end of the range until we can't
       fit the points any longer. Add the resulting shape to our list of shapes. Then set first_point to be the last point in the shape and go to step 1.
    4) We now have a list of all shapes that we produced in step(s) 3. In this step we try to merge these shapes together by taking the union of their points and trying
       to fit them to a shape. The point of this step is that there may be non-consecutive sequences of points that all match the same shape.

    It is up to the child classes to implement the following methods:
        * _is_match - takes a set of points and returns whether it is a match.
        * _get_match_distance - takes a set of points and returns the "distance" between them and the best match.

    '''

    def __init__(self, min_points_in_shape, min_arc_length_of_shape_segment, extension_step_size, scanning_step_size):
        '''
        fit_points - a FitPointsToShape object.
        min_points_in_shape - an integer. This is the smallest number of points that can be used to detect a shape.
        extension_step_size - after fitting a sequence of points to a shape, we itereratively try to add on extension_step_size points to each end.
        scanning_step_size - an integer. If the points in the range (a,b) did not fit the shape, we move on to the points (a+scanning_step_size, b+scanning_step_size).
        '''

        self._min_points_in_shape = min_points_in_shape
        self._min_arc_length_of_shape_segment = min_arc_length_of_shape_segment
        self._extension_step_size = extension_step_size
        self._scanning_step_size = scanning_step_size

        self._first_shape_index = 0
        self._points = None
        self._point_mask = None
        self._arc_distance_from_start = None
        
        return


    def _is_match(self, points):
        pass
    
    def _is_contiguous_segment(self, points):
        '''
        points - a numpy array of shape (number or points, 2)

        We return True if the points fit a contiguous segment on the shape
        '''

        pass

    def _on_same_shape(self, pointsA, pointsB):
        '''
        pointsA, pointsB - numpy arrays of shape (number of points, 2)
        
        We assume the points in each set lie on the shape. 
        Return a tuple (should merge, distance to shape)
        
        If the two sets of points lie on the same shape, return (True, distance from union of points to shape)
        Otherwise, return (False, np.inf)
        '''
        
        pass

    def _verify_match(self, shape, points):
        '''
        shape - a planar shape
        points - a numpy array of shape (number of points, 2)

        Return True if the points actually match the shape. This is called right before sending this back to the user as a match.
        '''
        pass
    
    def _get_match_distance(self, points):
        '''
        points - a numpy array of shape (number or points, 2)
        
        Return a number indicating how good of a fit the shape is to the points in the given range.
        A smaller number means a better match.

        If the points don't even match the shape we return np.inf
        '''

        pass

    def _fit_points_to_shape(self, points):
        '''
        points - a numpy array of shape (number or points, 2)

        Return a shape that fits the given points.
        '''

        pass

    def _compute_arc_length_from_start(self):
        '''
        We populate the arc_distance_from_start array. At the i-th index it contains the arc distance from the first point in the
        contour to the i-th point.
        '''

        vector_to_next = self._points[1:] - self._points[:-1]
        distance_to_next = np.linalg.norm(vector_to_next, axis=1)
        arc_length_from_start = np.cumsum(distance_to_next)

        #add a zero to the beginning because the distance to the first point is 0
        self._arc_length_from_start = np.hstack([np.zeros(1), arc_length_from_start])

        return

    def _get_next_point_index_by_arc_length(self, point_index):
        '''
        Return the smallest integer i such that:
        * i > point_index
        * the arc length from self._points[point_index] to self._points[i] is less than self._min_arc_length_of_shape_segment
        '''
        #we can speed this up by using searchsorted to find arc_length_from_start[point_index] + _min_arc_length_of_segment in
        #_arc_length_from_start[point_index:]
        
        print 'getting next point index by arc length, starting from index: ', point_index
        
        #if there are no more points to add, return an index that is out of bounds
        if point_index + self._min_points_in_shape + 1 >= len(self._points):
            return len(self._points) + 1
        
        #construct an array such that the i-th index contains the arc distance from points[point_index] to
        #points[point_index+i+1]
        arc_length_from_point = self._arc_length_from_start[point_index+self._min_points_in_shape:] - self._arc_length_from_start[point_index]

        print 'arc lengths from point:'
        print arc_length_from_point
        
        #get the index of the first element in arc_length_from_point whose value is greater than self._min_arc_length_of_shape_segment
        next_index = np.searchsorted(arc_length_from_point, self._min_arc_length_of_shape_segment)

        print 'the next index: ', next_index
        
        #to get the index we want we add the index of the starting point
        next_index += point_index + self._min_points_in_shape + 1

        print 'returning: ', next_index
        
        return next_index
        
    def _get_point_range(self):
        '''
        Find the first consecutive sequence of points such that:
            * the first point in the sequence is after or equal to self._first_point_index
            * the length of the sequence is self._min_points_in_shape
            * all of the points in the sequence are marked "true" by self._point_mask

        Return a tuple (first index, last index)
        '''

        print 'getting a point range starting from: ', self._first_point_index
        
        next_point_index = self._get_next_point_index_by_arc_length(self._first_point_index)
        current_range = (self._first_point_index, next_point_index)
        #current_range = (self._first_point_index, self._first_point_index + self._min_points_in_shape)

        #keep going as long as there is still a possibility of finding a suitable range
        while current_range[1] <= len(self._points):
            print 'checking the range: ', current_range
            current_point_mask = self._point_mask[current_range[0] : current_range[1]]

            #first check if the next self._min_points_in_shape points are marked True
            #if so, this is a valid range
            if current_point_mask.all():
                print 'all points are in the mask'

                return current_range

            #otherwise, we start over with the index of the last "false" value

            print 'not all points are in the mask'
            
            last_false_index = current_range[0] + np.where(current_point_mask == False)[0][-1]

            next_point_index = self._get_next_point_index_by_arc_length(last_false_index+1)
            current_range = (self._first_point_index, next_point_index)
            #current_range = (last_false_index + 1, last_false_index + 1 + self._min_points_in_shape)

        #we did not find anything so return None
        return None


    def _extend_match(self, point_range):
        '''
        point_range - a tuple (start index, end index)

        We assume that the points in point_range fit the shape. We try to add self._extension_step_size points to either direction
        to increase the size of the fit.
        '''

        print 'extending range: ', point_range
        
        extend_right = point_range[1] < len(self._points)
        extend_left = point_range[0] > 0

        #continue as long as it is possible to extend further in one of the directions
        while extend_right or extend_left:

            #try to extend to the right
            if extend_right:
                step_size = min(self._extension_step_size, len(self._points) - point_range[1])
                
                print 'extending right with a step size: ', step_size
                
                new_point_range = (point_range[0], point_range[1] + step_size)
                added_point_mask = self._point_mask[point_range[1] : new_point_range[1]]
                
                #if all of the added points are set to "true", and the new range matches,
                #set this to be the new range and check if we can possibly extend further
                if added_point_mask.all() and self._is_match(self._points[new_point_range[0]: new_point_range[1]]):
                    point_range = new_point_range

                    print 'extended range to: ', point_range
                    
                    extend_right = point_range[1] < len(self._points)

                #otherwise, stop extending to the right
                else:
                    extend_right = False
                    
            #try to extend to the left
            if extend_left:
                step_size = min(self._extension_step_size, point_range[0])
                print 'extending left with a step size: ', step_size
                
                new_point_range = (point_range[0] - step_size, point_range[1])
                added_point_mask = self._point_mask[new_point_range[0] : point_range[0]]
                
                #if all of the added points are set to "true", and the new range matches,
                #set this to be the new range and check if we can possibly extend further
                if added_point_mask.all() and self._is_match(self._points[new_point_range[0]: new_point_range[1]]):
                    point_range = new_point_range

                    print 'extended range to: ', point_range
                    
                    extend_left = point_range[0] > 0

                #otherwise, stop extending to the left
                else:
                    extend_left = False

        #now that we are finished extending it, return the new point range
        return point_range

    def _cluster_matches(self, matched_ranges):
        '''
        matched_ranges - A list of tuples (start index, end index). Each tuple represents a segment of self._points
        that we have managed to match to a shape.

        In this function we cluster the ranges such that the ranges in each cluster are part of the same shape.
        We return these clusters as a list of lists of integers. For each list of integers L, the ranges matched_ranges[L[0]], ..., matched_ranges[L[-1]]
        all belong to the same shape.

        The general algorithm works as follows:
            * We maintain a list of "partial clusters" and "maximal clusters:. The partial clusters are clusters which could still possibly be combined with one another.
              The maximal clusters are ones to which no further partial clusters can be added.
            * At each iteration, we select the partial_cluster with the best fitting score and try to combine it with each of the other partial clusters.
              If we succeed, combine it with the partial cluster that leads to a cluster with the best score.
              If we fail, this must be a maximal cluster so we remove it from the partial clusters list and add it to the maximal clusters list.
            * We finish when partial clusters contains only one cluster. This one must be maximal, so we add it to the list of maximal clusters and return that list.

        Note: We do not simply take a random range and combine it with all other ranges that seem to be from the same shape because we could get different
              answers depending on the order in which we combine the shapes. For example, it's possible that range R1 can be combined with both R2 and R3, but the points
              in R1, R2 and R3 together cannot be fitted to a shape (for instance, the combined noise could be too big).

              The proposed algorithm is an attempt to find combinations that lead to good fits to the shape. 
        '''
        
        print 'clustering the ranges: '
        print np.array(matched_ranges)
        
        #if there are no matched points, return an empty list
        if len(matched_ranges) == 0:
            return []
        
        INDICES = 0
        POINTS = 1
        MATCH_DISTANCE = 2
        
        maximal_clusters = [] #here we store clusters as lists of indices
        
        #we initialize the list of partial clusters to contain one partial cluster per list of matched points.
        #Specifically, this is a list of tuples of the form
        #(indices of matches we have merged, union of all points that we have merged, the distance of these points from the fitted shape)
        #we record this information in addition to the partial cluster itself since it will be used many times
        partial_clusters = [([index],
                             self._points[matched_range[0] : matched_range[1]],
                             self._get_match_distance(self._points[matched_range[0] : matched_range[1]]))
                            for index, matched_range in enumerate(matched_ranges)]

        #while there are still partial clusters to combine
        while len(partial_clusters) > 1:

            print 'the partial clusters are: '
            print partial_clusters
            
            #try to combine the best partial cluster with each of the others and take the one with the best fit.
            #if it cannot be combined with anything, remove it from the partial_clusters list and put it in the maximal_clusters list.
            
            best_partial_cluster_index, best_partial_cluster = min(enumerate(partial_clusters), key=lambda x: x[1][MATCH_DISTANCE])

            print 'the index of the best cluster is: ', best_partial_cluster_index
            
            #find the index of another partial cluster such that combining it with best_partial_cluster gives the lowest match_distance
            companion_distances = [(i, self._get_match_distance(np.vstack([best_partial_cluster[POINTS], partial_cluster[POINTS]])))
                                   for i, partial_cluster in enumerate(partial_clusters) if i != best_partial_cluster_index]
            best_companion_index, best_companion_match_distance = min(companion_distances, key=lambda x: x[1])

            print 'the best companion index is: ', best_companion_index
            
            #if this distance is infinite, no match was found so remove best_points from the list
            if best_companion_match_distance == np.inf:

                print 'we did not manage to combine these clusters. adding the best partial cluster to maximal ones'
                
                maximal_clusters.append(best_partial_cluster[INDICES])
                partial_clusters.pop(best_partial_cluster_index)
                continue

            #otherwise, merge best_points with the best_companion and replace best_points by the result
            print 'we managed to combine the clusters.'
            
            best_companion = partial_clusters[best_companion_index]
            partial_clusters[best_partial_cluster_index] = (best_partial_cluster[INDICES] + best_companion[INDICES],
                                                            np.vstack([best_partial_cluster[POINTS], best_companion[POINTS]]),
                                                            best_companion_match_distance)
            #and remove the companion from the list of partial clusters.
            partial_clusters.pop(best_companion_index)

        #add the last element left in partial_clusters to the list of clusters since it must be a maximal cluster
        maximal_clusters.append(partial_clusters[0][INDICES])

        return maximal_clusters

    def _merge_matches(self, matched_ranges):
        '''
        matched_ranges - A list of tuples (start index, end index). Each tuple represents a segment of self._points
        that we have managed to match to a shape.

        We cluster the ranges as in the self._cluster_matches method, and then for each cluster we return a tuple (shape, point_mask).
        shape - a shape fitting the points in the cluster
        point_mask - a numpy array with the same shape as self._points and type np.bool. It has the value "true" exactly when the corresponding point is in the cluster.
        '''
        merged_matches = []
        
        #first get the clusters
        print 'clustering ranges.'

        #turn off clustering for the moment
        clusters = self._cluster_matches(matched_ranges)
        #clusters = [[i] for i in range(len(matched_ranges))]
        
        print 'the clusters are: '
        print clusters
        
        for cluster in clusters:
            #first get the point mask
            point_mask = np.zeros(self._points.shape[0], np.bool)

            #for each of the matched ranges in the cluster, set that range of the point mask to true
            for matched_range_index in cluster:
                matched_range = matched_ranges[matched_range_index]
                point_mask[matched_range[0] : matched_range[1]] = 1

            #now fit all the points in the cluster to a shape
            shape = self._fit_points_to_shape(self._points[point_mask])
            merged_matches.append((shape, point_mask))

        return merged_matches

    def _find_contiguous_matches(self, is_match_function):
        '''

        is_match_function - a function which takes a list of points and returns true or false

        Return a list of tuples (start index, end_index) corresponding to a list of points for which is_match_function gives True.
        end_index - start_index must be greater or equal to min_points_in_range.
        In addition, all of the points in these ranges have to marked by 1 in the point_mask.

        Algorithm:
        1) Use self._get_point_range() to get a possible range of points of the length self._min_points_in_shape
        2) Use is_match_function to check if this is a match. If so, use self._extend_match to try to extend it to a longer match
        3) Goto 1) to get the next point range
        '''

        match_ranges = [] #this will hold a list of tuples (start_index, end_index) showing which sequences of points we've managed to match.
        self._first_point_index = 0
                
        point_range = self._get_point_range()

        #keep going as long as there are still points which could fit the shape.
        while point_range is not None:

            #first check if the points fit the shape
            #if it doesn't, look for a new range of points
            if not is_match_function(self._points[point_range[0]: point_range[1]]):

                print 'no match. incrementing the starting point by the scanning step size.'
                
                #increment the first point index
                self._first_point_index = point_range[0] + self._scanning_step_size

                #get a new point range to try
                point_range = self._get_point_range()
                continue

            print 'found a match. trying to extend it.'
            #if it does, try to extend it in both directions, and add the resulting match to the list of matches
            match_range = self._extend_match(point_range)

            print 'finished extending, added range: ', match_range
            
            match_ranges.append(match_range)

            #set the first index to be after this match
            self._first_point_index = match_range[1] + 1
            #self._first_point_index = match_range[1] - self._scanning_step_size
            
            #and get a new point range
            point_range = self._get_point_range()

        return match_ranges
        
    def find_shape(self, cv_contour, point_mask=None):
        '''
        cv_contour - an opencv contour. I.e, a numpy array of shape (number of points, 1, 2) and type np.int32
        point_mask - numpy array of length (number of points in contour) and type np.bool.

        We return a list of tuples (planar_shape, point_mask)
        Where planar_shape is shape and point_mask is a numpy array of length (number of points in contour) and of type np.bool.
        point_mask[i] is True if and only if the i-th point is contained in the shape.
        '''

        self._points = np.float64(cv_contour.reshape((-1,2)))
        self._point_mask = point_mask
        self._compute_arc_length_from_start()
        
        if point_mask is None:
            self._point_mask = np.ones(self._points.shape[0], np.bool)

        print 'finding a shape in %d points.' % len(self._points)
        
        match_ranges = self._find_contiguous_matches(self._is_match)

        #We now merge the matches together
        print 'mergeing matches.'
        
        matches = self._merge_matches(match_ranges)

        return matches
            
        
