from .find_shape_in_cv_contour import FindShapeInCvContour
from ..fit_points_to_shape.fit_points_to_ellipse_stable import FitPointsToEllipseStable
import numpy as np

MIN_POINTS_ON_ELLIPSE = 10 #should be 20
MIN_ARC_LENGTH_OF_SEGMENT = 50 # in pixels!
EXTENSION_STEP_SIZE = 5
SCANNING_STEP_SIZE = 5

MAX_ALGEBRAIC_RESIDUE = 100.0
MAX_GEOMETRIC_RESIDUE = 1.0

MAX_RADIUS = 200
MIN_RADIUS = 0

#########################
# TODO:
#
# 1) DONE Change the "search for contiruous segments" code so it works by arc length, not number of points.
#
# 2) Insert a difference between verifying that a set of points is a contiguous segment, and determining whether two sets of points should be merged.
#
# 3) Add a final step for disgarding bad ellipses (bad axis lengths, not in same direction)
#
# 4) Experiment with encorprating the direction verification into the segment phase. We do not expect that *all* points have the same direction because
#    we may be fitting only a small portion of an ellipse and getting a "flattened" ellipse. However, we would expect to a high fraction of the points going in the
#    same direction, and the ones that are not in the right direction should be at the ends.
#    Alternatively, maybe only verify the direction for points that were not added in the last extension phase. See 10)
#
# 5) Only merge two ellipses if the ratio (arc length / total arc length) does not grow to much.#
#
# 6) Note: When merging, it is not necessarily true that all points initially be going in the same direction, this should only be true *after* the merge.
#
# 7) In the segment phase, demand that the jumps in angle not be to big. This is in order to prevent a scenario where we accumulate some points on an ellipse (forming
#    a very thin ellipse), and then add a point going off in some other direction. It typically happens that the segment finder considers this as one big (fat) ellipse,
#    and it will be very hard to merge this with other parts of the original ellipse.
#        In addition to making sure that the angle doesn't jump, it may make sense to make sure that the distance between points doesn't jump either.
#
#        One way to do this: parts with high curvature should have many points! Or: The tangent vector on the ellipse should not change too much from point to point.
#
#        Alternatively, this usually happens after the last extension since the algorithm gets "stuck" after adding this points. So maybe 10) would help.
#
# 8) In the segment phase, do not allow the segment finder to exit through a wall and then reenter from a wall (possibly the same one).
#
# 9) There should be sharper bounds on the residue in the segment phase than in the merge phase
#
# 10) After finding a segment, maybe throw away the points that we added during the last extension
#
# 11) Make testers showing the various modes of failure.
#
# 12) It is easy to tell if an ellipse is actually fitting a circle by checking if various segments of the matched points fit a line.
#
# 13) After finding candidates for sidelines, i.e, two concentric ellipses, try to fit the data to two concentric ellipses.
#     Also, see the "unrelated remark" at the end. I.e, can we iteratively improve our initial ellipse estimates?
#
# Somewhat unrelated - experiment with iteratively improving the segments we find with the method in "Fitting conice sections to very scattered data". It's possible
# that this would improve the segments enough to not have to worry about removing the ends.
# Check if this is fast enough to do for *every* fit.

class FindEllipseInCvContour(FindShapeInCvContour):
    '''
    In this implementation of FindShapeInCvContour we look for an ellipse.

    In particular, we use FitPointsToEllipse as our fit_points object.
    '''

    def __init__(self):
        super(FindEllipseInCvContour, self).__init__(MIN_POINTS_ON_ELLIPSE, MIN_ARC_LENGTH_OF_SEGMENT,
                                                     EXTENSION_STEP_SIZE, SCANNING_STEP_SIZE)

        self._ellipse_fitter = FitPointsToEllipseStable()

    def _is_match(self, points):
        '''
        We return True if the average algebraic distance of the points from the line is less than MAX_AVG_ALGEBRAIC_RESIDUE
        '''

        return self._get_match_distance(points) < np.inf
    
    def _get_match_distance(self, points):
        '''
        Return the average algebraic resdiue.
        '''

        ellipse = self._ellipse_fitter.fit_points(points.reshape((-1,1,2)))

        if ellipse is None:
            return np.inf

        #ellipse.compute_geometric_parameters()
        #center, axes, angle = ellipse.get_geometric_parameters(in_degrees=True)
        #print 'axes: ', axes
        #if (axes < MIN_RADIUS).any() or (axes > MAX_RADIUS).any():
        #    return np.inf
        
        #algebraic_residue = self._ellipse_fitter.get_algebraic_residue() #/ len(points)
        geometric_residue = ellipse.get_geometric_residue(points.reshape((-1,1,2))) / len(points)
        
        #get the directions of the points
        directions = [ellipse.get_direction(points[i], points[i+1]) for i in range(len(points)-1)]
        all_in_same_direction = len(set(directions[3:-3])) == 1
        
        #print 'the algebraic residue was: ', algebraic_residue
        print 'the geometric residue was: ', geometric_residue
        print 'all is same direction: ', all_in_same_direction
        
        #if all_in_same_direction and (algebraic_residue <= MAX_ALGEBRAIC_RESIDUE):
        #if (geometric_residue <= MAX_GEOMETRIC_RESIDUE):
        if all_in_same_direction and (geometric_residue <= MAX_GEOMETRIC_RESIDUE):
            return geometric_residue

        return np.inf

    def _fit_points_to_shape(self, points):
        return self._ellipse_fitter.fit_points(points.reshape((-1,1,2)))

        
    
