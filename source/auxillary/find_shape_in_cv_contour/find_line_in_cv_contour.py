from .find_shape_in_cv_contour import FindShapeInCvContour
from ..fit_points_to_shape.fit_points_to_line import FitPointsToLine
import numpy as np

MIN_POINTS_ON_LINE = 10
EXTENSION_STEP_SIZE = 5
SCANNING_STEP_SIZE = 5

MAX_ALGEBRAIC_RESIDUE = 5.0

MIN_PAIR_DIST_FOR_LINE = 30

#TODO:
# 1) PROBLEM: There seems to be a problem with using the average residue. Namely, suppose we have matched a long list of points to a line.
#             Now, if we start deviating from the line, we will only figure this out fairly late, since the deviation of the new points will be
#             dimished by the averaging with the previous points.
#    SOLUTIONS: * During the extension phase, record the current variance and don't add points that are more than 1 std away from the expected position.
#               * Put an upper bound on the number of points that are allowed to have an individual residue that is larger than a specified value.
#               * There should be some additional process that removes points from the end of an extension if they are more than 1 std away from where they should be.
#               * In addition to the above, it would be useful to only declare a set of points p1,...,pn to be a "match" if the directions dn - d(n-1) all agree up to some
#                 epsilon. This would have the added advantage of ALWAYS having two lines per sideline, even if the sideline was very thin.
#
# 2) Maybe we should use the geometric distance instead of the algebraic one. This is so easy to do that we should at least try it.
#
#
# 3) PROBLEM: Finally, some straight lines get simplified by the contour detector so that we have two far apart points representing a line with nothing in between.
#    SOLUTIONS: * Artificially add points between them. However, this is sort of silly since these points probably *should* represent lines.
#               * Add all such pairs of points as line from the very beginning, and extend them as well.
#                 After doing this, mask them out and then start the find_shape as usual.
#
# 4) PROBLEM: There are sequences of many points squished into a very small region. For instance, on the side of a circle. In this case, the algorithm currently puts a line
#             there even though this is nonsense.
#    SOLUTIONS: * Put a lower bound on the distance from the first point to the last point of a match.
#               * Before calculating this "fitting distance", normalize the points so that max_x - min_x = max_y - min_y = 1. This will remove the advantage of points
#                 clustered in a tiny area.
#
# 5) PROBLEM: There are situations where we have a good line, but at the end there is a sequence of points that are very close together and not really on the line.
#             In this case, the points at the end start getting fitted to the line as well, and we end up with a line that is a little off.
#
# 6) Get better viewing capabilities in the debugger. For example, show a blown up version of each set of points that we matched.
#
# 7) General problem, we have to normalize the distance function.


class FindLineInCvContour(FindShapeInCvContour):
    '''
    In this implementation of FindShapeInCvContour we look for a line.

    In particular, we use FitPointsToLine as our fit_points object.
    '''

    def __init__(self):
        super(FindLineInCvContour, self).__init__(MIN_POINTS_ON_LINE, EXTENSION_STEP_SIZE, SCANNING_STEP_SIZE)

        self._line_fitter = FitPointsToLine()

    def _is_match(self, points):
        '''
        We return True if the average algebraic distance of the points from the line is less than MAX_AVG_ALGEBRAIC_RESIDUE
        '''

        return self._get_match_distance(points) < np.inf
    
    def _get_match_distance(self, points):
        '''
        Return the average algebraic resdiue.
        '''

        line = self._line_fitter.fit_points(points.reshape((-1,1,2)), weight_by_interval_length=False)
        algebraic_residue = self._line_fitter.get_algebraic_residue() #/ len(points)

        #get the directions of the points
        directions = [line.get_direction(points[i], points[i+1]) for i in range(len(points)-1)]
        all_in_same_direction = len(set(directions)) == 1
        
        print 'the average algebraic residue was: ', algebraic_residue
        print 'all is same direction: ', all_in_same_direction
        
        if all_in_same_direction and (algebraic_residue <= MAX_ALGEBRAIC_RESIDUE):
            return algebraic_residue

        return np.inf

    def _fit_points_to_shape(self, points):
        return self._line_fitter.fit_points(points.reshape((-1,1,2)), weight_by_interval_length=True)
        
    def _find_contiguous_matches(self, is_match_function):
        '''
        We overload this method and supplement it with the following procedure:
        
        1) Look for all pairs of points (points[n], points[n+1]) that are separated by more than
           MIN_PAIR_DIST_FOR_LINE pixels. For each such pair, use the _extend_match method to extend it as far as possible in each
           direction, then add it to the list of contiguous matches.

           We do this by setting self._min_points_in_shape to 2 and calling FindShapeInCvContour._find_contiguous_matches with an is_match_function
           that check if the input pair of points is far enough apart.

        2) set the value of the point mask to 0 at all the points on the lines which we found in 1), and reset self._min_points_in_shape to
           it's original value and call original FindShapeInCvContour._find_contiguous_matches with the original input to find the rest of the lines.

        3) Reset the point_mask to it's original value.

        4) Return the union of the matches we found in 1) and the matches we found in 2)
        '''

        original_point_mask = self._point_mask.copy()

        #set _min_points_in_shape to 2 so that we search for pairs
        self._min_points_in_shape = 2

        #set the scanning step to 1 since we want all pairs, i.e, don't skip over anything
        self._scanning_step_size = 1

        #set the extension step to 1 since we may have two adjacent long pairs
        self._extension_step_size = 1
        
        #call the parent find_contiguous_matches with a match function that tests if the given pair of points is far enough apart.
        #since min_points_in_shape is 2, this will have the effect of looking for all pairs that satisfy this matching criteria.
        print 'searching for points that are far apart'
        matches_with_long_segment = super(FindLineInCvContour, self)._find_contiguous_matches(lambda x: np.linalg.norm(x[1] - x[0]) > MIN_PAIR_DIST_FOR_LINE)

        #mark these matches as "taken" in self._point_mask
        for match_range in matches_with_long_segment:
            self._point_mask[match_range[0] : match_range[1]] = 0

        #now run _find_contiguous_matches with the usual parameters.
        self._min_points_in_shape = MIN_POINTS_ON_LINE
        self._extension_step_size = EXTENSION_STEP_SIZE
        self._scanning_step_size = SCANNING_STEP_SIZE

        standard_matches = super(FindLineInCvContour, self)._find_contiguous_matches(is_match_function)

        #reset the point mask to it's original value
        self._point_mask = original_point_mask
        
        #combine these matches together and return them
        match_ranges = matches_with_long_segment + standard_matches
        return match_ranges
        
        
    
